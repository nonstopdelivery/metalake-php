CREATE TABLE IF NOT EXISTS `#__dispatchtrack_agents` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`agent_code` VARCHAR(50)  NOT NULL ,
`description` VARCHAR(50)  NOT NULL ,
`dispatchtrack_api_code` VARCHAR(50)  NOT NULL ,
`dispatchtrack_api_key` VARCHAR(50)  NOT NULL ,
`dispatchtrack_account` VARCHAR(50)  NOT NULL ,
`client_account_code` VARCHAR(255)  NOT NULL ,
`orderid_mod_type` VARCHAR(255)  NOT NULL ,
`orderid_mod_value` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__dispatchtrack_log` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`agent_id` INT(10)  NOT NULL ,
`action` VARCHAR(200)  NOT NULL ,
`stamp_datetime` DATETIME NOT NULL ,
`stamp_datetime_gmt` DATETIME NOT NULL ,
`objreturn` TEXT NOT NULL ,
`error` TINYINT(1)  NOT NULL ,
`error_code` VARCHAR(10)  NOT NULL ,
`error_message` VARCHAR(200)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

