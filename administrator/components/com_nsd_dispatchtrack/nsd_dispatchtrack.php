<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_dispatchtrack
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_nsd_dispatchtrack'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Nsd_dispatchtrack', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Nsd_dispatchtrackHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'nsd_dispatchtrack.php');

$controller = JControllerLegacy::getInstance('Nsd_dispatchtrack');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
