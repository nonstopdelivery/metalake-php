<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_truckmate
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_nsd_truckmate'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Nsd_truckmate', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Nsd_truckmateHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'nsd_truckmate.php');

$controller = JControllerLegacy::getInstance('Nsd_truckmate');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
