CREATE TABLE IF NOT EXISTS `#__nsd_truckmate_log` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`trace_number` VARCHAR(50)  NOT NULL ,
`datetime` DATE NOT NULL ,
`datetime_gmt` DATE NOT NULL ,
`milliseconds` BIGINT(10)  NOT NULL ,
`error` VARCHAR(50)  NOT NULL ,
`error_code` VARCHAR(50)  NOT NULL ,
`error_message` VARCHAR(200)  NOT NULL ,
`objReturn` TEXT(255)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

