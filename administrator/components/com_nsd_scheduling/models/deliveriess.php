<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Nsd_scheduling records.
 *
 * @since  1.6
 */
class Nsd_schedulingModelDeliveriess extends JModelList
{
    
        
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'order_id', 'a.`order_id`',
				'agent_id', 'a.`agent_id`',
				'client_id', 'a.`client_id`',
				'detail_line_id', 'a.`detail_line_id`',
				'delivery_zipcode', 'a.`delivery_zipcode`',
				'delivery_latitude', 'a.`delivery_latitude`',
				'delivery_longitude', 'a.`delivery_longitude`',
				'delivery_distance', 'a.`delivery_distance`',
				'delivery_date', 'a.`delivery_date`',
				'delivery_window_id', 'a.`delivery_window_id`',
				'docked_date', 'a.`docked_date`',
				'request_delivery_date', 'a.`request_delivery_date`',
				'request_time_window_start', 'a.`request_time_window_start`',
				'request_time_window_end', 'a.`request_time_window_end`',
				'stamp_datetime', 'a.`stamp_datetime`',
				'stamp_datetime_gmt', 'a.`stamp_datetime_gmt`',
				'record_type', 'a.`record_type`',
				'error', 'a.`error`',
				'error_code', 'a.`error_code`',
				'error_message', 'a.`error_message`',
				'system_error_message', 'a.`system_error_message`',
				'id', 'a.`id`',
				'result', 'a.`result`',
				'system', 'a.`system`',
				'delivery_type_id', 'a.`delivery_type_id`',
			);
		}

		parent::__construct($config);
	}

    
        
    
        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
        // List state information.
        parent::populateState('stamp_datetime', 'DESC');

        $context = $this->getUserStateFromRequest($this->context . '.context', 'context', 'com_content.article', 'CMD');
        $this->setState('filter.context', $context);

        // Split context into component and optional section
        $parts = FieldsHelper::extract($context);

        if ($parts)
        {
            $this->setState('filter.component', $parts[0]);
            $this->setState('filter.section', $parts[1]);
        }
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

                
                    return parent::getStoreId($id);
                
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__os_deliveries` AS a');
                
                

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.delivery_zipcode LIKE ' . $search . ' )');
			}
		}
                

		// Filtering agent_id
		$filter_agent_id = $this->state->get("filter.agent_id");

		if ($filter_agent_id !== null && (is_numeric($filter_agent_id) || !empty($filter_agent_id)))
		{
			$query->where("a.`agent_id` = '".$db->escape($filter_agent_id)."'");
		}

		// Filtering client_id
		$filter_client_id = $this->state->get("filter.client_id");

		if ($filter_client_id !== null && (is_numeric($filter_client_id) || !empty($filter_client_id)))
		{
			$query->where("a.`client_id` = '".$db->escape($filter_client_id)."'");
		}

		// Filtering delivery_date
		$filter_delivery_date_from = $this->state->get("filter.delivery_date.from");

		if ($filter_delivery_date_from !== null && !empty($filter_delivery_date_from))
		{
			$query->where("a.`delivery_date` >= '".$db->escape($filter_delivery_date_from)."'");
		}
		$filter_delivery_date_to = $this->state->get("filter.delivery_date.to");

		if ($filter_delivery_date_to !== null  && !empty($filter_delivery_date_to))
		{
			$query->where("a.`delivery_date` <= '".$db->escape($filter_delivery_date_to)."'");
		}

		// Filtering request_delivery_date
		$filter_request_delivery_date_from = $this->state->get("filter.request_delivery_date.from");

		if ($filter_request_delivery_date_from !== null && !empty($filter_request_delivery_date_from))
		{
			$query->where("a.`request_delivery_date` >= '".$db->escape($filter_request_delivery_date_from)."'");
		}
		$filter_request_delivery_date_to = $this->state->get("filter.request_delivery_date.to");

		if ($filter_request_delivery_date_to !== null  && !empty($filter_request_delivery_date_to))
		{
			$query->where("a.`request_delivery_date` <= '".$db->escape($filter_request_delivery_date_to)."'");
		}

		// Filtering record_type
		$filter_record_type = $this->state->get("filter.record_type");

		if ($filter_record_type !== null && (is_numeric($filter_record_type) || !empty($filter_record_type)))
		{
			$query->where("a.`record_type` = '".$db->escape($filter_record_type)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', 'stamp_datetime');
		$orderDirn = $this->state->get('list.direction', 'DESC');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
                
		foreach ($items as $oneItem)
		{

			if (isset($oneItem->agent_id))
			{
				$values    = explode(',', $oneItem->agent_id);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select b.id, b.name from htc_os_agents b where b.state = 1 HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->name;
						}
					}
				}

				$oneItem->agent_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->agent_id;
			}

			if (isset($oneItem->client_id))
			{
				$values    = explode(',', $oneItem->client_id);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select b.id, b.name from htc_os_lu_clients b where b.state = 1 HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->name;
						}
					}
				}

				$oneItem->client_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->client_id;
			}
					$oneItem->record_type = JText::_('COM_NSD_SCHEDULING_DELIVERIESS_RECORD_TYPE_OPTION_' . strtoupper($oneItem->record_type));
		}

		return $items;
	}
}
