<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Nsd_scheduling records.
 *
 * @since  1.6
 */
class Nsd_schedulingModelMatrices extends JModelList
{
    
        
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'id_client', 'a.`id_client`',
				'id_shipper', 'a.`id_shipper`',
				'id_service_level', 'a.`id_service_level`',
				'id_mode', 'a.`id_mode`',
				'id_order_status', 'a.`id_order_status`',
				'base_date', 'a.`base_date`',
				'state', 'a.`state`',
			);
		}

		parent::__construct($config);
	}

    
        
    
        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
        // List state information.
        parent::populateState('id', 'ASC');

        $context = $this->getUserStateFromRequest($this->context . '.context', 'context', 'com_content.article', 'CMD');
        $this->setState('filter.context', $context);

        // Split context into component and optional section
        $parts = FieldsHelper::extract($context);

        if ($parts)
        {
            $this->setState('filter.component', $parts[0]);
            $this->setState('filter.section', $parts[1]);
        }
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

                
                    return parent::getStoreId($id);
                
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__os_matrix` AS a');
                
                

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.id_client LIKE ' . $search . '  OR  a.id_shipper LIKE ' . $search . '  OR  a.id_service_level LIKE ' . $search . '  OR  a.id_order_status LIKE ' . $search . ' )');
			}
		}
                

		// Filtering id_client
		$filter_id_client = $this->state->get("filter.id_client");

		if ($filter_id_client !== null && (is_numeric($filter_id_client) || !empty($filter_id_client)))
		{
			$query->where("a.`id_client` = '".$db->escape($filter_id_client)."'");
		}

		// Filtering id_shipper
		$filter_id_shipper = $this->state->get("filter.id_shipper");

		if ($filter_id_shipper !== null && (is_numeric($filter_id_shipper) || !empty($filter_id_shipper)))
		{
			$query->where("a.`id_shipper` = '".$db->escape($filter_id_shipper)."'");
		}

		// Filtering id_service_level
		$filter_id_service_level = $this->state->get("filter.id_service_level");

		if ($filter_id_service_level !== null && (is_numeric($filter_id_service_level) || !empty($filter_id_service_level)))
		{
			$query->where("a.`id_service_level` = '".$db->escape($filter_id_service_level)."'");
		}

		// Filtering id_order_status
		$filter_id_order_status = $this->state->get("filter.id_order_status");

		if ($filter_id_order_status !== null && (is_numeric($filter_id_order_status) || !empty($filter_id_order_status)))
		{
			$query->where("a.`id_order_status` = '".$db->escape($filter_id_order_status)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', 'id');
		$orderDirn = $this->state->get('list.direction', 'ASC');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
                
		foreach ($items as $oneItem)
		{

			if (isset($oneItem->id_client))
			{
				$values    = explode(',', $oneItem->id_client);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select b.id, b.name from htc_os_lu_clients b where b.state = 1 HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->name;
						}
					}
				}

				$oneItem->id_client = !empty($textValue) ? implode(', ', $textValue) : $oneItem->id_client;
			}

			if (isset($oneItem->id_shipper))
			{
				$values    = explode(',', $oneItem->id_shipper);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select b.id, b.name from htc_os_lu_shipper b where b.state = 1 HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->name;
						}
					}
				}

				$oneItem->id_shipper = !empty($textValue) ? implode(', ', $textValue) : $oneItem->id_shipper;
			}

			if (isset($oneItem->id_service_level))
			{
				$values    = explode(',', $oneItem->id_service_level);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select b.id, b.name from htc_os_lu_service_levels b where b.state = 1 HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->name;
						}
					}
				}

				$oneItem->id_service_level = !empty($textValue) ? implode(', ', $textValue) : $oneItem->id_service_level;
			}

			if (isset($oneItem->id_mode))
			{
				$values    = explode(',', $oneItem->id_mode);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select b.id, b.description from htc_os_lu_mode b where b.state = 1 HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->description;
						}
					}
				}

				$oneItem->id_mode = !empty($textValue) ? implode(', ', $textValue) : $oneItem->id_mode;
			}

			if (isset($oneItem->id_order_status))
			{
				$values    = explode(',', $oneItem->id_order_status);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select b.id, b.name from htc_os_lu_order_status b where b.state = 1 HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->name;
						}
					}
				}

				$oneItem->id_order_status = !empty($textValue) ? implode(', ', $textValue) : $oneItem->id_order_status;
			}
					$oneItem->base_date = JText::_('COM_NSD_SCHEDULING_MATRICES_BASE_DATE_OPTION_' . strtoupper($oneItem->base_date));
		}

		return $items;
	}
}
