CREATE TABLE IF NOT EXISTS `#__os_deliveries` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`order_id` VARCHAR(20)  NOT NULL ,
`agent_id` TEXT NOT NULL ,
`client_id` TEXT NOT NULL ,
`detail_line_id` VARCHAR(20)  NOT NULL ,
`delivery_zipcode` VARCHAR(5)  NOT NULL ,
`delivery_latitude` DECIMAL(10,7)  NOT NULL ,
`delivery_longitude` DECIMAL(10,7)  NOT NULL ,
`delivery_distance` INT(5)  NOT NULL ,
`delivery_date` DATETIME NOT NULL ,
`delivery_window_id` TEXT NOT NULL ,
`docked_date` DATETIME NOT NULL ,
`request_delivery_date` DATETIME NOT NULL ,
`request_time_window_start` DATETIME NOT NULL ,
`request_time_window_end` DATETIME NOT NULL ,
`stamp_datetime` DATETIME NOT NULL ,
`stamp_datetime_gmt` DATETIME NOT NULL ,
`record_type` VARCHAR(255)  NOT NULL ,
`error` VARCHAR(5)  NOT NULL ,
`error_code` VARCHAR(20)  NOT NULL ,
`error_message` VARCHAR(200)  NOT NULL ,
`system_error_message` VARCHAR(200)  NOT NULL ,
`result` VARCHAR(255)  NOT NULL ,
`system` VARCHAR(255)  NOT NULL ,
`delivery_type_id` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_agents` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`description` VARCHAR(100)  NOT NULL ,
`zipcode` VARCHAR(255)  NOT NULL ,
`latitude` DECIMAL(10,7)  NOT NULL ,
`longitude` DECIMAL(10,7)  NOT NULL ,
`range_miles` INT NOT NULL ,
`max_deliveries_per_day` INT(10)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_clients` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`client_id` TEXT NOT NULL ,
`agent_id` TEXT NOT NULL ,
`max_deliveries_per_day` INT(10)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_delivery_windows` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`agent_id` TEXT NOT NULL ,
`client_id` TEXT NOT NULL ,
`day_of_week` VARCHAR(255)  NOT NULL ,
`window_name` VARCHAR(100)  NOT NULL ,
`window_start_time` TEXT NOT NULL ,
`window_end_time` TEXT NOT NULL ,
`max_deliveries` INT(10)  NOT NULL ,
`ordering` INT(5)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`geographical_area` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_delivery_intervals` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`agent_id` TEXT NOT NULL ,
`client_id` TEXT NOT NULL ,
`delivery_interval_begin` INT(11)  NOT NULL ,
`delivery_interval_end` INT(11)  NOT NULL ,
`delivery_interval_cutoff` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_matrix` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`id_client` TEXT NOT NULL ,
`id_shipper` TEXT NOT NULL ,
`id_service_level` TEXT NOT NULL ,
`id_mode` TEXT NOT NULL ,
`id_order_status` TEXT NOT NULL ,
`base_date` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_lu_clients` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`description` VARCHAR(100)  NOT NULL ,
`customer_account_code` VARCHAR(255)  NOT NULL ,
`contact_phone` VARCHAR(255)  NOT NULL ,
`email_from` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_lu_shipper` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`origid` VARCHAR(255)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`description` VARCHAR(100)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_lu_error_messages` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`error_code` VARCHAR(5)  NOT NULL ,
`error` VARCHAR(100)  NOT NULL ,
`error_message` TEXT NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__os_lu_times` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`time_label` VARCHAR(20)  NOT NULL ,
`time_value` TIME NOT NULL ,
`ordering` INT(5)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

