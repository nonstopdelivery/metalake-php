<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_nsd_scheduling/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.agent_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('agent_idhidden')){
			js('#jform_agent_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_agent_id").trigger("liszt:updated");
	js('input:hidden.client_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('client_idhidden')){
			js('#jform_client_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_client_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'deliveries.cancel') {
			Joomla.submitform(task, document.getElementById('deliveries-form'));
		}
		else {
			
			if (task != 'deliveries.cancel' && document.formvalidator.isValid(document.id('deliveries-form'))) {
				
				Joomla.submitform(task, document.getElementById('deliveries-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_nsd_scheduling&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="deliveries-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_NSD_SCHEDULING_TITLE_DELIVERIES', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('order_id'); ?>
				<?php echo $this->form->renderField('agent_id'); ?>

			<?php
				foreach((array)$this->item->agent_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="agent_id" name="jform[agent_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('client_id'); ?>

			<?php
				foreach((array)$this->item->client_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="client_id" name="jform[client_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('detail_line_id'); ?>
				<?php echo $this->form->renderField('delivery_zipcode'); ?>
				<input type="hidden" name="jform[delivery_latitude]" value="<?php echo $this->item->delivery_latitude; ?>" />
				<input type="hidden" name="jform[delivery_longitude]" value="<?php echo $this->item->delivery_longitude; ?>" />
				<input type="hidden" name="jform[delivery_distance]" value="<?php echo $this->item->delivery_distance; ?>" />
				<input type="hidden" name="jform[delivery_date]" value="<?php echo $this->item->delivery_date; ?>" />
				<input type="hidden" name="jform[delivery_window_id]" value="<?php echo $this->item->delivery_window_id; ?>" />
				<?php echo $this->form->renderField('docked_date'); ?>
				<?php echo $this->form->renderField('request_delivery_date'); ?>
				<?php echo $this->form->renderField('request_time_window_start'); ?>
				<?php echo $this->form->renderField('request_time_window_end'); ?>
				<input type="hidden" name="jform[stamp_datetime]" value="<?php echo $this->item->stamp_datetime; ?>" />
				<input type="hidden" name="jform[stamp_datetime_gmt]" value="<?php echo $this->item->stamp_datetime_gmt; ?>" />
				<?php echo $this->form->renderField('record_type'); ?>
				<input type="hidden" name="jform[error]" value="<?php echo $this->item->error; ?>" />
				<?php echo $this->form->renderField('error_code'); ?>
				<?php echo $this->form->renderField('error_message'); ?>
				<?php echo $this->form->renderField('system_error_message'); ?>
				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('result'); ?>
				<?php echo $this->form->renderField('system'); ?>
				<?php echo $this->form->renderField('delivery_type_id'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
