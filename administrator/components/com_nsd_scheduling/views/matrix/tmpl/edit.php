<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_nsd_scheduling/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.id_client').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('id_clienthidden')){
			js('#jform_id_client option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_id_client").trigger("liszt:updated");
	js('input:hidden.id_shipper').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('id_shipperhidden')){
			js('#jform_id_shipper option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_id_shipper").trigger("liszt:updated");
	js('input:hidden.id_service_level').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('id_service_levelhidden')){
			js('#jform_id_service_level option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_id_service_level").trigger("liszt:updated");
	js('input:hidden.id_mode').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('id_modehidden')){
			js('#jform_id_mode option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_id_mode").trigger("liszt:updated");
	js('input:hidden.id_order_status').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('id_order_statushidden')){
			js('#jform_id_order_status option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_id_order_status").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'matrix.cancel') {
			Joomla.submitform(task, document.getElementById('matrix-form'));
		}
		else {
			
			if (task != 'matrix.cancel' && document.formvalidator.isValid(document.id('matrix-form'))) {
				
				Joomla.submitform(task, document.getElementById('matrix-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_nsd_scheduling&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="matrix-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_NSD_SCHEDULING_TITLE_MATRIX', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('id_client'); ?>

			<?php
				foreach((array)$this->item->id_client as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="id_client" name="jform[id_clienthidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('id_shipper'); ?>

			<?php
				foreach((array)$this->item->id_shipper as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="id_shipper" name="jform[id_shipperhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('id_service_level'); ?>

			<?php
				foreach((array)$this->item->id_service_level as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="id_service_level" name="jform[id_service_levelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('id_mode'); ?>

			<?php
				foreach((array)$this->item->id_mode as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="id_mode" name="jform[id_modehidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('id_order_status'); ?>

			<?php
				foreach((array)$this->item->id_order_status as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="id_order_status" name="jform[id_order_statushidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('base_date'); ?>
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
