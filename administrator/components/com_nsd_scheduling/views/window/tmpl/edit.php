<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_nsd_scheduling/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.agent_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('agent_idhidden')){
			js('#jform_agent_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_agent_id").trigger("liszt:updated");
	js('input:hidden.client_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('client_idhidden')){
			js('#jform_client_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_client_id").trigger("liszt:updated");
	js('input:hidden.window_start_time').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('window_start_timehidden')){
			js('#jform_window_start_time option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_window_start_time").trigger("liszt:updated");
	js('input:hidden.window_end_time').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('window_end_timehidden')){
			js('#jform_window_end_time option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_window_end_time").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'window.cancel') {
			Joomla.submitform(task, document.getElementById('window-form'));
		}
		else {
			
			if (task != 'window.cancel' && document.formvalidator.isValid(document.id('window-form'))) {
				
				Joomla.submitform(task, document.getElementById('window-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_nsd_scheduling&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="window-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_NSD_SCHEDULING_TITLE_WINDOW', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('agent_id'); ?>

			<?php
				foreach((array)$this->item->agent_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="agent_id" name="jform[agent_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('client_id'); ?>

			<?php
				foreach((array)$this->item->client_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="client_id" name="jform[client_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('day_of_week'); ?>
				<?php echo $this->form->renderField('window_name'); ?>
				<?php echo $this->form->renderField('window_start_time'); ?>

			<?php
				foreach((array)$this->item->window_start_time as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="window_start_time" name="jform[window_start_timehidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('window_end_time'); ?>

			<?php
				foreach((array)$this->item->window_end_time as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="window_end_time" name="jform[window_end_timehidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('max_deliveries'); ?>
				<?php echo $this->form->renderField('state'); ?>
				<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('geographical_area'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
