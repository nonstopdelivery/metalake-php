<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking_api
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Level controller class.
 *
 * @since  1.6
 */
class Nsd_tracking_apiControllerLevel extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'levels';
		parent::__construct();
	}
}
