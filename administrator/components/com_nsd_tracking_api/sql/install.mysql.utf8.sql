CREATE TABLE IF NOT EXISTS `#__nsd_tracking_api_clients` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(50)  NOT NULL ,
`id_level` TEXT NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_tracking_api_levels` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(50)  NOT NULL ,
`description` VARCHAR(100)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_tracking_api_matrix` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`id_level` TEXT NOT NULL ,
`tracking_code` VARCHAR(50)  NOT NULL ,
`reason_code` VARCHAR(50)  NOT NULL ,
`description` VARCHAR(200)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_tracking_api_reason` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`id_level` TEXT NOT NULL ,
`reason_code` VARCHAR(50)  NOT NULL ,
`translation` VARCHAR(200)  NOT NULL ,
`notes` VARCHAR(200)  NOT NULL ,
`client_facing` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_tracking_api_status` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`id_level` TEXT NOT NULL ,
`tracking_code` VARCHAR(50)  NOT NULL ,
`description` VARCHAR(200)  NOT NULL ,
`translation` VARCHAR(200)  NOT NULL ,
`client_facing` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

