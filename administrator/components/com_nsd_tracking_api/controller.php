<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking_api
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class Nsd_tracking_apiController
 *
 * @since  1.6
 */
class Nsd_tracking_apiController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   mixed    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return   JController This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$view = JFactory::getApplication()->input->getCmd('view', 'clients');
		JFactory::getApplication()->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}


	function logger($log_msg)
	{
		# http://devf.metalake.net/administrator/index.php?option=com_nsd_tracking_api&task=logger
		
	    #$logpath	= "/var/www/vhosts/jewelers.org/subdomains/dev/httpdocs/logs/galadmin.log";
	    #$logpath	= JPATH_COMPONENT . "/logs/galadmin.log";		
	    $logpath	= "/var/www/vhosts/metalake.net/subdomains/devf/httpdocs/logs/apiadmin.log";
		#$log_msg = "here";
		
		$log_str = "[".date("Y-m-d H:i:s")."] ".$log_msg."\n\r";
		$fp = fopen($logpath,"a+");
			fwrite($fp, $log_str);
			fclose($fp);
		#echo "test";
	}

}
