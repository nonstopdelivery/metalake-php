CREATE TABLE IF NOT EXISTS `#__nsd_fileupload_mmo` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`process_id` VARCHAR(255)  NOT NULL ,
`file_name` VARCHAR(255)  NOT NULL ,
`nsd_number` VARCHAR(255)  NOT NULL ,
`vendor_name` VARCHAR(255)  NOT NULL ,
`flag` VARCHAR(20)  NOT NULL ,
`consolidation_id` VARCHAR(255)  NOT NULL ,
`carrier_pro` VARCHAR(255)  NOT NULL ,
`eta` DATETIME NOT NULL ,
`cost` VARCHAR(20)  NOT NULL ,
`detail_line_id` VARCHAR(255)  NOT NULL ,
`record_count` INT(10)  NOT NULL ,
`timestamp_received` DATETIME NOT NULL ,
`timestamp_validate` DATETIME NOT NULL ,
`timestamp_upload` DATETIME NOT NULL ,
`notes` VARCHAR(240)  NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

