<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_fileupload
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_nsd_fileupload/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'tmmo.cancel') {
			Joomla.submitform(task, document.getElementById('tmmo-form'));
		}
		else {
			
			if (task != 'tmmo.cancel' && document.formvalidator.isValid(document.id('tmmo-form'))) {
				
				Joomla.submitform(task, document.getElementById('tmmo-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_nsd_fileupload&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="tmmo-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_NSD_FILEUPLOAD_TITLE_TMMO', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('process_id'); ?>
				<?php echo $this->form->renderField('file_name'); ?>
				<?php echo $this->form->renderField('nsd_number'); ?>
				<?php echo $this->form->renderField('vendor_name'); ?>
				<?php echo $this->form->renderField('flag'); ?>
				<?php echo $this->form->renderField('consolidation_id'); ?>
				<?php echo $this->form->renderField('carrier_pro'); ?>
				<?php echo $this->form->renderField('eta'); ?>
				<?php echo $this->form->renderField('cost'); ?>
				<?php echo $this->form->renderField('detail_line_id'); ?>
				<?php echo $this->form->renderField('record_count'); ?>
				<?php echo $this->form->renderField('timestamp_received'); ?>
				<?php echo $this->form->renderField('timestamp_validate'); ?>
				<?php echo $this->form->renderField('timestamp_upload'); ?>
				<?php echo $this->form->renderField('notes'); ?>

				<?php echo $this->form->renderField('created_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
