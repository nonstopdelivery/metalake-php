<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_nsd_tracking/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.order_status').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('order_statushidden')){
			js('#jform_order_status option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_order_status").trigger("liszt:updated");
	js('input:hidden.bar_state').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('bar_statehidden')){
			js('#jform_bar_state option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_bar_state").trigger("liszt:updated");
	js('input:hidden.current_state').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('current_statehidden')){
			js('#jform_current_state option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_current_state").trigger("liszt:updated");
	js('input:hidden.status_location').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('status_locationhidden')){
			js('#jform_status_location option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_status_location").trigger("liszt:updated");
	js('input:hidden.origin_location').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('origin_locationhidden')){
			js('#jform_origin_location option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_origin_location").trigger("liszt:updated");
	js('input:hidden.destination_location').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('destination_locationhidden')){
			js('#jform_destination_location option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_destination_location").trigger("liszt:updated");
	js('input:hidden.estimated_delivery').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('estimated_deliveryhidden')){
			js('#jform_estimated_delivery option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_estimated_delivery").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'trackingmatrix.cancel') {
			Joomla.submitform(task, document.getElementById('trackingmatrix-form'));
		}
		else {
			
			if (task != 'trackingmatrix.cancel' && document.formvalidator.isValid(document.id('trackingmatrix-form'))) {
				
				Joomla.submitform(task, document.getElementById('trackingmatrix-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_nsd_tracking&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="trackingmatrix-form" class="form-validate form-horizontal">

		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_NSD_TRACKING_TITLE_TRACKINGMATRIX', true)); ?>
		<div class="row-fluid">

			<div class="span4">

				<h2>Setup</h2>
				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

				<?php echo $this->form->renderField('status_engagement'); ?>
				<?php
				foreach((array)$this->item->status_location as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="status_engagement" name="jform[status_engagementhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
				?>		


				<?php echo $this->form->renderField('delivery_type'); ?>
				<?php echo $this->form->renderField('order_type'); ?>

				<?php echo $this->form->renderField('order_status'); ?>
				<?php
				foreach((array)$this->item->order_status as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="order_status" name="jform[order_statushidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
				?>				

				<?php echo $this->form->renderField('bar_state'); ?>
				<?php
				foreach((array)$this->item->bar_state as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="bar_state" name="jform[bar_statehidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
				?>				
			
				<?php echo $this->form->renderField('bar_color'); ?>

				<?php echo $this->form->renderField('current_state'); ?>
				<?php
				foreach((array)$this->item->current_state as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="current_state" name="jform[current_statehidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
				?>				
			
				<?php echo $this->form->renderField('status_location'); ?>
				<?php
				foreach((array)$this->item->status_location as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="status_location" name="jform[status_locationhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
				?>				




				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('state'); ?></div>
				</div>
				
				<?php echo $this->form->renderField('flag_milestone'); ?>

				<?php if ($this->state->params->get('save_history', 1)) : ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
				</div>
				<?php endif; ?>


			</div> <!-- /span4 -->
			
			<div class="span4">

				<h2>Details</h2>
				<?php echo $this->form->renderField('flag_tracking_number'); ?>
				<?php echo $this->form->renderField('flag_po_number'); ?>
				<?php echo $this->form->renderField('flag_ref_1_number'); ?>
				<?php echo $this->form->renderField('flag_ref_2_number'); ?>
				<?php echo $this->form->renderField('flag_service_level'); ?>
			
			</div> <!-- /span4 -->

			<div class="span4">

				<h2>Bar & History</h2>

				<?php echo $this->form->renderField('flag_origin'); ?>
				<?php echo $this->form->renderField('origin_location'); ?>
				<?php
				foreach((array)$this->item->origin_location as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="origin_location" name="jform[origin_locationhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
				?>				
			
				<?php echo $this->form->renderField('flag_destination'); ?>

				<?php echo $this->form->renderField('destination_location'); ?>
				<?php
				foreach((array)$this->item->destination_location as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="destination_location" name="jform[destination_locationhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
				?>
			
				<?php echo $this->form->renderField('flag_estimated_delivery_date'); ?>

				<?php echo $this->form->renderField('estimated_delivery'); ?>
				<?php
				foreach((array)$this->item->estimated_delivery as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="estimated_delivery" name="jform[estimated_deliveryhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
				?>
			
				<?php echo $this->form->renderField('flag_scheduled_delivery_date'); ?>
				<?php echo $this->form->renderField('flag_scheduled_pickup_date'); ?>
				<?php echo $this->form->renderField('flag_actual_pickup_date'); ?>
				<?php echo $this->form->renderField('flag_actual_delivery_date'); ?>
				<?php echo $this->form->renderField('flag_pod_information'); ?>


			</div> <!-- /span4 -->

		</div> <!-- /row-fluid -->
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

</form>
