<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;
/**
 * trackingmatrix Table class
 *
 * @since  1.6
 */
class Nsd_trackingTabletrackingmatrix extends JTable
{
	
	/**
	 * Constructor
	 *
	 * @param   JDatabase  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		JObserverMapper::addObserverClassToClass('JTableObserverContenthistory', 'Nsd_trackingTabletrackingmatrix', array('typeAlias' => 'com_nsd_tracking.trackingmatrix'));
		parent::__construct('#__nsd_tracking_matrix', 'id', $db);
	}

	/**
	 * Overloaded bind function to pre-process the params.
	 *
	 * @param   array  $array   Named array
	 * @param   mixed  $ignore  Optional array or list of parameters to ignore
	 *
	 * @return  null|string  null is operation was satisfactory, otherwise returns an error
	 *
	 * @see     JTable:bind
	 * @since   1.5
	 */
	public function bind($array, $ignore = '')
	{
	    $date = JFactory::getDate();
		$task = JFactory::getApplication()->input->get('task');
	    
		$input = JFactory::getApplication()->input;
		$task = $input->getString('task', '');

		// Support for multiple field: delivery_type
		if (isset($array['delivery_type']))
		{
			if (is_array($array['delivery_type']))
			{
				$array['delivery_type'] = implode(',',$array['delivery_type']);
			}
			elseif (strpos($array['delivery_type'], ',') != false)
			{
				$array['delivery_type'] = explode(',',$array['delivery_type']);
			}
			elseif (strlen($array['delivery_type']) == 0)
			{
				$array['delivery_type'] = '';
			}
		}
		else
		{
			$array['delivery_type'] = '';
		}

		// Support for multiple field: order_type
		if (isset($array['order_type']))
		{
			if (is_array($array['order_type']))
			{
				$array['order_type'] = implode(',',$array['order_type']);
			}
			elseif (strpos($array['order_type'], ',') != false)
			{
				$array['order_type'] = explode(',',$array['order_type']);
			}
			elseif (strlen($array['order_type']) == 0)
			{
				$array['order_type'] = '';
			}
		}
		else
		{
			$array['order_type'] = '';
		}

		// Support for multiple field: order_status
		if (isset($array['order_status']))
		{
			if (is_array($array['order_status']))
			{
				$array['order_status'] = implode(',',$array['order_status']);
			}
			elseif (strpos($array['order_status'], ',') != false)
			{
				$array['order_status'] = explode(',',$array['order_status']);
			}
			elseif (strlen($array['order_status']) == 0)
			{
				$array['order_status'] = '';
			}
		}
		else
		{
			$array['order_status'] = '';
		}

		// Support for multiple field: bar_state
		if (isset($array['bar_state']))
		{
			if (is_array($array['bar_state']))
			{
				$array['bar_state'] = implode(',',$array['bar_state']);
			}
			elseif (strpos($array['bar_state'], ',') != false)
			{
				$array['bar_state'] = explode(',',$array['bar_state']);
			}
			elseif (strlen($array['bar_state']) == 0)
			{
				$array['bar_state'] = '';
			}
		}
		else
		{
			$array['bar_state'] = '';
		}

		// Support for multiple field: bar_color
		if (isset($array['bar_color']))
		{
			if (is_array($array['bar_color']))
			{
				$array['bar_color'] = implode(',',$array['bar_color']);
			}
			elseif (strpos($array['bar_color'], ',') != false)
			{
				$array['bar_color'] = explode(',',$array['bar_color']);
			}
			elseif (strlen($array['bar_color']) == 0)
			{
				$array['bar_color'] = '';
			}
		}
		else
		{
			$array['bar_color'] = '';
		}

		// Support for multiple field: current_state
		if (isset($array['current_state']))
		{
			if (is_array($array['current_state']))
			{
				$array['current_state'] = implode(',',$array['current_state']);
			}
			elseif (strpos($array['current_state'], ',') != false)
			{
				$array['current_state'] = explode(',',$array['current_state']);
			}
			elseif (strlen($array['current_state']) == 0)
			{
				$array['current_state'] = '';
			}
		}
		else
		{
			$array['current_state'] = '';
		}

		// Support for multiple field: status_location
		if (isset($array['status_location']))
		{
			if (is_array($array['status_location']))
			{
				$array['status_location'] = implode(',',$array['status_location']);
			}
			elseif (strpos($array['status_location'], ',') != false)
			{
				$array['status_location'] = explode(',',$array['status_location']);
			}
			elseif (strlen($array['status_location']) == 0)
			{
				$array['status_location'] = '';
			}
		}
		else
		{
			$array['status_location'] = '';
		}

		// Support for multiple field: origin_location
		if (isset($array['origin_location']))
		{
			if (is_array($array['origin_location']))
			{
				$array['origin_location'] = implode(',',$array['origin_location']);
			}
			elseif (strpos($array['origin_location'], ',') != false)
			{
				$array['origin_location'] = explode(',',$array['origin_location']);
			}
			elseif (strlen($array['origin_location']) == 0)
			{
				$array['origin_location'] = '';
			}
		}
		else
		{
			$array['origin_location'] = '';
		}

		// Support for multiple field: destination_location
		if (isset($array['destination_location']))
		{
			if (is_array($array['destination_location']))
			{
				$array['destination_location'] = implode(',',$array['destination_location']);
			}
			elseif (strpos($array['destination_location'], ',') != false)
			{
				$array['destination_location'] = explode(',',$array['destination_location']);
			}
			elseif (strlen($array['destination_location']) == 0)
			{
				$array['destination_location'] = '';
			}
		}
		else
		{
			$array['destination_location'] = '';
		}

		// Support for multiple field: estimated_delivery
		if (isset($array['estimated_delivery']))
		{
			if (is_array($array['estimated_delivery']))
			{
				$array['estimated_delivery'] = implode(',',$array['estimated_delivery']);
			}
			elseif (strpos($array['estimated_delivery'], ',') != false)
			{
				$array['estimated_delivery'] = explode(',',$array['estimated_delivery']);
			}
			elseif (strlen($array['estimated_delivery']) == 0)
			{
				$array['estimated_delivery'] = '';
			}
		}
		else
		{
			$array['estimated_delivery'] = '';
		}

		if (isset($array['params']) && is_array($array['params']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['params']);
			$array['params'] = (string) $registry;
		}

		if (isset($array['metadata']) && is_array($array['metadata']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['metadata']);
			$array['metadata'] = (string) $registry;
		}

		if (!JFactory::getUser()->authorise('core.admin', 'com_nsd_tracking.trackingmatrix.' . $array['id']))
		{
			$actions         = JAccess::getActionsFromFile(
				JPATH_ADMINISTRATOR . '/components/com_nsd_tracking/access.xml',
				"/access/section[@name='trackingmatrix']/"
			);
			$default_actions = JAccess::getAssetRules('com_nsd_tracking.trackingmatrix.' . $array['id'])->getData();
			$array_jaccess   = array();

			foreach ($actions as $action)
			{
                if (key_exists($action->name, $default_actions))
                {
                    $array_jaccess[$action->name] = $default_actions[$action->name];
                }
			}

			$array['rules'] = $this->JAccessRulestoArray($array_jaccess);
		}

		// Bind the rules for ACL where supported.
		if (isset($array['rules']) && is_array($array['rules']))
		{
			$this->setRules($array['rules']);
		}

		return parent::bind($array, $ignore);
	}

	/**
	 * This function convert an array of JAccessRule objects into an rules array.
	 *
	 * @param   array  $jaccessrules  An array of JAccessRule objects.
	 *
	 * @return  array
	 */
	private function JAccessRulestoArray($jaccessrules)
	{
		$rules = array();

		foreach ($jaccessrules as $action => $jaccess)
		{
			$actions = array();

			if ($jaccess)
			{
				foreach ($jaccess->getData() as $group => $allow)
				{
					$actions[$group] = ((bool)$allow);
				}
			}

			$rules[$action] = $actions;
		}

		return $rules;
	}

	/**
	 * Overloaded check function
	 *
	 * @return bool
	 */
	public function check()
	{
		// If there is an ordering column and this is a new row then get the next ordering value
		if (property_exists($this, 'ordering') && $this->id == 0)
		{
			$this->ordering = self::getNextOrder();
		}
		
		

		return parent::check();
	}

	/**
	 * Method to set the publishing state for a row or list of rows in the database
	 * table.  The method respects checked out rows by other users and will attempt
	 * to checkin rows that it can after adjustments are made.
	 *
	 * @param   mixed    $pks     An optional array of primary key values to update.  If not
	 *                            set the instance property value is used.
	 * @param   integer  $state   The publishing state. eg. [0 = unpublished, 1 = published]
	 * @param   integer  $userId  The user id of the user performing the operation.
	 *
	 * @return   boolean  True on success.
	 *
	 * @since    1.0.4
	 *
	 * @throws Exception
	 */
	public function publish($pks = null, $state = 1, $userId = 0)
	{
		// Initialise variables.
		$k = $this->_tbl_key;

		// Sanitize input.
		ArrayHelper::toInteger($pks);
		$userId = (int) $userId;
		$state  = (int) $state;

		// If there are no primary keys set check to see if the instance key is set.
		if (empty($pks))
		{
			if ($this->$k)
			{
				$pks = array($this->$k);
			}
			// Nothing to set publishing state on, return false.
			else
			{
				throw new Exception(500, JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
			}
		}

		// Build the WHERE clause for the primary keys.
		$where = $k . '=' . implode(' OR ' . $k . '=', $pks);

		// Determine if there is checkin support for the table.
		if (!property_exists($this, 'checked_out') || !property_exists($this, 'checked_out_time'))
		{
			$checkin = '';
		}
		else
		{
			$checkin = ' AND (checked_out = 0 OR checked_out = ' . (int) $userId . ')';
		}

		// Update the publishing state for rows with the given primary keys.
		$this->_db->setQuery(
			'UPDATE `' . $this->_tbl . '`' .
			' SET `state` = ' . (int) $state .
			' WHERE (' . $where . ')' .
			$checkin
		);
		$this->_db->execute();

		// If checkin is supported and all rows were adjusted, check them in.
		if ($checkin && (count($pks) == $this->_db->getAffectedRows()))
		{
			// Checkin each row.
			foreach ($pks as $pk)
			{
				$this->checkin($pk);
			}
		}

		// If the JTable instance value is in the list of primary keys that were set, set the instance.
		if (in_array($this->$k, $pks))
		{
			$this->state = $state;
		}

		return true;
	}

	/**
	 * Define a namespaced asset name for inclusion in the #__assets table
	 *
	 * @return string The asset name
	 *
	 * @see JTable::_getAssetName
	 */
	protected function _getAssetName()
	{
		$k = $this->_tbl_key;

		return 'com_nsd_tracking.trackingmatrix.' . (int) $this->$k;
	}

	/**
	 * Returns the parent asset's id. If you have a tree structure, retrieve the parent's id using the external key field
	 *
	 * @param   JTable   $table  Table name
	 * @param   integer  $id     Id
	 *
	 * @see JTable::_getAssetParentId
	 *
	 * @return mixed The id on success, false on failure.
	 */
	protected function _getAssetParentId(JTable $table = null, $id = null)
	{
		// We will retrieve the parent-asset from the Asset-table
		$assetParent = JTable::getInstance('Asset');

		// Default: if no asset-parent can be found we take the global asset
		$assetParentId = $assetParent->getRootId();

		// The item has the component as asset-parent
		$assetParent->loadByName('com_nsd_tracking');

		// Return the found asset-parent-id
		if ($assetParent->id)
		{
			$assetParentId = $assetParent->id;
		}

		return $assetParentId;
	}

	/**
	 * Delete a record by id
	 *
	 * @param   mixed  $pk  Primary key value to delete. Optional
	 *
	 * @return bool
	 */
	public function delete($pk = null)
	{
		$this->load($pk);
		$result = parent::delete($pk);
		
		return $result;
	}
}
