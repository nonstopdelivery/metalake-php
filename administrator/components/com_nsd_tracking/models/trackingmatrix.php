<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Nsd_tracking model.
 *
 * @since  1.6
 */
class Nsd_trackingModelTrackingmatrix extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_NSD_TRACKING';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_nsd_tracking.trackingmatrix';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Trackingmatrix', $prefix = 'Nsd_trackingTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_nsd_tracking.trackingmatrix', 'trackingmatrix',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_nsd_tracking.edit.trackingmatrix.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;

			// Support for multiple or not foreign key field: delivery_type
			$array = array();

			foreach ((array) $data->delivery_type as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->delivery_type = $array;

			// Support for multiple or not foreign key field: order_type
			$array = array();

			foreach ((array) $data->order_type as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->order_type = $array;

			// Support for multiple or not foreign key field: order_status
			$array = array();

			foreach ((array) $data->order_status as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->order_status = implode(',',$array);

			// Support for multiple or not foreign key field: bar_state
			$array = array();

			foreach ((array) $data->bar_state as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->bar_state = implode(',',$array);

			// Support for multiple or not foreign key field: bar_color
			$array = array();

			foreach ((array) $data->bar_color as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->bar_color = $array;

			// Support for multiple or not foreign key field: current_state
			$array = array();

			foreach ((array) $data->current_state as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->current_state = implode(',',$array);

			// Support for multiple or not foreign key field: status_location
			$array = array();

			foreach ((array) $data->status_location as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->status_location = implode(',',$array);

			// Support for multiple or not foreign key field: origin_location
			$array = array();

			foreach ((array) $data->origin_location as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->origin_location = implode(',',$array);

			// Support for multiple or not foreign key field: destination_location
			$array = array();

			foreach ((array) $data->destination_location as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->destination_location = implode(',',$array);

			// Support for multiple or not foreign key field: estimated_delivery
			$array = array();

			foreach ((array) $data->estimated_delivery as $value)
			{
				if (!is_array($value))
				{
					$array[] = $value;
				}
			}

			$data->estimated_delivery = implode(',',$array);
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
		}

		return $item;
	}

	/**
	 * Method to duplicate an Trackingmatrix
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_nsd_tracking'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				

				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__nsd_tracking_matrix');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}
}
