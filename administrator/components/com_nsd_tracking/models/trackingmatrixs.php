<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Nsd_tracking records.
 *
 * @since  1.6
 */
class Nsd_trackingModelTrackingmatrixs extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'state', 'a.`state`',
				'delivery_type', 'a.`delivery_type`',
				'order_type', 'a.`order_type`',
				'order_status', 'a.`order_status`',
				'id', 'a.`id`',
				'bar_state', 'a.`bar_state`',
				'bar_color', 'a.`bar_color`',
				'current_state', 'a.`current_state`',
				'status_location', 'a.`status_location`',
				'flag_tracking_number', 'a.`flag_tracking_number`',
				'flag_po_number', 'a.`flag_po_number`',
				'flag_ref_1_number', 'a.`flag_ref_1_number`',
				'flag_ref_2_number', 'a.`flag_ref_2_number`',
				'flag_service_level', 'a.`flag_service_level`',
				'flag_origin', 'a.`flag_origin`',
				'origin_location', 'a.`origin_location`',
				'flag_destination', 'a.`flag_destination`',
				'destination_location', 'a.`destination_location`',
				'flag_estimated_delivery_date', 'a.`flag_estimated_delivery_date`',
				'estimated_delivery', 'a.`estimated_delivery`',
				'flag_scheduled_delivery_date', 'a.`flag_scheduled_delivery_date`',
				'flag_scheduled_pickup_date', 'a.`flag_scheduled_pickup_date`',
				'flag_actual_pickup_date', 'a.`flag_actual_pickup_date`',
				'flag_actual_delivery_date', 'a.`flag_actual_delivery_date`',
				'flag_pod_information', 'a.`flag_pod_information`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering delivery_type
		$this->setState('filter.delivery_type', $app->getUserStateFromRequest($this->context.'.filter.delivery_type', 'filter_delivery_type', '', 'string'));

		// Filtering order_type
		$this->setState('filter.order_type', $app->getUserStateFromRequest($this->context.'.filter.order_type', 'filter_order_type', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_nsd_tracking');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.delivery_type', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__nsd_tracking_matrix` AS a');


		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.delivery_type LIKE ' . $search . '  OR  a.order_type LIKE ' . $search . '  OR  a.order_status LIKE ' . $search . ' )');
			}
		}


		// Filtering delivery_type
		$filter_delivery_type = $this->state->get("filter.delivery_type");

		if ($filter_delivery_type !== null && (is_numeric($filter_delivery_type) || !empty($filter_delivery_type)))
		{
			$query->where("a.`delivery_type` = '".$db->escape($filter_delivery_type)."'");
		}

		// Filtering order_type
		$filter_order_type = $this->state->get("filter.order_type");

		if ($filter_order_type !== null && (is_numeric($filter_order_type) || !empty($filter_order_type)))
		{
			$query->where("a.`order_type` = '".$db->escape($filter_order_type)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem)
		{
					$oneItem->delivery_type = JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_DELIVERY_TYPE_OPTION_' . strtoupper($oneItem->delivery_type));
					$oneItem->order_type = JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_ORDER_TYPE_OPTION_' . strtoupper($oneItem->order_type));

/*
			if (isset($oneItem->order_status))
			{
				$values    = explode(',', $oneItem->order_status);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select '' as 'key', '' as 'value' from htc_nsd_tracking_lu_order_status a union select b.name as 'key', b.name as 'value' from htc_nsd_tracking_lu_order_status b where b.state = 1";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$oneItem->order_status = !empty($textValue) ? implode(', ', $textValue) : $oneItem->order_status;
			}
*/			
		}


		return $items;
	}
}
