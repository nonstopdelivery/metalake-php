CREATE TABLE IF NOT EXISTS `#__sendgrid_log` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`email_to` VARCHAR(200)  NOT NULL ,
`email_from` VARCHAR(200)  NOT NULL ,
`email_subject` VARCHAR(200)  NOT NULL ,
`email_body` TEXT NOT NULL ,
`objReturn` TEXT(255)  NOT NULL ,
`error` VARCHAR(5)  NOT NULL ,
`error_code` VARCHAR(5)  NOT NULL ,
`error_message` VARCHAR(200)  NOT NULL ,
`stamp_datetime` DATETIME NOT NULL ,
`stamp_datetime_gmt` DATETIME NOT NULL ,
`note` TEXT(255)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

