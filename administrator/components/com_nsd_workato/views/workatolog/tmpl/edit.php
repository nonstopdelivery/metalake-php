<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_workato
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_nsd_workato/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'workatolog.cancel') {
			Joomla.submitform(task, document.getElementById('workatolog-form'));
		}
		else {
			
			if (task != 'workatolog.cancel' && document.formvalidator.isValid(document.id('workatolog-form'))) {
				
				Joomla.submitform(task, document.getElementById('workatolog-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_nsd_workato&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="workatolog-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_NSD_WORKATO_TITLE_WORKATOLOG', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('detailLineID'); ?>
				<?php echo $this->form->renderField('stamp_datetime'); ?>
				<?php echo $this->form->renderField('stamp_datetime_gmt'); ?>
				<?php echo $this->form->renderField('action'); ?>
				<?php echo $this->form->renderField('error'); ?>
				<?php echo $this->form->renderField('error_code'); ?>
				<?php echo $this->form->renderField('error_message'); ?>
				<?php echo $this->form->renderField('system_message'); ?>
				<?php echo $this->form->renderField('note'); ?>
				<?php echo $this->form->renderField('json_return'); ?>
				<?php echo $this->form->renderField('voiceOptIn'); ?>
				<?php echo $this->form->renderField('textOptIn'); ?>
				<?php echo $this->form->renderField('emailOptIn'); ?>
				<?php echo $this->form->renderField('entry'); ?>
				<?php echo $this->form->renderField('scheduled'); ?>
				<?php echo $this->form->renderField('docked'); ?>
				<?php echo $this->form->renderField('outForDelivery'); ?>
				<?php echo $this->form->renderField('delivered'); ?>
				<?php echo $this->form->renderField('available'); ?>
				<?php echo $this->form->renderField('exception'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
