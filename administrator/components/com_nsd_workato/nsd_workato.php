<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_workato
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_nsd_workato'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Nsd_workato', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Nsd_workatoHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'nsd_workato.php');

$controller = JControllerLegacy::getInstance('Nsd_workato');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
