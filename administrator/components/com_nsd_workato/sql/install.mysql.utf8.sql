CREATE TABLE IF NOT EXISTS `#__nsd_workato_log` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`detailLineID` VARCHAR(20)  NOT NULL ,
`stamp_datetime` DATETIME NOT NULL ,
`stamp_datetime_gmt` DATETIME NOT NULL ,
`action` VARCHAR(10)  NOT NULL ,
`error` TINYINT(1)  NOT NULL ,
`error_code` VARCHAR(10)  NOT NULL ,
`error_message` VARCHAR(200)  NOT NULL ,
`system_message` VARCHAR(255)  NOT NULL ,
`note` VARCHAR(255)  NOT NULL ,
`json_return` TEXT NOT NULL ,
`voiceOptIn` VARCHAR(100)  NOT NULL ,
`textOptIn` VARCHAR(100)  NOT NULL ,
`emailOptIn` VARCHAR(100)  NOT NULL ,
`entry` TINYINT(1)  NOT NULL ,
`scheduled` TINYINT(1)  NOT NULL ,
`docked` TINYINT(1)  NOT NULL ,
`outForDelivery` TINYINT(1)  NOT NULL ,
`delivered` TINYINT(1)  NOT NULL ,
`available` TINYINT(1)  NOT NULL ,
`exception` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

