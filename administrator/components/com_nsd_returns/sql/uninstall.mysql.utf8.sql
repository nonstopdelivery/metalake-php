DROP TABLE IF EXISTS `#__nsd_returns_log`;
DROP TABLE IF EXISTS `#__nsd_returns_data`;
DROP TABLE IF EXISTS `#__nsd_returns_data_items`;
DROP TABLE IF EXISTS `#__nsd_returns_lu_hdvendors`;
DROP TABLE IF EXISTS `#__nsd_returns_lu_piece_code`;
DROP TABLE IF EXISTS `#__nsd_returns_lu_service_levels`;
