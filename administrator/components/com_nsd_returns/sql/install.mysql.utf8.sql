CREATE TABLE IF NOT EXISTS `#__nsd_returns_log` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`process_id` VARCHAR(200)  NOT NULL ,
`datetime` DATETIME NOT NULL ,
`datetime_gmt` DATETIME NOT NULL ,
`milliseconds` VARCHAR(200)  NOT NULL ,
`user` VARCHAR(50)  NOT NULL ,
`customer_code` VARCHAR(255)  NOT NULL ,
`remote_addr` VARCHAR(50)  NOT NULL ,
`request_method` VARCHAR(10)  NOT NULL ,
`collection` VARCHAR(50)  NOT NULL ,
`command` VARCHAR(50)  NOT NULL ,
`description` TEXT NOT NULL ,
`json_call` TEXT NOT NULL ,
`return_value` TEXT NOT NULL ,
`server_values` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_returns_data` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`datetime` DATETIME NOT NULL ,
`datetime_gmt` DATETIME NOT NULL ,
`reference1` VARCHAR(100)  NOT NULL ,
`reference2` VARCHAR(100)  NOT NULL ,
`reference3` VARCHAR(100)  NOT NULL ,
`reference4` VARCHAR(100)  NOT NULL ,
`disposal` VARCHAR(100)  NOT NULL ,
`return_auth` VARCHAR(100)  NOT NULL ,
`service_level` VARCHAR(100)  NOT NULL ,
`service_level_clean` VARCHAR(100)  NOT NULL ,
`operational_code` VARCHAR(100)  NOT NULL ,
`pickup_date` DATETIME NOT NULL ,
`requested_date` DATETIME NOT NULL ,
`customer_code` VARCHAR(100)  NOT NULL ,
`origin_name` VARCHAR(100)  NOT NULL ,
`origin_addressLine1` VARCHAR(100)  NOT NULL ,
`origin_addressLine2` VARCHAR(100)  NOT NULL ,
`origin_additionalInfo1` VARCHAR(100)  NOT NULL ,
`origin_additionalInfo2` VARCHAR(100)  NOT NULL ,
`origin_city` VARCHAR(100)  NOT NULL ,
`origin_state` VARCHAR(100)  NOT NULL ,
`origin_zipCode` VARCHAR(100)  NOT NULL ,
`origin_country` VARCHAR(100)  NOT NULL ,
`origin_phone` VARCHAR(100)  NOT NULL ,
`origin_alt_phone` VARCHAR(100)  NOT NULL ,
`origin_email` VARCHAR(100)  NOT NULL ,
`destination_code` VARCHAR(100)  NOT NULL ,
`destination_name` VARCHAR(100)  NOT NULL ,
`destination_name_clean` VARCHAR(100)  NOT NULL ,
`destination_addressLine1` VARCHAR(100)  NOT NULL ,
`destination_addressLine2` VARCHAR(100)  NOT NULL ,
`destination_additionalInfo1` VARCHAR(100)  NOT NULL ,
`destination_additionalInfo2` VARCHAR(100)  NOT NULL ,
`destination_city` VARCHAR(100)  NOT NULL ,
`destination_state` VARCHAR(100)  NOT NULL ,
`destination_zipCode` VARCHAR(100)  NOT NULL ,
`destination_country` VARCHAR(100)  NOT NULL ,
`destination_phone` VARCHAR(100)  NOT NULL ,
`destination_alt_phone` VARCHAR(100)  NOT NULL ,
`destination_email` VARCHAR(100)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_returns_data_items` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`data_id` VARCHAR(100)  NOT NULL ,
`datetime` DATETIME NOT NULL ,
`datetime_gmt` DATETIME NOT NULL ,
`goods_pieces` VARCHAR(100)  NOT NULL ,
`goods_weight` VARCHAR(100)  NOT NULL ,
`goods_length` VARCHAR(100)  NOT NULL ,
`goods_width` VARCHAR(100)  NOT NULL ,
`goods_height` VARCHAR(100)  NOT NULL ,
`goods_description` VARCHAR(100)  NOT NULL ,
`goods_code` VARCHAR(100)  NOT NULL ,
`goods_code_clean` VARCHAR(100)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_returns_lu_hdvendors` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(200)  NOT NULL ,
`address` VARCHAR(200)  NOT NULL ,
`postal_city` VARCHAR(100)  NOT NULL ,
`postal_state` VARCHAR(5)  NOT NULL ,
`postal_zip` VARCHAR(20)  NOT NULL ,
`truckmate_id` VARCHAR(20)  NOT NULL ,
`left_citystatezip` VARCHAR(200)  NOT NULL ,
`left_addcitystzip` VARCHAR(200)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_returns_lu_piece_code` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`description` VARCHAR(100)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__nsd_returns_lu_service_levels` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`code` VARCHAR(255)  NOT NULL ,
`service_level` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8mb4_unicode_ci;

