<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_returns
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_nsd_returns/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'data.cancel') {
			Joomla.submitform(task, document.getElementById('data-form'));
		}
		else {
			
			if (task != 'data.cancel' && document.formvalidator.isValid(document.id('data-form'))) {
				
				Joomla.submitform(task, document.getElementById('data-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_nsd_returns&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="data-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_NSD_RETURNS_TITLE_DATA', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('datetime'); ?>
				<?php echo $this->form->renderField('datetime_gmt'); ?>
				<?php echo $this->form->renderField('reference1'); ?>
				<?php echo $this->form->renderField('reference2'); ?>
				<?php echo $this->form->renderField('reference3'); ?>
				<?php echo $this->form->renderField('reference4'); ?>
				<?php echo $this->form->renderField('disposal'); ?>
				<?php echo $this->form->renderField('return_auth'); ?>
				<?php echo $this->form->renderField('service_level'); ?>
				<?php echo $this->form->renderField('service_level_clean'); ?>
				<?php echo $this->form->renderField('operational_code'); ?>
				<?php echo $this->form->renderField('pickup_date'); ?>
				<?php echo $this->form->renderField('requested_date'); ?>
				<?php echo $this->form->renderField('customer_code'); ?>
				<?php echo $this->form->renderField('origin_name'); ?>
				<?php echo $this->form->renderField('origin_addressLine1'); ?>
				<?php echo $this->form->renderField('origin_addressLine2'); ?>
				<?php echo $this->form->renderField('origin_additionalInfo1'); ?>
				<?php echo $this->form->renderField('origin_additionalInfo2'); ?>
				<?php echo $this->form->renderField('origin_city'); ?>
				<?php echo $this->form->renderField('origin_state'); ?>
				<?php echo $this->form->renderField('origin_zipCode'); ?>
				<?php echo $this->form->renderField('origin_country'); ?>
				<?php echo $this->form->renderField('origin_phone'); ?>
				<?php echo $this->form->renderField('origin_alt_phone'); ?>
				<?php echo $this->form->renderField('origin_email'); ?>
				<?php echo $this->form->renderField('destination_code'); ?>
				<?php echo $this->form->renderField('destination_name'); ?>
				<?php echo $this->form->renderField('destination_name_clean'); ?>
				<?php echo $this->form->renderField('destination_addressLine1'); ?>
				<?php echo $this->form->renderField('destination_addressLine2'); ?>
				<?php echo $this->form->renderField('destination_additionalInfo1'); ?>
				<?php echo $this->form->renderField('destination_additionalInfo2'); ?>
				<?php echo $this->form->renderField('destination_city'); ?>
				<?php echo $this->form->renderField('destination_state'); ?>
				<?php echo $this->form->renderField('destination_zipCode'); ?>
				<?php echo $this->form->renderField('destination_country'); ?>
				<?php echo $this->form->renderField('destination_phone'); ?>
				<?php echo $this->form->renderField('destination_alt_phone'); ?>
				<?php echo $this->form->renderField('destination_email'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
