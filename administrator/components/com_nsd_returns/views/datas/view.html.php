<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_returns
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Nsd_returns.
 *
 * @since  1.6
 */
class Nsd_returnsViewDatas extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		Nsd_returnsHelper::addSubmenu('datas');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = Nsd_returnsHelper::getActions();

		JToolBarHelper::title(JText::_('COM_NSD_RETURNS_TITLE_DATAS'), 'datas.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/data';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('data.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('datas.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('data.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('datas.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('datas.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'datas.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('datas.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('datas.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'datas.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('datas.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_nsd_returns');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_nsd_returns&view=datas');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`datetime`' => JText::_('COM_NSD_RETURNS_DATAS_DATETIME'),
			'a.`pickup_date`' => JText::_('COM_NSD_RETURNS_DATAS_PICKUP_DATE'),
			'a.`origin_name`' => JText::_('COM_NSD_RETURNS_DATAS_ORIGIN_NAME'),
			'a.`origin_city`' => JText::_('COM_NSD_RETURNS_DATAS_ORIGIN_CITY'),
			'a.`origin_state`' => JText::_('COM_NSD_RETURNS_DATAS_ORIGIN_STATE'),
			'a.`origin_zipCode`' => JText::_('COM_NSD_RETURNS_DATAS_ORIGIN_ZIPCODE'),
			'a.`destination_code`' => JText::_('COM_NSD_RETURNS_DATAS_DESTINATION_CODE'),
			'a.`destination_name`' => JText::_('COM_NSD_RETURNS_DATAS_DESTINATION_NAME'),
			'a.`destination_city`' => JText::_('COM_NSD_RETURNS_DATAS_DESTINATION_CITY'),
			'a.`destination_state`' => JText::_('COM_NSD_RETURNS_DATAS_DESTINATION_STATE'),
			'a.`destination_zipCode`' => JText::_('COM_NSD_RETURNS_DATAS_DESTINATION_ZIPCODE'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
