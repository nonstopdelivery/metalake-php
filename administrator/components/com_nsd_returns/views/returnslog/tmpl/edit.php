<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_returns
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_nsd_returns/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'returnslog.cancel') {
			Joomla.submitform(task, document.getElementById('returnslog-form'));
		}
		else {
			
			if (task != 'returnslog.cancel' && document.formvalidator.isValid(document.id('returnslog-form'))) {
				
				Joomla.submitform(task, document.getElementById('returnslog-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_nsd_returns&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="returnslog-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_NSD_RETURNS_TITLE_RETURNSLOG', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('process_id'); ?>
				<?php echo $this->form->renderField('datetime'); ?>
				<?php echo $this->form->renderField('datetime_gmt'); ?>
				<?php echo $this->form->renderField('milliseconds'); ?>
				<?php echo $this->form->renderField('user'); ?>
				<?php echo $this->form->renderField('customer_code'); ?>
				<?php echo $this->form->renderField('remote_addr'); ?>
				<?php echo $this->form->renderField('request_method'); ?>
				<?php echo $this->form->renderField('collection'); ?>
				<?php echo $this->form->renderField('command'); ?>
				<?php echo $this->form->renderField('description'); ?>
				<?php echo $this->form->renderField('json_call'); ?>
				<?php echo $this->form->renderField('return_value'); ?>
				<?php echo $this->form->renderField('server_values'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
