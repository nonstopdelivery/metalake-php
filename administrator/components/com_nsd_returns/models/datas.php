<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_returns
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Nsd_returns records.
 *
 * @since  1.6
 */
class Nsd_returnsModelDatas extends JModelList
{
    
        
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'datetime', 'a.`datetime`',
				'datetime_gmt', 'a.`datetime_gmt`',
				'reference1', 'a.`reference1`',
				'reference2', 'a.`reference2`',
				'reference3', 'a.`reference3`',
				'reference4', 'a.`reference4`',
				'disposal', 'a.`disposal`',
				'return_auth', 'a.`return_auth`',
				'service_level', 'a.`service_level`',
				'service_level_clean', 'a.`service_level_clean`',
				'operational_code', 'a.`operational_code`',
				'pickup_date', 'a.`pickup_date`',
				'requested_date', 'a.`requested_date`',
				'customer_code', 'a.`customer_code`',
				'origin_name', 'a.`origin_name`',
				'origin_addressLine1', 'a.`origin_addressLine1`',
				'origin_addressLine2', 'a.`origin_addressLine2`',
				'origin_additionalInfo1', 'a.`origin_additionalInfo1`',
				'origin_additionalInfo2', 'a.`origin_additionalInfo2`',
				'origin_city', 'a.`origin_city`',
				'origin_state', 'a.`origin_state`',
				'origin_zipCode', 'a.`origin_zipCode`',
				'origin_country', 'a.`origin_country`',
				'origin_phone', 'a.`origin_phone`',
				'origin_alt_phone', 'a.`origin_alt_phone`',
				'origin_email', 'a.`origin_email`',
				'destination_code', 'a.`destination_code`',
				'destination_name', 'a.`destination_name`',
				'destination_name_clean', 'a.`destination_name_clean`',
				'destination_addressLine1', 'a.`destination_addressLine1`',
				'destination_addressLine2', 'a.`destination_addressLine2`',
				'destination_additionalInfo1', 'a.`destination_additionalInfo1`',
				'destination_additionalInfo2', 'a.`destination_additionalInfo2`',
				'destination_city', 'a.`destination_city`',
				'destination_state', 'a.`destination_state`',
				'destination_zipCode', 'a.`destination_zipCode`',
				'destination_country', 'a.`destination_country`',
				'destination_phone', 'a.`destination_phone`',
				'destination_alt_phone', 'a.`destination_alt_phone`',
				'destination_email', 'a.`destination_email`',
			);
		}

		parent::__construct($config);
	}

    
        
    
        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_nsd_returns');
		$this->setState('params', $params);

                parent::populateState('id', 'DESC');

                $start = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'int');
                $limit = $app->getUserStateFromRequest($this->context . '.limit', 'limit', 0, 'int');

                if ($limit == 0)
                {
                    $limit = $app->get('list_limit', 0);
                }

                $this->setState('list.limit', $limit);
                $this->setState('list.start', $start);
        
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

                
                    return parent::getStoreId($id);
                
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__nsd_returns_data` AS a');
                
                

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.destination_code LIKE ' . $search . '  OR  a.destination_name LIKE ' . $search . ' )');
			}
		}
                
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', 'id');
		$orderDirn = $this->state->get('list.direction', 'DESC');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
                

		return $items;
	}
}
