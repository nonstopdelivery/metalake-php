CREATE TABLE IF NOT EXISTS `#__shipnsd_api_log` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`datetime` DATETIME NOT NULL ,
`datetime_gmt` DATETIME NOT NULL ,
`milliseconds` BIGINT NOT NULL ,
`user` VARCHAR(50)  NOT NULL ,
`remote_addr` VARCHAR(50)  NOT NULL ,
`request_method` VARCHAR(10)  NOT NULL ,
`collection` VARCHAR(50)  NOT NULL ,
`command` VARCHAR(50)  NOT NULL ,
`description` TEXT NOT NULL ,
`return_value` TEXT NOT NULL ,
`server_values` TEXT NOT NULL ,
`ordering` INT NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

