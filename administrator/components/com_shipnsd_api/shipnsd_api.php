<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Shipnsd_api
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_shipnsd_api'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Shipnsd_api', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Shipnsd_apiHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'shipnsd_api.php');

$controller = JControllerLegacy::getInstance('Shipnsd_api');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
