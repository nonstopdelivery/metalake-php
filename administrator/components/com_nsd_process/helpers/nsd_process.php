<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_process
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Nsd_process helper.
 *
 * @since  1.6
 */
class Nsd_processHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_NSD_PROCESS_TITLE_UPDATES'),
			'index.php?option=com_nsd_process&view=updates',
			$vName == 'updates'
		);

JHtmlSidebar::addEntry(
			JText::_('COM_NSD_PROCESS_TITLE_LOGS'),
			'index.php?option=com_nsd_process&view=logs',
			$vName == 'logs'
		);

JHtmlSidebar::addEntry(
			JText::_('COM_NSD_PROCESS_TITLE_UPDATESLOG'),
			'index.php?option=com_nsd_process&view=updateslog',
			$vName == 'updateslog'
		);

	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_nsd_process';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}

