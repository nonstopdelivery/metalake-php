<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_process
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Log controller class.
 *
 * @since  1.6
 */
class Nsd_processControllerLog extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'logs';
		parent::__construct();
	}
}
