CREATE TABLE IF NOT EXISTS `#__process_update` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`order_id` VARCHAR(20)  NOT NULL ,
`agent_code` VARCHAR(255)  NOT NULL ,
`update_system` VARCHAR(255)  NOT NULL ,
`process_datetime` DATETIME NOT NULL ,
`process_datetime_gmt` DATETIME NOT NULL ,
`stamp_datetime` DATETIME NOT NULL ,
`stamp_datetime_gmt` DATETIME NOT NULL ,
`error` TINYINT NOT NULL ,
`note` TEXT NOT NULL ,
`data_difference` TEXT NOT NULL ,
`notify` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__process_cron_log` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`state` TINYINT NOT NULL ,
`start_datetime_gmt` DATETIME NOT NULL ,
`end_datetime_gmt` DATETIME NOT NULL ,
`start_datetime` DATETIME NOT NULL ,
`end_datetime` DATETIME NOT NULL ,
`total_time` VARCHAR(10)  NOT NULL ,
`num_processed` INT(10)  NOT NULL ,
`num_outstanding` INT(10)  NOT NULL ,
`notes` VARCHAR(200)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__process_update_error_log` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`order_id` VARCHAR(20)  NOT NULL ,
`update_system` VARCHAR(255)  NOT NULL ,
`process_datetime` DATETIME NOT NULL ,
`process_datetime_gmt` DATETIME NOT NULL ,
`stamp_datetime` DATETIME NOT NULL ,
`stamp_datetime_gmt` DATETIME NOT NULL ,
`error` TINYINT NOT NULL ,
`note` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

