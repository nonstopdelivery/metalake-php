<?php

/**
 * @version     1.0.0
 * @package     com_metalake
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Lew Sawyer <lsawyer@metalake.com> - http://www.metalake.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Metalake helper.
 */
class MetalakeHelper {

    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($vName = '') {
        		JHtmlSidebar::addEntry(
			JText::_('COM_METALAKE_TITLE_METALAKE'),
			'index.php?option=com_metalake&view=metalake',
			$vName == 'metalake'
		);

    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return	JObject
     * @since	1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_metalake';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }


}
