<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_process
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');
JLoader::register('SimpleXMLExtended', JPATH_SITE.'/components/com_nsd_dispatchtrack/controller.php');






/**
 * Class Nsd_processController
 *
 * @since  1.6
 */
 
// !NSD_PROCESSCONTROLLER 
 
class Nsd_processController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'updates');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}




	public function cron_process_update()
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_process&task=cron_process_update



		$className = "Nsd_processController";
		$functionName = "cron_process_update";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";
	
		}		

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "SELECT * from htc_process_cron_log order by id desc limit 1";
		$db->setQuery($query);
		$objLogCron = $db->loadObject();


		if ( $objLogCron->state == "1" )
		{
			$logMessage = "INSIDE | ".$className." | ".$functionName." | cron_process_update is already running. The process started at ".$objLogCron->start_datetime;	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		


			$body = "";
			$body .= "<br>cron_process_update is already running. The process started at ".$objLogCron->start_datetime."<br>";
			

			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = "lsawyer@metalake.com";
			$objSendEmailMessage->email_from = "nsdalert@metalake.com";
			$objSendEmailMessage->email_subject = "ALERT | com_nsd_process | cron_process_update";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );

			
/*
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r Alert sent";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			#send email with order details
			$objAlert = new stdClass();
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Dispatchtrack Order Entry | cron_dispatchtrack_update is already running. The process started at ".$objLogCron->start_datetime;
			$body = "";
			$body .= "<br>cron_dispatchtrack_update is already running. The process started at ".$objLogCron->start_datetime."<br>";
			$body .= "<br>text 'resetsender' to (703) 996-4322 to unlock the process.<br>";
			$objAlert->body = $body;
			$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );


			$objAlert = new stdClass();
			$objAlert->recipients = array( '7039305383@txt.att.net' );
			$objAlert->subject = "NSD MONITOR | ".$functionName;
			$objAlert->body = $body;
			$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );
*/



			$logMessage = "INSIDE | ".$className." | ".$functionName." | Alert sent";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
			
					
		}
		else
		{


			$numberOfRecords = 15;
	
	
	
			$query = "SELECT * from htc_process_update c WHERE  c.process_datetime = '0000-00-00 00:00:00' and c.stamp_datetime >= DATE_SUB(NOW(),INTERVAL 1 HOUR) order by c.error, c.id limit ".$numberOfRecords;
			$db->setQuery($query);
			$objListRecords = $db->loadObjectList();

	
			if ( count($objListRecords) > 0 )
			{
	
				$logMessage = "INSIDE | ".$className." | ".$functionName;	
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
				$local_time = date("Y-m-d H:i:s");							
				$gmt_time = gmdate("Y-m-d H:i:s");
		
		        $objData = new stdClass();
		        $objData->state = 1;
				$objData->start_datetime_gmt = $gmt_time;
				$objData->start_datetime = $local_time;
				$objData->num_outstanding = "";
		        $result = JFactory::getDbo()->insertObject('htc_process_cron_log', $objData);
		        $cronID = $db->insertid();
		
		
				
				
				$delayInSeconds = 1;
				
				$return = 1;
				$count = 0;
				
				foreach( $objListRecords as $objRecord )
				{


					$send_email = TRUE;
					
					
					switch (  $objRecord->update_system  )
					{
						
						case "dispatchtrack":
						
							$return = Nsd_processController::process_update( $objRecord );
						
						
							if ( $return == "0" )
							{
								$send_email = FALSE;
							}
						
							
							break;
							
							
							
						default:
						
							break;	
						
					}


					if ( $send_email == TRUE )
					{
						
						
/*
	
						# 10/17 COMMENTED OUT SENDING EMAIL INDIVIDUALLY - CLIENTS WANT THEM SENT IN BUNK TO EACH AGENT.
	
						$objNotify = json_decode( $objRecord->notify );
						
						
						$objSendEmailMessage = new stdClass();
						$objSendEmailMessage->email_to = $objNotify->email_to;
						$objSendEmailMessage->email_from = $objNotify->email_from;
						$objSendEmailMessage->email_subject = $objNotify->email_subject;
						$objSendEmailMessage->email_body = $objNotify->email_body;
						$objSendEmailMessage->email_category = $objNotify->email_category;
				
						$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
						
						
						if ( $objSGReturn->error == "0" )
						{
						
							$objDataUpdate = new stdClass();
							$objDataUpdate->id = $objRecord->id;
							$objDataUpdate->process_datetime = date("Y-m-d H:i:s");
							$objDataUpdate->process_datetime_gmt = gmdate("Y-m-d H:i:s");
							$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id'); 
						
						}
*/
						
												
						
					}

					$count += 1;
		
					sleep( $delayInSeconds );					
				}
				
		
				$local_time = date("Y-m-d H:i:s");							
				$gmt_time = gmdate("Y-m-d H:i:s");		
		
				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		
		        $objData = new stdClass();
		        $objData->id = $cronID;
		        $objData->state = 0;
				$objData->end_datetime_gmt = $gmt_time;
				$objData->end_datetime = $local_time;
				$objData->num_processed = ($count - 1);        
		        $objData->total_time = number_format($stamp_total_micro,2);
		        $result = JFactory::getDbo()->updateObject('htc_process_cron_log', $objData, 'id'); 
	        
	        }
			else
			{
				$logMessage = "INSIDE | ".$className." | ".$functionName." | No records to process.";	
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
			}



			#find records that are over an hour old that have not been processed and send email to itsupport@shipnsd.com

			$query = "SELECT * from htc_process_update c WHERE  c.process_datetime = '0000-00-00 00:00:00' and error = 1 and c.stamp_datetime <= DATE_SUB(NOW(),INTERVAL 1 HOUR) and update_system = 'dispatchtrack' order by c.error, c.id";
			$db->setQuery($query);
			$objListRecordsStale = $db->loadObjectList();


			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objListRecordsStale: ".print_r($objListRecordsStale, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


			foreach( $objListRecordsStale as $objStale )
			{
			
			
				$objSendEmailMessage = new stdClass();
				#$objSendEmailMessage->email_to = "lsawyer@metalake.com";
				$objSendEmailMessage->email_to = "EDI@nonstopdelivery.com";
				$objSendEmailMessage->email_from = "nsdalert@shipnsd.com";
				$objSendEmailMessage->email_subject = "Online Scheduler - Order Not Updated in Dispatchtrack";
				$objSendEmailMessage->email_body = "The customer updated the scheduled time for this order via the online scheduler but the order did not get updated in DispatchTrack: ".$objStale->order_id."<br><br>details: ".print_r($objStale->order_schedule_info, true);
				$objSendEmailMessage->email_category = "scheduling support";
		
				$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );			
		

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objSGReturn: ".print_r($objSGReturn, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				if ( $objSGReturn->error == "0" )
				{
					$objDataUpdate = new stdClass();
					$objDataUpdate->id = $objStale->id;
					$objDataUpdate->error = "-1";
					$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id'); 
				}


			}





		}

		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);		

		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




	public static function process_update( $objRecord )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_process&task=process_update

		# this funciton called to update an order in dispatchtrack from order scheduling.

		$className = "Nsd_processController";
		$functionName = "process_update";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "process_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objRecord: ".print_r($objRecord, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		if ( count($objRecord) > 0 )
		{


			#Get order data from TM; if error (e.g. no response from TM), skip.
			
			$objInput = new stdClass();
	        $objInput->flag_api = "0";
	        $objInput->trace_number = $objRecord->order_id;
	        $objInput->error_code_test = "";
			
			
			$objTruckmate = Nsd_truckmateController::getTruckmateData( $objInput );
			
		
			
			if ( $objTruckmate->error == "0" )
			{

/*
				$objDataUpdate = new stdClass();
				$objDataUpdate->id = $objRecord->id;
				$objDataUpdate->process_datetime = date("Y-m-d H:i:s");
				$objDataUpdate->process_datetime_gmt = gmdate("Y-m-d H:i:s");
				$objDataUpdate->stamp_datetime = date("Y-m-d H:i:s");
				$objDataUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
				$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id');  
*/
				
				$logMessage = "INSIDE | ".$className." | ".$functionName. " | In Truckmate";
				$logMessage .= "\n\r objTruckmate: ".print_r($objTruckmate, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				


				$objInputDispatchtrack = new stdClass();
				$objInputDispatchtrack->agent_code = $objTruckmate->order->CARE_OF;
				$objInputDispatchtrack->customer_account_code = $objTruckmate->order->CUSTOMER;
				$objInputDispatchtrack->order_id = $objTruckmate->order->BILL_NUMBER;

				$logMessage = "INSIDE | ".$className." | ".$functionName. " | Is dispatchtrack agent 1";
				$logMessage .= "\n\r objInputDispatchtrack: ".print_r($objInputDispatchtrack, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
				$objAgentDT = Nsd_dispatchtrackController::determine_dispatchtrack_agent( $objInputDispatchtrack );


				$logMessage = "INSIDE | ".$className." | ".$functionName. " | Is dispatchtrack agent 2";
				$logMessage .= "\n\r objAgentDT: ".print_r($objAgentDT, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				
				#Get order data from DT; if error (e.g. no response from DT), skip.
				$objDispatchtrack = Nsd_dispatchtrackController::get_dispatchtrack_order( $objAgentDT );


				$logMessage = "INSIDE | ".$className." | ".$functionName. " | Is dispatchtrack order";
				$logMessage .= "\n\r objDispatchtrack: ".print_r($objDispatchtrack, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				
				if ( $objDispatchtrack->error == "0" )
				{

					$objDataUpdate = new stdClass();
					$objDataUpdate->id = $objRecord->id;
					$objDataUpdate->process_datetime = date("Y-m-d H:i:s");
					$objDataUpdate->process_datetime_gmt = gmdate("Y-m-d H:i:s");
					#$objDataUpdate->stamp_datetime = date("Y-m-d H:i:s");
					#$objDataUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
					$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id');  
					
					$logMessage = "INSIDE | ".$className." | ".$functionName. " | In Dispatchtrack";
					$logMessage .= "\n\r objDispatchtrack: ".print_r($objDispatchtrack, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



					#compare customer name, address, phones and other data as needed. Flag diffs.


					$arrDataDiff = array();
					
					$DTDestName = $objDispatchtrack->first_name." ".$objDispatchtrack->last_name;
					
					if ( $objTruckmate->order->DESTNAME != $DTDestName )
					{
						$objDataDifference = new stdClass();
						$objDataDifference->truckmate_DESTNAME = $objTruckmate->order->DESTNAME;
						$objDataDifference->dispatchtrack_DESTNAME = $DTDestName;

						$arrDataDiff[] = $objDataDifference;
					}

					
					
					if ( $objTruckmate->order->DESTADDR1 != $objDispatchtrack->address1 )
					{
						$objDataDifference = new stdClass();
						$objDataDifference->truckmate_DESTADDR1 = $objTruckmate->order->DESTADDR1;
						$objDataDifference->dispatchtrack_address1 = $objDispatchtrack->address1;

						$arrDataDiff[] = $objDataDifference;
					}

					if ( $objTruckmate->order->DESTCITY != $objDispatchtrack->city )
					{
						$objDataDifference = new stdClass();
						$objDataDifference->truckmate_DESTCITY = $objTruckmate->order->DESTCITY;
						$objDataDifference->dispatchtrack_city = $objDispatchtrack->city;

						$arrDataDiff[] = $objDataDifference;
					}


					if ( $objTruckmate->order->DESTPROV != $objDispatchtrack->state )
					{
						$objDataDifference = new stdClass();
						$objDataDifference->truckmate_DESTPROV = $objTruckmate->order->DESTPROV;
						$objDataDifference->dispatchtrack_state = $objDispatchtrack->state;

						$arrDataDiff[] = $objDataDifference;
					}


					if ( $objTruckmate->order->DESTPC != $objDispatchtrack->zip )
					{
						$objDataDifference = new stdClass();
						$objDataDifference->truckmate_DESTPC = $objTruckmate->order->DESTPC;
						$objDataDifference->dispatchtrack_zip = $objDispatchtrack->zip;

						$arrDataDiff[] = $objDataDifference;
					}


					$TM_destphone = preg_replace("/[^A-Za-z0-9 ]/", '', $objTruckmate->order->DESTPHONE);

					if ( $TM_destphone != $objDispatchtrack->phone1 )
					{
						$objDataDifference = new stdClass();
						$objDataDifference->truckmate_DESTPHONE = $objTruckmate->order->DESTPHONE;
						$objDataDifference->dispatchtrack_phone1 = $objDispatchtrack->phone1;

						$arrDataDiff[] = $objDataDifference;
					}
					
					$TM_destcell = preg_replace("/[^A-Za-z0-9 ]/", '', $objTruckmate->order->DESTCELL);

					if ( $TM_destcell != $objDispatchtrack->phone2 )
					{
						$objDataDifference = new stdClass();
						$objDataDifference->truckmate_DESTCELL = $objTruckmate->order->DESTCELL;
						$objDataDifference->dispatchtrack_phone2 = $objDispatchtrack->phone2;

						$arrDataDiff[] = $objDataDifference;
					}										

					
					if ( $objTruckmate->order->SERVICE_LEVEL != $objDispatchtrack->service_type )
					{
						$objDataDifference = new stdClass();
						$objDataDifference->truckmate_SERVICE_LEVEL = $objTruckmate->order->SERVICE_LEVEL;
						$objDataDifference->dispatchtrack_service_type = $objDispatchtrack->service_type;

						$arrDataDiff[] = $objDataDifference;
					}
					


					if ( count($arrDataDiff) > 0 )
					{
						
						$json_arrDataDiff = json_encode($arrDataDiff);
						
						$objDataUpdate = new stdClass();
						$objDataUpdate->id = $objRecord->id;
						$objDataUpdate->data_difference = $json_arrDataDiff;
						$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id'); 

						
					}



					
					#Update DT; if error (e.g. no response from DT), skip.
					
					

					$objUpdate = new stdClass();
					
					$objUpdate->agent_id = $objAgentDT->agent->id;
					$objUpdate->service_order_id = $objDispatchtrack->service_order_id;
					$objUpdate->account = $objDispatchtrack->account;
					$objUpdate->service_delivery_type = $objDispatchtrack->service_type;
					$objUpdate->destination_name = $DTDestName;
			
					$objUpdate->destination_address_1 = $objDispatchtrack->address1;
					$objUpdate->destination_city = $objDispatchtrack->city;
					$objUpdate->destination_state = $objDispatchtrack->state;
					$objUpdate->destination_zip = $objDispatchtrack->zip;
			
					$objUpdate->phone_1 = $objDispatchtrack->phone1;
					$objUpdate->phone_2 = $objDispatchtrack->phone2;
			
					$objUpdate->request_delivery_date_val = date("Y-m-d",strtotime($objTruckmate->order->DELIVER_BY));
					$objUpdate->request_time_window_start_val = date("Y-m-d H:i:s",strtotime($objTruckmate->order->DELIVER_BY));
					$objUpdate->request_time_window_end_val = date("Y-m-d H:i:s",strtotime($objTruckmate->order->DELIVER_BY_END));


					$logMessage = "INSIDE | ".$className." | ".$functionName. " | In Dispatchtrack";
					$logMessage .= "\n\r objUpdate: ".print_r($objUpdate, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					
					$objDTReturn = Nsd_dispatchtrackController::update_dispatchtrack_order( $objUpdate );
					

					if ( $objDTReturn->error == "0" )
					{

						$objDataUpdate = new stdClass();
						$objDataUpdate->id = $objRecord->id;
						$objDataUpdate->error = '0';
						$objDataUpdate->note = '';
						$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id'); 
						
						$return = 1;
						
					}
					else
					{

						$objDataUpdate = new stdClass();
						$objDataUpdate->id = $objRecord->id;
						$objDataUpdate->note = '';
						$objDataUpdate->error = '1';
						$objDataUpdate->note = 'Dispatchtrack: '.$objDTReturn->error_code.': '.$objDTReturn->error_message;
						$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id'); 
						
						$return = 0;
						
					}





				}
				else
				{

					$objDataUpdate = new stdClass();
					$objDataUpdate->id = $objRecord->id;
					$objDataUpdate->error = '1';
					$objDataUpdate->note = 'Dispatchtrack: '.$objDispatchtrack->error_code.': '.$objDispatchtrack->error_message;
					#$objDataUpdate->stamp_datetime = date("Y-m-d H:i:s");
					#$objDataUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
					$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id');


					$objDataInsert = new stdClass();
					$objDataInsert->order_id = $objRecord->order_id;
					$objDataInsert->update_system = "Dispatchtrack";
					$objDataInsert->process_datetime = date("Y-m-d H:i:s");
					$objDataInsert->process_datetime_gmt = gmdate("Y-m-d H:i:s");
					$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
					$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
					$objDataInsert->error = '1';
					$objDataInsert->note = 'Dispatchtrack: '.$objDispatchtrack->error_code.': '.$objDispatchtrack->error_message;
					$result = JFactory::getDbo()->insertObject('htc_process_update_error_log', $objDataInsert);
					$EID = $db->insertid();
					
					
					
					
					
					$logMessage = "INSIDE | ".$className." | ".$functionName. " | Error - Not in Dispatchtrack";
					$logMessage .= "\n\r Dispatchtrack: ".print_r($objDispatchtrack, true);
					$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
					$logMessage .= "\n\r EID: ".print_r($EID, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

					$return = 0;

				}

				


			}
			else
			{

				$objDataUpdate = new stdClass();
				$objDataUpdate->id = $objRecord->id;
				$objDataUpdate->error = '1';
				$objDataUpdate->note = 'Truckmate: '.$objTruckmate->error_code.': '.$objTruckmate->error_message;
				$objDataUpdate->stamp_datetime = date("Y-m-d H:i:s");
				$objDataUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
				$result = JFactory::getDbo()->updateObject('htc_process_update', $objDataUpdate, 'id'); 


				$objDataInsert = new stdClass();
				$objDataInsert->order_id = $objRecord->order_id;
				$objDataInsert->update_system = "Dispatchtrack";
				$objDataInsert->process_datetime = date("Y-m-d H:i:s");
				$objDataInsert->process_datetime_gmt = gmdate("Y-m-d H:i:s");
				$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
				$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
				$objDataInsert->error = '1';
				$objDataInsert->note = 'Truckmate: '.$objTruckmate->error_code.': '.$objTruckmate->error_message;
				$result = JFactory::getDbo()->insertObject('htc_process_update_error_log', $objDataInsert);
				$EID = $db->insertid();

				
				$logMessage = "INSIDE | ".$className." | ".$functionName. " | Error - Not in Truckmate";
				$logMessage .= "\n\r objTruckmate: ".print_r($objTruckmate, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
				
				$return = 0;
			}
			

		}
		else
		{

			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

			$logMessage = "INSIDE | ".$className." | ".$functionName." |  No record to send. | seconds: ".number_format($stamp_total_micro,2);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			$return = 0;
			
		}		






		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r return: ".print_r($return, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $return;
		
	}





// !ORDER ENTRY


	public function cron_process_orderentry( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_process&task=cron_process_orderentry

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_process&task=cron_process_orderentry

		# http://nsddev.metalake.net/index.php?option=com_nsd_process&task=cron_process_orderentry
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=cron_process_orderentry
		

		$className = "Nsd_processController";
		$functionName = "cron_process_orderentry";
		
		$db = JFactory::getDBO();

		#ML0628
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$returnOrderEntry = Nsd_processController::orderentry_collector();
		
		if ( $returnOrderEntry  )
		{
			$returnOrderEntryCleaner = Nsd_processController::orderentry_cleaner();			
		}


		#prune the processed directory
		Nsd_processController::orderentry_prune_directory( );
		

		# was commented out ?ported
		#prune log files
		#Nsd_orderentryController::pruneLogs();


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	



	public static function orderentry_collector( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_collector

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_collector
		
		# http://dev6.metalake.net/index.php?option=com_nsd_process&task=orderentry_collector
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=orderentry_collector
		

		$className = "Nsd_processController";
		$functionName = "orderentry_collector";
		
		$db = JFactory::getDBO();


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass('com_nsd_process');
        
		$paramsComponent->directoryInbound		= $params->get('directory_inbound');
		$paramsComponent->directoryProcessed	= $params->get('directory_processed');
		$paramsComponent->directorySkipped		= $params->get('directory_skipped');

		$paramsComponent->pathInbound			=  JPATH_SITE."/".$paramsComponent->directoryInbound."/";
		$paramsComponent->pathProcessed			=  JPATH_SITE."/".$paramsComponent->directoryProcessed."/";
		$paramsComponent->pathSkipped			=  JPATH_SITE."/".$paramsComponent->directorySkipped."/";


 /*
       $app       		= JFactory::getApplication();
        $params    		= $app->getParams('com_nsd_process');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->orderentry_ftp_prod_server			= $params->get('orderentry_ftp_prod_server');
		$paramsComponent->orderentry_ftp_prod_username			= $params->get('orderentry_ftp_prod_username');
		$paramsComponent->orderentry_ftp_prod_userpass			= $params->get('orderentry_ftp_prod_userpass');
		$paramsComponent->orderentry_ftp_prod_serverpath		= $params->get('orderentry_ftp_prod_serverpath');
		$paramsComponent->orderentry_ftp_prod_path_inbound		= $params->get('orderentry_ftp_prod_path_inbound');
		$paramsComponent->orderentry_ftp_prod_path_processed	= $params->get('orderentry_ftp_prod_path_processed');
		$paramsComponent->orderentry_ftp_prod_path_skipped		= $params->get('orderentry_ftp_prod_path_skipped');
		$paramsComponent->orderentry_ftp_prod_file_directory 	= $params->get('orderentry_ftp_prod_file_directory');

		$paramsComponent->orderentry_ftp_is_production 			= $params->get('orderentry_ftp_is_production');

		$paramsComponent->orderentry_ftp_dev_server				= $params->get('orderentry_ftp_dev_server');
		$paramsComponent->orderentry_ftp_dev_username			= $params->get('orderentry_ftp_dev_username');
		$paramsComponent->orderentry_ftp_dev_userpass			= $params->get('orderentry_ftp_dev_userpass');
		$paramsComponent->orderentry_ftp_dev_serverpath			= $params->get('orderentry_ftp_dev_serverpath');
		$paramsComponent->orderentry_ftp_dev_path_inbound		= $params->get('orderentry_ftp_dev_path_inbound');
		$paramsComponent->orderentry_ftp_dev_path_processed		= $params->get('orderentry_ftp_dev_path_processed');
		$paramsComponent->orderentry_ftp_dev_path_skipped		= $params->get('orderentry_ftp_dev_path_skipped');
		$paramsComponent->orderentry_ftp_dev_file_directory 	= $params->get('orderentry_ftp_dev_file_directory');

		
		if ( $paramsComponent->orderentry_ftp_is_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";
            
            
			$objFTP = new stdClass();
			$objFTP->server = $paramsComponent->orderentry_ftp_prod_server;		
			$objFTP->user_name = $paramsComponent->orderentry_ftp_prod_username;
			$objFTP->user_pass = $paramsComponent->orderentry_ftp_prod_userpass;
			$objFTP->server_path = $paramsComponent->orderentry_ftp_prod_serverpath;
			$objFTP->path = $paramsComponent->orderentry_ftp_prod_path_inbound;
			$objFTP->pathProcessed = $paramsComponent->orderentry_ftp_prod_path_processed;
			$objFTP->pathSkipped = $paramsComponent->orderentry_ftp_prod_path_skipped;
			$objFTP->fileDir = $paramsComponent->orderentry_ftp_prod_file_directory;            

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";
	

			$objFTP = new stdClass();
			$objFTP->server = $paramsComponent->orderentry_ftp_dev_server;		
			$objFTP->user_name = $paramsComponent->orderentry_ftp_dev_username;
			$objFTP->user_pass = $paramsComponent->orderentry_ftp_dev_userpass;
			$objFTP->server_path = $paramsComponent->orderentry_ftp_dev_serverpath;	
			$objFTP->path = $paramsComponent->orderentry_ftp_dev_path_inbound;
			$objFTP->pathProcessed = $paramsComponent->orderentry_ftp_dev_path_processed;
			$objFTP->pathSkipped = $paramsComponent->orderentry_ftp_dev_path_skipped;
			$objFTP->fileDir = $paramsComponent->orderentry_ftp_dev_file_directory;	

	
		}	
*/

        $objLogger = new stdClass();
        $objLogger->logger = 0;
        $objLogger->logFile = "orderentry_verbose";
        $objLogger->loggerProd = 1;
        $objLogger->logFileProd = "orderentry_prod";

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

/*
		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objFTP: ".print_r($objFTP, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
*/


		#$arrFiles = Nsd_processController::orderentry_ftp_file_list( $objFTP );


		$arrFiles = JFolder::files( $paramsComponent->pathInbound );


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r arrFiles: ".print_r($arrFiles, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$totalRecords = 0;

		if ( count($arrFiles) > 0 )
		{

			foreach( $arrFiles as $file_name  )
			{

				$datetime = date("Y-m-d H:i:s");
				$datetime_gmt = gmdate("Y-m-d H:i:s");
				$timestamp = date("YmdHisD").microtime(true);

				$objFile = new stdClass();


				$objFile->file_name = $file_name;
				$objFile->file_name_processed = $timestamp."-".$objFile->file_name;
				
/*
				$objFile->filePath = $objFTP->fileDir.$file_name;
				$objFile->fileFullServerPath = $objFTP->server_path.$objFTP->path.$file_name;
				$objFile->fileProcessedFullServerPath = $objFTP->server_path.$objFTP->pathProcessed.$objFile->file_name_processed;
				$objFile->fileSkippedFullServerPath = $objFTP->server_path.$objFTP->pathSkipped.$objFile->file_name;
*/

				
				$objFile->fileFullServerPath = $paramsComponent->pathInbound.$file_name;
				$objFile->fileProcessedFullServerPath = $paramsComponent->pathProcessed.$objFile->file_name_processed;
				$objFile->fileSkippedFullServerPath = $paramsComponent->pathSkipped.$objFile->file_name;




				$objFileInfo = Nsd_processController::orderentry_parse_filename( $file_name );


				if ( $objFileInfo->errorState == "0" )
				{
					$objFile->agent_id = $objFileInfo->agent_id;
					$objFile->origin = $objFileInfo->origin;

					$objFile = Nsd_processController::orderentry_parse_save( $objFile );
	
					if ( $objFile->errorState == "0" && ( $objFile->stage == "data saved" || $objFile->stage == "file size zero" ) )
					{
	
						if ( JFile::move( $objFile->fileFullServerPath, $objFile->fileProcessedFullServerPath ) )
						{
							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->file_name." moved/renamed ".$objFile->fileProcessedFullServerPath;
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						}
						else
						{
							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->file_name." NOT moved/renamed ".$objFile->fileProcessedFullServerPath;
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						}
	
					}
					else
					{
						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->file_name." Error ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
					}
	
					$last_status = "File saved";

					$objLog = new stdClass();
					
					$objLog->agent_id = $objFileInfo->agent_id;
					$objLog->origin = $objFileInfo->origin;
					$objLog->file_name = $objFile->file_name;
					$objLog->file_name_processed = $objFile->file_name_processed;
					$objLog->last_status = $last_status;
					$objLog->count_records = $objFile->countRecords;
					$objLog->stamp_datetime = $datetime;
					$objLog->stamp_datetime_gmt = $datetime_gmt;
					
	                $db->insertObject('htc_nsd_orderentry_log', $objLog);
	                $logid = $db->insertid();
					
					$totalRecords += $objFile->countRecords;
					
				}
				else
				{
					$last_status = $objFileInfo->errorMessage;

					if ( JFile::move( $objFile->fileFullServerPath, $objFile->fileSkippedFullServerPath ) )
					{
						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->file_name." moved/renamed ".$objFile->fileSkippedFullServerPath;
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					}
					else
					{
						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->file_name." NOT moved/renamed ".$objFile->fileSkippedFullServerPath;
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					}


					
					$objLog = new stdClass();
					
					$objLog->agent_id = $objFileInfo->agent_id;
					$objLog->origin = $objFileInfo->origin;
					$objLog->file_name = $objFile->file_name;
					$objLog->file_name_processed = $objFile->file_name;
					$objLog->last_status = $last_status." - file placed in SKIPPED";
					$objLog->count_records = 0;
					$objLog->stamp_datetime = $datetime;
					$objLog->stamp_datetime_gmt = $datetime_gmt;
					
	                $db->insertObject('htc_nsd_orderentry_log', $objLog);
	                $logid = $db->insertid();					
					
				}




			
			}
		
			$return = 1;
			
		}
		else
		{

			$logMessage = "INSIDE | ".$className." | ".$functionName." | No files to process";
			#$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			$return = 0;
		}



		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objFTP: ".print_r($objFTP, true);
		$logMessage .= "\n\r arrFiles: ".print_r($arrFiles, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | totalFiles: ".count($arrFiles)." | totalRecords: ".$totalRecords." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "END | ".$className." | ".$functionName." | totalFiles: ".count($arrFiles)." | totalRecords: ".$totalRecords." | seconds: ".number_format($stamp_total_micro,2);			
		#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $return;
	}	



	public static function orderentry_ftp_file_list( $objFTP )
	{
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_ftp_file_list

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_ftp_file_list
		
		# http://dev6.metalake.net/index.php?option=com_nsd_process&task=orderentry_ftp_file_list
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=orderentry_ftp_file_list		
		

		$className = "Nsd_processController";
		$functionName = "orderentry_ftp_file_list";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		// set up basic connection
		$conn_id = ftp_connect($objFTP->server);
		
		// login with username and password
		$login_result = ftp_login($conn_id, $objFTP->user_name, $objFTP->user_pass);
		
		ftp_chdir ( $conn_id , $directory = $objFTP->path );
		
		$contents = ftp_nlist($conn_id, ".");

		$arrFiles = array_filter($contents, array("Nsd_processController","orderentry_filter_wto"));

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r arrFiles: ".print_r($arrFiles, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		// close the connection
		ftp_close($conn_id);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $arrFiles;
		
	}



	public static function orderentry_filter_wto( $var )
	{
	    
	    return(  (strtolower(substr( $var, 0, 10 )) == 'orderentry' ) );
	    
	}



	public static function orderentry_parse_filename( $file_name )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_orderentry&task=orderentry_parse_filename

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_orderentry&task=orderentry_parse_filename
		
		# http://dev6.metalake.net/index.php?option=com_nsd_process&task=orderentry_parse_filename
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=orderentry_parse_filename		

		$className = "Nsd_processController";
		$functionName = "orderentry_parse_filename";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrFileNameInfo = explode( "-", $file_name );

			$query = "SELECT *
			FROM htc_dispatchtrack_agents
			WHERE UPPER(agent_code) = '".trim($arrFileNameInfo[1])."' AND state = '1' ";
			$db->setQuery($query);
			$objAgent = $db->loadObject();

			$objFileInfo = new stdClass();

			if ( is_object($objAgent) )
			{
				$objFileInfo->errorState = "0";
				$objFileInfo->errorMessage = "";
				$objFileInfo->agent_id = $objAgent->id;
				$objFileInfo->origin = trim(strtolower(substr($arrFileNameInfo[2],0,2)));
				
			}
			else
			{
				$objFileInfo->errorState = "1";
				$objFileInfo->errorMessage = "Agent does not exist";
				$objFileInfo->agent_id = "";
				$objFileInfo->origin = "";				
			}

			


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r file_name: ".print_r($file_name, true);
		$logMessage .= "\n\r arrFileNameInfo: ".print_r($arrFileNameInfo, true);
		$logMessage .= "\n\r objFileInfo: ".print_r($objFileInfo, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		return $objFileInfo;

		
	}



	public static function orderentry_parse_save( $objFile )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_parse_save

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_parse_save

		# http://dev6.metalake.net/index.php?option=com_nsd_process&task=orderentry_parse_save
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=orderentry_parse_save


		$className = "Nsd_processController";
		$functionName = "orderentry_parse_save";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		
		if ( file_exists( $objFile->fileFullServerPath ) )
		{
		    # file exists
		    
		    
		    $filesize = filesize( $objFile->fileFullServerPath  );
		    
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r filesize: ".print_r($filesize, true);	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			    
		    
		    
		    if ( $filesize > 0 )
		    {

			    #$delimiter = ( $objFile->origin == "rh"  ) ? "\t" : "," ;
			    $delimiter = "," ;
			    
				#$csvData = file_get_contents( $objFile->filePath );
				$csvData = file_get_contents( $objFile->fileFullServerPath );				
				
	
				$logMessage = "INSIDE | ".$className." | ".$functionName;	
				$logMessage .= "\n\r csvData: ".print_r($csvData, true);	
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				$lines = explode(PHP_EOL, $csvData);
				
				$arrData = array();
				
				foreach ($lines as $line) {
				    $arrData[] = str_getcsv($line, $delimiter);
				}
				$arrCount = count($arrData);
			    
				
				$countRecords = 0;
	
				foreach ($arrData as $key => $arrRowValues)
				{
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r key: ".print_r($key, true);
					$logMessage .= "\n\r arrRowValues: ".print_r($arrRowValues, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
				    if ( $key != "0" && $key < ($arrCount -1) && $arrRowValues[0] != "" && $arrRowValues[1] != "" ) 
					{
	
						foreach ($arrRowValues as $key2 => $value) 
						{
						    if (is_null($value) || $value == "NULL" ) { $arrRowValues[$key2] = ""; }
						}
	
						$appointment_start_time = ( $objFile->origin == "tm" ) ? date("H:i", strtotime($arrRowValues[23])) : $arrRowValues[23] ;
	
						$appointment_end_time = ( $objFile->origin == "tm" ) ? date("H:i", strtotime($arrRowValues[24])) : $arrRowValues[24] ;
						



								$objData = new stdClass();
								$objData->file_name = $objFile->file_name;
								$objData->agent_id = $objFile->agent_id;
								$objData->origin = $objFile->origin;
								$objData->service_order_id = $arrRowValues[0];
								$objData->service_delivery_type = $arrRowValues[1];
								$objData->number_delivery_men = $arrRowValues[2];
								$objData->client_account_name = $arrRowValues[3];
								$objData->destination_name = $arrRowValues[4];
								$objData->destination_address_1 = $arrRowValues[5];
								$objData->destination_address_2 = $arrRowValues[6];
								$objData->destination_city = $arrRowValues[7];
								$objData->destination_state = $arrRowValues[8];
								$objData->destination_zip = $arrRowValues[9];
								$objData->ship_name = $arrRowValues[10];
								$objData->ship_address_1 = $arrRowValues[11];
								$objData->ship_address_2 = $arrRowValues[12];
								$objData->ship_city = $arrRowValues[13];
								$objData->ship_state = $arrRowValues[14];
								$objData->ship_zip = $arrRowValues[15];
								$objData->customer_account = $arrRowValues[16];
								$objData->phone_1 = $arrRowValues[17];
								$objData->phone_2 = $arrRowValues[18];
								$objData->phone_3 = $arrRowValues[19];
								$objData->note_1 = $arrRowValues[20];
								$objData->note_2 = $arrRowValues[21];
								$objData->appointment_date = date( "Y-m-d", strtotime($arrRowValues[22]) );
								$objData->appointment_start_time = $appointment_start_time;
								$objData->appointment_end_time = $appointment_end_time;
								$objData->appointment_confirmed = $arrRowValues[25];
								$objData->pieces = $arrRowValues[26];
								
								$objData->weight = $arrRowValues[27];
								$objData->mileage = $arrRowValues[28];
								$objData->pre_delivery_charge = $arrRowValues[29];
								$objData->shipment_instructions = $arrRowValues[30];
								
								$objData->description = $arrRowValues[31];
								$objData->order_type = $arrRowValues[32];


								#ML1127	

								if ( $objFile->origin == "tm" and count($arrRowValues) == 34 )
								{
									$returnEmailValid = MetalakeHelperCore::validateEmail( $arrRowValues[33] );
									$objData->email = ( $returnEmailValid == "1" ) ? $arrRowValues[33] : "" ;
								}

								
								$objData->stamp_datetime = date("Y-m-d H:i:s");
								$objData->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			
								$result = $db->insertObject('htc_nsd_orderentry_data', $objData);								

						
						
						$countRecords += 1;					
					}
						
						
	
				}
	
				$objFile->stage = "data saved";
				$objFile->errorState = "0";
				$objFile->countRecords = $countRecords;
				    
		    }
		    else
		    {

				$objFile->stage = "file size zero";
				$objFile->errorState = "0";
				$objFile->countRecords = 0;
			    
		    }
		    



		}	
		else 
		{
		   #file does not exist
		    
		    $errMsg = "The upload file does not exist.";
		    
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r errMsg: ".print_r($errMsg, true);	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		    return $errMsg;
		    
		    $objFile->stage = "data saved - error";
		    $objFile->errorState = "1";
		    $objFile->errorMessage = $errMsg;

		}


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objFile;
	}




	public static function orderentry_cleaner( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_cleaner

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_cleaner
		
		# http://dev6.metalake.net/index.php?option=com_nsd_process&task=orderentry_cleaner
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=orderentry_cleaner

		

		$className = "Nsd_processController";
		$functionName = "orderentry_cleaner";
		
		$db = JFactory::getDBO();
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		# for each unprocessed record
		
		# check that previous record service order id is different, this means new clean record
		
		# clean phone number values and place in phone_1
		
		# put notes in notes table
		# put item in items table
		
		# if next record has the same service_order_id, add additional item to items table


		$query = "SELECT *, concat(appointment_date,' ',appointment_start_time) as appointment_start_time_clean, concat(appointment_date,' ',appointment_end_time) as appointment_end_time_clean from htc_nsd_orderentry_data WHERE processed_datetime = '0000-00-00 00:00:00' order by id";
		$db->setQuery($query);
		$objListRecords = $db->loadObjectList();
		
		$previous_service_order_id = "";


		$countRecords = 0;
		
		foreach( $objListRecords as $record )
		{
			
			if ( $previous_service_order_id != $record->service_order_id )
			{
				
				$phone_1_clean = Nsd_processController::orderentry_scrub_phone_data( $record->phone_1 );
				$phone_2_clean = Nsd_processController::orderentry_scrub_phone_data( $record->phone_2 );
				$phone_3_clean = Nsd_processController::orderentry_scrub_phone_data( $record->phone_3 );
				
				
				if ( $phone_1_clean != "" )
				{
					$phone_1 = $phone_1_clean;
					$phone_2 = $phone_2_clean;
					$phone_3 = $phone_3_clean;
					
				}
				else if ( $phone_1_clean == "" &&  $phone_2_clean != "" )
				{
					$phone_1 = $phone_2_clean;
					$phone_2 = $phone_2_clean;
					$phone_3 = $phone_3_clean;
				}				
				else if ( $phone_1_clean == "" &&  $phone_2_clean == "" &&  $phone_3_clean != "" )
				{
					$phone_1 = $phone_3_clean;
					$phone_2 = $phone_2_clean;
					$phone_3 = $phone_3_clean;					
				}
				else
				{
					$phone_1 = $phone_1_clean;
					$phone_2 = $phone_2_clean;
					$phone_3 = $phone_3_clean;					
				}
				
				
				switch( $record->agent_id )
				{
						
					case ( $record->agent_id > 0 ):
						
						$arrWhere = array();
						$arrWhere[] = "state = 1";
						$arrWhere[] = "agent_id = '".$record->agent_id."'";
						$arrWhere[] = "origin = '".$record->origin."'";
						$arrWhere[] = "code = '".$record->service_delivery_type."'";		
				
						$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;						
						
						$query = "SELECT * FROM htc_nsd_orderentry_service_levels ". $where ;
						$db->setQuery($query);
						$objServiceLevel = $db->loadObject();
						
						if ( is_object( $objServiceLevel ) && $objServiceLevel )
						{

							$service_delivery_type_clean = $objServiceLevel->service_type;

						}
						else
						{
							$service_delivery_type_clean = "Delivery";	
							
							#send email with order details
							$objAlert = new stdClass();
							$objAlert->recipients = array( 'support@metalake.com' );
							$objAlert->subject = "ALERT | Dispatchtrack Order Entry | Order does not have recognized Service Type";
							$body = "";
							$body .= "<br>The order below does not have a recognized Service Delivery Type:<br>";
							$body .= "<br>Service Order ID: ".$record->service_order_id;
							$body .= "<br>Service Delivery Type: ".$record->service_delivery_type;
							$body .= "<br>";
							$body .= "<br>Upload File: ".$record->file_name;
							$body .= "<br>File Date Time: ".$record->stamp_datetime;
							$objAlert->body = $body;
							#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );
				
							$logMessage = "INSIDE | ".$className." | ".$functionName." | Alert sent";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }							
							
						}
						
						
						break;
						
					default:
						$service_delivery_type_clean = "Delivery";
						break;
							
					
				}
				
				
				
				#https://stackoverflow.com/questions/13637145/split-text-string-into-first-and-last-name-in-php
				$destination_first_name_clean = $record->destination_name;
				$destination_last_name_clean = 	$record->destination_name;			
				
				$objData = new stdClass();
				$objData->data_id = $record->id;
				$objData->file_name = $record->file_name;
				$objData->agent_id = $record->agent_id;
				$objData->origin = $record->origin;
				$objData->service_order_id = $record->service_order_id;
				$objData->service_order_id_clean = $record->service_order_id;
				$objData->service_delivery_type = $record->service_delivery_type;
				$objData->service_delivery_type_clean = $service_delivery_type_clean;
				$objData->number_delivery_men = $record->number_delivery_men;
				$objData->client_account_name = $record->client_account_name;
				$objData->destination_name = $record->destination_name;
				$objData->destination_first_name_clean = $destination_first_name_clean;
				$objData->destination_last_name_clean = $destination_last_name_clean;
				$objData->destination_address_1 = $record->destination_address_1;
				$objData->destination_address_2 = $record->destination_address_2;
				$objData->destination_city = $record->destination_city;
				$objData->destination_state = $record->destination_state;
				$objData->destination_zip = $record->destination_zip;
				$objData->ship_name = $record->ship_name;
				$objData->ship_address_1 = $record->ship_address_1;
				$objData->ship_address_2 = $record->ship_address_2;
				$objData->ship_city = $record->ship_city;
				$objData->ship_state = $record->ship_state;
				$objData->ship_zip = $record->ship_zip;
				$objData->customer_account = $record->customer_account;
				$objData->phone_1 = $phone_1;
				$objData->phone_2 = $phone_2;
				$objData->phone_3 = $phone_3;
				$objData->phone_1_clean = $phone_1_clean;
				$objData->phone_2_clean = $phone_2_clean;
				$objData->phone_3_clean = $phone_3_clean;
				$objData->phone_1_data = $record->phone_1;
				$objData->phone_2_data = $record->phone_2;
				$objData->phone_3_data = $record->phone_3;
				$objData->appointment_date = $record->appointment_date;
				$objData->appointment_start_time = $record->appointment_start_time;
				$objData->appointment_end_time = $record->appointment_end_time;
				$objData->appointment_start_time_clean = $record->appointment_start_time_clean;
				$objData->appointment_end_time_clean = $record->appointment_end_time_clean;
				
				$objData->appointment_confirmed = $record->appointment_confirmed;
				$objData->order_type = ( $record->order_type == "DELIVER" ) ? "DELIVERY" : $record->order_type;
				$objData->order_status = "NEW";
				$objData->stamp_datetime = date("Y-m-d H:i:s");
				$objData->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");

				$objData->weight = $record->weight;
				$objData->pre_delivery_charge = $record->pre_delivery_charge;
				$objData->email = isset($record->email) ? $record->email : "";




				$objData->agent_id = ( $objData->agent_id == "2" && $objData->customer_account == "11597"  ) ? "3" : $objData->agent_id ;    //ISDL Amazon
				
				$objData->agent_id = ( $objData->agent_id == "11" && $objData->customer_account == "11597"  ) ? "20" : $objData->agent_id ;  //OTEX Amazon

				$objData->agent_id = ( $objData->agent_id == "5" && $objData->customer_account == "11597"  ) ? "29" : $objData->agent_id ;   //SHOWME Amazon

				$objData->agent_id = ( $objData->agent_id == "5" && $objData->customer_account == "11649"  ) ? "30" : $objData->agent_id ;   //SHOWME JCP

				$objData->agent_id = ( $objData->agent_id == "8" && $objData->customer_account == "11597"  ) ? "31" : $objData->agent_id ;   //ACEOC Amazon

				$objData->agent_id = ( $objData->agent_id == "26" && $objData->customer_account == "11597"  ) ? "33" : $objData->agent_id ;   //NORRIS Amazon

				$objData->agent_id = ( $objData->agent_id == "4" && $objData->customer_account == "11597"  ) ? "34" : $objData->agent_id ;   //GULO Amazon

				$objData->agent_id = ( $objData->agent_id == "35" && $objData->customer_account == "11597"  ) ? "36" : $objData->agent_id ;   //QEMCMH Amazon

				$objData->agent_id = ( $objData->agent_id == "37" && $objData->customer_account == "11597"  ) ? "39" : $objData->agent_id ;   //VTRANSK Amazon

				$objData->agent_id = ( $objData->agent_id == "41" && $objData->customer_account == "11597"  ) ? "42" : $objData->agent_id ;   //SHIP Amazon


				switch( $objData->agent_id )
				{
					case "2":
						#Island Nonstop
						$objData->service_order_id_clean = "NS".$record->service_order_id;
						break;

					case "3":
						#Island Amazon
						$objData->service_order_id_clean = "A".$record->service_order_id;
						break;

					case "20":
						#OTEX Amazon
						$objData->service_order_id_clean = "A".$record->service_order_id;
						break;

					case "42":
						#SHIP Amazon
						$objData->service_order_id_clean = $record->service_order_id."-AZN";
						break;

						
					default:
						$objData->service_order_id_clean = $record->service_order_id;
						break;					
				}



				$result = $db->insertObject('htc_nsd_orderentry_data_clean', $objData);				
				$cleanID = $db->insertid();
				
				if ( trim($record->note_1) != "" )
				{
					$objDataNotes = new stdClass();
					$objDataNotes->data_id = $record->id;
					$objDataNotes->service_order_id = $record->service_order_id;
					$objDataNotes->note = $record->note_1;
					$objDataNotes->author = "NSD";
					$objDataNotes->created_at = date("Y-m-d H:i:s");
					$result = $db->insertObject('htc_nsd_orderentry_data_clean_notes', $objDataNotes);	
				}
				
				if ( trim($record->note_2) != "" )
				{				
					$objDataNotes = new stdClass();
					$objDataNotes->data_id = $record->id;
					$objDataNotes->service_order_id = $record->service_order_id;
					$objDataNotes->note = $record->note_2;
					$objDataNotes->author = "NSD";
					$objDataNotes->created_at = date("Y-m-d H:i:s");
					$result = $db->insertObject('htc_nsd_orderentry_data_clean_notes', $objDataNotes);	
				}


				if ( trim($record->mileage) != "" )
				{				
					$objDataNotes = new stdClass();
					$objDataNotes->data_id = $record->id;
					$objDataNotes->service_order_id = $record->service_order_id;
					$objDataNotes->note = "Mileage: ".$record->mileage;
					$objDataNotes->author = "NSD";
					$objDataNotes->created_at = date("Y-m-d H:i:s");
					$result = $db->insertObject('htc_nsd_orderentry_data_clean_notes', $objDataNotes);	
				}

				if ( trim($record->shipment_instructions) != "" )
				{				
					$objDataNotes = new stdClass();
					$objDataNotes->data_id = $record->id;
					$objDataNotes->service_order_id = $record->service_order_id;
					$objDataNotes->note = "Shipment Instructions: ".$record->shipment_instructions;
					$objDataNotes->author = "NSD";
					$objDataNotes->created_at = date("Y-m-d H:i:s");
					$result = $db->insertObject('htc_nsd_orderentry_data_clean_notes', $objDataNotes);	
				}


				if ( $record->pieces > 0 )
				{
					$pre_delivery_charge = ( $record->pre_delivery_charge / $record->pieces );
				}
				else
				{
					$pre_delivery_charge = $record->pre_delivery_charge;
				}

				$objDataItems = new stdClass();
				$objDataItems->data_id = $record->id;
				$objDataItems->service_order_id = $record->service_order_id;
				$objDataItems->pieces = $record->pieces;
				$objDataItems->description = $record->description;
				$objDataItems->weight = $record->weight;
				$objDataItems->pre_delivery_charge = $pre_delivery_charge;
				$result = $db->insertObject('htc_nsd_orderentry_data_clean_items', $objDataItems);	
				
			}
			else
			{

				$objDataItems = new stdClass();
				$objDataItems->data_id = $record->id;
				$objDataItems->service_order_id = $record->service_order_id;
				$objDataItems->pieces = $record->pieces;
				$objDataItems->description = $record->description;
				$objDataItems->weight = $record->weight;
				$objDataItems->pre_delivery_charge = "";	
				$result = $db->insertObject('htc_nsd_orderentry_data_clean_items', $objDataItems);					

			}			

            $objDataUpdate = new stdClass();
            $objDataUpdate->id = $record->id;
            $objDataUpdate->processed_datetime = date("Y-m-d H:i:s");
            $objDataUpdate->processed_datetime_gmt = gmdate("Y-m-d H:i:s");
            $result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data', $objDataUpdate, 'id');     				

			
			$previous_service_order_id = $record->service_order_id;
			
			$countRecords += 1;
		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | totalRecords: ".$countRecords." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "END | ".$className." | ".$functionName." | totalRecords: ".$countRecords." | seconds: ".number_format($stamp_total_micro,2);			
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$return = 1;
		return $return;
	}



	public static function orderentry_scrub_phone_data( $input )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_scrub_phone_data

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_scrub_phone_data
		
		# http://dev6.metalake.net/index.php?option=com_nsd_process&task=orderentry_scrub_phone_data
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=orderentry_scrub_phone_data		


		$className = "Nsd_processController";
		$functionName = "orderentry_scrub_phone_data";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$step = preg_replace("/[^0-9]/", "", $input );
		
		$step = ( strlen( $step ) < 10 ) ? "" : $step ;

		$step = ( preg_match('/^(.)\1*$/', $step) ) ? "" : $step ;

		$step = substr($step, -10);

		$return = $step;
		
		
		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r input: ".print_r($input, true);
		$logMessage .= "\n\r return: ".print_r($return, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $return;
	}



	public function cron_process_orderentry_sender( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_process&task=cron_process_orderentry_sender

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_process&task=cron_process_orderentry_sender
		
		# http://nsddev.metalake.net/index.php?option=com_nsd_process&task=cron_process_orderentry_sender
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=cron_process_orderentry_sender		
		
		

		$className = "Nsd_processController";
		$functionName = "cron_process_orderentry_sender";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "SELECT * from htc_nsd_orderentry_log_cron order by id desc limit 1";
		$db->setQuery($query);
		$objLogCron = $db->loadObject();

		$query = "SELECT count(c.id) from htc_nsd_orderentry_data_clean c LEFT JOIN htc_dispatchtrack_agents a ON c.agent_id = a.id WHERE a.state=1 AND c.processed_datetime = '0000-00-00 00:00:00' order by c.id";
		$db->setQuery($query);
		$countNumberOfRecords = $db->loadresult();



		if ( $objLogCron->state == "1" )
		{
			$logMessage = "INSIDE | ".$className." | ".$functionName." | cronOrderEntrySender is already running. The process started at ".$objLogCron->start_datetime;	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		
			
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r Alert sent";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			#send email with order details
			$objAlert = new stdClass();
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Dispatchtrack Order Entry | cronOrderEntrySender is already running. The process started at ".$objLogCron->start_datetime;
			$body = "";
			$body .= "<br>cronOrderEntrySender is already running. The process started at ".$objLogCron->start_datetime."<br>";
			$body .= "<br>text 'resetsender' to (703) 996-4322 to unlock the process.<br>";
			$objAlert->body = $body;
			#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );


			$objAlert = new stdClass();
			$objAlert->recipients = array( '7039305383@txt.att.net' );
			$objAlert->subject = "NSD MONITOR | ".$functionName;
			$objAlert->body = $body;
			#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );



			$logMessage = "INSIDE | ".$className." | ".$functionName." | Alert sent";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
			
					
		}
		else
		{
	
			if ( $countNumberOfRecords > 0 )
			{
	
				$logMessage = "INSIDE | ".$className." | ".$functionName." | Number of records to process: ".$countNumberOfRecords;	
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
				$local_time = date("Y-m-d H:i:s");							
				$gmt_time = gmdate("Y-m-d H:i:s");
		
		        $objData = new stdClass();
		        $objData->state = 1;
				$objData->start_datetime_gmt = $gmt_time;
				$objData->start_datetime = $local_time;
				$objData->num_outstanding = $countNumberOfRecords;
		        $result = JFactory::getDbo()->insertObject('htc_nsd_orderentry_log_cron', $objData);
		        $cronID = $db->insertid();
		
		
				$numberOfRecords = 30;
				
				$delayInSeconds = 1;
				
				$return = 1;
				$count = 0;
				
				
				while ( $return && ( $count <  $numberOfRecords) )
				{
					$return = Nsd_processController::orderentry_sender();
					
					$count += 1;
		
					sleep( $delayInSeconds );
				}
		
		
				$local_time = date("Y-m-d H:i:s");							
				$gmt_time = gmdate("Y-m-d H:i:s");		
		
				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		
		
		        #update htc_ml_dt_documents
		        $objData = new stdClass();
		        $objData->id = $cronID;
		        $objData->state = 0;
				$objData->end_datetime_gmt = $gmt_time;
				$objData->end_datetime = $local_time;
				$objData->num_processed = ($count - 1);        
		        $objData->total_time = number_format($stamp_total_micro,2);
		        $result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_log_cron', $objData, 'id'); 
	        
	        }
			else
			{
				$logMessage = "INSIDE | ".$className." | ".$functionName." | No records to process.";	
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
			}

		}

		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);		

		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public static function orderentry_sender( )  
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_orderentry&task=orderentry_sender

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_orderentry&task=orderentry_sender
		
		# http://nsddev.metalake.net/index.php?option=com_nsd_process&task=orderentry_sender
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=orderentry_sender		
		

		$className = "Nsd_processController";
		$functionName = "orderentry_sender";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


				$query = "SELECT c.*, a.agent_code, a.description, a.dispatchtrack_api_code, a.dispatchtrack_api_key, a.dispatchtrack_account, a.state from htc_nsd_orderentry_data_clean c LEFT JOIN htc_dispatchtrack_agents a ON c.agent_id = a.id WHERE a.state=1 AND c.processed_datetime = '0000-00-00 00:00:00' order by c.id limit 1";
				$db->setQuery($query);
				$objRecord = $db->loadObject();

					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objRecord: ".print_r($objRecord, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				if ( count($objRecord) > 0 )
				{


					$objAgent = MetalakeHelperCore::getObjTableData( $table="htc_dispatchtrack_agents", $field="id", $value=$objRecord->agent_id );


					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		
					$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] | seconds: ".number_format($stamp_total_micro,2);
					$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					
					
					$xmlBody = new SimpleXMLExtended('<?xml version="1.0" encoding="utf-8"?><service_order></service_order>');
					
						$account_val = ( $objAgent->id == "1"  ) ? htmlspecialchars($objRecord->client_account_name) : $objAgent->dispatchtrack_account ; 

						$xmlBody->addChild('number', $objRecord->service_order_id_clean);
						$xmlBody->addChild('account', $account_val);
						$xmlBody->addChild('service_type', $objRecord->service_delivery_type_clean);
						#$xmlBody->addChildWithCDATA('description', '');
	
	
						$customer = $xmlBody->addChild('customer');
	
						#$customerInfo = $customer->addChild('customer_id');
						#$customerInfo = $customer->addChildWithCDATA('first_name', $objRecord->destination_name);
						#$customerInfo = $customer->addChild('first_name', "");
						$customerInfo = $customer->addChildWithCDATA('last_name', $objRecord->destination_name);
						$customerInfo = $customer->addChild('email', $objRecord->email);
						$customerInfo = $customer->addChild('phone1', $objRecord->phone_1);
						$customerInfo = $customer->addChild('phone2', $objRecord->phone_2);
						$customerInfo = $customer->addChild('phone3', $objRecord->phone_3);
						$customerInfo = $customer->addChild('address1', $objRecord->destination_address_1);		
						$customerInfo = $customer->addChild('address2', $objRecord->destination_address_2);		
						$customerInfo = $customer->addChild('city', $objRecord->destination_city);		
						$customerInfo = $customer->addChild('state', $objRecord->destination_state);		
						$customerInfo = $customer->addChild('zip', $objRecord->destination_zip);		
						#$customerInfo = $customer->addChild('latitude', '');		
						#$customerInfo = $customer->addChild('longitude', '');		

						$queryN = "SELECT * FROM htc_nsd_orderentry_data_clean_notes WHERE service_order_id = '".$objRecord->service_order_id."' order by id";
						
						$db->setQuery($queryN);
						$objListNotes = $db->loadObjectList();
						
						$countNotes = count($objListNotes);

						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
			
						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] | seconds: ".number_format($stamp_total_micro,2);
						$logMessage .= "\n\r queryN: ".print_r($queryN, true);
						$logMessage .= "\n\r objListNotes: ".print_r($objListNotes, true);
						$logMessage .= "\n\r countNotes: ".print_r($countNotes, true);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



						$notes = $xmlBody->addChild('notes');
						
						$notes->addAttribute('count', $countNotes);

						foreach( $objListNotes as $objNote )
						{
							$note = $notes->addChildWithCDATA('note',$objNote->note );
							$note->addAttribute('created_at', $objNote->created_at);
							$note->addAttribute('author', $objNote->author);
							$note->addAttribute('note_type', 'Public');

						}
	
// 						$queryN = "SELECT service_order_id, description, pieces, weight, pre_delivery_charge FROM htc_nsd_orderentry_data_clean_items WHERE service_order_id = '".$objRecord->service_order_id."' and data_id = '".$objRecord->data_id."' order by id";

						$queryN = "SELECT service_order_id, description, pieces, weight, pre_delivery_charge FROM htc_nsd_orderentry_data_clean_items WHERE service_order_id = '".$objRecord->service_order_id."' order by id";
						
						$db->setQuery($queryN);
						$objListItems = $db->loadObjectList();


						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] | seconds: ".number_format($stamp_total_micro,2);
						$logMessage .= "\n\r queryN: ".print_r($queryN, true);
						$logMessage .= "\n\r objListItems: ".print_r($objListItems, true);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


	
						$items = $xmlBody->addChild('items');

						foreach( $objListItems as $objItem )
						{
							$Item = $items->addChild('item');
							#$Item->addChild('sale_sequence', '1' );  //required
							$Item->addChild('item_id', '1' ); //required
							#$Item->addChild('serial_number', $objRecord->service_order_id ); //required
							$Item->addChildWithCDATA('description', $objItem->description );
							$Item->addChild('quantity', $objItem->pieces );
							#$Item->addChild('location', '1' ); //required
							$Item->addChild('cube', '100' );
							#$Item->addChild('setup_time', '' );
							$Item->addChild('weight', $objItem->weight );
							#$Item->addChild('amount', $objItem->pre_delivery_charge );
							$Item->addChild('price', $objItem->pre_delivery_charge );
							#$Item->addChild('countable', '' );
							#$Item->addChild('store_code', '' );

						}




						
						if ( $objRecord->appointment_confirmed == "CONFIRMED" )
						{
							$request_delivery_date_val = $objRecord->appointment_date;
							$request_time_window_start_val = $objRecord->appointment_start_time_clean;
							$request_time_window_end_val = $objRecord->appointment_end_time_clean;							
						}
						else
						{
							$request_delivery_date_val = "";
							$request_time_window_start_val = "";
							$request_time_window_end_val = "";							
						}

					
						
						#production
						#$xmlBody->addChild('pre_reqs', '');
						#$xmlBody->addChild('amount', '');
						#$xmlBody->addChild('cod_amount', '');
						#$xmlBody->addChild('service_unit', '');
						#$xmlBody->addChild('delivery_date', '');
						$xmlBody->addChild('request_delivery_date', $request_delivery_date_val);
						#$xmlBody->addChild('ready_to_schedule_date', '');
						#$xmlBody->addChild('schedule_before_date', '');
						#$xmlBody->addChild('status', $objRecord->order_status); // NEW
						#$xmlBody->addChild('status', 'Scheduled'); // NEW
						#$xmlBody->addChild('client_code', '');
						#$xmlBody->addChild('client_reference_number', '');
						#$xmlBody->addChild('client_tracking_number', '');
						#$xmlBody->addChild('driver_id', '');
						#$xmlBody->addChild('truck_id', '');
						#$xmlBody->addChild('origin', '');
						#$xmlBody->addChild('stop_number', '');
						#$xmlBody->addChild('stop_time', '');
						#$xmlBody->addChild('service_time', '');
						$xmlBody->addChild('request_time_window_start', $request_time_window_start_val);
						$xmlBody->addChild('request_time_window_end', $request_time_window_end_val);
						#$xmlBody->addChild('delivery_time_window_start', '');
						#$xmlBody->addChild('delivery_time_window_end', '');
						#$xmlBody->addChild('delivery_charge', '');
						#$xmlBody->addChild('taxes', '');
						#$xmlBody->addChild('store_code', '');						
						
						
						$queryEF = "SELECT distinct agent_id, field_name, data_field FROM htc_nsd_orderentry_extra_fields WHERE state = '1' AND  agent_id = '".$objAgent->id."' AND field_type = 'extra' order by id";
						$db->setQuery($queryEF);
						$objListExtraFields = $db->loadObjectList();						


						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] | seconds: ".number_format($stamp_total_micro,2);
						$logMessage .= "\n\r queryEF: ".print_r($queryEF, true);
						$logMessage .= "\n\r objListExtraFields: ".print_r($objListExtraFields, true);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

						
						$extra = $xmlBody->addChild('extra');

						foreach( $objListExtraFields as $objEF )
						{
							$data_field = (string) $objEF->data_field;
							
							$extra->addChild($objEF->field_name, $objRecord->$data_field);

						}						
						
						

						$queryCus = "SELECT distinct agent_id, field_name, data_field FROM htc_nsd_orderentry_extra_fields WHERE state = '1' AND agent_id = '".$objAgent->id."' AND field_type = 'custom' order by id";
						$db->setQuery($queryCus);
						$objListExtraCustom = $db->loadObjectList();						


						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] | seconds: ".number_format($stamp_total_micro,2);
						$logMessage .= "\n\r queryCus: ".print_r($queryCus, true);
						$logMessage .= "\n\r objListExtraCustom: ".print_r($objListExtraCustom, true);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


						
						$custom_fields = $xmlBody->addChild('custom_fields');


						foreach( $objListExtraCustom as $objCus )
						{
							$data_field = (string) $objCus->data_field;
							
							$custom_fields->addChild($objCus->field_name, $objRecord->$data_field);

						}	

					$dom = new DOMDocument("1.0");
					$dom->preserveWhiteSpace = FALSE;
					$dom->loadXML($xmlBody->asXML());
					$dom->formatOutput = TRUE;
					echo "<pre>".htmlentities($dom->saveXML())."</pre>";


					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		
					$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] | seconds: ".number_format($stamp_total_micro,2);
					$logMessage .= "\n\r xmlBody: ".print_r($xmlBody, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r xmlBody: ".print_r($xmlBody, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$urltopost = "https://".$objAgent->dispatchtrack_api_code.".dispatchtrack.com/orders/api/add_order";

					$domXML = $dom->saveXML();


					$datatopost	= array (
					  "code"	=> $objAgent->dispatchtrack_api_code,
					  "api_key"	=> $objAgent->dispatchtrack_api_key,
					  "data"	=> $domXML
					  );		

					if ( $objAgent->dispatchtrack_account != "" )
					{
						$datatopost["account"] = $objAgent->dispatchtrack_account;
					}

				
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r datatopost: ".print_r($datatopost, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$ch = curl_init ($urltopost);
					
					
					curl_setopt ($ch, CURLOPT_POST, true);
					curl_setopt ($ch, CURLOPT_POSTFIELDS, $datatopost);
					curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
					#curl_setopt ($ch, CURLOPT_FILE, $fp);
					
					$returnData = curl_exec ($ch);
				
					$error = curl_error($ch);
				
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r returnData: ".print_r($returnData, true);
					$logMessage .= "\n\r error: ".print_r($error, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					if ( substr($returnData, 0, 5) == "<?xml" )
					{
				
						$xml = simplexml_load_string( $returnData );
						
						$strReturn = (string)$xml;
						
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r returnData: ".print_r($returnData, true);
						$logMessage .= "\n\r xml: ".print_r($xml, true);
						$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				
						switch( $strReturn  )
						{
							
							case "Imported given orders!":

								$local_time = date("Y-m-d H:i:s");							
								$gmt_time = gmdate("Y-m-d H:i:s");
								
				                $objData = new stdClass();
				                $objData->id = $objRecord->id;
				                $objData->processed_datetime = $local_time;
				                $objData->processed_datetime_gmt = $gmt_time;
				                $objData->transfer_datetime = $local_time;
				                $objData->transfer_datetime_gmt = $gmt_time;
				                $objData->flag_error = 0;
				                $objData->process_note = $strReturn;				                
	
				                $result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id');    
					
					
								$stamp_end_micro = microtime(true);
								$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
					
								$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] Processed and Sent: ".$objRecord->service_order_id." | seconds: ".number_format($stamp_total_micro,2);
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

								
								
								#ML0424
								# INSERT RECORD FOR NEW ORDER INTO NSDDT PROCESS
								
								if ( substr($objRecord->service_order_id, 0, 1) == "R" || substr($objRecord->service_order_id, -1) == "R" )
								{

									$objData = new stdClass();
									$objData->agent_name = $objAgent->name;
									$objData->freight_bill = $objRecord->service_order_id_clean;
									$objData->freight_bill_export = $objRecord->service_order_id_clean;
									$objData->change_datetime = date("Y-m-d H:i:s");
									$objData->timezone = "ET";
									$objData->customer_city = $objRecord->destination_city;
									$objData->customer_state = $objRecord->destination_state;
									$objData->status_code = "NS";
									$objData->reason_code = "NS";
									$objData->truckmate_status = "XB";
									$objData->comment = "Shipment Acknowledged";
									$objData->stamp_datetime = date("Y-m-d H:i:s");
									$objData->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");	
									$objData->importfile = "";
									$objData->flag_processed = "0";		
									$objData->transfer_file = "";
									$objData->order_status = "New"; 
									$objData->agent_id = $objAgent->id;
	
									$objData->scheduled_at = "";
									$objData->scheduled_at_end = "";
									$objData->time_window_start = $objRecord->appointment_start_time_clean;
									$objData->time_window_end = $objRecord->appointment_end_time_clean;
									$objData->started_at = "";
									$objData->finished_at = "";
									$objData->custom_field_pod_name = "NOVALUE";
									$objData->request_window_start_time = "";
									$objData->request_window_end_time = "";
	
														
									#$result = $db->insertObject('htc_nsd_orderupdate_data_truckmate', $objData);
	
	
									$logMessage = "INSIDE | ".$className." | ".$functionName;
									$logMessage .= "\n\r objData: ".print_r($objData, true);
									$logMessage .= "\n\r ";
									#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
									
								}
								
								

								
								
								
								
								
								break;






							case "HTTP 500: Internal Server Error":

								$local_time = date("Y-m-d H:i:s");							
								$gmt_time = gmdate("Y-m-d H:i:s");
	
				                $objData = new stdClass();
				                $objData->id = $objRecord->id;
				                $objData->processed_datetime = $local_time;
				                $objData->processed_datetime_gmt = $gmt_time;
				                $objData->transfer_datetime = "0000-00-00 00:00:00";
				                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
				                $objData->flag_error = 1;
				                $objData->process_note = $strReturn;				                
	
				                $result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 

								$stamp_end_micro = microtime(true);
								$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
								$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r Alert sent";
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


								#send email with order details
								$objAlert = new stdClass();
								#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
								$objAlert->recipients = array( 'lsawyer@metalake.com' );
								$objAlert->subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
								$body = "";
								$body .= "<br>The order below failed to import into Dispatchtrack during the order entry process.";
								$body .= "<br><br>";
								$body .= "<br><br>This order will have to be entered into Dispatchtrack manually.";
								$body .= "<br><br>";
								$body .= "<pre>".htmlentities($domXML)."<pre>";
								$body .= "<br><br>";
								$body .= "<br>Error Message: ".$strReturn."<br>";
								$body .= "<br><br>The 500 Internal Server Error means that there may be information that is missing or malformed.  Dispatchtrack does not provide any additional error messaging when this occurs.";
													
								$objAlert->body = $body;
								#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );
			
								$logMessage = "INSIDE | ".$className." | ".$functionName." | Alert sent";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

	



								break;
							
							
							case "Another import is already in progress, please try after some time":

								$local_time = date("Y-m-d H:i:s");							
								$gmt_time = gmdate("Y-m-d H:i:s");
	
				                $objData = new stdClass();
				                $objData->id = $objRecord->id;
				                $objData->processed_datetime = "0000-00-00 00:00:00";
				                $objData->processed_datetime_gmt = "0000-00-00 00:00:00";
				                $objData->transfer_datetime = "0000-00-00 00:00:00";
				                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
				                $objData->flag_error = 1;
				                $objData->process_note = $strReturn;				                
	
				                $result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 
	
								$stamp_end_micro = microtime(true);
								$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
								$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

								break;							



							default:

								$local_time = date("Y-m-d H:i:s");							
								$gmt_time = gmdate("Y-m-d H:i:s");
	
				                $objData = new stdClass();
				                $objData->id = $objRecord->id;
				                $objData->processed_datetime = $local_time;
				                $objData->processed_datetime_gmt = $gmt_time;
				                $objData->transfer_datetime = "0000-00-00 00:00:00";
				                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
				                $objData->flag_error = 1;
				                $objData->process_note = $strReturn;				                
	
				                $result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 

								$stamp_end_micro = microtime(true);
								$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

								$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Record [".$objRecord->id."] Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
							
								#sent alert to admin

								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r Alert sent";
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



								#send email with order details
								$objAlert = new stdClass();
								#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
								$objAlert->recipients = array( 'lsawyer@metalake.com' );
								$objAlert->subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
								$body = "";
								$body .= "<br>Error Message: ".$strReturn."<br>";
								$body .= "<br><br>The following order did not get imported - error:<br>";
								$body .= "<br><br>";
								$body .= "<br><br>";
								$body .= "<pre>".htmlentities($domXML)."<pre>";
								$body .= "<br><br>";
																
								$objAlert->body = $body;
								#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );
			
								$logMessage = "INSIDE | ".$className." | ".$functionName." | Alert sent";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	




							
							
								break;
							
								

						}
				

				
				
					}
					else
					{
						
				
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r 500 error?: ".print_r($returnData, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | 500 ERROR | Number of records sent: 0 | Return data message: ".$returnData;
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
						
					}


					$return = 1;
					
				}
				else
				{

					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

					$logMessage = "INSIDE | ".$className." | ".$functionName." |  No record to send. | seconds: ".number_format($stamp_total_micro,2);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
					
					$return = 0;
					
				}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $return;
	}	



	public static function orderentry_prune_directory( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_prune_directory
		
		# http://process.nonstopdelivery.com/index.php?option=com_nsd_process&task=orderentry_prune_directory
		
		# http://dev6.metalake.net/index.php?option=com_nsd_process&task=orderentry_prune_directory
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=orderentry_prune_directory		
		
		$className = "Nsd_processController";
		$functionName = "orderentry_prune_directory";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderentry_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderentry_prod";
	
		}	

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_start_micro = microtime(true);
		
		$logPath = JPATH_SITE."/nsdorderentry/PROCESSED/";
		
		$command = "find ".$logPath."*.csv -mmin +360 -type f -exec rm -f {} + 2>&1" ;  //60 minutes = 1 hours

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r command: ".print_r($command, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
		exec( $command, $output, $return_var );		
		

		$logPath = JPATH_SITE."/nsdorderentry/SKIPPED/";
		
		$command = "find ".$logPath."*.csv -mmin +360 -type f -exec rm -f {} + 2>&1" ;  //60 minutes = 1 hours

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r command: ".print_r($command, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
		exec( $command, $output, $return_var );	
		


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}





// !ORDER UPDATE




	public function cron_process_orderupdate( )  
	{
		# old function name: cronDispatchtrackProcessMulti()

		# http://nsddev.metalake.net/index.php?option=com_nsd_process&task=cron_process_orderupdate
		
		# http://tracking.shipnsd.com/index.php?option=com_nsd_process&task=cron_process_orderupdate


		$className = "Nsd_processController";
		$functionName = "cron_process_orderupdate";
		
		$db = JFactory::getDBO();
		
        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
		$cron_xml_dispatchtrack	= $params->get('cron_xml_dispatchtrack');
		$cron_process_xml		= $params->get('cron_process_xml');
		$cron_process_tmw_rules	= $params->get('cron_process_tmw_rules');
		$cron_send_tmw_files	= $params->get('cron_send_tmw_files');		
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_start_micro = microtime(true);

		if (  $cron_xml_dispatchtrack == "1"  )
		{
	
			$query = "SELECT * FROM htc_dispatchtrack_agents WHERE state = '1' and flag_ignore_import = '0' ";
			$db->setQuery($query);
			$objListAgents = $db->loadObjectList();
	
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);				
			$logMessage = "INSIDE | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);		
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objListAgents: ".print_r($objListAgents, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
			
			$mh = curl_multi_init(); // init the curl Multi
			
			
			foreach( $objListAgents as $objAgent )
			{
				
				$objAgent->processid = $objAgent->id.".".date("YmdHis").".".time();		
				$objAgent->markedProcessed = "0";	
				
	
				$logTable = "htc_nsd_orderupdate_log_master";
				$logObj = new stdClass();
				$logObj->datetime = date("Y-m-d H:i:s");
				$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
				$logObj->agentid = $objAgent->id;
				$logObj->notes = "Start cron for agent ".$objAgent->agent_code;
				$logObj->function_name = $functionName;
				$logObj->log_level = "";
				   
				MetalakeHelperCore::logOBJ( $logTable, $logObj );
	
				
				$urltopost = JURI::root()."index.php?option=com_nsd_process&task=orderupdate_get_dispatchtrack_information";
				
				$agentid = $objAgent->id;
				$markedProcessed = $objAgent->markedProcessed;
				$processid = $objAgent->processid;
				
				$datatopost	= array (
				  "agentid"	=> $agentid,
				  "markedProcessed"	=> $markedProcessed,
				  "processid"	=> $processid,
				  
				  );		



				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);		
				$logMessage = "INSIDE | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);		
				$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
				$logMessage .= "\n\r datatopost: ".print_r($datatopost, true);
				$logMessage .= "\n\r ";		
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		
				$ch = curl_init ($urltopost);
				curl_setopt ($ch, CURLOPT_POST, true);
				curl_setopt ($ch, CURLOPT_POSTFIELDS, $datatopost);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);		
		
		        $aCurlHandles[$agentid] = $ch;
		        
		        curl_multi_add_handle($mh,$ch);		
				
			}
			
		    $active = null;
		    //execute the handles
		    
		    do 
		    {
		        $mrc = curl_multi_exec($mh, $active);
		    } 
		    while ($active);
	
			
		    foreach ($aCurlHandles as $agentid=>$ch) 
		    {
		    
		        $xmlcontent = curl_multi_getcontent($ch); // get the content
	
		        curl_multi_remove_handle($mh, $ch); // remove the handle (assuming  you are done with it);
		    }		
			
			
			 curl_multi_close($mh); // close the curl multi handler			
			
		}
		else
		{

			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = $objAgent->id;
			$logObj->notes = "ALERT: Cron to get XML information from Dispatchtrack is OFF";
			$logObj->function_name = $functionName;   
			$logObj->log_level = "1";
			MetalakeHelperCore::logOBJ( $logTable, $logObj );	
			
			$objAlert = new stdClass();
			$objAlert->recipients = array( 'support@metalake.com' );
			$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
			$body = "";
			$body .= "<br> ALERT: Cron to get XML information from Dispatchtrack is OFF";
			$body .= "<br><br>";
			$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
			$body .= "<br><br>";			
			$body .= "<br> Steps to triage:";
			$body .= "<br><br>";
			$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
			$body .= "<br> Go to configuration of the API component.";
			$body .= "<br> Turn on cron for: 'Cron to get XML information from Dispatchtrack'";
			$body .= "<br><br>";
			$body .= "<br> Check that master cron for this function is running on server.";
			$body .= "<br> ";
			$objAlert->body = $body;
			#$resultAlert = Nsd_processController::sendAlert( $objAlert );

			$objAlert = new stdClass();
			$objAlert->recipients = array( '7039305383@txt.att.net' );
			$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
			$objAlert->body = "ALERT: Cron to get XML information from Dispatchtrack is OFF. See email for details.";
			#$resultAlert = Nsd_processController::sendAlert( $objAlert );
			
			$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
							
			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = $objAgent->id;
			$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
			$logObj->function_name = $functionName;  
			$logObj->log_level = "1"; 
			MetalakeHelperCore::logOBJ( $logTable, $logObj );

		}




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		$logMessage .= "\n\r ";
		$logMessage .= "\n\r ";
		$logMessage .= "\n\r ";		
		$logMessage .= "\n\r ";
		$logMessage .= "\n\r ";		
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
	}



	public static function orderupdate_get_dispatchtrack_information( )  
	{
		# old function name: getDispatchtrackInfoMultiVars( )


		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=getDispatchtrackInfoMultiVars
		
		
		#http://us1.php.net/manual/en/function.curl-multi-init.php
		#http://php.net/manual/en/function.curl-multi-getcontent.php
		
		$className = "Nsd_processController";
		$functionName = "orderupdate_get_dispatchtrack_information";
		
		$db = JFactory::getDBO();

		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}

		

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_start_micro = microtime(true);
		
		$objVars = new stdClass();
		$objVars->agentid = strtoupper(JRequest::getVar('agentid'));
		$objVars->markedProcessed = strtoupper(JRequest::getVar('markedProcessed'));
		$objVars->processid = strtoupper(JRequest::getVar('processid'));
		
	
		$objAgent = MetalakeHelperCore::getObjTableData( $table="htc_dispatchtrack_agents", $column="id", $value=$objVars->agentid );		
		$objAgent->markedProcessed = $objVars->markedProcessed;
		$objAgent->processid = $objVars->processid;






		
		#$urltopost = "https://nonstopdelivery.dispatchtrack.com/orders/api/export.xml";

		$urltopost = "https://".$objAgent->dispatchtrack_api_code.".dispatchtrack.com/orders/api/export.xml";


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);		
		$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->dispatchtrack_code." | seconds: ".number_format($stamp_total_micro,2);		
		$logMessage .= "\n\r objVars: ".print_r($objVars, true);
		$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
		$logMessage .= "\n\r ";		
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		if ( $objAgent->markedProcessed == "1" )
		{
			$arrDateFrame = array('0');
		}
		else
		{
			
			#ML0424
			
			#production
			
			switch ( date('H') )
			{
				
				case "03":
				
				
					switch( date('i') )
					{
						
						case "00";
							$arrDateFrame = array('-1','-2','-3','-4','-5');
						
						break;

						
						case "10";
							$arrDateFrame = array('-6','-7','-8','-9','-10');					
						break;


						case "20";
							$arrDateFrame = array('-11','-12','-13','-14','-15');					
						break;


						case "30";
							$arrDateFrame = array('-16','-17','-18','-19','-20');					
						break;


						case "40";
							$arrDateFrame = array('-21','-22','-23','-24','-25');					
						break;
						
						
						case "50";
							$arrDateFrame = array('-26','-27','-28','-29','-30');					
						break;						
						
						default:
							
						break;
						
					}
				
				break;
				
				default:
				
			
					switch( $objAgent->id )
					{
						case "23":  //BIG
						case "28":  //CTE
						$arrDateFrame = array('-1','0','1','2','3','4','5','6','7');
						break;
						
						default:
						$arrDateFrame = array('-1','0','1','2','3');				
						break;
					}
				
				
				break;

				
			}




			
			#development
			#$arrDateFrame = array('-1','0','1','2');
			#$arrDateFrame = array('-1','0');
			#$arrDateFrame = array('0','1','2','3');
			$arrDateFrame = array('0');
			#$arrDateFrame = array('0','1');
		}

		$mh = curl_multi_init(); // init the curl Multi
		
		foreach( $arrDateFrame as $frame )
		{
				$datetoday = date("Y-m-d", strtotime( $frame.' days' ) );

				#$datetoday = date("Y-m-d");	
				#$datetoday = "2017-09-27";
		
				$datatopost	= array (
				  "code"	=> $objAgent->dispatchtrack_api_code,
				  "api_key"	=> $objAgent->dispatchtrack_api_key,
				  "schedule_date"	=> $datetoday,    		//schedule_date in yyyy-mm-dd format
				  );		
				
				if ( $objAgent->account != "" )
				{
					$datatopost["account"] = $objAgent->dispatchtrack_account;
				}		


				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);		
				$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->dispatchtrack_code." | seconds: ".number_format($stamp_total_micro,2);		
				$logMessage .= "\n\r urltopost: ".print_r($urltopost, true);
				$logMessage .= "\n\r frame: ".print_r($frame, true);
				$logMessage .= "\n\r datatopost: ".print_r($datatopost, true);
				$logMessage .= "\n\r ";		
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
		
				$ch = curl_init ($urltopost);
				curl_setopt ($ch, CURLOPT_POST, true);
				curl_setopt ($ch, CURLOPT_POSTFIELDS, $datatopost);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);		
		
		        $aCurlHandles[$datetoday] = $ch;
		        
		        curl_multi_add_handle($mh,$ch);		
				
		}
		
		
		
	    $active = null;
	    //execute the handles
	    
	    do 
	    {
	        $mrc = curl_multi_exec($mh, $active);
	    } 
	    while ($active);
	
		
	    foreach ($aCurlHandles as $datetoday=>$ch) 
	    {
	    
	        $xmlcontent = curl_multi_getcontent($ch); // get the content

			$file_name = $objAgent->dispatchtrack_api_code ."_". $objAgent->processid . "_" . date("YmdHis"). "_" . $datetoday . ".xml";
			
			$file_directory = "nsdorderupdate";
			$file_directory_agent = $objAgent->dispatchtrack_api_code;
			
			
			$fullpath = JPATH_SITE."/".$file_directory."/".$file_directory_agent;
			if ( !JFolder::exists( $fullpath ) )
			{	
				$result = JFolder::create( $fullpath, 0777 );
			}				
			
			
			$file_fullpath = $file_directory."/".$file_directory_agent."/".$file_name;

			$fp = fopen($file_fullpath, 'w');
			
			fwrite( $fp, $xmlcontent );
		
			fclose($fp);

			if ( $objAgent->markedProcessed == "1" )
			{
	
					$logTable = "htc_nsd_orderupdate_xmlfiles";
					$logObj = new stdClass();
					$logObj->agentid = $objAgent->id;
					$logObj->processid = $objAgent->processid;
					$logObj->created_datetime_gmt = gmdate("Y-m-d H:i:s");
					$logObj->created_datetime = date("Y-m-d H:i:s");
	
					$logObj->processed_datetime_gmt = gmdate("Y-m-d H:i:s");
					$logObj->processed_datetime = date("Y-m-d H:i:s");
					$logObj->flag_processed = "1";
	
					$logObj->file_directory = $file_directory;
					$logObj->file_directory_agent = $file_directory_agent;
					$logObj->file_name = $file_name;
					$logObj->file_fullpath = $file_fullpath;
					$logObj->datadate = $datetoday;
					MetalakeHelperCore::logOBJ( $logTable, $logObj );

					$logTable = "htc_nsd_orderupdate_log_master";
					$logObj = new stdClass();
					$logObj->datetime = date("Y-m-d H:i:s");
					$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
					$logObj->agentid = $objAgent->id;
					$logObj->notes = "File information for ".$file_name." inserted into htc_nsd_orderupdate_xmlfiles";
					$logObj->function_name = $functionName;
					$logObj->log_level = "";   
					#MetalakeHelperCore::logOBJ( $logTable, $logObj );
					#evaluate if needed
	
					$objReturn = new stdClass();
					$objReturn->getDispatchtrackInfo = 1;
					$objReturn->file_directory = $file_directory;
					$objReturn->file_directory_agent = $file_directory_agent;
					$objReturn->file_name = $file_name;
					$objReturn->file_fullpath = $file_fullpath;
					$objReturn->processed_datetime_gmt = gmdate("Y-m-d H:i:s");
					$objReturn->processed_datetime = date("Y-m-d H:i:s");
					$objReturn->flag_processed = "1";			
		
					
					return $objReturn;		
	
					
			}
			else
			{

				$logTable = "htc_nsd_orderupdate_xmlfiles";
				$logObj = new stdClass();
				$logObj->agentid = $objAgent->id;
				$logObj->processid = $objAgent->processid;
				$logObj->created_datetime_gmt = gmdate("Y-m-d H:i:s");
				$logObj->created_datetime = date("Y-m-d H:i:s");
				$logObj->processed_datetime_gmt = "";
				$logObj->processed_datetime = "";
				$logObj->flag_processed = "0";				
				$logObj->file_directory = $file_directory;
				$logObj->file_directory_agent = $file_directory_agent;
				$logObj->file_name = $file_name;
				$logObj->file_fullpath = $file_fullpath;
				$logObj->datadate = $datetoday;
				MetalakeHelperCore::logOBJ( $logTable, $logObj );
				
				$logTable = "htc_nsd_orderupdate_log_master";
				$logObj = new stdClass();
				$logObj->datetime = date("Y-m-d H:i:s");
				$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
				$logObj->agentid = $objAgent->id;
				$logObj->notes = "File information for ".$file_name." inserted into htc_nsd_orderupdate_xmlfiles";
				$logObj->function_name = $functionName;
				$logObj->log_level = "";  
				MetalakeHelperCore::logOBJ( $logTable, $logObj );
				#evaluate if needed						

				$logMessage = "INSIDE | ".$className." | ".$functionName." | File information for ".$file_name." inserted into htc_nsd_orderupdate_xmlfiles";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			}
	
	        curl_multi_remove_handle($mh, $ch); // remove the handle (assuming  you are done with it);
	    }		
		
		
		 curl_multi_close($mh); // close the curl multi handler


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);		
		$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objAgent->dispatchtrack_code." | XML download seconds: ".number_format($stamp_total_micro,2);		
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		Nsd_processController::orderupdate_save_data( $objAgent );
		 


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$className." | ".$functionName." | ".$objAgent->dispatchtrack_code." | seconds: ".number_format($stamp_total_micro,2);	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
	}



	public static function orderupdate_save_data( $objAgent )  
	{
		# old function name: saveDispatchtrackDataByAgent( $objAgent )


		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=saveDispatchtrackData

		$className = "Nsd_processController";
		$functionName = "orderupdate_save_data";
		
		$db = JFactory::getDBO();

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
		$cron_xml_dispatchtrack	= $params->get('cron_xml_dispatchtrack');
		$cron_process_xml		= $params->get('cron_process_xml');
		$cron_process_tmw_rules	= $params->get('cron_process_tmw_rules');
		$cron_send_tmw_files	= $params->get('cron_send_tmw_files');	
		$thresholdInMinutes		= $params->get('thresholdInMinutes');
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}	

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_start_micro = microtime(true);

		if (  $cron_process_xml == "1"  )
		{


			$file_name = "sql_".$objAgent->dispatchtrack_api_code ."_" . date("YmdHis"). ".sql";
				
			$file_directory = "nsdorderupdate";
			$file_directory_agent = $objAgent->dispatchtrack_api_code;
			
			
			$fullpath = JPATH_SITE."/".$file_directory."/".$file_directory_agent;
			if ( !JFolder::exists( $fullpath ) )
			{	
				$result = JFolder::create( $fullpath, 0777 );
			}				
			
			
			$sql_file_fullpath = $file_directory."/".$file_directory_agent."/".$file_name;		
		
		
			$query = "SELECT *
			FROM htc_nsd_orderupdate_xmlfiles
			WHERE processed_datetime_gmt = '0000-00-00 00:00:00' AND agentid = '".$objAgent->id."' AND flag_processed = '0' order by id ";
			$db->setQuery($query);
			$objListFiles = $db->loadObjectList();
	
	
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);		
			$logMessage = "INSIDE | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);
			$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objListFiles: ".print_r($objListFiles, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
			if ( $objAgent->id != "" && count($objListFiles) > 0 )
			{

				foreach( $objListFiles as $objFile )
				{
	
					
	
					$strFile = file_get_contents($objFile->file_fullpath);

					
					$needles1 = array();
					$needles1[] = "service_orders";
			
			
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						//$logMessage .= "\n\r strFile: ".print_r($strFile, true);
						$logMessage .= "\n\r needles1: ".print_r($needles1, true);
						
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


					
					#if ( substr($strFile, 0, 5) == "<?xml" )
					
					if ( Nsd_processController::match_all($needles1, $strFile) )
					{
	
						$xml = simplexml_load_file( $objFile->file_fullpath, 'SimpleXMLElement' ); 
						$count = (string)$xml->attributes()->count;
					
	
						if ( $count > 0 )
						{
		
							$stamp_start_micro_file = microtime(true);	
		
							foreach( $xml->service_order as $service_order )
							{
								#load object one with data from previous service order record
								
									$arrFields = array();
									
									$arrFields[] = "service_order_id";
									$arrFields[] = "order_number";
									$arrFields[] = "customer_id";
									$arrFields[] = "first_name";
									$arrFields[] = "last_name";
									$arrFields[] = "email";
									$arrFields[] = "phone1";
									$arrFields[] = "phone2";
									$arrFields[] = "address1";
									$arrFields[] = "address2";
									$arrFields[] = "city";
									$arrFields[] = "state";
									$arrFields[] = "zip";
									$arrFields[] = "latitude";
									$arrFields[] = "longitude";
									$arrFields[] = "display_order_number";
									$arrFields[] = "status";
									$arrFields[] = "service_type";
									$arrFields[] = "description";
									$arrFields[] = "account";
									$arrFields[] = "pre_call_confirmation_status";
									$arrFields[] = "pre_call_text_confirmation_status";
									$arrFields[] = "stop_number";
									$arrFields[] = "scheduled_at";
									$arrFields[] = "time_window_start";
									$arrFields[] = "time_window_end";
									$arrFields[] = "service_time";
									$arrFields[] = "origin";
									$arrFields[] = "request_window_start_time";
									$arrFields[] = "request_window_end_time";
									$arrFields[] = "service_unit";
									$arrFields[] = "started_at";
									$arrFields[] = "finished_at";
									$arrFields[] = "amount";
									$arrFields[] = "cod";
									$arrFields[] = "cod_amount";
									$arrFields[] = "payment_notes";
									$arrFields[] = "payment_collected";
									$arrFields[] = "pieces";
									$arrFields[] = "signature_callback_code";
									$arrFields[] = "signature_callback_created_at";
									$arrFields[] = "extra_account_name";
									$arrFields[] = "extra_delivery_men";
									$arrFields[] = "extra_delivery_return";
									$arrFields[] = "additional_fields_account_name";
									$arrFields[] = "additional_fields_delivery_men";
									$arrFields[] = "additional_fields_delivery_return";
									$arrFields[] = "custom_field_order_exception";
									$arrFields[] = "custom_field_pod_name";
									
									$strFields = implode( ", ", $arrFields);
							
									
									$query = "SELECT $strFields FROM htc_nsd_orderupdate_data_dispatchtrack WHERE service_order_id = '".(string)$service_order->attributes()->id."'  ORDER BY id DESC LIMIT 1" ; 
									$db->setQuery($query);
							        $objDataComparePrior = $db->loadObject();
								
				
									$objData = new stdClass();
									$objData->service_order_id = (string)$service_order->attributes()->id;
									$objData->order_number = (string)$service_order->attributes()->order_number;
									$objData->customer_id = (string)$service_order->customer->customer_id;
									$objData->first_name = (string)$service_order->customer->first_name;
									$objData->last_name = (string)$service_order->customer->last_name;
									$objData->email = (string)$service_order->customer->email;
									$objData->phone1 = (string)$service_order->customer->phone1;
									$objData->phone2 = (string)$service_order->customer->phone2;
									$objData->address1 = (string)$service_order->customer->address1;
									$objData->address2 = (string)$service_order->customer->address2;
									$objData->city = (string)$service_order->customer->city;
									$objData->state = (string)$service_order->customer->state;
									$objData->zip = (string)$service_order->customer->zip;
									$objData->latitude = (string)$service_order->customer->latitude;
									$objData->longitude = (string)$service_order->customer->longitude;
									$objData->display_order_number = (string)$service_order->display_order_number;
									$objData->status = (string)$service_order->status;
									$objData->service_type = (string)$service_order->service_type;
									$objData->description = (string)trim($service_order->description);
									$objData->account = (string)$service_order->account;
									$objData->pre_call_confirmation_status = (string)$service_order->pre_call->confirmation_status;
									$objData->pre_call_text_confirmation_status = (string)$service_order->pre_call->text_confirmation_status;
									$objData->stop_number = (string)$service_order->stop_number;
									$objData->scheduled_at = (string)$service_order->scheduled_at;
									$objData->time_window_start = (string)$service_order->time_window_start;
									$objData->time_window_end = (string)$service_order->time_window_end;
									$objData->service_time = (string)$service_order->service_time;
									$objData->origin = (string)$service_order->origin;
									$objData->request_window_start_time = (string)$service_order->request_window_start_time;
									$objData->request_window_end_time = (string)$service_order->request_window_end_time;
									$objData->service_unit = (string)$service_order->service_unit;
									$objData->started_at = (string)$service_order->started_at;
									$objData->finished_at = (string)$service_order->finished_at;
									$objData->amount = (string)$service_order->amount;
									$objData->cod_amount = (string)$service_order->cod_amount;
									$objData->cod = (string)$service_order->cod;
									$objData->payment_notes = (string)$service_order->payment_notes;
									$objData->payment_collected = (string)$service_order->payment_collected;
									$objData->pieces = (string)$service_order->pieces;
									$objData->signature_callback_code = isset( $service_order->signature['callback_code'] ) ? (string)$service_order->signature->attributes()->callback_code : "" ;
									$objData->signature_callback_created_at = isset( $service_order->signature['created_at'] ) ? (string)$service_order->signature->attributes()->created_at : "" ;
									$objData->extra_account_name = (string)$service_order->extra->account_name;
									$objData->extra_delivery_men = (string)$service_order->extra->delivery_men;
									$objData->extra_delivery_return = (string)$service_order->extra->deliver_return;
									$objData->additional_fields_account_name = (string)$service_order->additional_fields->account_name;
									$objData->additional_fields_delivery_men = (string)$service_order->additional_fields->delivery_men;
									$objData->additional_fields_delivery_return = (string)$service_order->additional_fields->deliver_return;
									$objData->custom_field_order_exception = (string)$service_order->custom_fields->order_exception;
									$value_custom_field_pod_name = "";
									$value_custom_field_pod_name = isset( $service_order->custom_fields->pod_name ) ? (string)$service_order->custom_fields->pod_name : "NOVALUE" ;
									$objData->custom_field_pod_name = ( trim($value_custom_field_pod_name) != "" ) ? $value_custom_field_pod_name : "NOVALUE" ;
									
									if ( $objDataComparePrior != $objData )
									{
										$objData->agent_id = $objFile->agentid;
										$objData->importfile = $objFile->id;
										$objData->stamp_datetime = date("Y-m-d H:i:s");
										$objData->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			
										$result = $db->insertObject('htc_nsd_orderupdate_data_dispatchtrack', $objData);
										$objID = $db->insertid();

/*
											$fpsql = fopen($sql_file_fullpath, 'a+');
											$arrColumns = array();
											$arrValues = array();
											
											foreach( $objData as $key => $value ) 
											{
												
												$arrColumns[] = $key;
												$arrValues[] = addslashes($value);
											}
									
											$strInsert = "";
											$strInsert .= "INSERT INTO htc_nsd_orderupdate_data_dispatchtrack (".implode(',',$arrColumns).") VALUES (\"".implode("\",\"",$arrValues)."\");\n\r";
													
											if ( count( $arrColumns ) > 0 )
											{
												fwrite( $fpsql, $strInsert );												
											}
											fclose($fpsql);
*/

							
										$logMessage = "INSIDE | ".$className." | ".$functionName;
										$logMessage .= "\n\r objData: ".print_r($objData, true);
										$logMessage .= "\n\r result: ".print_r($result, true);	
										$logMessage .= "\n\r objID: ".print_r($objID, true);	
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	
										$logTable = "htc_nsd_orderupdate_log_master";
										$logObj = new stdClass();
										$logObj->datetime = date("Y-m-d H:i:s");
										$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
										$logObj->agentid = $objFile->agentid;
										$logObj->notes = "Data inserted into htc_nsd_orderupdate_data_dispatchtrack from, ".$objFile->file_name;
										$logObj->function_name = $functionName;  
										$logObj->log_level = ""; 
										MetalakeHelperCore::logOBJ( $logTable, $logObj );
										
										
										#ML0424
										#Nsd_processController::orderupdate_get_status( $objData );

										$query = "select * from htc_nsd_orderentry_service_levels where agent_id = '".$objAgent->id."' and service_type = '".$objData->service_type."' limit 1";
										$db->setQuery($query);
										$objRow = $db->loadObject();


										$arrValidCodes = array();
										$arrValidCodes[] = "BS1";
										$arrValidCodes[] = "BS1A";
										$arrValidCodes[] = "BS2";
										$arrValidCodes[] = "BS2A";
										$arrValidCodes[] = "LTL";
										$arrValidCodes[] = "BASIC";

										
										if ( in_array( $objRow->code, $arrValidCodes) )
										{
											# basic order


											if ( date("Y-m-d", strtotime($objDataComparePrior->time_window_start)) != date("Y-m-d", strtotime($objData->time_window_start)) )
											{
											
												$objStatus = MetalakeHelperCore::getObjTableData( $table="htc_nsd_orderupdate_data_dispatchtrack_current_status", $field="order_number", $value = $objData->order_number );
												$objUpdate = new stdClass();
												$objUpdate->id = $objStatus->id;
												$objUpdate->time_window_start = $objData->time_window_start;
												$objUpdate->flag_confirmation = "0";
												$objUpdate->stamp_datetime = date("Y-m-d H:i:s");
												$objUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
												
												$resultUpdate = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_dispatchtrack_current_status', $objUpdate, 'id'); 																						

												switch( $objAgent->name  )
												{
													case "ISDL":
													
														$strSOID = $objData->service_order_id;
													
														if ( $strSOID[0] == "A" )
														{
															$freight_bill_export = substr( $objData->service_order_id, 1 );												
														}
														else
														{
															$freight_bill_export = substr( $objData->service_order_id, 2 );										
														}
													
					
														break;
					
													case "OTEX":
													
														$strSOID = $objData->service_order_id;
													
														if ( $strSOID[0] == "A" )
														{
															$freight_bill_export = substr( $objData->service_order_id, 1 );												
														}
														else
														{
															$freight_bill_export = $objData->service_order_id;
														}
					
														break;

													case "SHIP":
													
														$strSOID = $objData->service_order_id;
													
														$freight_bill_parts = explode("-", $strSOID);
													
														$freight_bill_export = $freight_bill_parts[0];
					
														break;
													
													default:
														$freight_bill_export = $objData->service_order_id;
														break;
												}	


			
												$objDataT = new stdClass();
												$objDataT->agent_name = $objAgent->name;
												$objDataT->freight_bill = $objData->service_order_id;
												$objDataT->freight_bill_export = $freight_bill_export;
												$objDataT->change_datetime = date("Y-m-d H:i:s");
												$objDataT->timezone = "ET";
												$objDataT->customer_city = $objData->city;
												$objDataT->customer_state = $objData->state;
												$objDataT->status_code = "NS";
												$objDataT->reason_code = "NS";
												$objDataT->truckmate_status = "AG";
												$objDataT->comment = "BASIC Order Auto-Confirmation";
												$objDataT->stamp_datetime = date("Y-m-d H:i:s");
												$objDataT->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");	
												$objDataT->importfile = "";
												$objDataT->flag_processed = "0";		
												$objDataT->transfer_file = "";
												$objDataT->order_status = "Scheduled"; 
												$objDataT->agent_id = $objAgent->id;
				
												$objDataT->scheduled_at = "";
												$objDataT->scheduled_at_end = "";
												$objDataT->time_window_start = $objData->time_window_start;
												$objDataT->time_window_end = $objData->time_window_end;
												$objDataT->started_at = "";
												$objDataT->finished_at = "";
												$objDataT->custom_field_pod_name = "NOVALUE";
												$objDataT->request_window_start_time = "";
												$objDataT->request_window_end_time = "";
				
																	
												$result = $db->insertObject('htc_nsd_orderupdate_data_truckmate', $objDataT);
												
												
												$objUpdate = new stdClass();
												$objUpdate->id = $objStatus->id;
												$objUpdate->flag_confirmation = "1";
												$objUpdate->stamp_datetime = date("Y-m-d H:i:s");
												$objUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
												
												$resultUpdate = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_dispatchtrack_current_status', $objUpdate, 'id'); 	
												
												
											}

											
										}
										else
										{
											# not basic order
											

											if ( $objDataComparePrior->time_window_start != $objData->time_window_start )
											{
											
												$objStatus = MetalakeHelperCore::getObjTableData( $table="htc_nsd_orderupdate_data_dispatchtrack_current_status", $field="order_number", $value = $objData->order_number );
												$objUpdate = new stdClass();
												$objUpdate->id = $objStatus->id;
												$objUpdate->time_window_start = $objData->time_window_start;
												$objUpdate->flag_confirmation = "0";
												$objUpdate->stamp_datetime = date("Y-m-d H:i:s");
												$objUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
												
												$resultUpdate = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_dispatchtrack_current_status', $objUpdate, 'id'); 											
												
											}
											
										}
			
									}		
					
								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r service_order->order_history: ".print_r($service_order->order_history, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					
					
								if ( count( $service_order->order_history->history ) > 0 )
								{		
									$fpsql = fopen($sql_file_fullpath, 'a+');
								
									foreach( $service_order->order_history->history as $history )
									{
						
										#$json = json_encode($history);
										#$inputArray = json_decode($json,TRUE);				
						
										$description = (string)trim($history);
						
										$date = (string)$history->attributes()->date;
										$time = (string)$history->attributes()->time;
										$lat = (string)$history->attributes()->lat;
										$lng = (string)$history->attributes()->lng;
						
										
										$logMessage = "INSIDE | ".$className." | ".$functionName;
										#$logMessage .= "\n\r inputArray: ".print_r($inputArray, true);
										$logMessage .= "\n\r history attributes: ".print_r($history, true);
										$logMessage .= "\n\r date: ".print_r($date, true);
										$logMessage .= "\n\r time: ".print_r($time, true);
										$logMessage .= "\n\r lat: ".print_r($lat, true);
										$logMessage .= "\n\r lng: ".print_r($lng, true);
										$logMessage .= "\n\r description: ".print_r($description, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

										
										
										$fulltime = $date." ".$time;
										$strFulltime = strtotime($fulltime);
										$datetime = date("Y-m-d H:i:s", $strFulltime );
										
			
										$objAgent = MetalakeHelperCore::getObjTableData( $table="htc_dispatchtrack_agents", $column="id", $value=$objFile->agentid );
			
										
										$objDataHistory = new stdClass();
										$objDataHistory->service_order_id = $objData->service_order_id;
										$objDataHistory->date = $date;
										$objDataHistory->time = $time;
										$objDataHistory->datetime = $datetime;
										$objDataHistory->lat = $lat;
										$objDataHistory->lng = $lng;
										$objDataHistory->description = $description;
										$objDataHistory->stamp_datetime = date("Y-m-d H:i:s");
										$objDataHistory->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
										$objDataHistory->importfile = $objFile->id;	
										$objDataHistory->customer_city = $objData->city;
										$objDataHistory->customer_state = $objData->state;
										$objDataHistory->agent_id = $objAgent->id;
										$objDataHistory->agent_name = $objAgent->name;
										$objDataHistory->order_status = $objData->status;
										$objDataHistory->custom_field_pod_name = $objData->custom_field_pod_name;
			

										$recordsUnixTimestamp = strtotime( $objDataHistory->datetime );
										
										$timeDifferenceInMinutes = ( ( time() - $recordsUnixTimestamp ) / 60 ); #in minutes
										
										#$thresholdInMinutes = 30; #minutes	
										
										#$timeThreshold = 240; #minutes		
							
										if ( $timeDifferenceInMinutes < $thresholdInMinutes || $thresholdInMinutes == "ALL" )
										{
										
											#under threshold
											$logMessage = "INSIDE | ".$className." | ".$functionName." | record under threshold: ".number_format($timeDifferenceInMinutes, 2)." | ".$objDataHistory->datetime." | ".$objDataHistory->service_order_id." | ".$thresholdInMinutes;	
											#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

											$descipt200 = substr($objDataHistory->description, 0, 200);
											$queryExist = "SELECT id from htc_nsd_orderupdate_data_dispatchtrack_order_history WHERE service_order_id = '".$objDataHistory->service_order_id."' and description = \"".addslashes($descipt200)."\" and datetime = '".$objDataHistory->datetime."' and agent_id = '".$objAgent->id."' ";
											$db->setQuery($queryExist);
											$rowExists = $db->loadResult();
	
											$logMessage = "INSIDE | ".$className." | ".$functionName;
											$logMessage .= "\n\r queryExist: ".print_r($queryExist, true);
											$logMessage .= "\n\r rowExists: ".print_r($rowExists, true);
											$logMessage .= "\n\r ";
											if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
											
											if ( $objData->service_order_id == "9100446" )
											{
												#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
											}
					
											if ( !$rowExists || $rowExists == "" )
											{
												try
												{
													$result = $db->insertObject('htc_nsd_orderupdate_data_dispatchtrack_order_history', $objDataHistory);
													$objIDHistory = $db->insertid();
			
													$logTable = "htc_nsd_orderupdate_log_master";
													$logObj = new stdClass();
													$logObj->datetime = date("Y-m-d H:i:s");
													$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
													$logObj->agentid = $objFile->agentid;
													$logObj->notes = "Data inserted into htc_nsd_orderupdate_data_dispatchtrack_order_history from, ".$objFile->file_name;
													$logObj->function_name = $functionName; 
													$logObj->log_level = "";  
													MetalakeHelperCore::logOBJ( $logTable, $logObj );
			
													
												}
												catch ( Exception $e )
												{
													$result = "not saved";
													$objIDHistory = "";
												}	
											}

/*
											$arrColumns = array();
											$arrValues = array();
											
											foreach( $objDataHistory as $key => $value ) 
											{
												
												$arrColumns[] = $key;
												$arrValues[] = addslashes($value);
											}
									
											$strInsert = "";
											$strInsert .= "INSERT INTO htc_nsd_orderupdate_data_dispatchtrack_order_history (".implode(',',$arrColumns).") VALUES (\"".implode("\",\"",$arrValues)."\");\n\r";
													
											if ( count( $arrColumns ) > 0 )
											{
												fwrite( $fpsql, $strInsert );												
											}
*/

											
											
										}
										else
										{
											$logMessage = "INSIDE | ".$className." | ".$functionName." | record over threshold: ".number_format($timeDifferenceInMinutes, 2)." | ".$objDataHistory->datetime." | ".$objDataHistory->service_order_id." | ".$thresholdInMinutes;		
											#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }											
											if ( $objData->service_order_id == "9100446" )
											{
												#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
											}

										}
				
				
											
										
									}
									
									fclose($fpsql);
								}	
			
								if ( count( $service_order->notes->note ) > 0 )
								{
									
									$fpsql = fopen($sql_file_fullpath, 'a+');
									
									foreach( $service_order->notes->note as $note )
									{
						
										#$json = json_encode($history);
										#$inputArray = json_decode($json,TRUE);				
						
										$description = (string)trim($note);
						
										$created_at = (string)$note->attributes()->created_at;
										$author = (string)$note->attributes()->author;
					
										
										$logMessage = "INSIDE | ".$className." | ".$functionName;
										#$logMessage .= "\n\r inputArray: ".print_r($inputArray, true);
										$logMessage .= "\n\r note attributes: ".print_r($note, true);
										$logMessage .= "\n\r created_at: ".print_r($created_at, true);
										$logMessage .= "\n\r author: ".print_r($author, true);
				
							
										$logMessage .= "\n\r description: ".print_r($description, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										
										
										$objDataNote = new stdClass();
										$objDataNote->service_order_id = $objData->service_order_id;
										$objDataNote->created_at = $created_at;
										$objDataNote->author = $author;
										$objDataNote->note = $description;
										$objDataNote->stamp_datetime = date("Y-m-d H:i:s");
										$objDataNote->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");	
										$objDataNote->importfile = $objFile->id;


										$note248 = substr($description, 0, 248);
										
										$queryExist = "SELECT id from htc_nsd_orderupdate_data_dispatchtrack_notes WHERE service_order_id = '".$objDataNote->service_order_id."' and created_at = '".$objDataNote->created_at."' and author = \"".addslashes($objDataNote->author)."\" and note = \"".addslashes($note248)."\" ";
				
										$queryExistOLD = "SELECT id from htc_nsd_orderupdate_data_dispatchtrack_notes WHERE service_order_id = '".$objDataNote->service_order_id."' and created_at = '".$objDataNote->created_at."' and author = '".$objDataNote->author."' and note = \"".addslashes($objDataNote->note)."\" ";

										$db->setQuery($queryExist);
										$rowExists = $db->loadResult();

										if ( !$rowExists || $rowExists == "" )
										{	

											$logMessage = "INSIDE | ".$className." | ".$functionName;
											$logMessage .= "\n\r queryExistOLD: ".print_r($queryExistOLD, true);
											$logMessage .= "\n\r queryExist: ".print_r($queryExist, true);
											$logMessage .= "\n\r description: ".print_r($description, true);
											$logMessage .= "\n\r note248: ".print_r($note248, true);
											$logMessage .= "\n\r rowExists: ".print_r($rowExists, true);
											$logMessage .= "\n\r ";
											#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
											#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

										}

				
										if ( !$rowExists || $rowExists == "" )
										{				
											try
											{
												$result = $db->insertObject('htc_nsd_orderupdate_data_dispatchtrack_notes', $objDataNote);
												$objIDNote = $db->insertid();
		
		
												$logTable = "htc_nsd_orderupdate_log_master";
												$logObj = new stdClass();
												$logObj->datetime = date("Y-m-d H:i:s");
												$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
												$logObj->agentid = $objFile->agentid;
												$logObj->notes = "Data inserted into htc_nsd_orderupdate_data_dispatchtrack_notes from, ".$objFile->file_name;
												$logObj->function_name = $functionName;   
												$logObj->log_level = "";
												MetalakeHelperCore::logOBJ( $logTable, $logObj );
		
											}
											catch ( Exception $e )
											{
				
												$result = "not saved";
												$objIDNote = "";
				
											
											}	
										}

/*
											$arrColumns = array();
											$arrValues = array();
											
											foreach( $objDataNote as $key => $value ) 
											{
												
												$arrColumns[] = $key;
												$arrValues[] = addslashes($value);
											}
									
											$strInsert = "";
											$strInsert .= "INSERT INTO htc_nsd_orderupdate_data_dispatchtrack_notes (".implode(',',$arrColumns).") VALUES (\"".implode("\",\"",$arrValues)."\");\n\r";
													
											if ( count( $arrColumns ) > 0 )
											{
												fwrite( $fpsql, $strInsert );												
											}
*/
										
										
										$logMessage = "INSIDE | ".$className." | ".$functionName;
										$logMessage .= "\n\r objDataNote: ".print_r($objDataNote, true);
										$logMessage .= "\n\r result: ".print_r($result, true);	
										$logMessage .= "\n\r objIDNote: ".print_r($objIDNote, true);	
										$logMessage .= "\n\r ";
										#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
										
										
									}
									
									
									fclose($fpsql);
									
								}
			
			
								if ( count( $service_order->images->image ) > 0 )
								{


					
									$fpsql = fopen($sql_file_fullpath, 'a+');
								
									foreach( $service_order->images->image as $image )
									{
						
										$src = (string)$image->attributes()->src;
										$thumbnail = (string)$image->attributes()->thumbnail;
										$created_at = (string)$image->attributes()->created_at;
					
										
										$objAgent = Nsd_processController::getObjTableData( $table="htc_nsd_orderupdate_agents", $column="id", $value=$objFile->agentid );

										$file_directory = "nsdorderupdate";
										$file_directory_agent = $objAgent->dispatchtrack_code;
										$export_image_path = JPATH_SITE."/".$file_directory."/".$file_directory_agent;
										
										$logMessage = "INSIDE | ".$className." | ".$functionName;
										$logMessage .= "\n\r src: ".print_r($src, true);
										$logMessage .= "\n\r thumbnail: ".print_r($thumbnail, true);
										$logMessage .= "\n\r created_at: ".print_r($created_at, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										
										
										$objDataImage = new stdClass();
										$objDataImage->service_order_id = $objData->service_order_id;
										$objDataImage->created_at = $created_at;
										$objDataImage->src = $src;
										$objDataImage->thumbnail = $thumbnail;
										$objDataImage->stamp_datetime = date("Y-m-d H:i:s");
										$objDataImage->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");	
										$objDataImage->importfile = $objFile->id;
										$objDataImage->export_image_path = $export_image_path;
										$objDataImage->agent_id = $objFile->agentid;


										$queryExist = "SELECT id from htc_nsd_orderupdate_data_dispatchtrack_images WHERE service_order_id = '".$objDataImage->service_order_id."' and created_at = '".$objDataImage->created_at."' and src = '".$objDataImage->src."' ";
										$db->setQuery($queryExist);
										$rowExists = $db->loadResult();
				
										if ( !$rowExists || $rowExists == "" )
										{					
											try
											{
												$result = JFactory::getDbo()->insertObject('htc_nsd_orderupdate_data_dispatchtrack_images', $objDataImage);
												$objIDImage = $db->insertid();
												
												$logTable = "htc_nsd_orderupdate_log_master";
												$logObj = new stdClass();
												$logObj->datetime = date("Y-m-d H:i:s");
												$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
												$logObj->agentid = $objFile->agentid;
												$logObj->notes = "Data inserted into htc_nsd_orderupdate_data_dispatchtrack_images from, ".$objFile->file_name;
												$logObj->function_name = $functionName;   
												$logObj->log_level = "";
												MetalakeHelperCore::logOBJ( $logTable, $logObj );
												
											}
											catch ( Exception $e )
											{
											
											}	
										}
										
							
/*
										$arrColumns = array();
										$arrValues = array();
										
										foreach( $objDataImage as $key => $value ) 
										{
											
											$arrColumns[] = $key;
											$arrValues[] = addslashes($value);
										}
								
										$strInsert = "";
										$strInsert .= "INSERT INTO htc_nsd_orderupdate_data_dispatchtrack_images (".implode(',',$arrColumns).") VALUES (\"".implode("\",\"",$arrValues)."\");\n\r";
										
										if ( count( $arrColumns ) > 0 )
										{
											fwrite( $fpsql, $strInsert );												
										}
*/
										
										$logMessage = "INSIDE | ".$className." | ".$functionName;
										$logMessage .= "\n\r objDataImage: ".print_r($objDataImage, true);
										$logMessage .= "\n\r result: ".print_r($result, true);	
										$logMessage .= "\n\r objIDImage: ".print_r($objIDImage, true);	
										$logMessage .= "\n\r ";
										#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
										
										
									}

									fclose($fpsql);


								}
	
			
			
								if ( count( $service_order->items->item ) > 0 )
								{
									
									
									$fpsql = fopen($sql_file_fullpath, 'a+');
									
									foreach( $service_order->items->item as $item )
									{
						
							
										$number = (string)$item->number;
										$serial_number = (string)$item->serial_number;
										$sale_sequence = (string)$item->sale_sequence;
										$quantity = (string)$item->quantity;
										$delivered = (string)$item->delivered;
										$delivered_quantity = (string)$item->delivered_quantity;
										$item_notes = (string)$item->item_notes;
										$notes = (string)$item->notes;
										$warehouse_notes = (string)$item->warehouse_notes;
										$amount = (string)$item->amount;
										$checked_quantity = (string)$item->checked_quantity;
										$location = (string)$item->location;
									
										$item_description = (string)trim($item->description);
										
										$return_code_manager = $return_code_warehouse = $return_code_customer = "";
										
										if (  count( $item->return_codes->return_code ) > 0  )
										{
											foreach( $item->return_codes->return_code as $rc )
											{
			
												if ( (string)$rc->attributes()->source == "customer" )
												{
													$return_code_customer = (string)$rc;
												}
			
												if ( (string)$rc->attributes()->source == "warehouse" )
												{
													$return_code_warehouse = (string)$rc;
												}
			
			
												if ( (string)$rc->attributes()->source == "manager" )
												{
													$return_code_manager = (string)$rc;
												}
			
											}
			
										}
					
										
									
										
										$objDataItems = new stdClass();
										$objDataItems->service_order_id = $objData->service_order_id;
										$objDataItems->number = $number;
										$objDataItems->serial_number = $serial_number;
										$objDataItems->description = $item_description;
										$objDataItems->sale_sequence = $sale_sequence;
										$objDataItems->quantity = $quantity;
										$objDataItems->delivered = $delivered;
										$objDataItems->delivered_quantity = $delivered_quantity;
										$objDataItems->item_notes = $item_notes;
										$objDataItems->notes = $notes;
										$objDataItems->warehouse_notes = $warehouse_notes;
										$objDataItems->amount = $amount;
										$objDataItems->checked_quantity = $checked_quantity;
										$objDataItems->location = $location;
										$objDataItems->return_code_customer = $return_code_customer;
										$objDataItems->return_code_warehouse = $return_code_warehouse;
										$objDataItems->return_code_manager = $return_code_manager;
										$objDataItems->stamp_datetime = date("Y-m-d H:i:s");
										$objDataItems->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");	
										$objDataItems->importfile = $objFile->id;
										$objDataItems->agent_id = $objFile->agentid;					

										$desciptItem200 = substr($objDataItems->description, 0, 200);

										$queryExist = "SELECT id from htc_nsd_orderupdate_data_dispatchtrack_items WHERE service_order_id = '".$objDataItems->service_order_id."' 
										and description = '".addslashes($desciptItem200)."' 
										and delivered = '".$objDataItems->delivered."' 
										and return_code_customer = '".$objDataItems->return_code_customer."' 
										and return_code_warehouse = '".$objDataItems->return_code_warehouse."' 
										and return_code_manager = '".$objDataItems->return_code_manager."' 
										and notes = \"".addslashes($objDataItems->notes)."\" 
										and warehouse_notes = \"".addslashes($objDataItems->warehouse_notes)."\" 
										";
										$db->setQuery($queryExist);
										$rowExists = $db->loadResult();
				
										if ( !$rowExists || $rowExists == "" )
										{	
				
											try
											{
												$result = $db->insertObject('htc_nsd_orderupdate_data_dispatchtrack_items', $objDataItems);
												$objIDItems = $db->insertid();
		
												$logTable = "htc_nsd_orderupdate_log_master";
												$logObj = new stdClass();
												$logObj->datetime = date("Y-m-d H:i:s");
												$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
												$logObj->agentid = $objFile->agentid;
												$logObj->notes = "Data inserted into htc_nsd_orderupdate_data_dispatchtrack_items from, ".$objFile->file_name;
												$logObj->function_name = $functionName;  
												$logObj->log_level = ""; 
												MetalakeHelperCore::logOBJ( $logTable, $logObj );
		
											}
											catch ( Exception $e )
											{
												$result = "not saved";
												$objIDItems = "";
											}	
										}
/*
										$arrColumns = array();
										$arrValues = array();
										
										foreach( $objDataItems as $key => $value ) 
										{
											
											$arrColumns[] = $key;
											$arrValues[] = addslashes($value);
										}
								
										$strInsert = "";
										$strInsert .= "INSERT INTO htc_nsd_orderupdate_data_dispatchtrack_items (".implode(',',$arrColumns).") VALUES (\"".implode("\",\"",$arrValues)."\");\n\r";
												
										if ( count( $arrColumns ) > 0 )
										{
											fwrite( $fpsql, $strInsert );												
										}
*/
										
										
										$logMessage = "INSIDE | ".$className." | ".$functionName;
										$logMessage .= "\n\r objDataItems: ".print_r($objDataItems, true);
										$logMessage .= "\n\r result: ".print_r($result, true);	
										$logMessage .= "\n\r objIDItems: ".print_r($objIDItems, true);	
										$logMessage .= "\n\r ";
										#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
										
										
									}
									
									fclose($fpsql);
								}
								
							}

							$stamp_end_micro_file = microtime(true);
							$stamp_total_micro_file = ($stamp_end_micro_file - $stamp_start_micro_file);


							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
							$logMessage .= "\n\r HERE";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
							$logTable = "htc_nsd_orderupdate_xmlfiles";
							$logObj = new stdClass();
							$logObj->id = $objFile->id;
							$logObj->processed_datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->processed_datetime = date("Y-m-d H:i:s");
							$logObj->flag_processed = 1;
							$logObj->order_count = $count;
							$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');
	
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objFile->agentid;
							$logObj->notes = "Data analyzed and processed from, ".$objFile->file_name;
							$logObj->function_name = $functionName;   
							$logObj->log_level = "";
							MetalakeHelperCore::logOBJ( $logTable, $logObj );

							$logMessage = "INSIDE | ".$className." | ".$functionName." | Data analyzed and processed from, ".$objFile->file_name." | seconds: ".number_format($stamp_total_micro_file,2);	
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
							
						}
						else if ( $count == 0 )
						{
		
							$logTable = "htc_nsd_orderupdate_xmlfiles";
							$logObj = new stdClass();
							$logObj->id = $objFile->id;
							$logObj->processed_datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->processed_datetime = date("Y-m-d H:i:s");
							$logObj->flag_processed = 1;
							$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');
			
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objFile->agentid;
							$logObj->notes = "There are no service orders to save or process from, ".$objFile->file_name;
							$logObj->function_name = $functionName;
							$logObj->log_level = "";   
							MetalakeHelperCore::logOBJ( $logTable, $logObj );
							
							$logMessage = "INSIDE | ".$className." | ".$functionName." | There are no service orders to save or process from, ".$objFile->file_name;
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
							
						}
						else
						{

							$error_message = (string)$xml->error;

							$logTable = "htc_nsd_orderupdate_xmlfiles";
							$logObj = new stdClass();
							$logObj->id = $objFile->id;
							$logObj->processed_datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->processed_datetime = date("Y-m-d H:i:s");
							$logObj->flag_processed = 1;
							$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');
			
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objFile->agentid;
							$logObj->notes = "There is an error, ".$error_message.", in, ".$objFile->file_name;
							$logObj->function_name = $functionName;
							$logObj->log_level = "";   
							MetalakeHelperCore::logOBJ( $logTable, $logObj );
							
							$logMessage = "INSIDE | ".$className." | ".$functionName." | There is an error, ".$error_message.", in, ".$objFile->file_name;
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }							


							$objAlert = new stdClass();
							$objAlert->recipients = array( 'support@metalake.com' );
							$objAlert->subject = "NSD MONITOR | ".$functionName;
							$objAlert->body = "ALERT: There is an error, ".$error_message.", in, ".$objFile->file_name;
							#$resultAlert = Nsd_processController::sendAlert( $objAlert );
							
						}
	
						
					}
					else
					{

						$needles2 = array();
						$needles2[] = "Valid";
						$needles2[] = "Key";
						$needles2[] = "Code";
				
				
						$needles3 = array();
						$needles3[] = "502";
						$needles3[] = "Bad";
						$needles3[] = "Gateway";
	
	
						$needles4 = array();
						$needles4[] = "Api";
						$needles4[] = "Request";


						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r strFile: ".print_r($strFile, true);
						$logMessage .= "\n\r needles1: ".print_r($needles1, true);
						$logMessage .= "\n\r needles2: ".print_r($needles2, true);
						$logMessage .= "\n\r needles3: ".print_r($needles3, true);
						$logMessage .= "\n\r needles4: ".print_r($needles4, true);
						$logMessage .= "\n\r haystack: ".print_r($haystack, true);
						$logMessage .= "\n\r result: ".print_r($result, true);
						
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



						if (  Nsd_processController::match_all($needles2, $strFile) )
						{
							$lognote = "ALERT: Valid API Key and API Code is required | ".$objFile->file_name;
							$log_level = "2";
							$flag_send_alert = "1";
						}	
						elseif (  Nsd_processController::match_all($needles3, $strFile) )
						{
							$lognote = "ALERT: 502 Bad Gateway | ".$objFile->file_name;
							$log_level = "2";
							$flag_send_alert = "1";
						}	
						elseif (  Nsd_processController::match_all($needles4, $strFile) )
						{
							$lognote = "ALERT: Api Request count exceeded | ".$objFile->file_name;
							$log_level = "2";
							$flag_send_alert = "0";
						}	
						else
						{
							$lognote = "ALERT: Not an XML file, ".$objFile->file_name." | ";
							$log_level = "2";
							$flag_send_alert = "1";
						}				
						
						
						$logTable = "htc_nsd_orderupdate_xmlfiles";
						$logObj = new stdClass();
						$logObj->id = $objFile->id;
						$logObj->processed_datetime_gmt = gmdate("Y-m-d H:i:s");
						$logObj->processed_datetime = date("Y-m-d H:i:s");
						$logObj->flag_processed = 1;
						$logObj->notes = $lognote;
						$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');
	
						$logTable = "htc_nsd_orderupdate_log_master";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
						$logObj->agentid = $objAgent->id;
						$logObj->notes = $lognote;
						$logObj->function_name = $functionName;  
						$logObj->log_level = $log_level; 
						MetalakeHelperCore::logOBJ( $logTable, $logObj );


						if ( $flag_send_alert == 1 )
						{

							$objAlert = new stdClass();
							#$objAlert->recipients = array( 'support@metalake.com' );
							$objAlert->recipients = array( 'lsawyer@metalake.com' );
							$objAlert->subject = "NSD MONITOR | ".$functionName;
							$objAlert->body = $lognote." | ".$strFile;
							#$resultAlert = Nsd_processController::sendAlert( $objAlert );
	
							$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
											
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objAgent->id;
							$logObj->notes = $lognote;
							$logObj->function_name = $functionName; 
							$logObj->log_level = $log_level;  
							#MetalakeHelperCore::logOBJ( $logTable, $logObj );

						}


						
					}
					
				
				}
				
			}
	

		}
		else
		{

			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = $objAgent->id;
			$logObj->notes = "ALERT: Cron to process and save information from XML files is OFF";
			$logObj->function_name = $functionName;   
			$logObj->log_level = "1";
			MetalakeHelperCore::logOBJ( $logTable, $logObj );	

			$objAlert = new stdClass();
			$objAlert->recipients = array( 'support@metalake.com' );
			$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
			$body = "";
			$body .= "<br> ALERT: Cron to process and save information from XML files is OFF";
			$body .= "<br><br>";
			$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
			$body .= "<br><br>";			
			$body .= "<br> Steps to triage:";
			$body .= "<br><br>";
			$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
			$body .= "<br> Go to configuration of the API component.";
			$body .= "<br> Turn on cron for: 'Cron to process and save information from XML files'";
			$body .= "<br><br>";
			$body .= "<br> Check that master cron for this function is running on server.";
			$body .= "<br> ";
			$objAlert->body = $body;
			#$resultAlert = Nsd_processController::sendAlert( $objAlert );


			
			$objAlert = new stdClass();
			$objAlert->recipients = array( '7039305383@txt.att.net' );
			$objAlert->subject = "NSD MONITOR | ".$functionName;
			$objAlert->body = "ALERT: Cron to process and save information from XML files is OFF. Check email for details.";
			#$resultAlert = Nsd_processController::sendAlert( $objAlert );

			$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
							
			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = $objAgent->id;
			$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
			$logObj->function_name = $functionName; 
			$logObj->log_level = "1";  
			MetalakeHelperCore::logOBJ( $logTable, $logObj );

		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | ".$objAgent->dispatchtrack_code." | seconds: ".number_format($stamp_total_micro,2);		
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
	}



	public static function orderupdate_get_status( $objData )
	{
		# old function name: getCurrentStatus( $objData )

		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=getCurrentStatus
		
		$className = "Nsd_processController";
		$functionName = "getCurrentStatus";
		
		$db = JFactory::getDBO();
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objResult = MetalakeHelperCore::getObjTableData( $table="htc_nsd_orderupdate_data_dispatchtrack_current_status", $field="order_number", $value = $objData->order_number );

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objResult: ".print_r($objResult, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( !is_object($objResult) )
		{
			$objInsert = new stdClass();
			$objInsert->agent_id = $objData->agent_id;
			$objInsert->order_number = $objData->order_number;
			$objInsert->time_window_start = $objData->time_window_start;
			$objInsert->flag_confirmation = "0";
			$objInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			
			$result = JFactory::getDbo()->insertObject('htc_nsd_orderupdate_data_dispatchtrack_current_status', $objInsert);
			$insertID = $db->insertid();


			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objInsert: ".print_r($objInsert, true);
			$logMessage .= "\n\r result: ".print_r($result, true);
			$logMessage .= "\n\r insertID: ".print_r($insertID, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			
		}


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r aaa: ".print_r($aaa, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

	
	
	public function cron_process_orderupdate_sender( )  
	{
		# old function name: cronProcessDataForTruckmate()

		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=cronProcessDataForTruckmate
	
		$className = "Nsd_processController";
		$functionName = "cronProcessDataForTruckmate";

		$db = JFactory::getDBO();

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
		$cron_xml_dispatchtrack	= $params->get('cron_xml_dispatchtrack');
		$cron_process_xml		= $params->get('cron_process_xml');
		$cron_process_tmw_rules	= $params->get('cron_process_tmw_rules');
		$cron_send_tmw_files	= $params->get('cron_send_tmw_files');	
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}
		

		$logMessage = "START | ".$className." | ".$functionName;
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_start_micro = microtime(true);

		if (  $cron_send_tmw_files == "1"  )
		{
			
			#process order history
			$return_poh = Nsd_processController::orderupdate_order_history( );

		
			#ML050218 - no more OFD process		
			#run out for delivery process for all agents
			#$return_cofd = Nsd_processController::cronOutForDelivery( );


			$query = "SELECT * FROM htc_nsd_orderupdate_agents WHERE state = '1' ";
			$db->setQuery($query);
			$objListAgents = $db->loadObjectList();
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objListAgents: ".print_r($objListAgents, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
			foreach( $objListAgents as $objAgent )
			{

				#create TMW file
				$return_ctf = Nsd_processController::orderupdate_create_files( $objAgent );
				
				#if tmw file created, send it
/*
				if ( $return_ctf )
				{
					$return_stf = Nsd_processController::sendTruckmateTransferFileAgentMultiFile( $objAgent ); 
				}
*/
				#ML081618
				#$return_stf = Nsd_processController::sendTruckmateTransferFileAgentMultiFile( $objAgent ); 
				$return_stf = Nsd_processController::orderupdate_send_files( $objAgent );
			}

			#ML081618
			#send images - sends any new image to nsd 
			#$return_sitnsd = Nsd_processController::sendImagesToNSD( );

			#ML081618
			#prune directories - prunes each agent directory, removing xml, sql, jpg files
			#$return_pd = Nsd_processController::pruneDirectory( );

			#ML081618
			#prune logs - will only run around 11pm est
			#$return_logs = Nsd_processController::pruneLogs( );
			
			#prune tables - will only run once first day of the month at 00:08 AM 
			#$return_tables = Nsd_processController::pruneTables();
			
		}
		else
		{

			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = "";
			$logObj->notes = "ALERT: Cron to create and send TMW file to NSD is OFF";
			$logObj->function_name = $functionName;   
			$logObj->log_level = "1";
			MetalakeHelperCore::logOBJ( $logTable, $logObj );	

			$objAlert = new stdClass();
			$objAlert->recipients = array( 'support@metalake.com' );
			$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
			$body = "";
			$body .= "<br> ALERT: Cron to create and send TMW file to NSD is OFF";
			$body .= "<br><br>";
			$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
			$body .= "<br><br>";			
			$body .= "<br> Steps to triage:";
			$body .= "<br><br>";
			$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
			$body .= "<br> Go to configuration of the API component.";
			$body .= "<br> Turn on cron for: 'Cron to create and send TMW file to NSD'";
			$body .= "<br><br>";
			$body .= "<br> Check that master cron for this function is running on server.";
			$body .= "<br> ";
			$objAlert->body = $body;
			#$resultAlert = Nsd_processController::sendAlert( $objAlert );
			
			$objAlert = new stdClass();
			$objAlert->recipients = array( '7039305383@txt.att.net' );
			$objAlert->subject = "NSD MONITOR | ".$functionName;
			$objAlert->body = "ALERT: Cron to create and send TMW file to NSD is OFF. See email for details.";
			#$resultAlert = Nsd_processController::sendAlert( $objAlert );
			
			$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
							
			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = "";
			$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
			$logObj->function_name = $functionName;   
			$logObj->log_level = "1";
			MetalakeHelperCore::logOBJ( $logTable, $logObj );

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);
		$logMessage .= "\n\r ";
		$logMessage .= "\n\r ";
		$logMessage .= "\n\r ";		
		$logMessage .= "\n\r ";
		$logMessage .= "\n\r ";		
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		
		
	}	



	public static function orderupdate_rules( $rule_table, $objRecord )
	{
		# old function name: applyTruckmateRule( $rule_table, $objRecord )


		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=applyTruckmateRule
		
		$className = "Nsd_processController";
		$functionName = "applyTruckmateRule";
		
		$db = JFactory::getDBO();
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}	

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objReturn = new stdClass();


		$query = "SELECT * FROM htc_nsd_orderupdate_truckmate_rules WHERE state = '1' AND rule_table = '".$rule_table."' ORDER BY id";
		$db->setQuery($query);
		$objListRules = $db->loadObjectList();				 

		$arrRules = array();

		foreach( $objListRules as $objRules )
		{
			$result = 0 ;
	
			
	
			switch( $objRules->rule_action )
			{
				case "begins":
				
					$strLengthRules = strlen( $objRules->rule );
					
					$strBegins = strtolower(substr($objRecord->description, 0, $strLengthRules));
				
					$strRules = strtolower($objRules->rule);
					
					if ($strRules == $strBegins )
					{
						$result = '1';
						
						$arrRules[] = $objRules;
					}
					
				
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r rule_table: ".print_r($rule_table, true);
					$logMessage .= "\n\r objRecord: ".print_r($objRecord, true);
					$logMessage .= "\n\r objRules: ".print_r($objRules, true);
					$logMessage .= "\n\r strLengthRules: ".print_r($strLengthRules, true);
					$logMessage .= "\n\r strBegins: ".print_r($strBegins, true);
					$logMessage .= "\n\r strRules: ".print_r($strRules, true);
					$logMessage .= "\n\r result: ".print_r($result, true);
					$logMessage .= "\n\r ";
					#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
				
					break;



				case "contains":
				
				
					if (stripos(strtolower($objRecord->description), $objRules->rule) !== false) 
					{
							$result = '1';
							
							$arrRules[] = $objRules;
					}
	
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r rule_table: ".print_r($rule_table, true);
					$logMessage .= "\n\r objRecord: ".print_r($objRecord, true);
					$logMessage .= "\n\r objRules: ".print_r($objRules, true);
					$logMessage .= "\n\r result: ".print_r($result, true);
					$logMessage .= "\n\r ";
					#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



				
					break;

/*
				case "ends":
				
					break
*/
				

				#default:	
				$rule_processing_description = "";
			}
		
			
			
		}		

		$objReturn->countArrRules = count( $arrRules );
		$objReturn->arrRules = $arrRules;
		
		switch( $objReturn->countArrRules )
		{
			case "0":
				$objReturn->rule_processing_description = "No rule identified.";
				break;

			case "1":
			
				$strMessage = "";
				$strMessage .= "Rule: ".$arrRules[0]->id." - ".strtoupper($arrRules[0]->rule_action)." ".$arrRules[0]->rule;				
				$strMessage .= ( $arrRules[0]->flag_ignore == '1' ) ? " :: Data not sent to Truckmate " : "" ;
			
				$objReturn->rule_processing_description = $strMessage;						
				
				break;
				
			default:

				#get list of confirmation rules
				$query = "select id from htc_nsd_orderupdate_truckmate_rules where flag_confirmation = 1 and state = 1";
				$db->setQuery($query);
				$arrListConfirmRules = $db->loadColumn();


				#determine if one rule is a confirmation rule.
				$exist = "0";
				foreach( $arrRules as $objRule  )
				{
	
					if ( in_array($objRule->id, $arrListConfirmRules ) )
					{
						$exist = "1";
					}
				}

				#if there is a confirmation rule, return only that rule
				if ( $exist == "1" )
				{
					$arrKey = 0;
					
					foreach( $arrRules as $objRule  )
					{
						if ( !in_array($objRule->id, $arrListConfirmRules ) )
						{
							unset($arrRules[$arrKey]);
							
							
						}
						
						$arrKey += 1;
					}				
					
					# if more than one confirmation rule, return only one				
					if ( count($arrRules) > 1 )
					{				
					
						$arrRules = array_slice($arrRules,0,1);
					
					}		
					
					$arrRules = array_values($arrRules);

					$objReturn->countArrRules = count( $arrRules );
					$objReturn->arrRules = $arrRules;
					
					$strMessage = "";
					$strMessage .= "Rule: ".$arrRules[0]->id." - ".strtoupper($arrRules[0]->rule_action)." ".$arrRules[0]->rule;				
					$strMessage .= ( $arrRules[0]->flag_ignore == '1' ) ? " :: Data not sent to Truckmate " : "" ;
				
					$objReturn->rule_processing_description = $strMessage;						

				}
				else
				{

					$strMessage = "";
					$strMessage .= "Meets more than one rule:  ";
					
					$count = 0;
					foreach( $arrRules as $rule )
					{
						
						$strMessage .= " [".($count+1)."] Rule: ".$arrRules[$count]->id." - ".strtoupper($arrRules[$count]->rule_action)." ".$arrRules[$count]->rule; 
						$count += 1;
					}
				
					$objReturn->rule_processing_description = $strMessage;
					
				}

			
				break;				

			
		}


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "END | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}	



	public static function orderupdate_order_history( )
	{
		# old function name: processOrderHistory()

		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=processOrderHistory
		
		$className = "Nsd_processController";
		$functionName = "processOrderHistory";
		
		$db = JFactory::getDBO();

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
		$cron_xml_dispatchtrack	= $params->get('cron_xml_dispatchtrack');
		$cron_process_xml		= $params->get('cron_process_xml');
		$cron_process_tmw_rules	= $params->get('cron_process_tmw_rules');
		$cron_send_tmw_files	= $params->get('cron_send_tmw_files');	

		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}	

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_start_micro = microtime(true);

		if (  $cron_process_tmw_rules == "1"  )
		{


			# get all records from order_history that have not been processed
			
			 
			#check against rules
			
			
			#if rule found
			#add to data_truckmate table
			#or if ignore, do not add to data_truckmate table
			#update order_histry table; flag_rule_processed=1; flag_rule_id
			
			
			#if no rule found
			#update order_histry table; flag_rule_processed=1; flag_rule_id = 0 (remains)
			#need to report these order_history records as having no rule 
	
			# get all records from order_history that have not been processed
			
			$query = "SELECT * FROM htc_nsd_orderupdate_data_dispatchtrack_order_history WHERE flag_rule_processed = '0' ORDER BY datetime, id";
			$db->setQuery($query);
			$objListRecords = $db->loadObjectList();				 
	
			$countRecords = 0;
	
			foreach( $objListRecords as $objRecord )
			{
				
				$flag_suppressed = "0";
				
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objRecord: ".print_r($objRecord, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				$objStatus = MetalakeHelperCore::getObjTableData( $table="htc_nsd_orderupdate_data_dispatchtrack_current_status", $field="order_number", $value = $objRecord->service_order_id );
				
				
				$rule_table = 'orderhistory';  # which rule_table 
				
				$objReturnRule = Nsd_processController::orderupdate_rules( $rule_table, $objRecord );
	
	
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objReturnRule: ".print_r($objReturnRule, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				switch( $objReturnRule->countArrRules )
				{
					
					case "1":
						
						
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r objReturnRule->arrRules[0]->id: ".print_r($objReturnRule->arrRules[0]->id, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
						
						#update order_histry table; flag_rule_processed=1; flag_rule_id
		                $objData = new stdClass();
		                $objData->id = $objRecord->id;
		                $objData->flag_rule_processed = 1;
		                $objData->flag_rule_id = $objReturnRule->arrRules[0]->id;
		                $objData->rule_processing_description = $objReturnRule->rule_processing_description;
		                $result = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_dispatchtrack_order_history', $objData, 'id');
		                
		                if ( $objReturnRule->arrRules[0]->flag_ignore == "0" )
		                {
	


							#ML0424 - We are no longer supressing NS codes.
							
												
								
							
							# for NS,  are there NS Scheduled records in htc_nsd_orderupdate_data_truckmate. yes-flag_suppressed=1; no-add flag_suppressed=0

/*
							if ( $objReturnRule->arrRules[0]->reason_code == 'NS' && $objRecord->order_status == 'Scheduled' )
							{

								$query = "SELECT * FROM htc_nsd_orderupdate_data_truckmate WHERE freight_bill = '".$objRecord->service_order_id."' AND reason_code = 'NS' AND order_status = 'Scheduled'";
								$db->setQuery($query);
								$objListNSRecords = $db->loadObjectList();
		
								$flag_suppressed = ( count( $objListNSRecords ) > 0 ) ? 1 : 0 ;
	
	
								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r query: ".print_r($query, true);
								$logMessage .= "\n\r objListNSRecords: ".print_r($objListNSRecords, true);
								$logMessage .= "\n\r count objListNSRecords: ".print_r(count( $objListNSRecords ), true);
								$logMessage .= "\n\r flag_suppressed: ".print_r($flag_suppressed, true);
								
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

							}
							else
							{
								$flag_suppressed = 0;
							}
*/


							#ML0424 - this is new
							if ( $objReturnRule->arrRules[0]->flag_confirmation == "1")
							{
								if( $objStatus->flag_confirmation == "0" )
								{
									#no suppression									
									$flag_suppressed = "0";
									
									#set flag_confirmation to 1; means it is confirmed
									$objUpdate = new stdClass();
									$objUpdate->id = $objStatus->id;
									$objUpdate->flag_confirmation = "1";
									$objUpdate->stamp_datetime = date("Y-m-d H:i:s");
									$objUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
									
									$resultUpdate = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_dispatchtrack_current_status', $objUpdate, 'id'); 

								}
								else
								{
									#suppress confirmation updates
									$flag_suppressed = "1";
								}
								
							}


							#ML0424




	
							#ML1013
							# force the order status to "Finished' if it passes rule #6; Rule: 6 - BEGINS Status changed from Started to Finished
							if ( $objReturnRule->arrRules[0]->id == "6")
							{
								$order_status = "Finished";
							}
							else
							{
								$order_status = $objRecord->order_status;
							}
	
	
							switch( $objRecord->agent_name  )
							{
								case "ISDL":
								
									$strSOID = $objRecord->service_order_id;
								
									if ( $strSOID[0] == "A" )
									{
										$freight_bill_export = substr( $objRecord->service_order_id, 1 );												
									}
									else
									{
										$freight_bill_export = substr( $objRecord->service_order_id, 2 );										
									}
								

									break;

								case "OTEX":
								
									$strSOID = $objRecord->service_order_id;
								
									if ( $strSOID[0] == "A" )
									{
										$freight_bill_export = substr( $objRecord->service_order_id, 1 );												
									}
									else
									{
										$freight_bill_export = $objRecord->service_order_id;
									}

									break;


								case "GULO":
								
									#check if orderid is a GULOCLT
									#yes - change agent id to GULOCLT and Agent Name to GULOCLT

										$query = "SELECT * FROM htc_nsd_orderentry_agents WHERE name = 'GULOCLT'";
										$db->setQuery($query);
										$objAgentGuloOrderEntry = $db->loadObject();

								
									$query = "SELECT * FROM htc_nsd_orderentry_data_clean WHERE agent_id = '".$objAgentGuloOrderEntry->id."' and service_order_id_clean = '".$objRecord->service_order_id."'";
									$db->setQuery($query);
									$objListGulo = $db->loadObjectList();

									
									if ( count($objListGulo) > 0 )
									{

										$query = "SELECT * FROM htc_nsd_orderupdate_agents WHERE name = 'GULOCLT'";
										$db->setQuery($query);
										$objAgentGulo = $db->loadObject();
									

										$objRecord->agent_name = $objAgentGulo->name;
										$objRecord->agent_id = $objAgentGulo->id;
										
									}
								
									$freight_bill_export = $objRecord->service_order_id;

									break;


								case "SHIP":
								
									$strSOID = $objRecord->service_order_id;
								
									$freight_bill_parts = explode("-", $strSOID);
								
									$freight_bill_export = $freight_bill_parts[0];

									break;
								

								default:
									$freight_bill_export = $objRecord->service_order_id;
									break;
							}	
	
							#ML1116
							# supress any record if the order has been marked finished
							
/*
							$query = "SELECT * FROM htc_nsd_orderupdate_data_truckmate WHERE freight_bill = '".$objRecord->service_order_id."' AND order_status = 'Finished' ";
							$db->setQuery($query);
							$objListNSRecords0 = $db->loadObjectList();
							
							$flag_suppressed = ( count( $objListNSRecords0 ) > 0 ) ? 1 : 0 ;
*/
	
	
							if ( $objReturnRule->arrRules[0]->status_code == 'NS' &&  $objReturnRule->arrRules[0]->reason_code == 'NS' && $order_status == 'In Transit' )
							{
								$flag_suppressed = '1';
							}
	
	
	
							$objData = new stdClass();
							$objData->agent_name = $objRecord->agent_name;
							$objData->freight_bill = $objRecord->service_order_id;
							$objData->freight_bill_export = $freight_bill_export;
							$objData->change_datetime = $objRecord->datetime;
							$objData->timezone = "ET";
							$objData->customer_city = $objRecord->customer_city;
							$objData->customer_state = $objRecord->customer_state;
							$objData->status_code = $objReturnRule->arrRules[0]->status_code;
							$objData->reason_code = $objReturnRule->arrRules[0]->reason_code;
							$objData->comment = $objRecord->description;
							$objData->stamp_datetime = date("Y-m-d H:i:s");
							$objData->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");	
							$objData->importfile = $objRecord->importfile;
							$objData->flag_processed = "0";		
							$objData->transfer_file = "";
							$objData->order_status = $order_status; 	#ML1013
							$objData->agent_id = $objRecord->agent_id; 		
							
							#ML2016
							$objData->custom_field_pod_name = $objRecord->custom_field_pod_name;
							
							#ML1010
							$objData->flag_suppressed = $flag_suppressed; 									
							#ML1010
							
							
							
							#ML081618
							
							$strStatusCode = $objReturnRule->arrRules[0]->status_code;
							
							if ( $strStatusCode == "NS" )
							{
								
								switch( $order_status )
								{

									case "Finished":
									$objData->truckmate_status = "D1";
										break;

									case "In Transit":
									$objData->truckmate_status = "OD";
										break;


									case "New":
									$objData->truckmate_status = "XB";
										break; 									
										  
									case "Scheduled":
									$objData->truckmate_status = "AG";
										break; 



									default:
									$objData->truckmate_status = "";
										break;


								}
								
							}
							else
							{
								$objData->truckmate_status = $strStatusCode;
							}
							
							
							
							
							$query = "SELECT 
							STR_TO_DATE( scheduled_at, '%Y-%m-%d %H:%i:%s') as 'scheduled_at', 
							DATE_ADD( scheduled_at, INTERVAL service_time MINUTE ) as 'scheduled_at_end', 
							STR_TO_DATE( time_window_start, '%Y-%m-%d %H:%i:%s') as 'time_window_start', 
							STR_TO_DATE( time_window_end, '%Y-%m-%d %H:%i:%s') as 'time_window_end', 
							STR_TO_DATE( started_at, '%Y-%m-%d %H:%i:%s') as 'started_at', 
							STR_TO_DATE( finished_at, '%Y-%m-%d %H:%i:%s') as 'finished_at',
							request_window_start_time,
							request_window_end_time 
							FROM htc_nsd_orderupdate_data_dispatchtrack WHERE service_order_id = '".$objRecord->service_order_id."'  ORDER BY id DESC LIMIT 1" ; 
							$db->setQuery($query);
					        $objDataDispatchtrack = $db->loadObject();							
							
							$objData->scheduled_at = $objDataDispatchtrack->scheduled_at;
							$objData->scheduled_at_end = $objDataDispatchtrack->scheduled_at_end;
							$objData->time_window_start = $objDataDispatchtrack->time_window_start;
							$objData->time_window_end = $objDataDispatchtrack->time_window_end;
							$objData->started_at = $objDataDispatchtrack->started_at;
							$objData->finished_at = $objDataDispatchtrack->finished_at;
							$objData->request_window_start_time = $objDataDispatchtrack->request_window_start_time;
							$objData->request_window_end_time = $objDataDispatchtrack->request_window_end_time;
							
													
										
							$result = $db->insertObject('htc_nsd_orderupdate_data_truckmate', $objData);
							$tID = $db->insertid();
							
							
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objRecord->agent_id;
							$logObj->notes = "Truckmate rule ".$objReturnRule->arrRules[0]->id." met, '".$objReturnRule->rule_processing_description."'. Data inserted into htc_nsd_orderupdate_data_truckmate. [id: ".$tID."]";
							$logObj->function_name = $functionName; 
							$logObj->log_level = "";  
							MetalakeHelperCore::logOBJ( $logTable, $logObj );						
							
						}
					
						if ( $objReturnRule->arrRules[0]->id > 0 ) { $countRecords += 1; }
					
						break;
	
	
					case "0":
	
		                $objData = new stdClass();
		                $objData->id = $objRecord->id;
		                $objData->flag_rule_processed = "1";
		                $objData->flag_rule_id = "0";
		                $objData->rule_processing_description = $objReturnRule->rule_processing_description;
		                $result = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_dispatchtrack_order_history', $objData, 'id');
					
						break;
	
	
					default:
	
		                $objData = new stdClass();
		                $objData->id = $objRecord->id;
		                $objData->flag_rule_processed = "1";
		                $objData->flag_rule_id = "0";
		                $objData->rule_processing_description = $objReturnRule->rule_processing_description;
		                $result = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_dispatchtrack_order_history', $objData, 'id');
					
						break;
					
				}
				
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objReturnRule: ".print_r($objReturnRule, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}
	
	
			if ( $countRecords > 0 )
			{
	
/*
				#this may be commented out in production
				$objAlert = new stdClass();
				$objAlert->recipients = array( 'lsawyer@metalake.com' );
				$objAlert->subject = "NSD MONITOR | ".$functionName;
				$objAlert->body = "There are ".$countRecords." records identified to be sent to NSD";
				$resultAlert = Nsd_processController::sendAlert( $objAlert );
*/
				
			}


		}
		else
		{

			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = $objAgent->id;
			$logObj->notes = "ALERT: Cron to process TMW rules and populate TMW table is OFF";
			$logObj->function_name = $functionName;   
			$logObj->log_level = "1";
			MetalakeHelperCore::logOBJ( $logTable, $logObj );	

			$objAlert = new stdClass();
			$objAlert->recipients = array( 'support@metalake.com' );
			$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
			$body = "";
			$body .= "<br> ALERT: Cron to process TMW rules and populate TMW table is OFF";
			$body .= "<br><br>";
			$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
			$body .= "<br><br>";			
			$body .= "<br> Steps to triage:";
			$body .= "<br><br>";
			$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
			$body .= "<br> Go to configuration of the API component.";
			$body .= "<br> Turn on cron for: 'Cron to process TMW rules and populate TMW table'";
			$body .= "<br><br>";
			$body .= "<br> Check that master cron for this function is running on server.";
			$body .= "<br> ";
			$objAlert->body = $body;
			#$resultAlert = Nsd_processController::sendAlert( $objAlert );

			
			$objAlert = new stdClass();
			$objAlert->recipients = array( '7039305383@txt.att.net' );
			$objAlert->subject = "NSD MONITOR | ".$functionName;
			$objAlert->body = "ALERT: Cron to process TMW rules and populate TMW table is OFF. See email for details.";
			#$resultAlert = Nsd_processController::sendAlert( $objAlert );

			$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
							
			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = $objAgent->id;
			$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
			$logObj->function_name = $functionName;   
			$logObj->log_level = "1";
			MetalakeHelperCore::logOBJ( $logTable, $logObj );

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		$logMessage .= "\n\r \n\r";	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public static function orderupdate_create_files( $objAgent )
	{
		# old function name: createTruckmateTransferFileAgent( $objAgent )

		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=createTruckmateTransferFileAgent
		
		$className = "Nsd_processController";
		$functionName = "createTruckmateTransferFileAgent";
	
		$db = JFactory::getDBO();
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}
		
		$return_ctf = 0;

		$logMessage = "START | ".$className." | ".$functionName;
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		$stamp_start_micro = microtime(true);



		/*
		EXPORT FIELDS

		agent_name
		freight_bill
		order_status
		status_code
		reason_code
		status_datetime  ( format: mm/dd/yyyy HH:ii:ss  default: 00/00/0000 00:00:00 )
		status_timezone
		status_city
		status_status
		comment
		scheduled_from  ( format: mm/dd/yyyy HH:ii:ss  default: 00/00/0000 00:00:00 )
		scheduled_to  ( format: mm/dd/yyyy HH:ii:ss  default: 00/00/0000 00:00:00 )
		window_from  ( format: mm/dd/yyyy HH:ii:ss  default: 00/00/0000 00:00:00 )
		window_to  ( format: mm/dd/yyyy HH:ii:ss  default: 00/00/0000 00:00:00 )
		actual_from  ( format: mm/dd/yyyy HH:ii:ss  default: 00/00/0000 00:00:00 )
		actual_to  ( format: mm/dd/yyyy HH:ii:ss  default: 00/00/0000 00:00:00 )
		pod_name ( default: NOVALUE )
		*/

	
		#CURRENT PRODUCTION
	
		$query = "SELECT 
		id,
		agent_name,
		freight_bill_export,
		order_status, 
		status_code,
		reason_code,		
		DATE_FORMAT( change_datetime, '%m/%d/%Y %H:%i:%s' ) as 'status_datetime',
		timezone as 'status_timezone',
		customer_city as 'status_city',
		customer_state as 'status_status',
		comment,
		DATE_FORMAT( scheduled_at, '%m/%d/%Y %H:%i:%s' ) as 'scheduled_at',
		DATE_FORMAT( scheduled_at_end, '%m/%d/%Y %H:%i:%s' ) as 'scheduled_at_end',
		DATE_FORMAT( time_window_start, '%m/%d/%Y %H:%i:%s' ) as 'time_window_start',
		DATE_FORMAT( time_window_end, '%m/%d/%Y %H:%i:%s' ) as 'time_window_end',
		DATE_FORMAT( started_at, '%m/%d/%Y %H:%i:%s' ) as 'started_at',
		DATE_FORMAT( finished_at, '%m/%d/%Y %H:%i:%s' ) as 'finished_at'
		FROM htc_nsd_orderupdate_data_truckmate
		WHERE flag_processed = '0' 
		AND flag_suppressed = '0'
		AND agent_id = '".$objAgent->id."'  
		order by freight_bill, id ";		
		
		$db->setQuery($query);
		$arrListRecords = $db->loadAssocList();
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r arrListRecords: ".print_r($arrListRecords, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		if ( count($arrListRecords) > 0 )
		{

			$return_ctf = 1;

			$filePath = JPATH_SITE."/nsdorderupdate/outbound/";  #new
	
			$fileName_rh = date("Ymdhis")."-".time()."-".$objAgent->id."-".$objAgent->dispatchtrack_code."-rhfile.csv";
			
			$fileInfo = $filePath.$fileName_rh;


			$apiRecord = "";
	
			$fp = fopen( $fileInfo, "a+" );
			fwrite($fp, $apiRecord);
			fclose($fp);
	
			$logMessage = "INSIDE | ".$className." | ".$functionName ." | Successful created transfer file: ".$fileName_rh;
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r filePath: ".print_r($filePath, true);
			$logMessage .= "\n\r fileName_rh: ".print_r($fileName_rh, true);
			$logMessage .= "\n\r fileInfo: ".print_r($fileInfo, true);
			$logMessage .= "\n\r apiRecord: ".print_r($apiRecord, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
			$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_created = date("Y-m-d H:i:s");
			$logObj->fileName = $fileName_rh;
			$logObj->agent_id = $objAgent->id;
			$returnID = MetalakeHelperCore::logOBJ( $logTable, $logObj );
	
	
			$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_created = date("Y-m-d H:i:s");
			$logObj->fileName = $fileName_rh;
			$logObj->description = "Create file: ".$fileName_rh;
			$logObj->agent_id = $objAgent->id;
			MetalakeHelperCore::logOBJ( $logTable, $logObj );
	


#no header record
/*
			#header record
			$strExport = array();
			$strExport['agent_name'] = 'agent_name';
			$strExport['freight_bill'] = 'freight_bill';
			$strExport['status_code'] = 'status_code';
			$strExport['reason_code'] = 'reason_code';
			$strExport['change_datetime'] = 'status_datetime';
			$strExport['timezone'] = 'status_timezone';
			$strExport['customer_city'] = 'status_city';
			$strExport['customer_state'] = 'status_state';
			$strExport['comment'] = 'comment';
			$fp = fopen( $fileInfo, "a+" );
			fputcsv( $fp, $strExport, "\t" );
			fclose( $fp );
*/
	
	
	
	
			$record_count = 0;
	
			foreach ( $arrListRecords as $arrRecord )
			{
	
	            $objData = new stdClass();
	            $objData->id = $arrRecord['id'];
	            $objData->flag_processed = "1";
	            $objData->transfer_file = $fileName_rh;
	            $result = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_truckmate', $objData, 'id');  			
	
				$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
				$logObj = new stdClass();
				$logObj->datetime = date("Y-m-d H:i:s");
				$logObj->fileName = $fileName_rh;
				$logObj->description = "Added data record id: ".$arrRecord['id']." to file: ".$fileName_rh;
				$logObj->agent_id = $objAgent->id;
				MetalakeHelperCore::logOBJ( $logTable, $logObj );	
	
				$logMessage = "INSIDE | ".$className." | ".$functionName ." | Added data record id: ".$arrRecord['id']." to file: ".$fileName_rh;
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
				unset($arrRecord['id']);
	
				$fp = fopen( $fileInfo, "a+" );
				fputcsv( $fp, $arrRecord, "\t" );
				fclose( $fp );
	
				$record_count += 1;

			}
	
	
			#UPDATE NUMBER OF RECORDS IN FILE
			$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
			$logObj = new stdClass();
			$logObj->id = $returnID;
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->record_count = $record_count;
			$logObj->fileName = $fileName_rh;
			$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');  

			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = "";
			$logObj->notes = "RH file, ".$fileName_rh.", was successfully created.  It has ".$record_count." records in it. ";
			$logObj->function_name = $functionName;  
			$logObj->log_level = ""; 
			$logObj->agentid = $objAgent->id;
			MetalakeHelperCore::logOBJ( $logTable, $logObj );	

			$logMessage = "INSIDE | ".$className." | ".$functionName ." | RH file, ".$fileName_rh.", was successfully created.  It has ".$record_count." records in it. ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			




			
			#ML081618
			
			# for new truckmate file
			


			$filePath = JPATH_SITE."/nsdorderupdate/outbound/";  #new
	
			$fileName_tmw = date("Ymdhis")."-".time()."-".$objAgent->id."-".$objAgent->dispatchtrack_code."-tmwfile.csv";
			
			$fileInfo = $filePath.$fileName_tmw;


			$apiRecord = "";
	
			$fp = fopen( $fileInfo, "a+" );
			fwrite($fp, $apiRecord);
			fclose($fp);
	
			$logMessage = "INSIDE | ".$className." | ".$functionName ." | Successful created transfer file: ".$fileName_tmw;
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r filePath: ".print_r($filePath, true);
			$logMessage .= "\n\r fileName_tm: ".print_r($fileName_tmw, true);
			$logMessage .= "\n\r fileInfo: ".print_r($fileInfo, true);
			$logMessage .= "\n\r apiRecord: ".print_r($apiRecord, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
			$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
			$logObj = new stdClass();
			$logObj->id = $returnID;
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_created = date("Y-m-d H:i:s");
			$logObj->fileName_tmw = $fileName_tmw;
			$logObj->agent_id = $objAgent->id;
			$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id'); 
			#$returnIDTMW = MetalakeHelperCore::logOBJ( $logTable, $logObj );
	
	
			$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_created = date("Y-m-d H:i:s");
			$logObj->fileName = $fileName_tmw;
			$logObj->description = "Create file: ".$fileName_tmw;
			$logObj->agent_id = $objAgent->id;
			MetalakeHelperCore::logOBJ( $logTable, $logObj );
	


#no header record
/*
			#header record
			$strExport = array();
			$strExport['agent_name'] = 'agent_name';
			$strExport['freight_bill'] = 'freight_bill';
			$strExport['status_code'] = 'status_code';
			$strExport['reason_code'] = 'reason_code';
			$strExport['change_datetime'] = 'status_datetime';
			$strExport['timezone'] = 'status_timezone';
			$strExport['customer_city'] = 'status_city';
			$strExport['customer_state'] = 'status_state';
			$strExport['comment'] = 'comment';
			$fp = fopen( $fileInfo, "a+" );
			fputcsv( $fp, $strExport, "\t" );
			fclose( $fp );
*/
	
	

		$query = "SELECT 
		id,
		agent_name,
		freight_bill_export,
		truckmate_status as 'order_status', 
		DATE_FORMAT( change_datetime, '%m/%d/%Y %H:%i:%s' ) as 'status_datetime',
		timezone as 'status_timezone',
		customer_city as 'status_city',
		customer_state as 'status_status',
		comment,
		DATE_FORMAT( scheduled_at, '%m/%d/%Y %H:%i:%s' ) as 'scheduled_at',
		DATE_FORMAT( scheduled_at_end, '%m/%d/%Y %H:%i:%s' ) as 'scheduled_at_end',
		DATE_FORMAT( time_window_start, '%m/%d/%Y %H:%i:%s' ) as 'time_window_start',
		DATE_FORMAT( time_window_end, '%m/%d/%Y %H:%i:%s' ) as 'time_window_end',
		DATE_FORMAT( started_at, '%m/%d/%Y %H:%i:%s' ) as 'started_at',
		DATE_FORMAT( finished_at, '%m/%d/%Y %H:%i:%s' ) as 'finished_at'
		FROM htc_nsd_orderupdate_data_truckmate
		WHERE flag_processed_tmw = '0' 
		AND flag_suppressed = '0'
		AND agent_id = '".$objAgent->id."'  
		order by freight_bill, id ";		
		
		$db->setQuery($query);
		$arrListRecordsTMW = $db->loadAssocList();
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r arrListRecordsTMW: ".print_r($arrListRecordsTMW, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


	
	
			$record_count = 0;
	
			foreach ( $arrListRecordsTMW as $arrRecord )
			{
	
	            $objData = new stdClass();
	            $objData->id = $arrRecord['id'];
	            $objData->flag_processed_tmw = "1";
	            $objData->transfer_file_tmw = $fileName_tmw;
	            $result = JFactory::getDbo()->updateObject('htc_nsd_orderupdate_data_truckmate', $objData, 'id');  			
	
				$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
				$logObj = new stdClass();
				$logObj->datetime = date("Y-m-d H:i:s");
				$logObj->fileName = $fileName_tmw;
				$logObj->description = "Added data record id: ".$arrRecord['id']." to file: ".$fileName_tmw;
				$logObj->agent_id = $objAgent->id;
				MetalakeHelperCore::logOBJ( $logTable, $logObj );	
	
				$logMessage = "INSIDE | ".$className." | ".$functionName ." | Added data record id: ".$arrRecord['id']." to file: ".$fileName_tmw;
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
				unset($arrRecord['id']);
	
				$fp = fopen( $fileInfo, "a+" );
				fputcsv( $fp, $arrRecord, "\t" );
				fclose( $fp );
	
				$record_count += 1;

			}
	
	
			#UPDATE NUMBER OF RECORDS IN FILE
			$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
			$logObj = new stdClass();
			$logObj->id = $returnID;
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->record_count = $record_count;
			$logObj->fileName_tmw = $fileName_tmw;
			$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');  

			$logTable = "htc_nsd_orderupdate_log_master";
			$logObj = new stdClass();
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->agentid = "";
			$logObj->notes = "TMW file, ".$fileName_tmw.", was successfully created.  It has ".$record_count." records in it. ";
			$logObj->function_name = $functionName;  
			$logObj->log_level = ""; 
			$logObj->agentid = $objAgent->id;
			MetalakeHelperCore::logOBJ( $logTable, $logObj );	

			$logMessage = "INSIDE | ".$className." | ".$functionName ." | TMW file, ".$fileName_tmw.", was successfully created.  It has ".$record_count." records in it. ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			
			
			
			
			
			
			
			
		}
		else
		{

			$return_ctf = 0;

			$logMessage = "INSIDE | ".$className." | ".$functionName. " | No records to process for agent [".$objAgent->id."] ".$objAgent->dispatchtrack_code.".";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
			
		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		return $return_ctf;

	}



	public static function orderupdate_send_files( $objAgent ) 
	{
		# old function name: NEW_sendTruckmateTransferFileAgentMultiFile( $objAgent )

		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=sendTruckmateTransferFileAgent
	
		# ssl ftp
		#https://secure.php.net/manual/en/function.ssh2-sftp.php
	
		$className = "Nsd_processController";
		$functionName = "sendTruckmateTransferFileAgentMultiFile";

		$db = JFactory::getDBO();
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}


		$logMessage = "START | ".$className." | ".$functionName;
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	


		$stamp_start_micro = microtime(true);

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objFTP: ".print_r($objFTP, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		#send TMW Files for Agent
		
		$query = "SELECT *
		FROM htc_nsd_orderupdate_truckmate_transfer_files
		WHERE datetime_sent_tmw = '0000-00-00 00:00:00'
		AND agent_id = '".$objAgent->id."' ";
		$db->setQuery($query);
		$objListFiles = $db->loadObjectList();
		
		
		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objListFiles: ".print_r($objListFiles, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		if ( count($objListFiles) > 0 )
		{

			$query = "SELECT *
			FROM htc_nsd_orderupdate_agents_ftp
			WHERE state = '1' 
			AND flag_rockhopper = '0'
			AND agent_id = '".$objAgent->id."' ";
			$db->setQuery($query);
			$objFTP = $db->loadObject();
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objFTP: ".print_r($objFTP, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				foreach( $objListFiles as $objFile )
				{
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r objFile: ".print_r($objFile, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

					$objFile->origin_path = JPATH_SITE."/nsdorderupdate/outbound/";		
					$objFile->processed_path = JPATH_SITE."/nsdorderupdate/outbound/processed/";


					$objFTP->server = $objFTP->ftp_server;		
					$objFTP->user_name = $objFTP->ftp_user;
					$objFTP->user_pass = $objFTP->ftp_pwd;
					$objFTP->port = $objFTP->ftp_port;
					$objFTP->ftp_use_passive_mode = $objFTP->ftp_use_passive_mode;
					$objFTP->destination_path = $objFTP->ftp_destination_path;
								


					if ( $objFTP->port == "22" )
					{
						$conn_id = ftp_ssl_connect($objFTP->server, $objFTP->port );									
					}
					else
					{
						$conn_id = ftp_connect($objFTP->server);					
					}
	
	
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r conn_id: ".print_r($conn_id, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
				
					$login_result = ftp_login($conn_id, $objFTP->user_name, $objFTP->user_pass);
					
					if ( $objFTP->ftp_use_passive_mode )
					{
						ftp_pasv($conn_id, true);
					}
					else
					{
						ftp_pasv($conn_id, false);
					}
					
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r login_result: ".print_r($login_result, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
					if ( $login_result )
					{
		
						$logMessage = "INSIDE | ".$className." | ".$functionName." | Successful FTP connection to: ".$objFTP->server;
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
						$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->fileName = $objFile->fileName_tmw;
						$logObj->description = "Successful FTP connection to: ".$objFTP->server;
						$logObj->agent_id = $objAgent->id;
						MetalakeHelperCore::logOBJ( $logTable, $logObj );	
						
						
						if ( ftp_put( $conn_id , $remote_file = $objFTP->destination_path.$objFile->fileName_tmw , $local_file = $objFile->origin_path.$objFile->fileName_tmw , FTP_ASCII ) )
						{
							
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_sent = date("Y-m-d H:i:s");
							$logObj->fileName = $objFile->fileName_tmw;
							$logObj->description = "Successful FTP transfer of: ".$objFile->fileName_tmw. " to: ".$objFTP->server." | ".$objFTP->user_name;
							$logObj->agent_id = $objAgent->id;
							MetalakeHelperCore::logOBJ( $logTable, $logObj );						
							
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
							$logObj = new stdClass();
							$logObj->id = $objFile->id;
							$logObj->datetime = date("Y-m-d H:i:s");
							if ($objFTP->flag_rockhopper == "1" )
							{
								$logObj->datetime_sent_rh = date("Y-m-d H:i:s");
							}
							else
							{
								$logObj->datetime_sent_tmw = date("Y-m-d H:i:s");
							}								
							$logObj->fileName_tmw = $objFile->fileName_tmw;
							$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');  
		
	
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objAgent->id;
							$logObj->notes = "Successfully transferred file, ".$objFile->fileName_tmw. " to: ".$objFTP->server." | ".$objFTP->user_name;
							$logObj->function_name = $functionName;  
							$logObj->log_level = ""; 
							MetalakeHelperCore::logOBJ( $logTable, $logObj );	
	
		
							$logMessage = "INSIDE | ".$className." | ".$functionName ." | Successful FTP transfer of: ".$objFile->fileName_tmw. " to: ".$objFTP->server." | ".$objFTP->user_name;
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
		
						}
						else
						{
	
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r conn_id: ".print_r($conn_id, true);
							$logMessage .= "\n\r remote_file: ".print_r($remote_file, true);
							$logMessage .= "\n\r local_file: ".print_r($local_file, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
		
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->fileName = $objFile->fileName_tmw;
							$logObj->description = "Unsuccessful FTP transfer of: ".$objFile->fileName_tmw. "to: ".$objFTP->server." | ".$objFTP->user_name;
							$logObj->agent_id = $objAgent->id;
							MetalakeHelperCore::logOBJ( $logTable, $logObj );						
							
		
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
							$logObj = new stdClass();
							$logObj->id = $objFile->id;
							$logObj->datetime = date("Y-m-d H:i:s");
							
							if ($objFTP->flag_rockhopper == "1" )
							{
								$logObj->datetime_sent_rh = "0000-00-00 00:00:00";
							}
							else
							{
								$logObj->datetime_sent_tmw = "0000-00-00 00:00:00";
							}	
							$logObj->fileName_tmw = $objFile->fileName_tmw;
							$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');  
		
							$logMessage = "INSIDE | ".$className." | ".$functionName." | Unsuccessful FTP transfer of: ".$objFile->fileName_tmw. " to: ".$objFTP->server." | ".$objFTP->user_name;
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objAgent->id;
							$logObj->notes = "ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName_tmw. " to: ".$objFTP->server." | ".$objFTP->user_name;
							$logObj->function_name = $functionName;  
							$logObj->log_level = "1"; 
							MetalakeHelperCore::logOBJ( $logTable, $logObj );	
	
	
							$objAlert = new stdClass();
							$objAlert->recipients = array( 'support@metalake.com' );
							$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
							$body = "";
							$body .= "<br> ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName_tmw. " to: ".$objFTP->server." | ".$objFTP->user_name;
							$body .= "<br><br>";
							$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
							$body .= "<br><br>";			
							$body .= "<br> Steps to triage:";
							$body .= "<br><br>";
							$body .= "<br> The system cannot reach nonstopdelivery to delivery TMW file.";
							$body .= "<br> Contact Scott Hendrickson at (c)703-848-5474 or shendrickson@nonstopdelivery.com.";
							$body .= "<br><br>";
							#$body .= "<br> You may want to shut down the cron, 'Cron to create and send TMW file to NSD'";
							#$body .= "<br> while this is being triaged.";
							#$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
							#$body .= "<br> Go to configuration of the API component.";
							#$body .= "<br> Turn off cron for: 'Cron to create and send TMW file to NSD'";
							#$body .= "<br> ";
							$body .= "<br> FTP service to ".$objFTP->server." | ".$objFTP->user." may be down or intermittent.";
							$body .= "<br> ";
							$objAlert->body = $body;
							#$resultAlert = Nsd_processController::sendAlert( $objAlert );
	
	
							$objAlert = new stdClass();
							$objAlert->recipients = array( '7039305383@txt.att.net' );
							$objAlert->subject = "NSD MONITOR | ".$functionName;
							$objAlert->body = "ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName_tmw." to ".$objFTP->server." | ".$objFTP->user_name." See email for details.";
							#$resultAlert = Nsd_processController::sendAlert( $objAlert );
							
							$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
											
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objAgent->id;
							$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
							$logObj->function_name = $functionName;   
							$logObj->log_level = "1";
							MetalakeHelperCore::logOBJ( $logTable, $logObj );
	
		
						}
		
		
						ftp_close($conn_id);
		
					}
					else
					{
						#no FTP connection
						
						$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->fileName = $objFile->fileName_tmw;
						$logObj->description = "Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
						$logObj->agent_id = $objAgent->id;
						MetalakeHelperCore::logOBJ( $logTable, $logObj );					
		
						$logMessage = "INSIDE | ".$className." | ".$functionName ." | Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user;
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
		
						$logTable = "htc_nsd_orderupdate_log_master";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
						$logObj->agentid = $objAgent->id;
						$logObj->notes = "ALERT: Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
						$logObj->function_name = $functionName; 
						$logObj->log_level = "1"; 
						MetalakeHelperCore::logOBJ( $logTable, $logObj );	
	
						$objAlert = new stdClass();
						$objAlert->recipients = array( 'support@metalake.com' );
						$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
						$body = "";
						$body .= "<br> ALERT: Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
						$body .= "<br><br>";
						$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
						$body .= "<br><br>";			
						$body .= "<br> Steps to triage:";
						$body .= "<br><br>";
						$body .= "<br> The system cannot reach nonstopdelivery to delivery TMW file.";
						$body .= "<br> Contact Scott Hendrickson at (c)703-848-5474 or shendrickson@nonstopdelivery.com.";
						$body .= "<br><br>";
						#$body .= "<br> You may want to shut down the cron, 'Cron to create and send TMW file to NSD'";
						#$body .= "<br> while this is being triaged.";
						#$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
						#$body .= "<br> Go to configuration of the API component.";
						#$body .= "<br> Turn off cron for: 'Cron to create and send TMW file to NSD'";
						#$body .= "<br> ";
						#$body .= "<br> <a href='http://process.nonstopdelivery.com/index.php?option=com_ml_api&task=triage_NSD_FTP' target=ftptriage'>Try and FTP to server.</a>";
						#$body .= "<br> ";
						#$body .= "<br> ";
						$body .= "<br> FTP service to ".$objFTP->server." | ".$objFTP->user." may be down or intermittent.";
						$body .= "<br> ";
						$objAlert->body = $body;
						#$resultAlert = Nsd_processController::sendAlert( $objAlert );
	
	
						$objAlert = new stdClass();
						$objAlert->recipients = array( '7039305383@txt.att.net' );
						$objAlert->subject = "NSD MONITOR | ".$functionName;
						$objAlert->body = "ALERT: Unsuccessful FTP connection to: ".$objFTP->server.". See email for details.";
						#$resultAlert = Nsd_processController::sendAlert( $objAlert );
	
						$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
										
						$logTable = "htc_nsd_orderupdate_log_master";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
						$logObj->agentid = $objAgent->id;
						$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
						$logObj->function_name = $functionName;   
						$logObj->log_level = "1";
						MetalakeHelperCore::logOBJ( $logTable, $logObj );
						
						ftp_close($conn_id);
					}

				}




		}
		
		
		
		
		#send RH Files for Agent


		$query = "SELECT *
		FROM htc_nsd_orderupdate_truckmate_transfer_files
		WHERE datetime_sent_rh = '0000-00-00 00:00:00'
		AND agent_id = '".$objAgent->id."' ";
		$db->setQuery($query);
		$objListFiles = $db->loadObjectList();
		
		
		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objListFiles: ".print_r($objListFiles, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		if ( count($objListFiles) > 0 )
		{

			$query = "SELECT *
			FROM htc_nsd_orderupdate_agents_ftp
			WHERE state = '1' 
			AND flag_rockhopper = '1'
			AND agent_id = '".$objAgent->id."' ";
			$db->setQuery($query);
			$objFTP = $db->loadObject();
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objFTP: ".print_r($objFTP, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				foreach( $objListFiles as $objFile )
				{
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r objFile: ".print_r($objFile, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

					$objFile->origin_path = JPATH_SITE."/nsdorderupdate/outbound/";		
					$objFile->processed_path = JPATH_SITE."/nsdorderupdate/outbound/processed/";


					$objFTP->server = $objFTP->ftp_server;		
					$objFTP->user_name = $objFTP->ftp_user;
					$objFTP->user_pass = $objFTP->ftp_pwd;
					$objFTP->port = $objFTP->ftp_port;
					$objFTP->ftp_use_passive_mode = $objFTP->ftp_use_passive_mode;
					$objFTP->destination_path = $objFTP->ftp_destination_path;
								


					if ( $objFTP->port == "22" )
					{
						$conn_id = ftp_ssl_connect($objFTP->server, $objFTP->port );									
					}
					else
					{
						$conn_id = ftp_connect($objFTP->server);					
					}
	
	
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r conn_id: ".print_r($conn_id, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
				
					$login_result = ftp_login($conn_id, $objFTP->user_name, $objFTP->user_pass);
					
					if ( $objFTP->ftp_use_passive_mode )
					{
						ftp_pasv($conn_id, true);
					}
					else
					{
						ftp_pasv($conn_id, false);
					}
					
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r login_result: ".print_r($login_result, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
					if ( $login_result )
					{
		
						$logMessage = "INSIDE | ".$className." | ".$functionName." | Successful FTP connection to: ".$objFTP->server;
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
						$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->fileName = $objFile->fileName;
						$logObj->description = "Successful FTP connection to: ".$objFTP->server;
						$logObj->agent_id = $objAgent->id;
						MetalakeHelperCore::logOBJ( $logTable, $logObj );	
						
						
						if ( ftp_put( $conn_id , $remote_file = $objFTP->destination_path.$objFile->fileName , $local_file = $objFile->origin_path.$objFile->fileName , FTP_ASCII ) )
						{
							
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_sent = date("Y-m-d H:i:s");
							$logObj->fileName = $objFile->fileName;
							$logObj->description = "Successful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
							$logObj->agent_id = $objAgent->id;
							MetalakeHelperCore::logOBJ( $logTable, $logObj );						
							
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
							$logObj = new stdClass();
							$logObj->id = $objFile->id;
							$logObj->datetime = date("Y-m-d H:i:s");
							if ($objFTP->flag_rockhopper == "1" )
							{
								$logObj->datetime_sent_rh = date("Y-m-d H:i:s");
							}
							else
							{
								$logObj->datetime_sent_tmw = date("Y-m-d H:i:s");
							}								
							$logObj->fileName = $objFile->fileName;
							$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');  
		
	
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objAgent->id;
							$logObj->notes = "Successfully transferred file, ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
							$logObj->function_name = $functionName;  
							$logObj->log_level = ""; 
							MetalakeHelperCore::logOBJ( $logTable, $logObj );	
	
		
							$logMessage = "INSIDE | ".$className." | ".$functionName ." | Successful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
		
						}
						else
						{
	
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r conn_id: ".print_r($conn_id, true);
							$logMessage .= "\n\r remote_file: ".print_r($remote_file, true);
							$logMessage .= "\n\r local_file: ".print_r($local_file, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
		
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->fileName = $objFile->fileName;
							$logObj->description = "Unsuccessful FTP transfer of: ".$objFile->fileName. "to: ".$objFTP->server." | ".$objFTP->user_name;
							$logObj->agent_id = $objAgent->id;
							MetalakeHelperCore::logOBJ( $logTable, $logObj );						
							
		
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
							$logObj = new stdClass();
							$logObj->id = $objFile->id;
							$logObj->datetime = date("Y-m-d H:i:s");
							
							if ($objFTP->flag_rockhopper == "1" )
							{
								$logObj->datetime_sent_rh = "0000-00-00 00:00:00";
							}
							else
							{
								$logObj->datetime_sent_tmw = "0000-00-00 00:00:00";
							}	
							$logObj->fileName = $objFile->fileName;
							$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');  
		
							$logMessage = "INSIDE | ".$className." | ".$functionName." | Unsuccessful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objAgent->id;
							$logObj->notes = "ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
							$logObj->function_name = $functionName;  
							$logObj->log_level = "1"; 
							MetalakeHelperCore::logOBJ( $logTable, $logObj );	
	
	
							$objAlert = new stdClass();
							$objAlert->recipients = array( 'support@metalake.com' );
							$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
							$body = "";
							$body .= "<br> ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
							$body .= "<br><br>";
							$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
							$body .= "<br><br>";			
							$body .= "<br> Steps to triage:";
							$body .= "<br><br>";
							$body .= "<br> The system cannot reach nonstopdelivery to delivery TMW file.";
							$body .= "<br> Contact Scott Hendrickson at (c)703-848-5474 or shendrickson@nonstopdelivery.com.";
							$body .= "<br><br>";
							#$body .= "<br> You may want to shut down the cron, 'Cron to create and send TMW file to NSD'";
							#$body .= "<br> while this is being triaged.";
							#$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
							#$body .= "<br> Go to configuration of the API component.";
							#$body .= "<br> Turn off cron for: 'Cron to create and send TMW file to NSD'";
							#$body .= "<br> ";
							$body .= "<br> FTP service to ".$objFTP->server." | ".$objFTP->user." may be down or intermittent.";
							$body .= "<br> ";
							$objAlert->body = $body;
							#$resultAlert = Nsd_processController::sendAlert( $objAlert );
	
	
							$objAlert = new stdClass();
							$objAlert->recipients = array( '7039305383@txt.att.net' );
							$objAlert->subject = "NSD MONITOR | ".$functionName;
							$objAlert->body = "ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName." to ".$objFTP->server." | ".$objFTP->user_name." See email for details.";
							#$resultAlert = Nsd_processController::sendAlert( $objAlert );
							
							$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
											
							$logTable = "htc_nsd_orderupdate_log_master";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->agentid = $objAgent->id;
							$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
							$logObj->function_name = $functionName;   
							$logObj->log_level = "1";
							MetalakeHelperCore::logOBJ( $logTable, $logObj );
	
		
						}
		
		
						ftp_close($conn_id);
		
					}
					else
					{
						#no FTP connection
						
						$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->fileName = $objFile->fileName;
						$logObj->description = "Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
						$logObj->agent_id = $objAgent->id;
						MetalakeHelperCore::logOBJ( $logTable, $logObj );					
		
						$logMessage = "INSIDE | ".$className." | ".$functionName ." | Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user;
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
		
						$logTable = "htc_nsd_orderupdate_log_master";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
						$logObj->agentid = $objAgent->id;
						$logObj->notes = "ALERT: Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
						$logObj->function_name = $functionName; 
						$logObj->log_level = "1"; 
						MetalakeHelperCore::logOBJ( $logTable, $logObj );	
	
						$objAlert = new stdClass();
						$objAlert->recipients = array( 'support@metalake.com' );
						$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
						$body = "";
						$body .= "<br> ALERT: Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
						$body .= "<br><br>";
						$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
						$body .= "<br><br>";			
						$body .= "<br> Steps to triage:";
						$body .= "<br><br>";
						$body .= "<br> The system cannot reach nonstopdelivery to delivery TMW file.";
						$body .= "<br> Contact Scott Hendrickson at (c)703-848-5474 or shendrickson@nonstopdelivery.com.";
						$body .= "<br><br>";
						#$body .= "<br> You may want to shut down the cron, 'Cron to create and send TMW file to NSD'";
						#$body .= "<br> while this is being triaged.";
						#$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
						#$body .= "<br> Go to configuration of the API component.";
						#$body .= "<br> Turn off cron for: 'Cron to create and send TMW file to NSD'";
						#$body .= "<br> ";
						#$body .= "<br> <a href='http://process.nonstopdelivery.com/index.php?option=com_ml_api&task=triage_NSD_FTP' target=ftptriage'>Try and FTP to server.</a>";
						#$body .= "<br> ";
						#$body .= "<br> ";
						$body .= "<br> FTP service to ".$objFTP->server." | ".$objFTP->user." may be down or intermittent.";
						$body .= "<br> ";
						$objAlert->body = $body;
						#$resultAlert = Nsd_processController::sendAlert( $objAlert );
	
	
						$objAlert = new stdClass();
						$objAlert->recipients = array( '7039305383@txt.att.net' );
						$objAlert->subject = "NSD MONITOR | ".$functionName;
						$objAlert->body = "ALERT: Unsuccessful FTP connection to: ".$objFTP->server.". See email for details.";
						#$resultAlert = Nsd_processController::sendAlert( $objAlert );
	
						$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
										
						$logTable = "htc_nsd_orderupdate_log_master";
						$logObj = new stdClass();
						$logObj->datetime = date("Y-m-d H:i:s");
						$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
						$logObj->agentid = $objAgent->id;
						$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
						$logObj->function_name = $functionName;   
						$logObj->log_level = "1";
						MetalakeHelperCore::logOBJ( $logTable, $logObj );
						
						ftp_close($conn_id);
					}

				}




		}



		if ( count($objListFiles) > 0 )
		{
			
			$query = "SELECT *
			FROM htc_nsd_orderupdate_truckmate_transfer_files
			WHERE id = '".$objFile->id."' ";
			$db->setQuery($query);
			$objTest = $db->loadObject();
	
			if ( $objTest->datetime_sent_tmw != "0000-00-00 00:00:00" )
			{
	
				#MOVE FILE TO PROCESSED
				if ( JFile::move( $objFile->origin_path.$objFile->fileName_tmw, $objFile->processed_path.$objFile->fileName_tmw ) )
				{
					$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
					$logObj = new stdClass();
					$logObj->datetime = date("Y-m-d H:i:s");
					$logObj->fileName = $objFile->fileName_tmw;
					$logObj->description = "Successful archive of: ".$objFile->fileName_tmw;
					$logObj->agent_id = $objAgent->id;
					MetalakeHelperCore::logOBJ( $logTable, $logObj );					
	
					$logMessage = "INSIDE | ".$className." | sendTransferFile | Successful archive of: ".$objFile->fileName_tmw;
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
					
				}
				else
				{
	
					$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
					$logObj = new stdClass();
					$logObj->datetime = date("Y-m-d H:i:s");
					$logObj->fileName = $objFile->fileName_tmw;
					$logObj->description = "Unsuccessful archive of ".$objFile->fileName_tmw;
					$logObj->agent_id = $objAgent->id;
					MetalakeHelperCore::logOBJ( $logTable, $logObj );					
	
					$logMessage = "INSIDE | ".$className." | ".$functionName ." | Unsuccessful archive of ".$objFile->fileName_tmw;
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
					
				}
				
			}
	
	
			if ( $objTest->datetime_sent_rh != "0000-00-00 00:00:00" )
			{
	
				#MOVE FILE TO PROCESSED
				if ( JFile::move( $objFile->origin_path.$objFile->fileName, $objFile->processed_path.$objFile->fileName ) )
				{
					$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
					$logObj = new stdClass();
					$logObj->datetime = date("Y-m-d H:i:s");
					$logObj->fileName = $objFile->fileName;
					$logObj->description = "Successful archive of: ".$objFile->fileName;
					$logObj->agent_id = $objAgent->id;
					MetalakeHelperCore::logOBJ( $logTable, $logObj );					
	
					$logMessage = "INSIDE | ".$className." | sendTransferFile | Successful archive of: ".$objFile->fileName;
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
					
				}
				else
				{
	
					$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
					$logObj = new stdClass();
					$logObj->datetime = date("Y-m-d H:i:s");
					$logObj->fileName = $objFile->fileName;
					$logObj->description = "Unsuccessful archive of ".$objFile->fileName;
					$logObj->agent_id = $objAgent->id;
					MetalakeHelperCore::logOBJ( $logTable, $logObj );					
	
					$logMessage = "INSIDE | ".$className." | ".$functionName ." | Unsuccessful archive of ".$objFile->fileName;
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
					
				}
				
			}


		}



		#old code
		
/*
		$query = "SELECT *
		FROM htc_nsd_orderupdate_truckmate_transfer_files
		WHERE (datetime_sent = '0000-00-00 00:00:00' OR datetime_sent_rh = '0000-00-00 00:00:00')
		AND agent_id = '".$objAgent->id."' ";
		$db->setQuery($query);
		$objListFiles = $db->loadObjectList();


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objListFiles: ".print_r($objListFiles, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		if ( count($objListFiles) > 0 )
		{

			$query = "SELECT *
			FROM htc_nsd_orderupdate_agents_ftp
			WHERE state = '1' 
			AND agent_id = '".$objAgent->id."' ";
			$db->setQuery($query);
			$objListAgentFTP = $db->loadObjectList();
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objListAgentFTP: ".print_r($objListAgentFTP, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				foreach( $objListFiles as $objFile )
				{
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r objFile: ".print_r($objFile, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

					$objFile->origin_path = JPATH_SITE."/nsddt/outbound/";		
					$objFile->processed_path = JPATH_SITE."/nsddt/outbound/processed/";
					
					#FTP
					
					foreach( $objListAgentFTP as $objFTP )
					{
		
						$objFTP->server = $objFTP->ftp_server;		
						$objFTP->user_name = $objFTP->ftp_user;
						$objFTP->user_pass = $objFTP->ftp_pwd;
						$objFTP->port = $objFTP->ftp_port;
						$objFTP->ftp_use_passive_mode = $objFTP->ftp_use_passive_mode;

						$objFTP->destination_path = $objFTP->ftp_destination_path;
									
					
						if ( ($objFTP->flag_rockhopper == "1" && $objFile->datetime_sent_rh == "0000-00-00 00:00:00") || ($objFTP->flag_rockhopper == "0" && $objFile->datetime_sent == "0000-00-00 00:00:00") )
						{


							if ( $objFTP->port == "22" )
							{
								$conn_id = ftp_ssl_connect($objFTP->server, $objFTP->port );									
							}
							else
							{
								$conn_id = ftp_connect($objFTP->server);					
							}
			
			
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r conn_id: ".print_r($conn_id, true);
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
						
							$login_result = ftp_login($conn_id, $objFTP->user_name, $objFTP->user_pass);
							
							if ( $objFTP->ftp_use_passive_mode )
							{
								ftp_pasv($conn_id, true);
							}
							else
							{
								ftp_pasv($conn_id, false);
							}
							
				
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r login_result: ".print_r($login_result, true);
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
						
							if ( $login_result )
							{
				
								$logMessage = "INSIDE | ".$className." | ".$functionName." | Successful FTP connection to: ".$objFTP->server;
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
				
								$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
								$logObj = new stdClass();
								$logObj->datetime = date("Y-m-d H:i:s");
								$logObj->fileName = $objFile->fileName;
								$logObj->description = "Successful FTP connection to: ".$objFTP->server;
								$logObj->agent_id = $objAgent->id;
								MetalakeHelperCore::logOBJ( $logTable, $logObj );	
								
								
								if ( ftp_put( $conn_id , $remote_file = $objFTP->destination_path.$objFile->fileName , $local_file = $objFile->origin_path.$objFile->fileName , FTP_ASCII ) )
								{
									
									$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
									$logObj = new stdClass();
									$logObj->datetime = date("Y-m-d H:i:s");
									$logObj->datetime_sent = date("Y-m-d H:i:s");
									$logObj->fileName = $objFile->fileName;
									$logObj->description = "Successful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
									$logObj->agent_id = $objAgent->id;
									MetalakeHelperCore::logOBJ( $logTable, $logObj );						
									
									$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
									$logObj = new stdClass();
									$logObj->id = $objFile->id;
									$logObj->datetime = date("Y-m-d H:i:s");
									if ($objFTP->flag_rockhopper == "1" )
									{
										$logObj->datetime_sent_rh = date("Y-m-d H:i:s");
									}
									else
									{
										$logObj->datetime_sent = date("Y-m-d H:i:s");
									}								
									$logObj->fileName = $objFile->fileName;
									$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');  
				
			
									$logTable = "htc_nsd_orderupdate_log_master";
									$logObj = new stdClass();
									$logObj->datetime = date("Y-m-d H:i:s");
									$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
									$logObj->agentid = $objAgent->id;
									$logObj->notes = "Successfully transferred file, ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
									$logObj->function_name = $functionName;  
									$logObj->log_level = ""; 
									MetalakeHelperCore::logOBJ( $logTable, $logObj );	
			
				
									$logMessage = "INSIDE | ".$className." | ".$functionName ." | Successful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
					
				
								}
								else
								{
			
									$logMessage = "INSIDE | ".$className." | ".$functionName;
									$logMessage .= "\n\r conn_id: ".print_r($conn_id, true);
									$logMessage .= "\n\r remote_file: ".print_r($remote_file, true);
									$logMessage .= "\n\r local_file: ".print_r($local_file, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			
				
									$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
									$logObj = new stdClass();
									$logObj->datetime = date("Y-m-d H:i:s");
									$logObj->fileName = $objFile->fileName;
									$logObj->description = "Unsuccessful FTP transfer of: ".$objFile->fileName. "to: ".$objFTP->server." | ".$objFTP->user_name;
									$logObj->agent_id = $objAgent->id;
									MetalakeHelperCore::logOBJ( $logTable, $logObj );						
									
				
									$logTable = "htc_nsd_orderupdate_truckmate_transfer_files";
									$logObj = new stdClass();
									$logObj->id = $objFile->id;
									$logObj->datetime = date("Y-m-d H:i:s");
									
									if ($objFTP->flag_rockhopper == "1" )
									{
										$logObj->datetime_sent_rh = "0000-00-00 00:00:00";
									}
									else
									{
										$logObj->datetime_sent = "0000-00-00 00:00:00";
									}	
									$logObj->fileName = $objFile->fileName;
									$result = JFactory::getDbo()->updateObject($logTable, $logObj, 'id');  
				
									$logMessage = "INSIDE | ".$className." | ".$functionName." | Unsuccessful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
				
				
									$logTable = "htc_nsd_orderupdate_log_master";
									$logObj = new stdClass();
									$logObj->datetime = date("Y-m-d H:i:s");
									$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
									$logObj->agentid = $objAgent->id;
									$logObj->notes = "ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
									$logObj->function_name = $functionName;  
									$logObj->log_level = "1"; 
									MetalakeHelperCore::logOBJ( $logTable, $logObj );	
			
			
									$objAlert = new stdClass();
									$objAlert->recipients = array( 'support@metalake.com' );
									$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
									$body = "";
									$body .= "<br> ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName. " to: ".$objFTP->server." | ".$objFTP->user_name;
									$body .= "<br><br>";
									$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
									$body .= "<br><br>";			
									$body .= "<br> Steps to triage:";
									$body .= "<br><br>";
									$body .= "<br> The system cannot reach nonstopdelivery to delivery TMW file.";
									$body .= "<br> Contact Scott Hendrickson at (c)703-848-5474 or shendrickson@nonstopdelivery.com.";
									$body .= "<br><br>";
									#$body .= "<br> You may want to shut down the cron, 'Cron to create and send TMW file to NSD'";
									#$body .= "<br> while this is being triaged.";
									#$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
									#$body .= "<br> Go to configuration of the API component.";
									#$body .= "<br> Turn off cron for: 'Cron to create and send TMW file to NSD'";
									#$body .= "<br> ";
									$body .= "<br> FTP service to ".$objFTP->server." | ".$objFTP->user." may be down or intermittent.";
									$body .= "<br> ";
									$objAlert->body = $body;
									#$resultAlert = Nsd_processController::sendAlert( $objAlert );
			
			
									$objAlert = new stdClass();
									$objAlert->recipients = array( '7039305383@txt.att.net' );
									$objAlert->subject = "NSD MONITOR | ".$functionName;
									$objAlert->body = "ALERT: Unsuccessful FTP transfer of: ".$objFile->fileName." to ".$objFTP->server." | ".$objFTP->user_name." See email for details.";
									$resultAlert = Nsd_processController::sendAlert( $objAlert );
									
									$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
													
									$logTable = "htc_nsd_orderupdate_log_master";
									$logObj = new stdClass();
									$logObj->datetime = date("Y-m-d H:i:s");
									$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
									$logObj->agentid = $objAgent->id;
									$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
									$logObj->function_name = $functionName;   
									$logObj->log_level = "1";
									MetalakeHelperCore::logOBJ( $logTable, $logObj );
			
				
								}
				
				
								ftp_close($conn_id);
				
							}
							else
							{
								#no FTP connection
								
								$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
								$logObj = new stdClass();
								$logObj->datetime = date("Y-m-d H:i:s");
								$logObj->fileName = $objFile->fileName;
								$logObj->description = "Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
								$logObj->agent_id = $objAgent->id;
								MetalakeHelperCore::logOBJ( $logTable, $logObj );					
				
								$logMessage = "INSIDE | ".$className." | ".$functionName ." | Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user;
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
				
								$logTable = "htc_nsd_orderupdate_log_master";
								$logObj = new stdClass();
								$logObj->datetime = date("Y-m-d H:i:s");
								$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
								$logObj->agentid = $objAgent->id;
								$logObj->notes = "ALERT: Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
								$logObj->function_name = $functionName; 
								$logObj->log_level = "1"; 
								MetalakeHelperCore::logOBJ( $logTable, $logObj );	
			
								$objAlert = new stdClass();
								$objAlert->recipients = array( 'support@metalake.com' );
								$objAlert->subject = "ALERT | NSD MONITOR | ".$functionName;
								$body = "";
								$body .= "<br> ALERT: Unsuccessful FTP connection to: ".$objFTP->server." | ".$objFTP->user_name;
								$body .= "<br><br>";
								$body .= "<br> Location: process.nonstopdelivery.com | ".$functionName;
								$body .= "<br><br>";			
								$body .= "<br> Steps to triage:";
								$body .= "<br><br>";
								$body .= "<br> The system cannot reach nonstopdelivery to delivery TMW file.";
								$body .= "<br> Contact Scott Hendrickson at (c)703-848-5474 or shendrickson@nonstopdelivery.com.";
								$body .= "<br><br>";
								#$body .= "<br> You may want to shut down the cron, 'Cron to create and send TMW file to NSD'";
								#$body .= "<br> while this is being triaged.";
								#$body .= "<br> Login to the <a href='http://process.nonstopdelivery.com/administrator/index.php?option=com_config&view=component&component=com_ml_api' target='adminpanel'>administrator panel</a>:   ";
								#$body .= "<br> Go to configuration of the API component.";
								#$body .= "<br> Turn off cron for: 'Cron to create and send TMW file to NSD'";
								#$body .= "<br> ";
								#$body .= "<br> <a href='http://process.nonstopdelivery.com/index.php?option=com_ml_api&task=triage_NSD_FTP' target=ftptriage'>Try and FTP to server.</a>";
								#$body .= "<br> ";
								#$body .= "<br> ";
								$body .= "<br> FTP service to ".$objFTP->server." | ".$objFTP->user." may be down or intermittent.";
								$body .= "<br> ";
								$objAlert->body = $body;
								#$resultAlert = Nsd_processController::sendAlert( $objAlert );
			
			
								$objAlert = new stdClass();
								$objAlert->recipients = array( '7039305383@txt.att.net' );
								$objAlert->subject = "NSD MONITOR | ".$functionName;
								$objAlert->body = "ALERT: Unsuccessful FTP connection to: ".$objFTP->server.". See email for details.";
								#$resultAlert = Nsd_processController::sendAlert( $objAlert );
			
								$strResultAlerts = ( !$resultAlert ) ? "NOT" : "" ;
												
								$logTable = "htc_nsd_orderupdate_log_master";
								$logObj = new stdClass();
								$logObj->datetime = date("Y-m-d H:i:s");
								$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
								$logObj->agentid = $objAgent->id;
								$logObj->notes = "ALERT: Alerts ".$strResultAlerts." sent to: ".implode(', ', $objAlert->recipients );
								$logObj->function_name = $functionName;   
								$logObj->log_level = "1";
								MetalakeHelperCore::logOBJ( $logTable, $logObj );
								
								ftp_close($conn_id);
							}


							
						}
						else
						{
							$logMessage = "INSIDE | ".$className." | ".$functionName." | file sent already: ".$objFile->fileName;
							#$logMessage .= "\n\r objFile: ".print_r($objFile, true);
							#$logMessage .= "\n\r objFTP: ".print_r($objFTP, true);
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }							
							
						}
					
	

		
					}
					


					$query = "SELECT *
					FROM htc_nsd_orderupdate_truckmate_transfer_files
					WHERE id = '".$objFile->id."' ";
					$db->setQuery($query);
					$objTest = $db->loadObject();

					if ( $objTest->datetime_sent != "0000-00-00 00:00:00" &&  $objTest->datetime_sent_rh != "0000-00-00 00:00:00" )
					{

						#MOVE FILE TO PROCESSED
						if ( JFile::move( $objFile->origin_path.$objFile->fileName, $objFile->processed_path.$objFile->fileName ) )
						{
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->fileName = $objFile->fileName;
							$logObj->description = "Successful archive of: ".$objFile->fileName;
							$logObj->agent_id = $objAgent->id;
							MetalakeHelperCore::logOBJ( $logTable, $logObj );					
	
							$logMessage = "INSIDE | ".$className." | sendTransferFile | Successful archive of: ".$objFile->fileName;
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
							
						}
						else
						{
			
							$logTable = "htc_nsd_orderupdate_truckmate_transfer_log";
							$logObj = new stdClass();
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->fileName = $objFile->fileName;
							$logObj->description = "Unsuccessful archive of ".$objFile->fileName;
							$logObj->agent_id = $objAgent->id;
							MetalakeHelperCore::logOBJ( $logTable, $logObj );					
	
							$logMessage = "INSIDE | ".$className." | ".$functionName ." | Unsuccessful archive of ".$objFile->fileName;
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
							
						}



						
					}

					
					
				}

				
			


			
		}
		else
		{
			$logMessage = "INSIDE | ".$className." | ".$functionName ." | No file to transfer to agent [".$objAgent->id."]";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
		}
*/




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	}


	public static function match_all($needles, $haystack)
	{

		$className = "Nsd_processController";
		$functionName = "match_all";


		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "orderupdate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "orderupdate_prod";
	
		}

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r needles: ".print_r($needles, true);
		$logMessage .= "\n\r haystack: ".print_r($haystack, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



	    if(empty($needles)){

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r FALSE: ".print_r("FALSE", true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	        return false;
	    }
	
	    foreach($needles as $needle) {
	        if (strpos($haystack, $needle) === false) {

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r FALSE: ".print_r("FALSE", true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


	            return false;
	        }
	    }
		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r TRUE: ".print_r("TRUE", true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	    return true;
	}



// !TEST FUNCTIONS

	public function test_cron_dispatchtrack_update( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_process&task=test_cron_dispatchtrack_update



		$className = "Nsd_processController";
		$functionName = "test_cron_dispatchtrack_update";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		Nsd_processController::cron_dispatchtrack_update();


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


	public function test_sendSendgrid( )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_process&task=test_sendSendgrid



		$className = "Nsd_processController";
		$functionName = "test_sendSendgrid";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objSendEmailMessage = new stdClass();
		$objSendEmailMessage->email_to = "lsawyer@metalake.com";
		$objSendEmailMessage->email_from = "rfeldman@metalake.com";
		$objSendEmailMessage->email_subject = "test subject";
		$objSendEmailMessage->email_body = "test body<br>test body";

		$objReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objSendEmailMessage: ".print_r($objSendEmailMessage, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




// !FUNCTION TEMPLATE

	public function test_function_template( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_process&task=test_function_template



		$className = "Nsd_processController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "process_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "process_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


}
