<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_process
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Nsd_process', JPATH_COMPONENT);
JLoader::register('Nsd_processController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Nsd_process');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
