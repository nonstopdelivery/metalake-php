<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_fileupload
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_nsd_fileupload', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/media/com_nsd_fileupload/js/form.js');

$user    = JFactory::getUser();
$canEdit = Nsd_fileuploadHelpersNsd_fileupload::canUserEdit($this->item, $user);

$showObject = false;


?>
<script type="text/javascript">

	jQuery(function() { 
	   
		 jQuery("#mmoUploadLink").click(function(e) {    

			e.preventDefault();

			var msgSuccess = "";
			var msgError = "";


			jQuery('#mmoUploadLink').addClass('disabled');


			// show this message when we begin the process
			var msgUploadBegin = "<li>Uploading file to server.</li>"

			jQuery(".the-return").html(msgUploadBegin);

				var url = 'index.php?option=com_nsd_fileupload&task=mmoUpload';
	
				jQuery.ajax({ 
					type: "GET", 
					url: url,
					data: {
						//notifyPhone: notifyPhone
						},
					success: function(dataReturn) {

							var dataResponse = JSON.parse(dataReturn);
	
							if ( dataResponse.error == "0" )
							{
								
								
								var msgSuccess = "<li>Upload successful. Processing file.</li>"
								
								jQuery(".the-return").append(msgSuccess);

								var url = 'index.php?option=com_nsd_fileupload&task=mmoExecute';
								jQuery.ajax({ 
									type: "GET", 
									url: url,
									data: {
										//notifyPhone: notifyPhone
										},
									success: function(dataReturn) {
											//alert("SUCCESS:");
											//alert(dataReturn);

											var dataResponse = JSON.parse(dataReturn);
					
											if ( dataResponse.error == "0" )
											{

												// Show this message when the entire process is complete
												var msgSuccess = "<li>File processed successfully.</li>"
												jQuery(".the-return").append(msgSuccess);									
												
											}
											else
											{
												
												// Show this message when there is an error is the MMO validation process.  Returns a objReturnFromWorkato with table.
												if ( dataResponse.errorTable )
												{
													var msgError = "<li class='error'>The file contains errors. Please update and upload again.<br>"+dataResponse.errorTable+"</li>";
												}
												else
												{
													var msgError = "<li class='error'>The file contains errors. Please update and upload again.<br>"+dataResponse.note+"</li>";
												}
												jQuery(".the-return").append(msgError);
												
											}
									}
									});
							}	
							else
							{
								// Show this message when the upload process has any failures
								var msgError = "<li class='error'>Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: <a href='https://help.shipnsd.com' target='new'>https://help.shipnsd.com</a></li>"
								jQuery(".the-return").append(msgError);
							}
						},
					error:function(dataReturn) {
							//alert("server error occured"); 
							var msgServerError = "<li class='error'> Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: <a href='https://help.shipnsd.com' target='new'>https://help.shipnsd.com</a></li>"
							jQuery(".the-return").html(msgServerError);
	    				}
					});

					return false;
	        });
	 }); 

</script>

<?php




$display = "";

$display .= "<h2>MMO File Upload</h2><br />";

$arrFormSession = (array) $this->objFormSession;
if ( count($arrFormSession) == 0 || $this->objFormSession->reset == "1" )
{
	

	$display .= "<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='post' name='adminForm' id='adminForm' enctype='multipart/form-data' class='form-inline track' role='form'>";

	$display .= "	<div class='input-group'>";
	$display .= "		<label class='input-group-btn'>";
	$display .= "			<span class='btn btn-primary' id='button-browse'>";
	$display .= "				Choose File: <input type='file' style='display:none;' name='jform[fileUpload]' id='jform_fileUpload' accept='text/csv' multiple>";
	$display .= "			</span>";
	$display .= "		</label>";
	$display .= "		<input type='text' class='form-control' id='file-input-name' readonly>";
	$display .= "	</div>";

	$display .= "	<input class='form-control btn' type='Submit' value='Go' />";
	$display .= "	<input type='hidden' name='option' value='com_nsd_fileupload' />";
	$display .= "	<input type='hidden' name='task' value='fileUpload' />";
	$display .= 	JHTML::_( 'form.token' );
	
	$display .= "</form>";

}
else
{

	if ( $this->objFormSession->error == "0" )
	{
		
		$display .= "Below is a sample of records contained in the file you provided. Please review for accuracy. When ready, click <b>Process This File</b> to proceed.<br /><br />";
		$display .= "<table class='table table-bordered'>\n\r";


		$display .= "<thead><tr>";
		
		foreach( $this->objFormSession->arrColumnHeaders as $header )
		{
			$display .= "<th>".$header."</th>";
		}
		
		$display .= "</tr></thead>";		



		$display .= "<tbody>\n\r";	

		$rowCount = 0;
		foreach(  $this->objFormSession->objListFileData as $dataRow )
		{
			if ( $rowCount > 0 && $rowCount <= $this->objFormSession->numRecordsDisplay )
			{
				$display .= "<tr>";		
				
				foreach( $this->objFormSession->arrColumnHeaders as $propertyName )
				{
					$display .= "<td>".$dataRow->$propertyName."</td>";
				}				
				
				$display .= "</tr>";		
			}
			$rowCount += 1;
		}

		$display .= "</tbody>\n\r";


		$display .= "</table>\n\r";	
		
		$display .= "<br />";

		$reset_link = JRoute::_('index.php?option=com_nsd_fileupload&view=mmoform&id=1&Itemid=782');		
		$display .= "<a class='btn' href='#' id='mmoUploadLink'>Process This File</a> ";
		$display .= "<a class='btn secondary' href='".$reset_link."'>Upload New File</a>";


		$display .= "<br /><br />";
		$display .= "<ul class='the-return'><ul>";
		
	}
	else
	{
		$display .= "<h3>Error</h3>";
		$display .= $this->objFormSession->error_message;

		$display .= "<br />";

		$reset_link = JRoute::_('index.php?option=com_nsd_fileupload&view=mmoform&id=1&Itemid=782');		
		$display .= "<a class='btn secondary' href='".$reset_link."'>Upload New File</a>";
		
	}
	
}


if ($showObject) :
	$display .= "<br /><br /><h3>objFormSession</h3><pre style='text-align:left'>".print_r($this->objFormSession, true)."</pre>";	
endif;

echo $display;
?>

<script type="text/javascript">
	document.getElementById('jform_fileUpload').addEventListener('change', function() {
		document.getElementById('file-input-name').value = this.value.replace("C:\\fakepath\\", "");
	});
</script>

