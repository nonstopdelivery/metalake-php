<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_fileupload
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_nsd_fileupload', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/media/com_nsd_fileupload/js/form.js');

$user    = JFactory::getUser();
$canEdit = Nsd_fileuploadHelpersNsd_fileupload::canUserEdit($this->item, $user);

$showObject = false;


?>
<script type="text/javascript">

	jQuery(function() { 
	   
		 jQuery("#slotUploadLink").submit(function(e) {    

			e.preventDefault();

			var msgSuccess = "";
			var msgError = "";

			// show this message when we begin the process
			var msgUploadBegin = "<li>Uploading file to server.</li>"

			jQuery(".the-return").html(msgUploadBegin);

				var url = 'index.php?option=com_nsd_fileupload&task=slotFileUpload';
	
				alert("a");
	
			    // Get form
		        //var form = $('#adminForm')[0];

				alert("b");		
				//alert(form);
		
				// Create an FormData object 
		        var data = new FormData(this);

				//data.append("jform", jform_fileUpload);
	
				alert(data);
	
				jQuery.ajax({ 
					type: "POST", 
					url: url,
					enctype: 'multipart/form-data',
					data: data,
		            processData: false,
		            contentType: false,
		            cache: false,
		            timeout: 600000,					
					success: function(dataReturn) {
						alert("SUCCESS:");
						alert(dataReturn);
						//alert("a");

						var dataResponse = JSON.parse(dataReturn);

						if ( dataResponse.error == "0" )
						{
							
							var msgSuccess = "<li>Upload successful.</li>"
							
							jQuery(".the-return").append(msgSuccess);

						}	
						else
						{
							// Show this message when the upload process has any failures
							var msgError = "<li class='error'>Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: <a href='https://shipnsd.atlassian.net/servicedesk' target='new'>https://shipnsd.atlassian.net/servicedesk</a></li>"
							jQuery(".the-return").append(msgError);
						}
					},
					error:function(dataReturn) {
							//alert("server error occured"); 
							var msgServerError = "<li class='error'> Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: <a href='https://shipnsd.atlassian.net/servicedesk' target='new'>https://shipnsd.atlassian.net/servicedesk</a></li>"
							jQuery(".the-return").html(msgServerError);
	    			}
				});
				return false;
	        });
	 }); 

</script>

<?php




$display = "";

$display .= "<h2>Slot File Upload</h2><br />";


	$display .= "<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='post' name='adminForm' id='adminForm' enctype='multipart/form-data' class='form-inline track' role='form'>";

	$display .= "	<div class='input-group'>";
	$display .= "		<label class='input-group-btn'>";
	$display .= "			<span class='btn btn-primary' id='button-browse'>";
	$display .= "				Choose File: <input type='file' style='display:none;' name='jform[fileUpload]' id='jform_fileUpload' accept='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' multiple>";
	$display .= "			</span>";
	$display .= "		</label>";
	$display .= "		<input type='text' class='form-control' id='file-input-name' readonly>";
	$display .= "	</div>";

	$display .= "	<input class='form-control btn' type='Submit' value='Go' />";
	$display .= "	<input type='hidden' name='option' value='com_nsd_fileupload' />";
	$display .= "	<input type='hidden' name='task' value='slotFileUpload' />";
	
	#$display .= "	<a class='btn' type='Submit' id='slotUploadLink'>goAjax</a> ";
	
	$display .= 	JHTML::_( 'form.token' );
	
	$display .= "</form>";


	$display .= "<br /><br />";
	$display .= "<ul class='the-return'><ul>";


	switch( $this->objFormSessionSlot->error )
	{
		case "0":
			$display .= "The file, ".$this->objFormSessionSlot->fileInformation->fileUploadName.", has been successfully uploaded to the server<br /><br />";
			break;

		case "1":
			$display .= "<h3>Error</h3>";
			$display .= $this->objFormSessionSlot->error_message;
			$display .= "<br />";
			break;
		
		default:
		
			$display .= "";
			break;

	}

	
if ($showObject) :
	$display .= "<br /><br /><h3>objFormSessionSlot</h3><pre style='text-align:left'>".print_r($this->objFormSessionSlot, true)."</pre>";	
endif;

echo $display;
?>

<script type="text/javascript">
	document.getElementById('jform_fileUpload').addEventListener('change', function() {
		document.getElementById('file-input-name').value = this.value.replace("C:\\fakepath\\", "");
	});
</script>

