<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_fileupload
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');

/**
 * Class Nsd_fileuploadController
 *
 * @since  1.6
 */
class Nsd_fileuploadController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'tmmos');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}



// !MMO FUNCTIONS

	public function fileUpload( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=fileUpload



		$className = "Nsd_fileuploadController";
		$functionName = "fileUpload";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

        $app           			= JFactory::getApplication();
        $params         		= $app->getParams();
		$itemid_mmoform			= $params->get('itemid_mmoform');
		$maxNumberOfDataRecords	= $params->get('max_data_records');
		$numRecordsDisplay		= $params->get('num_records_display');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$postData = JRequest::get('post');


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r postData; ".print_r($postData, true)."\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objFormSession = MetalakeHelperCore::getSessObj( "objFormSession" );
		
		$objForm = new stdClass();
		foreach($postData as $key => $value)
		{		
			
			if ( is_string($value) )
			{
				$objForm->$key = trim($value);
			}
		}


		$objForm->numRecordsDisplay = $numRecordsDisplay;
		
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$arrFile = $_FILES;


		$objFile 						= new stdClass();

		$objFile->fileUploadName 		= 	$arrFile['jform']['name']['fileUpload'];
		$objFile->fileUploadType 		= 	$arrFile['jform']['type']['fileUpload'];
		$objFile->fileUploadTmpName		= 	$arrFile['jform']['tmp_name']['fileUpload'];
		$objFile->fileUploadError 		= 	$arrFile['jform']['error']['fileUpload'];
		$objFile->fileUploadSize 		= 	$arrFile['jform']['size']['fileUpload'];

		$arrayName = explode('.', $objFile->fileUploadName);
		
		$objFile->fileUploadExtension = strtolower( end($arrayName) );

		

		$objForm->fileInformation = $objFile;
		


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		#$logMessage .= "\n\r _FILES: ".print_r($_FILES, true);
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$csvData = file_get_contents($objFile->fileUploadTmpName);
		$lines = explode(PHP_EOL, $csvData);
		
		$arrData = array();
		
		foreach ($lines as $line) {
		    $arrData[] = str_getcsv($line);
		}


		$arrCount = count($arrData);

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r arrCount: ".print_r($arrCount, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$objListFileData = array();

		$count = 0;

		foreach( $arrData as $data )
		{
			$objData = new stdClass();

			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r data: ".print_r($data, true);
			#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
			
			if ( $count == 0 )
			{
				
				$arrColumnHeaders = $data;

				$objForm->arrColumnHeaders = $arrColumnHeaders;

				$logMessage = "INSIDE | ".$className." | ".$functionName;	
				$logMessage .= "\n\r arrColumnHeaders: ".print_r($arrColumnHeaders, true);
				#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				
				
			}
			


			for( $x=0; $x < count( $data ); $x++ )
			{
			
				$colhead = $arrColumnHeaders[$x];
			
				$logMessage = "INSIDE | ".$className." | ".$functionName;	
				$logMessage .= "\n\r colhead: ".print_r($colhead, true);
				#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			
				$objData->$colhead = $data[$x];				
				
			}
			
		
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r objData: ".print_r($objData, true);
			#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			$objListFileData[] = $objData;	
			
			$count += 1;
			
		}





		$objForm->objListFileData = $objListFileData;
		

		$objForm->error = 0;
		$objForm->error_message = "";
		
		#VALIDATION
		
		#File is not empty
		if ( $objForm->fileInformation->fileUploadSize == "0")
		{
			
			$objForm->error = 1;
			$objForm->error_message = "File is empty.";			
			
		}



		if ( $objForm->error == 0 &&  $objForm->fileInformation->fileUploadType != "text/csv" )
		{

			$objForm->error = 1;
			$objForm->error_message = "File needs to be a comma separated text file (.csv).";	
			
			
			if ( $objForm->fileInformation->fileUploadType == "application/vnd.ms-excel" &&  $objForm->fileInformation->fileUploadExtension == "csv" )
			{
				$objForm->error = 0;
				$objForm->error_message = "";
				
			}
			
			
		}



	
		#File headers are properly named and in correct order


	    $arrMasterColumnHeader = array();
		$arrMasterColumnHeader[] = "FREIGHT_BILL_NUMBER";
		$arrMasterColumnHeader[] = "CARRIER_NAME";
		$arrMasterColumnHeader[] = "CONSOLIDATION_PRO";
		$arrMasterColumnHeader[] = "ETA";
		$arrMasterColumnHeader[] = "COST";
		$arrMasterColumnHeader[] = "DETAIL_LINE_ID";





		if ( $objForm->error == 0 && $arrColumnHeaders !== $arrMasterColumnHeader )
		{

			$objForm->error = 1;
			$objForm->error_message = "Header row columns are incorrect.  The columns should be:<br>".implode('<br>', $arrMasterColumnHeader);	
			
		}
		



		
		#File does not exceed maximum number of records

		#$maxNumberOfDataRecords = 999;

		if ( $objForm->error == 0 &&  count( $objForm->objListFileData ) - 1 > $maxNumberOfDataRecords )
		{
			
			$objForm->error = 1;
			$objForm->error_message = "File exceeds maximum number of records [".$maxNumberOfDataRecords."].";			
			
		}






								
		if ( $objForm->error == 0 )
		{								

			#validate that each record row is not empty
			
			$logMessage = "INSIDE | ".$className." | ".$functionName." | BEACON | validate that each record row is not empty";	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			
			$arrErrorMessage = array();
			$arrayIndexValue = 0;
			$countDataRecord = 1;
			foreach( $objForm->objListFileData as $objDataRecord )
			{
				
				$countEmpty = 0;
				foreach( $arrMasterColumnHeader as $header )
				{
					$propertyTest = !property_exists($objDataRecord, $header);
	
					$logMessage = "INSIDE | ".$className." | ".$functionName;	
					$logMessage .= "\n\r objDataRecord: ".print_r($objDataRecord, true);
					$logMessage .= "\n\r header: ".print_r($header, true);
					$logMessage .= "\n\r propertyTest: ".print_r($propertyTest, true);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
					if ( $propertyTest || ( !$propertyTest && $objDataRecord->$header == "" ) )
					{
						#$arrErrorMessage[] = "Record #".$countDataRecord." is missing data in ".$header." column.";
						$countEmpty += 1;
					}
	
				}
	
				if ( $countEmpty == count($arrMasterColumnHeader) ) 
				{
					$arrErrorMessage[] = "Record #".$countDataRecord." is empty.";
					
					unset(  $objForm->objListFileData[$arrayIndexValue] );

					$logMessage = "INSIDE | ".$className." | ".$functionName." |  unset record #".$countDataRecord;	
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					
				}
	
				
				$arrayIndexValue += 1;
				$countDataRecord += 1;
			}


			$objForm->objListFileData = array_values($objForm->objListFileData);

			
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r arrErrorMessage: ".print_r($arrErrorMessage, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		




			
			
/*
			if ( $objForm->error == 0 && count( $arrErrorMessage ) > 0 )
			{
				$objForm->error = 1;
				$objForm->error_message = implode( "<br />", $arrErrorMessage );				
			}
*/



			
		}



		#File has at least one data record

		if ( $objForm->error == 0 &&  count( $objForm->objListFileData ) == 1 )
		{
			
			$objForm->error = 1;
			$objForm->error_message = "File does not contain any data records.";			
			
		}

		
		
/*
		if ( $objForm->error == 0 )
		{
		
			#data validation
			$logMessage = "INSIDE | ".$className." | ".$functionName." | BEACON | data validation";	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			
			$arrErrorMessageValidation = array();
			$countDataRecord = 1;
			foreach( $objForm->objListFileData as $objDataRecord )
			{
				if ( $countDataRecord > 1 )
				{
					foreach( $arrMasterColumnHeader as $header )
					{
		
						if ( $header == "ETA" )
						{
							
							$isValidDate = Nsd_fileuploadController::validateDate($objDataRecord->$header, $format = 'n/j/Y');
		
							if( !$isValidDate )
							{
								$arrErrorMessageValidation[] = "Record #".$countDataRecord." | field: ".$header." | date is invalid, ".$objDataRecord->$header;						
							}					
							
						}
		
						$logMessage = "INSIDE | ".$className." | ".$functionName;	
						$logMessage .= "\n\r objDataRecord: ".print_r($objDataRecord, true);
						$logMessage .= "\n\r header: ".print_r($header, true);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
					}
					
				}
	
			
				$countDataRecord += 1;
			}
	
			$logMessage = "INSIDE | ".$className." | ".$functionName." | BEACON 2";	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
	
			
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r arrErrorMessage: ".print_r($arrErrorMessage, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		
			
			
			if ( $objForm->error == 0 && count( $arrErrorMessageValidation ) > 0 )
			{
				$objForm->error = 1;
				$objForm->error_message = implode( "<br />", $arrErrorMessageValidation );				
			}		
		
		
		}
*/
		
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		#$logMessage .= "\n\r arrFileData: ".print_r($arrFileData, true);
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		MetalakeHelperCore::setSessObj( "objFormSession", $objForm );
		
		#$objFormSession = MetalakeHelperCore::getSessObj( "objFormSession" );



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		#$link = JRoute::_('index.php?option=com_nsd_scheduling&view=complete&Itemid='.$itemid_complete);		
		$link = JRoute::_('index.php?option=com_nsd_fileupload&view=mmoform&Itemid='.$itemid_mmoform);		

		$this->setRedirect($link, $msg);
	}




	public static function mmoUpload()
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=mmoUpload



		$className = "Nsd_fileuploadController";
		$functionName = "mmoUpload";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

        $app           			= JFactory::getApplication();
        $params         		= $app->getParams();
		$itemid_mmoform			= $params->get('itemid_mmoform');
		$maxNumberOfDataRecords	= $params->get('max_data_records');
		$numRecordsDisplay		= $params->get('num_records_display');




		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		ob_start();


		$objFormSession = MetalakeHelperCore::getSessObj( "objFormSession" );


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objFormSession: ".print_r($objFormSession, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$arrOfRecords = $objFormSession->objListFileData;

		#remove the header record form array
		array_shift( $arrOfRecords );


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r arrOfRecords: ".print_r($arrOfRecords, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$countMMOInsertErrors = 0;
		
		foreach( $arrOfRecords as $objData )
		{
			

			
			# call workato function to insert into staging table
			
			$rtnWorkato = Nsd_workatoController::insertMMOStaging( $objData );


			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objData: ".print_r($objData, true);
			$logMessage .= "\n\r rtnWorkato: ".print_r($rtnWorkato, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			
			#figure out return, error or not, possibly count them to continue
			if ( $rtnWorkato->error == "0" && $rtnWorkato->objReturnFromWorkato->result == "ok" )
			{
				$countMMOInsertErrors += 0;
			}
			else
			{
				$countMMOInsertErrors += 1;
			}
		
			
		}


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r countMMOInsertErrors: ".print_r($countMMOInsertErrors, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			
		$objReturn = new stdClass();
		
		
/*
		#uncomment to continue without errors
		
		$countMMOInsertErrors = 0;
*/
		
		
		
		if ( $countMMOInsertErrors == 0 )
		{
	
			$objReturn->error = "0";
			$objReturn->error_code = "200";
			$objReturn->error_message = "";
			$objReturn->system_message = "SUCCESS";
			$objReturn->note = "";			
			
		}
		else
		{
	
			$objReturn->error = "1";
			$objReturn->error_code = "422";
			$objReturn->error_message = "";
			$objReturn->system_message = "MMO upload failure";
			$objReturn->note = "";			
			
		}



		$jsonReturn = json_encode($objReturn);

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		ob_end_clean();
		
		echo $jsonReturn;

		JFactory::getApplication()->close();
	}





	public static function mmoExecute()
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=mmoUpload



		$className = "Nsd_fileuploadController";
		$functionName = "mmoExecute";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

        $app           			= JFactory::getApplication();
        $params         		= $app->getParams();
		$itemid_mmoform			= $params->get('itemid_mmoform');
		$maxNumberOfDataRecords	= $params->get('max_data_records');
		$numRecordsDisplay		= $params->get('num_records_display');




		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		ob_start();


		$objReturn = Nsd_workatoController::executeMMOValidation();			



		#build table for display error information
		if ( property_exists( $objReturn->objReturnFromWorkato, "arrErrors" ) )
		{
	
	
			if ( count($objReturn->objReturnFromWorkato->arrErrors) > 0  )
			{

				$errorTable .= "";
				$errorTable .= "<table class='table records'><thead><tr><th>Freight Bill</th><th>Error</th></tr></thead><tbody>";
				foreach( $objReturn->objReturnFromWorkato->arrErrors as $rowError )
				{
					
					$errorTable .= "<tr><td>".$rowError->freight_bill."</td><td>".$rowError->error."</td></tr>";				
					
				}
				$errorTable .= "</tbody></table>";
		
				$objReturn->errorTable = $errorTable;
				
			}
	

		}



/*
		#uncomment to show success

		$objReturn->error = "0";
		$objReturn->error_code = "";
		$objReturn->error_message = "";
		$objReturn->system_message = "SUCCESS";
		$objReturn->note = "The request is valid and successful.";
		$objReturn->objReturnFromWorkato = "";
*/


		$jsonReturn = json_encode($objReturn);

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		ob_end_clean();
		
		echo $jsonReturn;

		JFactory::getApplication()->close();
	}





// !SLOT FILE FUNCTIONS


	public function slotFileUpload( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=slotFileUpload



		$className = "Nsd_fileuploadController";
		$functionName = "slotFileUpload";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

        $app           			= JFactory::getApplication();
        $params         		= $app->getParams();
		$itemid_slotform		= $params->get('itemid_slotform');
		$directory_inbound		= $params->get('directory_inbound');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$postData = JRequest::get('post');


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r postData; ".print_r($postData, true)."\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objFormSession = MetalakeHelperCore::getSessObj( "objFormSessionSlot" );
		
		$objForm = new stdClass();
		foreach($postData as $key => $value)
		{		
			
			if ( is_string($value) )
			{
				$objForm->$key = trim($value);
			}
		}


		$objForm->numRecordsDisplay = $numRecordsDisplay;
		
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$arrFile = $_FILES;


		$objFile 						= new stdClass();

		$objFile->fileUploadName 		= 	$arrFile['jform']['name']['fileUpload'];
		$objFile->fileUploadType 		= 	$arrFile['jform']['type']['fileUpload'];
		$objFile->fileUploadTmpName		= 	$arrFile['jform']['tmp_name']['fileUpload'];
		$objFile->fileUploadError 		= 	$arrFile['jform']['error']['fileUpload'];
		$objFile->fileUploadSize 		= 	$arrFile['jform']['size']['fileUpload'];
		$objFile->cleanFileName 		= 	preg_replace("/[^A-Za-z0-9.]/i", "-", $objFile->fileUploadName);

		$objForm->fileInformation = $objFile;
		


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		#$logMessage .= "\n\r _FILES: ".print_r($_FILES, true);
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$csvData = file_get_contents($objFile->fileUploadTmpName);

		$objForm->error = 0;
		$objForm->error_message = "";
		
		#VALIDATION
		
		#File is not empty
		if ( $objForm->fileInformation->fileUploadSize == "0")
		{
			
			$objForm->error = 1;
			$objForm->error_message = "File is empty.";			
			
		}
		else
		{
			$fileUploadPath = JPATH_ROOT."/".$directory_inbound."/".$objFile->cleanFileName;


			if(!JFile::upload($objFile->fileUploadTmpName, $fileUploadPath, false, true)) 
			{
	
				$logMessage = "INSIDE | ".$className." | ".$functionName." | error moving file";	
				$logMessage .= "\n\r objFile: ".print_r($objFile, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				
				$objForm->error = 1;
				$objForm->error_message = "The file, ".$objFile->fileUploadName." did not upload to the server.";	
	
			}
			else
			{
	
				$logMessage = "INSIDE | ".$className." | ".$functionName." | success moving file";	
				$logMessage .= "\n\r objFile: ".print_r($objFile, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
				$objForm->error = 0;
				$objForm->error_message = "The file, ".$objFile->fileUploadName.", has been successfully uploaded to the server<br /><br />";	
	
	
			}
			
		}

		




		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		#$logMessage .= "\n\r arrFileData: ".print_r($arrFileData, true);
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		MetalakeHelperCore::setSessObj( "objFormSessionSlot", $objForm );
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		#$link = JRoute::_('index.php?option=com_nsd_scheduling&view=complete&Itemid='.$itemid_complete);		
		$link = JRoute::_('index.php?option=com_nsd_fileupload&view=slotform&Itemid='.$itemid_slotform);		

		$this->setRedirect($link, $msg);
		
		
		
/*
			ob_start();
			$objReturn = new stdClass();
			$objReturn->error = "0";
			$objReturn->error_code = "200";
			$objReturn->error_message = "";
			$objReturn->system_message = "SUCCESS";
			$objReturn->note = "";	
			$jsonReturn = json_encode($objReturn);
		
			ob_end_clean();
		
			echo $jsonReturn;

			JFactory::getApplication()->close();
*/
	}





// !COMMON FUNCTIONS


	public static function validateDate($date, $format = 'm/d/Y')
	{

		$className = "Nsd_fileuploadController";
		$functionName = "test_validate_date";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




	    $d = DateTime::createFromFormat($format, $date);
	    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.

		$return = $d && $d->format($format) === $date;



		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r date: ".print_r($date, true);
		$logMessage .= "\n\r format: ".print_r($format, true);
		$logMessage .= "\n\r d: ".print_r($d, true);
		$logMessage .= "\n\r d->format(format): ".print_r($d->format($format), true);
		$logMessage .= "\n\r return: ".print_r($return, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


	    return $return;
	}


// !TEST FUNCTIONS

	public function testFileUpload( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=testFileUpload



		$className = "Nsd_fileuploadController";
		$functionName = "testFileUpload";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$postData = JRequest::get('post');


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r postData; ".print_r($postData, true)."\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objFormSession = MetalakeHelperCore::getSessObj( "objFormSession" );
		
		$objForm = new stdClass();
		foreach($postData as $key => $value)
		{		
			
			if ( is_string($value) )
			{
				$objForm->$key = trim($value);
			}
		}

/*
		if ( $objForm->reset == "1" )
		{

			MetalakeHelperCore::clearSessObj( "objFormSession" );
			$objFormSession = MetalakeHelperCore::getSessObj( "objFormSession" );
			$objFormSession->reset = 1;
		}
*/



		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$arrFile = $_FILES;


		$objFile 						= new stdClass();

		$objFile->fileUploadName 		= 	$arrFile['jform']['name']['fileUpload'];
		$objFile->fileUploadType 		= 	$arrFile['jform']['type']['fileUpload'];
		$objFile->fileUploadTmpName		= 	$arrFile['jform']['tmp_name']['fileUpload'];
		$objFile->fileUploadError 		= 	$arrFile['jform']['error']['fileUpload'];
		$objFile->fileUploadSize 		= 	$arrFile['jform']['size']['fileUpload'];
		

		$objForm->fileInformation = $objFile;
		


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		#$logMessage .= "\n\r _FILES: ".print_r($_FILES, true);
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$csvData = file_get_contents($objFile->fileUploadTmpName);
		$lines = explode(PHP_EOL, $csvData);
		
		$arrData = array();
		
		foreach ($lines as $line) {
		    $arrData[] = str_getcsv($line);
		}


		$arrCount = count($arrData);

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r arrCount: ".print_r($arrCount, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$objListFileData = array();

		$count = 0;

		foreach( $arrData as $data )
		{
			$objData = new stdClass();

			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r data: ".print_r($data, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
			
			if ( $count == 0 )
			{
				
				$arrColumnHeaders = $data;

				$objForm->arrColumnHeaders = $arrColumnHeaders;

				$logMessage = "INSIDE | ".$className." | ".$functionName;	
				$logMessage .= "\n\r arrColumnHeaders: ".print_r($arrColumnHeaders, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				
				
			}
			


			for( $x=0; $x < count( $data ); $x++ )
			{
			
				$colhead = $arrColumnHeaders[$x];
			
				$logMessage = "INSIDE | ".$className." | ".$functionName;	
				$logMessage .= "\n\r colhead: ".print_r($colhead, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			
				$objData->$colhead = $data[$x];				
				
			}
			
		
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r objData: ".print_r($objData, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			$objListFileData[] = $objData;	
			
			$count += 1;
			
		}





		$objForm->objListFileData = $objListFileData;
		
		
		
		#VALIDATION
		
		
		
		
		
		
		$objForm->error = 0;

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		#$logMessage .= "\n\r arrFileData: ".print_r($arrFileData, true);
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		MetalakeHelperCore::setSessObj( "objFormSession", $objForm );
		
		#$objFormSession = MetalakeHelperCore::getSessObj( "objFormSession" );



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		#$link = JRoute::_('index.php?option=com_nsd_scheduling&view=complete&Itemid='.$itemid_complete);		
		$link = JRoute::_('index.php?option=com_nsd_fileupload&view=mmoform&Itemid=782');		

		$this->setRedirect($link, $msg);
	}


	public function test_pdo( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=test_pdo



		$className = "Nsd_fileuploadController";
		$functionName = "test_pdo";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		$return = JDatabaseDriverPdo::isSupported() ;

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r return: ".print_r($return, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public function test_validate_date( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=test_validate_date



		$className = "Nsd_fileuploadController";
		$functionName = "test_validate_date";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$testDate = "3/24/1965";

		$isValidDate = Nsd_fileuploadController::validateDate($testDate, $format = 'n/j/Y');

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r testDate: ".print_r($testDate, true);
		$logMessage .= "\n\r isValidDate: ".print_r($isValidDate, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_ajax( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=test_ajax



		$className = "Nsd_fileuploadController";
		$functionName = "test_ajax";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		ob_start();

		$objReturn = new stdClass();

		$objReturn->error = "0";
		$objReturn->error_code = "200";
		$objReturn->error_message = "";
		$objReturn->system_message = "SUCCESS";
		$objReturn->note = "success</a>.";


/*
		$objReturn->error = "1";
		$objReturn->error_code = "500";
		$objReturn->error_message = "SERVER UNAVAILABLE";
		$objReturn->system_message = "SERVER UNAVAILABLE";
		$objReturn->note = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: <a href='https://shipnsd.atlassian.net/servicedesk'>https://shipnsd.atlassian.net/servicedesk</a>.";
*/




		$jsonReturn = json_encode($objReturn);


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		ob_end_clean();

		echo $jsonReturn;
		
		JFactory::getApplication()->close();		
	}


// !FUNCTION TEMPLATE

	public function test_function_template( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_fileupload&task=test_function_template



		$className = "Nsd_fileuploadController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "fileupload_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "fileupload_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}





}
