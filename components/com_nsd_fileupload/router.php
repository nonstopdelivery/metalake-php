<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_fileupload
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JLoader::registerPrefix('Nsd_fileupload', JPATH_SITE . '/components/com_nsd_fileupload/');

/**
 * Class Nsd_fileuploadRouter
 *
 * @since  3.3
 */
class Nsd_fileuploadRouter extends JComponentRouterBase
{
	/**
	 * Build method for URLs
	 * This method is meant to transform the query parameters into a more human
	 * readable form. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$query  An array of URL arguments
	 *
	 * @return  array  The URL arguments to use to assemble the subsequent URL.
	 *
	 * @since   3.3
	 */
	public function build(&$query)
	{
		$segments = array();
		$view     = null;

		if (isset($query['task']))
		{
			$taskParts  = explode('.', $query['task']);
			$segments[] = implode('/', $taskParts);
			$view       = $taskParts[0];
			unset($query['task']);
		}

		if (isset($query['view']))
		{
			$segments[] = $query['view'];
			$view = $query['view'];
			
			unset($query['view']);
		}

		if (isset($query['id']))
		{
			if ($view !== null)
			{
				$segments[] = $query['id'];
			}
			else
			{
				$segments[] = $query['id'];
			}

			unset($query['id']);
		}

		return $segments;
	}

	/**
	 * Parse method for URLs
	 * This method is meant to transform the human readable URL back into
	 * query parameters. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$segments  The segments of the URL to parse.
	 *
	 * @return  array  The URL attributes to be used by the application.
	 *
	 * @since   3.3
	 */
	public function parse(&$segments)
	{

		$vars = array();
		switch($segments[0])
		{

		       case 'slotform':
						$vars['view'] = 'slotform';
						$vars['id'] = $segments[1];
		                break;





		       default:
						$vars['view'] = 'mmoform';
						$vars['id'] = $segments[1];		                
						break;




		
		}
		
		#echo $q = print_r($vars, true);
		
		return $vars;


/*
		$vars = array();

		// View is always the first element of the array
		$vars['view'] = array_shift($segments);
		$model        = Nsd_fileuploadHelpersNsd_fileupload::getModel($vars['view']);

		while (!empty($segments))
		{
			$segment = array_pop($segments);

			// If it's the ID, let's put on the request
			if (is_numeric($segment))
			{
				$vars['id'] = $segment;
			}
			else
			{
				$vars['task'] = $vars['view'] . '.' . $segment;
			}
		}

		return $vars;
*/
	}
}
