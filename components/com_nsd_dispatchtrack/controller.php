<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_dispatchtrack
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');

/**
 * Class Nsd_dispatchtrackController
 *
 * @since  1.6
 */
class Nsd_dispatchtrackController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'agents');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
	




// !FUNCTIONS


	public static function update_dispatchtrack_order( $objUpdate )  
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_dispatchtrack&task=update_dispatchtrack_order

		$controllerName = "Nsd_dispatchtrackController";
		$functionName = "update_dispatchtrack_order";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objUpdate: ".print_r($objUpdate, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		if ( count($objUpdate) > 0 )
		{
	
	
			$objAgent = MetalakeHelperCore::getObjTableData( $table="htc_dispatchtrack_agents", $field="id", $value=$objUpdate->agent_id );
	
			
			$xmlBody = new SimpleXMLExtended('<?xml version="1.0" encoding="utf-8"?><service_order></service_order>');
		
			#$account_val = ( $objAgent->id == "1"  ) ? htmlspecialchars($objRecord->client_account_name) : $objAgent->dispatchtrack_account ; 

			$xmlBody->addChild('number', $objUpdate->service_order_id);
			
			$xmlBody->addChild('account', $objUpdate->account);
			$xmlBody->addChild('service_type', $objUpdate->service_delivery_type);
			
			#$xmlBody->addChildWithCDATA('description', '');


			$customer = $xmlBody->addChild('customer');

			#$customerInfo = $customer->addChild('customer_id');
			#$customerInfo = $customer->addChildWithCDATA('first_name', $objRecord->destination_name);
			#$customerInfo = $customer->addChild('first_name', "");
			
			$customerInfo = $customer->addChildWithCDATA('last_name', $objUpdate->destination_name);
			
			#$customerInfo = $customer->addChild('email', '');
			
			$customerInfo = $customer->addChild('phone1', $objUpdate->phone_1);
			$customerInfo = $customer->addChild('phone2', $objUpdate->phone_2);
			#$customerInfo = $customer->addChild('phone3', $objRecord->phone_3);
			$customerInfo = $customer->addChild('address1', $objUpdate->destination_address_1);		
			#$customerInfo = $customer->addChild('address2', $objRecord->destination_address_2);		
			$customerInfo = $customer->addChild('city', $objUpdate->destination_city);		
			$customerInfo = $customer->addChild('state', $objUpdate->destination_state);		
			$customerInfo = $customer->addChild('zip', $objUpdate->destination_zip);		

			$xmlBody->addChild('request_delivery_date', $objUpdate->request_delivery_date_val);
			$xmlBody->addChild('request_time_window_start', $objUpdate->request_time_window_start_val);
			$xmlBody->addChild('request_time_window_end', $objUpdate->request_time_window_end_val);
	
	
			$dom = new DOMDocument("1.0");
			$dom->preserveWhiteSpace = FALSE;
			$dom->loadXML($xmlBody->asXML());
			$dom->formatOutput = TRUE;
			#echo "<pre>".htmlentities($dom->saveXML())."</pre>";
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r xmlBody: ".print_r($xmlBody, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
			$urltopost = "https://".$objAgent->dispatchtrack_api_code.".dispatchtrack.com/orders/api/add_order";
	
			$domXML = $dom->saveXML();
	
	
			$datatopost	= array (
			  "code"	=> $objAgent->dispatchtrack_api_code,
			  "api_key"	=> $objAgent->dispatchtrack_api_key,
			  "data"	=> $domXML
			  );		
	
			if ( $objAgent->dispatchtrack_account != "" )
			{
				$datatopost["account"] = $objAgent->dispatchtrack_account;
			}
	
		
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r urltopost: ".print_r($urltopost, true);
			$logMessage .= "\n\r datatopost: ".print_r($datatopost, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
			$ch = curl_init ($urltopost);
			
			
			curl_setopt ($ch, CURLOPT_POST, true);
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $datatopost);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$returnData = curl_exec ($ch);
		
			$error = curl_error($ch);
		
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r returnData: ".print_r($returnData, true);
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
			if ( substr($returnData, 0, 5) == "<?xml" )
			{
		
				$xml = simplexml_load_string( $returnData );
				
				$strReturn = (string)$xml;
				
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r returnData: ".print_r($returnData, true);
				$logMessage .= "\n\r xml: ".print_r($xml, true);
				$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
				switch( $strReturn  )
				{
					
					case "Imported given orders!":
	
		
			
						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
			
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Processed and Sent: ".$objUpdate->service_order_id." | seconds: ".number_format($stamp_total_micro,2);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
						$return = 1;						
	
						
						$objReturn->error = '0';
						$objReturn->error_code = '';
						$objReturn->error_message = '';
						
						
						
						break;
	
	
	
	
	
	
					case "HTTP 500: Internal Server Error":
	
						$local_time = date("Y-m-d H:i:s");							
						$gmt_time = gmdate("Y-m-d H:i:s");
	
		                $objData = new stdClass();
		                $objData->id = $objRecord->id;
		                $objData->processed_datetime = $local_time;
		                $objData->processed_datetime_gmt = $gmt_time;
		                $objData->transfer_datetime = "0000-00-00 00:00:00";
		                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
		                $objData->flag_error = 1;
		                $objData->process_note = $strReturn;				                
	
		                #$result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 
	
						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
						$logMessage .= "\n\r Alert sent";
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	

						$body = "";
						$body .= "<br>The order below failed to import into Dispatchtrack during the order entry process.";
						$body .= "<br><br>";
						$body .= "<br><br>This order will have to be entered into Dispatchtrack manually.";
						$body .= "<br><br>";
						$body .= "<pre>".htmlentities($domXML)."<pre>";
						$body .= "<br><br>";
						$body .= "<br>Error Message: ".$strReturn."<br>";
						$body .= "<br><br>The 500 Internal Server Error means that there may be information that is missing or malformed.  Dispatchtrack does not provide any additional error messaging when this occurs.";
	
	
						$objSendEmailMessage = new stdClass();
						$objSendEmailMessage->email_to = "lsawyer@metalake.com";
						$objSendEmailMessage->email_from = "nsdalert@metalake.com";
						$objSendEmailMessage->email_subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
						$objSendEmailMessage->email_body = $body;
						$objSendEmailMessage->email_category = "scheduling support";
				
						$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );	
	
/*
						#send email with order details
						$objAlert = new stdClass();
						$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
						#$objAlert->recipients = array( 'lsawyer@metalake.com' );
						$objAlert->subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
						$body = "";
						$body .= "<br>The order below failed to import into Dispatchtrack during the order entry process.";
						$body .= "<br><br>";
						$body .= "<br><br>This order will have to be entered into Dispatchtrack manually.";
						$body .= "<br><br>";
						$body .= "<pre>".htmlentities($domXML)."<pre>";
						$body .= "<br><br>";
						$body .= "<br>Error Message: ".$strReturn."<br>";
						$body .= "<br><br>The 500 Internal Server Error means that there may be information that is missing or malformed.  Dispatchtrack does not provide any additional error messaging when this occurs.";
											
						$objAlert->body = $body;
						#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );
*/
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Alert sent";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
	
						$objReturn->error = '1';
						$objReturn->error_code = '60';
						$objReturn->error_message = 'Dispatchtrack: HTTP 500: Internal Server Error';
	
	
						break;
					
					
					case "Another import is already in progress, please try after some time":
	
						$local_time = date("Y-m-d H:i:s");							
						$gmt_time = gmdate("Y-m-d H:i:s");
	
		                $objData = new stdClass();
		                $objData->id = $objRecord->id;
		                $objData->processed_datetime = "0000-00-00 00:00:00";
		                $objData->processed_datetime_gmt = "0000-00-00 00:00:00";
		                $objData->transfer_datetime = "0000-00-00 00:00:00";
		                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
		                $objData->flag_error = 1;
		                $objData->process_note = $strReturn;				                
	
		                #$result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 
	
						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
						
	
						$objReturn->error = '1';
						$objReturn->error_code = '61';
						$objReturn->error_message = 'Dispatchtrack: Another import is already in progress, please try after some time';	
	
	
						break;							
	
	
	
					default:
	
						$local_time = date("Y-m-d H:i:s");							
						$gmt_time = gmdate("Y-m-d H:i:s");
	
		                $objData = new stdClass();
		                $objData->id = $objRecord->id;
		                $objData->processed_datetime = $local_time;
		                $objData->processed_datetime_gmt = $gmt_time;
		                $objData->transfer_datetime = "0000-00-00 00:00:00";
		                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
		                $objData->flag_error = 1;
		                $objData->process_note = $strReturn;				                
	
		                #$result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 
	
						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
					
						#sent alert to admin
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
						$logMessage .= "\n\r Alert sent";
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	

						$body = "";
						$body .= "<br>Error Message: ".$strReturn."<br>";
						$body .= "<br><br>The following order did not get imported - error:<br>";
						$body .= "<br><br>";
						$body .= "<br><br>";
						$body .= "<pre>".htmlentities($domXML)."<pre>";
						$body .= "<br><br>";	



						$objSendEmailMessage = new stdClass();
						$objSendEmailMessage->email_to = "lsawyer@metalake.com";
						$objSendEmailMessage->email_from = "nsdalert@metalake.com";
						$objSendEmailMessage->email_subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
						$objSendEmailMessage->email_body = $body;
						$objSendEmailMessage->email_category = "scheduling support";
				
						$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
	
	
	
/*
						#send email with order details
						$objAlert = new stdClass();
						#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
						$objAlert->recipients = array( 'lsawyer@metalake.com' );
						$objAlert->subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
						$body = "";
						$body .= "<br>Error Message: ".$strReturn."<br>";
						$body .= "<br><br>The following order did not get imported - error:<br>";
						$body .= "<br><br>";
						$body .= "<br><br>";
						$body .= "<pre>".htmlentities($domXML)."<pre>";
						$body .= "<br><br>";
														
						$objAlert->body = $body;
						#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );
*/
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Alert sent";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
	
						$objReturn->error = '1';
						$objReturn->error_code = '62';
						$objReturn->error_message = 'Dispatchtrack: Unknown Error from DT';	
	
					
					
						break;
					
						
	
				}
		
	
		
		
			}
			else
			{
				
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r 500 error?: ".print_r($returnData, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->agent_code." | 500 ERROR?: ".$returnData;
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
				
				$objReturn->error = '1';
				$objReturn->error_code = '64';
				$objReturn->error_message = 'Dispatchtrack: Error';	
				
			}
	
	
			
			
		}
		else
		{
	
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." |  No record to send. | seconds: ".number_format($stamp_total_micro,2);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			$objReturn->error = '1';
			$objReturn->error_code = '66';
			$objReturn->error_message = 'Dispatchtrack: No order to update.';
			
		}



		#log
		
		$objDataInsert = new stdClass();
		$objDataInsert->agent_id = $objUpdate->agent_id;
		$objDataInsert->action = "update_dispatchtrack_order";
		$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
		$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
		$objDataInsert->objreturn = json_encode($objReturn);
		$objDataInsert->error = $objReturn->error;
		$objDataInsert->error_code = $objReturn->error_code;
		$objDataInsert->error_message = $objReturn->error_message;
		$result = JFactory::getDbo()->insertObject('htc_dispatchtrack_log', $objDataInsert);
		$logID = $db->insertid();



		if ( $objDataInsert->error == "1" )
		{

			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not update dispatchtrack:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$objDataInsert->objreturn."<pre>";
			$body .= "<br><br>";


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = "lsawyer@metalake.com";
			$objSendEmailMessage->email_from = "nsdalert@metalake.com";
			$objSendEmailMessage->email_subject = "ALERT | Nsd_dispatchtrackController | update_dispatchtrack_order";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );



/*
			#send email with error details
			$objAlert = new stdClass();
			#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			#$objAlert->recipients = array( '7039305383@txt.att.net', 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Nsd_dispatchtrackController | update_dispatchtrack_order";
			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not update dispatchtrack:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$objDataInsert->objreturn."<pre>";
			$body .= "<br><br>";
											
			$objAlert->body = $body;
			#$resultAlert = Nsd_dispatchtrackController::sendAlert( $objAlert );
*/
		}


		$logMessage = $functionName." | order_id: ".$objUpdate->service_order_id." | error: ".$objDataInsert->error." | error code: ".$objDataInsert->error_code." | error message: ".$objDataInsert->error_message;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);



		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}


	public static function get_dispatchtrack_order( $objInput )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_dispatchtrack&task=get_dispatchtrack_order



		$controllerName = "Nsd_dispatchtrackController";
		$functionName = "get_dispatchtrack_order";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		$objReturn = new stdClass();

		
		$arrParams = array();
		$arrParams[] = "code=".$objInput->agent->dispatchtrack_api_code;
		$arrParams[] = "api_key=".$objInput->agent->dispatchtrack_api_key;
		$arrParams[] = "service_order_id=".$objInput->dispatchtrack_orderid_clean;
		
		if ( $objInput->agent->dispatchtrack_account != "" )
		{
			$arrParams[] = "account=".rawurlencode($objInput->agent->dispatchtrack_account);
		}		
		
		
		$strParams = implode("&", $arrParams);
		$urlToPost = "https://".$objInput->agent->dispatchtrack_api_code.".dispatchtrack.com/orders/api/export.xml?".$strParams;


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r arrParams: ".print_r($arrParams, true);
		$logMessage .= "\n\r strParams: ".print_r($strParams, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$ch = curl_init ($urlToPost);
		curl_setopt ($ch, CURLOPT_POST, false);
		#curl_setopt ($ch, CURLOPT_POSTFIELDS, $dataToPost);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
	    $retValue = curl_exec($ch);          
	    curl_close($ch);


		$needles1 = array();
		$needles1[] = "service_orders";


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r retValue: ".print_r($retValue, true);
			$logMessage .= "\n\r needles1: ".print_r($needles1, true);
			
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		
		#if ( substr($strFile, 0, 5) == "<?xml" )
		
		if ( Nsd_dispatchtrackController::match_all($needles1, $retValue) )
		{

			#$xml = simplexml_load_file( $objFile->file_fullpath, 'SimpleXMLElement' ); 
			$strFile = simplexml_load_string( $retValue );
			$count = (string)$strFile->attributes()->count;

			switch( $count )
			{
				
				case "0":

					$objReturn->error = '1';
					$objReturn->error_code = '50';
					$objReturn->error_message = "No Order in Dispatchtrack | ".$objInput->agent->id." | ".$objInput->agent->agent_code." | ".$objInput->dispatchtrack_orderid_clean;
				
				break;
				
				
				default:
				
					foreach( $strFile->service_order as $service_order )
					{
					
						$objReturn = new stdClass();
						$objReturn->error = '0';
						$objReturn->error_code = "";
						$objReturn->error_message = "";						
						$objReturn->service_order_id = (string)$service_order->attributes()->id;
						$objReturn->order_number = (string)$service_order->attributes()->order_number;
						$objReturn->customer_id = (string)$service_order->customer->customer_id;
						$objReturn->first_name = (string)$service_order->customer->first_name;
						$objReturn->last_name = (string)$service_order->customer->last_name;
						$objReturn->email = (string)$service_order->customer->email;
						$objReturn->phone1 = (string)$service_order->customer->phone1;
						$objReturn->phone2 = (string)$service_order->customer->phone2;
						$objReturn->address1 = (string)$service_order->customer->address1;
						$objReturn->address2 = (string)$service_order->customer->address2;
						$objReturn->city = (string)$service_order->customer->city;
						$objReturn->state = (string)$service_order->customer->state;
						$objReturn->zip = (string)$service_order->customer->zip;
						$objReturn->latitude = (string)$service_order->customer->latitude;
						$objReturn->longitude = (string)$service_order->customer->longitude;
						$objReturn->display_order_number = (string)$service_order->display_order_number;
						$objReturn->status = (string)$service_order->status;
						$objReturn->service_type = (string)$service_order->service_type;
						$objReturn->description = (string)trim($service_order->description);
						$objReturn->account = (string)$service_order->account;
						$objReturn->pre_call_confirmation_status = (string)$service_order->pre_call->confirmation_status;
						$objReturn->pre_call_text_confirmation_status = (string)$service_order->pre_call->text_confirmation_status;
						$objReturn->stop_number = (string)$service_order->stop_number;
						$objReturn->scheduled_at = (string)$service_order->scheduled_at;
						$objReturn->time_window_start = (string)$service_order->time_window_start;
						$objReturn->time_window_end = (string)$service_order->time_window_end;
						$objReturn->service_time = (string)$service_order->service_time;
						$objReturn->origin = (string)$service_order->origin;
						$objReturn->request_window_start_time = (string)$service_order->request_window_start_time;
						$objReturn->request_window_end_time = (string)$service_order->request_window_end_time;
					
					}
				
				break;
				
			}




		}
		else
		{

			$needles2 = array();
			$needles2[] = "Valid";
			$needles2[] = "Key";
			$needles2[] = "Code";
	
	
			$needles3 = array();
			$needles3[] = "502";
			$needles3[] = "Bad";
			$needles3[] = "Gateway";


			$needles4 = array();
			$needles4[] = "Api";
			$needles4[] = "Request";


			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r strFile: ".print_r($strFile, true);
			$logMessage .= "\n\r needles1: ".print_r($needles1, true);
			$logMessage .= "\n\r needles2: ".print_r($needles2, true);
			$logMessage .= "\n\r needles3: ".print_r($needles3, true);
			$logMessage .= "\n\r needles4: ".print_r($needles4, true);
			$logMessage .= "\n\r haystack: ".print_r($haystack, true);
			$logMessage .= "\n\r result: ".print_r($result, true);
			
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			if (  Nsd_dispatchtrackController::match_all($needles2, $retValue) )
			{
				$objReturn->error = '1';
				$objReturn->error_code = '10';
				$objReturn->error_message = "Valid API Key and API Code is required | ".$objInput->agent->id." | ".$objInput->agent->agent_code." | ".$objInput->dispatchtrack_orderid_clean;

			}	
			elseif (  Nsd_dispatchtrackController::match_all($needles3, $retValue) )
			{
				$objReturn->error = '1';
				$objReturn->error_code = '20';
				$objReturn->error_message = "502 Bad Gateway | ".$objInput->agent->id." | ".$objInput->agent->agent_code." | ".$objInput->dispatchtrack_orderid_clean;
			}	
			elseif (  Nsd_dispatchtrackController::match_all($needles4, $retValue) )
			{
				$objReturn->error = '1';
				$objReturn->error_code = '30';
				$objReturn->error_message = "Api Request count exceeded | ".$objInput->agent->id." | ".$objInput->agent->agent_code." | ".$objInput->dispatchtrack_orderid_clean;
			}	
			else
			{
				$objReturn->error = '1';
				$objReturn->error_code = '40';
				$objReturn->error_message = "Undefined API Issue | ".$objInput->agent->id." | ".$objInput->agent->agent_code." | ".$objInput->dispatchtrack_orderid_clean;
			}				

		}



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r retValue: ".print_r($retValue, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		


		#log
		
		$objDataInsert = new stdClass();
		$objDataInsert->agent_id = $objInput->agent->id;
		$objDataInsert->action = "get_dispatchtrack_order";
		$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
		$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
		$objDataInsert->objreturn = json_encode($objReturn);
		$objDataInsert->error = $objReturn->error;
		$objDataInsert->error_code = $objReturn->error_code;
		$objDataInsert->error_message = $objReturn->error_message;
		$result = JFactory::getDbo()->insertObject('htc_dispatchtrack_log', $objDataInsert);
		$logID = $db->insertid();



		if ( $objDataInsert->error == "1" )
		{

			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not retrieve from dispatchtrack:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$objDataInsert->objreturn."<pre>";
			$body .= "<br><br>";


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = "lsawyer@metalake.com";
			$objSendEmailMessage->email_from = "nsdalert@metalake.com";
			$objSendEmailMessage->email_subject = "ALERT | Nsd_dispatchtrackController | get_dispatchtrack_order";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			#$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );

/*
			#send email with error details
			$objAlert = new stdClass();
			#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			#$objAlert->recipients = array( '7039305383@txt.att.net', 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Nsd_dispatchtrackController | get_dispatchtrack_order";
			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not retrieve from dispatchtrack:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$objDataInsert->objreturn."<pre>";
			$body .= "<br><br>";
			$objAlert->body = $body;
			#$resultAlert = Nsd_dispatchtrackController::sendAlert( $objAlert );
*/







		}

		$logMessage = $functionName." | order_id: ".$objInput->dispatchtrack_orderid_clean." | error: ".$objDataInsert->error." | error code: ".$objDataInsert->error_code." | error message: ".$objDataInsert->error_message;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}	


	public static function match_all($needles, $haystack)
	{

		$className = "Nsd_dispatchtrackController";
		$functionName = "match_all";


		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}
		
		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r needles: ".print_r($needles, true);
		$logMessage .= "\n\r haystack: ".print_r($haystack, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



	    if(empty($needles)){

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r FALSE: ".print_r("FALSE", true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	        return false;
	    }
	
	    foreach($needles as $needle) {
	        if (strpos($haystack, $needle) === false) {

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r FALSE: ".print_r("FALSE", true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


	            return false;
	        }
	    }
		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r TRUE: ".print_r("TRUE", true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	    return true;
	}



	public static function determine_dispatchtrack_agent( $objInput )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_dispatchtrack&task=determine_dispatchtrack_agent



		$controllerName = "Nsd_dispatchtrackController";
		$functionName = "determine_dispatchtrack_agent";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn= new stdClass();

		$arrWhere = array();
		$arrWhere[] = "state = 1";
		$arrWhere[] = "agent_code = '".$objInput->agent_code."' ";
		$arrWhere[] = "client_account_code = '".$objInput->customer_account_code."' ";
		
		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;

		$query = "SELECT * FROM htc_dispatchtrack_agents ". $where;
        $db->setQuery($query);
        $objAgent = $db->loadObject() ;

		if ( count($objAgent) == 0 )
		{

			$arrWhere = array();
			$arrWhere[] = "state = 1";
			$arrWhere[] = "agent_code = '".$objInput->agent_code."' ";
			$arrWhere[] = "client_account_code = '' ";			
			
			$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
	
			$query2 = "SELECT * FROM htc_dispatchtrack_agents ". $where;
	        $db->setQuery($query2);
	        $objAgent = $db->loadObject() ;
		
		}

		$objReturn->agent = $objAgent;

			
		switch( $objAgent->orderid_mod_type )
		{
			
			case "append":
			
				$dispatchtrack_order_id_clean = $objInput->order_id.$objAgent->orderid_mod_value;
			
			
				break;


			case "prepend":
			
				$dispatchtrack_order_id_clean = $objAgent->orderid_mod_value.$objInput->order_id;
			
				break;


			default:

				$dispatchtrack_order_id_clean = $objInput->order_id;			
			
				break;
			
			
		}
		
		$objReturn->dispatchtrack_orderid = $objInput->order_id;
		$objReturn->dispatchtrack_orderid_clean = $dispatchtrack_order_id_clean;

		



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r query2: ".print_r($query2, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;

		
	}	



	public static function sendAlert( $objAlert )
	{

		$mailer =& JFactory::getMailer();
		$config = JFactory::getConfig();

		$sender = array( 
		    $config->get( 'mailfrom' ),
		    $config->get( 'fromname' ) 
		);
		$mailer->setSender($sender);

		$mailer->addRecipient( $objAlert->recipients );

		$mailer->setSubject( $objAlert->subject );
		$mailer->setBody( $objAlert->body );
		$mailer->isHTML(true);

		if ($mailer->Send() !== true)
		{
			$return = 0;			
		}			
		else
		{
			$return = 1;			
		}


		return $return;
	}


	public static function pruneLogs()
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_dispatchtrack&task=pruneLogs
		
		$controllerName = "Nsd_dispatchtrackController";
		$functionName = "pruneLogs";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrTables = array();
		$arrTables[] = "htc_dispatchtrack_log";

		#if ( date('H') == '23' )
		
		if ( TRUE )
		{
			foreach( $arrTables as $table )
			{
				$query = "select count(z.id) from ".$table." z";
				$db->setQuery($query);
				$recordCount = $db->loadResult();

				$logMessage = "INSIDE | Ml_apiController | ".$functionName;
				$logMessage .= "\n\r recordCount for ".$table.": ".print_r($recordCount, true);
				$logMessage .= "\n\r dateH: ".print_r(date('H'), true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				if ( $recordCount > 600000 )
				{
		
					$query = "select z.id from ".$table." z order by z.id desc limit 500000, 1";
					$db->setQuery($query);
					$idRecord = $db->loadResult();
			
					if ( $idRecord != "" )
					{
						$queryD = "Delete FROM ".$table." WHERE id <= '".$idRecord."' ";
						$db->setQuery($queryD);
						$db->query();
					}
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r idRecord: ".print_r($idRecord, true);
					$logMessage .= "\n\r queryD: ".print_r($queryD, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				}


			}
		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

// !TEST

	public function test_dispatchtrack_update( )  
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_dispatchtrack&task=test_dispatchtrack_update

		$controllerName = "Nsd_dispatchtrackController";
		$functionName = "test_dispatchtrack_update";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


/*
				$query = "SELECT c.*, a.name, a.description, a.dispatchtrack_code, a.dispatchtrack_api_key, a.dispatchtrack_account, a.agent_timezone, a.state from htc_nsd_orderentry_data_clean c LEFT JOIN htc_nsd_orderentry_agents a ON c.agent_id = a.id WHERE a.state=1 AND c.processed_datetime = '0000-00-00 00:00:00' order by c.id limit 1";
				$db->setQuery($query);
				$objRecord = $db->loadObject();
*/

				$objRecord = new stdClass();
				$objRecord->agent_id = "1";
				$objRecord->service_order_id_clean = "13713039";
				$objRecord->account_val = "AMAZON";
				$objRecord->service_delivery_type_clean = "BASIC";
				$objRecord->destination_name = "JOSEPH PIROZZOLO";

				$objRecord->destination_address_1 = "1112 16TH ST NW STE 900";
				$objRecord->destination_city = "WASHINGTON";
				$objRecord->destination_state = "DC";
				$objRecord->destination_zip = "20036";

				$objRecord->phone_1 = "2024571200";
				$objRecord->phone_3 = "2024571203";

				$request_delivery_date_val = "2018-08-29";
				$request_time_window_start_val = "2018-08-29 09:00:00";
				$request_time_window_end_val = "2018-08-29 21:00:00";

				$request_delivery_date_val = "2018-08-30";
				$request_time_window_start_val = "2018-08-30 08:00:00";
				$request_time_window_end_val = "2018-08-30 20:00:00";


					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objRecord: ".print_r($objRecord, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				if ( count($objRecord) > 0 )
				{


					$objAgent = MetalakeHelperCore::getObjTableData( $table="htc_dispatchtrack_agents", $field="id", $value=$objRecord->agent_id );

			
					/* $xmlBody = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><service_order></service_order>'); */
					
					$xmlBody = new SimpleXMLExtended('<?xml version="1.0" encoding="utf-8"?><service_order></service_order>');
					
						#$account_val = ( $objAgent->id == "1"  ) ? htmlspecialchars($objRecord->client_account_name) : $objAgent->dispatchtrack_account ; 

						$xmlBody->addChild('number', $objRecord->service_order_id_clean);
						
						$xmlBody->addChild('account', $objRecord->account_val);
						$xmlBody->addChild('service_type', $objRecord->service_delivery_type_clean);
						
						#$xmlBody->addChildWithCDATA('description', '');
	
	
						$customer = $xmlBody->addChild('customer');
	
						#$customerInfo = $customer->addChild('customer_id');
						#$customerInfo = $customer->addChildWithCDATA('first_name', $objRecord->destination_name);
						#$customerInfo = $customer->addChild('first_name', "");
						
						$customerInfo = $customer->addChildWithCDATA('last_name', $objRecord->destination_name);
						
						#$customerInfo = $customer->addChild('email', '');
						
						$customerInfo = $customer->addChild('phone1', $objRecord->phone_1);
						#$customerInfo = $customer->addChild('phone2', $objRecord->phone_2);
						$customerInfo = $customer->addChild('phone3', $objRecord->phone_3);
						$customerInfo = $customer->addChild('address1', $objRecord->destination_address_1);		
						#$customerInfo = $customer->addChild('address2', $objRecord->destination_address_2);		
						$customerInfo = $customer->addChild('city', $objRecord->destination_city);		
						$customerInfo = $customer->addChild('state', $objRecord->destination_state);		
						$customerInfo = $customer->addChild('zip', $objRecord->destination_zip);		
						
						
						#$customerInfo = $customer->addChild('latitude', '');		
						#$customerInfo = $customer->addChild('longitude', '');		

/*
						$queryN = "SELECT * FROM htc_nsd_orderentry_data_clean_notes WHERE service_order_id = '".$objRecord->service_order_id."' order by id";
						
						$db->setQuery($queryN);
						$objListNotes = $db->loadObjectList();
						
						$countNotes = count($objListNotes);

						$notes = $xmlBody->addChild('notes');
						
						$notes->addAttribute('count', $countNotes);

						foreach( $objListNotes as $objNote )
						{
							$note = $notes->addChildWithCDATA('note',$objNote->note );
							$note->addAttribute('created_at', $objNote->created_at);
							$note->addAttribute('author', $objNote->author);
							$note->addAttribute('note_type', 'Public');

						}
	
// 						$queryN = "SELECT service_order_id, description, pieces, weight, pre_delivery_charge FROM htc_nsd_orderentry_data_clean_items WHERE service_order_id = '".$objRecord->service_order_id."' and data_id = '".$objRecord->data_id."' order by id";

						$queryN = "SELECT service_order_id, description, pieces, weight, pre_delivery_charge FROM htc_nsd_orderentry_data_clean_items WHERE service_order_id = '".$objRecord->service_order_id."' order by id";
						
						$db->setQuery($queryN);
						$objListItems = $db->loadObjectList();
	
						$items = $xmlBody->addChild('items');

						foreach( $objListItems as $objItem )
						{
							$Item = $items->addChild('item');
							#$Item->addChild('sale_sequence', '1' );  //required
							$Item->addChild('item_id', '1' ); //required
							#$Item->addChild('serial_number', $objRecord->service_order_id ); //required
							$Item->addChildWithCDATA('description', $objItem->description );
							$Item->addChild('quantity', $objItem->pieces );
							#$Item->addChild('location', '1' ); //required
							$Item->addChild('cube', '100' );
							#$Item->addChild('setup_time', '' );
							$Item->addChild('weight', $objItem->weight );
							#$Item->addChild('amount', $objItem->pre_delivery_charge );
							$Item->addChild('price', $objItem->pre_delivery_charge );
							#$Item->addChild('countable', '' );
							#$Item->addChild('store_code', '' );

						}
*/



/*
						$items = $xmlBody->addChild('items');
							$Item = $items->addChild('item');
							#$Item->addChild('sale_sequence', '1' );  //required
							$Item->addChild('item_id', '' ); //required
							#$Item->addChild('serial_number', $objRecord->service_order_id ); //required
							$Item->addChildWithCDATA('description', '' );
							$Item->addChild('quantity', '' );
							#$Item->addChild('location', '1' ); //required
							$Item->addChild('cube', '' );
							#$Item->addChild('setup_time', '' );
							$Item->addChild('weight', '' );
							#$Item->addChild('amount', $objItem->pre_delivery_charge );
							$Item->addChild('price', '' );
							#$Item->addChild('countable', '' );
							#$Item->addChild('store_code', '' );
*/





/*
						
						if ( $objRecord->appointment_confirmed == "CONFIRMED" )
						{
							$request_delivery_date_val = $objRecord->appointment_date;
							$request_time_window_start_val = $objRecord->appointment_start_time_clean;
							$request_time_window_end_val = $objRecord->appointment_end_time_clean;							
						}
						else
						{
							$request_delivery_date_val = "";
							$request_time_window_start_val = "";
							$request_time_window_end_val = "";							
						}
*/

					
						
	#production
						#$xmlBody->addChild('pre_reqs', '');
						#$xmlBody->addChild('amount', '');
						#$xmlBody->addChild('cod_amount', '');
						#$xmlBody->addChild('service_unit', '');
						#$xmlBody->addChild('delivery_date', '');
						$xmlBody->addChild('request_delivery_date', $request_delivery_date_val);
						#$xmlBody->addChild('ready_to_schedule_date', '');
						#$xmlBody->addChild('schedule_before_date', '');
						#$xmlBody->addChild('status', $objRecord->order_status); // NEW
						#$xmlBody->addChild('status', 'Scheduled'); // NEW
						#$xmlBody->addChild('client_code', '');
						#$xmlBody->addChild('client_reference_number', '');
						#$xmlBody->addChild('client_tracking_number', '');
						#$xmlBody->addChild('driver_id', '');
						#$xmlBody->addChild('truck_id', '');
						#$xmlBody->addChild('origin', '');
						#$xmlBody->addChild('stop_number', '');
						#$xmlBody->addChild('stop_time', '');
						#$xmlBody->addChild('service_time', '');
						$xmlBody->addChild('request_time_window_start', $request_time_window_start_val);
						$xmlBody->addChild('request_time_window_end', $request_time_window_end_val);
						#$xmlBody->addChild('delivery_time_window_start', '');
						#$xmlBody->addChild('delivery_time_window_end', '');
						#$xmlBody->addChild('delivery_charge', '');
						#$xmlBody->addChild('taxes', '');
						#$xmlBody->addChild('store_code', '');						
						


/*
						#$xmlBody->addChild('pre_reqs', '');
						#$xmlBody->addChild('amount', '');
						#$xmlBody->addChild('cod_amount', '');
						$xmlBody->addChild('service_unit', 'T01');
						#$xmlBody->addChild('delivery_date', '');
						$xmlBody->addChild('request_delivery_date', $request_delivery_date_val);
						#$xmlBody->addChild('ready_to_schedule_date', '');
						#$xmlBody->addChild('schedule_before_date', '');
						#$xmlBody->addChild('status', $objRecord->order_status); // NEW
						$xmlBody->addChild('status', 'Scheduled'); // NEW
						#$xmlBody->addChild('client_code', '');
						#$xmlBody->addChild('client_reference_number', '');
						#$xmlBody->addChild('client_tracking_number', '');
						#$xmlBody->addChild('driver_id', '');
						$xmlBody->addChild('truck_id', 'D01');
						#$xmlBody->addChild('origin', '');
						$xmlBody->addChild('stop_number', '1');
						#$xmlBody->addChild('stop_time', '');
						#$xmlBody->addChild('service_time', '');
						$xmlBody->addChild('request_time_window_start', $request_time_window_start_val);
						$xmlBody->addChild('request_time_window_end', $request_time_window_end_val);
						#$xmlBody->addChild('delivery_time_window_start', '2018-08-28 12:00:00 -0400');
						#$xmlBody->addChild('delivery_time_window_end', '2018-08-28 16:00:00 -0400');
						#$xmlBody->addChild('delivery_charge', '');
						#$xmlBody->addChild('taxes', '');
						#$xmlBody->addChild('store_code', '');	
						
						#$xmlBody->addChild('scheduled_at', '2018-08-28 11:35:00 -0400');	
						#$xmlBody->addChild('time_window_start', '2018-08-28 12:00:00 -0400');	
						#$xmlBody->addChild('time_window_end', '2018-08-28 16:00:00 -0400');	
*/	

						
/*
						
						$queryEF = "SELECT distinct agent_id, field_name, data_field FROM htc_nsd_orderentry_extra_fields WHERE state = '1' AND  agent_id = '".$objAgent->id."' AND field_type = 'extra' order by id";
						$db->setQuery($queryEF);
						$objListExtraFields = $db->loadObjectList();						
						
						$extra = $xmlBody->addChild('extra');

						foreach( $objListExtraFields as $objEF )
						{
							$data_field = (string) $objEF->data_field;
							
							$extra->addChild($objEF->field_name, $objRecord->$data_field);

						}						
						
						

						$queryCus = "SELECT distinct agent_id, field_name, data_field FROM htc_nsd_orderentry_extra_fields WHERE state = '1' AND agent_id = '".$objAgent->id."' AND field_type = 'custom' order by id";
						$db->setQuery($queryCus);
						$objListExtraCustom = $db->loadObjectList();						

						
						$custom_fields = $xmlBody->addChild('custom_fields');


						foreach( $objListExtraCustom as $objCus )
						{
							$data_field = (string) $objCus->data_field;
							
							$custom_fields->addChild($objCus->field_name, $objRecord->$data_field);

						}	
*/

					$dom = new DOMDocument("1.0");
					$dom->preserveWhiteSpace = FALSE;
					$dom->loadXML($xmlBody->asXML());
					$dom->formatOutput = TRUE;
					echo "<pre>".htmlentities($dom->saveXML())."</pre>";

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r xmlBody: ".print_r($xmlBody, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$urltopost = "https://".$objAgent->dispatchtrack_api_code.".dispatchtrack.com/orders/api/add_order";

					$domXML = $dom->saveXML();


					$datatopost	= array (
					  "code"	=> $objAgent->dispatchtrack_api_code,
					  "api_key"	=> $objAgent->dispatchtrack_api_key,
					  "data"	=> $domXML
					  );		

					if ( $objAgent->dispatchtrack_account != "" )
					{
						$datatopost["account"] = $objAgent->dispatchtrack_account;
					}

				
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r urltopost: ".print_r($urltopost, true);
					$logMessage .= "\n\r datatopost: ".print_r($datatopost, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$ch = curl_init ($urltopost);
					
					
					curl_setopt ($ch, CURLOPT_POST, true);
					curl_setopt ($ch, CURLOPT_POSTFIELDS, $datatopost);
					curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
					#curl_setopt ($ch, CURLOPT_FILE, $fp);
					
					$returnData = curl_exec ($ch);
				
					$error = curl_error($ch);
				
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r returnData: ".print_r($returnData, true);
					$logMessage .= "\n\r error: ".print_r($error, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					if ( substr($returnData, 0, 5) == "<?xml" )
					{
				
						$xml = simplexml_load_string( $returnData );
						
						$strReturn = (string)$xml;
						
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
						$logMessage .= "\n\r returnData: ".print_r($returnData, true);
						$logMessage .= "\n\r xml: ".print_r($xml, true);
						$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				
						switch( $strReturn  )
						{
							
							case "Imported given orders!":

					
								$stamp_end_micro = microtime(true);
								$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
					
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->name." | Record [".$objRecord->id."] Processed and Sent: ".$objRecord->service_order_id." | seconds: ".number_format($stamp_total_micro,2);
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

								
								
								break;






							case "HTTP 500: Internal Server Error":

								$local_time = date("Y-m-d H:i:s");							
								$gmt_time = gmdate("Y-m-d H:i:s");
	
				                $objData = new stdClass();
				                $objData->id = $objRecord->id;
				                $objData->processed_datetime = $local_time;
				                $objData->processed_datetime_gmt = $gmt_time;
				                $objData->transfer_datetime = "0000-00-00 00:00:00";
				                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
				                $objData->flag_error = 1;
				                $objData->process_note = $strReturn;				                
	
				                #$result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 

								$stamp_end_micro = microtime(true);
								$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->name." | Record [".$objRecord->id."] Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

								$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
								$logMessage .= "\n\r Alert sent";
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




								$body = "";
								$body .= "<br>The order below failed to import into Dispatchtrack during the order entry process.";
								$body .= "<br><br>";
								$body .= "<br><br>This order will have to be entered into Dispatchtrack manually.";
								$body .= "<br><br>";
								$body .= "<pre>".htmlentities($domXML)."<pre>";
								$body .= "<br><br>";
								$body .= "<br>Error Message: ".$strReturn."<br>";
								$body .= "<br><br>The 500 Internal Server Error means that there may be information that is missing or malformed.  Dispatchtrack does not provide any additional error messaging when this occurs.";
					
					
								$objSendEmailMessage = new stdClass();
								$objSendEmailMessage->email_to = "lsawyer@metalake.com";
								$objSendEmailMessage->email_from = "nsdalert@metalake.com";
								$objSendEmailMessage->email_subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
								$objSendEmailMessage->email_body = $body;
								$objSendEmailMessage->email_category = "scheduling support";
						
								$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );


/*
								#send email with order details
								$objAlert = new stdClass();
								$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
								#$objAlert->recipients = array( 'lsawyer@metalake.com' );
								$objAlert->subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
								$body = "";
								$body .= "<br>The order below failed to import into Dispatchtrack during the order entry process.";
								$body .= "<br><br>";
								$body .= "<br><br>This order will have to be entered into Dispatchtrack manually.";
								$body .= "<br><br>";
								$body .= "<pre>".htmlentities($domXML)."<pre>";
								$body .= "<br><br>";
								$body .= "<br>Error Message: ".$strReturn."<br>";
								$body .= "<br><br>The 500 Internal Server Error means that there may be information that is missing or malformed.  Dispatchtrack does not provide any additional error messaging when this occurs.";
													
								$objAlert->body = $body;
								#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );
*/




			
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Alert sent";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

	



								break;
							
							
							case "Another import is already in progress, please try after some time":

								$local_time = date("Y-m-d H:i:s");							
								$gmt_time = gmdate("Y-m-d H:i:s");
	
				                $objData = new stdClass();
				                $objData->id = $objRecord->id;
				                $objData->processed_datetime = "0000-00-00 00:00:00";
				                $objData->processed_datetime_gmt = "0000-00-00 00:00:00";
				                $objData->transfer_datetime = "0000-00-00 00:00:00";
				                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
				                $objData->flag_error = 1;
				                $objData->process_note = $strReturn;				                
	
				                #$result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 
	
								$stamp_end_micro = microtime(true);
								$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->name." | Record [".$objRecord->id."] Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

								break;							



							default:

								$local_time = date("Y-m-d H:i:s");							
								$gmt_time = gmdate("Y-m-d H:i:s");
	
				                $objData = new stdClass();
				                $objData->id = $objRecord->id;
				                $objData->processed_datetime = $local_time;
				                $objData->processed_datetime_gmt = $gmt_time;
				                $objData->transfer_datetime = "0000-00-00 00:00:00";
				                $objData->transfer_datetime_gmt = "0000-00-00 00:00:00";
				                $objData->flag_error = 1;
				                $objData->process_note = $strReturn;				                
	
				                #$result = JFactory::getDbo()->updateObject('htc_nsd_orderentry_data_clean', $objData, 'id'); 

								$stamp_end_micro = microtime(true);
								$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->name." | Record [".$objRecord->id."] Processed and NOT Sent: ".$objRecord->service_order_id." | Return message: ".$strReturn." | seconds: ".number_format($stamp_total_micro,2);
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
							
								#sent alert to admin

								$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
								$logMessage .= "\n\r Alert sent";
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


								$body = "";
								$body .= "<br>Error Message: ".$strReturn."<br>";
								$body .= "<br><br>The following order did not get imported - error:<br>";
								$body .= "<br><br>";
								$body .= "<br><br>";
								$body .= "<pre>".htmlentities($domXML)."<pre>";
								$body .= "<br><br>";



								$objSendEmailMessage = new stdClass();
								$objSendEmailMessage->email_to = "lsawyer@metalake.com";
								$objSendEmailMessage->email_from = "nsdalert@metalake.com";
								$objSendEmailMessage->email_subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
								$objSendEmailMessage->email_body = $body;
								$objSendEmailMessage->email_category = "scheduling support";
						
								$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );



/*
								#send email with order details
								$objAlert = new stdClass();
								#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
								$objAlert->recipients = array( 'lsawyer@metalake.com' );
								$objAlert->subject = "ALERT | Dispatchtrack Order Entry | ".$objAgent->name." | Order Insert Error";
								$body = "";
								$body .= "<br>Error Message: ".$strReturn."<br>";
								$body .= "<br><br>The following order did not get imported - error:<br>";
								$body .= "<br><br>";
								$body .= "<br><br>";
								$body .= "<pre>".htmlentities($domXML)."<pre>";
								$body .= "<br><br>";
																
								$objAlert->body = $body;
								#$resultAlert = Nsd_orderentryController::sendAlert( $objAlert );
*/
			
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Alert sent";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	




							
							
								break;
							
								

						}
				

				
				
					}
					else
					{
						
				
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
						$logMessage .= "\n\r 500 error?: ".print_r($returnData, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$objAgent->id." ".$objAgent->name." | 500 ERROR | Number of records sent: 0 | Return data message: ".$returnData;
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
						
					}


					$return = 1;
					
				}
				else
				{

					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." |  No record to send. | seconds: ".number_format($stamp_total_micro,2);
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
					
					$return = 0;
					
				}





		


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $return;
	}

	

	public function test_get_dispatchtrack_order(  )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_dispatchtrack&task=test_get_dispatchtrack_order



		$controllerName = "Nsd_dispatchtrackController";
		$functionName = "test_get_dispatchtrack_order";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$objAgentDT = Nsd_dispatchtrackController::test_determine_dispatchtrack_agent( );

		#$objInput = Nsd_dispatchtrackController::determine_dispatchtrack_agent( $objAgentDT );
		
		$obReturn = Nsd_dispatchtrackController::get_dispatchtrack_order( $objAgentDT );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objAgentDT: ".print_r($objAgentDT, true);
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r obReturn: ".print_r($obReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_determine_dispatchtrack_agent(  )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_dispatchtrack&task=test_determine_dispatchtrack_agent



		$controllerName = "Nsd_dispatchtrackController";
		$functionName = "test_determine_dispatchtrack_agent";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



/*
		$objInputDispatchtrack = new stdClass();
		$objInputDispatchtrack->agent_code = $objTruckmate->order->CARE_OF;
		$objInputDispatchtrack->customer_account_code = $objTruckmate->order->CUSTOMER;
		$objInputDispatchtrack->order_id = $objTruckmate->order->BILL_NUMBER;
*/


		$objInputDispatchtrack = new stdClass();
		$objInputDispatchtrack->agent_code = "CTE";
		$objInputDispatchtrack->customer_account_code = "11649";
		$objInputDispatchtrack->order_id = "32962096";


/*
		$objInputDispatchtrack = new stdClass();
		$objInputDispatchtrack->agent_code = "SHOWME";
		$objInputDispatchtrack->customer_account_code = "11649";
		$objInputDispatchtrack->order_id = "31141902";
*/


/*
		$objInputDispatchtrack = new stdClass();
		$objInputDispatchtrack->agent_code = "SHOWME";
		$objInputDispatchtrack->customer_account_code = "11597";
		$objInputDispatchtrack->order_id = "31147805";
*/

		$objAgentDT = Nsd_dispatchtrackController::determine_dispatchtrack_agent( $objInputDispatchtrack );


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInputDispatchtrack: ".print_r($objInputDispatchtrack, true);
		$logMessage .= "\n\r objAgentDT: ".print_r($objAgentDT, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objAgentDT;
	}

	
// !FUNCTION TEMPLATE

	public static function test_function_template( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_dispatchtrack&task=test_function_template



		$controllerName = "Nsd_dispatchtrackController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	
}


// !COMMON

class SimpleXMLExtended extends SimpleXMLElement
{
    public function addCData($cdata_text)
    {
        $node = dom_import_simplexml($this);
        $no   = $node->ownerDocument;
        $node->appendChild($no->createCDATASection($cdata_text));
    }
    /**
     * Adds a child with $value inside CDATA
     * @param unknown $name
     * @param unknown $value
     */
    public function addChildWithCDATA($name, $value = NULL)
    {
        $new_child = $this->addChild($name);
        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no   = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }
        return $new_child;
    }
}
