<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_truckmate
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');
jimport('joomla.filesystem.file' );

/**
 * Class Nsd_truckmateController
 *
 * @since  1.6
 */
class Nsd_truckmateController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'truckmatelogs');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}




// !TMW4WEB

	public static function getTruckmateData( $objInput )  //PRODUCTION
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=getTruckmateData


		$controllerName = "Nsd_truckmateController";
		$functionName = "getTruckmateData";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();

		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');
		$paramsComponent->truckmate_version 			= $params->get('truckmate_version');
		$paramsComponent->flag_active_server			= $params->get('flag_active_server');
		$paramsComponent->api_rate_limit_milliseconds 	= $params->get('api_rate_limit_milliseconds');
		$paramsComponent->enable_logging 				= $params->get('enable_logging');

        
		$paramsComponent->tmw4web_user_server_1			= $params->get('truckmate_user_server_1');
		$paramsComponent->tmw4web_password_server_1		= $params->get('truckmate_password_server_1');
		$paramsComponent->tmw4web_domain_server_1		= $params->get('truckmate_domain_server_1');
		
		$paramsComponent->tmw4web_user_server_2			= $params->get('truckmate_user_server_2');
		$paramsComponent->tmw4web_password_server_2		= $params->get('truckmate_password_server_2');
		$paramsComponent->tmw4web_domain_server_2		= $params->get('truckmate_domain_server_2');	
		
		$paramsComponent->tmw4web_user_server_3			= $params->get('truckmate_user_server_3');
		$paramsComponent->tmw4web_password_server_3		= $params->get('truckmate_password_server_3');
		$paramsComponent->tmw4web_domain_server_3		= $params->get('truckmate_domain_server_3');	

		$paramsComponent->flag_active_server_trace			 	= $params->get('flag_active_server_trace');

		$paramsComponent->tmw4web_user_serve_tracer_1			= $params->get('truckmate_user_server_trace_1');
		$paramsComponent->tmw4web_password_server_trace_1		= $params->get('truckmate_password_server_trace_1');
		$paramsComponent->tmw4web_domain_server_trace_1			= $params->get('truckmate_domain_server_trace_1');
		
		$paramsComponent->tmw4web_user_server_trace_2			= $params->get('truckmate_user_server_trace_2');
		$paramsComponent->tmw4web_password_server_trace_2		= $params->get('truckmate_password_server_trace_2');
		$paramsComponent->tmw4web_domain_serve_tracer_2			= $params->get('truckmate_domain_server_trace_2');	
		
		$paramsComponent->tmw4web_user_server_trace_3			= $params->get('truckmate_user_server_trace_3');
		$paramsComponent->tmw4web_password_server_trace_3		= $params->get('truckmate_password_server_trace_3');
		$paramsComponent->tmw4web_domain_server_trace_3			= $params->get('truckmate_domain_server_trace_3');


		$connection_protocol = $paramsComponent->connection_protocol;

		

		switch( $paramsComponent->flag_active_server )
		{

			case "3":
				$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user_server_3;
				$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password_server_3;
				$domain 	= $paramsComponent->tmw4web_domain_server_3;   
				break;

			case "2":
				$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user_server_2;
				$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password_server_2;
				$domain 	= $paramsComponent->tmw4web_domain_server_2;   
				break;
			
			case "1":	
			default:
				$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user_server_1;
				$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password_server_1;
				$domain 	= $paramsComponent->tmw4web_domain_server_1;   
				break;
							
		}

		if ( $objInput->flag_trace_page == "1" )
		{

			switch( $paramsComponent->flag_active_server_trace )
			{
	
				case "3":
					$tm4web_usr = $paramsComponent->tmw4web_user_server_trace_3;
					$tm4web_pwd = $paramsComponent->tmw4web_password_server_trace_3;
					$domain 	= $paramsComponent->tmw4web_domain_server_trace_3; 
					break;
	
				case "2":
					$tm4web_usr = $paramsComponent->tmw4web_user_server_trace_2;
					$tm4web_pwd = $paramsComponent->tmw4web_password_server_trace_2;
					$domain 	= $paramsComponent->tmw4web_domain_serve_tracer_2	;  
					break;

				case "1":	
				default:
					$tm4web_usr = $paramsComponent->tmw4web_user_serve_tracer_1;
					$tm4web_pwd = $paramsComponent->tmw4web_password_server_trace_1;
					$domain 	= $paramsComponent->tmw4web_domain_server_trace_1;   
					break;
								
			}

		}

/*
		if ( $objInput->flag_trace_page == "1" )
		{
			$paramsComponent->tmw4web_user				= $params->get('tmw4web_user_trace');
			$paramsComponent->tmw4web_password			= $params->get('tmw4web_password_trace');
			$paramsComponent->tmw4web_subdomain			= $params->get('tmw4web_subdomain_trace');
			$paramsComponent->flag_test_mode			= $params->get('flag_test_mode_trace');
			$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test_trace');
			$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test_trace');
			$paramsComponent->tmw4web_subdomain_test	= $params->get('tmw4web_subdomain_test_trace');	

		}


		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_domain;   
			
		}
		else
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_domain_test;   		
		}
*/
		

		$trace_number = $objInput->trace_number;

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r objForm: ".print_r($objInput, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }







		switch(  $objInput->error_code_test	)
		{
			case"31":
				$trace_number = "asdasd";
				break;
	
			case"32":
				$trace_number = "";
				break;	

			case"03":
				$tm4web_usr = "ASD";  // invalid truckmate user
				break;	


			case"04":
				$tm4web_usr = "";  // truckmate user or password is missing
				break;	

			
			default:
				break;
			
		}



		$objReturn = new stdClass();
		
		$objReturn->error_code_test = $objInput->error_code_test;

		if ( $tm4web_usr == "" || $tm4web_pwd == "" )
		{

			$objReturn->error = 1;
			$objReturn->error_code = "04";
			$objReturn->error_message = "Missing API Username or Password";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}
		else
		{
	
			$validInterval = 1;
				
			if ( $objInput->flag_api == "1" )
			{

				#$query = "select * from htc_nsd_truckmate_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$query = "select trace_milliseconds from htc_nsd_tracking_activity_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$db->setQuery($query);
				$objAPILog = $db->loadObject();


				$millisecondsNow = round(microtime(true) * 1000);
				
				#10000 = 10 seconds or 360 requests an hour
				#5000 = 5 seconds or 720 requests an hour
				#2000 = 2 seconds or 1200 requests an hour
				#2000 = 2 seconds or 1800 requests an hour
				#$intervalInMilliseconds = 1;
				$intervalInMilliseconds = $paramsComponent->api_rate_limit_milliseconds;
				
				
				$diffMilliseconds = $millisecondsNow - $objAPILog->trace_milliseconds;
	
				$validInterval = ( $diffMilliseconds > $intervalInMilliseconds ) ? 1 : 0 ;

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | validInterval";
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objAPILog: ".print_r($objAPILog, true);
					$logMessage .= "\n\r millisecondsNow: ".print_r($millisecondsNow, true);
					$logMessage .= "\n\r diffMilliseconds: ".print_r($diffMilliseconds, true);
					$logMessage .= "\n\r validInterval: ".print_r($validInterval, true);
					$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}	
			
			if( $validInterval == 1 )
			{

				if ( $trace_number != "" )
				{



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$objCookie = new stdClass();
					$objCookie->tm4web_usr = $tm4web_usr;
					$objCookie->tm4web_pwd = $tm4web_pwd;
					$objCookie->domain = $domain;					
					$objCookie->cookieName = $objCookie->domain.".".$objCookie->tm4web_usr;
					$objCookie->cookieFilePath = JPATH_SITE."/tmp/".$objCookie->cookieName;
			
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." |";
					$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
					
			
					if ( !JFile::exists( $objCookie->cookieFilePath ) )
					{
						#call func createCurlCookie
						#$objCookie = Nsd_trackingController::createCurlCookie( $objCookie );
						$objCookie = Nsd_truckmateController::createCurlCookie( $objCookie );
						
						if ( $objCookie->result == "1" )
						{
							$msg = "cURL cookie";
			
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$msg;
							$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }					
							
						}
						else
						{
							#return error
							
							$objReturn->error = 1;
							$objReturn->error_code = 99;
							$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password 1";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
							
						}
						
					}
					else
					{

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


						#see if cookie is stale (5 minutes)

						$fat = filemtime($objCookie->cookieFilePath);
						$sfat = strtotime( $fat );
						$dsfat = date('Y-m-d H:i:s',$fat);

						$date_a = time();
						$date_b = $fat;

						$interval_seconds = $date_a - $date_b;
						
						$interval_minutes = $interval_seconds/60;







						$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Curl Session Cookie Data";
						$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
						$logMessage .= "\n\r fat: ".print_r($fat, true);
						$logMessage .= "\n\r sfat: ".print_r($sfat, true);
						$logMessage .= "\n\r dsfat: ".print_r($dsfat, true);
						$logMessage .= "\n\r date_a: ".print_r($date_a, true);
						$logMessage .= "\n\r date_b: ".print_r($date_b, true);
						$logMessage .= "\n\r interval_seconds: ".print_r($interval_seconds, true);
						$logMessage .= "\n\r interval_minutes: ".print_r($interval_minutes, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
						

						if ( $interval_minutes > 5 )
						{

							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Curl Session Delete Cookie Begin";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



							JFile::delete( $objCookie->cookieFilePath );


							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Curl Session Delete Cookie End";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

							
							#call func createCurlCookie
							#$objCookie = Nsd_trackingController::createCurlCookie( $objCookie );
							$objCookie = Nsd_truckmateController::createCurlCookie( $objCookie );							
							

							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 3 | seconds: ".number_format($stamp_total_micro,2);
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
							if ( $objCookie->result == "1" )
							{
								$msg = "cURL cookie";
				
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$msg;
								$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }					
								
							}
							else
							{
								#return error
								
								$objReturn->error = 1;
								$objReturn->error_code = 99;
								$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
					
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password 1";
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
								
							}

							
						}

						
					}

			
/*
					$tmp_fname = tempnam("/tmp", "COOKIE");
			
			
					#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD
		
					
					$loginUrl = "https://".$domain."/login/do_login.msw";
					
					//init curl
					$ch = curl_init();
					
					//Set the URL to work with
					curl_setopt($ch, CURLOPT_URL, $loginUrl);
					
					// ENABLE HTTP POST
					curl_setopt($ch, CURLOPT_POST, 1);
					
					//Set the post parameters
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
					
					//Handle cookies for the login
					curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
					curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
					curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					
					//execute the request (the login)
					$store = curl_exec($ch);
					
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
					//$logMessage .= "\n\r store: ".print_r($store, true);
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
*/
					
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 4 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
					if ( JFile::exists( $objCookie->cookieFilePath ) )
					{

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 5 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
						$ch = curl_init();
					
						if ( $objInput->flag_api == "1" )
						{
							#get api display level and set it.
		
							$query = "select * from htc_nsd_tracking_api_clients where name = '".$objInput->tmw4web_user."' and state = '1' ";
							$db->setQuery($query);
							$objApiAgent = $db->loadObject();
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | htc_nsd_truckmate_api_clients ";
							$logMessage .= "\n\r query: ".print_r($query, true);
							$logMessage .= "\n\r objApiAgent: ".print_r($objApiAgent, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
							if ( is_object($objApiAgent) )
							{
								$objReturn->id_level = $objApiAgent->id_level;						
							}
							else
							{
								
								## NEVER HAPPEN BECAUSE THIS IS NOT AN API CALL WITH A SPECIFIC USER.  ALL CALLS ARE SYSTEM CALLS.
								
								$objReturn->error = 1;
								$objReturn->error_code = "00";
								$objReturn->error_message = "The API account is not active";					
								return $objReturn;	
							}
		
						}
						
						
						
						
						#bill number
						$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						
						//execute the request
						$contentQuery = curl_exec($ch);
						$objDataOrder = json_decode($contentQuery);
		
						
						#echo "contentQuery: <PRE>".htmlentities($contentQuery)."</PRE>";
				
/*
						if(curl_errno($ch)){
						    echo 'Curl error: ' . curl_error($ch);
						}
*/
		
				
						#print_r(curl_getinfo($ch));

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 6 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Bill Number ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
						if ( $objDataOrder->totalCount == "0" )
						{
		
							#BOL
							$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=BPTRACE&search_style=exact&trace_number=".$trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);	
							$objDataOrder = json_decode($contentQuery);				
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | BOL ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
							
						}
		
		
						if ( $objDataOrder->totalCount == "0" )
						{
		
							#po #
							$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=PPTRACE&search_style=exact&trace_number=".$trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);	
							$objDataOrder = json_decode($contentQuery);				
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | PO ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
		
							
						}
		
		
						if ( $objDataOrder->totalCount == "0" )
						{
		
							#reference1
							$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=QPTRACE&search_style=exact&trace_number=".$trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);
							$objDataOrder = json_decode($contentQuery);					
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | reference1 ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
		
							
						}
		
		
		
				
				
						$objDataOrder = json_decode($contentQuery);
				
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | jsonDecode ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r objDataOrder->dataResults[0]->BILL_NUMBER: ".print_r($objDataOrder->dataResults[0]->BILL_NUMBER, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
				
				
						#if total counts > 1, remove array with obj data where current status is cancelled or billed
				
				
						if ( $objDataOrder->totalCount > 1 )
						{
							
							$arrKey = 0;
	
							foreach( $objDataOrder->dataResults as $objData )
							{
								
									if ( $objData->CURRENT_STATUS == "CANCELLED" || $objData->CURRENT_STATUS == "BILLED" )
									{
										
										unset($objDataOrder->dataResults[$arrKey]);
		
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | testing ";
										$logMessage .= "\n\r data: ".print_r($data, true);
										$logMessage .= "\n\r objData->CURRENT_STATUS: ".print_r($objData->CURRENT_STATUS, true);
										$logMessage .= "\n\r arrKey: ".print_r($arrKey, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										
									}
	
								$arrKey += 1;
								
							}
							
							$objDataOrder->dataResults = array_values($objDataOrder->dataResults);
							
							$objDataOrder->totalCount = count($objDataOrder->dataResults);
							
						}
				
				
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | jsonDecode 2";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r objDataOrder->dataResults[0]->BILL_NUMBER: ".print_r($objDataOrder->dataResults[0]->BILL_NUMBER, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				
				
				
						if ($objInput->error_code_test == "01") { $objDataOrder = ""; }
				
				
						if ( trim($objDataOrder->dataResults[0]->BILL_NUMBER) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_B) == trim($trace_number)  || trim($objDataOrder->dataResults[0]->TRACE_TYPE_P) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_Q) == trim($trace_number)  )
						{
			
							$detailLineID = $objDataOrder->dataResults[0]->DETAIL_LINE_ID;
					
				
							$objReturn->order = $objDataOrder->dataResults[0];	
					
							$objReturn->order->agent_code = $objDataOrder->dataResults[0]->CARE_OF;
					
							switch( $paramsComponent->truckmate_version )
							{
								case "19":
									$detailURL = $connection_protocol."://".$domain."/trace/bill_details_ajax.msw?func=BillDetails&dld=".$detailLineID;												break;
								
								default:
									$detailURL = $connection_protocol."://".$domain."/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;	
									break;
							}
					
								
								
								
								#$detailURL = "https://".$domain."/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;
								
								
						
								curl_setopt ($ch, CURLOPT_HEADER, 0);
								curl_setopt ($ch, CURLOPT_HTTPGET, 1);
								curl_setopt ($ch, CURLOPT_URL, $detailURL);
								curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
								curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
								curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
								curl_setopt ($ch, CURLOPT_ENCODING, "");
								curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
								curl_setopt ($ch, CURLOPT_VERBOSE, 0);
								
								//execute the request
								$detailQuery = curl_exec($ch);
						
								#$headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT ); // request headers
								#echo "headerSent: <PRE>".htmlentities($headerSent)."</PRE>";
								#echo "detailQuery: <PRE>".htmlentities($detailQuery)."</PRE>";
								#print_r(curl_getinfo($ch));
						
						
								$curl_error = "";
								if (curl_error($ch)) {
								    $curl_error = curl_error($ch);
								}
						
								$objDataOrderDetail = json_decode($detailQuery);
								
								#$arrOrderSteps = $objDataOrderDetail->ODRSTAT->data;
								
								#$arrTraceTypes = $objDataOrderDetail->TRACE;
	
								switch( $paramsComponent->truckmate_version )
								{
									case "19":
										$objReturn->order_history = $objDataOrderDetail->ODRSTAT;
										break;
									
									default:
										$objReturn->order_history = $objDataOrderDetail->ODRSTAT->data;	
										break;
								}
						
						
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
								$logMessage .= "\n\r ch: ".print_r($ch, true);
								$logMessage .= "\n\r curl_error: ".print_r(json_decode($curl_error), true);
								$logMessage .= "\n\r detailLineID: ".print_r($detailLineID, true);
								$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
								$logMessage .= "\n\r detailQuery: ".print_r($detailQuery, true);
								$logMessage .= "\n\r objDataOrderDetail: ".print_r($objDataOrderDetail, true);
								$logMessage .= "\n\r arrOrderSteps: ".print_r($arrOrderSteps, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						
						
								curl_close($ch);
								#unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
								unset($ch);
								
								#$objReturn->order_history = $objDataOrderDetail->ODRSTAT->data;
								
								$objReturn->trace_types = $objDataOrderDetail->TRACE;
								
								$objReturn->order_details = property_exists($objDataOrderDetail, "TLDTL" ) ? $objDataOrderDetail->TLDTL->data : "";
							
								if ( count( $objReturn->order_details ) > 0 )
								{
									foreach( $objReturn->order_details as $oDetails )
									{
																				
										$oDetails->WEIGHT = ( $oDetails->WEIGHT != "" ) ?  number_format((float) $oDetails->WEIGHT, 2, '.', '') : "";

									}
									
								}
								
								
								/*

								
								$objLastDocked = Nsd_truckmateController::getOrderHistoryData( $objOrderHistory=$objReturn->order_history, $order_status="DOCKED" );
								
		
		
								# get the later of today, last docked date from order history and (if available) ETA of Docked date.						
								$arrDates = array();
								$arrDates[] = date("Y-m-d");
								$arrDates[] = date("Y-m-d", strtotime(trim($objLastDocked->OS_INS_DATE)));
								#$arrDates[] = "2018-10-09";  // if available, need to put ETA date here

								
								
								$max = max(array_map('strtotime', $arrDates));
								$objReturn->order->docked_date = date('Y-m-d', $max); 
	
	
*/
								
								
/*
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
								$logMessage .= "\n\r arrDates: ".print_r($arrDates, true);
								$logMessage .= "\n\r max: ".print_r($max, true);
								$logMessage .= "\n\r objReturn->order->docked_date: ".print_r($objReturn->order->docked_date, true);	
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
*/						
								
								
								
								
								$objReturn->error = 0;
								$objReturn->error_code = "";
								$objReturn->error_message = "";
								
	
						
						}
						elseif( $objDataOrder == "" )
						{ 
							
							$objReturn->error = 1;
							$objReturn->error_code = "01";
							#$objReturn->error_message = "Truckmate is currently unavailable";
							$objReturn->error_message = "Oops, something went wrong. This service isn't available right now - please try again later.";
							
						}
						else
						{
			
							$objReturn->error = 1;
							$objReturn->error_code = "31";
							$objReturn->error_message = "The trace number you entered cannot be found";
				
				
							#Try Rockhopper, if Rockhopper exists, transfer them to Rockhopper tracking page. If not, keep error.
							
							#Nsd_trackingController::existRockhopper( $objInput );
				
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number in system";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						}
					
				
					}
				
				}
				else
				{

		
					$objReturn->error = 1;
					$objReturn->error_code = "32";
					$objReturn->error_message = "No trace number was entered";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
				}
				
			}
			else
			{

					#NEVER HAPPEN BECAUSE INTERVAL IS ALWAYS VALID

					$objReturn->error = 1;
					$objReturn->error_code = "00";
					$objReturn->error_message = "Account exceeded the API rate limit";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Rate Limit Exceeded";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}


		}



		$objDataInsert = new stdClass();
		$objDataInsert->trace_number = $trace_number;
		$objDataInsert->datetime = date("Y-m-d H:i:s");
		$objDataInsert->datetime_gmt = gmdate("Y-m-d H:i:s");
		$objDataInsert->milliseconds = round(microtime(true) * 1000);
		$objDataInsert->error = $objReturn->error;
		$objDataInsert->error_code = $objReturn->error_code;
		$objDataInsert->error_message = $objReturn->error_message;
		$objDataInsert->objReturn = json_encode($objReturn);


		if ( $paramsComponent->enable_logging == "1")
		{

			$result = JFactory::getDbo()->insertObject('htc_nsd_truckmate_log', $objDataInsert);
			$logID = $db->insertid();

		}


		if ( $objDataInsert->error == "1" &&  $objDataInsert->error_code != "32")  // don't send 'No trace number was entered' alerts
		{

			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not retrieve from truckmate:<br>";
			$body .= "<br>objInput<br>";
			$body .= "<pre>".print_r($objInput, true)."<pre>";			
			$body .= "<br><br>";
			$body .= "<br>objReturn<br>";
			$body .= "<pre>".print_r($objReturn, true)."<pre>";
			$body .= "<br><br>";


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = "lsawyer@metalake.com";
			$objSendEmailMessage->email_from = "nsdalert@metalake.com";
			$objSendEmailMessage->email_subject = "ALERT | Nsd_dispatchtrackController | get_dispatchtrack_order";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			#$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );


/*
			#send email with error details
			$objAlert = new stdClass();
			#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			#$objAlert->recipients = array( '7039305383@txt.att.net', 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Nsd_truckmateController | getTruckmateData";
			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not retrieve from truckmate:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$objDataInsert->objReturn."<pre>";
			$body .= "<br><br>";
											
			$objAlert->body = $body;
			#$resultAlert = Nsd_truckmateController::sendAlert( $objAlert );
*/
		}



		$logMessage = $functionName." | trace: ".$trace_number." | error: ".$objReturn->error." | error code: ".$objReturn->error_code;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}


	public static function createCurlCookie( $objCookie )  //PRODUCTION
	{
		
		$controllerName = "Nsd_truckmateController";
		$functionName = "createCurlCookie";
		
		$db =& JFactory::getDBO();

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');
		$paramsComponent->truckmate_version 			= $params->get('truckmate_version');


		$connection_protocol = $paramsComponent->connection_protocol;
		$truckmate_version = $paramsComponent->truckmate_version;

		
		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		$objCookie->curl_error = "";
		$objCookie->result = "0";
		$objCookie->attempt = 1;
		$objCookie->cookieName = $objCookie->domain.".".$objCookie->tm4web_usr;
		$objCookie->cookieFilePath = JPATH_SITE."/tmp/".$objCookie->cookieName;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 0 ";
		$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


/*
		$datatopost	= array (
		  "tm4web_usr"	=> $objCookie->tm4web_usr,
		  "tm4web_pwd"	=> $objCookie->tm4web_pwd
		  );
*/		


        switch( $truckmate_version )
        {
        	case "19":
                $loginUrl = $connection_protocol."://".$objCookie->domain."/inc/tm_ajax.msw";
                $datatopost = 'tm4web_usr='.$objCookie->tm4web_usr.'&tm4web_pwd='.$objCookie->tm4web_pwd.'&remember_password=1&func=SubmitLogin';

                break;


			default:
                $loginUrl = $connection_protocol."://".$objCookie->domain."/login/do_login.msw";
                $datatopost = 'tm4web_usr='.$objCookie->tm4web_usr.'&tm4web_pwd='.$objCookie->tm4web_pwd;
                break;
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 0 ";
		$logMessage .= "\n\r datatopost: ".print_r($datatopost, true);
		$logMessage .= "\n\r loginUrl: ".print_r($loginUrl, true);		
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		while ( $objCookie->attempt <= 9 )
		{
			
			//init curl
			$ch = curl_init();
			

			//Set the URL to work with
			#curl_setopt($ch, CURLOPT_URL, $objCookie->loginUrl);
			curl_setopt($ch, CURLOPT_URL, $loginUrl);


			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 1);
			
			//Set the post parameters
			curl_setopt($ch, CURLOPT_POSTFIELDS, $datatopost);
						
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $objCookie->cookieFilePath);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
			
	
			ob_start();      // prevent any output

			$store = curl_exec ($ch); // execute the curl command
			
			if ( curl_errno($ch) )
			{
			    $objCookie->curl_error = curl_error($ch);
			}			
	
			ob_end_clean();  // stop preventing output

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
			#$logMessage .= "\n\r store: ".print_r($store, true);
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	

			
			curl_close ($ch);
			unset($ch);					
			
			if ( $store != "" )
			{

				$objCookie->result = "1";

				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 ";
				$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
				$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
				$logMessage .= "\n\r \n\r";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


				$objDataInsert = new stdClass();
				$objDataInsert->tmw_user = $objCookie->tm4web_usr;
				$objDataInsert->cookie_name = $objCookie->cookieName;
				$objDataInsert->datetime = date("Y-m-d H:i:s");;
				$objDataInsert->datetime_gmt = gmdate("Y-m-d H:i:s");
				$objDataInsert->attempts = $objCookie->attempt;
				$objDataInsert->result = $objCookie->result;
				$objDataInsert->result_error = "";
				$objDataInsert->duration = number_format($stamp_total_micro,2);
				$objDataInsert->objCookie = json_encode($objCookie);
				$result = JFactory::getDbo()->insertObject('htc_nsd_truckmate_cookie_log', $objDataInsert);
				$IID = $db->insertid();

/*
				$fp = fopen($objCookie->cookieFilePath, 'w');
				fwrite($fp, $store);
				fclose($fp);
*/

				return $objCookie;				
			}
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1.1 ";
			$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				
			
			
			$objCookie->attempt += 1;
			
			#remove cookie file
			unlink( $objCookie->cookieFilePath );
		}






		# there has been 9 attempts to create a curl cookie and it failed.
		# log
		# alert admin
		# return fail

		$objCookie->result = "0";
		$objCookie->result_error = "Reached maximum attempts to create cURL cookie for user";		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$objDataInsert = new stdClass();
		$objDataInsert->tmw_user = $objCookie->tm4web_usr;
		$objDataInsert->cookie_name = $objCookie->cookieName;
		$objDataInsert->datetime = date("Y-m-d H:i:s");;
		$objDataInsert->datetime_gmt = gmdate("Y-m-d H:i:s");
		$objDataInsert->attempts = ($objCookie->attempt - 1) ;
		$objDataInsert->result = $objCookie->result;
		$objDataInsert->result_error = $objCookie->result_error;
		$objDataInsert->duration = number_format($stamp_total_micro,2);
		$objDataInsert->objCookie = json_encode($objCookie);
		$result = JFactory::getDbo()->insertObject('htc_nsd_truckmate_cookie_log', $objDataInsert);
		$IID = $db->insertid();


		$body = "";
		$body .= "<br>Unabele to create a cURL cookie for ".$objCookie->tm4web_usr."<br>";
		$body .= "<br><br>The objCookie is: ".json_encode($objCookie)."<br>";
		

		$objSendEmailMessage = new stdClass();
		$objSendEmailMessage->email_to = "lsawyer@metalake.com";
		$objSendEmailMessage->email_from = "nsdalert@metalake.com";
		$objSendEmailMessage->email_subject = "ALERT | com_nsd_tracking | createCurlCookie";
		$objSendEmailMessage->email_body = $body;
		$objSendEmailMessage->email_category = "tracking support";

		#$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3 ";
		$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		return $objCookie;



		
	}


	public static function updateTruckmateData( $objUpdate )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=updateTruckmateData



		$controllerName = "Nsd_truckmateController";
		$functionName = "updateTruckmateData";
		
		$db = JFactory::getDBO();


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();


		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');
		$paramsComponent->truckmate_version 			= $params->get('truckmate_version');
		$paramsComponent->flag_active_server			= $params->get('flag_active_server');
		$paramsComponent->api_rate_limit_milliseconds 	= $params->get('api_rate_limit_milliseconds');
		$paramsComponent->enable_logging 				= $params->get('enable_logging');

        
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	

			
			$objData = new stdClass();
            $objData->detailLineId = $objUpdate->detail_line_id;
            $objData->windowStart = $objUpdate->requested_time_start;
            $objData->windowEnd = $objUpdate->requested_time_end;
            
            $jsonBody = json_encode($objData);

			$objReturnWorkato = Nsd_workatoController::updateTruckmateOrderScheduling( $objData );


/*
					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
*/


	
			switch( $objReturnWorkato->error  )
			{

				case "0":
					$objUpdate->error = "0";
					$objUpdate->error_code = "";
					$objUpdate->error_message = "";			
					$objUpdate->system_error_message = "";	
					break;

				
				case "1":
					$objUpdate->error = "1";
					$objUpdate->error_code = '91';
					$objUpdate->error_message = "Update API is not responding";		
					$objUpdate->system_error_message = $objReturnWorkato->system_message;		
					break;
				
				
				default:
					$objUpdate->error = "1";
					$objUpdate->error_code = '92';
					$objUpdate->error_message = "Update Failed";		
					$objUpdate->system_error_message = $objReturnWorkato->system_message;
					break;
				
				
			}
			
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
			$logMessage .= "\n\r returnData: ".print_r($returnData, true);
			$logMessage .= "\n\r objReturnWorkato: ".print_r($objReturnWorkato, true);
			$logMessage .= "\n\r objUpdate: ".print_r($objUpdate, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
		$logMessage = $functionName." | order_id: ".$objUpdate->trace_number." | dlid: ".$objUpdate->detail_line_id." | error: ".$objUpdate->error." | error code: ".$objUpdate->error_code;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
	
	
		if ( $objUpdate->error == "1" )
		{

			$body = "";
			$body .= "<br>Error Message: ".$objUpdate->error_message."<br>";
			$body .= "<br><br>The following order did not update from truckmate:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$jsonBody."<pre>";
			$body .= "<br><br>";


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = "lsawyer@metalake.com";
			$objSendEmailMessage->email_from = "nsdalert@metalake.com";
			$objSendEmailMessage->email_subject = "ALERT | Nsd_truckmateController | updateTruckmateData";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );




/*
			#send email with error details
			$objAlert = new stdClass();
			#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			#$objAlert->recipients = array( '7039305383@txt.att.net', 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Nsd_truckmateController | updateTruckmateData";
			$body = "";
			$body .= "<br>Error Message: ".$objUpdate->error_message."<br>";
			$body .= "<br><br>The following order did not update from truckmate:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$jsonBody."<pre>";
			$body .= "<br><br>";
											
			$objAlert->body = $body;
			#$resultAlert = Nsd_truckmateController::sendAlert( $objAlert );
*/
		}
	
	
	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objUpdate;


		
	}


	public static function getAgentfromZipcode( $objData )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=getAgentfromZipcode



		$controllerName = "Nsd_truckmateController";
		$functionName = "getAgentfromZipcode";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	

			#https://nonstoptest.tmwcloud.com/api/agents/search.php?zip=60016&network=BASIC&auth=dk0v930alv$ad
	
	
			$authVal = 'dk0v930alv$ad';
	
			$theUrl 	= "https://nonstoptest.tmwcloud.com/api/agents/search.php?auth=".$authVal."&zip=".$objData->zipcode."&network=".$objData->network;

			$ch = curl_init();
			
			//Set the URL to work with
			curl_setopt($ch, CURLOPT_URL, $theUrl);
			
			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 0);
			
			//Set the post parameters
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
			
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
			
			//execute the request (the login)
			$returnData = curl_exec($ch);
	
			$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);
	
			$returnDataJsonDecode = json_decode($returnData);
			
			$json_last_error = json_last_error();
			
						
			if(curl_errno($ch)){
			    $curlError = curl_error($ch);
			}
	
	
			# record returned
			
			if ( count( $returnDataJsonDecode->agents ) == "0" )
			{
				
				$objData->error = "1";
				$objData->error_code = "02";
				$objData->error_message = "Agent not returned from Truckmate.";				
				
			}
			else
			{

				# agent exists in our table?

				# production
				$agent_code = $returnDataJsonDecode->agents[0]->agent_code;
				
				#development
				#$agent_code = "COLT";



				#get agent information based upon service area 
				$query = "select * from htc_os_agents where name = '".$agent_code."' ";
		        $db->setQuery($query);
		        $objAgent = $db->loadObject() ;

				
				if ( is_object($objAgent) )
				{

					$objData->error = "0";
					$objData->error_code = "";
					$objData->error_message = "";	
					$objData->agent_code = $objAgent->name;							
					
				}				
				else
				{

					$objData->error = "1";
					$objData->error_code = "33";
					$objData->error_message = "Agent not in Scheduling System";
					
					
				}
				
			}
			
	
			
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r theUrl: ".print_r($theUrl, true);
			$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
			$logMessage .= "\n\r returnData: ".print_r($returnData, true);
			$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
			$logMessage .= "\n\r curlError: ".print_r($curlError, true);
			$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
			$logMessage .= "\n\r objData: ".print_r($objData, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
		$logMessage = $functionName." | dlid: ".$objUpdate->detail_line_id." | error: ".$objUpdate->error." | error code: ".$objUpdate->error_code;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objData;


		
	}


	public static function insertHDReturnIntoTMW( $objInput )
	{
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=importTMW

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_returns&task=importTMW

		$controllerName = "Nsd_truckmateController";
		$functionName = "insertHDReturnIntoTMW";


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();

		$paramsComponent->connection_protocol 					= $params->get('connection_protocol');

		$paramsComponent->flag_active_server_returns			= $params->get('flag_active_server_returns');

		$paramsComponent->tmw4web_user_server_returns_1			= $params->get('truckmate_user_server_returns_1');
		$paramsComponent->tmw4web_password_server_returns_1		= $params->get('truckmate_password_server_returns_1');
		$paramsComponent->tmw4web_domain_server_returns_1		= $params->get('truckmate_domain_server_returns_1');
		
		$paramsComponent->tmw4web_user_server_returns_2			= $params->get('truckmate_user_server_returns_2');
		$paramsComponent->tmw4web_password_server_returns_2		= $params->get('truckmate_password_server_returns_2');
		$paramsComponent->tmw4web_domain_serve_returns_2		= $params->get('truckmate_domain_server_returns_2');	
		
		$paramsComponent->tmw4web_user_server_returns_3			= $params->get('truckmate_user_server_returns_3');
		$paramsComponent->tmw4web_password_server_returns_3		= $params->get('truckmate_password_server_returns_3');
		$paramsComponent->tmw4web_domain_server_returns_3		= $params->get('truckmate_domain_server_returns_3');

		$connection_protocol = $paramsComponent->connection_protocol;


		switch( $paramsComponent->flag_active_server_returns )
		{

			case "3":
				$tm4web_usr = $paramsComponent->tmw4web_user_server_returns_3;
				$tm4web_pwd = $paramsComponent->tmw4web_password_server_returns_3;
				$domain 	= $paramsComponent->tmw4web_domain_server_returns_3;   
				break;

			case "2":
				$tm4web_usr = $paramsComponent->tmw4web_user_server_returns_2;
				$tm4web_pwd = $paramsComponent->tmw4web_password_server_returns_2;
				$domain 	= $paramsComponent->tmw4web_domain_server_returns_2;   
				break;
			
			case "1":	
			default:
				$tm4web_usr = $paramsComponent->tmw4web_user_server_returns_1;
				$tm4web_pwd = $paramsComponent->tmw4web_password_server_returns_1;
				$domain 	= $paramsComponent->tmw4web_domain_server_returns_1;   
				break;
							
		}

			$loginUrl 	= $connection_protocol.'://'.$domain.'/import/login/';
			$createUrl 	= $connection_protocol.'://'.$domain.'/import/orders/create';

        
/*
		$paramsComponent->hd_tmw4web_user				= $params->get('hd_truckmate_user');
		$paramsComponent->hd_tmw4web_password			= $params->get('hd_truckmate_password');
		$paramsComponent->hd_tmw4web_domain				= $params->get('hd_truckmate_domain');
		$paramsComponent->hd_flag_test_mode				= $params->get('hd_flag_test_mode');
		$paramsComponent->hd_tmw4web_user_test			= $params->get('hd_tmw4web_user_test');
		$paramsComponent->hd_tmw4web_password_test		= $params->get('hd_tmw4web_password_test');
		$paramsComponent->hd_tmw4web_domain_test		= $params->get('hd_tmw4web_domain_test');
		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');
*/


/*
		
		
		if ( $paramsComponent->hd_flag_test_mode == "0"  )
		{
			#production NSD-WEB02 to NSD-WEB01
			
			$tm4web_usr =  $paramsComponent->hd_tmw4web_user;
			$tm4web_pwd =  $paramsComponent->hd_tmw4web_password;
			$domain = $paramsComponent->hd_tmw4web_domain;   

			$loginUrl 	= $connection_protocol.'://'.$domain.'/import/login/';
			$createUrl 	= $connection_protocol.'://'.$domain.'/import/orders/create';

			#configure to use this domain: NSD-WEB01.nonstopdelivery.local:83
			#$loginUrl 	= 'http://'.$domain.'/import/login/';
			#$createUrl 	= 'http://'.$domain.'/import/orders/create';
			
			
			
		}
		else
		{

			#development from metalake server to NSD-WEB01
			
			$tm4web_usr = $paramsComponent->hd_tmw4web_user_test;
			$tm4web_pwd = $paramsComponent->hd_tmw4web_password_test;
			$domain = $paramsComponent->hd_tmw4web_domain_test;   		
			
			#configure to use this domain: nonstoptest.tmwcloud.com
			
			$loginUrl 	= $connection_protocol.'://'.$domain.'/import/login/';
			$createUrl 	= $connection_protocol.'://'.$domain.'/import/orders/create';
			
			
			
		}
*/





		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#$loginUrl 	= 'https://'.$domain.'/import/login/';
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		$logMessage .= "\n\r loginUrl: ".print_r($loginUrl, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login to TMW - Start ";
		$logMessage .= "\n\r endpoint: ".print_r($loginUrl, true);		
		$logMessage .= "\n\r";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$json_last_error = json_last_error();

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login to TMW - End ";
		$logMessage .= "\n\r";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		

		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		    
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
			$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2.5 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login Error to TMW ";
			$logMessage .= "\n\r Error: ".print_r($curlError, true);
			$logMessage .= "\n\r";
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		    
   	    
		}

		
		$objReturnCredentials = json_decode($store);
		
		$sessionId = $objReturnCredentials->sessionId;
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		$logMessage .= "\n\r objReturnCredentials: ".print_r($objReturnCredentials, true);
		$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$query = "SELECT * from htc_nsd_returns_data WHERE id = '".$objInput->dataID."'";
		$db->setQuery($query);
		$objOrderData = $db->loadObject() ;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objOrderData: ".print_r($objOrderData, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

		$query = "SELECT * from htc_nsd_returns_data_items WHERE data_id = '".$objInput->dataID."'";
		$db->setQuery($query);
		$objOrderDataItems = $db->loadObjectList() ;


		$arrControls = array( 
		"populateClientInfo"=>"TRUE",  		//hard coded
		"populateCommodityInfo"=>"TRUE",  	//hard coded
		"autoAccept"=>"TRUE",  				//hard coded
		"autoPost"=>"TRUE",  				//hard coded
		"autoUpdate"=>"TRUE"  				//hard coded
		);
		
		
		$arrDetails = array(); 
		foreach( $objOrderDataItems as $item )
		{
			$arrDetails[] = array(
				"description"		=>substr($item->goods_description, 0, 50),
				"pieces"			=>substr($item->goods_pieces, 0, 4),
				"piecesUnits"		=>substr($item->goods_code_clean, 0, 3),
				"weight"			=>substr( round($item->goods_weight,2), 0, 8 ),
				"weightUnits"		=>"LB",
				"commodity"			=>substr($item->goods_commodity, 0, 10 )				
			);
		}


		

		$arrTraces = array(); 
		$arrTraces[] = array( 
		"traceType"=>"P",							//hard coded
		"traceNumber"=>$objOrderData->reference1,
		"refQualifier"=>"PO"							//empty
		);

		$arrTraces[] = array( 
		"traceType"=>"Q",							//hard coded
		"traceNumber"=>$objOrderData->reference2,
		"refQualifier"=>"OMS"							//empty
		);


		if ( $objOrderData->reference3 != "" )
		{
			$arrTraces[] = array( 
			"traceType"=>"R",							
			"traceNumber"=>$objOrderData->reference3,
			"refQualifier"=>""							
			);
		}


		if ( $objOrderData->return_auth != "" )
		{
			$arrTraces[] = array( 
			"traceType"=>"W",							
			"traceNumber"=>$objOrderData->return_auth,
			"refQualifier"=>""							
			);
		}

		
		
		switch( $objOrderData->destination_code )
		{
			case "11106":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8615",
				"refQualifier"=>"SN"
				);
				break;

			case "11107":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8616",
				"refQualifier"=>"SN"
				);
				break;

			case "11108":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8617",
				"refQualifier"=>"SN"
				);
				break;
			
		}
		


		if ( strtoupper($objOrderData->disposal) == "TRUE" || strtoupper($objOrderData->disposal) == "YES" || $objOrderData->disposal == "1" )
		{

			$arrTraces[] = array( 
			"traceType"=>"3",
			"traceNumber"=>"LM",
			"refQualifier"=>"OPS"
			);

			$arrTraces[] = array( 
			"traceType"=>"S",						
			"traceNumber"=>"SHIPINSTRUCT12", 		
			"refQualifier"=>"SHP" 					
			);		
			
		}
		else
		{
			$arrTraces[] = array( 
			"traceType"=>"3",
			"traceNumber"=>"DD",
			"refQualifier"=>"OPS"
			);			
		}




		$arrNotes = array(); 

		if ( $objOrderData->origin_additionalInfo1 != "" )
		{
			$arrNotes[] = array( 
			"noteType"=>"3",
			"theNote"=>$objOrderData->origin_additionalInfo1
			);			
		}


		
		

		$currentDateTime = date("Y-m-d-H.i.s.00000");



		$arrOrders = array();
		$arrOrders[] = array( 
		"billToCode"	=>"10343",  						//hard coded
		"serviceLevel"	=>$objOrderData->service_level_clean,
		"customer"		=>"10343",  							//hard coded
		"destination"	=>$objOrderData->destination_code,
		"destname"		=>strtoupper( $objOrderData->destination_name_clean ),

		"pickUpBy"		=>$currentDateTime,
		"pickUpByEnd"	=>$currentDateTime,
		
		"deliverBy"		=>$currentDateTime,
		"deliverByEnd"	=>$currentDateTime,

		"destaddr1"		=>strtoupper( $objOrderData->destination_addressLine1." ".$objOrderData->destination_addressLine2 ),
		"destcity"		=>strtoupper( $objOrderData->destination_city ),
		"destprov"		=>strtoupper( $objOrderData->destination_state ),
		"destpc"		=>strtoupper( substr($objOrderData->destination_zipCode, 0, 5) ),
		"destphone"		=>"",
		"destemail"		=>"",
		"origin"		=>"",
		"origname"		=>strtoupper( substr($objOrderData->origin_name, 0, 40) ),
		"origaddr1"		=>strtoupper( substr( ($objOrderData->origin_addressLine1." ".$objOrderData->origin_addressLine2), 0, 40) ),
		"origcity"		=>strtoupper( substr($objOrderData->origin_city, 0, 30) ),
		"origprov"		=>strtoupper( substr($objOrderData->origin_state, 0, 4) ),
		"origpc"		=>strtoupper( substr($objOrderData->origin_zipCode, 0, 10) ),
		"origphone"		=>strtoupper( substr($objOrderData->origin_phone, 0, 20) ),
		"origemail"		=>strtoupper( substr($objOrderData->origin_email, 0, 40) ),
		"siteId"		=>"SITE5",   							//hard coded
		"startZone"		=>strtoupper( substr($objOrderData->origin_zipCode, 0, 5) ),
		"billTo"		=>"C",  								//hard coded
		"endZone"		=>substr( $objOrderData->destination_zipCode, 0, 5 ),
		"details"		=>$arrDetails,
		"traces"		=>$arrTraces,
		"notes"			=>$arrNotes, 
		);



		$arrBody = array( 
		"sessionId"=>$sessionId,
		"controls"=>$arrControls,
		"orders"=>$arrOrders
		);
		
		$jsonBody = json_encode($arrBody);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		#$createUrl 	= 'https://'.$domain.'/import/orders/create';
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $createUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 3 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_start_micro_post = microtime(true);	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 4 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Post to TMW - Start ";
		$logMessage .= "\n\r endpoint: ".print_r($createUrl, true);		
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		
		//execute the request
		$returnData = curl_exec($ch);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		
		$stamp_total_micro_post = ($stamp_end_micro - $stamp_start_micro_post);
		
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 5 | post seconds: ".number_format($stamp_total_micro_post,2) ." |   total seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Post to TMW - End ";
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);

		$returnDataJsonDecode = json_decode($returnData);
		
		$json_last_error = json_last_error();
		
		$detailURL = $returnDataJsonDecode[0]->href;
		
		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
			    
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
			$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 5.5 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Post Error to TMW ";
			$logMessage .= "\n\r Error: ".print_r($curlError, true);
			$logMessage .= "\n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		    
		    
		    
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r createUrl: ".print_r($createUrl, true);
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		//init curl
		#$ch = curl_init();
		
		$detailURL = ( $paramsComponent->hd_flag_test_mode == "0"  ) ?  $detailURL : str_replace("http", $connection_protocol, $detailURL);
		
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $detailURL);
		curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 6 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		//execute the request
		$detailData = curl_exec($ch);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 7 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$detailDataJsonDecode = json_decode($detailData);
		
		$orderId = $detailDataJsonDecode->orderId;
		$billNumber = $detailDataJsonDecode->billNumber;
		
		
		$objImportReturn = new stdClass();
		$objImportReturn->orderId = $orderId;
		$objImportReturn->billNumber = $billNumber;	
		$objImportReturn->message = isset($returnDataJsonDecode[0]->Message) ? $returnDataJsonDecode[0]->Message : "";

		
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r detailData: ".print_r($detailData, true);
		$logMessage .= "\n\r detailDataJsonDecode: ".print_r($detailDataJsonDecode, true);
		$logMessage .= "\n\r orderId: ".print_r($orderId, true);
		$logMessage .= "\n\r billNumber: ".print_r($billNumber, true);
		$logMessage .= "\n\r objImportReturn: ".print_r($objImportReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }





		curl_close($ch);
		unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
		unset($ch);



		


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objImportReturn;
	}




// !TRACE PAGE FUNCTIONS


	public static function getTruckmateListOrderFromWorkato( $objForm )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=getTruckmateOrderFromWorkato
		# http://dev6.metalake.net/index.php?option=com_nsd_truckmate&task=getTruckmateOrderFromWorkato


		$controllerName = "Nsd_truckmateController";
		$functionName = "getTruckmateListOrderFromWorkato";

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$objTMW = Nsd_workatoController::getTruckmateListOrderData( $objForm );


		if ( $objTMW->message == "Endpoint recipe is not running"  )
		{
			$objReturn = new stdClass();
			$objReturn->error = "1";
			$objReturn->error_message = $objTMW->message;
			$objReturn->rowresults = 0;
		}
		else
		{
			

			$objReturn = new stdClass();
			$objReturn->error = "0";
			$objReturn->error_message = "";
			$objReturn->rowresults = $objTMW->rowresults;
				
				$arrOrders = array();
				
				foreach( $objTMW->orders as $order )
				{
					
					$objOrder = new stdClass();				
					$objOrder->BILLNUMBER = $order->BILLNUMBER;
					$objOrder->SITEID = $order->SITEID;
					$objOrder->CALLNAME = $order->CALLNAME;
					$objOrder->ORDERTYPE = $order->ORDERTYPE;
					$objOrder->ORIGNAME = $order->ORIGNAME;
					$objOrder->ORIGCITY = $order->ORIGCITY;
					$objOrder->ORIGPROV = $order->ORIGPROV;
					$objOrder->ORIGPHONE = $order->ORIGPHONE;
					$objOrder->DESTNAME = $order->DESTNAME;
					$objOrder->DESTCITY = $order->DESTCITY;
					$objOrder->DESTPROV = $order->DESTPROV;
					$objOrder->DESTPHONE = $order->DESTPHONE;
					$objOrder->ORDERSTATUS = $order->ORDERSTATUS;
					$objOrder->ROWTIMESTAMP = $order->ROWTIMESTAMP;
					$arrOrders[] = $objOrder;
				}
				
				#rsort($arrOrderHistory);
	
				$objReturn->orders = $arrOrders;
				
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
	
	
			$arrOrders = array();
			foreach( $objReturn->orders as $order )
			{
	
	
				$pos = strpos(strtoupper($order->ORIGNAME), strtoupper($objForm->trace_number)) ;
	
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r order->ORIGNAME: ".print_r($order->ORIGNAME, true);
			$logMessage .= "\n\r objForm->trace_number: ".print_r($objForm->trace_number, true);
			$logMessage .= "\n\r pos: ".print_r($pos, true);
			$logMessage .= "\n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
	
	
				IF ($pos === false)
				{
	
					$objOrder = new stdClass();				
					$objOrder->BILLNUMBER = $order->BILLNUMBER;
					$objOrder->CALLNAME = $order->CALLNAME;
					$objOrder->ORDERTYPE = $order->ORDERTYPE;
					$objOrder->SHIPPERNAME = $order->DESTNAME; 
					$objOrder->SHIPPERCITY = $order->DESTCITY;;
					$objOrder->SHIPPERPROV = $order->DESTPROV;
					$objOrder->SHIPPERPHONE = $order->DESTPHONE;
					$objOrder->SHIPPERPHONE = $order->DESTPHONE;
					$objOrder->ORDERSTATUS = $order->ORDERSTATUS;
					$objOrder->ROWTIMESTAMP = $order->ROWTIMESTAMP;
					$objOrder->LINK = "/trace?a=".$objForm->histUpdatedByEmail."&t=".$order->BILLNUMBER."&r=1&s=1";
					$arrOrders[] = $objOrder;
					
				}
				else			
				{
		
					$objOrder = new stdClass();				
					$objOrder->BILLNUMBER = $order->BILLNUMBER;
					$objOrder->CALLNAME = $order->CALLNAME;
					$objOrder->ORDERTYPE = $order->ORDERTYPE;
					$objOrder->SHIPPERNAME = $order->ORIGNAME ;
					$objOrder->SHIPPERCITY = $order->ORIGCITY ;
					$objOrder->SHIPPERPROV = $order->ORIGPROV ;
					$objOrder->SHIPPERPHONE = $order->ORIGPHONE ;
					$objOrder->ORDERSTATUS = $order->ORDERSTATUS ;
					$objOrder->ROWTIMESTAMP = $order->ROWTIMESTAMP;
					$objOrder->LINK = "/trace?a=".$objForm->histUpdatedByEmail."&t=".$order->BILLNUMBER."&r=1&s=1";
					$arrOrders[] = $objOrder;	
					
				}
			
	
	
			
				
				
			}
			
			$objReturn->orders = $arrOrders;
	

		}


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}


	public static function getTruckmateOrderFromWorkato( $objForm )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=getTruckmateOrderFromWorkato
		# http://dev6.metalake.net/index.php?option=com_nsd_truckmate&task=getTruckmateOrderFromWorkato


		$controllerName = "Nsd_truckmateController";
		$functionName = "getTruckmateOrderFromWorkato";

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$objTMW = Nsd_workatoController::getTruckmateOrderData( $objForm );


		if ( $objTMW->return == "Error"  )
		{
			$objReturn = new stdClass();
			$objReturn->error = "1";
			$objReturn->error_message = $objTMW->message;
			$objReturn->rowresults = 0;
		}
		else
		{


			$objReturn = new stdClass();
			$objReturn->error = "0";
	
				$objOrder = new stdClass();
	            $objOrder->DETAIL_LINE_ID = $objTMW->detailLineId;
	            $objOrder->BILL_NUMBER = $objTMW->trackingNumber;
	            
	            
	            $objOrder->CUSTOMER 		= $objTMW->customer;
	            $objOrder->CALLNAME 		= $objTMW->callName;
	            
	            $objOrder->OP_CODE 			= $objTMW->opCode;
	            $objOrder->SITE_ID 			= $objTMW->siteId;
	            $objOrder->SERVICE_LEVEL	= $objTMW->serviceLevel;
	            $objOrder->CURRENT_STATUS	= $objTMW->currentStatus;
	            $objOrder->agent_code 		= $objTMW->detailLineId;
	            
	
				$objOrder->ORIGID 		= $objTMW->origId;
	            $objOrder->ORIGNAME 	= $objTMW->origin;
	            $objOrder->ORIGADDR1  	= $objTMW->originAddress1;
	            $objOrder->ORIGADDR2 	= $objTMW->originAddress2;
	            $objOrder->ORIGCITY 	= $objTMW->originCity;
	            $objOrder->ORIGPROV 	= $objTMW->originProv;
	            $objOrder->ORIGCOUNTRY 	= "";
	            $objOrder->ORIGPC 		= $objTMW->originPC;
	            $objOrder->ORIGPHONE 	= $objTMW->originPhone;
	            $objOrder->ORIGPHONEEXT	= $objTMW->originPhoneExt;
	            $objOrder->ORIGCELL		= $objTMW->originCell;
	            $objOrder->ORIGEMAIL 	= $objTMW->originEmail;
	
	            $objOrder->DESTNAME 	= $objTMW->destination;
	            $objOrder->DESTADDR1  	= $objTMW->destinationAddress1;           
	            $objOrder->DESTADDR2  	= $objTMW->destinationAddress2;
	            $objOrder->DESTCITY  	= $objTMW->destinationCity;
	            $objOrder->DESTPROV  	= $objTMW->destinationProv;
	            $objOrder->DESTPC  		= $objTMW->destinationPC;
	            $objOrder->DESTPHONE  	= $objTMW->destinationPhone;
	            $objOrder->DESTPHONEEXT	= $objTMW->destinationPhoneExt;
	            $objOrder->DESTEMAIL  	= $objTMW->destinationEmail;
	            $objOrder->DESTCELL  	= $objTMW->destinationCell;
	
	            $objOrder->CARE_OF  		= $objTMW->careOf;
	            $objOrder->CARE_OF_NAME  	= $objTMW->careOfName;
	            $objOrder->CARE_OF_ADDR1  	= $objTMW->careOfAddress1;
	            $objOrder->CARE_OF_ADDR2  	= $objTMW->careOfAddress2;
	            $objOrder->CARE_OF_CITY  	= $objTMW->careOfCity;
	            $objOrder->CARE_OF_PROV  	= $objTMW->careOfProv;
	            $objOrder->CARE_OF_PC  		= $objTMW->careOfPC;
	            $objOrder->CARE_OF_PHONE  	= $objTMW->careOfPhone;
	            $objOrder->CARE_OF_PHONEEXT = $objTMW->careOfPhoneExt;
	            $objOrder->CARE_OF_EMAIL 	= $objTMW->careOfEmail;
	            $objOrder->CARE_OF_ZONE  	= $objTMW->careOfZone;
	                        
	            $objOrder->DELIVER_BY 			= $objTMW->scheduledDeliveryDateStart;
	            $objOrder->DELIVER_BY_END 		= $objTMW->scheduledDeliveryDateEnd;
	            $objOrder->DELIVERY_APPT_MADE 	= $objTMW->scheduledDeliveryApptMade;
	            $objOrder->PICK_UP_BY 			= $objTMW->scheduledPickupDateStart;
	            $objOrder->PICK_UP_BY_END 		= $objTMW->scheduledPickupDateEnd;
	            $objOrder->PICK_UP_APPT_MADE 	= $objTMW->scheduledPickupApptMade;
	            $objOrder->USER8 				= $objTMW->user8;
	            $objOrder->USER9 				= $objTMW->user9;
	            $objOrder->USER5 				= $objTMW->user5;
	            
	            
				$objReturn->order = $objOrder;
				
				$arrOrderHistory = array();
				
				foreach( $objTMW->orderHistory as $history )
				{
					
					$objOrderHistory = new stdClass();				
					$objOrderHistory->OS_INS_DATE = $history->insertDate;
					$objOrderHistory->OS_CHANGED = $history->changeDate;
					$objOrderHistory->OS_STATUS_CODE = $history->statusCode;
					$objOrderHistory->OS_SF_REASON_CODE = $history->reasonCode;
					$objOrderHistory->OS_STAT_COMMENT = $history->statusDescription;
					$objOrderHistory->OS_UPDATED_BY = $history->updatedBy;
					$arrOrderHistory[] = $objOrderHistory;
				}
				
				#rsort($arrOrderHistory);
	
				$objReturn->order_history = $arrOrderHistory;
	
	
	
	
	
				$arrReferences = array();
				
				foreach( $objTMW->references as $reference )
				{
					
					$objReference = new stdClass();				
					$objReference->TRACE_TYPE = $reference->type;
					$objReference->TRACE_NUMBER = $reference->value;
					$arrReferences[] = $objReference;
				}
		
	
				$objReturn->trace_types = $arrReferences;
				
				
	
	
				$arrItems = array();
				
				foreach( $objTMW->lineItems as $item )
				{
					
					$objItem = new stdClass();				
					$objItem->DESCRIPTION = $item->description;
					$objItem->PIECES = $item->pieces;
					$objItem->PIECES_UNITS = $item->piecesUnits;
					$objItem->WEIGHT = $item->weight;
					$objItem->WEIGHT_UNITS = $item->weightUnits;
					$objItem->SEQUENCE = $item->sequence;
					
					$arrItems[] = $objItem;
				}
		
	
				$objReturn->order_details = $arrItems;
	
				
	
				$arrNotes = array();
				
				foreach( $objTMW->notes as $note )
				{
					
					if ( $note->noteType == "D" )
					{
	
											
						$noteExpanded = nl2br($note->note);
						
						$objNote = new stdClass();				
						$objNote->noteType = $note->noteType;
						$objNote->note = $note->note;
						$objNote->noteExpanded = $noteExpanded;
						$objNote->createdOn = $note->createdOn;
						$objNote->createdBy = $note->createdBy;
					
						$arrNotes[] = $objNote;
					}
					
				}
		
	
				$objReturn->notes = $arrNotes;


				$arrShippingInstructions = array();
				
				foreach( $objTMW->shippingInstructions as $instruction )
				{
					
					$objShippingInstruction = new stdClass();				
					$objShippingInstruction->Instruction = $instruction;
					
					$arrShippingInstructions[] = $instruction->shippingInstruction;

				}
		
	
				sort($arrShippingInstructions);
	
				$objReturn->shipping_instructions = $arrShippingInstructions;



				$arrShippingInstructionsPOD = array();
				
				foreach( $objTMW->shippingInstructions as $instruction )
				{
					$firstVal = substr($instruction->shortDescription,0,1);
					
					if ( $firstVal != "_" )
					{
						
						$instruction->shortDescriptionView = substr($instruction->shortDescription, strpos($instruction->shortDescription, "-") + 2);
						
						if ( $instruction->instructionID == "24" )
						{
							$instruction->shortDescriptionView = "WAS THERE ANY DAMAGE CAUSED TO YOUR HOME OR PROPERTY WHILE THE NSD TEAM WAS ONSITE?";
						}
						
						
						$arrShippingInstructionsPOD[] = $instruction;
					}

					
				}


				$logMessage = "INSIDE before sort | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r arrShippingInstructionsPOD: ".print_r($arrShippingInstructionsPOD, true);
				$logMessage .= "\n\r";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	

				usort($arrShippingInstructionsPOD, array("Nsd_truckmateController", "sort_objects_by_SDV"));

				$logMessage = "INSIDE after sort | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r arrShippingInstructionsPOD: ".print_r($arrShippingInstructionsPOD, true);
				$logMessage .= "\n\r";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
		

				$objReturn->shipping_instructions_pod = $arrShippingInstructionsPOD;

			
			
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}



	public function sort_objects_by_SDV($a, $b) {
	    if ($a->shortDescriptionView == $b->shortDescriptionView) { return 0; }
	    return ($a->shortDescriptionView < $b->shortDescriptionView) ? -1 : 1;
	}


// !COMMON FUNCTIONS


	public static function sendAlert( $objAlert )
	{

		$mailer =& JFactory::getMailer();
		$config = JFactory::getConfig();

		$sender = array( 
		    $config->get( 'mailfrom' ),
		    $config->get( 'fromname' ) 
		);
		$mailer->setSender($sender);

		$mailer->addRecipient( $objAlert->recipients );

		$mailer->setSubject( $objAlert->subject );
		$mailer->setBody( $objAlert->body );
		$mailer->isHTML(true);

		if ($mailer->Send() !== true)
		{
			$return = 0;			
		}			
		else
		{
			$return = 1;			
		}


		return $return;
	}



	public static function logActivity( $objActivityLog )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=logActivity

		$controllerName = "Nsd_truckmateController";
		$functionName = "logActivity";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objData = new stdClass();
		$objData->remote_address = $objActivityLog->remote_address;
		$objData->trace = $objActivityLog->trace;
		$objData->trace_type = $objActivityLog->trace_type;
		$objData->delivery_type = $objActivityLog->delivery_type;
		$objData->order_type = $objActivityLog->order_type;
		$objData->json_return = $objActivityLog->json_return;
		$objData->trace_datetime = date("Y-m-d H:i:s");
		$objData->trace_datetime_gmt = gmdate("Y-m-d H:i:s");
		$objData->trace_milliseconds = round(microtime(true) * 1000);
		$objData->tm4web_usr = $objActivityLog->tm4web_usr;
		$objData->flag_api = $objActivityLog->flag_api;
		$objData->return_format = $objActivityLog->return_format;
		
		$result = JFactory::getDbo()->insertObject('htc_nsd_truckmate_log', $objData);
		$logID = $db->insertid();


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public static function getOrderHistoryData( $objorderHistory, $order_status )
	{

		$controllerName = "Nsd_truckmateController";
		$functionName = "getOrderHistoryData";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		


				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r objorderHistory: ".print_r($objorderHistory, true);
				$logMessage .= "\n\r order_status: ".print_r($order_status, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		foreach( $objorderHistory as $history )
		{
			
			if ( $history->OS_STATUS_CODE == $order_status )
			{


				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r history: ".print_r($history, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		
		
				$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
				$logMessage .= "\n\r \n\r";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

				$history->history_exists = "1";
				
				return $history;
			}
			
		}


		$objReturn = new stdClass();
		$objReturn->history_exists = "0";
		$objReturn->OS_CHANGED = "";


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;			
		
	}



	public static function pruneLogs()
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_truckmate&task=pruneLogs
		
		$controllerName = "Nsd_truckmateController";
		$functionName = "pruneLogs";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrTables = array();
		$arrTables[] = "htc_nsd_truckmate_log";

		#if ( date('H') == '23' )
		
		if ( TRUE )
		{
			foreach( $arrTables as $table )
			{
				$query = "select count(z.id) from ".$table." z";
				$db->setQuery($query);
				$recordCount = $db->loadResult();

				$logMessage = "INSIDE | Ml_apiController | ".$functionName;
				$logMessage .= "\n\r recordCount for ".$table.": ".print_r($recordCount, true);
				$logMessage .= "\n\r dateH: ".print_r(date('H'), true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				if ( $recordCount > 600000 )
				{
		
					$query = "select z.id from ".$table." z order by z.id desc limit 500000, 1";
					$db->setQuery($query);
					$idRecord = $db->loadResult();
			
					if ( $idRecord != "" )
					{
						$queryD = "Delete FROM ".$table." WHERE id <= '".$idRecord."' ";
						$db->setQuery($queryD);
						$db->query();
					}
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r idRecord: ".print_r($idRecord, true);
					$logMessage .= "\n\r queryD: ".print_r($queryD, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				}


			}
		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


// !TEST FUNCTIONS




	public function test_getTruckmateData( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_truckmate&task=test_getTruckmateData&trace=8151225



		$controllerName = "Nsd_truckmateController";
		$functionName = "test_getTruckmateData";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$jinput = JFactory::getApplication()->input;
		$trace_number = $jinput->get('trace', '', RAW);


        $objInput = new stdClass();
        $objInput->flag_api = "0";
        $objInput->trace_number = $trace_number;
        #$objInput->trace_number = "2000113702";
        


		$objReturn = Nsd_truckmateController::getTruckmateData( $objInput );




		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




	public function test_updateTruckmateData( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=test_updateTruckmateData



		$controllerName = "Nsd_truckmateController";
		$functionName = "test_updateTruckmateData";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

	
		#for test order : 10015435
	
		$objUpdate = new stdClass();
		$objUpdate->error = "0";
		$objUpdate->error_code = "";
		$objUpdate->error_message = "";
		$objUpdate->detail_line_id = "137837";
		$objUpdate->requested_time_start = "2018-11-22 13:00:00";
		$objUpdate->requested_time_end = "2018-11-22 16:00:00";	
	
	
	
			$arrBody = array( 
			"detail_line_id"=>$objUpdate->detail_line_id,
			"deliver_by"=>$objUpdate->requested_time_start,
			"deliver_by_end"=>$objUpdate->requested_time_end
			);
			
			$jsonBody = json_encode($arrBody);
	
	
	
			$createUrl 	= 'https://nonstoptest.tmwcloud.com/api/freightbills/update.php?auth=dk0v930alv$ad';

			$ch = curl_init();
			
			//Set the URL to work with
			curl_setopt($ch, CURLOPT_URL, $createUrl);
			
			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 1);
			
			//Set the post parameters
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
			
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
			
			//execute the request (the login)
			$returnData = curl_exec($ch);
	
			$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);
	
			$returnDataJsonDecode = json_decode($returnData);
			
			$json_last_error = json_last_error();
			
						
			if(curl_errno($ch)){
			    $curlError = curl_error($ch);
			}
			
	
			switch( $returnDataJsonDecode->Result  )
			{

				case "Freight bill was updated":
					$objUpdate->error = "0";
					$objUpdate->error_code = "";
					$objUpdate->error_message = "";				
					break;

				
				case "":
					$objUpdate->error = "1";
					$objUpdate->error_code = "1";
					$objUpdate->error_message = "Truckmate is currently unavailable";				
					break;
				
				default:
					$objUpdate->error = "1";
					$objUpdate->error_code = "2";
					$objUpdate->error_message = "Unable to update Freight Bill";	
					break;
				
				
			}
	
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r createUrl: ".print_r($createUrl, true);
			$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
			$logMessage .= "\n\r returnData: ".print_r($returnData, true);
			$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
			$logMessage .= "\n\r curlError: ".print_r($curlError, true);
			$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
			$logMessage .= "\n\r objUpdate: ".print_r($objUpdate, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;


		
	}



	public function test_tmw_trace( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=test_tmw_trace&trace=31143093

		$controllerName = "Nsd_truckmateController";
		$functionName = "test_tmw_trace";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$jinput = JFactory::getApplication()->input;
		$trace_number = $jinput->get('trace', '', RAW);

		#test
		$wsdl = "https://nonstoptest.tmwcloud.com/webservices/ws_trace/index.msw?wsdl";

		#prod
		#$wsdl = "https://nonstopdelivery.tmwcloud.com/webservices/ws_trace/index.msw?wsdl";

		$arrSoapClient = array('connection_timeout' => 0, 'trace' => 1, 'compression' => 'SOAP_COMPRESSION_ACCEPT');


		$soap_client = new SoapClient( $wsdl, $arrSoapClient );



		$params_trace = Nsd_truckmateController::poc_trace_set_parameters( $trace_number );


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r arrSoapClient: ".print_r($arrSoapClient, true);
			$logMessage .= "\n\r soap_client: ".print_r($soap_client, true);
			$logMessage .= "\n\r params_trace: ".print_r($params_trace, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		try 
		{
			$Trace_Result = $soap_client->Trace( $params_trace );
		} 
		catch (SoapFault $exception) 
		{
			#echo '<br><br>EXCEPTION = <br>'.$exception;
			#echo "<br><br>EX RESPONSE: " . $soap_client->__getLastResponse() ."<br/>";
			#echo "<br/><br/>exception->getMessage: ".$exception->getMessage() ."<br/>";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r exception: ".print_r($exception, true);
			$logMessage .= "\n\r exception response: ".print_r($soap_client->__getLastResponse(), true);
			$logMessage .= "\n\r exception->getMessage: ".print_r($exception->getMessage(), true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		}
		
		
		if( isset( $Trace_Result ) )
		{
		
			echo "<br/><br/> Trace results from nonstoptest.tmwcloud.com: ";
			echo "<pre>".print_r($Trace_Result, true)."</pre>";


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r Trace_Result: ".print_r($Trace_Result, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		}
		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




	public function poc_trace_set_parameters( $trace_number )
	{

		//$trace_attributes = "T00113,~|L00031,~";
		#$trace_attributes = "R1147751,~";
		$trace_attributes = $trace_number.",~";
		
		
		//$output_attributes = "TLORDER=UNIQUE_ID, USER3,DETAIL_LINE_ID,TOTAL_CHARGES";
		
		//$output_attributes = "TLORDER=DETAIL_LINE_ID, USER3, CREATED_TIME,CREATED_BY, CALLER, PICK_UP_BY, DELIVER_BY, SERVICE_LEVEL, CURRENT_ZONE,START_ZONE, END_ZONE, BILL_NUMBER, BILL_TO_CODE, BILL_TO, DECLARED_VALUE,TOTAL_CHARGES, CURRENCY_CODE"; //|ACHARGE_TLORDER=ACODE_ID";
		
		//$output_attributes = "TLORDER=BILL_NUMBER, TOTAL_CHARGES,CURRENCY_CODE|TLDTL=RATE";
		
		
		
		$arrOuput = array();
		
		#$arrOuput[] = "order_type";  /don't use
		$arrOuput[] = "BILL_NUMBER";
		$arrOuput[] = "detail_line_id";
		#$arrOuput[] = "bill_to_code";
		#$arrOuput[] = "CustomsNumber";
		$arrOuput[] = "pick_up_by";
		$arrOuput[] = "pick_up_by_end";
		$arrOuput[] = "deliver_by";
		$arrOuput[] = "deliver_by_end";
		$arrOuput[] = "site_id";
		$arrOuput[] = "bill_to";
		$arrOuput[] = "start_zone";
		$arrOuput[] = "end_zone";
		$arrOuput[] = "service_level";
		$arrOuput[] = "caller";
		$arrOuput[] = "shipper";
		$arrOuput[] = "consignee";
		#$arrOuput[] = "master_order";
		#$arrOuput[] = "extra_stops";
		#$arrOuput[] = "FBDetails";
		$arrOuput[] = "FBTraceNumbers";


		sort($arrOuput);		
		
		$strOutput = implode( ', ', $arrOuput );
		
		$strOutput = strtoupper($strOutput);
		
		$output_attributes = "TLORDER=".$strOutput;
		
		
		
		#$output_attributes = "TLORDER=BILL_NUMBER, USER3, DELIVER_BY, site_id, caller";
		
		
		$parameters['trace_attributes'] = $trace_attributes;
		
		$parameters['output_attributes'] = $output_attributes;
		
		return $parameters;

	}


	public function test_getAgentfromZipcode( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=test_getAgentfromZipcode



		$controllerName = "Nsd_truckmateController";
		$functionName = "test_getAgentfromZipcode";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objData = new stdClass();
		$objData->zipcode = "01001";
		$objData->network = "BASIC";

		$objReturn = Nsd_truckmateController::getAgentfromZipcode( $objData );


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

	public static function test_tmwLogin( )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=test_tmwLogin
		# http://nsddev.metalake.net/index.php?option=com_nsd_truckmate&task=test_tmwLogin


		$controllerName = "Nsd_truckmateController";
		$functionName = "test_tmwLogin";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	


		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('truckmate_user');
		$paramsComponent->tmw4web_password			= $params->get('truckmate_password');
		$paramsComponent->tmw4web_domain			= $params->get('truckmate_domain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_domain_test		= $params->get('tmw4web_domain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		$paramsComponent->enable_logging 			= $params->get('enable_logging');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_domain;   
			
		}
		else
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_domain_test;   		
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		#$tmp_fname = tempnam("/tmp", "COOKIE");

		$tmp_fname = "/tmp/cookieFileName";
		$tmp_fname = JPATH_SITE."/tmp/ChocChipCookie";


		#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD

		
		$loginUrl = "https://".$domain."/login/do_login.msw";
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		//$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);


		ob_start();      // prevent any output
		curl_exec ($ch); // execute the curl command
		ob_end_clean();  // stop preventing output
		
		curl_close ($ch);
		unset($ch);


/*

		#bill number
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_HEADER, 0);
		
		//execute the request
		$contentQuery = curl_exec($ch);
		$objDataOrder = json_decode($contentQuery);

		$ch = curl_init();

		$trace_number = "2000113701";

		$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$trace_number;
		
		curl_setopt ($ch, CURLOPT_HTTPGET, 1);
		curl_setopt ($ch, CURLOPT_URL, $queryURL);



		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, "/tmp/cookieFileName");
		
		$buf2 = curl_exec ($ch);
		
		curl_close ($ch);
		
		#echo "<PRE>".htmlentities($buf2);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r buf2: ".print_r($buf2, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
*/


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

	public static function test_tmwLogin2( )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=test_tmwLogin2
		# http://nsddev.metalake.net/index.php?option=com_nsd_truckmate&task=test_tmwLogin2


		$controllerName = "Nsd_truckmateController";
		$functionName = "test_tmwLogin2";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	


		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('truckmate_user');
		$paramsComponent->tmw4web_password			= $params->get('truckmate_password');
		$paramsComponent->tmw4web_domain			= $params->get('truckmate_domain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_domain_test		= $params->get('tmw4web_domain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		$paramsComponent->enable_logging 			= $params->get('enable_logging');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_domain;   
			
		}
		else
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_domain_test;   		
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		#$tmp_fname = tempnam("/tmp", "COOKIE");

		#$tmp_fname = "/tmp/cookieFileName1";
		$tmp_fname = JPATH_SITE."/tmp/ChocChipCookie";


		#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD

/*
		
		$loginUrl = "https://".$domain."/login/do_login.msw";
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		//$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);


		ob_start();      // prevent any output
		curl_exec ($ch); // execute the curl command
		ob_end_clean();  // stop preventing output
		
		curl_close ($ch);
		unset($ch);
*/

		$ch = curl_init();

		#bill number
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_HEADER, 0);
		
		//execute the request
		$contentQuery = curl_exec($ch);
		$objDataOrder = json_decode($contentQuery);



		$trace_number = "2000114444";

		$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$trace_number;
		
		curl_setopt ($ch, CURLOPT_HTTPGET, 1);
		curl_setopt ($ch, CURLOPT_URL, $queryURL);



		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		
		$buf2 = curl_exec ($ch);
		
		curl_close ($ch);
		
		#echo "<PRE>".htmlentities($buf2);
		
		
		$json_buf2 = json_decode($buf2);
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r buf2: ".print_r($buf2, true);
		$logMessage .= "\n\r json_buf2: ".print_r($json_buf2, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_exponent( )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_truckmate&task=test_exponent


		$controllerName = "Nsd_truckmateController";
		$functionName = "test_exponent";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$result = pow(3, 3);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r result: ".print_r($result, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public function test_curl( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_truckmate&task=test_curl



		$controllerName = "Nsd_truckmateController";
		$functionName = "test_curl";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	


       $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->hd_tmw4web_user				= $params->get('hd_truckmate_user');
		$paramsComponent->hd_tmw4web_password			= $params->get('hd_truckmate_password');
		$paramsComponent->hd_tmw4web_domain				= $params->get('hd_truckmate_domain');
		$paramsComponent->hd_flag_test_mode				= $params->get('hd_flag_test_mode');
		$paramsComponent->hd_tmw4web_user_test			= $params->get('hd_tmw4web_user_test');
		$paramsComponent->hd_tmw4web_password_test		= $params->get('hd_tmw4web_password_test');
		$paramsComponent->hd_tmw4web_domain_test		= $params->get('hd_tmw4web_domain_test');
		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');


		$connection_protocol = $paramsComponent->connection_protocol;
		
		if ( $paramsComponent->hd_flag_test_mode == "0"  )
		{
			#production NSD-WEB02 to NSD-WEB01
			
			$tm4web_usr =  $paramsComponent->hd_tmw4web_user;
			$tm4web_pwd =  $paramsComponent->hd_tmw4web_password;
			$domain = $paramsComponent->hd_tmw4web_domain;   

			$loginUrl 	= $connection_protocol.'://'.$domain.'/import/login/';
			$createUrl 	= $connection_protocol.'://'.$domain.'/login/do_login.msw';

			#configure to use this domain: NSD-WEB01.nonstopdelivery.local:83
			#$loginUrl 	= 'http://'.$domain.'/import/login/';
			#$createUrl 	= 'http://'.$domain.'/import/orders/create';
			
			
			
		}
		else
		{

			#development from metalake server to NSD-WEB01
			
			$tm4web_usr = $paramsComponent->hd_tmw4web_user_test;
			$tm4web_pwd = $paramsComponent->hd_tmw4web_password_test;
			$domain = $paramsComponent->hd_tmw4web_domain_test;   		
			
			#configure to use this domain: nonstoptest.tmwcloud.com
			
			$loginUrl 	= $connection_protocol.'://'.$domain.'/import/login';
			$createUrl 	= $connection_protocol.'://'.$domain.'/import/orders/create';
			
			
			
		}

$testArray = array();



/*
	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TM_API";
	$objCurl->tm4web_pwd = "a94c2709caf58b8e727970d06fd1dd70";
	$objCurl->domain = "nonstoptest.tmwcloud.com";
	$objCurl->port = "83";
	$objCurl->connection_protocol = "http";
	$testArray[] = $objCurl;
*/


/*
	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TM_API";
	$objCurl->tm4web_pwd = "a94c2709caf58b8e727970d06fd1dd70";
	$objCurl->domain = "nonstoptest.tmwcloud.com";
	$objCurl->port = "";
	$objCurl->connection_protocol = "https";
	$testArray[] = $objCurl;
*/
/*

	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TM_API";
	$objCurl->tm4web_pwd = "a94c2709caf58b8e727970d06fd1dd70";
	$objCurl->domain = "NSD-WEB01.nonstopdelivery.local";
	$objCurl->port = "83";
	$objCurl->connection_protocol = "http";
	$testArray[] = $objCurl;


	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TM_API";
	$objCurl->tm4web_pwd = "28c834f2078b535eb7fe903ed7570875";
	$objCurl->domain = "nsd-tagt01.nonstopdelivery.local";
	$objCurl->port = "";
	$objCurl->connection_protocol = "http";
	$testArray[] = $objCurl;
*/



/*
	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TM_API";
	$objCurl->tm4web_pwd = "a94c2709caf58b8e727970d06fd1dd70";
	$objCurl->domain = "NSD-WEB01.nonstopdelivery.local";
	$objCurl->port = "83";
	$objCurl->connection_protocol = "http";
	$testArray[] = $objCurl;
	
	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TM_API";
	$objCurl->tm4web_pwd = "a94c2709caf58b8e727970d06fd1dd70";
	$objCurl->domain = "nonstoptest.tmwcloud.com";
	$objCurl->port = "83";
	$objCurl->connection_protocol = "http";
	$testArray[] = $objCurl;
*/



	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TRACKING";
	$objCurl->tm4web_pwd = "rVELXtjAjh";
	$objCurl->domain = "nsd-tagt01.nonstopdelivery.local";
	$objCurl->port = "";
	$objCurl->connection_protocol = "http";
	$objCurl->loginUrl = $objCurl->connection_protocol."://".$objCookie->domain."/login/do_login.msw";
	$objCurl->trace_number = "10017154";	
	$testArray[] = $objCurl;



	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TM_API";
	$objCurl->tm4web_pwd = "a94c2709caf58b8e727970d06fd1dd70";
	$objCurl->domain = "nsd-tagt01.nonstopdelivery.local";
	$objCurl->port = "";
	$objCurl->connection_protocol = "http";
	$objCurl->loginUrl = $objCurl->connection_protocol."://".$objCookie->domain."/login/do_login.msw";
	$objCurl->trace_number = "10017154";
	$testArray[] = $objCurl;



	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TRACKING";
	$objCurl->tm4web_pwd = "rVELXtjAjh";
	$objCurl->domain = "nsd-tmweb01.nonstopdelivery.local";
	$objCurl->port = "";
	$objCurl->connection_protocol = "http";
	$objCurl->loginUrl = $objCurl->connection_protocol."://".$objCurl->domain."/login.msw";
	$objCurl->trace_number = "31649438";
	$testArray[] = $objCurl;



	$objCurl = new stdClass();
	$objCurl->tm4web_usr = "TM_API";
	$objCurl->tm4web_pwd = "a94c2709caf58b8e727970d06fd1dd70";
	$objCurl->domain = "nsd-tmweb01.nonstopdelivery.local";
	$objCurl->port = "";
	$objCurl->connection_protocol = "http";
	$objCurl->loginUrl = $objCurl->connection_protocol."://".$objCurl->domain."/login.msw";
	$objCurl->trace_number = "31649438";
	$testArray[] = $objCurl;



foreach( $testArray as $conTest )
{


			$tm4web_usr = $conTest->tm4web_usr;
			$tm4web_pwd = $conTest->tm4web_pwd;
			$domain = $conTest->domain; 
			$port = ($conTest->port != "") ? ":".$conTest->port : ""; 
			#$port = $conTest->port;
			$connection_protocol = $conTest->connection_protocol;   		
			
			#configure to use this domain: nonstoptest.tmwcloud.com
			
			$loginUrl 	= $objCurl->loginUrl;
			
			$trace_number 	= $objCurl->trace_number;

			


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		#$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		$logMessage .= "\n\r loginUrl: ".print_r($loginUrl, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		echo "<br><br><pre>".$logMessage."</pre>";


		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		$tmp_fname = JPATH_SITE."/tmp/COOKIE";
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
        curl_setopt ($ch, CURLOPT_PORT, $port);
        curl_setopt ($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt ($ch, CURLOPT_VERBOSE, true);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login to TMW - Start ";
		$logMessage .= "\n\r endpoint: ".print_r($loginUrl, true);		
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$curlInformation = curl_getinfo($ch);
		
		$json_last_error = json_last_error();

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login to TMW - End ";
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		

		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		    
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
			$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2.5 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login Error to TMW ";
			$logMessage .= "\n\r Error: ".print_r($curlError, true);
			$logMessage .= "\n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		    
   	    
		}

		
		$objReturnCredentials = json_decode($store);
		
		$sessionId = $objReturnCredentials->sessionId;
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		$logMessage .= "\n\r objReturnCredentials: ".print_r($objReturnCredentials, true);
		$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
        $logMessage .= "\n\r curlInformation: ".print_r($curlInformation, true);		
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		echo "<br><br><pre>".$logMessage."</pre>";





		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 3 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Content from TMW - Start ";
		$logMessage .= "\n\r endpoint: ".print_r($loginUrl, true);		
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#bill number
		$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$trace_number;
		
		curl_setopt ($ch, CURLOPT_HTTPGET, 1);
		curl_setopt ($ch, CURLOPT_URL, $queryURL);
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
        curl_setopt ($ch, CURLOPT_PORT, $port);
        curl_setopt ($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt ($ch, CURLOPT_VERBOSE, true);
		
		//execute the request
		$contentQuery = curl_exec($ch);
		$objDataOrder = json_decode($contentQuery);


		$json_last_error = json_last_error();

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 4 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Content from TMW - End ";
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		

		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		    
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
			$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 4.5 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Content Error to TMW ";
			$logMessage .= "\n\r Error: ".print_r($curlError, true);
			$logMessage .= "\n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		    
   	    
		}



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Bill Number ";
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
		$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
        $logMessage .= "\n\r curlInformation: ".print_r($curlInformation, true);		
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);	
		$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
		$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 5 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r content from TMW - End ";
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		


}
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_rounding( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=test_function_template
		# http://nsddev.metalake.net/index.php?option=com_nsd_truckmate&task=test_rounding


		$controllerName = "Nsd_truckmateController";
		$functionName = "test_rounding";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$weight = "10524.424";
		
		$new_weight = substr( round($weight,2), 0, 8 );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r weight: ".print_r($weight, true);
		$logMessage .= "\n\r new_weight: ".print_r($new_weight, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


// !FUNCTION TEMPLATE

	public function test_function_template( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=test_function_template
		# http://dev6.metalake.net/index.php?option=com_nsd_truckmate&task=test_function_template


		$controllerName = "Nsd_truckmateController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



// !DEPRICATED


	public function DEPRICATED_updateTruckmateData( $objInput=null )  
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=updateTruckmateData




		#[DETAIL_LINE_ID] => 21439  how does this figure into the update?



		$controllerName = "Nsd_truckmateController";
		$functionName = "updateTruckmateData";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$subdomain 	= "nonstoptest";
		$loginUrl 	= 'https://'.$subdomain.'.tmwcloud.com/import/login/';
		$tm4web_usr = "TM_API";
		$tm4web_pwd = "981a8e9169fa53dd4fc9b7ab79c3a860";
		$tm4web_pwd = "e692993a5869a2142769a5eb1883e45f";		
		

		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$store = curl_exec($ch);


		if ( $store == "" )
		{
			
			$objReturn->error = 1;
			$objReturn->error_code = 1;
			$objReturn->error_message = "Truckmate is currently unavailable";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Truckmate is not responding.";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
			
			
		}
		else
		{

			$objReturnCredentials = json_decode($store);
			
			$sessionId = $objReturnCredentials->sessionId;
			
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r store: ".print_r($store, true);
			$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
			$logMessage .= "\n\r objReturnCredentials: ".print_r($objReturnCredentials, true);
			$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
			$arrControls = array( 
			"populateClientInfo"=>"FALSE",  		//hard coded
			"populateCommodityInfo"=>"FALSE",  	//hard coded
			"autoAccept"=>"TRUE",  				//hard coded
			"autoPost"=>"TRUE",  				//hard coded
			"autoUpdate"=>"TRUE"  				//hard coded
			);
	
			$currentDateTime = date("Y-m-d-H.i.s.00000");
	
			$arrOrders = array();
			$arrOrders[] = array( 
			"orderId"=>"137392",  	
			//"billNumber"=>"31147809",
			"customer"=>"10020", 		
			"billToCode"=>"10343",
			"origcity"=>"Wyoming",
			"deliverBy"=>$currentDateTime,
			"deliverByEnd"=>$currentDateTime,
			);
	
	
	
			$arrBody = array( 
			"sessionId"=>$sessionId,
			"controls"=>$arrControls,
			"orders"=>$arrOrders
			);
			
			$jsonBody = json_encode($arrBody);
	
	
	
			$createUrl 	= 'https://'.$subdomain.'.tmwcloud.com/import/orders/update';
			
			//Set the URL to work with
			curl_setopt($ch, CURLOPT_URL, $createUrl);
			
			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 1);
			
			//Set the post parameters
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
			
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
			
			//execute the request (the login)
			$returnData = curl_exec($ch);
	
			$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);
	
			$returnDataJsonDecode = json_decode($returnData);
			
			$json_last_error = json_last_error();
			
			$detailURL = $returnDataJsonDecode[0]->href;
			
			if(curl_errno($ch)){
			    $curlError = curl_error($ch);
			}
			
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r createUrl: ".print_r($createUrl, true);
			$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
			$logMessage .= "\n\r returnData: ".print_r($returnData, true);
			$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
			$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
			$logMessage .= "\n\r curlError: ".print_r($curlError, true);
			$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
	
			if ( $detailURL != "" )
			{
				//init curl
				#$ch = curl_init();
				
				$detailURL = str_replace("http", "https", $detailURL);
				
				
				//Set the URL to work with
				curl_setopt($ch, CURLOPT_URL, $detailURL);
				
				// ENABLE HTTP POST
				//curl_setopt($ch, CURLOPT_POST, 0);
				
				//Set the post parameters
				//curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
				
				//Handle cookies for the login
				//curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
				//curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
				//curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
				
				curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
				
				//execute the request (the login)
				$detailData = curl_exec($ch);
		
				$detailDataJsonDecode = json_decode($detailData);
				
				$orderId = $detailDataJsonDecode->orderId;
				$billNumber = $detailDataJsonDecode->billNumber;
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r ch: ".print_r($ch, true);
				$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
				$logMessage .= "\n\r detailData: ".print_r($detailData, true);
				$logMessage .= "\n\r detailDataJsonDecode: ".print_r($detailDataJsonDecode, true);
				$logMessage .= "\n\r orderId: ".print_r($orderId, true);
				$logMessage .= "\n\r billNumber: ".print_r($billNumber, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
		
		
		
				curl_close($ch);
				unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
				unset($ch);				
				
			}
			else
			{

				$objReturn->error = 1;
				$objReturn->error_code = 2;
				$objReturn->error_message = "Order not found";
	
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Truckmate is not responding.";
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

				
			}
	
			
		}




		#LOG to htc_os_deliveries






		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}



	public static function getTruckmateData_current( $objInput )  //DEPRICATED
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=getTruckmateData


		$controllerName = "Nsd_truckmateController";
		$functionName = "getTruckmateData";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('truckmate_user');
		$paramsComponent->tmw4web_password			= $params->get('truckmate_password');
		$paramsComponent->tmw4web_domain			= $params->get('truckmate_domain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_domain_test		= $params->get('tmw4web_domain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		$paramsComponent->enable_logging 			= $params->get('enable_logging');
		$paramsComponent->connection_protocol 		= $params->get('connection_protocol');


		$connection_protocol = $paramsComponent->connection_protocol;

		if ( $objInput->flag_trace_page == "1" )
		{
			$paramsComponent->tmw4web_user				= $params->get('tmw4web_user_trace');
			$paramsComponent->tmw4web_password			= $params->get('tmw4web_password_trace');
			$paramsComponent->tmw4web_subdomain			= $params->get('tmw4web_subdomain_trace');
			$paramsComponent->flag_test_mode			= $params->get('flag_test_mode_trace');
			$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test_trace');
			$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test_trace');
			$paramsComponent->tmw4web_subdomain_test	= $params->get('tmw4web_subdomain_test_trace');	

		}
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_domain;   
			
		}
		else
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_domain_test;   		
		}
		

		$trace_number = $objInput->trace_number;

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r objForm: ".print_r($objInput, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }







		switch(  $objInput->error_code_test	)
		{
			case"31":
				$trace_number = "asdasd";
				break;
	
			case"32":
				$trace_number = "";
				break;	

			case"03":
				$tm4web_usr = "ASD";  // invalid truckmate user
				break;	


			case"04":
				$tm4web_usr = "";  // truckmate user or password is missing
				break;	

			
			default:
				break;
			
		}



		$objReturn = new stdClass();
		
		$objReturn->error_code_test = $objInput->error_code_test;

		if ( $tm4web_usr == "" || $tm4web_pwd == "" )
		{

			$objReturn->error = 1;
			$objReturn->error_code = "04";
			$objReturn->error_message = "Missing API Username or Password";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}
		else
		{
	
			$validInterval = 1;
				
			if ( $objInput->flag_api == "1" )
			{

				#$query = "select * from htc_nsd_truckmate_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$query = "select trace_milliseconds from htc_nsd_tracking_activity_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$db->setQuery($query);
				$objAPILog = $db->loadObject();


				$millisecondsNow = round(microtime(true) * 1000);
				
				#10000 = 10 seconds or 360 requests an hour
				#5000 = 5 seconds or 720 requests an hour
				#2000 = 2 seconds or 1200 requests an hour
				#2000 = 2 seconds or 1800 requests an hour
				#$intervalInMilliseconds = 1;
				$intervalInMilliseconds = $paramsComponent->api_rate_limit_milliseconds;
				
				
				$diffMilliseconds = $millisecondsNow - $objAPILog->trace_milliseconds;
	
				$validInterval = ( $diffMilliseconds > $intervalInMilliseconds ) ? 1 : 0 ;

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | validInterval";
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objAPILog: ".print_r($objAPILog, true);
					$logMessage .= "\n\r millisecondsNow: ".print_r($millisecondsNow, true);
					$logMessage .= "\n\r diffMilliseconds: ".print_r($diffMilliseconds, true);
					$logMessage .= "\n\r validInterval: ".print_r($validInterval, true);
					$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}	
			
			if( $validInterval == 1 )
			{

				if ( $trace_number != "" )
				{



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$objCookie = new stdClass();
					$objCookie->tm4web_usr = $tm4web_usr;
					$objCookie->tm4web_pwd = $tm4web_pwd;
					$objCookie->domain = $domain;					
					$objCookie->cookieName = $objCookie->domain.".".$objCookie->tm4web_usr;
					$objCookie->cookieFilePath = JPATH_SITE."/tmp/".$objCookie->cookieName;
			
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." |";
					$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
					
			
					if ( !JFile::exists( $objCookie->cookieFilePath ) )
					{
						#call func createCurlCookie
						$objCookie = Nsd_trackingController::createCurlCookie( $objCookie );
						
						if ( $objCookie->result == "1" )
						{
							$msg = "cURL cookie";
			
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$msg;
							$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }					
							
						}
						else
						{
							#return error
							
							$objReturn->error = 1;
							$objReturn->error_code = 99;
							$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password 1";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
							
						}
						
					}
					else
					{

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


						#see if cookie is stale (5 minutes)

						$fat = filemtime($objCookie->cookieFilePath);
						$sfat = strtotime( $fat );
						$dsfat = date('Y-m-d H:i:s',$fat);

						$date_a = time();
						$date_b = $fat;

						$interval_seconds = $date_a - $date_b;
						
						$interval_minutes = $interval_seconds/60;







						$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Curl Session Cookie Data";
						$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
						$logMessage .= "\n\r fat: ".print_r($fat, true);
						$logMessage .= "\n\r sfat: ".print_r($sfat, true);
						$logMessage .= "\n\r dsfat: ".print_r($dsfat, true);
						$logMessage .= "\n\r date_a: ".print_r($date_a, true);
						$logMessage .= "\n\r date_b: ".print_r($date_b, true);
						$logMessage .= "\n\r interval_seconds: ".print_r($interval_seconds, true);
						$logMessage .= "\n\r interval_minutes: ".print_r($interval_minutes, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
						

						if ( $interval_minutes > 5 )
						{

							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Curl Session Delete Cookie Begin";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



							JFile::delete( $objCookie->cookieFilePath );


							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Curl Session Delete Cookie End";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

							
							#call func createCurlCookie
							$objCookie = Nsd_trackingController::createCurlCookie( $objCookie );

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 3 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
							if ( $objCookie->result == "1" )
							{
								$msg = "cURL cookie";
				
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$msg;
								$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }					
								
							}
							else
							{
								#return error
								
								$objReturn->error = 1;
								$objReturn->error_code = 99;
								$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
					
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password 1";
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
								
							}

							
						}

						
					}

			
/*
					$tmp_fname = tempnam("/tmp", "COOKIE");
			
			
					#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD
		
					
					$loginUrl = "https://".$domain."/login/do_login.msw";
					
					//init curl
					$ch = curl_init();
					
					//Set the URL to work with
					curl_setopt($ch, CURLOPT_URL, $loginUrl);
					
					// ENABLE HTTP POST
					curl_setopt($ch, CURLOPT_POST, 1);
					
					//Set the post parameters
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
					
					//Handle cookies for the login
					curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
					curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
					curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					
					//execute the request (the login)
					$store = curl_exec($ch);
					
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
					//$logMessage .= "\n\r store: ".print_r($store, true);
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
*/
					
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 4 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
					if ( JFile::exists( $objCookie->cookieFilePath ) )
					{

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 5 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
						$ch = curl_init();
					
						if ( $objInput->flag_api == "1" )
						{
							#get api display level and set it.
		
							$query = "select * from htc_nsd_tracking_api_clients where name = '".$objInput->tmw4web_user."' and state = '1' ";
							$db->setQuery($query);
							$objApiAgent = $db->loadObject();
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | htc_nsd_truckmate_api_clients ";
							$logMessage .= "\n\r query: ".print_r($query, true);
							$logMessage .= "\n\r objApiAgent: ".print_r($objApiAgent, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
							if ( is_object($objApiAgent) )
							{
								$objReturn->id_level = $objApiAgent->id_level;						
							}
							else
							{
								
								## NEVER HAPPEN BECAUSE THIS IS NOT AN API CALL WITH A SPECIFIC USER.  ALL CALLS ARE SYSTEM CALLS.
								
								$objReturn->error = 1;
								$objReturn->error_code = "00";
								$objReturn->error_message = "The API account is not active";					
								return $objReturn;	
							}
		
						}
						
						
						
						
						#bill number
						$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						
						//execute the request
						$contentQuery = curl_exec($ch);
						$objDataOrder = json_decode($contentQuery);
		
						
						#echo "contentQuery: <PRE>".htmlentities($contentQuery)."</PRE>";
				
/*
						if(curl_errno($ch)){
						    echo 'Curl error: ' . curl_error($ch);
						}
*/
		
				
						#print_r(curl_getinfo($ch));

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 6 | seconds: ".number_format($stamp_total_micro,2);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Bill Number ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
						if ( $objDataOrder->totalCount == "0" )
						{
		
							#BOL
							$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=BPTRACE&search_style=exact&trace_number=".$trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);	
							$objDataOrder = json_decode($contentQuery);				
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | BOL ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
							
						}
		
		
						if ( $objDataOrder->totalCount == "0" )
						{
		
							#po #
							$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=PPTRACE&search_style=exact&trace_number=".$trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);	
							$objDataOrder = json_decode($contentQuery);				
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | PO ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
		
							
						}
		
		
						if ( $objDataOrder->totalCount == "0" )
						{
		
							#reference1
							$queryURL = $connection_protocol."://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=QPTRACE&search_style=exact&trace_number=".$trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);
							$objDataOrder = json_decode($contentQuery);					
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | reference1 ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
		
							
						}
		
		
		
				
				
						$objDataOrder = json_decode($contentQuery);
				
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | jsonDecode ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r objDataOrder->dataResults[0]->BILL_NUMBER: ".print_r($objDataOrder->dataResults[0]->BILL_NUMBER, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
				
				
						#if total counts > 1, remove array with obj data where current status is cancelled or billed
				
				
						if ( $objDataOrder->totalCount > 1 )
						{
							
							$arrKey = 0;
	
							foreach( $objDataOrder->dataResults as $objData )
							{
								
									if ( $objData->CURRENT_STATUS == "CANCELLED" || $objData->CURRENT_STATUS == "BILLED" )
									{
										
										unset($objDataOrder->dataResults[$arrKey]);
		
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | testing ";
										$logMessage .= "\n\r data: ".print_r($data, true);
										$logMessage .= "\n\r objData->CURRENT_STATUS: ".print_r($objData->CURRENT_STATUS, true);
										$logMessage .= "\n\r arrKey: ".print_r($arrKey, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										
									}
	
								$arrKey += 1;
								
							}
							
							$objDataOrder->dataResults = array_values($objDataOrder->dataResults);
							
							$objDataOrder->totalCount = count($objDataOrder->dataResults);
							
						}
				
				
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | jsonDecode 2";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r objDataOrder->dataResults[0]->BILL_NUMBER: ".print_r($objDataOrder->dataResults[0]->BILL_NUMBER, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				
				
				
						if ($objInput->error_code_test == "01") { $objDataOrder = ""; }
				
				
						if ( trim($objDataOrder->dataResults[0]->BILL_NUMBER) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_B) == trim($trace_number)  || trim($objDataOrder->dataResults[0]->TRACE_TYPE_P) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_Q) == trim($trace_number)  )
						{
			
							$detailLineID = $objDataOrder->dataResults[0]->DETAIL_LINE_ID;
					
				
							$objReturn->order = $objDataOrder->dataResults[0];	
					
							$objReturn->order->agent_code = $objDataOrder->dataResults[0]->CARE_OF;
					
			
					
								$detailURL = $connection_protocol."://".$domain."/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;
								
						
								curl_setopt ($ch, CURLOPT_HEADER, 0);
								curl_setopt ($ch, CURLOPT_HTTPGET, 1);
								curl_setopt ($ch, CURLOPT_URL, $detailURL);
								curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
								curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
								curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
								curl_setopt ($ch, CURLOPT_ENCODING, "");
								curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
								curl_setopt ($ch, CURLOPT_VERBOSE, 0);
								
								//execute the request
								$detailQuery = curl_exec($ch);
						
								#$headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT ); // request headers
								#echo "headerSent: <PRE>".htmlentities($headerSent)."</PRE>";
								#echo "detailQuery: <PRE>".htmlentities($detailQuery)."</PRE>";
								#print_r(curl_getinfo($ch));
						
						
								$curl_error = "";
								if (curl_error($ch)) {
								    $curl_error = curl_error($ch);
								}
						
								$objDataOrderDetail = json_decode($detailQuery);
								
								$arrOrderSteps = $objDataOrderDetail->ODRSTAT->data;
								
								#$arrTraceTypes = $objDataOrderDetail->TRACE;
						
						
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
								$logMessage .= "\n\r ch: ".print_r($ch, true);
								$logMessage .= "\n\r curl_error: ".print_r(json_decode($curl_error), true);
								$logMessage .= "\n\r detailLineID: ".print_r($detailLineID, true);
								$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
								$logMessage .= "\n\r detailQuery: ".print_r($detailQuery, true);
								$logMessage .= "\n\r objDataOrderDetail: ".print_r($objDataOrderDetail, true);
								$logMessage .= "\n\r arrOrderSteps: ".print_r($arrOrderSteps, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						
						
								curl_close($ch);
								#unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
								unset($ch);
								
								$objReturn->order_history = $objDataOrderDetail->ODRSTAT->data;
								
								$objReturn->trace_types = $objDataOrderDetail->TRACE;
								
								$objReturn->order_details = property_exists($objDataOrderDetail, "TLDTL" ) ? $objDataOrderDetail->TLDTL->data : "";
							
								if ( count( $objReturn->order_details ) > 0 )
								{
									foreach( $objReturn->order_details as $oDetails )
									{
																				
										$oDetails->WEIGHT = ( $oDetails->WEIGHT != "" ) ?  number_format((float) $oDetails->WEIGHT, 2, '.', '') : "";

									}
									
								}
								
								
								/*

								
								$objLastDocked = Nsd_truckmateController::getOrderHistoryData( $objOrderHistory=$objReturn->order_history, $order_status="DOCKED" );
								
		
		
								# get the later of today, last docked date from order history and (if available) ETA of Docked date.						
								$arrDates = array();
								$arrDates[] = date("Y-m-d");
								$arrDates[] = date("Y-m-d", strtotime(trim($objLastDocked->OS_INS_DATE)));
								#$arrDates[] = "2018-10-09";  // if available, need to put ETA date here

								
								
								$max = max(array_map('strtotime', $arrDates));
								$objReturn->order->docked_date = date('Y-m-d', $max); 
	
	
*/
								
								
/*
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
								$logMessage .= "\n\r arrDates: ".print_r($arrDates, true);
								$logMessage .= "\n\r max: ".print_r($max, true);
								$logMessage .= "\n\r objReturn->order->docked_date: ".print_r($objReturn->order->docked_date, true);	
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
*/						
								
								
								
								
								$objReturn->error = 0;
								$objReturn->error_code = "";
								$objReturn->error_message = "";
								
	
						
						}
						elseif( $objDataOrder == "" )
						{ 
							
							$objReturn->error = 1;
							$objReturn->error_code = "01";
							$objReturn->error_message = "Truckmate is currently unavailable";
							
						}
						else
						{
			
							$objReturn->error = 1;
							$objReturn->error_code = "31";
							$objReturn->error_message = "The trace number you entered cannot be found";
				
				
							#Try Rockhopper, if Rockhopper exists, transfer them to Rockhopper tracking page. If not, keep error.
							
							#Nsd_trackingController::existRockhopper( $objInput );
				
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number in system";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						}
					
				
					}
				
				}
				else
				{

		
					$objReturn->error = 1;
					$objReturn->error_code = "32";
					$objReturn->error_message = "No trace number was entered";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
				}
				
			}
			else
			{

					#NEVER HAPPEN BECAUSE INTERVAL IS ALWAYS VALID

					$objReturn->error = 1;
					$objReturn->error_code = "00";
					$objReturn->error_message = "Account exceeded the API rate limit";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Rate Limit Exceeded";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}


		}



		$objDataInsert = new stdClass();
		$objDataInsert->trace_number = $trace_number;
		$objDataInsert->datetime = date("Y-m-d H:i:s");
		$objDataInsert->datetime_gmt = gmdate("Y-m-d H:i:s");
		$objDataInsert->milliseconds = round(microtime(true) * 1000);
		$objDataInsert->error = $objReturn->error;
		$objDataInsert->error_code = $objReturn->error_code;
		$objDataInsert->error_message = $objReturn->error_message;
		$objDataInsert->objReturn = json_encode($objReturn);


		if ( $paramsComponent->enable_logging == "1")
		{

			$result = JFactory::getDbo()->insertObject('htc_nsd_truckmate_log', $objDataInsert);
			$logID = $db->insertid();

		}


		if ( $objDataInsert->error == "1" &&  $objDataInsert->error_code != "32")  // don't send 'No trace number was entered' alerts
		{

			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not retrieve from truckmate:<br>";
			$body .= "<br>objInput<br>";
			$body .= "<pre>".print_r($objInput, true)."<pre>";			
			$body .= "<br><br>";
			$body .= "<br>objReturn<br>";
			$body .= "<pre>".print_r($objReturn, true)."<pre>";
			$body .= "<br><br>";


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = "lsawyer@metalake.com";
			$objSendEmailMessage->email_from = "nsdalert@metalake.com";
			$objSendEmailMessage->email_subject = "ALERT | Nsd_dispatchtrackController | get_dispatchtrack_order";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );


/*
			#send email with error details
			$objAlert = new stdClass();
			#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			#$objAlert->recipients = array( '7039305383@txt.att.net', 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Nsd_truckmateController | getTruckmateData";
			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not retrieve from truckmate:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$objDataInsert->objReturn."<pre>";
			$body .= "<br><br>";
											
			$objAlert->body = $body;
			#$resultAlert = Nsd_truckmateController::sendAlert( $objAlert );
*/
		}



		$logMessage = $functionName." | trace: ".$trace_number." | error: ".$objReturn->error." | error code: ".$objReturn->error_code;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}


	public static function getTruckmateData_ORIG( $objInput ) //DEPRICATED
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=getTruckmateData


		$controllerName = "Nsd_truckmateController";
		$functionName = "getTruckmateData";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('truckmate_user');
		$paramsComponent->tmw4web_password			= $params->get('truckmate_password');
		$paramsComponent->tmw4web_domain			= $params->get('truckmate_domain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_domain_test		= $params->get('tmw4web_domain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		$paramsComponent->enable_logging 			= $params->get('enable_logging');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_domain;   
			
		}
		else
		{
			$tm4web_usr = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objInput->flag_api == "1" ) ? $objInput->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_domain_test;   		
		}
		

		$trace_number = $objInput->trace_number;

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r objForm: ".print_r($objInput, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }







		switch(  $objInput->error_code_test	)
		{
			case"31":
				$trace_number = "asdasd";
				break;
	
			case"32":
				$trace_number = "";
				break;	

			case"03":
				$tm4web_usr = "ASD";  // invalid truckmate user
				break;	


			case"04":
				$tm4web_usr = "";  // truckmate user or password is missing
				break;	

			
			default:
				break;
			
		}



		$objReturn = new stdClass();
		
		$objReturn->error_code_test = $objInput->error_code_test;

		if ( $tm4web_usr == "" || $tm4web_pwd == "" )
		{

			$objReturn->error = 1;
			$objReturn->error_code = "04";
			$objReturn->error_message = "Missing API Username or Password";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}
		else
		{
	
			$validInterval = 1;
				
			if ( $objInput->flag_api == "1" )
			{

				$query = "select * from htc_nsd_truckmate_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$db->setQuery($query);
				$objAPILog = $db->loadObject();


				$millisecondsNow = round(microtime(true) * 1000);
				
				#10000 = 10 seconds or 360 requests an hour
				#5000 = 5 seconds or 720 requests an hour
				#2000 = 2 seconds or 1200 requests an hour
				#2000 = 2 seconds or 1800 requests an hour
				#$intervalInMilliseconds = 1;
				$intervalInMilliseconds = $paramsComponent->api_rate_limit_milliseconds;
				
				
				$diffMilliseconds = $millisecondsNow - $objAPILog->trace_milliseconds;
	
				$validInterval = ( $diffMilliseconds > $intervalInMilliseconds ) ? 1 : 0 ;

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | validInterval";
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objAPILog: ".print_r($objAPILog, true);
					$logMessage .= "\n\r millisecondsNow: ".print_r($millisecondsNow, true);
					$logMessage .= "\n\r diffMilliseconds: ".print_r($diffMilliseconds, true);
					$logMessage .= "\n\r validInterval: ".print_r($validInterval, true);
					$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}	
			
			if( $validInterval == 1 )
			{

				if ( $trace_number != "" )
				{
			
					$tmp_fname = tempnam("/tmp", "COOKIE");
			
			
					#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD
		
					
					$loginUrl = "https://".$domain."/login/do_login.msw";
					
					//init curl
					$ch = curl_init();
					
					//Set the URL to work with
					curl_setopt($ch, CURLOPT_URL, $loginUrl);
					
					// ENABLE HTTP POST
					curl_setopt($ch, CURLOPT_POST, 1);
					
					//Set the post parameters
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
					
					//Handle cookies for the login
					curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
					curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
					curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					
					//execute the request (the login)
					$store = curl_exec($ch);
					
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
					//$logMessage .= "\n\r store: ".print_r($store, true);
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					
					if ( $store != "" && $objInput->flag_api == "1" )
					{
						#get api display level and set it.
	
						$query = "select * from htc_nsd_truckmate_api_clients where name = '".$objInput->tmw4web_user."' and state = '1' ";
						$db->setQuery($query);
						$objApiAgent = $db->loadObject();
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | htc_nsd_truckmate_api_clients ";
						$logMessage .= "\n\r query: ".print_r($query, true);
						$logMessage .= "\n\r objApiAgent: ".print_r($objApiAgent, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
						if ( is_object($objApiAgent) )
						{
							$objReturn->id_level = $objApiAgent->id_level;						
						}
						else
						{
							
							## NEVER HAPPEN BECAUSE THIS IS NOT AN API CALL WITH A SPECIFIC USER.  ALL CALLS ARE SYSTEM CALLS.
							
							$objReturn->error = 1;
							$objReturn->error_code = "00";
							$objReturn->error_message = "The API account is not active";					
							return $objReturn;	
						}
	
					}
					
					
					
					
					#bill number
					$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$trace_number;
					
					curl_setopt ($ch, CURLOPT_HTTPGET, 1);
					curl_setopt ($ch, CURLOPT_URL, $queryURL);
					curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
					curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt ($ch, CURLOPT_HEADER, 0);
					
					//execute the request
					$contentQuery = curl_exec($ch);
					$objDataOrder = json_decode($contentQuery);
	
					
					#echo "contentQuery: <PRE>".htmlentities($contentQuery)."</PRE>";
			
	/*
					if(curl_errno($ch)){
					    echo 'Curl error: ' . curl_error($ch);
					}
	*/
			
					#print_r(curl_getinfo($ch));
	
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Bill Number ";
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
					$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
					$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
	
					if ( $objDataOrder->totalCount == "0" )
					{
	
						#BOL
						$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=BPTRACE&search_style=exact&trace_number=".$trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						
						//execute the request
						$contentQuery = curl_exec($ch);	
						$objDataOrder = json_decode($contentQuery);				
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | BOL ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
						
					}
	
	
					if ( $objDataOrder->totalCount == "0" )
					{
	
						#po #
						$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=PPTRACE&search_style=exact&trace_number=".$trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						
						//execute the request
						$contentQuery = curl_exec($ch);	
						$objDataOrder = json_decode($contentQuery);				
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | PO ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
	
						
					}
	
	
					if ( $objDataOrder->totalCount == "0" )
					{
	
						#reference1
						$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=QPTRACE&search_style=exact&trace_number=".$trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						
						//execute the request
						$contentQuery = curl_exec($ch);
						$objDataOrder = json_decode($contentQuery);					
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | reference1 ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
	
						
					}
	
	
	
			
			
					$objDataOrder = json_decode($contentQuery);
			
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | jsonDecode ";
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
					$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
					$logMessage .= "\n\r objDataOrder->dataResults[0]->BILL_NUMBER: ".print_r($objDataOrder->dataResults[0]->BILL_NUMBER, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
			
			
					#if total counts > 1, remove array with obj data where current status is cancelled or billed
			
			
					if ( $objDataOrder->totalCount > 1 )
					{
						
						$arrKey = 0;

						foreach( $objDataOrder->dataResults as $objData )
						{
							
								if ( $objData->CURRENT_STATUS == "CANCELLED" || $objData->CURRENT_STATUS == "BILLED" )
								{
									
									unset($objDataOrder->dataResults[$arrKey]);
	
									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | testing ";
									$logMessage .= "\n\r data: ".print_r($data, true);
									$logMessage .= "\n\r objData->CURRENT_STATUS: ".print_r($objData->CURRENT_STATUS, true);
									$logMessage .= "\n\r arrKey: ".print_r($arrKey, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
								}

							$arrKey += 1;
							
						}
						
						$objDataOrder->dataResults = array_values($objDataOrder->dataResults);
						
						$objDataOrder->totalCount = count($objDataOrder->dataResults);
						
					}
			
			
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | jsonDecode 2";
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
					$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
					$logMessage .= "\n\r objDataOrder->dataResults[0]->BILL_NUMBER: ".print_r($objDataOrder->dataResults[0]->BILL_NUMBER, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
			
			
			
					if ($objInput->error_code_test == "01") { $objDataOrder = ""; }
			
			
					if ( $store == "" )
					{
						
						$objReturn->error = 1;
						$objReturn->error_code = "03";
						$objReturn->error_message = "Invalid Truckmate Username or Password";
			
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						
					}
					elseif ( trim($objDataOrder->dataResults[0]->BILL_NUMBER) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_B) == trim($trace_number)  || trim($objDataOrder->dataResults[0]->TRACE_TYPE_P) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_Q) == trim($trace_number)  )
					{
		
						$detailLineID = $objDataOrder->dataResults[0]->DETAIL_LINE_ID;
				
			
						$objReturn->order = $objDataOrder->dataResults[0];	
				
						$objReturn->order->agent_code = $objDataOrder->dataResults[0]->CARE_OF;
				
		
				
							$detailURL = "https://".$domain."/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;
							
					
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $detailURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
							curl_setopt ($ch, CURLOPT_ENCODING, "");
							curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt ($ch, CURLOPT_VERBOSE, 0);
							
							//execute the request
							$detailQuery = curl_exec($ch);
					
							#$headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT ); // request headers
							#echo "headerSent: <PRE>".htmlentities($headerSent)."</PRE>";
							#echo "detailQuery: <PRE>".htmlentities($detailQuery)."</PRE>";
							#print_r(curl_getinfo($ch));
					
					
							$curl_error = "";
							if (curl_error($ch)) {
							    $curl_error = curl_error($ch);
							}
					
							$objDataOrderDetail = json_decode($detailQuery);
							
							$arrOrderSteps = $objDataOrderDetail->ODRSTAT->data;
					
					
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r curl_error: ".print_r(json_decode($curl_error), true);
							$logMessage .= "\n\r detailLineID: ".print_r($detailLineID, true);
							$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
							$logMessage .= "\n\r detailQuery: ".print_r($detailQuery, true);
							$logMessage .= "\n\r objDataOrderDetail: ".print_r($objDataOrderDetail, true);
							$logMessage .= "\n\r arrOrderSteps: ".print_r($arrOrderSteps, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					
					
							curl_close($ch);
							unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
							unset($ch);
							
							$objReturn->order_history =$objDataOrderDetail->ODRSTAT->data;
							
							
							
							$objLastDocked = Nsd_truckmateController::getOrderHistoryData( $objOrderHistory=$objReturn->order_history, $order_status="DOCKED" );
							
	
	
							# get the later of today, last docked date from order history and (if available) ETA of Docked date.						
							$arrDates = array();
							$arrDates[] = date("Y-m-d");
							$arrDates[] = date("Y-m-d", strtotime(trim($objLastDocked->OS_INS_DATE)));
							#$arrDates[] = "2018-10-09";  // if available, need to put ETA date here
							
							
							$max = max(array_map('strtotime', $arrDates));
							$objReturn->order->docked_date = date('Y-m-d', $max); 
							
							
							
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
							$logMessage .= "\n\r arrDates: ".print_r($arrDates, true);
							$logMessage .= "\n\r max: ".print_r($max, true);
							$logMessage .= "\n\r objReturn->order->docked_date: ".print_r($objReturn->order->docked_date, true);	
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }						
							
							
							
							
							
							
							
							
							$objReturn->error = 0;
							$objReturn->error_code = "";
							$objReturn->error_message = "";
							

					
					}
					elseif( $objDataOrder == "" )
					{ 
						
						$objReturn->error = 1;
						$objReturn->error_code = "01";
						$objReturn->error_message = "Truckmate is currently unavailable";
						
					}
					else
					{
		
						$objReturn->error = 1;
						$objReturn->error_code = "31";
						$objReturn->error_message = "The trace number you entered cannot be found";
			
			
						#Try Rockhopper, if Rockhopper exists, transfer them to Rockhopper tracking page. If not, keep error.
						
						#Nsd_trackingController::existRockhopper( $objInput );
			
			
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number in system";
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					}
					
				
				}
				else
				{
		
					$objReturn->error = 1;
					$objReturn->error_code = "32";
					$objReturn->error_message = "No trace number was entered";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
				}
				
			}
			else
			{

					#NEVER HAPPEN BECAUSE INTERVAL IS ALWAYS VALID

					$objReturn->error = 1;
					$objReturn->error_code = "00";
					$objReturn->error_message = "Account exceeded the API rate limit";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Rate Limit Exceeded";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}


		}



		$objDataInsert = new stdClass();
		$objDataInsert->trace_number = $trace_number;
		$objDataInsert->datetime = date("Y-m-d H:i:s");
		$objDataInsert->datetime_gmt = gmdate("Y-m-d H:i:s");
		$objDataInsert->milliseconds = round(microtime(true) * 1000);
		$objDataInsert->error = $objReturn->error;
		$objDataInsert->error_code = $objReturn->error_code;
		$objDataInsert->error_message = $objReturn->error_message;
		$objDataInsert->objReturn = json_encode($objReturn);


		if ( $paramsComponent->enable_logging == "1")
		{

			$result = JFactory::getDbo()->insertObject('htc_nsd_truckmate_log', $objDataInsert);
			$logID = $db->insertid();

		}


		if ( $objDataInsert->error == "1" &&  $objDataInsert->error_code != "32")  // don't send 'No trace number was entered' alerts
		{

			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not retrieve from truckmate:<br>";
			$body .= "<br>objInput<br>";
			$body .= "<pre>".print_r($objInput, true)."<pre>";			
			$body .= "<br><br>";
			$body .= "<br>objReturn<br>";
			$body .= "<pre>".print_r($objReturn, true)."<pre>";
			$body .= "<br><br>";


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = "lsawyer@metalake.com";
			$objSendEmailMessage->email_from = "nsdalert@metalake.com";
			$objSendEmailMessage->email_subject = "ALERT | Nsd_dispatchtrackController | get_dispatchtrack_order";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );


/*
			#send email with error details
			$objAlert = new stdClass();
			#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			#$objAlert->recipients = array( '7039305383@txt.att.net', 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Nsd_truckmateController | getTruckmateData";
			$body = "";
			$body .= "<br>Error Message: ".$objDataInsert->error_message."<br>";
			$body .= "<br><br>The following order did not retrieve from truckmate:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$objDataInsert->objReturn."<pre>";
			$body .= "<br><br>";
											
			$objAlert->body = $body;
			#$resultAlert = Nsd_truckmateController::sendAlert( $objAlert );
*/
		}



		$logMessage = $functionName." | trace: ".$trace_number." | error: ".$objReturn->error." | error code: ".$objReturn->error_code;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}



	public static function updateTruckmateData_OLD( $objUpdate )  //depricated
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=updateTruckmateData



		$controllerName = "Nsd_truckmateController";
		$functionName = "updateTruckmateData";
		
		$db = JFactory::getDBO();


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();


		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');
		$paramsComponent->truckmate_version 			= $params->get('truckmate_version');
		$paramsComponent->flag_active_server			= $params->get('flag_active_server');
		$paramsComponent->api_rate_limit_milliseconds 	= $params->get('api_rate_limit_milliseconds');
		$paramsComponent->enable_logging 				= $params->get('enable_logging');

        
		$paramsComponent->tmw4web_user_server_1			= $params->get('truckmate_user_server_1');
		$paramsComponent->tmw4web_password_server_1		= $params->get('truckmate_password_server_1');
		$paramsComponent->tmw4web_domain_server_1		= $params->get('truckmate_domain_server_1');
		
		$paramsComponent->tmw4web_user_server_2			= $params->get('truckmate_user_server_2');
		$paramsComponent->tmw4web_password_server_2		= $params->get('truckmate_password_server_2');
		$paramsComponent->tmw4web_domain_server_2		= $params->get('truckmate_domain_server_2');	
		
		$paramsComponent->tmw4web_user_server_3			= $params->get('truckmate_user_server_3');
		$paramsComponent->tmw4web_password_server_3		= $params->get('truckmate_password_server_3');
		$paramsComponent->tmw4web_domain_server_3		= $params->get('truckmate_domain_server_3');

		$connection_protocol = $paramsComponent->connection_protocol;

		

		switch( $paramsComponent->flag_active_server )
		{

			case "3":
				$tm4web_usr = $paramsComponent->tmw4web_user_server_3;
				$tm4web_pwd = $paramsComponent->tmw4web_password_server_3;
				$domain 	= $paramsComponent->tmw4web_domain_server_3;   
				break;

			case "2":
				$tm4web_usr = $paramsComponent->tmw4web_user_server_2;
				$tm4web_pwd = $paramsComponent->tmw4web_password_server_2;
				$domain 	= $paramsComponent->tmw4web_domain_server_2;   
				break;
			
			case "1":	
			default:
				$tm4web_usr = $paramsComponent->tmw4web_user_server_1;
				$tm4web_pwd = $paramsComponent->tmw4web_password_server_1;
				$domain 	= $paramsComponent->tmw4web_domain_server_1;   
				break;
							
		}

/*
        
		$paramsComponent->tmw4web_user				= $params->get('truckmate_user');
		$paramsComponent->tmw4web_password			= $params->get('truckmate_password');
		$paramsComponent->tmw4web_domain			= $params->get('truckmate_domain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_domain_test		= $params->get('tmw4web_domain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		$paramsComponent->enable_logging 			= $params->get('enable_logging');
		$paramsComponent->connection_protocol 		= $params->get('connection_protocol');


		$connection_protocol = $paramsComponent->connection_protocol;



		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = $paramsComponent->tmw4web_user	;
			$tm4web_pwd = $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_domain;   
			
		}
		else
		{
			$tm4web_usr = $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_domain_test;   		
		}
		
*/


		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
	
	
	
			$arrBody = array( 
			"detail_line_id"=>$objUpdate->detail_line_id,
			"deliver_by"=>$objUpdate->requested_time_start,
			"deliver_by_end"=>$objUpdate->requested_time_end
			);
			
			$jsonBody = json_encode($arrBody);
	
			#$createUrl 	= 'https://nonstoptest.tmwcloud.com/api/freightbills/update.php?auth=dk0v930alv$ad';

			$updateUrl 	= $connection_protocol.'://'.$domain.'/api/freightbills/update.php?auth=dk0v930alv$ad';
			


			$ch = curl_init();
			
			//Set the URL to work with
			curl_setopt($ch, CURLOPT_URL, $updateUrl);
			
			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 1);
			
			//Set the post parameters
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
			
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
			
			//execute the request (the login)
			$returnData = curl_exec($ch);
	
			$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);
	
			$returnDataJsonDecode = json_decode($returnData);
			
			$json_last_error = json_last_error();
			
						
			if(curl_errno($ch)){
			    $curlError = curl_error($ch);
			}
	
	
			switch( $returnDataJsonDecode->Result  )
			{

				case "Freight bill was updated":
					$objUpdate->error = "0";
					$objUpdate->error_code = "";
					$objUpdate->error_message = "";			
					$objUpdate->system_error_message = "";	
					break;

				
				case "":
					$objUpdate->error = "1";
					$objUpdate->error_code = "91";
					$objUpdate->error_message = "Update API is not responding";		
					$objUpdate->system_error_message = "Update API is not responding";		
					break;
				
				
				default:
					$objUpdate->error = "1";
					$objUpdate->error_code = "92";
					$objUpdate->error_message = "Update Failed";
					$objUpdate->system_error_message = "Update Failed";	
					break;
				
				
			}
			
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r updateUrl: ".print_r($updateUrl, true);
			$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
			$logMessage .= "\n\r returnData: ".print_r($returnData, true);
			$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
			$logMessage .= "\n\r curlError: ".print_r($curlError, true);
			$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
			$logMessage .= "\n\r objUpdate: ".print_r($objUpdate, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
		$logMessage = $functionName." | order_id: ".$objUpdate->trace_number." | dlid: ".$objUpdate->detail_line_id." | error: ".$objUpdate->error." | error code: ".$objUpdate->error_code;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
	
	
		if ( $objUpdate->error == "1" )
		{

			$body = "";
			$body .= "<br>Error Message: ".$objUpdate->error_message."<br>";
			$body .= "<br><br>The following order did not update from truckmate:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$jsonBody."<pre>";
			$body .= "<br><br>";


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = "lsawyer@metalake.com";
			$objSendEmailMessage->email_from = "nsdalert@metalake.com";
			$objSendEmailMessage->email_subject = "ALERT | Nsd_truckmateController | updateTruckmateData";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );




/*
			#send email with error details
			$objAlert = new stdClass();
			#$objAlert->recipients = array( 'support@metalake.com', 'dhatch@nonstopdelivery.com', 'shendrickson@nonstopdelivery.com' );
			$objAlert->recipients = array( 'lsawyer@metalake.com' );
			#$objAlert->recipients = array( '7039305383@txt.att.net', 'lsawyer@metalake.com' );
			$objAlert->subject = "ALERT | Nsd_truckmateController | updateTruckmateData";
			$body = "";
			$body .= "<br>Error Message: ".$objUpdate->error_message."<br>";
			$body .= "<br><br>The following order did not update from truckmate:<br>";
			$body .= "<br><br>";
			$body .= "<br><br>";
			$body .= "<pre>".$jsonBody."<pre>";
			$body .= "<br><br>";
											
			$objAlert->body = $body;
			#$resultAlert = Nsd_truckmateController::sendAlert( $objAlert );
*/
		}
	
	
	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objUpdate;


		
	}

	public static function insertHDReturnIntoTMW_old( $objInput )  //depricated
	{
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=importTMW

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_returns&task=importTMW

		$controllerName = "Nsd_truckmateController";
		$functionName = "insertHDReturnIntoTMW";


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->hd_tmw4web_user				= $params->get('hd_truckmate_user');
		$paramsComponent->hd_tmw4web_password			= $params->get('hd_truckmate_password');
		$paramsComponent->hd_tmw4web_domain				= $params->get('hd_truckmate_domain');
		$paramsComponent->hd_flag_test_mode				= $params->get('hd_flag_test_mode');
		$paramsComponent->hd_tmw4web_user_test			= $params->get('hd_tmw4web_user_test');
		$paramsComponent->hd_tmw4web_password_test		= $params->get('hd_tmw4web_password_test');
		$paramsComponent->hd_tmw4web_domain_test		= $params->get('hd_tmw4web_domain_test');
		
		if ( $paramsComponent->hd_flag_test_mode == "0"  )
		{
			#production web02
			
			$tm4web_usr =  $paramsComponent->hd_tmw4web_user;
			$tm4web_pwd =  $paramsComponent->hd_tmw4web_password;
			$domain = $paramsComponent->hd_tmw4web_domain;   
			
			$loginUrl 	= 'http://'.$domain.'/import/login/';
			$createUrl 	= 'http://'.$domain.'/import/orders/create';
			
			
			
		}
		else
		{

			#development
			
			$tm4web_usr = $paramsComponent->hd_tmw4web_user_test;
			$tm4web_pwd = $paramsComponent->hd_tmw4web_password_test;
			$domain = $paramsComponent->hd_tmw4web_domain_test;   		
			
			$loginUrl 	= 'https://'.$domain.'/import/login/';
			$createUrl 	= 'https://'.$domain.'/import/orders/create';
			
			
			
		}


		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#$loginUrl 	= 'https://'.$domain.'/import/login/';
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		$logMessage .= "\n\r loginUrl: ".print_r($loginUrl, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$store = curl_exec($ch);

		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		}

		
		$objReturnCredentials = json_decode($store);
		
		$sessionId = $objReturnCredentials->sessionId;
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		$logMessage .= "\n\r objReturnCredentials: ".print_r($objReturnCredentials, true);
		$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$query = "SELECT * from htc_nsd_returns_data WHERE id = '".$objInput->dataID."'";
		$db->setQuery($query);
		$objOrderData = $db->loadObject() ;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objOrderData: ".print_r($objOrderData, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

		$query = "SELECT * from htc_nsd_returns_data_items WHERE data_id = '".$objInput->dataID."'";
		$db->setQuery($query);
		$objOrderDataItems = $db->loadObjectList() ;


		$arrControls = array( 
		"populateClientInfo"=>"TRUE",  		//hard coded
		"populateCommodityInfo"=>"TRUE",  	//hard coded
		"autoAccept"=>"TRUE",  				//hard coded
		"autoPost"=>"TRUE",  				//hard coded
		"autoUpdate"=>"TRUE"  				//hard coded
		);
		
		
		$arrDetails = array(); 
		foreach( $objOrderDataItems as $item )
		{
			$arrDetails[] = array(
				"description"		=>substr($item->goods_description, 0, 50),
				"pieces"			=>substr($item->goods_pieces, 0, 4),
				"piecesUnits"		=>substr($item->goods_code_clean, 0, 3),
				"weight"			=>substr($item->goods_weight, 0, 8 ),
				"weightUnits"		=>"LB",
				"commodity"			=>substr($item->goods_commodity, 0, 10 )				
			);
		}


		

		$arrTraces = array(); 
		$arrTraces[] = array( 
		"traceType"=>"P",							//hard coded
		"traceNumber"=>$objOrderData->reference1,
		"refQualifier"=>"PO"							//empty
		);

		$arrTraces[] = array( 
		"traceType"=>"Q",							//hard coded
		"traceNumber"=>$objOrderData->reference2,
		"refQualifier"=>"OMS"							//empty
		);


		if ( $objOrderData->reference3 != "" )
		{
			$arrTraces[] = array( 
			"traceType"=>"R",							
			"traceNumber"=>$objOrderData->reference3,
			"refQualifier"=>""							
			);
		}


		if ( $objOrderData->return_auth != "" )
		{
			$arrTraces[] = array( 
			"traceType"=>"W",							
			"traceNumber"=>$objOrderData->return_auth,
			"refQualifier"=>""							
			);
		}

		
		
		switch( $objOrderData->destination_code )
		{
			case "11106":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8615",
				"refQualifier"=>"SN"
				);
				break;

			case "11107":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8616",
				"refQualifier"=>"SN"
				);
				break;

			case "11108":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8617",
				"refQualifier"=>"SN"
				);
				break;
			
		}
		


		if ( strtoupper($objOrderData->disposal) == "TRUE" || strtoupper($objOrderData->disposal) == "YES" || $objOrderData->disposal == "1" )
		{

			$arrTraces[] = array( 
			"traceType"=>"3",
			"traceNumber"=>"LM",
			"refQualifier"=>"OPS"
			);

			$arrTraces[] = array( 
			"traceType"=>"S",						
			"traceNumber"=>"SHIPINSTRUCT12", 		
			"refQualifier"=>"SHP" 					
			);		
			
		}
		else
		{
			$arrTraces[] = array( 
			"traceType"=>"3",
			"traceNumber"=>"DD",
			"refQualifier"=>"OPS"
			);			
		}




		$arrNotes = array(); 

		if ( $objOrderData->origin_additionalInfo1 != "" )
		{
			$arrNotes[] = array( 
			"noteType"=>"3",
			"theNote"=>$objOrderData->origin_additionalInfo1
			);			
		}


		
		

		$currentDateTime = date("Y-m-d-H.i.s.00000");



		$arrOrders = array();
		$arrOrders[] = array( 
		"billToCode"	=>"10343",  						//hard coded
		"serviceLevel"	=>$objOrderData->service_level_clean,
		"customer"		=>"10343",  							//hard coded
		"destination"	=>$objOrderData->destination_code,
		"destname"		=>strtoupper( $objOrderData->destination_name_clean ),

		"pickUpBy"		=>$currentDateTime,
		"pickUpByEnd"	=>$currentDateTime,
		
		"deliverBy"		=>$currentDateTime,
		"deliverByEnd"	=>$currentDateTime,

		"destaddr1"		=>strtoupper( $objOrderData->destination_addressLine1." ".$objOrderData->destination_addressLine2 ),
		"destcity"		=>strtoupper( $objOrderData->destination_city ),
		"destprov"		=>strtoupper( $objOrderData->destination_state ),
		"destpc"		=>strtoupper( substr($objOrderData->destination_zipCode, 0, 5) ),
		"destphone"		=>"",
		"destemail"		=>"",
		"origin"		=>"",
		"origname"		=>strtoupper( substr($objOrderData->origin_name, 0, 40) ),
		"origaddr1"		=>strtoupper( substr( ($objOrderData->origin_addressLine1." ".$objOrderData->origin_addressLine2), 0, 40) ),
		"origcity"		=>strtoupper( substr($objOrderData->origin_city, 0, 30) ),
		"origprov"		=>strtoupper( substr($objOrderData->origin_state, 0, 4) ),
		"origpc"		=>strtoupper( substr($objOrderData->origin_zipCode, 0, 10) ),
		"origphone"		=>strtoupper( substr($objOrderData->origin_phone, 0, 20) ),
		"origemail"		=>strtoupper( substr($objOrderData->origin_email, 0, 40) ),
		"siteId"		=>"SITE5",   							//hard coded
		"startZone"		=>strtoupper( substr($objOrderData->origin_zipCode, 0, 5) ),
		"billTo"		=>"C",  								//hard coded
		"endZone"		=>substr( $objOrderData->destination_zipCode, 0, 5 ),
		"details"		=>$arrDetails,
		"traces"		=>$arrTraces,
		"notes"			=>$arrNotes, 
		);



		$arrBody = array( 
		"sessionId"=>$sessionId,
		"controls"=>$arrControls,
		"orders"=>$arrOrders
		);
		
		$jsonBody = json_encode($arrBody);




		#$createUrl 	= 'https://'.$domain.'/import/orders/create';
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $createUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
		//execute the request
		$returnData = curl_exec($ch);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);

		$returnDataJsonDecode = json_decode($returnData);
		
		$json_last_error = json_last_error();
		
		$detailURL = $returnDataJsonDecode[0]->href;
		
		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r createUrl: ".print_r($createUrl, true);
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		//init curl
		#$ch = curl_init();
		
		$detailURL = ( $paramsComponent->hd_flag_test_mode == "0"  ) ?  $detailURL : str_replace("http", "https", $detailURL);
		
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $detailURL);
		curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 3 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		//execute the request
		$detailData = curl_exec($ch);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 4 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$detailDataJsonDecode = json_decode($detailData);
		
		$orderId = $detailDataJsonDecode->orderId;
		$billNumber = $detailDataJsonDecode->billNumber;
		
		
		$objImportReturn = new stdClass();
		$objImportReturn->orderId = $orderId;
		$objImportReturn->billNumber = $billNumber;	
		$objImportReturn->message = isset($returnDataJsonDecode[0]->Message) ? $returnDataJsonDecode[0]->Message : "";

		
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r detailData: ".print_r($detailData, true);
		$logMessage .= "\n\r detailDataJsonDecode: ".print_r($detailDataJsonDecode, true);
		$logMessage .= "\n\r orderId: ".print_r($orderId, true);
		$logMessage .= "\n\r billNumber: ".print_r($billNumber, true);
		$logMessage .= "\n\r objImportReturn: ".print_r($objImportReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }





		curl_close($ch);
		unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
		unset($ch);



		


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objImportReturn;
	}

	public static function insertOrderIntoTMW_NOTUSED( $objInput )  //depricated
	{
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=importTMW

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_returns&task=importTMW

		$controllerName = "Nsd_truckmateController";
		$functionName = "insertOrderIntoTMW";


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->hd_tmw4web_user				= $params->get('hd_truckmate_user');
		$paramsComponent->hd_tmw4web_password			= $params->get('hd_truckmate_password');
		$paramsComponent->hd_tmw4web_domain				= $params->get('hd_truckmate_domain');
		$paramsComponent->hd_flag_test_mode				= $params->get('hd_flag_test_mode');
		$paramsComponent->hd_tmw4web_user_test			= $params->get('hd_tmw4web_user_test');
		$paramsComponent->hd_tmw4web_password_test		= $params->get('hd_tmw4web_password_test');
		$paramsComponent->hd_tmw4web_domain_test		= $params->get('hd_tmw4web_domain_test');
		$paramsComponent->connection_protocol 			= $params->get('connection_protocol');


		$connection_protocol = $paramsComponent->connection_protocol;
		
		if ( $paramsComponent->hd_flag_test_mode == "0"  )
		{
			#production NSD-WEB02 to NSD-WEB01
			
			$tm4web_usr =  $paramsComponent->hd_tmw4web_user;
			$tm4web_pwd =  $paramsComponent->hd_tmw4web_password;
			$domain = $paramsComponent->hd_tmw4web_domain;   

			$loginUrl 	= $connection_protocol.'://'.$domain.'/import/login/';
			$createUrl 	= $connection_protocol.'://'.$domain.'/import/orders/create';

			#configure to use this domain: NSD-WEB01.nonstopdelivery.local:83
			#$loginUrl 	= 'http://'.$domain.'/import/login/';
			#$createUrl 	= 'http://'.$domain.'/import/orders/create';
			
			
			
		}
		else
		{

			#development from metalake server to NSD-WEB01
			
			$tm4web_usr = $paramsComponent->hd_tmw4web_user_test;
			$tm4web_pwd = $paramsComponent->hd_tmw4web_password_test;
			$domain = $paramsComponent->hd_tmw4web_domain_test;   		
			
			#configure to use this domain: nonstoptest.tmwcloud.com
			
			$loginUrl 	= $connection_protocol.'://'.$domain.'/import/login/';
			$createUrl 	= $connection_protocol.'://'.$domain.'/import/orders/create';
			
			
			
		}


		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "truckmate_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "truckmate_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#$loginUrl 	= 'https://'.$domain.'/import/login/';
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r domain: ".print_r($domain, true);
		$logMessage .= "\n\r loginUrl: ".print_r($loginUrl, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login to TMW - Start ";
		$logMessage .= "\n\r endpoint: ".print_r($loginUrl, true);		
		$logMessage .= "\n\r";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$json_last_error = json_last_error();

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login to TMW - End ";
		$logMessage .= "\n\r";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		

		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		    
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
			$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2.5 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Login Error to TMW ";
			$logMessage .= "\n\r Error: ".print_r($curlError, true);
			$logMessage .= "\n\r";
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		    
   	    
		}

		
		$objCredentials = json_decode($store);
		
		$sessionId = $objCredentials->sessionId;
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		$logMessage .= "\n\r objCredentials: ".print_r($objCredentials, true);
		$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$query = "SELECT * from htc_nsd_apiorders_data WHERE id = '".$objInput->dataID."'";
		$db->setQuery($query);
		$objOrderData = $db->loadObject() ;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objOrderData: ".print_r($objOrderData, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

		$query = "SELECT * from htc_nsd_apiorders_data_items WHERE data_id = '".$objInput->dataID."'";
		$db->setQuery($query);
		$objOrderDataItems = $db->loadObjectList() ;


		$arrControls = array( 
		"populateClientInfo"=>"TRUE",  		//hard coded
		"populateCommodityInfo"=>"TRUE",  	//hard coded
		"autoAccept"=>"TRUE",  				//hard coded
		"autoPost"=>"TRUE",  				//hard coded
		"autoUpdate"=>"TRUE"  				//hard coded
		);
		
		
		$arrDetails = array(); 
		foreach( $objOrderDataItems as $item )
		{
			$arrDetails[] = array(
				"description"		=>substr($item->goods_description, 0, 50),
				"pieces"			=>substr($item->goods_pieces, 0, 4),
				"piecesUnits"		=>substr($item->goods_code_clean, 0, 3),
				"weight"			=>substr($item->goods_weight, 0, 8 ),
				"weightUnits"		=>"LB",
				"commodity"			=>substr($item->goods_commodity, 0, 10 )				
			);
		}


		

		$arrTraces = array(); 
		$arrTraces[] = array( 
		"traceType"=>"P",							//hard coded
		"traceNumber"=>$objOrderData->reference1,
		"refQualifier"=>"PO"							//empty
		);

		$arrTraces[] = array( 
		"traceType"=>"Q",							//hard coded
		"traceNumber"=>$objOrderData->reference2,
		"refQualifier"=>"OMS"							//empty
		);


		if ( $objOrderData->reference3 != "" )
		{
			$arrTraces[] = array( 
			"traceType"=>"R",							
			"traceNumber"=>$objOrderData->reference3,
			"refQualifier"=>""							
			);
		}


		if ( $objOrderData->return_auth != "" )
		{
			$arrTraces[] = array( 
			"traceType"=>"W",							
			"traceNumber"=>$objOrderData->return_auth,
			"refQualifier"=>""							
			);
		}

		
/*
		
		switch( $objOrderData->destination_code )
		{
			case "11106":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8615",
				"refQualifier"=>"SN"
				);
				break;

			case "11107":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8616",
				"refQualifier"=>"SN"
				);
				break;

			case "11108":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8617",
				"refQualifier"=>"SN"
				);
				break;
			
		}
*/
		


		if ( strtoupper($objOrderData->disposal) == "TRUE" || strtoupper($objOrderData->disposal) == "YES" || $objOrderData->disposal == "1" )
		{

			$arrTraces[] = array( 
			"traceType"=>"3",
			"traceNumber"=>"LM",
			"refQualifier"=>"OPS"
			);

			$arrTraces[] = array( 
			"traceType"=>"S",						
			"traceNumber"=>"SHIPINSTRUCT12", 		
			"refQualifier"=>"SHP" 					
			);		
			
		}
		else
		{
			$arrTraces[] = array( 
			"traceType"=>"3",
			"traceNumber"=>"DD",
			"refQualifier"=>"OPS"
			);			
		}




		$arrNotes = array(); 

		if ( $objOrderData->origin_additionalInfo1 != "" )
		{
			$arrNotes[] = array( 
			"noteType"=>"3",
			"theNote"=>$objOrderData->origin_additionalInfo1
			);			
		}


		
		

		$currentDateTime = date("Y-m-d-H.i.s.00000");



		$arrOrders = array();
		$arrOrders[] = array( 
		"billToCode"	=>$objOrderData->customer_code,  						
		"serviceLevel"	=>$objOrderData->service_level_clean,
		"customer"		=>$objOrderData->customer_code,  						
		"destination"	=>$objOrderData->destination_code,
		"destname"		=>strtoupper( $objOrderData->destination_name_clean ),

		"pickUpBy"		=>$currentDateTime,
		"pickUpByEnd"	=>$currentDateTime,
		
		"deliverBy"		=>$currentDateTime,
		"deliverByEnd"	=>$currentDateTime,

		"destaddr1"		=>strtoupper( $objOrderData->destination_addressLine1." ".$objOrderData->destination_addressLine2 ),
		"destcity"		=>strtoupper( $objOrderData->destination_city ),
		"destprov"		=>strtoupper( $objOrderData->destination_state ),
		"destpc"		=>strtoupper( substr($objOrderData->destination_zipCode, 0, 5) ),
		"destphone"		=>"",
		"destemail"		=>"",
		"origin"		=>"",
		"origname"		=>strtoupper( substr($objOrderData->origin_name, 0, 40) ),
		"origaddr1"		=>strtoupper( substr( ($objOrderData->origin_addressLine1." ".$objOrderData->origin_addressLine2), 0, 40) ),
		"origcity"		=>strtoupper( substr($objOrderData->origin_city, 0, 30) ),
		"origprov"		=>strtoupper( substr($objOrderData->origin_state, 0, 4) ),
		"origpc"		=>strtoupper( substr($objOrderData->origin_zipCode, 0, 10) ),
		"origphone"		=>strtoupper( substr($objOrderData->origin_phone, 0, 20) ),
		"origemail"		=>strtoupper( substr($objOrderData->origin_email, 0, 40) ),
		"siteId"		=>strtoupper( $objOrderData->siteId ),   							
		"startZone"		=>strtoupper( substr($objOrderData->origin_zipCode, 0, 5) ),
		"billTo"		=>"",  								//hard coded
		"endZone"		=>substr( $objOrderData->destination_zipCode, 0, 5 ),
		"details"		=>$arrDetails,
		"traces"		=>$arrTraces,
		"notes"			=>$arrNotes, 
		);



		$arrBody = array( 
		"sessionId"=>$sessionId,
		"controls"=>$arrControls,
		"orders"=>$arrOrders
		);
		
		$jsonBody = json_encode($arrBody);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		#$createUrl 	= 'https://'.$domain.'/import/orders/create';
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $createUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 3 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_start_micro_post = microtime(true);	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 4 | seconds: ".number_format($stamp_total_micro,2);				
		$logMessage .= "\n\r Post to TMW - Start ";
		$logMessage .= "\n\r endpoint: ".print_r($createUrl, true);		
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		
		//execute the request
		$returnData = curl_exec($ch);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		
		$stamp_total_micro_post = ($stamp_end_micro - $stamp_start_micro_post);
		
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 5 | post seconds: ".number_format($stamp_total_micro_post,2) ." |   total seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Post to TMW - End ";
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);

		$returnDataJsonDecode = json_decode($returnData);
		
		$json_last_error = json_last_error();
		
		$detailURL = $returnDataJsonDecode[0]->href;
		
		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
			    
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
			$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 5.5 | seconds: ".number_format($stamp_total_micro,2);				$logMessage .= "\n\r Post Error to TMW ";
			$logMessage .= "\n\r Error: ".print_r($curlError, true);
			$logMessage .= "\n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		    
		    
		    
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r createUrl: ".print_r($createUrl, true);
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		//init curl
		#$ch = curl_init();
		
		$detailURL = ( $paramsComponent->hd_flag_test_mode == "0"  ) ?  $detailURL : str_replace("http", $connection_protocol, $detailURL);
		
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $detailURL);
		curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 6 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		//execute the request
		$detailData = curl_exec($ch);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 7 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$detailDataJsonDecode = json_decode($detailData);
		
		$orderId = $detailDataJsonDecode->orderId;
		$billNumber = $detailDataJsonDecode->billNumber;
		
		
		$objImportReturn = new stdClass();
		$objImportReturn->orderId = $orderId;
		$objImportReturn->billNumber = $billNumber;	
		$objImportReturn->message = isset($returnDataJsonDecode[0]->Message) ? $returnDataJsonDecode[0]->Message : "";

		
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r detailData: ".print_r($detailData, true);
		$logMessage .= "\n\r detailDataJsonDecode: ".print_r($detailDataJsonDecode, true);
		$logMessage .= "\n\r orderId: ".print_r($orderId, true);
		$logMessage .= "\n\r billNumber: ".print_r($billNumber, true);
		$logMessage .= "\n\r objImportReturn: ".print_r($objImportReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }





		curl_close($ch);
		unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
		unset($ch);



		


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objImportReturn;
	}

}
