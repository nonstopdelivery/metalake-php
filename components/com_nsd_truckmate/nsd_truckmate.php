<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_truckmate
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Nsd_truckmate', JPATH_COMPONENT);
JLoader::register('Nsd_truckmateController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Nsd_truckmate');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
