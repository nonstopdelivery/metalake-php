<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Shipnsd_api
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');

require_once JPATH_SITE . '/components/com_nsd_scheduling/controller.php';


/**
 * Class Shipnsd_apiController
 *
 * @since  1.6
 */
class Shipnsd_apiController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'logs');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}

// !FUNCTIONS

	public static function validateJoomlaUser( $objInput )
	{	
	

		$controllerName = "Shipnsd_apiController";
		$functionName = "validateJoomlaUser";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}
		
		
		$stamp_start_micro = microtime(true);	

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		#validate user username/pwd

		$options = array();
		$options['remember'] = false;
		$options['return'] = "";
		
		$credentials = array();
		$credentials['username'] = $objInput->username;
		$credentials['password'] = $objInput->password;;
		
		jimport( 'joomla.user.authentication');
		$authenticate = JAuthentication::getInstance();
		$response	  = $authenticate->authenticate($credentials, $options);		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 | ";	
		$logMessage .= "\n\r objInput: ".print_r( $objInput, true );
		$logMessage .= "\n\r response: ".print_r( $response, true );
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		
		

		
		if ( $response->status == 1 )
		{

			$objInputVU = new stdClass();
			$objInputVU->username = $objInput->username;

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 | ";	
			$logMessage .= "\n\r objInputVU: ".print_r( $objInputVU, true );
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	


			$objUser = Nsd_returnsController::getObjUser( $objInputVU );

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3 | ";	
			$logMessage .= "\n\r objUser: ".print_r( $objUser, true );
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				

			

			
			#if ( $objUser->block == 1 || $validGroup == 1 )
			
			if ( $objUser->block == 1  )
			{
	
				$objReturn = new stdClass();
				$objReturn->validUser = 0;
				$objReturn->validGroup = 0;
				$objReturn->validInterval = 0;
				
			}
			else
			{

				$validGroup = ( in_array("10", $objUser->groups ) ) ? 1 : 0 ;

				#get time of last api call
				$query = "SELECT * from htc_nsd_returns_log WHERE user = '".$objInputVU->username."' and description='Initial Call' ORDER BY id DESC limit 1 ";
				$db->setQuery($query);
		        $objAPILog = $db->loadObject();
	
				$millisecondsNow = round(microtime(true) * 1000);
				
				$diffMilliseconds = $millisecondsNow - $objAPILog->milliseconds;
	
				$validInterval = ( $diffMilliseconds > $objUser->apiinterval ) ? 1 : 0 ;
	
	
				$objReturn = new stdClass();
				$objReturn->id = $objUser->id;
				$objReturn->username = $objUser->username;
				$objReturn->validUser = 1;
	
				$objReturn->arrGroups = $objUser->groups;
				$objReturn->validGroup = $validGroup;
	
				$objReturn->apiInterval = $objUser->apiinterval;
				$objReturn->diffMilliseconds = $diffMilliseconds;
				$objReturn->validInterval = $validInterval;
				
				
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 4 | ";	
				$logMessage .= "\n\r objAPILog: ".print_r( $objAPILog, true );
				$logMessage .= "\n\r millisecondsNow: ".print_r( $millisecondsNow, true );
				$logMessage .= "\n\r objAPILog->milliseconds: ".print_r( $objAPILog->milliseconds, true );
				$logMessage .= "\n\r diffMilliseconds: ".print_r( $diffMilliseconds, true );
				$logMessage .= "\n\r objUser->api-interval: ".print_r( $objUser->apiinterval, true );
				$logMessage .= "\n\r validInterval: ".print_r( $validInterval, true );			
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
				
			}
			
	
			
		}
		else
		{

			$objReturn = new stdClass();
			$objReturn->validUser = 0;
			$objReturn->validGroup = 0;
			$objReturn->validInterval = 0;			
			
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 5 | ";	
		$logMessage .= "\n\r objReturn: ".print_r( $objReturn, true );
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		$logMessage .= "\n\r\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		
		return $objReturn;
	
	}



	public static function getObjUser( $objInput )
	{	
	
		$controllerName = "Shipnsd_apiController";
		$functionName = "getObjUser";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}
		
		
		$stamp_start_micro = microtime(true);	

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	


		$objUser = JFactory::getUser( $objInput->username );

		$groups = $objUser->get('groups');

		$objUser->groups = $groups;



		$query = "SELECT f.name, v.value from htc_fields f, htc_fields_values v WHERE f.id = v.field_id AND v.item_id = '".$objUser->id."' and f.state='1' ";
		$db->setQuery($query);
        $objList = $db->loadObjectList();

		foreach( $objList as $obj )
		{
			$name = (string)$obj->name;
			
			$objUser->$name = $obj->value;
			
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ";	
		$logMessage .= "\n\r objInput: ".print_r( $objInput, true );
		$logMessage .= "\n\r query: ".print_r( $query, true );
		$logMessage .= "\n\r objUser: ".print_r( $objUser, true );
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		$logMessage .= "\n\r\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
		
		return $objUser;
		
	
	
	}




	public static function determineCanSchedule( $objInput )
	{
		# http://nsddev.metalake.net/index.php?option=com_shipnsd_api&task=determineCanSchedule


		$controllerName = "Shipnsd_apiController";
		$functionName = "determineCanSchedule";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}	
		
		
        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_truckmate');
        
        $paramsComponent = new stdClass();

		$paramsComponent->connection_protocol_joomla 	= $params->get('connection_protocol');
		$paramsComponent->flag_joomla_active_server		= $params->get('flag_active_server');
		$paramsComponent->joomla_domain_server_1		= $params->get('truckmate_domain_server_1');
		$paramsComponent->joomla_domain_server_2		= $params->get('truckmate_domain_server_2');
		$paramsComponent->joomla_domain_server_3		= $params->get('truckmate_domain_server_3');

		$connection_protocol = $paramsComponent->connection_protocol_joomla;

		switch( $paramsComponent->flag_joomla_active_server )
		{

			case "3":
				$domain 	= $paramsComponent->joomla_domain_server_3; 
				break;

			case "2":
				$domain 	= $paramsComponent->joomla_domain_server_2;  
				break;

			case "1":	
			default:
				$domain 	= $paramsComponent->joomla_domain_server_1;   
				break;
							
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$hello_world = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r aaa: ".print_r($hello_world, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		#call scheduling api and see if order can be scheduled
		#http://nsddev.metalake.net/api2/schedule/get-windows/?billNumber=31454361&zipCode=20151


		#$urlSchedule = JURI::root()."api2/schedule/get-windows/?billNumber=".$trace_number."&zipCode=".$objTMW->order->DESTPC;
		
		#tweb02
		#$urlSchedule = "http://nsd-tweb02.nonstopdelivery.local/api2/schedule/get-windows/?billNumber=".$trace_number."&zipCode=".$objTMW->order->DESTPC;
		
		
		#production
		#$urlSchedule = "http://10.10.106.15/api2/schedule/get-windows/?billNumber=".$trace_number."&zipCode=".$objTMW->order->DESTPC;


		#$urlSchedule = "api2/schedule/get-windows/?billNumber=".$trace_number."&zipCode=".$objTMW->order->DESTPC;

		#$urlSchedule = $connection_protocol."://".$domain."/api2/schedule/get-windows/?billNumber=".$objInput->trace_number."&zipCode=".$objInput->zip_code;
		$urlSchedule = "http://10.10.106.15/api2/schedule/get-windows/?billNumber=".$objInput->trace_number."&zipCode=".$objInput->zip_code;
		
		
		
		//init curl
		$ch = curl_init();
		

		curl_setopt($ch, CURLOPT_URL, $urlSchedule);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$returnCurl = curl_exec($ch);

		$curl_error = "";
		if (curl_error($ch)) {
		    $curl_error = curl_error($ch);
		}

		$objDataOrderSchedule = json_decode($returnCurl);

		$objReturn = new stdClass();
		
		$objReturn->canSchedule = ( $objDataOrderSchedule->result == "ok" ) ? 1 : 0 ;


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r urlSchedule: ".print_r($urlSchedule, true);
		$logMessage .= "\n\r returnCurl: ".print_r($returnCurl, true);
		$logMessage .= "\n\r objDataOrderSchedule: ".print_r($objDataOrderSchedule, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;		

		
	}




// !ORDER CREATE FUNCTIONS


	public static function cleanServiceLevel( $service_level )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=cleanServiceLevel
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=cleanServiceLevel

		$controllerName = "Shipnsd_apiController";
		$functionName = "cleanServiceLevel";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $service_level != "" )
		{
			$lower_service_level = strtolower( $service_level ); 
	
	
			
			$query = "SELECT * from htc_nsd_apiorders_lu_service_levels WHERE state = '1' and LOWER(code) = '".$lower_service_level."' ";
			
			
			$db->setQuery($query);
	        $objReturn = $db->loadObject();
	
			$strReturn = ( $objReturn->service_level != "" ) ? $objReturn->service_level : "not_avail" ; 
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
		}
		else
		{
			$strReturn = "not_avail" ;

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		}




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $strReturn;
	}



	public static function cleanGoodsCode( $good_code )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=cleanGoodsCode
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=cleanGoodsCode

		$controllerName = "Shipnsd_apiController";
		$functionName = "cleanGoodsCode";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $good_code != "" )
		{
	
	
			$query = "SELECT * from htc_nsd_apiorders_lu_piece_code WHERE FIND_IN_SET('".$good_code."',description) and state=1 ";
			$db->setQuery($query);
	        $objReturn = $db->loadObject();
	
			$strReturn = ( $objReturn->name != "" ) ? $objReturn->name : "ERR" ; 
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
		}
		else
		{
			$strReturn = "ERR" ;

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			#$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
			
		}




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $strReturn;
	}



	public static function evalAdditionalInfo2( $additionalInfo2 )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_returns&task=evalAdditionalInfo2

		$controllerName = "Shipnsd_apiController";
		$functionName = "evalAdditionalInfo2";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$needle = "Sender Phone:";
			$arrString = "";
			#$return = "";
			
			
			$objReturn = new JObject;
			$objReturn->isPhoneNumber = "no";		
			$objReturn->additionalInfo2 = $additionalInfo2;
			$objReturn->phone = "";
			
			if ( stripos( strtolower( $additionalInfo2 ), $needle ) !== false )
			{
				
				$arrString = explode( ":", $additionalInfo2 );
				
				$objReturn->isPhoneNumber = "yes";
				$objReturn->additionalInfo2 = "";
				$objReturn->phone = trim($arrString[1]);
			}



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r additionalInfo2: ".print_r($additionalInfo2, true);
		$logMessage .= "\n\r arrString: ".print_r($arrString, true);
		$logMessage .= "\n\r needle: ".print_r($needle, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}	




// !TEST FUNCTIONS

	public function test_token_time( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_shipnsd_api&task=test_token_time



		$className = "Shipnsd_apiController";
		$functionName = "test_token_time";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$window_start_time = "18:00:00";
		
		$timestamp = strtotime($window_start_time);
		
		$newTime = date("H:i", $timestamp);


		$tokenNow = strtotime("+0 minutes");
		
		
		$tokenNow_view = date("Y-m-d H:i", $tokenNow);

		$tokenExpire = strtotime("+15 minutes");
		
		$tokenExpire_view = date("Y-m-d H:i", $tokenExpire);
/*
		$k = 1234567;

		$tokenExpire1 = $tokenExpire + $k;

		
		$tokenExpire_view1 = date("Y-m-d H:i", $tokenExpire1);
		
		
		$tokenExpire2 = $tokenExpire1 - $k;
		
		$tokenExpire_view2 = date("Y-m-d H:i", $tokenExpire2);
*/
		
		
		$b64eTokenExpire = base64_encode($tokenExpire);
		$b64dTokenExpire = base64_decode($b64eTokenExpire);
		$b64dtokenExpire_view = date("Y-m-d H:i", $b64dTokenExpire);
		
		
		$diff = ( $tokenExpire - $tokenNow );
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r window_start_time: ".print_r($window_start_time, true);
		$logMessage .= "\n\r timestamp: ".print_r($timestamp, true);
		$logMessage .= "\n\r newTime: ".print_r($newTime, true);
		$logMessage .= "\n\r tokenNow: ".print_r($tokenNow, true);
		$logMessage .= "\n\r tokenNow_view: ".print_r($tokenNow_view, true);
		$logMessage .= "\n\r tokenExpire: ".print_r($tokenExpire, true);
		$logMessage .= "\n\r tokenExpire_view: ".print_r($tokenExpire_view, true);
/*
		$logMessage .= "\n\r tokenExpire1: ".print_r($tokenExpire1, true);
		$logMessage .= "\n\r tokenExpire2: ".print_r($tokenExpire2, true);
		$logMessage .= "\n\r tokenExpire_view1: ".print_r($tokenExpire_view1, true);
		$logMessage .= "\n\r tokenExpire_view2: ".print_r($tokenExpire_view2, true);
*/
		

		$logMessage .= "\n\r b64dTokenExpire: ".print_r($b64dTokenExpire, true);
		$logMessage .= "\n\r b64dtokenExpire_view: ".print_r($b64dtokenExpire_view, true);
		$logMessage .= "\n\r diff: ".print_r($diff, true);
		

		

		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


// !FUNCTION TEMPLATE

	public static function test_function_template( )
	{
		# http://nsddev.metalake.net/index.php?option=com_shipnsd_api&task=test_function_template


		$controllerName = "Shipnsd_apiController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$hello_world = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r aaa: ".print_r($hello_world, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

}
