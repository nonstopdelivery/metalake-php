<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Shipnsd_api
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Shipnsd_api', JPATH_COMPONENT);
JLoader::register('Shipnsd_apiController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Shipnsd_api');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
