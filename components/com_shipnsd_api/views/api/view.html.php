<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Shipnsd_api
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Shipnsd_apiViewApi extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');
		$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_shipnsd_api');


		$controllerName = "Shipnsd_apiViewApi";
		$functionName = "view";

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}	



/*
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
*/
		

		
		$objReturnAPI = Shipnsd_apiModelApi::getApi_return();
		
		$this->assignRef('objReturnAPI', $objReturnAPI); 	








		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_shipnsd_api');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_SHIPNSD_API_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
