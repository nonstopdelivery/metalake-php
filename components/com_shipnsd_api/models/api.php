<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Shipnsd_api
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2019 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Shipnsd_api model.
 *
 * @since  1.6
 */
class Shipnsd_apiModelApi extends JModelItem
{


	public static function getApi_return( )
	{
	
		# for authentication and return headers
		# https://secure.php.net/manual/en/features.http-auth.php
	
		$controllerName = "Shipnsd_apiModelApi";
		$functionName = "getApi_return";

	
		$db = JFactory::getDBO();

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->api_token_expire_minutes		= $params->get('api_token_expire_minutes');


		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "shipnsd_api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "shipnsd_api_prod";
	
		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName."";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#variables

		$jinput = JFactory::getApplication()->input;

		$collection = trim($jinput->get('collection', '', 'STRING'));
		$command = trim($jinput->get('command', '', 'STRING'));

		$jsonData = file_get_contents("php://input");
		
		$data = json_decode($jsonData);

		

		#schedule
		$traceNumber = trim($jinput->get('billNumber', '', 'STRING'));
		$zipCode = trim($jinput->get('zipCode', '', 'STRING'));





		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		$logMessage .= "\n\r collection: ".print_r($collection, true);
		$logMessage .= "\n\r command: ".print_r($command, true);
		$logMessage .= "\n\r traceNumber: ".print_r($traceNumber, true);
		$logMessage .= "\n\r zipCode: ".print_r($zipCode, true);

		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }





		$serverValues = $_SERVER;
	
		$validUserSuccess = 1;


		$objInput = new stdClass();

		switch( $collection )
		{

			case "order":

				switch( $command )
				{
					
					case "create": 	# for home depot
					case "create-return-order":
						$validUserSuccess = 0;
						$objInput->username = $serverValues["PHP_AUTH_USER"];
						$objInput->password = $serverValues["PHP_AUTH_PW"];				
						break;
						
					case "trace":
					case "get-trace":
						$validUserSuccess = 1;
						$objInput->username = "";
						$objInput->password = "";	

						break;



					case "createdev": 	# 
						$validUserSuccess = 0;
						$objInput->username = $serverValues["PHP_AUTH_USER"];
						$objInput->password = $serverValues["PHP_AUTH_PW"];				
						break;


						
					default:
						$validUserSuccess = 0;
						break;
					
				}
				break;

/*
			case "schedule":
				#for schedule api
				
				$objInput->username = "scheduleapiuser";
				$objInput->password = "ShipNSD02152019!";					
				break;
*/

			case "":
				$validUserSuccess = 0;
				$objInput->username = $serverValues["PHP_AUTH_USER"];
				$objInput->password = $serverValues["PHP_AUTH_PW"];				
				break;
			
		}
		
		
		
		
		
		
		
		
		$objValidation = Shipnsd_apiController::validateJoomlaUser( $objInput );
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 ";
		$logMessage .= "\n\r serverValues: ".print_r($serverValues, true);
		#$logMessage .= "\n\r PHP_AUTH_USER: ".print_r($serverValues["PHP_AUTH_USER"], true);
		#$logMessage .= "\n\r PHP_AUTH_PW: ".print_r($serverValues["PHP_AUTH_PW"], true);
		$logMessage .= "\n\r REQUEST_METHOD: ".print_r($serverValues["REQUEST_METHOD"], true);
		$logMessage .= "\n\r objValidation: ".print_r($objValidation, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		


		#log
		$logTable = "htc_shipnsd_api_log";
		$logObj = new stdClass();
		$logObj->trace_number = $traceNumber;
		$logObj->zip_code = $zipCode;
		$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
		$logObj->datetime = date("Y-m-d H:i:s");
		$logObj->milliseconds = round(microtime(true) * 1000);
		$logObj->user = isset($serverValues["PHP_AUTH_USER"]) ? $serverValues["PHP_AUTH_USER"] : "";
		$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
		$logObj->request_method = $serverValues["REQUEST_METHOD"];
		$logObj->collection = $collection;
		$logObj->command = $command;
		$logObj->description = "Initial Call";
		$logObj->server_values = json_encode($serverValues);
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3 ";
		$logMessage .= "\n\r logObj: ".print_r($logObj, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		

		MetalakeHelperCore::logOBJ( $logTable, $logObj );


		require_once ('configuration.php' ); // since this file n configuration file both are at the same location

		$var_cls = new JConfig(); // object of the class

	    // variables that you want to use 
	    $offline = $var_cls->offline; 
	    $sitename = $var_cls->sitename; 


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		$logMessage .= "\n\r offline: ".print_r($offline, true);
		$logMessage .= "\n\r sitename: ".print_r($sitename, true);
		$logMessage .= "\n\r var_cls: ".print_r($var_cls, true);

		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		if ( $offline == "1" )
		{

			http_response_code(400);
		
			$apiReturn = new stdClass();
			$apiReturn->error_state = "1";
			$apiReturn->error_code = "9999";
			$apiReturn->error_message = "Temporarily Offline for Maintenance. We are performing scheduled maintenance. We should be back online shortly.";
			$return_value = json_encode($apiReturn);		


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		$logMessage .= "\n\r apiReturn: ".print_r($apiReturn, true);
		$logMessage .= "\n\r return_value: ".print_r($return_value, true);

		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			echo $return_value;
			
			exit();

		}



		if ( $objValidation->validUser == 1 || $validUserSuccess == 1 )
		{

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Valid User";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			if ( $objValidation->validGroup == 1  || $validUserSuccess == 1 )
			{
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Valid Group";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

				if ( $objValidation->validInterval == 1  || $validUserSuccess == 1 )
				{

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Valid Interval";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
					switch( $collection )
					{
						
						case "schedule":  // used for schedule component api calls

							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						
							switch( $command )
							{
								case "get-windows":

									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								
									if ( $serverValues["REQUEST_METHOD"] != "GET" )
									{
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows NOT GET";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
										http_response_code(400);
									
										$apiReturn = new stdClass();
										$apiReturn->result = "noSchedule";
										$apiReturn->error_state = "1";
										$apiReturn->error_code = "63";
										$apiReturn->error_message = "Invalid request method. Expecting GET.";
										$return_value = json_encode($apiReturn);
										
									
										$logTable = "htc_shipnsd_api_log";
										$logObj = new stdClass();
										$logObj->trace_number = $traceNumber;
										$logObj->zip_code = $zipCode;										
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
										$logObj->datetime = date("Y-m-d H:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->return_value = $return_value;
										$logObj->description = "http_response_code set to 400 | Request Method should be GET";
										$logObj->server_values = json_encode($serverValues);
										$logObj->error_state = $apiReturn->error_state;
										$logObj->error_code = $apiReturn->error_code;
										$logObj->error_message = $apiReturn->error_message;											
										MetalakeHelperCore::logOBJ( $logTable, $logObj );

										#log to htc_os_deliveries
										$objDeliveries = new stdClass();
										$objDeliveries->system = "API";
										$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
										$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
										$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
										$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
										$objDeliveries->result = $apiReturn->result;	
										$objDeliveries->record_type = "get";	
										$objDeliveries->error = $apiReturn->error_state;
										$objDeliveries->error_code = $apiReturn->error_code;
										$objDeliveries->system_error_message = $apiReturn->error_message;
										$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
										$deliveryID = $db->insertid();
			
										echo $return_value;	

									
									
									}
									else
									{
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows GET";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

										$objInput = new stdClass();
								        $objInput->flag_api = "0";
								        $objInput->trace_number = $traceNumber;
								        $objInput->zip = $zipCode;
								        $objInput->component_origin == "com_shipnsd_api";

										$objDisplay = Nsd_schedulingController::getOrderData( $objInput );


										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ";	
										$logMessage .= "\n\r objInput: ".print_r( $objInput, true );
										$logMessage .= "\n\r objDisplay: ".print_r( $objDisplay, true );
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



								
										if ( $objDisplay->error == "0" && ( $objDisplay->order->destination_zipcode != $objInput->zip ) )
										{
											
											$objDisplay->error = "1";
											$objDisplay->error_code = "57";
											$objDisplay->system_error_message = "zipCode does not match the order destination zipcode";
											
										}
								

										$objOrderAllowSchedule = new stdClass();
								            $objOrderAllowSchedule->customer_account_code = $objDisplay->order->customer_account_code;
								            $objOrderAllowSchedule->origid = $objDisplay->order->origid;
								            $objOrderAllowSchedule->service_level = $objDisplay->order->service_level;
								            $objOrderAllowSchedule->op_code = $objDisplay->order->op_code;
								

										$objReturnAllow = Nsd_schedulingController::determine_allow_schedule( $objOrderAllowSchedule );


										if ( $objReturnAllow->allowSchedule == "1" )
										{
											#if it meets criteria above, proceed, else fail.
										}										
										else
										{
											$objDisplay->error = "1";
											$objDisplay->error_code = "787";
											$objDisplay->system_error_message = "This order cannot be scheduled at this time.";
											
										}

							
								
								
										$apiReturn = new stdClass();
								
								
								
										switch( $objDisplay-> error )
										{
											
											case "0":
												http_response_code(200);
									
												$apiReturn->result = "ok";	
												
									
												#token		
												$tokenExpire = strtotime("+".$paramsComponent->api_token_expire_minutes." minutes");
												$b64eTokenExpire = base64_encode($tokenExpire);	
												$apiReturn->token = $b64eTokenExpire;
												
												#windows
												$arrWindows = array();
												foreach( $objDisplay->days as $days )
												{
													$objWindow = new stdClass();
													$objWindow->date = $days->windows[0]->APIdate;
													$objWindow->startTime = $days->windows[0]->APIstartTime;
													$objWindow->endTime = $days->windows[0]->APIendTime; 
													$objWindow->slotsAvailable = $days->windows[0]->APIslotsAvailable; 
										
													$arrWindows[] = $objWindow; 
													
												}
												$apiReturn->windows = $arrWindows;
									
									
												break;
								

											case "1":
											case "99":


												switch( $objDisplay-> error_code  )
												{

													case "01":
													case "02":
													case "03":
													case "04":
													case "05":
													case "06":
													case "07":
														# Truckmate Issue
														http_response_code(500);
														$apiReturn->result = "noSchedule";
														$apiReturn->error_state = "1";
														$apiReturn->error_code = $objDisplay-> error_code;
														$apiReturn->error_message = "There has been an error.  We have been alerted and are working toward a solution.";					
									
														$body = "";
														$body .= "<br>The com_nsd_scheduling API is throwing a Truckmate error<br>";
														$body .= "<br><br>objDisplay: <br>";
														$body .= print_r($objDisplay, true);
														$body .= "<br>";
														
											
														$objSendEmailMessage = new stdClass();
														$objSendEmailMessage->email_to = "lsawyer@metalake.com";
														$objSendEmailMessage->email_from = "nsdalert@metalake.com";
														$objSendEmailMessage->email_subject = "ALERT | com_nsd_scheduling | API ";
														$objSendEmailMessage->email_body = $body;
														$objSendEmailMessage->email_category = "scheduling support";
												
														$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
													
														break;

													case "31":
													case "32":

														# set header to 400 - Missing parameters and validation errors			
														http_response_code(400);
														$apiReturn->result = "notFound";
														$apiReturn->error_state = "1";
														$apiReturn->error_code = $objDisplay-> error_code;
														$apiReturn->error_message = $objDisplay-> system_error_message;	
													
														break;


													case "51":

														# set header to 400 - Missing parameters and validation errors			
														http_response_code(400);
														$apiReturn->result = "noReschedule";
														$apiReturn->error_state = "1";
														$apiReturn->error_code = $objDisplay-> error_code;
														$apiReturn->error_message = $objDisplay-> system_error_message;	
													
														break;



													default:
													
														# set header to 400 - Missing parameters and validation errors			
														http_response_code(400);
														$apiReturn->result = "noSchedule";
														$apiReturn->error_state = "1";
														$apiReturn->error_code = $objDisplay-> error_code;
														$apiReturn->error_message = $objDisplay-> system_error_message;											
														
														break;
													
												}


											
												break;

								
								
											default:
								
												http_response_code(500);
												
												$apiReturn->result = "noSchedule";
												$apiReturn->error_state = "1";
												$apiReturn->error_code = $objDisplay-> error_code;
												$apiReturn->error_message = "There has been an error.  We have been alerted and are working toward a solution.";	;															
								
												$body = "";
												$body .= "<br>The com_nsd_scheduling API is throwing a Truckmate error<br>";
												$body .= "<br><br>objDisplay: <br>";
												$body .= print_r($objDisplay, true);
												$body .= "<br>";
												
									
												$objSendEmailMessage = new stdClass();
												$objSendEmailMessage->email_to = "lsawyer@metalake.com";
												$objSendEmailMessage->email_from = "nsdalert@metalake.com";
												$objSendEmailMessage->email_subject = "ALERT | com_nsd_scheduling | API ";
												$objSendEmailMessage->email_body = $body;
												$objSendEmailMessage->email_category = "scheduling support";
										
												$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
								
								
								
												break;
													
													
											
										}
								
										#this is the Return to call
										$return_value = json_encode($apiReturn);
								
										#debugging
										$jsonApiReturndecode = json_decode($return_value);
								
								
										$tokenExpire_view = date("Y-m-d H:i", $tokenExpire);
								
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
										$logMessage .= "\n\r objInput: ".print_r($objInput, true);
										$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
										$logMessage .= "\n\r return_value: ".print_r($return_value, true);
										$logMessage .= "\n\r jsonApiReturndecode: ".print_r($jsonApiReturndecode, true);
										$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
										$logMessage .= "\n\r tokenExpire_view: ".print_r($tokenExpire_view, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


										$logTable = "htc_shipnsd_api_log";
										$logObj = new stdClass();
										$logObj->agent_id = $objDisplay->agent->id;
										$logObj->trace_number = $traceNumber;
										$logObj->zip_code = $zipCode;										
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
										$logObj->datetime = date("Y-m-d H:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->return_value = $return_value;
										$logObj->description = "response";
										$logObj->server_values = json_encode($serverValues);
										$logObj->error_state = $apiReturn->error_state;
										$logObj->error_code = $apiReturn->error_code;
										$logObj->error_message = $apiReturn->error_message;											
										MetalakeHelperCore::logOBJ( $logTable, $logObj );


										if ( $apiReturn->result != "ok" )
										{
											#log to htc_os_deliveries
											$objDeliveries = new stdClass();
											$objDeliveries->system = "API";
											$objDeliveries->agent_id = $objGetOrderData->agent->id;
											$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
											$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
											$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
											$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
											$objDeliveries->result = $apiReturn->result;	
											$objDeliveries->record_type = "get";	
											$objDeliveries->error = $apiReturn->error_state;
											$objDeliveries->error_code = $apiReturn->error_code;
											$objDeliveries->error_message = $apiReturn->error_message;
											$objDeliveries->system_error_message = $apiReturn->error_message;
											$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
											$deliveryID = $db->insertid();
										}

										echo $return_value;



									}
								
									break;

									
								case "set-window":
									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command set-window";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }								
								
									if ( $serverValues["REQUEST_METHOD"] != "POST" )
									{
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows NOT POST";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
										http_response_code(400);
									
										$apiReturn = new stdClass();
										$apiReturn->result = "noSchedule";
										$apiReturn->error_state = "1";
										$apiReturn->error_code = "63";
										$apiReturn->error_message = "Invalid request method. Expecting POST.";
										$return_value = json_encode($apiReturn);
										
										#$return_value = "Error [66] - Invalid request method. Expecting POST.";									
									
										$logTable = "htc_shipnsd_api_log";
										$logObj = new stdClass();
										$logObj->trace_number = $traceNumber;
										$logObj->zip_code = $zipCode;
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
										$logObj->datetime = date("Y-m-d H:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->return_value = $return_value;
										$logObj->description = "Request Method should be POST";
										$logObj->server_values = json_encode($serverValues);
										$logObj->error_state = $apiReturn->error_state;
										$logObj->error_code = $apiReturn->error_code;
										$logObj->error_message = $apiReturn->error_message;															
										
										MetalakeHelperCore::logOBJ( $logTable, $logObj );

										#log to htc_os_deliveries
										$objDeliveries = new stdClass();
										$objDeliveries->system = "API";
										$objDeliveries->agent_id = $objGetOrderData->agent->id;
										$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
										$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
										$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
										$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
										$objDeliveries->result = $apiReturn->result;	
										$objDeliveries->record_type = "post";	
										$objDeliveries->error = $apiReturn->error_state;
										$objDeliveries->error_code = $apiReturn->error_code;
										$objDeliveries->system_error_message = $apiReturn->error_message;
										$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
										$deliveryID = $db->insertid();

			
										echo $return_value;	

									
									
									}
									else
									{
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command set-windows POST";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


										$jsonData = file_get_contents("php://input");
										
										$data = json_decode($jsonData);
								
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | beacon 1 ";
										$logMessage .= "\n\r jsonData: ".print_r($jsonData, true);
										$logMessage .= "\n\r data: ".print_r($data, true);
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

										$b64dTokenExpire = base64_decode($data->token);
										$b64dtokenExpire_view = date("Y-m-d H:i:s", $b64dTokenExpire);										

										
										$tokenNow = strtotime("+0 minutes");
										$tokenNow_view = date("Y-m-d H:i:s", $tokenNow);

										if ( $tokenNow < $b64dTokenExpire )
										{
											$result = "inside timeframe";
										}
										else
										{
											$result = "expired timeframe";											
										}
										
										
										$validToken = ( $tokenNow < $b64dTokenExpire ) ? 1 : 0 ; 

										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ";	
										$logMessage .= "\n\r b64dTokenExpire: ".print_r( $b64dTokenExpire, true );
										$logMessage .= "\n\r tokenNow: ".print_r( $tokenNow, true );
										$logMessage .= "\n\r b64dTokenExpire_view: ".print_r( $b64dtokenExpire_view, true );
										$logMessage .= "\n\r tokenNow_view: ".print_r( $tokenNow_view, true );
										$logMessage .= "\n\r result: ".print_r( $result, true );
										$logMessage .= "\n\r validToken: ".print_r( $validToken, true );
										#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	



  
										if (  $validToken == "1" )
										{
											#valid token
											$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows Valid Token";
											if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }											


											$objInput = new stdClass();
									        $objInput->flag_api = "0";
									        $objInput->trace_number = $data->billNumber;
									        $objInput->zip = $data->zipCode;
									        $objInput->component_origin == "com_shipnsd_api";
	
											$objGetOrderData = Nsd_schedulingController::getOrderData( $objInput );

											$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ";	
											$logMessage .= "\n\r objInput: ".print_r( $objInput, true );
											$logMessage .= "\n\r objGetOrderData: ".print_r( $objGetOrderData, true );
											if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


											if ( $objGetOrderData->error == "0" && ( $objGetOrderData->order->destination_zipcode != $objInput->zip ) )
											{
												
												$objGetOrderData->error = "1";
												$objGetOrderData->error_code = "57";
												$objGetOrderData->system_error_message = "zipCode does not match the order destination zipcode";
												
											}


											switch( $objGetOrderData-> error )
											{
												
												case "0":
													http_response_code(200);
													$apiReturn = new stdClass();
													$apiReturn->result = "ok";	
	
	
	
	
												$arrWindows = array();
												foreach( $objGetOrderData->days as $days )
												{
													$objWindow = new stdClass();
													$objWindow->trace_number = $objGetOrderData->order->trace_number;
													$objWindow->detail_line_id = $objGetOrderData->order->detail_line_id;
													$objWindow->request_delivery_date = $days->windows[0]->request_date_start;
													$objWindow->requested_time_start = $days->windows[0]->request_date_start;
													$objWindow->requested_time_end = $days->windows[0]->request_date_end;
													
													$objWindow->docked_date = $objGetOrderData->order->docked_date;
													$objWindow->agent_id = $objGetOrderData->agent->id;
													$objWindow->care_of = $objGetOrderData->order->care_of;
													$objWindow->care_of_email = $objGetOrderData->order->care_of_email;
													$objWindow->customer_account_code = $objGetOrderData->order->customer_account_code;
													$objWindow->delivery_zipcode = $objGetOrderData->order->destination_zipcode;
													$objWindow->delivery_latitude = $objGetOrderData->order->destination_latitude;
													$objWindow->delivery_longitude = $objGetOrderData->order->destination_longitude;
													$objWindow->delivery_distance = $objGetOrderData->order->distance;;
													$objWindow->delivery_date = $days->windows[0]->request_date_start;
													$objWindow->delivery_window_id = $days->windows[0]->id;
													
	
	
													$objWindow->date = $days->windows[0]->APIdate;
													$objWindow->startTime = $days->windows[0]->APIstartTime;
													$objWindow->endTime = $days->windows[0]->APIendTime; 
													$objWindow->slotsAvailable = $days->windows[0]->APIslotsAvailable; 
										
													$arrWindows[] = $objWindow; 
													
												}
		
												$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ";	
												$logMessage .= "\n\r objInput: ".print_r( $objInput, true );
												$logMessage .= "\n\r objGetOrderData: ".print_r( $objGetOrderData, true );
												$logMessage .= "\n\r arrWindows: ".print_r( $arrWindows, true );
												if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		
		
		
		
												#window still exist with available slots
	
												$setWindow = 0;
	
												foreach ( $arrWindows as $window )
												{
	
												$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ";	
												$logMessage .= "\n\r window: ".print_r( $window, true );
												$logMessage .= "\n\r data: ".print_r( $data, true );
												if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
													
													if ( 	$window->date == $data->selectedDate &&  
															$window->startTime == $data->selectedStartTime && 
															$window->endTime == $data->selectedEndTime &&
															$window->slotsAvailable > 0 )
													{
														
														$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows window available";
														if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
														$setWindow = 1;
														break;																		
													}
													else
													{
														$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows Window not available";
														if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }								
														
													}
													
													
													
												}
		
												$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ";	
												$logMessage .= "\n\r setWindow: ".print_r( $setWindow, true );
												if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
												
	
	
	
												if ( $setWindow == "1" )
												{
													
													$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows window available";	
													$logMessage .= "\n\r window: ".print_r( $window, true );
													$logMessage .= "\n\r data: ".print_r( $data, true );
													if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }									
													
													
													
													$objUpdate = new stdClass();
													$objUpdate->error = "0";
													$objUpdate->error_code = "";
													$objUpdate->error_message = "";
													$objUpdate->trace_number = $window->trace_number;
													$objUpdate->detail_line_id = $window->detail_line_id;
													$objUpdate->requested_time_start = $window->requested_time_start;
													$objUpdate->requested_time_end = $window->requested_time_end;
													$objUpdate->window_id = $window->delivery_window_id;
											
											
													# UNCOMMENT FOR SYSTEM TESTING & PRODUCTION												
													$objUpdateFromTruckmate = Nsd_truckmateController::updateTruckmateData( $objUpdate );
													
													
													
													switch( $objUpdateFromTruckmate->error_code )
													{
														case "91":
														case "92":
	
															$apiReturn = new stdClass();
															$apiReturn->result = "noSchedule";
															$apiReturn->error_state = "1";
															$apiReturn->error_code = $objUpdateFromTruckmate->error_code;
															$apiReturn->error_message = "Order not scheduled";
															$apiReturn->system_error_message = $objUpdateFromTruckmate->system_error_message;
															
															
															break;
										
														case "":
															$objUpdateFromTruckmate->error = "0";
															$objUpdateFromTruckmate->error_code = "";
															$objUpdateFromTruckmate->error_message = "";
															
															
															$apiReturn = new stdClass();
															$apiReturn->result = "ok";
															
															
															break;
														
														default:
															$objUpdateFromTruckmate->error = "1";
															$objUpdateFromTruckmate->error_code = $objUpdateFromTruckmate->error_code;
															$objUpdateFromTruckmate->error_message = "We are currently unable to schedule your delivery. If the problem continues, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
	
	
															$apiReturn = new stdClass();
															$apiReturn->result = "noSchedule";
															$apiReturn->error_state = "1";
															$apiReturn->error_code = $objUpdateFromTruckmate->error_code;
															$apiReturn->error_message = "Order not scheduled";
															$apiReturn->system_error_message = $objUpdateFromTruckmate->system_error_message;
	
	
	
	
															break;
														
													}
													
											
											
											
											
													 
													if ( $objUpdateFromTruckmate->error == "0" )
													{
	
														$objDeliveries = new stdClass();
														$objDeliveries->system = "API";
														$objDeliveries->agent_id = $window->agent_id;
														$objDeliveries->client_id = "";
														$objDeliveries->order_id = $window->trace_number;
														$objDeliveries->detail_line_id = $window->detail_line_id;
														$objDeliveries->delivery_zipcode = $window->delivery_zipcode;
														$objDeliveries->delivery_latitude = $window->delivery_latitude;
														$objDeliveries->delivery_longitude = $window->delivery_longitude;
														$objDeliveries->delivery_distance = $window->delivery_distance;
														$objDeliveries->delivery_date = $window->delivery_date;
														$objDeliveries->delivery_window_id = $window->delivery_window_id;
														$objDeliveries->delivery_type_id = "1";
														$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
														$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
														$objDeliveries->docked_date = $window->docked_date;
														$objDeliveries->record_type = "update";	
														$objDeliveries->request_delivery_date = $window->request_delivery_date;	
														$objDeliveries->request_time_window_start = $window->request_delivery_date;	
														$objDeliveries->request_time_window_end = $window->requested_time_end;		
														
														$objDeliveries->error = "0";
														$objDeliveries->error_code = "";
														$objDeliveries->system_error_message = "";
														
														$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
														$deliveryID = $db->insertid();
												
														$objAgentSchedule = MetalakeHelperCore::getObjTableData( $table="htc_os_agents", $field="id", $value=$window->agent_id );
												
														$objAgentDispatchtrack = MetalakeHelperCore::getObjTableData( $table="htc_dispatchtrack_agents", $field="agent_code", $value=$objAgentSchedule->name );
												
												
														$logMessage = "INSIDE | ".$className." | ".$functionName." | udpate process table";
														$logMessage .= "\n\r objAgentSchedule: ".print_r($objAgentSchedule, true);
														$logMessage .= "\n\r objAgentDispatchtrack: ".print_r($objAgentDispatchtrack, true);
														$logMessage .= "\n\r objUpdateFromTruckmate: ".print_r($objUpdateFromTruckmate, true);
														$logMessage .= "\n\r ";
														if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
											
											
														$update_system = ( count($objAgentDispatchtrack) > 0 ) ?  "dispatchtrack" : "" ;
											
											
														$email_body = "";
											
														$email_body .= $window->care_of." client with order number ".$window->trace_number." has scheduled their delivery online for the day of ".date('n/d', strtotime($objWindow->requested_time_start))." between ".date('ga', strtotime($window->requested_time_start))." and ".date('ga', strtotime($objWindow->requested_time_end)).". Please call the customer the day prior to setup their window.";
											
														$email_body .= "<br><br>Sincerely, NSD.";
														
														$email_body .= "<br><br>Please reach out to your assigned LMC if you should have any questions.";
											
											
														$email_from = "shipping@shipnsd.com"; 
											
											
														$objNotify = new stdClass();
														$objNotify->email_to = $window->care_of_email;
														#$objNotify->email_to = "rfeldman@metalake.com";
														$objNotify->email_to = "lsawyer@metalake.com";
														$objNotify->email_from = $email_from;
														$objNotify->email_subject = "Order number ".$window->trace_number." has been scheduled online for ".date('n/d ga', strtotime($objWindow->requested_time_start))."-".date('ga', strtotime($window->requested_time_end))." – Please call to confirm exact window";
														$objNotify->email_body = $email_body;
											
														$objNotify->email_category = "agent notify";
														
														$jsonObjNotify = json_encode( $objNotify );


														$objScheduleInfo = new stdClass();
														$objScheduleInfo->agent = $window->care_of;
														$objScheduleInfo->order_id = $window->trace_number;
														$objScheduleInfo->scheduled_delivery_date = date('n/d', strtotime($objWindow->requested_time_start));
														$objScheduleInfo->scheduled_delivery_start_time = date('ga', strtotime($window->requested_time_start));
														$objScheduleInfo->scheduled_delivery_end_time = date('ga', strtotime($objWindow->requested_time_end));
											
											
											
														$jsonObjScheduleInfo = json_encode( $objScheduleInfo );

											
											
														$objInsertProccessUpdate = new stdClass();
														$objInsertProccessUpdate->order_id = $objWindow->trace_number;
														$objInsertProccessUpdate->process_datetime = "";
														$objInsertProccessUpdate->process_datetime_gmt = "";		
														$objInsertProccessUpdate->stamp_datetime = date("Y-m-d H:i:s");
														$objInsertProccessUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");	
														$objInsertProccessUpdate->update_system = $update_system;		
														$objInsertProccessUpdate->notify = $jsonObjNotify;		
														$objInsertProccessUpdate->agent_code = $window->care_of;
														$objInsertProccessUpdate->order_schedule_info = $jsonObjScheduleInfo;
														
														$result = JFactory::getDbo()->insertObject('htc_process_update', $objInsertProccessUpdate);
														$InsertIDProcessID = $db->insertid();
														
														
														
														#SUCCESS - SEND RESPONSE
														
														http_response_code(200);
													
	
														$return_value = json_encode($apiReturn);
														
		
													
														$logTable = "htc_shipnsd_api_log";
														$logObj = new stdClass();
														$logObj->agent_id = $objGetOrderData->agent->id;
														$logObj->trace_number = $data->billNumber;
														$logObj->zip_code = $data->zipCode;
														$logObj->process_id = $process_id;
														$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
														$logObj->datetime = date("Y-m-d H:i:s");
														$logObj->milliseconds = round(microtime(true) * 1000);
														$logObj->user = $serverValues["PHP_AUTH_USER"];
														$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
														$logObj->request_method = $serverValues["REQUEST_METHOD"];
														$logObj->collection = $collection;
														$logObj->command = $command;
														$logObj->return_value = $return_value;
														$logObj->description = "response";
														$logObj->server_values = json_encode($serverValues);
														$logObj->error_state = $apiReturn->error_state;
														$logObj->error_code = $apiReturn->error_code;
														$logObj->error_message = $apiReturn->error_message;															
														MetalakeHelperCore::logOBJ( $logTable, $logObj );


							
														echo $return_value;															
														
														
														
														
													}
													else
													{
														
														$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows fail to update truckmate";
														if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
													
														http_response_code(400);
													
	
														$return_value = json_encode($apiReturn);
														
		
													
														$logTable = "htc_shipnsd_api_log";
														$logObj = new stdClass();
														$logObj->agent_id = $objGetOrderData->agent->id;
														$logObj->trace_number = $data->billNumber;
														$logObj->zip_code = $data->zipCode;
														$logObj->process_id = $process_id;
														$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
														$logObj->datetime = date("Y-m-d H:i:s");
														$logObj->milliseconds = round(microtime(true) * 1000);
														$logObj->user = $serverValues["PHP_AUTH_USER"];
														$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
														$logObj->request_method = $serverValues["REQUEST_METHOD"];
														$logObj->collection = $collection;
														$logObj->command = $command;
														$logObj->return_value = $return_value;
														$logObj->description = "Fail to update from truckmate";
														$logObj->server_values = json_encode($serverValues);
														$logObj->error_state = $apiReturn->error_state;
														$logObj->error_code = $apiReturn->error_code;
														$logObj->error_message = $apiReturn->error_message;															
														
														MetalakeHelperCore::logOBJ( $logTable, $logObj );



														$objDeliveries = new stdClass();
														$objDeliveries->system = "API";
														$objDeliveries->agent_id = $window->agent_id;
														$objDeliveries->client_id = "";
														$objDeliveries->order_id = $window->trace_number;
														$objDeliveries->detail_line_id = $window->detail_line_id;
														$objDeliveries->delivery_zipcode = $window->delivery_zipcode;
														$objDeliveries->delivery_latitude = $window->delivery_latitude;
														$objDeliveries->delivery_longitude = $window->delivery_longitude;
														$objDeliveries->delivery_distance = $window->delivery_distance;
														$objDeliveries->delivery_date = $window->delivery_date;
														$objDeliveries->delivery_window_id = $window->delivery_window_id;
														$objDeliveries->delivery_type_id = "1";
														$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
														$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
														$objDeliveries->docked_date = $window->docked_date;
														$objDeliveries->record_type = "update";	
														$objDeliveries->request_delivery_date = $window->request_delivery_date;	
														$objDeliveries->request_time_window_start = $window->request_delivery_date;	
														$objDeliveries->request_time_window_end = $window->requested_time_end;		
														
														$objDeliveries->error = $apiReturn->error_state;
														$objDeliveries->error_code = $apiReturn->error_code;
														$objDeliveries->error_message = $apiReturn->error_message;
														$objDeliveries->system_error_message = $apiReturn->system_error_message;
														
														$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
														$deliveryID = $db->insertid();

							
														echo $return_value;														
														
														
														
													}
											
												}
												else
												{
	
													#no window available
													
													$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows window not available";
													if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
												
													http_response_code(400);
												
													$apiReturn = new stdClass();
													$apiReturn->result = "noSchedule";
													$apiReturn->error_state = "1";
													$apiReturn->error_code = "62";
													$apiReturn->error_message = "Window not available";
													$return_value = json_encode($apiReturn);
													
	
												
													$logTable = "htc_shipnsd_api_log";
													$logObj = new stdClass();
													$logObj->agent_id = $objGetOrderData->agent->id;
													$logObj->trace_number = $data->billNumber;
													$logObj->zip_code = $data->zipCode;
													$logObj->process_id = $process_id;
													$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
													$logObj->datetime = date("Y-m-d H:i:s");
													$logObj->milliseconds = round(microtime(true) * 1000);
													$logObj->user = $serverValues["PHP_AUTH_USER"];
													$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
													$logObj->request_method = $serverValues["REQUEST_METHOD"];
													$logObj->collection = $collection;
													$logObj->command = $command;
													$logObj->return_value = $return_value;
													$logObj->description = "Window not available";
													$logObj->server_values = json_encode($serverValues);
													$logObj->error_state = $apiReturn->error_state;
													$logObj->error_code = $apiReturn->error_code;
													$logObj->error_message = $apiReturn->error_message;													
													MetalakeHelperCore::logOBJ( $logTable, $logObj );


													#log to htc_os_deliveries
													$objDeliveries = new stdClass();
													$objDeliveries->system = "API";
													$objDeliveries->agent_id = $objGetOrderData->agent->id;
													$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
													$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
													$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
													$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
													$objDeliveries->result = $apiReturn->result;	
													$objDeliveries->record_type = "post";	
													$objDeliveries->error = $apiReturn->error_state;
													$objDeliveries->error_code = $apiReturn->error_code;
													$objDeliveries->system_error_message = $apiReturn->error_message;
													$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
													$deliveryID = $db->insertid();
						
													echo $return_value;	
													
													
												}
	
										
													break;
									
	
												case "1":
												case "99":
	
	
													switch( $objGetOrderData-> error_code  )
													{
	
														case "01":
														case "02":
														case "03":
														case "04":
														case "05":
														case "06":
														case "07":
															# Truckmate Issue
															http_response_code(500);
															$apiReturn = new stdClass();
															$apiReturn->result = "noSchedule";
															$apiReturn->error_state = "1";
															$apiReturn->error_code = $objGetOrderData-> error_code;
															$apiReturn->error_message = "There has been an error.  We have been alerted and are working toward a solution.";					
										
										
										
															$body = "";
															$body .= "<br>The com_nsd_scheduling API is throwing a Truckmate error<br>";
															$body .= "<br><br>objDisplay: <br>";
															$body .= print_r($objDisplay, true);
															$body .= "<br>";
															
												
															$objSendEmailMessage = new stdClass();
															$objSendEmailMessage->email_to = "lsawyer@metalake.com";
															$objSendEmailMessage->email_from = "nsdalert@metalake.com";
															$objSendEmailMessage->email_subject = "ALERT | com_nsd_scheduling | API ";
															$objSendEmailMessage->email_body = $body;
															$objSendEmailMessage->email_category = "scheduling support";
													
															$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
														
															break;
	
														case "31":
														case "32":
	
															# set header to 400 - Missing parameters and validation errors			
															http_response_code(400);
															$apiReturn = new stdClass();
															$apiReturn->result = "notFound";
															$apiReturn->error_state = "1";
															$apiReturn->error_code = $objGetOrderData-> error_code;
															$apiReturn->error_message = $objGetOrderData-> system_error_message;	
														
															break;
	
	
														case "51":
	
															# set header to 400 - Missing parameters and validation errors			
															http_response_code(400);
															$apiReturn = new stdClass();
															$apiReturn->result = "noReschedule";
															$apiReturn->error_state = "1";
															$apiReturn->error_code = $objGetOrderData-> error_code;
															$apiReturn->error_message = $objGetOrderData-> system_error_message;	
														
															break;
	
	
	
														default:
														
															# set header to 400 - Missing parameters and validation errors			
															http_response_code(400);
															$apiReturn = new stdClass();
															$apiReturn->result = "noSchedule";
															$apiReturn->error_state = "1";
															$apiReturn->error_code = $objGetOrderData-> error_code;
															$apiReturn->error_message = $objGetOrderData-> system_error_message;											
															
															break;
														
													}
	
	
													$return_value = json_encode($apiReturn);
													
	
												
													$logTable = "htc_shipnsd_api_log";
													$logObj = new stdClass();
													$logObj->agent_id = $objGetOrderData->agent->id;
													$logObj->trace_number = $data->billNumber;
													$logObj->zip_code = $data->zipCode;
													$logObj->process_id = $process_id;
													$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
													$logObj->datetime = date("Y-m-d H:i:s");
													$logObj->milliseconds = round(microtime(true) * 1000);
													$logObj->user = $serverValues["PHP_AUTH_USER"];
													$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
													$logObj->request_method = $serverValues["REQUEST_METHOD"];
													$logObj->collection = $collection;
													$logObj->command = $command;
													$logObj->return_value = $return_value;
													$logObj->description = "Window not available";
													$logObj->server_values = json_encode($serverValues);
													$logObj->error_state = $apiReturn->error_state;
													$logObj->error_code = $apiReturn->error_code;
													$logObj->error_message = $apiReturn->error_message;	
													MetalakeHelperCore::logOBJ( $logTable, $logObj );


													#log to htc_os_deliveries
													$objDeliveries = new stdClass();
													$objDeliveries->system = "API";
													$objDeliveries->agent_id = $objGetOrderData->agent->id;
													$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
													$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
													$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
													$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
													$objDeliveries->result = $apiReturn->result;	
													$objDeliveries->record_type = "post";	
													$objDeliveries->error = $apiReturn->error_state;
													$objDeliveries->error_code = $apiReturn->error_code;
													$objDeliveries->system_error_message = $apiReturn->error_message;
													$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
													$deliveryID = $db->insertid();

						
													echo $return_value;	
	
	
												
													break;
	
									
									
												default:
									
													http_response_code(500);
													$apiReturn = new stdClass();
													$apiReturn->result = "noSchedule";
													$apiReturn->error_state = "1";
													$apiReturn->error_code = $objDisplay->error_code;
													$apiReturn->error_message = "There has been an error.  We have been alerted and are working toward a solution.";	;					
									
													$body = "";
													$body .= "<br>The com_nsd_scheduling API is throwing a Truckmate error<br>";
													$body .= "<br><br>objDisplay: <br>";
													$body .= print_r($objDisplay, true);
													$body .= "<br>";
													
										
													$objSendEmailMessage = new stdClass();
													$objSendEmailMessage->email_to = "lsawyer@metalake.com";
													$objSendEmailMessage->email_from = "nsdalert@metalake.com";
													$objSendEmailMessage->email_subject = "ALERT | com_nsd_scheduling | API ";
													$objSendEmailMessage->email_body = $body;
													$objSendEmailMessage->email_category = "scheduling support";
											
													$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
	
													$return_value = json_encode($apiReturn);
													
	
												
													$logTable = "htc_shipnsd_api_log";
													$logObj = new stdClass();
													$logObj->agent_id = $objGetOrderData->agent->id;
													$logObj->trace_number = $data->billNumber;
													$logObj->zip_code = $data->zipCode;
													$logObj->process_id = $process_id;
													$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
													$logObj->datetime = date("Y-m-d H:i:s");
													$logObj->milliseconds = round(microtime(true) * 1000);
													$logObj->user = $serverValues["PHP_AUTH_USER"];
													$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
													$logObj->request_method = $serverValues["REQUEST_METHOD"];
													$logObj->collection = $collection;
													$logObj->command = $command;
													$logObj->return_value = $return_value;
													$logObj->description = "truckmate error";
													$logObj->server_values = json_encode($serverValues);
													$logObj->error_state = $apiReturn->error_state;
													$logObj->error_code = $apiReturn->error_code;
													$logObj->error_message = $apiReturn->error_message;													
													MetalakeHelperCore::logOBJ( $logTable, $logObj );


													#log to htc_os_deliveries
													$objDeliveries = new stdClass();
													$objDeliveries->system = "API";
													$objDeliveries->agent_id = $objGetOrderData->agent->id;
													$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
													$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
													$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
													$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
													$objDeliveries->result = $apiReturn->result;	
													$objDeliveries->record_type = "post";	
													$objDeliveries->error = $apiReturn->error_state;
													$objDeliveries->error_code = $apiReturn->error_code;
													$objDeliveries->system_error_message = $apiReturn->error_message;
													$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
													$deliveryID = $db->insertid();

						
													echo $return_value;									
									
									
													break;
														
														
												
											}


											
										}
										else
										{
											#invalid token

											$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command get-windows Invalid Token";
											if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										
											http_response_code(400);
										
											$apiReturn = new stdClass();
											$apiReturn->result = "noSchedule";
											$apiReturn->error_state = "1";
											$apiReturn->error_code = "61";
											$apiReturn->error_message = "Token expired";
											$return_value = json_encode($apiReturn);
											
											#$return_value = "Error [66] - Invalid request method. Expecting POST.";									
										
											$logTable = "htc_shipnsd_api_log";
											$logObj = new stdClass();
											$logObj->trace_number = $data->billNumber;
											$logObj->zip_code = $data->zipCode;
											$logObj->process_id = $process_id;
											$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
											$logObj->datetime = date("Y-m-d H:i:s");
											$logObj->milliseconds = round(microtime(true) * 1000);
											$logObj->user = $serverValues["PHP_AUTH_USER"];
											$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
											$logObj->request_method = $serverValues["REQUEST_METHOD"];
											$logObj->collection = $collection;
											$logObj->command = $command;
											$logObj->return_value = $return_value;
											$logObj->description = "http_response_code set to 400 | Token Expired";
											$logObj->server_values = json_encode($serverValues);
											$logObj->error_state = $apiReturn->error_state;
											$logObj->error_code = $apiReturn->error_code;
											$logObj->error_message = $apiReturn->error_message;												
											MetalakeHelperCore::logOBJ( $logTable, $logObj );

											#log to htc_os_deliveries
											$objDeliveries = new stdClass();
											$objDeliveries->system = "API";
											$objDeliveries->agent_id = $objGetOrderData->agent->id;
											$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
											$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
											$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
											$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
											$objDeliveries->result = $apiReturn->result;	
											$objDeliveries->record_type = "post";	
											$objDeliveries->error = $apiReturn->error_state;
											$objDeliveries->error_code = $apiReturn->error_code;
											$objDeliveries->system_error_message = $apiReturn->error_message;
											$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
											$deliveryID = $db->insertid();

				
											echo $return_value;	
											
										}

									
										
									}								
								
								
									break;	
									
									
								default:
		
									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule command default ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

									# set header to 405 - Missing parameters and validation errors			
									http_response_code(405);

									$apiReturn = new stdClass();
									$apiReturn->error_state = "1";
									$apiReturn->error_code = "60";
									$apiReturn->error_message = "Invalid command";
									$return_value = json_encode($apiReturn);


									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | schedule Get Windows";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

									$logTable = "htc_shipnsd_api_log";
									$logObj = new stdClass();
									$logObj->trace_number = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
									$logObj->zip_code = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
									$logObj->process_id = $process_id;
									$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
									$logObj->datetime = date("Y-m-d H:i:s");
									$logObj->milliseconds = round(microtime(true) * 1000);
									$logObj->user = $serverValues["PHP_AUTH_USER"];
									$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
									$logObj->request_method = $serverValues["REQUEST_METHOD"];
									$logObj->collection = $collection;
									$logObj->command = $command;
									$logObj->return_value = $return_value;
									$logObj->description = "ERROR | http_response_code set to 405 | Invalid command";
									$logObj->server_values = json_encode($serverValues);
									$logObj->error_state = $apiReturn->error_state;
									$logObj->error_code = $apiReturn->error_code;
									$logObj->error_message = $apiReturn->error_message;							
									MetalakeHelperCore::logOBJ( $logTable, $logObj );


									#log to htc_os_deliveries
									$objDeliveries = new stdClass();
									$objDeliveries->system = "API";
									$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
									$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
									$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
									$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
									$objDeliveries->record_type = "system";	
									$objDeliveries->error = $apiReturn->error_state;
									$objDeliveries->error_code = $apiReturn->error_code;
									$objDeliveries->system_error_message = $apiReturn->error_message;
									$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
									$deliveryID = $db->insertid();



									echo $return_value;	
								
									break;	
								
								
							}
						
							break;


						case "order":  // used for tracking api calls
									
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | trace ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	

							switch( $command )
							{
								case "trace":
								case "get-trace":
								
									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | order trace";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


									if ( $serverValues["REQUEST_METHOD"] != "GET" )
									{
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | order trace NOT GET";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
										http_response_code(400);
									
										$apiReturn = new stdClass();
										$apiReturn->result = "noSchedule";
										$apiReturn->error_state = "1";
										$apiReturn->error_code = "63";
										$apiReturn->error_message = "Invalid request method. Expecting GET.";
										$return_value = json_encode($apiReturn);
										
									
										$logTable = "htc_shipnsd_api_log";
										$logObj = new stdClass();
										$logObj->trace_number = $traceNumber;
										$logObj->zip_code = $zipCode;										
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
										$logObj->datetime = date("Y-m-d H:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->return_value = $return_value;
										$logObj->description = "http_response_code set to 400 | Request Method should be GET";
										$logObj->server_values = json_encode($serverValues);
										$logObj->error_state = $apiReturn->error_state;
										$logObj->error_code = $apiReturn->error_code;
										$logObj->error_message = $apiReturn->error_message;											
										MetalakeHelperCore::logOBJ( $logTable, $logObj );
			
										echo $return_value;	

									}
									else
									{
										
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | order trace GET";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }										


										#$jinput = JFactory::getApplication()->input;
										$trace_number	= $jinput->get('trace_number', '', 'STRING');
										$tm4web_usr		= $jinput->get('user', '', 'STRING');
										$tm4web_pwd 		= $jinput->get('pswd', '', 'STRING');
										$output 			= $jinput->get('output', '', 'STRING');

										$objInput = new stdClass();
										$objInput->flag_api = "1";
										$objInput->trace_number = $trace_number;
										$objInput->tmw4web_user = $tm4web_usr;
										$objInput->tmw4web_password = $tm4web_pwd;
										$objInput->output = $output;
										$objInput->flag_trace_page = "0";
										$objInput->error_code_test = "";

										$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
										$logMessage .= "\n\r objInput: ".print_r($objInput, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


										$objTMW = Nsd_truckmateController::getTruckmateData( $objInput );

										$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
										$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


										$objTrackingMatrix = Nsd_trackingController::getTrackingMatrix( $objInput, $objTMW );


										$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
										$logMessage .= "\n\r objTrackingMatrix: ".print_r($objTrackingMatrix, true);
										$logMessage .= "\n\r ";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


										$objDisplay = new stdClass();


										if (  $objTMW->error  == "1" )
										{
											$objDisplay->error = $objTMW->error;
											
											$objDisplay->error_code = $objTMW->error_code;
											
											$objDisplay->error_message = $objTMW->error_message;			
										}
										else
										{			
										
											$objDisplay = Nsd_trackingController::getAPIDisplayDataForOutput( $objDisplay, $objInput, $objTMW, $objTrackingMatrix );			
	
											$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
											$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
											$logMessage .= "\n\r ";
											if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
	
											$return_value = json_encode($objDisplay);

											$apiReturn = new stdClass();
											$apiReturn->error_state = "";
											$apiReturn->error_code = "";
											$apiReturn->error_message = "";


											$logTable = "htc_shipnsd_api_log";
											$logObj = new stdClass();
											$logObj->trace_number = $trace_number;
											$logObj->zip_code = "";										
											#$logObj->process_id = $process_id;
											$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
											$logObj->datetime = date("Y-m-d H:i:s");
											$logObj->milliseconds = round(microtime(true) * 1000);
											$logObj->user = isset($serverValues["PHP_AUTH_USER"]) ? $serverValues["PHP_AUTH_USER"] : "" ;
											$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
											$logObj->request_method = $serverValues["REQUEST_METHOD"];
											$logObj->collection = $collection;
											$logObj->command = $command;
											$logObj->return_value = $return_value;
											$logObj->description = "http_response_code set to 400 | Request Method should be GET";
											$logObj->server_values = json_encode($serverValues);
											$logObj->error_state = $apiReturn->error_state;
											$logObj->error_code = $apiReturn->error_code;
											$logObj->error_message = $apiReturn->error_message;											
											MetalakeHelperCore::logOBJ( $logTable, $logObj );
	
	
											if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
											    $remote_address = $_SERVER['HTTP_CLIENT_IP'];
											} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
											    $remote_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
											} else {
											    $remote_address = $_SERVER['REMOTE_ADDR'];
											}
								
											$objActivityLog = new stdClass();
											$objActivityLog->remote_address =  $remote_address;
											$objActivityLog->trace = $trace_number;
											$objActivityLog->trace_type = "Bill Number";
											$objActivityLog->delivery_type = $objDisplay->delivery_type_description;
											$objActivityLog->order_type = $objDisplay->order_type;
											$objActivityLog->json_return = $return_value;
											$objActivityLog->tm4web_usr = $tm4web_usr;
											$objActivityLog->flag_api = $objInput->flag_api;
											$objActivityLog->return_format = $output;
											
											#Nsd_trackingController::logActivity( $objActivityLog );
											
											switch( $output )
											{
												case "xml": 
													$replacements = array();
													$return_value = XMLSerializer::generateValidXmlFromMixiedObj($objDisplay, "", $replacements, $add_header="false");
													break;
													
												default:
													$return_value = $return_value;
													break;
												
											}
											
											echo $return_value;
	
										
										}	

										
									}

									break;



								case "create-return-order":
								case "create":			
										$jsonData = file_get_contents("php://input");
										
										$data = json_decode($jsonData);
								
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3 ";
										$logMessage .= "\n\r jsonData: ".print_r($jsonData, true);
										$logMessage .= "\n\r data: ".print_r($data, true);
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
							
										$origin = $data->origin;
										$objDestination = $data->destination;
										
										$arrGoods = $data->goods;
							
							
										$arrDataIds = array();


											$objData = new stdClass();
											$objData->datetime = date("Y-m-d H:i:s");
											$objData->datetime_gmt = gmdate("Y-m-d H:i:s");
											
											$objData->reference1 = $data->reference1;
											$objData->reference2 = $data->reference2;
											$objData->reference3 = $data->reference3;
											$objData->reference4 = $data->reference4;
											$objData->disposal = $data->disposal;
											$objData->return_auth = $data->returnAuth;
											$objData->service_level = $data->localService;
											$objData->service_level_clean = Nsd_returnsController::cleanServiceLevel( $data->localService );
											$objData->pickup_date = $data->pickupTime;
											$objData->requested_date = property_exists('data','requestedDate') ? $data->requestedDate : "";
		
		
											#check additionalemail contains email, if so, put in origin email
											
											$returnEmailTest = MetalakeHelperCore::validateEmail( $origin->additionalInfo1 );		

											if ( $returnEmailTest == "1" )
											{
												
												$origin->email = $origin->additionalInfo1;
												$origin->additionalInfo1 = "";
												
											}


											
											$objEvalReturn = Nsd_returnsController::evalAdditionalInfo2( $origin->additionalInfo2 );



		
											$objData->customer_code = $data->customerCode;
											$objData->origin_name = $origin->name;
											$objData->origin_addressLine1 =$origin->addressLine1;
											$objData->origin_addressLine2 = $origin->addressLine2;
											$objData->origin_additionalInfo1 = $origin->additionalInfo1;
											$objData->origin_additionalInfo2 = $objEvalReturn->additionalInfo2;
											$objData->origin_city = $origin->city;
											$objData->origin_state = $origin->state;
											$objData->origin_zipCode = isset( $origin->zipCode ) ? $origin->zipCode : "" ;
											$objData->origin_country = isset( $origin->country ) ? $origin->country : "" ;
											$objData->origin_phone = $objEvalReturn->phone;
											$objData->origin_alt_phone = isset( $origin->altPhone ) ? $origin->altPhone : "";
											$objData->origin_email = isset( $origin->email ) ? $origin->email : "" ;
											
		
											$destinationCode = Nsd_returnsController::getDestinationCode( $objDestination );
											
											# 12/20: when the disposal flag is set to yes or true the destination code should be defaulted to 10343
											
											$destinationCode = ( strtolower($objData->disposal) == "yes" || strtolower($objData->disposal) == "true" || $objData->disposal == "1"  ) ? "13708" : $destinationCode  ;




											/*
											Home Depot indicate if the order is HAZMAT by putting the word HAZMAT in the consignee name or consignee address (or both).
											
											Whenever we get a HAZMAT order it needs to be processed into our system as disposal. i.e.   "disposal": "true",
											*/
											
											$modifyGoodsDesriptions = "0";
		
											if ( strpos( strtoupper($objDestination->name), "HAZMAT" ) !== false )
											{
												
												$objData->disposal = "true";
												
												$destinationCode = "13708";
												
												$modifyGoodsDesriptions = "1";
											}

		
											#if ( $destinationCode == "10343" ) 
											if ( $destinationCode != "" ) 
											{
												$objDestination->name = "";
												$objDestination->addressLine1 = "";
												$objDestination->addressLine2 = "";
												$objDestination->city = "";
												$objDestination->state = "";
												$objDestination->zipCode = "";
												$objDestination->country = "";
											}
		
		
											switch( $destinationCode )
											{
													
												case "13708":
													$destination_name_clean = "";
													break;	
												
												default:
													$destination_name_clean = $objDestination->name;

													break;
											}	
		
		
											$objData->destination_code = $destinationCode;
											$objData->destination_name = $objDestination->name;
											$objData->destination_name_clean = $destination_name_clean;
											$objData->destination_addressLine1 = $objDestination->addressLine1;
											$objData->destination_addressLine2 = $objDestination->addressLine2;
											$objData->destination_additionalInfo1 = $objDestination->additionalInfo1;
											$objData->destination_additionalInfo2 = $objDestination->additionalInfo2;
											$objData->destination_city = $objDestination->city;
											$objData->destination_state = $objDestination->state;
											$objData->destination_zipCode = isset( $objDestination->zipCode ) ? $objDestination->zipCode : "" ;
											$objData->destination_country = isset( $objDestination->country ) ? $objDestination->country : "" ;
											$objData->destination_phone = isset( $objDestination->phone ) ? $objDestination->phone : "" ;
											$objData->destination_alt_phone = isset( $objDestination->altPhone ) ? $objDestination->altPhone : "" ;
											$objData->destination_email = isset( $objDestination->email ) ? $objDestination->email : "" ;


											$result = JFactory::getDbo()->insertObject('htc_nsd_returns_data', $objData);
											$strDataId = $db->insertid();

										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3.5 ";
										$logMessage .= "\n\r objData: ".print_r($objData, true);
										$logMessage .= "\n\r result: ".print_r($result, true);
										$logMessage .= "\n\r strDataId: ".print_r($strDataId, true);
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


							
										foreach( $arrGoods as $good )
										{
							
											#save data
		
		
											$objDataItem = new stdClass();
											$objDataItem->data_id = $strDataId;
											$objDataItem->datetime = date("Y-m-d H:i:s");
											$objDataItem->datetime_gmt = gmdate("Y-m-d H:i:s");
											
											
											$objDataItem->goods_pieces = $good->pieces;
											$objDataItem->goods_weight = $good->weight;
											$objDataItem->goods_length = $good->length;
											$objDataItem->goods_width = $good->width;
											$objDataItem->goods_height = $good->height;
											
											$objDataItem->goods_description = ( $modifyGoodsDesriptions == "1" ) ? "HAZMAT - ".$good->description :  $good->description ;
											


											$objDataItem->goods_code = $good->code;
											$objDataItem->goods_code_clean = Nsd_returnsController::cleanGoodsCode( $good->code );
											
											$objDataItem->goods_commodity = ( $modifyGoodsDesriptions == "1" ) ? "HAZMAT" : "" ;

											
											$result = JFactory::getDbo()->insertObject('htc_nsd_returns_data_items', $objDataItem);
		
						                }
						                
						                
						                
						                $objImport = new stdClass();
						                $objImport->dataID = $strDataId;
						                
						                #$objImportReturn = Nsd_returnsController::importTMW( $objImport );
						                $objImportReturn = Nsd_truckmateController::insertHDReturnIntoTMW( $objImport );
						                #$objImportReturn = Nsd_truckmateController::insertHDReturnIntoTMW_new( $objImport );						                
						                
						                
						                
						                $arrReturn = (array)$objImportReturn;
						                
						                $jsonReturn = json_encode($arrReturn);
						                
										$arrFind = array('"billNumber":"', '","message"');
										$arrReplace = array('"billNumber":', ',"message"');
										$jsonReturn = str_replace( $arrFind, $arrReplace, $jsonReturn );						                


										$logTable = "htc_nsd_returns_log";
										$logObj = new stdClass();
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
										$logObj->datetime = date("Y-m-d h:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->customer_code = $customer_code;
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->description = "Response from TMW";
										$logObj->json_call = $jsonData;
										$logObj->return_value = $jsonReturn;
										$logObj->server_values = json_encode($serverValues);
										MetalakeHelperCore::logOBJ( $logTable, $logObj );


						                echo $jsonReturn;
						                break;

								
								
								
								
								case "createdev":
								
									if ( $serverValues["REQUEST_METHOD"] != "POST" )
									{
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | order create NOT POST";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
										http_response_code(400);
									
										$apiReturn = new stdClass();
										$apiReturn->result = "noSchedule";
										$apiReturn->error_state = "1";
										$apiReturn->error_code = "63";
										$apiReturn->error_message = "Invalid request method. Expecting POST.";
										$return_value = json_encode($apiReturn);
										
									
										$logTable = "htc_shipnsd_api_log";
										$logObj = new stdClass();
										$logObj->trace_number = $traceNumber;
										$logObj->zip_code = $zipCode;										
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
										$logObj->datetime = date("Y-m-d H:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->return_value = $return_value;
										$logObj->description = "http_response_code set to 400 | Request Method should be POST";
										$logObj->server_values = json_encode($serverValues);
										$logObj->error_state = $apiReturn->error_state;
										$logObj->error_code = $apiReturn->error_code;
										$logObj->error_message = $apiReturn->error_message;											
										MetalakeHelperCore::logOBJ( $logTable, $logObj );
			
										echo $return_value;	

									}
									else
									{

										$jsonData = file_get_contents("php://input");
										
										$data = json_decode($jsonData);
								
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$collection." | ".$command;
										$logMessage .= "\n\r jsonData: ".print_r($jsonData, true);
										$logMessage .= "\n\r data: ".print_r($data, true);
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

										$origin = $data->origin;
										$objDestination = $data->destination;
										
										$arrGoods = $data->goods;
							
							
										$arrDataIds = array();


											$objData = new stdClass();
											$objData->datetime = date("Y-m-d H:i:s");
											$objData->datetime_gmt = gmdate("Y-m-d H:i:s");
											$objData->order_type = strtolower($data->orderType);
											$objData->siteId = ($objData->order_type == "return") ? "SITE5" : "SITE6" ;
											$objData->reference1 = $data->reference1;
											$objData->reference2 = $data->reference2;
											$objData->reference3 = $data->reference3;
											$objData->reference4 = $data->reference4;
											$objData->disposal = $data->disposal;
											$objData->return_auth = $data->returnAuth;
											$objData->service_level = $data->localService;
											$objData->service_level_clean = Shipnsd_apiController::cleanServiceLevel( $data->localService );
											$objData->pickup_date = property_exists($data,'pickupTime') ? $data->pickupTime : "";
											$objData->requested_date = property_exists($data,'requestedDate') ? $data->requestedDate : "";

		
											#check additionalemail contains email, if so, put in origin email
											
											$returnEmailTest = MetalakeHelperCore::validateEmail( $origin->additionalInfo1 );		

											if ( $returnEmailTest == "1" )
											{
												
												$origin->email = $origin->additionalInfo1;
												$origin->additionalInfo1 = "";
												
											}


											
											$objEvalReturn = Shipnsd_apiController::evalAdditionalInfo2( $origin->additionalInfo2 );



		
											$objData->customer_code = $data->customerCode;
											$objData->origin_name = $origin->name;
											$objData->origin_addressLine1 =$origin->addressLine1;
											$objData->origin_addressLine2 = $origin->addressLine2;
											$objData->origin_additionalInfo1 = $origin->additionalInfo1;
											$objData->origin_additionalInfo2 = $objEvalReturn->additionalInfo2;
											$objData->origin_city = $origin->city;
											$objData->origin_state = $origin->state;
											$objData->origin_zipCode = isset( $origin->zipCode ) ? $origin->zipCode : "" ;
											$objData->origin_country = isset( $origin->country ) ? $origin->country : "" ;
											$objData->origin_phone = $objEvalReturn->phone;
											$objData->origin_alt_phone = isset( $origin->altPhone ) ? $origin->altPhone : "";
											$objData->origin_email = isset( $origin->email ) ? $origin->email : "" ;
											

		
		
											$objData->destination_code = $destinationCode;
											$objData->destination_name = $objDestination->name;
											$objData->destination_name_clean = $objDestination->name;
											$objData->destination_addressLine1 = $objDestination->addressLine1;
											$objData->destination_addressLine2 = $objDestination->addressLine2;
											$objData->destination_additionalInfo1 = $objDestination->additionalInfo1;
											$objData->destination_additionalInfo2 = $objDestination->additionalInfo2;
											$objData->destination_city = $objDestination->city;
											$objData->destination_state = $objDestination->state;
											$objData->destination_zipCode = isset( $objDestination->zipCode ) ? $objDestination->zipCode : "" ;
											$objData->destination_country = isset( $objDestination->country ) ? $objDestination->country : "" ;
											$objData->destination_phone = isset( $objDestination->phone ) ? $objDestination->phone : "" ;
											$objData->destination_alt_phone = isset( $objDestination->altPhone ) ? $objDestination->altPhone : "" ;
											$objData->destination_email = isset( $objDestination->email ) ? $objDestination->email : "" ;


											$result = JFactory::getDbo()->insertObject('htc_nsd_apiorders_data', $objData);
											$strDataId = $db->insertid();

										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3.5 ";

										$logMessage .= "\n\r objData: ".print_r($objData, true);
										$logMessage .= "\n\r result: ".print_r($result, true);
										$logMessage .= "\n\r strDataId: ".print_r($strDataId, true);
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


							
										foreach( $arrGoods as $good )
										{
							
											#save data
		
		
											$objDataItem = new stdClass();
											$objDataItem->data_id = $strDataId;
											$objDataItem->datetime = date("Y-m-d H:i:s");
											$objDataItem->datetime_gmt = gmdate("Y-m-d H:i:s");
											
											
											$objDataItem->goods_pieces = $good->pieces;
											$objDataItem->goods_weight = $good->weight;
											$objDataItem->goods_length = $good->length;
											$objDataItem->goods_width = $good->width;
											$objDataItem->goods_height = $good->height;
											
											$objDataItem->goods_description = $good->description ;
											


											$objDataItem->goods_code = $good->code;
											$objDataItem->goods_code_clean = Shipnsd_apiController::cleanGoodsCode( $good->code );
											
											$objDataItem->goods_commodity = "" ;

											
											$result = JFactory::getDbo()->insertObject('htc_nsd_apiorders_data_items', $objDataItem);
		
						                }
						                
						                
						                
						                $objImport = new stdClass();
						                $objImport->dataID = $strDataId;
						                
						                #$objImportReturn = Nsd_returnsController::importTMW( $objImport );
						                #$objImportReturn = Nsd_truckmateController::insertHDReturnIntoTMW( $objImport );
						                $objImportReturn = Nsd_truckmateController::insertOrderIntoTMW( $objImport );						                
						                
						                
						                
						                $arrReturn = (array)$objImportReturn;
						                
						                $jsonReturn = json_encode($arrReturn);
						                
										$arrFind = array('"billNumber":"', '","message"');
										$arrReplace = array('"billNumber":', ',"message"');
										$jsonReturn = str_replace( $arrFind, $arrReplace, $jsonReturn );						                


										$logTable = "htc_nsd_apiorders_log";
										$logObj = new stdClass();
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
										$logObj->datetime = date("Y-m-d h:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->customer_code = $customer_code;
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->description = "Response from TMW";
										$logObj->json_call = $jsonData;
										$logObj->return_value = $jsonReturn;
										$logObj->server_values = json_encode($serverValues);
										MetalakeHelperCore::logOBJ( $logTable, $logObj );


						                echo $jsonReturn;

										
									}								
									
									break;

								
								
								
								
								
								default:
	
									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | order default";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }								


									# set header to 405 - Missing parameters and validation errors			
									http_response_code(405);

									$apiReturn = new stdClass();
									$apiReturn->error_state = "1";
									$apiReturn->error_code = "60";
									$apiReturn->error_message = "Invalid command";
									$return_value = json_encode($apiReturn);


									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | order command  default";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

									$logTable = "htc_shipnsd_api_log";
									$logObj = new stdClass();
									$logObj->trace_number = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
									$logObj->zip_code = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
									$logObj->process_id = $process_id;
									$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
									$logObj->datetime = date("Y-m-d H:i:s");
									$logObj->milliseconds = round(microtime(true) * 1000);
									$logObj->user = $serverValues["PHP_AUTH_USER"];
									$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
									$logObj->request_method = $serverValues["REQUEST_METHOD"];
									$logObj->collection = $collection;
									$logObj->command = $command;
									$logObj->return_value = $return_value;
									$logObj->description = "ERROR | http_response_code set to 405 | Invalid command";
									$logObj->server_values = json_encode($serverValues);
									$logObj->error_state = $apiReturn->error_state;
									$logObj->error_code = $apiReturn->error_code;
									$logObj->error_message = $apiReturn->error_message;							
									MetalakeHelperCore::logOBJ( $logTable, $logObj );


									echo $return_value;	


	
									break;
							}

							break;


		
						default:
		
							# set header to 405 Method Not Allowed			
							#http_response_code(405);				
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 405 | Invalid collection";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
							#$return_value = "Error [66] - Invalid collection";

							$apiReturn = new stdClass();
							$apiReturn->error_state = "1";
							$apiReturn->error_code = "66";
							$apiReturn->error_message = "Invalid collection";
							$return_value = json_encode($apiReturn);

		
							$logTable = "htc_shipnsd_api_log";
							$logObj = new stdClass();
							$logObj->trace_number = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
							$logObj->zip_code = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
							$logObj->process_id = $process_id;
							$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
							$logObj->datetime = date("Y-m-d H:i:s");
							$logObj->milliseconds = round(microtime(true) * 1000);
							$logObj->user = $serverValues["PHP_AUTH_USER"];
							$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
							$logObj->request_method = $serverValues["REQUEST_METHOD"];
							$logObj->collection = $collection;
							$logObj->command = $command;
							$logObj->return_value = $return_value;
							$logObj->description = "ERROR | http_response_code set to 405 | Invalid collection";
							$logObj->server_values = json_encode($serverValues);
							$logObj->error_state = $apiReturn->error_state;
							$logObj->error_code = $apiReturn->error_code;
							$logObj->error_message = $apiReturn->error_message;							
							MetalakeHelperCore::logOBJ( $logTable, $logObj );
			

							#log to htc_os_deliveries
							$objDeliveries = new stdClass();
							$objDeliveries->system = "API";
							$objDeliveries->order_id = ( $traceNumber != "" ) ? $traceNumber : $data->billNumber;
							$objDeliveries->delivery_zipcode = ( $zipCode != "" ) ? $zipCode : $data->zipCode;
							$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
							$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
							$objDeliveries->record_type = "system";	
							$objDeliveries->error = $apiReturn->error_state;
							$objDeliveries->error_code = $apiReturn->error_code;
							$objDeliveries->system_error_message = $apiReturn->error_message;
							$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
							$deliveryID = $db->insertid();


							echo $return_value;							

		
							break;
						
					}


				}
				else
				{
	
					#throw error
		
					# set header to 429 Too Many Requests		
					http_response_code(429);
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 429 | Exceeded the API rate limit";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
					#$return_value = "Error [77] - User exceeded the API rate limit";

					$apiReturn = new stdClass();

					$apiReturn->error_state = "1";
					$apiReturn->error_code = "77";
					$apiReturn->error_message = "User exceeded the API rate limit";
					$return_value = json_encode($apiReturn);
			
			
					$logTable = "htc_shipnsd_api_log";
					$logObj = new stdClass();
					$logObj->trace_number = $traceNumber;
					$logObj->zip_code = $zipCode;
					$logObj->process_id = $process_id;
					$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
					$logObj->datetime = date("Y-m-d H:i:s");
					$logObj->milliseconds = round(microtime(true) * 1000);
					$logObj->user = $serverValues["PHP_AUTH_USER"];
					$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
					$logObj->request_method = $serverValues["REQUEST_METHOD"];
					$logObj->collection = $collection;
					$logObj->command = $command;
					$logObj->return_value = $return_value;
					$logObj->description = "ERROR | http_response_code set to 429 | Exceeded the API rate limit | apiInterval: ".$objValidation->apiInterval." | diffMilliseconds: ".$objValidation->diffMilliseconds;
					$logObj->server_values = json_encode($serverValues);
					$logObj->error_state = $apiReturn->error_state;
					$logObj->error_code = $apiReturn->error_code;
					$logObj->error_message = $apiReturn->error_message;

					MetalakeHelperCore::logOBJ( $logTable, $logObj );


					#log to htc_os_deliveries
					$objDeliveries = new stdClass();
					$objDeliveries->system = "API";
					$objDeliveries->order_id = $traceNumber;
					$objDeliveries->delivery_zipcode = $zipCode;
					$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
					$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
					$objDeliveries->record_type = "system";	
					$objDeliveries->error = $objData->error_state;
					$objDeliveries->error_code = $objData->error_code;
					$objDeliveries->system_error_message = $objData->error_message;
					$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
					$deliveryID = $db->insertid();

				
					echo $return_value;	
	
	
				}


			}
			else
			{

				#throw error
	
				# set header to 403 - 403 Forbidden The user might not have the necessary permissions for a resource, or may need an account of some sort.			
				http_response_code(403);
	
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 403 | User is not in the API group | strGroups: ".$strGroups;
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
				$strGroups = implode(",", $objValidation->arrGroups);

				#$return_value = "Error [88] - User not authorized for API";
	
				$objData->error_state = "1";
				$objData->error_code = "88";
				$objData->error_message = "User not authorized for API";
				$return_value = json_encode($objData);



				$logTable = "htc_shipnsd_api_log";
				$logObj = new stdClass();
				$logObj->trace_number = $traceNumber;
				$logObj->zip_code = $zipCode;
				$logObj->process_id = $process_id;
				$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
				$logObj->datetime = date("Y-m-d H:i:s");
				$logObj->milliseconds = round(microtime(true) * 1000);
				$logObj->user = $serverValues["PHP_AUTH_USER"];
				$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
				$logObj->request_method = $serverValues["REQUEST_METHOD"];
				$logObj->collection = $collection;
				$logObj->command = $command;
				$logObj->return_value = $return_value;
				$logObj->description = "ERROR | http_response_code set to 403 | User is not in the API group | strGroups: ".$strGroups;
				$logObj->server_values = json_encode($serverValues);
				$logObj->error_state = $objData->error_state;
				$logObj->error_code = $objData->error_code;
				$logObj->error_message = $objData->error_message;
				MetalakeHelperCore::logOBJ( $logTable, $logObj );


				#log to htc_os_deliveries
				$objDeliveries = new stdClass();
				$objDeliveries->system = "API";
				$objDeliveries->order_id = $traceNumber;
				$objDeliveries->delivery_zipcode = $zipCode;
				$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
				$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
				$objDeliveries->record_type = "system";	
				$objDeliveries->error = $objData->error_state;
				$objDeliveries->error_code = $objData->error_code;
				$objDeliveries->system_error_message = $objData->error_message;
				$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
				$deliveryID = $db->insertid();

				
				echo $return_value;	


			}
		
	
		}
		else
		{
			#throw error

			# set header to 401 - 401 Unauthorized		
			http_response_code(401);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 401 | Invalid user or password";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			#$return_value = "Error [99] - Invalid user or password";

			$objData->error_state = "1";
			$objData->error_code = "99";
			$objData->error_message = "Invalid user or password";
			$return_value = json_encode($objData);


			$logTable = "htc_shipnsd_api_log";
			$logObj = new stdClass();
			$logObj->trace_number = $traceNumber;
			$logObj->zip_code = $zipCode;			
			$logObj->process_id = $process_id;
			$logObj->datetime_gmt = gmdate("Y-m-d H:i:s");
			$logObj->datetime = date("Y-m-d H:i:s");
			$logObj->milliseconds = round(microtime(true) * 1000);
			$logObj->user = $serverValues["PHP_AUTH_USER"];
			$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
			$logObj->request_method = $serverValues["REQUEST_METHOD"];
			$logObj->collection = $collection;
			$logObj->command = $command;
			$logObj->return_value = $return_value;
			$logObj->description = "ERROR | http_response_code set to 401 | Invalid user or password";
			$logObj->server_values = json_encode($serverValues);
			
			$logObj->error_state = $objData->error_state;
			$logObj->error_code = $objData->error_code;
			$logObj->error_message = $objData->error_message;
			
			MetalakeHelperCore::logOBJ( $logTable, $logObj );



			#log to htc_os_deliveries
			$objDeliveries = new stdClass();
			$objDeliveries->system = "API";
			$objDeliveries->order_id = $traceNumber;
			$objDeliveries->delivery_zipcode = $zipCode;
			$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
			$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
			$objDeliveries->record_type = "system";	
			$objDeliveries->error = $objData->error_state;
			$objDeliveries->error_code = $objData->error_code;
			$objDeliveries->system_error_message = $objData->error_message;
			$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
			$deliveryID = $db->insertid();

			
			
/*
			JFactory::getDocument()->setMimeEncoding( 'application/text' );
			JResponse::setHeader('Content-Disposition','attachment;filename="results.text"');
*/
			echo $return_value;			
			
		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		JFactory::getApplication()->close(); // or jexit();


	}
}
