<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JLoader::registerPrefix('Nsd_scheduling', JPATH_SITE . '/components/com_nsd_scheduling/');

/**
 * Class Nsd_schedulingRouter
 *
 * @since  3.3
 */
class Nsd_schedulingRouter extends JComponentRouterBase
{
	/**
	 * Build method for URLs
	 * This method is meant to transform the query parameters into a more human
	 * readable form. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$query  An array of URL arguments
	 *
	 * @return  array  The URL arguments to use to assemble the subsequent URL.
	 *
	 * @since   3.3
	 */
	public function build(&$query)
	{
		$segments = array();
		$view     = null;
		
		

		if (isset($query['task']))
		{
			$taskParts  = explode('.', $query['task']);
			$segments[] = implode('/', $taskParts);
			$view       = $taskParts[0];
			unset($query['task']);
		}

		if (isset($query['view']))
		{
			$segments[] = $query['view'];
			$view = $query['view'];
			
			unset($query['view']);
		}

		if (isset($query['id']))
		{
			if ($view !== null)
			{
				$segments[] = $query['id'];
			}
			else
			{
				$segments[] = $query['id'];
			}

			unset($query['id']);
		}

		#echo $s = print_r($segments, true);

		return $segments;
	}

	/**
	 * Parse method for URLs
	 * This method is meant to transform the human readable URL back into
	 * query parameters. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$segments  The segments of the URL to parse.
	 *
	 * @return  array  The URL attributes to be used by the application.
	 *
	 * @since   3.3
	 */
	public function parse(&$segments)
	{
		$vars = array();
		switch($segments[0])
		{

		       case 'complete':
						$vars['view'] = 'complete';
						//$vars['id'] = $segments[1];
		                break;
		
/*
		       case 'schedule':
						$vars['view'] = 'schedule';
						$vars['t'] = $segments[1];
						$vars['z'] = $segments[2];
		                break;
*/



		       default:
						$vars['view'] = 'schedule';
						$vars['t'] = $segments[0];
						$vars['z'] = $segments[1];
		                break;

		
		}
		
		#echo $q = print_r($vars, true);
		
		return $vars;
	}
}
