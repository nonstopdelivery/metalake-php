<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Nsd_scheduling model.
 *
 * @since  1.6
 */
class Nsd_schedulingModelApi extends JModelItem
{


	public static function getObjDisplay( $objInput )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=getObjDisplay



		$controllerName = "Nsd_schedulingModelApi";
		$functionName = "getObjDisplay";
		
		$db = JFactory::getDBO();

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->api_token_expire_minutes		= $params->get('api_token_expire_minutes');

		
		$flag_production = 0;
		
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}		

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objDisplay = Nsd_schedulingController::getOrderData( $objInput );


		#check if oder zip code equals inupt zip code; if not throw error

		if ( $objDisplay->error == "0" && ( $objDisplay->order->destination_zipcode != $objInput->zip ) )
		{
			
			$objDisplay->error = "1";
			$objDisplay->error_code = "57";
			$objDisplay->system_error_message = "zipCode does not match the order destination zipcode";
			
		}




		$apiReturn = new stdClass();



		switch( $objDisplay-> error )
		{
			
			case "0":
				http_response_code(200);
	
				$apiReturn->result = "ok";	
	
				#token		
				$tokenExpire = strtotime("+".$paramsComponent->api_token_expire_minutes." minutes");
				$b64eTokenExpire = base64_encode($tokenExpire);	
				$apiReturn->token = $b64eTokenExpire;
				
				#windows
				$arrWindows = array();
				foreach( $objDisplay->days as $days )
				{
					$objWindow = new stdClass();
					$objWindow->date = $days->windows[0]->APIdate;
					$objWindow->startTime = $days->windows[0]->APIstartTime;
					$objWindow->endTime = $days->windows[0]->APIendTime; 
					$objWindow->slotsAvailable = $days->windows[0]->APIslotsAvailable; 
		
					$arrWindows[] = $objWindow; 
					
				}
				$apiReturn->windows = $arrWindows;
	
	
				break;


			case "1":
			case "99":				
				$apiReturn->result = "fail";

				
				$arrTruckmateErrorCodes = array("01","02","03","04","05","06","07");


				if ( in_array( $objDisplay-> error_code, $arrTruckmateErrorCodes ) )
				{

					# Truckmate Issue
					http_response_code(500);
					$apiReturn->error_code = $objDisplay-> error_code;
					$apiReturn->error_message = "There has been an error.  We have been alerted and are working toward a solution.";					



					$body = "";
					$body .= "<br>The com_nsd_scheduling API is throwing a Truckmate error<br>";
					$body .= "<br><br>objDisplay: <br>";
					$body .= print_r($objDisplay, true);
					$body .= "<br>";
					
		
					$objSendEmailMessage = new stdClass();
					$objSendEmailMessage->email_to = "lsawyer@metalake.com";
					$objSendEmailMessage->email_from = "nsdalert@metalake.com";
					$objSendEmailMessage->email_subject = "ALERT | com_nsd_scheduling | API ";
					$objSendEmailMessage->email_body = $body;
					$objSendEmailMessage->email_category = "scheduling support";
			
					$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );

					
				}
				else
				{
					# set header to 400 - Missing parameters and validation errors			
					http_response_code(400);
					$apiReturn->error_code = $objDisplay-> error_code;
					$apiReturn->error_message = $objDisplay-> system_error_message;
				}
				
				break;


			default:

				$apiReturn->result = "fail";

				http_response_code(500);
				$apiReturn->error_code = $objDisplay-> error_code;
				$apiReturn->error_message = "There has been an error.  We have been alerted and are working toward a solution.";	;					

				$body = "";
				$body .= "<br>The com_nsd_scheduling API is throwing a Truckmate error<br>";
				$body .= "<br><br>objDisplay: <br>";
				$body .= print_r($objDisplay, true);
				$body .= "<br>";
				
	
				$objSendEmailMessage = new stdClass();
				$objSendEmailMessage->email_to = "lsawyer@metalake.com";
				$objSendEmailMessage->email_from = "nsdalert@metalake.com";
				$objSendEmailMessage->email_subject = "ALERT | com_nsd_scheduling | API ";
				$objSendEmailMessage->email_body = $body;
				$objSendEmailMessage->email_category = "scheduling support";
		
				$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );



				break;
					
					
			
		}




		#this is the jsonReturn to call
		$jsonApiReturn = json_encode($apiReturn);

		#debugging
		$jsonApiReturndecode = json_decode($jsonApiReturn);


		$tokenExpire_view = date("Y-m-d H:i", $tokenExpire);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r apiReturn: ".print_r($apiReturn, true);
		$logMessage .= "\n\r jsonApiReturn: ".print_r($jsonApiReturn, true);
		$logMessage .= "\n\r jsonApiReturndecode: ".print_r($jsonApiReturndecode, true);
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r tokenExpire_view: ".print_r($tokenExpire_view, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		echo $jsonApiReturn;	
		JFactory::getApplication()->close(); // or jexit();

	}


}
