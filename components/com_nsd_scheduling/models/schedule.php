<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Nsd_scheduling model.
 *
 * @since  1.6
 */
class Nsd_schedulingModelSchedule extends JModelItem
{


	public static function getObjDisplay( $objInput )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=getObjDisplay



		$controllerName = "Nsd_schedulingModelSchedule";
		$functionName = "getObjDisplay";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}		

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objDisplay = Nsd_schedulingController::getOrderData( $objInput );


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objDisplay;
	}



	public function getTruckmateData( $objInput )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_truckmate&task=test_getTruckmateData



		$controllerName = "Nsd_schedulingModelSchedule";
		$functionName = "getTruckmateData";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}		

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objReturn = Nsd_truckmateController::getTruckmateData( $objInput );




		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}



}
