<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');


require JPATH_SITE.'/libraries/phpspreadsheet/vendor/autoload.php';
JLoader::register('Spreadsheet', JPATH_SITE.'/libraries/phpspreadsheet/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php');



/**
 * Class Nsd_schedulingController
 *
 * @since  1.6
 */
class Nsd_schedulingController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'deliveries');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}




	public static function getOrderData( $objInput )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=getOrderData



		$className = "Nsd_schedulingController";
		$functionName = "getOrderData";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objTruckmate = Nsd_truckmateController::getTruckmateData( $objInput );



		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objTruckmate: ".print_r($objTruckmate, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$objReturn = new stdClass();
		$objReturn->error_code_test = $objInput->error_code_test;


		if ( $objTruckmate->error == "0"  )
		{
			$objReturn->error = "0";
			$objReturn->error_code = "";
			$objReturn->error_message = "";
			
			
			$objOrder = new stdClass();
			
			$objOrder->detail_line_id 		= trim($objTruckmate->order->DETAIL_LINE_ID);
			$objOrder->trace_number 		= trim($objTruckmate->order->BILL_NUMBER);
			$objOrder->op_code 				= trim($objTruckmate->order->OP_CODE);
			$objOrder->origid 				= trim($objTruckmate->order->ORIGIN);
			
			$objOrder->current_status 		= trim($objTruckmate->order->CURRENT_STATUS);

			
			$objOrder->current_status_admin	= strtoupper(trim($objTruckmate->order->CURRENT_STATUS_ADMIN));

			
			
			$objOrder->service_level 		= trim($objTruckmate->order->SERVICE_LEVEL);

			
			$objOrder->delivery_appt_made	= trim($objTruckmate->order->DELIVERY_APPT_MADE); // true or false


			$objOrder->destination_zipcode 	= trim($objTruckmate->order->DESTPC);


			$objOrder->customer_account_code 	= trim($objTruckmate->order->CUSTOMER);

			
			
			$objOrder->agent_code 		= trim($objTruckmate->order->agent_code);
			$objOrder->care_of 			= trim($objTruckmate->order->CARE_OF);
			$objOrder->care_of_pc 		= trim($objTruckmate->order->CARE_OF_PC);
			$objOrder->care_of_email	= trim($objTruckmate->order->CARE_OF_EMAIL);


			$objOrder->order_history = $objTruckmate->order_history;
			

			switch(  $objInput->error_code_test	)
			{
				case"51":
					$objOrder->delivery_appt_made = "true";
					break;
		
				case"52":
					$objOrder->service_level = "WHITEGLOVE";
					break;	
	
				case"53":
					$objOrder->customer_account_code 	= "1164977"; // to test bad client code
					break;	
	
	
				case"54":
					$objOrder->destination_zipcode = "80124";  //to test zipcode not found in agents service area
					break;	
	
	
				case"55":
					$objOrder->current_status = "SHPTACK";
					break;
	
	
				case"56":
					$objOrder->agent_code = "MetaLake";
					break;
				
				default:
					break;
				
			}



			
			
			#determine Docked Date
			
			/*
			To determine the current “Docked Date” for an order:
			a. CURRENT_STATUS = “DOCKED”
			b. CURRENT_STATUS_ADMIN is “” or “PCF”
			c. Then get latest DOCKED record in order history and use the OS_INS_DATE
			
			
			An Order is not ready for scheduling because there is an issue with docked status.
			a. CURRENT_STATUS = “DOCKED”
			b. CURRENT_STATUS_ADMIN is “NCF”  *need client feedback
			c. The latest DOCKED record in order history has OS_SF_REASON_CODE not blank
			*/			

			#default
			$objOrder->status_docked = "false";


			if ( $objOrder->current_status == "DOCKED" && ($objOrder->current_status_admin == "" || $objOrder->current_status_admin == "PCF") )
			{



								
				$objLastDocked = Nsd_truckmateController::getOrderHistoryData( $objOrderHistory=$objOrder->order_history, $order_status="DOCKED" );
				


				# get the later of today, last docked date from order history and (if available) ETA of Docked date.						
				$arrDates = array();
				$arrDates[] = date("Y-m-d");
				$arrDates[] = date("Y-m-d", strtotime(trim($objLastDocked->OS_CHANGED)));
				#$arrDates[] = "2018-10-09";  // if available, need to put ETA date here
				
				# use the later of today and date returned.
				
				$max = max(array_map('strtotime', $arrDates));
				
				$objOrder->docked_date = date('Y-m-d', $max); 
				$objOrder->status_docked = "true";

				#$objReturn->order->docked_date = date('Y-m-d', $max); 
				#$objOrder->docked_date = trim($objTruckmate->order->docked_date);



				$logMessage = "INSIDE 1 | ".$className." | ".$functionName;
				$logMessage .= "\n\r objLastDocked: ".print_r($objLastDocked, true);
				$logMessage .= "\n\r arrDates: ".print_r($arrDates, true);
				$logMessage .= "\n\r max: ".print_r($max, true);
				$logMessage .= "\n\r objOrder->docked_date: ".print_r($objOrder->docked_date, true);
				$logMessage .= "\n\r objOrder->status_docked: ".print_r($objOrder->status_docked, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
				
			}


			/* If the order status is not DOCKED, the system will examine the order and look for a match on Client, Shipper, Service Level, and Mode. 
				If a match is found, the system will use the status from the table (e.g. DEPSHIP) for condition #3 . */






			if (  $objOrder->current_status != "DOCKED" )
			{
				
				$query = "select m.*, st.name as 'orderStatusName' from htc_os_matrix m
				left join htc_os_lu_clients c on c.id = m.id_client
				left join htc_os_lu_shipper s on s.id = m.id_shipper
				left join htc_os_lu_service_levels l on l.id = m.id_service_level
				left join htc_os_lu_mode o on o.id = m.id_mode
				left join htc_os_lu_order_status st on st.id = m.id_order_status
				where
				c.customer_account_code = '".$objOrder->customer_account_code."'
				and s.origid = '".$objOrder->origid."'
				and l.name = '".$objOrder->service_level."'
				and o.name = '".$objOrder->op_code."' 
				and st.name = '".$objOrder->current_status."' 
				and m.state = 1";
				$db->setQuery($query);
				$objMatrix = $db->loadObject() ;


					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r objOrder: ".print_r($objOrder, true);
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objMatrix: ".print_r($objMatrix, true);
					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				

				if ( is_object($objMatrix) && $objOrder->current_status == $objMatrix->orderStatusName  )
				{


					$objLastStatus = Nsd_truckmateController::getOrderHistoryData( $objOrderHistory=$objOrder->order_history, $order_status = $objMatrix->base_date );


					if ( $objLastStatus->history_exists == "1" )
					{


					# get the later of today, last docked date from order history and (if available) ETA of Docked date.						
					$arrDates = array();
					$arrDates[] = date("Y-m-d");
					$arrDates[] = date("Y-m-d", strtotime(trim($objLastStatus->OS_CHANGED)));
					#$arrDates[] = "2018-10-09";  // if available, need to put ETA date here
					

					# use the later of today and date returned.
					
					$max = max(array_map('strtotime', $arrDates));
					$objOrder->docked_date = date('Y-m-d', $max); 
	
					$objOrder->status_docked = "true";



					$logMessage = "INSIDE 2 | ".$className." | ".$functionName;
					$logMessage .= "\n\r objLastStatus: ".print_r($objLastStatus, true);
					$logMessage .= "\n\r arrDates: ".print_r($arrDates, true);
					$logMessage .= "\n\r max: ".print_r($max, true);
					$logMessage .= "\n\r objOrder->docked_date: ".print_r($objOrder->docked_date, true);
					$logMessage .= "\n\r objOrder->status_docked: ".print_r($objOrder->status_docked, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					}
					else
					{
						$objOrder->status_docked = "false";
					}




				}
				
			}





			
			$objReturn->order = $objOrder;
			$objReturn->system_error_message = "";
			

			
			$objZipcode = new stdClass();
			$objZipcode->zipcode_form = $objInput->zip;
			$objZipcode->zipcode_order = $objOrder->destination_zipcode;
			$objZipcode->zipcode_identical = ( $objZipcode->zipcode_form == $objZipcode->zipcode_order ) ? 1 : 0 ;

			$objReturn->zipcode_validation = $objZipcode;




			
			#Check if agent exists
			$query = "select a.* from htc_os_agents a WHERE a.name = '".$objOrder->agent_code."' ";
			$db->setQuery($query);
			$objAgent = $db->loadObject() ;
			
			




			if ( !$objAgent || count($objAgent) == 0  )
			{
					$objError = new stdClass();
					$objError->error_code = "56";
					$objError->customer_account_code = $objOrder->customer_account_code;
					$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );


					$objReturn->error = "1";
					$objReturn->error_code = "56";
					$objReturn->error_message = $objError->error_message_revised;
					#$objReturn->error_message = "Online scheduling is not available for your order. To schedule your delivery, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
					$objReturn->system_error_message = "Agent not set up in system. ".$objAgentComplete->error_message;

			
			}
			else
			{
				
				$objReturn->agent = $objAgent;
				
				
				
				

				if ( $objOrder->status_docked == "false" && strtoupper($objOrder->delivery_appt_made) == "FALSE"   )
				{
					$objError = new stdClass();
					$objError->error_code = "55";
					$objError->customer_account_code = $objOrder->customer_account_code;
					$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );

	
						$objReturn->error = "1";
						$objReturn->error_code = "55";
						$objReturn->error_message = $objError->error_message_revised;
						#$objReturn->error_message = "Online scheduling is not available for your order. To schedule your delivery, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";

						$objReturn->system_error_message = "Order not in DOCKED state";
	
				
				}
				else
				{	
					
	
					if ( strtoupper($objOrder->delivery_appt_made) == "TRUE"  )
					{

						$objError = new stdClass();
						$objError->error_code = "51";
						$objError->customer_account_code = $objOrder->customer_account_code;
						$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );
		
						$objReturn->error = "1";
						$objReturn->error_code = "51";
						$objReturn->error_message = $objError->error_message_revised;
						#$objReturn->error_message = "Our records show that delivery for this order has already been scheduled. To modify your scheduled delivery, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
						$objReturn->system_error_message = "Order already scheduled";
						
					}
					else
					{

						
						#check if order zip code is in the delivery area
						$query = "select a.* from htc_os_lu_geographical_areas a where a.agent_id = '".$objAgent->id."' and a.postalcode = '".$objOrder->destination_zipcode."' ";
				        $db->setQuery($query);
				        $objGeographicalPostalCode = $db->loadObject() ;

							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r query: ".print_r($query, true);
							$logMessage .= "\n\r objGeographicalPostalCode: ".print_r($objGeographicalPostalCode, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

						
						if ( !is_object($objGeographicalPostalCode) || count($objGeographicalPostalCode) == 0 )
						{

								$objError = new stdClass();
								$objError->error_code = "54";
								$objError->customer_account_code = $objOrder->customer_account_code;
								$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );
		
								$objReturn->error = "1";
								$objReturn->error_code = "54";
								$objReturn->error_message = $objError->error_message_revised;
								$objReturn->system_error_message = "Order delivery address is not in geographic area";

						}						
						else
						{


							#check if lat/long is set for agent			
							if ( $objAgent->latitude == "0.0000000" || $objAgent->longitude == "0.0000000"  )
							{
				
								$objGeocodeInput = new stdClass();
								$objGeocodeInput->zipcode = $objOrder->care_of_pc;
								
								$objGeocode = Nsd_schedulingController::getLatLng( $objGeocodeInput );
				
				
								$objDataUpdate = new stdClass();
								$objDataUpdate->id = $objAgent->id;
								$objDataUpdate->zipcode = $objGeocodeInput->zipcode;
								$objDataUpdate->latitude = $objGeocode->latitude;
								$objDataUpdate->longitude = $objGeocode->longitude;
								$result = JFactory::getDbo()->updateObject('htc_os_agents', $objDataUpdate, 'id');         
				
								
								$objAgent->latitude = $objGeocode->latitude;
								$objAgent->longitude = $objGeocode->longitude;
								
							}

							
							#calculate distance from agent to destination zipcode					
							$objDestination = new stdClass();
							$objDestination->zipcode 	= $objOrder->destination_zipcode;
							$objDestinationReturn = Nsd_schedulingController::getLatLng( $objDestination );
							$objOrder->destination_latitude = $objDestinationReturn->latitude;
							$objOrder->destination_longitude = $objDestinationReturn->longitude;
							
					
							$distance = MetalakeHelperCore::distance($lat1 = $objAgent->latitude, $lon1 = $objAgent->longitude, $lat2 = $objDestinationReturn->latitude, $lon2 = $objDestinationReturn->longitude, $unit="M");		
							
							$objReturn->order->distance	= $distance;	
							
							$objReturn->order->destination_in_range = ( $objOrder->distance  < $objAgent->range_miles ) ? "TRUE" : "FALSE" ;
	
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r objOrder->distance: ".print_r($objOrder->distance, true);
							$logMessage .= "\n\r objAgent->range_miles: ".print_r($objAgent->range_miles, true);
							$logMessage .= "\n\r objReturn->order->destination_in_range: ".print_r($objReturn->order->destination_in_range, true);
							
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
				
				
							if ( $objReturn->order->destination_in_range == "FALSE"  )
							{

								$objError = new stdClass();
								$objError->error_code = "54";
								$objError->customer_account_code = $objOrder->customer_account_code;
								$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );
		
								$objReturn->error = "1";
								$objReturn->error_code = "54";
								$objReturn->error_message = $objError->error_message_revised;
								#$objReturn->error_message = "Online scheduling is not available in your area. To schedule your delivery, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
								$objReturn->system_error_message = "Order delivery address is not in service range (distance)";
								
							}
							else
							{
	
	
								#get intervals
								#$query = "select a.* from htc_os_delivery_intervals a where a.agent_id = '".$objAgent->id."' and a.client_id = '".$objClient->id."' ";
								$query = "select a.* from htc_os_delivery_intervals a where a.agent_id = '".$objAgent->id."' ";
						        $db->setQuery($query);
	
	
						        $objInterval = $db->loadObject() ;
								


								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r query: ".print_r($query, true);
								$logMessage .= "\n\r objInterval: ".print_r($objInterval, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	
								$objReturn->interval = $objInterval;
	
					
								$dateToday = date('Y-m-d'); //today date
					
								$tt = $dateToday." ".$objInterval->delivery_interval_cutoff;
					
								if (new DateTime() > new DateTime($tt)) 
								{
									$logMessage = "INSIDE | ".$className." | ".$functionName. " | time";
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
					
					
									$objInterval->delivery_interval_begin += 1;
									$objInterval->delivery_interval_end += 1;
					
								}
								
								$date = $objOrder->docked_date;
								
								
								$numofDays = $objInterval->delivery_interval_end - $objInterval->delivery_interval_begin + 1;
								$lastNumDays = $objInterval->delivery_interval_begin + $numofDays - 1;

								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r numofDays: ".print_r($numofDays, true);
								$logMessage .= "\n\r lastNumDays: ".print_r($lastNumDays, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								

								#$dateCompare = date(  'Y-m-d', strtotime("+$objInterval->delivery_interval_begin day", strtotime($dateToday))  );
								$dateCompare = date(  'Y-m-d', strtotime("+$objInterval->delivery_interval_begin day", strtotime($date))  );


								$logMessage = "INSIDE | ".$className." | ".$functionName;


								$logMessage .= "\n\r date: ".print_r($date, true);
								$logMessage .= "\n\r dateCompare: ".print_r($dateCompare, true);
								$logMessage .= "\n\r objInterval->delivery_interval_begin: ".print_r($objInterval->delivery_interval_begin, true);
								$logMessage .= "\n\r objInterval->delivery_interval_end: ".print_r($objInterval->delivery_interval_end, true);
									$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

								
								
								if ( $date >= $dateCompare )
								{
									
									$objInterval->delivery_interval_end = $objInterval->delivery_interval_end - $objInterval->delivery_interval_begin;
									$objInterval->delivery_interval_begin = 0;
								}
								

								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r objInterval->delivery_interval_begin: ".print_r($objInterval->delivery_interval_begin, true);
								$logMessage .= "\n\r objInterval->delivery_interval_end: ".print_r($objInterval->delivery_interval_end, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

								
								
								$dib = $objInterval->delivery_interval_begin;
								
								$test_dow = date('w', strtotime("+$dib day", strtotime($date) ) );

								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r date: ".print_r($date, true);
								$logMessage .= "\n\r dib: ".print_r($dib, true);
								$logMessage .= "\n\r test_dow: ".print_r($test_dow, true);
								$logMessage .= "\n\r objInterval->delivery_interval_begin: ".print_r($objInterval->delivery_interval_begin, true);
								$logMessage .= "\n\r objInterval->delivery_interval_end: ".print_r($objInterval->delivery_interval_end, true);
								$logMessage .= "\n\r test_dow: ".print_r($test_dow, true);
								
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

								
								switch( $test_dow )
								{

									case "0":  //if prospective delivery date is Sunday; push first date to Wednesday
										$objInterval->delivery_interval_begin = $objInterval->delivery_interval_begin + 2;
										$lastNumDays = $objInterval->delivery_interval_begin + $numofDays - 1;
										break;

									case "1":  //if prospective delivery date is Thursday; push first date to Monday
										$objInterval->delivery_interval_begin = $objInterval->delivery_interval_begin + 2;
										$lastNumDays = $objInterval->delivery_interval_begin + $numofDays - 1;
										break;

									case "2":  //if prospective delivery date is Friday; push first date to Tuesday
										$objInterval->delivery_interval_begin = $objInterval->delivery_interval_begin + 1;
										$lastNumDays = $objInterval->delivery_interval_begin + $numofDays - 1;
										break;

									case "6":  //if prospective delivery date is Saturday; push first date to Monday
										$objInterval->delivery_interval_begin = $objInterval->delivery_interval_begin + 2;
										$lastNumDays = $objInterval->delivery_interval_begin + $numofDays - 1;
										break;

									default:
										break;
									
								}
								

								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r objInterval->delivery_interval_begin: ".print_r($objInterval->delivery_interval_begin, true);
								$logMessage .= "\n\r objInterval->delivery_interval_end: ".print_r($objInterval->delivery_interval_end, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }								
								

								$logMessage = "INSIDE | ".$className." | ".$functionName;
								$logMessage .= "\n\r numofDays: ".print_r($numofDays, true);
								$logMessage .= "\n\r lastNumDays: ".print_r($lastNumDays, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

								
								$arrDays = array();
								
								
								for ($i = $objInterval->delivery_interval_begin;  $i <= $lastNumDays;  $i++)
								{
									$objDay = new stdClass();
									$objDay->date = date('Y-m-d', strtotime("+$i day", strtotime($date)));
									$objDay->dayNumOfWeek = date('w', strtotime("+$i day", strtotime($date)));
									$objDay->dayLabelOfWeek = date('D', strtotime("+$i day", strtotime($date)));
									$objDay->dayLabelOfWeekFull = date('l', strtotime("+$i day", strtotime($date)));
					
									
									#get array list of holidays for agent									
									$query = "select a.holiday_date from htc_os_lu_holidays a where a.agent_id = '".$objAgent->id."' ";
								    $db->setQuery($query);
								    $arrHolidays = $db->loadColumn();
	
					
									if ( in_array( $objDay->date, $arrHolidays )  ) #is holiday  array of holidays for A/C
									{
										
										$lastNumDays += 1;
									}
									else
									{
					
										#$query = "select a.* from htc_os_delivery_windows a where a.agent_id = '".$objAgent->id."' and a.client_id = '".$objClient->id."' and day_of_week = '".$objDay->dayNumOfWeek."' and a.state='1' order by ordering ";
										$query = "select a.* from htc_os_delivery_windows a where a.agent_id = '".$objAgent->id."' and geographical_area = '".$objGeographicalPostalCode->geographical_area."' and day_of_week = '".$objDay->dayNumOfWeek."' and a.state='1' order by ordering ";
										
								        $db->setQuery($query);
								        $objListWindows = $db->loadObjectList() ;


												$logMessage = "INSIDE | ".$className." | ".$functionName;
												$logMessage .= "\n\r query: ".print_r($query, true);
												$logMessage .= "\n\r objListWindows: ".print_r($objListWindows, true);
												$logMessage .= "\n\r ";
												#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

						
										if ( count($objListWindows) == 0 )
										{
											$lastNumDays += 1;
										}
										else
										{
					
											$objDay->firstWindowID = "";
											foreach( $objListWindows as $window )
											{
												$window->request_date_start = $objDay->date." ".$window->window_start_time;
												$window->request_date_end = $objDay->date." ".$window->window_end_time;
												$window->uid = $objDay->date.".".$window->id;
												
												
												$window->APIdate = date('Y-m-d', strtotime("+$i day", strtotime($date)));

												$wst = strtotime($window->window_start_time);
												$window->APIstartTime = date("H:i", $wst);

												$wet = strtotime($window->window_end_time);
												$window->APIendTime = date("H:i", $wet);
												


												#$query = "select a.* from htc_os_deliveries a where a.agent_id = '".$objAgent->id."' and a.client_id = '".$objClient->id."' and request_time_window_start = '".$window->request_date_start."'  and request_time_window_end = '".$window->request_date_end."' and a.record_type='update' ";
												
												$query = "select a.* from htc_os_deliveries a where a.agent_id = '".$objAgent->id."' and request_time_window_start = '".$window->request_date_start."'  and request_time_window_end = '".$window->request_date_end."' and a.record_type='update' and a.error = '0' ";
												
										        $db->setQuery($query);
										        $objListDeliveries = $db->loadObjectList() ;


												$logMessage = "INSIDE | ".$className." | ".$functionName;
												$logMessage .= "\n\r query: ".print_r($query, true);
												$logMessage .= "\n\r objListDeliveries: ".print_r($objListDeliveries, true);
												$logMessage .= "\n\r ";
												if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

												$query = "select a.* from htc_os_lu_windows a where a.agent_id = '".$objAgent->id."' and geographical_area = '".$objGeographicalPostalCode->geographical_area."' and day_of_week_php = '".$objDay->dayNumOfWeek."' ";
												
										        $db->setQuery($query);
										        $objLUWindows = $db->loadObject() ;

												$logMessage = "INSIDE | ".$className." | ".$functionName;
												$logMessage .= "\n\r query: ".print_r($query, true);
												$logMessage .= "\n\r objLUWindows: ".print_r($objLUWindows, true);												
												$logMessage .= "\n\r objLUWindows->max_slots: ".print_r($objLUWindows->max_slots, true);
												
												$logMessage .= "\n\r count(objListDeliveries): ".print_r(count($objListDeliveries), true);
												$logMessage .= "\n\r ";
												if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

												$window->APIslotsAvailable = 0; 

												if ( count($objListDeliveries) >=  $objLUWindows->max_slots ) //$window->max_deliveries
												{
													$window->enabled = "0";
													$window->APIslotsAvailable = 0; 

												}
												else
												{
													$window->enabled = "1";
													if ($objDay->firstWindowID == "") {
														$objDay->firstWindowID = $window->uid;
													}

													$window->APIslotsAvailable = ( $objLUWindows->max_slots - count($objListDeliveries) );
												}


											}
							
							
											$objDay->windows = $objListWindows;
											
											#$objDay->firstWindowID = $objDay->windows[0]->uid;
											
											$arrDays[] = $objDay;
					
											
										}
						
										
									}
					
									
								}
					
								$objReturn->days = $arrDays;
												
								
								
							}
						
						}
								
							
							
						
						
					}
	
				}


				
				
			}



		}
		else
		{
			
			switch( $objTruckmate->error_code )
			{
				case "31":
				case "32":
				
					
					$objError = new stdClass();
					$objError->error_code = $objTruckmate->error_code;
					$objError->customer_account_code = $objOrder->customer_account_code;
					$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );
				
				
					$objReturn->error = "99";
					$objReturn->error_code = $objTruckmate->error_code;
					$objReturn->error_message = $objError->error_message_revised;

					#$objReturn->error_message = "The delivery order number provided cannot be found. Please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
					
					$objReturn->system_error_message = $objTruckmate->error_message;
					break;

				case "33":
				case "01":
				case "02":
				case "03":
				case "04":
				case "05":
				case "06":
				case "07":

					$objError = new stdClass();
					$objError->error_code = $objTruckmate->error_code;
					$objError->customer_account_code = $objOrder->customer_account_code;
					$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );

					$objReturn->error = "99";
					$objReturn->error_code = $objTruckmate->error_code;
					$objReturn->error_message = $objError->error_message_revised;
					
					
					#$objReturn->error_message = "We are currently unable to schedule your delivery. For further assistance, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
					$objReturn->system_error_message = $objTruckmate->error_message;
					break;
				
				default:

					$objError = new stdClass();
					$objError->error_code = $objTruckmate->error_code;
					$objError->customer_account_code = $objOrder->customer_account_code;
					$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );				
				
				
					$objReturn->error = "99";				
					$objReturn->error_code = $objTruckmate->error_code;
					$objReturn->error_message = $objError->error_message_revised;
					#$objReturn->error_message = "We are currently unable to schedule your delivery. If the problem continues, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
					
					$objReturn->system_error_message = $objTruckmate->error_message;
					break;
				
			}
		}


		#$objInput->component_origin = "com_nsd_scheduling";

		if ( $objInput->component_origin == "com_nsd_scheduling" &&  $objReturn->error == "99" )
		{

			$objDataInsert = new stdClass();
			$objDataInsert->system = "WEB";
			$objDataInsert->agent_id = $objReturn->agent->id;
			$objDataInsert->client_id = $objOrder->customer_account_code;
			$objDataInsert->order_id = $objInput->trace_number; //$objOrder->trace_number;
			$objDataInsert->detail_line_id = $objOrder->detail_line_id;
			$objDataInsert->delivery_zipcode = $objOrder->destination_zipcode;
			$objDataInsert->delivery_latitude = $objOrder->destination_latitude;
			$objDataInsert->delivery_longitude = $objOrder->destination_longitude;
			$objDataInsert->delivery_distance = $objOrder->distance;
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
			$objDataInsert->docked_date = $objOrder->docked_date;		
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;	
			$objDataInsert->system_error_message = $objReturn->system_error_message;
			$objDataInsert->record_type = "display";					
			$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDataInsert);
			$deliveryID = $db->insertid();

		}


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$logMessage = $functionName." | trace: ".$objOrder->trace_number." | dlid: ".$objOrder->detail_line_id." | error: ".$objReturn->error." | error code: ".$objReturn->error_code." | error code: ".$objReturn->system_error_message;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
		
	}	




	public function updateOrderData( )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=updateOrderData



		$className = "Nsd_schedulingController";
		$functionName = "updateOrderData";


        $app           		= JFactory::getApplication();
        $params         	= $app->getParams();
		$itemid_complete	= $params->get('itemid_complete');

		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$postData = JRequest::get('post');

		$logMessage = "INSIDE | Ml_activitiesController | updateActivity"."\n\r";
		$logMessage .= "postData: ".print_r($postData, true)."\n\r";
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }


		$objForm = new stdClass();
		foreach($postData as $key => $value)
		{		
			
			if ( is_string($value) )
			{
				$objForm->$key = trim($value);
			}
		}


		$arrRequestTimes = explode( "|", $objForm->optionsWindows );

		$objDisplaySess = MetalakeHelperCore::getSessObj( "objDisplaySess" );



		$objUpdate = new stdClass();
		$objUpdate->error = "0";
		$objUpdate->error_code = "";
		$objUpdate->error_message = "";
		$objUpdate->trace_number = $objDisplaySess->order->trace_number;
		$objUpdate->detail_line_id = $objDisplaySess->order->detail_line_id;
		$objUpdate->requested_time_start = $arrRequestTimes[0];
		$objUpdate->requested_time_end = $arrRequestTimes[1];
		$objUpdate->window_id = $arrRequestTimes[2];
		$objUpdate->error_code_test = $objDisplaySess->error_code_test;

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r postData: ".print_r($postData, true);
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r objUpdate: ".print_r($objUpdate, true);
		$logMessage .= "\n\r arrRequestTimes: ".print_r($arrRequestTimes, true);
		$logMessage .= "\n\r itemid_complete: ".print_r($itemid_complete, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		


		$objDisplay = MetalakeHelperCore::getSessObj( "objDisplaySess" );



		# UNCOMMENT FOR SYSTEM TESTING & PRODUCTION
		$objUpdateFromTruckmate = Nsd_truckmateController::updateTruckmateData( $objUpdate );

			switch( $objUpdate->error_code_test )
			{
				case "91":
				case "92":				
					$objUpdateFromTruckmate->error_code = $objUpdate->error_code_test;
					break;
				
				default:
				
					break;
				
			}



			switch( $objUpdateFromTruckmate->error_code )
			{
				case "91":
				case "92":
				
					$objError = new stdClass();
					$objError->error_code = $objUpdateFromTruckmate->error_code;
					$objError->customer_account_code = $objOrder->customer_account_code;
					$objError = Nsd_schedulingController::evaluateErrorMessage( $objError );
				
					$objUpdateFromTruckmate->error = "1";
					$objUpdateFromTruckmate->error_code = $objUpdateFromTruckmate->error_code;
					$objUpdateFromTruckmate->error_message = $objError->error_message_revised;
					#$objUpdateFromTruckmate->error_message = "We are currently unable to schedule your delivery. For further assistance, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
					break;

				case "":
					$objUpdateFromTruckmate->error = "0";
					$objUpdateFromTruckmate->error_code = "";
					$objUpdateFromTruckmate->error_message = "";
					break;
				
				default:
					$objUpdateFromTruckmate->error = "1";
					$objUpdateFromTruckmate->error_code = $objUpdateFromTruckmate->error_code;
					$objUpdateFromTruckmate->error_message = "We are currently unable to schedule your delivery. If the problem continues, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
					break;
				
			}
			
			#$objDisplay->update = $objUpdateFromTruckmate;

		
		$objDisplay->update = $objUpdate;


		MetalakeHelperCore::setSessObj( "objDisplaySess", $objDisplay );



		 
		if ( $objUpdateFromTruckmate->error == "0" )
		{



			$objDeliveries = new stdClass();
			$objDeliveries->system = "WEB";
			$objDeliveries->agent_id = $objDisplay->agent->id;
			$objDeliveries->client_id = $objDisplay->order->customer_account_code;
			$objDeliveries->order_id = $objDisplay->order->trace_number;
			$objDeliveries->detail_line_id = $objUpdate->detail_line_id;
			$objDeliveries->delivery_zipcode = $objDisplay->order->destination_zipcode;
			$objDeliveries->delivery_latitude = $objDisplay->order->destination_latitude;
			$objDeliveries->delivery_longitude = $objDisplay->order->destination_longitude;
			$objDeliveries->delivery_distance = $objDisplay->order->distance;
			$objDeliveries->delivery_date = $objUpdate->requested_time_start;
			$objDeliveries->delivery_window_id = $objDisplay->update->window_id;
			$objDeliveries->delivery_type_id = "1";
			$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
			$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
			$objDeliveries->docked_date = $objDisplay->order->docked_date;
			$objDeliveries->record_type = "update";	
			$objDeliveries->request_delivery_date = $objUpdate->requested_time_start;	
			$objDeliveries->request_time_window_start = $objUpdate->requested_time_start;	
			$objDeliveries->request_time_window_end = $objUpdate->requested_time_end;		
			
			$objDeliveries->error = "0";
			$objDeliveries->error_code = "";
			$objDeliveries->system_error_message = "";
			
			$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
			$deliveryID = $db->insertid();
	
			$objAgentSchedule = MetalakeHelperCore::getObjTableData( $table="htc_os_agents", $field="id", $value=$objDisplay->agent->id );
	
			$objAgentDispatchtrack = MetalakeHelperCore::getObjTableData( $table="htc_dispatchtrack_agents", $field="agent_code", $value=$objAgentSchedule->name );
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName." | udpate process table";
			$logMessage .= "\n\r objAgentSchedule: ".print_r($objAgentSchedule, true);
			$logMessage .= "\n\r objAgentDispatchtrack: ".print_r($objAgentDispatchtrack, true);
			$logMessage .= "\n\r objUpdateFromTruckmate: ".print_r($objUpdateFromTruckmate, true);
			$logMessage .= "\n\r objDeliveries: ".print_r($objDeliveries, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }





			$update_system = ( count($objAgentDispatchtrack) > 0  && $objAgentDispatchtrack->state == '1' ) ?  "dispatchtrack" : "" ;


			$email_body = "";

			$email_body .= $objDisplay->order->care_of." client with order number ".$objDisplay->order->trace_number." has scheduled their delivery online for the day of ".date('n/d', strtotime($objDisplay->update->requested_time_start))." between ".date('ga', strtotime($objDisplay->update->requested_time_start))." and ".date('ga', strtotime($objDisplay->update->requested_time_end)).". Please call the customer the day prior to setup their window.";

			$email_body .= "<br><br>Sincerely, NSD.";
			
			$email_body .= "<br><br>Please reach out to your assigned LMC if you should have any questions.";


			$email_from = ( $objDisplay->client->email_from != "" ) ? $objDisplay->client->email_from : "shipping@shipnsd.com"; 


			$objNotify = new stdClass();
			$objNotify->email_to = $objDisplay->order->care_of_email;
			#$objNotify->email_to = "rfeldman@metalake.com";
			$objNotify->email_to = "lsawyer@metalake.com";
			$objNotify->email_from = $email_from;
			$objNotify->email_subject = "Order number ".$objDisplay->order->trace_number." has been scheduled online for ".date('n/d ga', strtotime($objDisplay->update->requested_time_start))."-".date('ga', strtotime($objDisplay->update->requested_time_end))." – Please call to confirm exact window";
			$objNotify->email_body = $email_body;

			$objNotify->email_category = "agent notify";
			
			$jsonObjNotify = json_encode( $objNotify );


			$objScheduleInfo = new stdClass();
			$objScheduleInfo->agent = $objDisplay->order->care_of;
			$objScheduleInfo->order_id = $objDisplay->order->trace_number;
			$objScheduleInfo->scheduled_delivery_date = date('n/d', strtotime($objDisplay->update->requested_time_start));
			$objScheduleInfo->scheduled_delivery_start_time = date('ga', strtotime($objDisplay->update->requested_time_start));
			$objScheduleInfo->scheduled_delivery_end_time = date('ga', strtotime($objDisplay->update->requested_time_end));



			$jsonObjScheduleInfo = json_encode( $objScheduleInfo );





			$objInsertProccessUpdate = new stdClass();
			$objInsertProccessUpdate->order_id = $objDisplay->order->trace_number;
			$objInsertProccessUpdate->process_datetime = "";
			$objInsertProccessUpdate->process_datetime_gmt = "";		
			$objInsertProccessUpdate->stamp_datetime = date("Y-m-d H:i:s");
			$objInsertProccessUpdate->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");	
			$objInsertProccessUpdate->update_system = $update_system;		
			$objInsertProccessUpdate->notify = $jsonObjNotify;		
			$objInsertProccessUpdate->agent_code = $objDisplay->order->care_of;
			$objInsertProccessUpdate->order_schedule_info = $jsonObjScheduleInfo;
			
			$result = JFactory::getDbo()->insertObject('htc_process_update', $objInsertProccessUpdate);
			$InsertIDProcessID = $db->insertid();
			
		}
		else
		{
			
			# update failed because of truckmate 
			
			$objDeliveries = new stdClass();
			$objDeliveries->system = "WEB";
			$objDeliveries->agent_id = $objDisplay->agent->id;
			$objDeliveries->client_id = $objDisplay->client->id;
			$objDeliveries->order_id = $objDisplay->order->trace_number;
			$objDeliveries->detail_line_id = $objUpdate->detail_line_id;
			$objDeliveries->delivery_zipcode = $objDisplay->order->destination_zipcode;
			$objDeliveries->delivery_latitude = $objDisplay->order->destination_latitude;
			$objDeliveries->delivery_longitude = $objDisplay->order->destination_longitude;
			$objDeliveries->delivery_distance = $objDisplay->order->distance;
			$objDeliveries->delivery_date = $objUpdate->requested_time_start;
			$objDeliveries->delivery_window_id = $objDisplay->update->window_id;
			$objDeliveries->delivery_type_id = "1";
			$objDeliveries->stamp_datetime = date("Y-m-d H:i:s");
			$objDeliveries->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");		
			$objDeliveries->docked_date = $objDisplay->order->docked_date;
			$objDeliveries->record_type = "update";	
			$objDeliveries->request_delivery_date = $objUpdate->requested_time_start;	
			$objDeliveries->request_time_window_start = $objUpdate->requested_time_start;	
			$objDeliveries->request_time_window_end = $objUpdate->requested_time_end;		
			
			$objDeliveries->error = $objUpdateFromTruckmate->error;
			$objDeliveries->error_code = $objUpdateFromTruckmate->error_code;
			$objDeliveries->system_error_message = $objUpdateFromTruckmate->system_error_message;
			
			$result = JFactory::getDbo()->insertObject('htc_os_deliveries', $objDeliveries);
			$deliveryID = $db->insertid();			


			$logMessage = "INSIDE | ".$className." | ".$functionName." | update process table";
			$logMessage .= "\n\r objDeliveries: ".print_r($objDeliveries, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
		}







		$logMessage = $functionName." | trace: ".$objDisplay->order->trace_number." | dlid: ".$objUpdate->detail_line_id." | delivery_date: ".$objDisplay->update->requested_time_start." | error: ".$objUpdate->error." | error code: ".$objUpdate->error_code;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#$link = JRoute::_('index.php?option=com_nsd_scheduling&view=complete&Itemid='.$itemid_complete);		
		$link = JRoute::_('index.php?option=com_nsd_scheduling&view=complete');		

		$this->setRedirect($link, $msg);

		
	}	

	
// !COMMON FUNCTIONS

	public static function sendAlert( $objAlert )
	{
        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
		$receive_alerts	= $params->get('send_alerts');

		if ( $receive_alerts == "1" )
		{

			$mailer =& JFactory::getMailer();
			$config = JFactory::getConfig();
	
			$sender = array( 
			    $config->get( 'mailfrom' ),
			    $config->get( 'fromname' ) 
			);
			$mailer->setSender($sender);
	
			$mailer->addRecipient( $objAlert->recipients );
	
			$mailer->setSubject( $objAlert->subject );
			$mailer->setBody( $objAlert->body );
			$mailer->isHTML(true);
	
			if ($mailer->Send() !== true)
			{
				$return = 0;			
			}			
			else
			{
				$return = 1;			
			}

		}	
		else
		{
			$return = 0;
		}	

		return $return;
	}


	public static function arrZipLookup( )
	{
	
		$arrZipcodes = array();
	
		$file = fopen('ziplatlng.txt', 'r');
		while (($line = fgetcsv($file)) !== FALSE) 
		{
		   $arrZipcodes[] = $line;	
		}
		fclose($file);	
		
		return $arrZipcodes;
		
	}

	public static function findMyValue( $array, $search_key, $search_str, $key ) 
	{
	    foreach ($array as $v) {
	        if ($v[$search_key] == $search_str) {
	            return $v[$key];
	        }
	    }
	    return 'NOT_FOUND';
	}


	public static function observed_date($holiday){
	    $day = date("w", strtotime($holiday));
	    if($day == 6) {
	        $observed_date = $holiday -1;
	    } elseif ($day == 0) {
	        $observed_date = $holiday +1;
	    } else {
	        $observed_date = $holiday;
	    }
	    
	    return $formattedDate = date("Y-m-d", strtotime($observed_date));


	}



	public static function getLatLng( $objInput )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=getLatLng



		$className = "Nsd_schedulingController";
		$functionName = "getLatLng";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrZipcodes = Nsd_schedulingController::arrZipLookup( );
		
		
		$objReturn = new stdClass();

		$objReturn->zipcode 	= $objInput->zipcode;
		$objReturn->latitude 	= Nsd_schedulingController::findMyValue( $arrZipcodes, 0, $objInput->zipcode, 1 );
		$objReturn->longitude 	= Nsd_schedulingController::findMyValue( $arrZipcodes, 0, $objInput->zipcode, 2 );		
		$objReturn->latlng		= $objReturn->latitude.",".$objReturn->longitude;

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
		
	}


	public static function validateAgentSetupComplete( $objInput )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=validateAgentSetupComplete

		#used to validate agent setup in getOrderData()

		$className = "Nsd_schedulingController";
		$functionName = "validateAgentSetupComplete";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objAgentComplete = new stdClass();
		$objAgentComplete->flag_complete = 1;


		# is active in agent table
		
        $query = "select a.* from htc_os_agents a where a.name = '".$objInput->agent_code."' and a.state = '1' ";

        $db->setQuery($query);
        
        $objAgent = $db->loadObject();

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

        
        if ( !$objAgent || count($objAgent) == 0 )
        {
			$objAgentComplete->flag_complete = 0;
			$objAgentComplete->error_code = "1";
			$objAgentComplete->error_message = "Agent not complete in htc_os_agents.";
	        
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	        
        }
		else
		{
			# has a record in htc_os_clients table

	        $query = "select a.* from htc_os_clients a where a.agent_id = '".$objAgent->id."' and a.state = '1' ";
	
	        $db->setQuery($query);
	        
	        $objClients = $db->loadObject();
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objClients: ".print_r($objClients, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			
	        if ( !$objClients || count($objClients) == 0 )
	        {

				$objAgentComplete->flag_complete = 0;
				$objAgentComplete->error_code = "2";
				$objAgentComplete->error_message = "Agent not complete in htc_os_clients.";

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




	        }
			else
			{

				# has a record in  htc_os_delivery_intervals table
		        $query = "select a.* from htc_os_delivery_intervals a where a.agent_id = '".$objAgent->id."' ";
		
		        $db->setQuery($query);
		        
		        $objIntervals = $db->loadObject();
		
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objIntervals: ".print_r($objIntervals, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			

		        if ( !$objIntervals || count($objIntervals) == 0 )
		        {
					$objAgentComplete->flag_complete = 0;
					$objAgentComplete->error_code = "3";
					$objAgentComplete->error_message = "Agent not complete in htc_os_delivery_intervals.";
			        
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			        
		        }
				else
				{
	
					# has a record in  htc_os_delivery_windows table
			        $query = "select a.* from htc_os_delivery_windows a where a.agent_id = '".$objAgent->id."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objWindows = $db->loadObject();
			
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objWindows: ".print_r($objWindows, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
				
			        if ( !$objWindows || count($objWindows) == 0 )
			        {
						$objAgentComplete->flag_complete = 0;
						$objAgentComplete->error_code = "4";
						$objAgentComplete->error_message = "Agent not complete in htc_os_delivery_windows.";
				        
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				        
			        }
					else
					{
						$objAgentComplete->flag_complete = 1;
						$objAgentComplete->error_code = "0";
						$objAgentComplete->error_message = "";
					}
				
				}
			
			}
			
		}        
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		return $objAgentComplete;
		
	}	


	public static function fullValidateSetupComplete( $objOrder )  //depricated
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=fullValidateSetupComplete

		# used in setupAgentClientIntervalWindows();

		$className = "Nsd_schedulingController";
		$functionName = "fullValidateSetupComplete";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objAgentComplete = new stdClass();
		$objAgentComplete->flag_complete = 1;


		# is active in agent table
		
        $query = "select a.* from htc_os_agents a where a.name = '".$objOrder->care_of."' and a.state = '1' ";

        $db->setQuery($query);
        
        $objAgent = $db->loadObject();

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

        
        if ( !$objAgent || count($objAgent) == 0 )
        {
			$objAgentComplete->flag_complete = 0;
			$objAgentComplete->error_code = "1";
			$objAgentComplete->error_message = "Agent not complete in htc_os_agents.";
	        
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	        
        }
		else
		{
			# has a record in htc_os_clients table
			
	        $query = "select a.* from htc_os_lu_clients a where a.customer_account_code = '".$objOrder->customer_account_code."' and a.state = '1' ";
	
	        $db->setQuery($query);
	        
	        $objClientsLU = $db->loadObject();			
			
			

	        $query = "select a.* from htc_os_clients a where a.agent_id = '".$objAgent->id."' and client_id = '".$objClientsLU->id."' and a.state = '1' ";
	
	        $db->setQuery($query);
	        
	        $objClients = $db->loadObject();
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objClients: ".print_r($objClients, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			
	        if ( !$objClients || count($objClients) == 0 )
	        {

				$objAgentComplete->flag_complete = 0;
				$objAgentComplete->error_code = "2";
				$objAgentComplete->error_message = "Agent not complete in htc_os_clients.";

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




	        }
			else
			{

		        $query = "select a.* from htc_os_lu_clients a where a.customer_account_code = '".$objOrder->customer_account_code."' and a.state = '1' ";
		
		        $db->setQuery($query);
		        
		        $objClientsLU = $db->loadObject();	


				# has a record in  htc_os_delivery_intervals table
		        $query = "select a.* from htc_os_delivery_intervals a where a.agent_id = '".$objAgent->id."' and client_id = '".$objClientsLU->id."' ";
		
		        $db->setQuery($query);
		        
		        $objIntervals = $db->loadObject();
		
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objIntervals: ".print_r($objIntervals, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			

		        if ( !$objIntervals || count($objIntervals) == 0 )
		        {
					$objAgentComplete->flag_complete = 0;
					$objAgentComplete->error_code = "3";
					$objAgentComplete->error_message = "Agent not complete in htc_os_delivery_intervals.";
			        
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			        
		        }
				else
				{
	
			        $query = "select a.* from htc_os_lu_clients a where a.customer_account_code = '".$objOrder->customer_account_code."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objClientsLU = $db->loadObject();		
	
					# has a record in  htc_os_delivery_windows table
			        $query = "select a.* from htc_os_delivery_windows a where a.agent_id = '".$objAgent->id."' and client_id = '".$objClientsLU->id."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objWindows = $db->loadObject();
			
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objWindows: ".print_r($objWindows, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
				
			        if ( !$objWindows || count($objWindows) == 0 )
			        {
						$objAgentComplete->flag_complete = 0;
						$objAgentComplete->error_code = "4";
						$objAgentComplete->error_message = "Agent not complete in htc_os_delivery_windows.";
				        
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				        
			        }
					else
					{
						$objAgentComplete->flag_complete = 1;
						$objAgentComplete->error_code = "0";
						$objAgentComplete->error_message = "";
					}
				
				}
			
			}
			
		}        
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		return $objAgentComplete;
		
	}	

	public static function setupAgentClientIntervalWindows( $objOrder )  //depricated
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=setupAgentClientIntervalWindows



		$className = "Nsd_schedulingController";
		$functionName = "setupAgentClientIntervalWindows";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	


        $app           							= JFactory::getApplication();
        $params         						= $app->getParams();
		$default_agent_range_miles				= $params->get('default_agent_range_miles');
		$default_agent_max_deliveries_per_day	= $params->get('default_agent_max_deliveries_per_day');

		$default_client_max_deliveries_per_day	= $params->get('default_client_max_deliveries_per_day');
		
		$default_intervals_delivery_interval_begin		= $params->get('default_intervals_delivery_interval_begin');
		$default_intervals_delivery_interval_end		= $params->get('default_intervals_delivery_interval_end');
		$default_intervals_delivery_interval_cutoff		= $params->get('default_intervals_delivery_interval_cutoff');

		$default_windows_by_client	= $params->get('default_windows_by_client');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$logMessage = "INSIDE | ".$className." | ".$functionName." | object ";
		$logMessage .= "\n\r objOrder: ".print_r($objOrder, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	

		$objSetupACIW = new stdClass();



		#check if agent complete
		$objAgentComplete = Nsd_schedulingController::fullValidateSetupComplete( $objOrder );
		
		
		if ( $objAgentComplete->flag_complete == "0" )
		{
			

			switch( $objAgentComplete->error_code )
			{
				
				case "1":
				
					#missing agent
					
					#insert into agent table
					
					$objGeocodeInput = new stdClass();
					$objGeocodeInput->zipcode = $objOrder->care_of_pc;
					
					$objGeocode = Nsd_schedulingController::getLatLng( $objGeocodeInput );
					
					
					$objDataInsert = new stdClass();
					$objDataInsert->name = $objOrder->care_of;
					$objDataInsert->zipcode = $objOrder->care_of_pc;
					$objDataInsert->latitude = $objGeocode->latitude;
					$objDataInsert->longitude = $objGeocode->longitude;
					$objDataInsert->state = "1";
					$objDataInsert->range_miles = $default_agent_range_miles;
					$objDataInsert->max_deliveries_per_day = $default_agent_max_deliveries_per_day;
					$result = JFactory::getDbo()->insertObject('htc_os_agents', $objDataInsert);
					$objOrder->agent_id = $db->insertid();	

					$logMessage = "INSIDE | ".$className." | ".$functionName." | AGENT ";
					$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
					Nsd_schedulingController::setupAgentClientIntervalWindows( $objOrder );
					
					break;


					
				case "2":
					#missing client

			        $query = "select a.* from htc_os_agents a where a.name = '".$objOrder->care_of."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objAgentLU = $db->loadObject();



			        $query = "select a.* from htc_os_lu_clients a where a.customer_account_code = '".$objOrder->customer_account_code."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objClientsLU = $db->loadObject();

					if ( is_object($objClientsLU) )
					{

						$objDataInsert = new stdClass();
						$objDataInsert->client_id = $objClientsLU->id;
						$objDataInsert->agent_id = $objAgentLU->id;
						$objDataInsert->max_deliveries_per_day = $default_client_max_deliveries_per_day;
						$objDataInsert->state = "1";
						$result = JFactory::getDbo()->insertObject('htc_os_clients', $objDataInsert);
						$objOrder->client_id = $db->insertid();	
						
						$logMessage = "INSIDE | ".$className." | ".$functionName." | CLIENT ";
						$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
					
						Nsd_schedulingController::setupAgentClientIntervalWindows( $objOrder );
						
					}
				
				
					break;

				case "3":
				
					#missing interval

			        $query = "select a.* from htc_os_agents a where a.name = '".$objOrder->care_of."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objAgentLU = $db->loadObject();



			        $query = "select a.* from htc_os_lu_clients a where a.customer_account_code = '".$objOrder->customer_account_code."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objClientsLU = $db->loadObject();

				
					$objDataInsert = new stdClass();
					$objDataInsert->client_id = $objClientsLU->id;
					$objDataInsert->agent_id = $objAgentLU->id;
					$objDataInsert->delivery_interval_begin = $default_intervals_delivery_interval_begin;
					$objDataInsert->delivery_interval_end = $default_intervals_delivery_interval_end;
					$objDataInsert->delivery_interval_cutoff = $default_intervals_delivery_interval_cutoff;
					$result = JFactory::getDbo()->insertObject('htc_os_delivery_intervals', $objDataInsert);
					$intervalID = $db->insertid();	

					$logMessage = "INSIDE | ".$className." | ".$functionName." | INTERVAL ";
					$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
					Nsd_schedulingController::setupAgentClientIntervalWindows( $objOrder );


				
					break;



				case "4":
					#missing windows


			        $query = "select a.* from htc_os_agents a where a.name = '".$objOrder->care_of."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objAgentLU = $db->loadObject();



			        $query = "select a.* from htc_os_lu_clients a where a.customer_account_code = '".$objOrder->customer_account_code."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objClientsLU = $db->loadObject();



					switch( $objClientsLU->id )
					{
						
						case "1":
							#home depot
						
								$arrHD = array();
								
								$arrHD[] =  array( "day_of_week"=>"1", "window_name"=>"Morning", "window_start_time"=>"7:00:00", "window_end_time"=>"10:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"1", "window_name"=>"Mid-day", "window_start_time"=>"10:00:00", "window_end_time"=>"14:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"1", "window_name"=>"Afternoon", "window_start_time"=>"14:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );
								
								$arrHD[] =  array( "day_of_week"=>"2", "window_name"=>"Morning", "window_start_time"=>"7:00:00", "window_end_time"=>"10:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"2", "window_name"=>"Mid-day", "window_start_time"=>"10:00:00", "window_end_time"=>"14:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"2", "window_name"=>"Afternoon", "window_start_time"=>"14:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );

								

								$arrHD[] =  array( "day_of_week"=>"3", "window_name"=>"Morning", "window_start_time"=>"7:00:00", "window_end_time"=>"10:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"3", "window_name"=>"Mid-day", "window_start_time"=>"10:00:00", "window_end_time"=>"14:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"3", "window_name"=>"Afternoon", "window_start_time"=>"14:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );



								$arrHD[] =  array( "day_of_week"=>"4", "window_name"=>"Morning", "window_start_time"=>"7:00:00", "window_end_time"=>"10:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"4", "window_name"=>"Mid-day", "window_start_time"=>"10:00:00", "window_end_time"=>"14:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"4", "window_name"=>"Afternoon", "window_start_time"=>"14:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );

						
						
								$arrHD[] =  array( "day_of_week"=>"5", "window_name"=>"Morning", "window_start_time"=>"7:00:00", "window_end_time"=>"10:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"5", "window_name"=>"Mid-day", "window_start_time"=>"10:00:00", "window_end_time"=>"14:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"5", "window_name"=>"Afternoon", "window_start_time"=>"14:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );



								$arrHD[] =  array( "day_of_week"=>"6", "window_name"=>"Morning", "window_start_time"=>"7:00:00", "window_end_time"=>"10:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"6", "window_name"=>"Mid-day", "window_start_time"=>"10:00:00", "window_end_time"=>"14:00:00", "max_deliveries"=>"999" );
								$arrHD[] =  array( "day_of_week"=>"6", "window_name"=>"Afternoon", "window_start_time"=>"14:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );



								foreach( $arrHD as $window )
								{

							        $query = "select ordering from htc_os_delivery_windows";
							        $db->setQuery($query);
							        $arrOrdering = $db->loadColumn() ;			
							
									$nextOrderVal = max($arrOrdering) + 1;



									$objDataInsert = new stdClass();
									$objDataInsert->agent_id = $objAgentLU->id;
									$objDataInsert->client_id = $objClientsLU->id;
									$objDataInsert->day_of_week = $window["day_of_week"];
									$objDataInsert->window_name = $window["window_name"];
									$objDataInsert->window_start_time = $window["window_start_time"];
									$objDataInsert->window_end_time = $window["window_end_time"];
									$objDataInsert->max_deliveries = $window["max_deliveries"];
									$objDataInsert->ordering = $nextOrderVal;
									$objDataInsert->state = "1";
									$result = JFactory::getDbo()->insertObject('htc_os_delivery_windows', $objDataInsert);
									$windowID = $db->insertid();	

									$logMessage = "INSIDE | ".$className." | ".$functionName." | WINDOW ";
									$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

									
								}
						
							break;						
						
						
						case "2":
							#JCPenny

						        $query = "select ordering from htc_os_delivery_windows";
						        $db->setQuery($query);
						        $arrOrdering = $db->loadColumn() ;			
						
								$nextOrderVal = max($arrOrdering) + 1;

						
								$arrHD = array();
								
								$arrHD[] =  array( "day_of_week"=>"1", "window_name"=>"All Day", "window_start_time"=>"07:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );
								
								$arrHD[] =  array( "day_of_week"=>"2", "window_name"=>"All Day", "window_start_time"=>"07:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );

								$arrHD[] =  array( "day_of_week"=>"3", "window_name"=>"All Day", "window_start_time"=>"07:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );

								$arrHD[] =  array( "day_of_week"=>"4", "window_name"=>"All Day", "window_start_time"=>"07:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );
						
								$arrHD[] =  array( "day_of_week"=>"5", "window_name"=>"All Day", "window_start_time"=>"07:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"999" );

								$arrHD[] =  array( "day_of_week"=>"6", "window_name"=>"All Day", "window_start_time"=>"07:00:00", "window_end_time"=>"19:00:00", "max_deliveries"=>"2" );


								foreach( $arrHD as $window )
								{

							        $query = "select ordering from htc_os_delivery_windows";
							        $db->setQuery($query);
							        $arrOrdering = $db->loadColumn() ;			
							
									$nextOrderVal = max($arrOrdering) + 1;


									$objDataInsert = new stdClass();
									$objDataInsert->agent_id = $objAgentLU->id;
									$objDataInsert->client_id = $objClientsLU->id;
									$objDataInsert->day_of_week = $window["day_of_week"];
									$objDataInsert->window_name = $window["window_name"];
									$objDataInsert->window_start_time = $window["window_start_time"];
									$objDataInsert->window_end_time = $window["window_end_time"];
									$objDataInsert->max_deliveries = $window["max_deliveries"];
									$objDataInsert->ordering = $nextOrderVal;
									$objDataInsert->state = "1";
									$result = JFactory::getDbo()->insertObject('htc_os_delivery_windows', $objDataInsert);
									$windowID = $db->insertid();	

									$logMessage = "INSIDE | ".$className." | ".$functionName." | WINDOW ";
									$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

									
								}
						
							break;		

						
					}

				
					Nsd_schedulingController::setupAgentClientIntervalWindows( $objOrder );

				
					break;

				default:
				
					break;					
				
			}
			
			
		}


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objSetupACIW: ".print_r($objSetupACIW, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objSetupACIW;
	}





	public static function createHolidayArray( )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=createHolidayArray



		$className = "Nsd_schedulingController";
		$functionName = "createHolidayArray";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrHoliday = array();
		
		$currentYear = date('Y');


		// New Years Day
        $arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("first day of january $currentYear")));

        // Martin Luther King, Jr. Day
        //$arrHoliday[] = date('Y-m-d', strtotime("january $currentYear third monday"));

        // President's Day
		// $arrHoliday[] = date('Y-m-d', strtotime("february $currentYear third monday"));

        // Memorial Day
        $arrHoliday[] = (new DateTime("Last monday of May"))->format("Y-m-d");

        // Independence Day
        $arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("july 4 $currentYear")));

        // Labor Day
        $arrHoliday[] = date('Y-m-d', strtotime("september $currentYear first monday"));

        // Columbus Day
        // $arrHoliday[] = date('Y-m-d', strtotime("october $currentYear second monday"));

        // Veteran's Day
		// $arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("november 11 $currentYear")));

        // Thanksgiving Day
		$arrHoliday[] = date('Y-m-d', strtotime("november $currentYear fourth thursday"));

        // Christmas Day
        $arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("december 25 $currentYear")));

		//test holiday
		//$arrHolidays[] = "2018-09-26";
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r arrHoliday: ".print_r($arrHoliday, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $arrHoliday;
	}	







	public function populateAgentLatLng( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=populateAgentLatLng



		$className = "Nsd_schedulingController";
		$functionName = "populateAgentLatLng";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$query = "select * from htc_os_agents";
		$db->setQuery($query);
		$objListAgents = $db->loadObjectList();



		$arrZipcodes = Nsd_schedulingController::arrZipLookup( );

		foreach( $objListAgents as $agent )
		{

			$latitude = Nsd_schedulingController::findMyValue( $arrZipcodes, 0, $agent->zipcode, 1 );
			$longitude = Nsd_schedulingController::findMyValue( $arrZipcodes, 0, $agent->zipcode, 2 );

			$objDataUpdate = new stdClass();
			$objDataUpdate->id = $agent->id;
			$objDataUpdate->latitude = $latitude;
			$objDataUpdate->longitude = $longitude;
			
			
			$result = JFactory::getDbo()->updateObject('htc_os_agents', $objDataUpdate, 'id');           
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r agent: ".print_r($agent, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			
		}




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	

	

	public function populateServiceAreas( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=populateServiceAreas



		$className = "Nsd_schedulingController";
		$functionName = "populateServiceAreas";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$query = "select distinct (s.zip_serviced), s.agent_code, a.id from htc_os_service_areas_import s left join htc_os_agents a on s.agent_code=a.name where a.id is not null order by s.agent_code, s.zip_serviced";
		$db->setQuery($query);
		$objListAreas = $db->loadObjectList();


		foreach( $objListAreas as $row )
		{

			$objDataInsert = new stdClass();
			$objDataInsert->agent_id 	= $row->id;
			$objDataInsert->zipcode 	= $row->zip_serviced;
			$objDataInsert->state 		= '1';
			
			$result = JFactory::getDbo()->insertObject('htc_os_service_areas', $objDataInsert);
			$newID = $db->insertid();          
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r row: ".print_r($row, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			
		}




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	
	

	public static function evaluateErrorMessage( $objError )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_scheduling&task=evaluateErrorMessage



		$className = "Nsd_schedulingController";
		$functionName = "evaluateErrorMessage";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "SELECT * FROM htc_os_lu_error_messages WHERE state='1' AND error_code = '".$objError->error_code."' ";
        $db->setQuery($query);
        $objErrorMessage = $db->loadObject();
        
		$query = "SELECT * FROM htc_os_lu_clients WHERE state='1' AND customer_account_code = '".$objError->customer_account_code."' ";
        $db->setQuery($query);
        $objLuClient = $db->loadObject();        
        
        
        if ( $objLuClient->contact_phone != "" )
        {
	        
			$objError->error_message_revised = str_replace( "%phone%", $objLuClient->contact_phone, $objErrorMessage->error_message );
	        
        }
		else
		{
			$objError->error_message_revised = str_replace( "%phone%", "1-833-SHIPNSD (1-833-744-7673)", $objErrorMessage->error_message );
			
		}        
        

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objError: ".print_r($objError, true);
		$logMessage .= "\n\r objErrorMessage: ".print_r($objErrorMessage, true);
		$logMessage .= "\n\r objLuClient: ".print_r($objLuClient, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objError;
	}	
	

	public function cron_sendSchedulingReport( )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=cron_sendSchedulingReport



		$className = "Nsd_schedulingController";
		$functionName = "cron_sendSchedulingReport";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->directoryInbound		= $params->get('directory_inbound');
		$paramsComponent->directoryProcessed	= $params->get('directory_processed');
		$paramsComponent->directorySkipped		= $params->get('directory_skipped');
		$paramsComponent->directoryOutbound		= $params->get('directory_outbound');
		
		$paramsComponent->reportEmailSendTo			= $params->get('report_email_send_to');
		$paramsComponent->reportEmailSendFrom		= $params->get('report_email_send_from');
		$paramsComponent->reportEmailSendFromName	= $params->get('report_email_send_from_name');

		$paramsComponent->pathInbound			=  JPATH_SITE."/".$paramsComponent->directoryInbound."/";
		$paramsComponent->pathProcessed			=  JPATH_SITE."/".$paramsComponent->directoryProcessed."/";
		$paramsComponent->pathSkipped			=  JPATH_SITE."/".$paramsComponent->directorySkipped."/";
		$paramsComponent->pathOutbound			=  JPATH_SITE."/".$paramsComponent->directoryOutbound."/";


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$logMessage = "INSIDE | ".$className." | ".$functionName." | object ";
		$logMessage .= "\n\r paramsComponentbjFile: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



        $query = "select d.id, d.stamp_datetime, d.system, d.agent_id, 
if (a.name is not null, a.name, '') as 'agent_name',
d.client_id,d.order_id, d.delivery_zipcode, d.record_type, 
if(d.request_delivery_date != '0000-00-00', d.request_delivery_date, '') as request_delivery_date, 
if(d.request_time_window_start != '0000-00-00 00:00:00', d.request_time_window_start, '') as request_time_window_start, 
if(d.request_time_window_end != '0000-00-00 00:00:00', d.request_time_window_end, '') as request_time_window_end,
d.result, d.error, d.error_code, d.system_error_message from htc_os_deliveries d left join htc_os_agents a on a.id=d.agent_id  where stamp_datetime > DATE_SUB(NOW(), INTERVAL 24 hour) and record_type = 'update'";

        $db->setQuery($query);
        
        $assocListReportRecords = $db->loadAssocList();
        


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r assocListReportRecords: ".print_r($assocListReportRecords, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		if ( count($assocListReportRecords) > 0 )
		{

			$return_ctf = 1;

			$filePath = $paramsComponent->pathOutbound;  
	
			#$fileName = "shipnsd_schedule_report_".date("Ymdhis").".txt";
			$fileName = "shipnsd_schedule_report_".date("Ymdhis").".csv";
			
			$fileInfo = $filePath.$fileName;


			$rptRecord = "";
	
			$fp = fopen( $fileInfo, "a+" );
			fwrite($fp, $rptRecord);
			fclose($fp);
	
			$logMessage = "INSIDE | ".$className." | ".$functionName. " Successful created transfer file: ".$fileName;
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r filePath: ".print_r($filePath, true);
			$logMessage .= "\n\r fileName: ".print_r($fileName, true);
			$logMessage .= "\n\r fileInfo: ".print_r($fileInfo, true);
			$logMessage .= "\n\r rptRecord: ".print_r($rptRecord, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	

			#header record
			$strExport = array();
			$strExport['stamp_datetime'] = 'stamp_datetime';
			$strExport['system'] = 'system';
			$strExport['agent_id'] = 'agent_id';
			$strExport['agent_name'] = 'agent_name';
			$strExport['client_id'] = 'client_id';
			$strExport['order_id'] = 'order_id';
			$strExport['delivery_zipcode'] = 'delivery_zipcode';
			$strExport['record_type'] = 'record_type';
			$strExport['request_delivery_date'] = 'request_delivery_date';
			$strExport['request_time_window_start'] = 'request_time_window_start';
			$strExport['request_time_window_end'] = 'request_time_window_end';
			$strExport['result'] = 'result';
			$strExport['error'] = 'error';
			$strExport['error_code'] = 'error_code';
			$strExport['system_error_message'] = 'system_error_message';
			$fp = fopen( $fileInfo, "a+" );
			#fputcsv( $fp, $strExport, "\t" );
			fputcsv( $fp, $strExport );
			fclose( $fp );
	
	
			$record_count = 0;
	
			foreach ( $assocListReportRecords as $arrRecord )
			{
	
	
				$logMessage = "INSIDE | ".$className." | ".$functionName ." | Added data record id: ".$arrRecord['id']." to file: ".$fileName;
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
				unset($arrRecord['id']);
	
				$fp = fopen( $fileInfo, "a+" );
				#fputcsv( $fp, $arrRecord, "\t" );
				fputcsv( $fp, $arrRecord );
				fclose( $fp );
	
				$record_count += 1;

			}


			$body = "";
			$body .= "<br>Attached is the ShipNSD Scheduling Report for: ".date("Y-m-d")."<br>";
			$body .= "<br><br>The report file is a comma delimited text file that can be opened in MSExcel.<br>";


			$fgc = file_get_contents($fileInfo);
			$attachment_content = base64_encode( file_get_contents($fileInfo) );


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = $paramsComponent->reportEmailSendTo;
			$objSendEmailMessage->email_from = $paramsComponent->reportEmailSendFrom;
			$objSendEmailMessage->email_from_name = $paramsComponent->reportEmailSendFromName;
			$objSendEmailMessage->email_subject = "ShipNSD Scheduling Report for ".date("Y-m-d");
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->attachment_content = $attachment_content;
			$objSendEmailMessage->attachment_filename = $fileName;
			
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );

			$logMessage = "INSIDE | Ml_apiController | ".$functionName;
			$logMessage .= "\n\r objSGReturn: ".print_r($objSGReturn, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			if (  $objSGReturn->error == "0"  )
			{
				
				JFile::delete($fileInfo);

				$logMessage = "INSIDE | Ml_apiController | ".$functionName. "| Sending Schedule Report Success";
				$logMessage .= "\n\r objSGReturn: ".print_r($objSGReturn, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
			}
			else
			{

				$logMessage = "INSIDE | Ml_apiController | ".$functionName. "| Sending Schedule Report Failed";
				$logMessage .= "\n\r objSendEmailMessage: ".print_r($objSendEmailMessage, true);
				$logMessage .= "\n\r objSGReturn: ".print_r($objSGReturn, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}

		}
		else
		{

			$body = "";
			$body .= "<br>There was no scheduling activity recorded on: ".date("Y-m-d")."<br>";

			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = $paramsComponent->reportEmailSendTo;
			$objSendEmailMessage->email_from = $paramsComponent->reportEmailSendFrom;
			$objSendEmailMessage->email_from_name = $paramsComponent->reportEmailSendFromName;
			$objSendEmailMessage->email_subject = "ShipNSD Scheduling Report for ".date("Y-m-d");
			$objSendEmailMessage->email_body = $body;
			
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );

			$logMessage = "INSIDE | Ml_apiController | ".$functionName;
			$logMessage .= "\n\r objSGReturn: ".print_r($objSGReturn, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
		}





		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public static function pruneScheduleLogs()
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=pruneScheduleLogs
		
		
		$className = "Nsd_schedulingController";
		$functionName = "pruneScheduleLogs";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	



        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->log_table_prune_size	= $params->get('log_table_prune_size');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;		
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrTables = array();
		$arrTables[] = "htc_os_schedule_validation_cron_log";


		$log_table_prune_size = $paramsComponent->log_table_prune_size;
		$log_table_prune_size_upper_boundry = ( $log_table_prune_size + 10000 );

				$logMessage = "INSIDE | Ml_apiController | ".$functionName;
				$logMessage .= "\n\r log_table_prune_size: ".print_r($log_table_prune_size, true);
				$logMessage .= "\n\r log_table_prune_size_upper_boundry: ".print_r($log_table_prune_size_upper_boundry, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		#if ( date('H') == '23' )
		
		if ( TRUE )
		{
			foreach( $arrTables as $table )
			{
				$query = "select count(z.id) from ".$table." z";
				$db->setQuery($query);
				$recordCount = $db->loadResult();

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r recordCount for ".$table.": ".print_r($recordCount, true);
				#$logMessage .= "\n\r dateH: ".print_r(date('H'), true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				if ( $recordCount > $log_table_prune_size_upper_boundry )
				{
		
					$query = "select z.id from ".$table." z order by z.id desc limit ".$log_table_prune_size.", 1";
					$db->setQuery($query);
					$idRecord = $db->loadResult();
			
					if ( $idRecord != "" )
					{
						$queryD = "Delete FROM ".$table." WHERE id <= '".$idRecord."' ";
						$db->setQuery($queryD);
						$db->query();
					}
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r idRecord: ".print_r($idRecord, true);
					$logMessage .= "\n\r queryD: ".print_r($queryD, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				}


			}
		}

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




	public static function determine_allow_schedule( $objOrder )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=determine_allow_schedule



		$className = "Nsd_schedulingController";
		$functionName = "determine_allow_schedule";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "select count(m.id) as count from htc_os_matrix m
		left join htc_os_lu_clients c on c.id = m.id_client
		left join htc_os_lu_shipper s on s.id = m.id_shipper
		left join htc_os_lu_service_levels l on l.id = m.id_service_level
		left join htc_os_lu_mode o on o.id = m.id_mode
		left join htc_os_lu_order_status st on st.id = m.id_order_status
		where
		c.customer_account_code = '".$objOrder->customer_account_code."'
		and s.origid = '".$objOrder->origid."'
		and l.name = '".$objOrder->service_level."'
		and o.name = '".$objOrder->op_code."' 
		and m.state = 1";
		$db->setQuery($query);
		$objMatrix = $db->loadObject() ;


			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objOrder: ".print_r($objOrder, true);
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objMatrix: ".print_r($objMatrix, true);
			
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objReturn = new stdClass();
		$objReturn->allowSchedule = ( $objMatrix->count > 0 ) ? 1 : 0 ;


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}	



// !SCHEDULE DATA PROCESSING


// !SCHEDULE PRE - PROCESSING


	public function cron_schedule_data_validation( )
	{

		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=cron_schedule_data_validation
		

		$className = "Nsd_schedulingController";
		$functionName = "cron_schedule_data_validation";
		
		$db = JFactory::getDBO();

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}		

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->agent_process_send_to			= $params->get('agent_process_send_to');
		$paramsComponent->agent_process_email_send_from	= $params->get('agent_process_email_send_from');
		$paramsComponent->agent_process_send_from_name	= $params->get('agent_process_send_from_name');


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }





		#$query = "SELECT * from htc_os_schedule_validation_cron_log order by id desc limit 1";
		
		$query = "SELECT start_datetime, NOW(), IF(start_datetime < (NOW() - INTERVAL 20 MINUTE), 'RESET', 'ALERT') as 'timestatus', state, notes FROM htc_os_schedule_validation_cron_log order By id DESC limit 1";
		
		$db->setQuery($query);
		$objLogCron = $db->loadObject();

		if ( $objLogCron->state == "1" )
		{


			if ( $objLogCron->timestatus == "RESET" )
			{
				Nsd_schedulingController::cron_schedule_data_validation_reset();
			}
			else
			{
	
				$logMessage = "INSIDE | ".$className." | ".$functionName." | cron_schedule_data_validation is already running. The process started at ".$objLogCron->start_datetime;	
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		
	
	
				$body = "";
				$body .= "<br>The cron_schedule_data_validation process is still running. The process started at ".$objLogCron->start_datetime."<br>";
				$body .= "<br><br>The last note is: ".$objLogCron->notes."<br>";
				$body .= "<br><br>Click here to reset: ".JURI::root()."index.php?option=com_nsd_scheduling&task=cron_schedule_data_validation_reset<br>";
	
				$body .= "<br><br>Click here to reset: https://tracking.shipnsd.com/index.php?option=com_nsd_scheduling&task=cron_schedule_data_validation_reset<br>";
				
	
				$objSendEmailMessage = new stdClass();
				$objSendEmailMessage->email_to = $paramsComponent->agent_process_send_to;
				$objSendEmailMessage->email_from = $paramsComponent->agent_process_email_send_from;
				$objSendEmailMessage->email_from_name  = $paramsComponent->agent_process_send_from_name;
				$objSendEmailMessage->email_subject = "ALERT | ".$className." | ".$functionName;
				$objSendEmailMessage->email_body = $body;
				$objSendEmailMessage->email_category = "scheduling support";
		
				$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
	
	
	
				$logMessage = "INSIDE | ".$className." | ".$functionName." | Alert sent";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
				
			}


			
			
					
		}
		else
		{


			$datetime = date("Y-m-d H:i:s");
			$datetime_gmt = gmdate("Y-m-d H:i:s");
			$timestamp = date("YmdHisD").microtime(true);

	
	        $objData = new stdClass();
	        $objData->state = 1;
			$objData->start_datetime_gmt = $datetime_gmt;
			$objData->start_datetime = $datetime;
			$objData->notes = "Start validation processing of files.";
			$result = JFactory::getDbo()->insertObject('htc_os_schedule_validation_cron_log', $objData);
	        $cronID = $db->insertid();
	        
	        
	        

			$returnScheduleDataValidator = Nsd_schedulingController::schedule_data_validator(  $cronID  );



			$datetime = date("Y-m-d H:i:s");
			$datetime_gmt = gmdate("Y-m-d H:i:s");
			$timestamp = date("YmdHisD").microtime(true);	
	
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
	        $objData = new stdClass();
	        $objData->id = $cronID;
	        $objData->state = 0;
			$objData->end_datetime_gmt = $datetime_gmt;
			$objData->end_datetime = $datetime;
	        $objData->total_time = number_format($stamp_total_micro,2);
	        $objData->notes = "Finished validation processing all files.";
	        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 

	
	
	
		}
		

		#prune the schedule processed directory
		Nsd_schedulingController::pruneScheduleLogs( );
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public function cron_schedule_data_validation_reset( )
	{

		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=cron_schedule_data_validation_reset
		

		$className = "Nsd_schedulingController";
		$functionName = "cron_schedule_data_validation_reset";
		
		$db = JFactory::getDBO();

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}		

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->agent_process_send_to			= $params->get('agent_process_send_to');
		$paramsComponent->agent_process_email_send_from	= $params->get('agent_process_email_send_from');
		$paramsComponent->agent_process_send_from_name	= $params->get('agent_process_send_from_name');

        
		$paramsComponent->directoryInbound		= $params->get('directory_inbound');
		$paramsComponent->directoryProcessed	= $params->get('directory_processed');
		$paramsComponent->directorySkipped		= $params->get('directory_skipped');

		$paramsComponent->pathInbound			=  JPATH_SITE."/".$paramsComponent->directoryInbound."/";
		$paramsComponent->pathProcessed			=  JPATH_SITE."/".$paramsComponent->directoryProcessed."/";
		$paramsComponent->pathSkipped			=  JPATH_SITE."/".$paramsComponent->directorySkipped."/";




		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "SELECT * from htc_os_schedule_validation_cron_log order by id desc limit 1";
		$db->setQuery($query);
		$objLogCron = $db->loadObject();
		
		if ( $objLogCron->last_file_name != "" )
		{
			$objFile = new stdClass();
			$objFile->fileName = $objLogCron->last_file_name;
		}

		if ( $objLogCron->state == "1" )
		{
			$logMessage = "INSIDE | ".$className." | ".$functionName." | Cron state is 1.  Begin reset.";	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		


				
			$objFile->fileFullServerPath = $paramsComponent->pathInbound.$objFile->fileName;
			$objFile->fileSkippedFullServerPath = $paramsComponent->pathSkipped.$objFile->fileName;



			if ( JFile::move( $objFile->fileFullServerPath, $objFile->fileSkippedFullServerPath ) )
			{
				$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->fileName." moved/renamed ".$objFile->fileProcessedFullServerPath;
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				$additionalBody = "".$objFile->fileName." moved/renamed successfully.";
			}
			else
			{
				$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->fileName." NOT moved/renamed ".$objFile->fileProcessedFullServerPath;
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
				$additionalBody = "".$objFile->fileName." NOT moved/renamed ".$objFile->fileProcessedFullServerPath;
				$additionalBody .= "<br><br>objFile: ".print_r($objFile, true)."<br>";
			}





			$objDataUpdate = new stdClass();
			$objDataUpdate->id = $objLogCron->id;
			$objDataUpdate->state = "0";
			$result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objDataUpdate, 'id');          





			$body = "";
			$body .= "<br>The cron_schedule_data_validation process has been reset.";

			$body .= "<br><br>The following file has been skipped and needs to be reviewed because of data/file errors: ".$objFile->fileName."<br>";
			
			$body .= "<br><br>Click here to rerun: ".JURI::root()."index.php?option=com_nsd_scheduling&task=cron_schedule_data_validation<br>";
			

			$body .= "<br><br>Click here to rerun: https://tracking.shipnsd.com/index.php?option=com_nsd_scheduling&task=cron_schedule_data_validation<br>";

			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = $paramsComponent->agent_process_send_to;
			$objSendEmailMessage->email_from = $paramsComponent->agent_process_email_send_from;
			$objSendEmailMessage->email_from_name  = $paramsComponent->agent_process_send_from_name;
			$objSendEmailMessage->email_subject = "ALERT | ".$className." | ".$functionName." | Cron Reset";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );



			$logMessage = "INSIDE | ".$className." | ".$functionName." | Cron state was reset to  0. Alert sent";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
			
					
		}
		else
		{

			$logMessage = "INSIDE | ".$className." | ".$functionName." | Cron state was 0. Alert sent";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		}
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}







	public static function schedule_data_validator( $cronID )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=schedule_data_validator
		

		$className = "Nsd_schedulingController";
		$functionName = "schedule_data_validator";
		
		$db = JFactory::getDBO();

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->directoryInbound		= $params->get('directory_inbound');
		$paramsComponent->directoryProcessed	= $params->get('directory_processed');
		$paramsComponent->directorySkipped		= $params->get('directory_skipped');

		$paramsComponent->pathInbound			=  JPATH_SITE."/".$paramsComponent->directoryInbound."/";
		$paramsComponent->pathProcessed			=  JPATH_SITE."/".$paramsComponent->directoryProcessed."/";
		$paramsComponent->pathSkipped			=  JPATH_SITE."/".$paramsComponent->directorySkipped."/";




		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$arrFiles = JFolder::files( $paramsComponent->pathInbound );


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r arrFiles: ".print_r($arrFiles, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		if ( count($arrFiles) > 0 )
		{

			foreach( $arrFiles as $fileName )
			{

				$datetime = date("Y-m-d H:i:s");
				$datetime_gmt = gmdate("Y-m-d H:i:s");
				$timestamp = date("YmdHisD").microtime(true);

				$objFile = new stdClass();
				$objFile->fileName = $fileName;
				$objFile->fileFullServerPath = $paramsComponent->pathInbound.$fileName;
				
				
				#get Agent Name
				
				$objWork = new stdClass();
				
				$objWork->fileName = $fileName;
		
				$arrFind = array( " ", "-" );
				$arrReplace = array( "_", "_" );
		
				$objWork->fileNameClean = str_replace( $arrFind, $arrReplace, $objWork->fileName );
				
				$arrfileName = explode( "_", $objWork->fileNameClean );
				
				$objWork->arrfileName = $arrfileName;
				
				$arrfileName = array_reverse( $arrfileName );
				
				$objWork->arrfileNameRev = $arrfileName;
				
				$objFile->agent_name = strstr($arrfileName[0], '.', true); 		
				
				$objFile->cronID = $cronID;
						
	
				$last_status = "Data Validation";
				
				$objValidateData = Nsd_schedulingController::schedule_validate( $objFile );

			        $objData = new stdClass();
			        $objData->id = $cronID;
			        $objData->end_datetime_gmt = $datetime_gmt;
					$objData->end_datetime = $datetime;
					$objData->notes = "Started file: ".$fileName;
			        $objData->total_time = number_format($stamp_total_micro,2);
			        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 



				if ( $objValidateData->flag_complete == "1" )
				{
					
					$logMessage = "INSIDE | ".$className." | ".$functionName. " | Data Validation Success";
					$logMessage .= "\n\r objFile: ".print_r($objFile, true);
					$logMessage .= "\n\r objValidateData: ".print_r($objValidateData, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$objLog = new stdClass();
					
					$objLog->agent_id = "";
					$objLog->agent_name = $objFile->agent_name;
					$objLog->file_name = $fileName;
					$objLog->flag_data_validation = $objValidateData->flag_complete;
					$objLog->last_status = $last_status;
					$objLog->stamp_datetime = $datetime;
					$objLog->stamp_datetime_gmt = $datetime_gmt;
					
	                $db->insertObject('htc_os_schedule_data_log', $objLog);
	                $logid = $db->insertid();
	                
			
					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


					$datetime = date("Y-m-d H:i:s");
					$datetime_gmt = gmdate("Y-m-d H:i:s");
					$timestamp = date("YmdHisD").microtime(true);
			
			        $objData = new stdClass();
			        $objData->id = $cronID;
			        $objData->end_datetime_gmt = $datetime_gmt;
					$objData->end_datetime = $datetime;
					$objData->notes = "Finished with file: ".$fileName;
			        $objData->total_time = number_format($stamp_total_micro,2);
			        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 	                
	                
					
				}
				else
				{
					
					$logMessage = "INSIDE | ".$className." | ".$functionName. " | Data Validation Failed";
					#$logMessage .= "\n\r objFile: ".print_r($objFile, true);
					$logMessage .= "\n\r objValidateData: ".print_r($objValidateData, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$datetime = date("Y-m-d H:i:s");
					$datetime_gmt = gmdate("Y-m-d H:i:s");
					$timestamp = date("YmdHisD").microtime(true);


					$objLog = new stdClass();
					
					$objLog->agent_id = "";
					$objLog->agent_name = $objFile->agent_name;
					$objLog->file_name = $fileName;
					$objLog->flag_data_validation = $objValidateData->flag_complete;
					$objLog->last_status = $last_status;
					$objLog->stamp_datetime = $datetime;
					$objLog->stamp_datetime_gmt = $datetime_gmt;
	                $db->insertObject('htc_os_schedule_data_log', $objLog);




			        $objData = new stdClass();
			        $objData->id = $cronID;
			        
					$objData->end_datetime_gmt = $datetime_gmt;
					$objData->end_datetime = $datetime;
					$objData->notes = "Finished with file: ".$fileName;
			        $objData->total_time = number_format($stamp_total_micro,2);
			        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 		                
	                
					
				}
				
				
			
			}
		
			$return = 1;
			
		}
		else
		{

			$logMessage = "INSIDE | ".$className." | ".$functionName." | No files to process";
			#$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			$return = 0;
		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | totalFiles: ".count($arrFiles)." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "END | ".$className." | ".$functionName." | totalFiles: ".count($arrFiles)." | seconds: ".number_format($stamp_total_micro,2);			
		#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $return;
	}



	public static function schedule_validate( $objFile )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=schedule_validate


		#memory issues
		#https://blog.bitexpert.de/blog/think-about-it-phpexcel-performance-tweaks-part-1/



		$className = "Nsd_schedulingController";
		$functionName = "schedule_validate";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->agent_process_send_to			= $params->get('agent_process_send_to');
		$paramsComponent->agent_process_email_send_from	= $params->get('agent_process_email_send_from');
		$paramsComponent->agent_process_send_from_name	= $params->get('agent_process_send_from_name');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$logMessage = "INSIDE | ".$className." | ".$functionName." | object ";
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$objValidateData = new stdClass();
		#$objValidateData->flag_complete = "1";


		if ( file_exists( $objFile->fileFullServerPath ) )
		{
		    # file exists
	
			$countRecords = 0;
		    
		    
		    $filesize = filesize( $objFile->fileFullServerPath  );
		    
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r filesize: ".print_r($filesize, true);	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		    
		    
		    if ( $filesize > 0 )
		    {


				$inputFileType = 'Xlsx';
				$inputFileName = $objFile->fileFullServerPath;
		
		
				$arrSheetNames = array();
				$arrSheetNames[] = "geographicalAreas";
				$arrSheetNames[] = "schedules";
				$arrSheetNames[] = "slots";
				$arrSheetNames[] = "holidays";
		
		

		
		
				foreach ( $arrSheetNames as $sheetname )
				{

					$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
					
					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
					$datetime = date("Y-m-d H:i:s");
					$datetime_gmt = gmdate("Y-m-d H:i:s");
					$timestamp = date("YmdHisD").microtime(true);

			        $objData = new stdClass();
			        $objData->id = $objFile->cronID;
					$objData->end_datetime_gmt = $datetime_gmt;
					$objData->end_datetime = $datetime;
					$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname." | define reader";
			        $objData->total_time = number_format($stamp_total_micro,2);
			        $objData->last_file_name = $objFile->fileName;
			        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 




					$reader->setReadDataOnly(true);

					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r sheetname: ".print_r($sheetname, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
					$datetime = date("Y-m-d H:i:s");
					$datetime_gmt = gmdate("Y-m-d H:i:s");
					$timestamp = date("YmdHisD").microtime(true);

			        $objData = new stdClass();
			        $objData->id = $objFile->cronID;
					$objData->end_datetime_gmt = $datetime_gmt;
					$objData->end_datetime = $datetime;
					$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname." | reader->setReadDataOnly";
			        $objData->total_time = number_format($stamp_total_micro,2);
			        $objData->last_file_name = $objFile->fileName;
			        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 


					
					$reader->setLoadSheetsOnly($sheetname);
					
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r reader: ".print_r($reader, true);
					$logMessage .= "\n\r ";
					#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
					$datetime = date("Y-m-d H:i:s");
					$datetime_gmt = gmdate("Y-m-d H:i:s");
					$timestamp = date("YmdHisD").microtime(true);


			        $objData = new stdClass();
			        $objData->id = $objFile->cronID;
					$objData->end_datetime_gmt = $datetime_gmt;
					$objData->end_datetime = $datetime;
					$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname." | reader->setLoadSheetsOnly";
			        $objData->total_time = number_format($stamp_total_micro,2);
			        $objData->last_file_name = $objFile->fileName;
			        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 

					
					
					$spreadsheet = $reader->load($inputFileName);
					
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r spreadsheet: ".print_r($spreadsheet, true);
					$logMessage .= "\n\r ";
					#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					

					$stamp_end_micro = microtime(true);
					$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
					$datetime = date("Y-m-d H:i:s");
					$datetime_gmt = gmdate("Y-m-d H:i:s");
					$timestamp = date("YmdHisD").microtime(true);

			        $objData = new stdClass();
			        $objData->id = $objFile->cronID;
					$objData->end_datetime_gmt = $datetime_gmt;
					$objData->end_datetime = $datetime;
					$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname." | reader->load";
			        $objData->total_time = number_format($stamp_total_micro,2);
			        $objData->last_file_name = $objFile->fileName;
			        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 
					
					
					$sheetData = $spreadsheet->getActiveSheet()->toArray();			
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r sheetname: ".print_r($sheetname, true);
					$logMessage .= "\n\r sheetData: ".print_r($sheetData, true);
					$logMessage .= "\n\r ";
					#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
			        $objData = new stdClass();
			        $objData->id = $objFile->cronID;
					$objData->end_datetime_gmt = $datetime_gmt;
					$objData->end_datetime = $datetime;
					$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname." | reader spreadsheet->getActiveSheet()->toArray()";
			        $objData->total_time = number_format($stamp_total_micro,2);
			        $objData->last_file_name = $objFile->fileName;
			        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 		
		

		
					switch( $sheetname )
					{
		
						case "geographicalAreas":
							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$datetime = date("Y-m-d H:i:s");
							$datetime_gmt = gmdate("Y-m-d H:i:s");
							$timestamp = date("YmdHisD").microtime(true);

					        $objData = new stdClass();
					        $objData->id = $objFile->cronID;
					        
							$objData->end_datetime_gmt = $datetime_gmt;
							$objData->end_datetime = $datetime;
							$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname;
					        $objData->total_time = number_format($stamp_total_micro,2);
					        $objData->last_file_name = $objFile->fileName;
					        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 



							$countRow = 1;
							
							$countEmptyMinPostalCode = 0;
							$countEmptyMaxPostalCode = 0;
							$countEmptyPostalCodes = 0;							
							
							foreach( $sheetData as $arrData )
							{
		
								if ( $countRow > 1 && trim($arrData[0]) != "" )
								{


									$countEmptyMinPostalCode = ( $arrData[4] == "" ) ? ($countEmptyMinPostalCode + 1) : ($countEmptyMinPostalCode + 0) ;
									$countEmptyMaxPostalCode = ( $arrData[5] == "" ) ? ($countEmptyMaxPostalCode + 1) : ($countEmptyMaxPostalCode + 0) ;
									
			
									$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
									$logMessage .= "\n\r countEmptyMinPostalCode: ".print_r($countEmptyMinPostalCode, true);
									$logMessage .= "\n\r countEmptyMaxPostalCode: ".print_r($countEmptyMaxPostalCode, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	

									
								}
								
								$countRow += 1;
							}


							$countEmptyPostalCodes = $countEmptyMinPostalCode + $countEmptyMaxPostalCode;

							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
							$logMessage .= "\n\r countEmptyMinPostalCode: ".print_r($countEmptyMinPostalCode, true);
							$logMessage .= "\n\r countEmptyMaxPostalCode: ".print_r($countEmptyMaxPostalCode, true);
							$logMessage .= "\n\r countEmptyPostalCodes: ".print_r($countEmptyPostalCodes, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	

							break;
		
		
						case "schedules":

							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
							
							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$datetime = date("Y-m-d H:i:s");
							$datetime_gmt = gmdate("Y-m-d H:i:s");
							$timestamp = date("YmdHisD").microtime(true);


					        $objData = new stdClass();
					        $objData->id = $objFile->cronID;
					        
							$objData->end_datetime_gmt = $datetime_gmt;
							$objData->end_datetime = $datetime;
							$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname;
					        $objData->total_time = number_format($stamp_total_micro,2);
					        $objData->last_file_name = $objFile->fileName;
					        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id');  							
									
			
							$strTabSchedulesHolidays = $sheetData[1][4];
							$arrTabSchedulesHolidays = explode( "|", $strTabSchedulesHolidays );

							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
							$logMessage .= "\n\r strTabSchedulesHolidays: ".print_r($strTabSchedulesHolidays, true);
							$logMessage .= "\n\r arrTabSchedulesHolidays: ".print_r($arrTabSchedulesHolidays, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

						
							break;
		
		
						case "slots":


							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$datetime = date("Y-m-d H:i:s");
							$datetime_gmt = gmdate("Y-m-d H:i:s");
							$timestamp = date("YmdHisD").microtime(true);


					        $objData = new stdClass();
					        $objData->id = $objFile->cronID;
					        
							$objData->end_datetime_gmt = $datetime_gmt;
							$objData->end_datetime = $datetime;
							$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname;
					        $objData->total_time = number_format($stamp_total_micro,2);
					        $objData->last_file_name = $objFile->fileName;
					        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 			

							$countRow = 1;
							
							$arrTabSlotsSlotDays = array();
							
							foreach( $sheetData as $arrData )
							{
		
								if ( $countRow > 1 && trim($arrData[0]) != "" )
								{
									
									
									$arrFind = array( ";", " " );
									$arrReplace = array( ",", "" );
									$slot = str_replace( $arrFind, $arrReplace, $arrData[2] );	
									
									#$arrTabSlotsSlotDays[] = trim( strtoupper( $arrData[2] ) ) ;
									$arrTabSlotsSlotDays[] = trim( strtoupper( $slot ) ) ;


			
									$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
									$logMessage .= "\n\r arrData: ".print_r($arrData, true);
									$logMessage .= "\n\r arrTabSlotsSlotDays: ".print_r($arrTabSlotsSlotDays, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	

									
								}
								
								$countRow += 1;
							}
			
		
			
			
						
							break;
		
		
						case "holidays":

							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$datetime = date("Y-m-d H:i:s");
							$datetime_gmt = gmdate("Y-m-d H:i:s");
							$timestamp = date("YmdHisD").microtime(true);


					        $objData = new stdClass();
					        $objData->id = $objFile->cronID;
					       
							$objData->end_datetime_gmt = $datetime_gmt;
							$objData->end_datetime = $datetime;
							$objData->notes = "Inside ".$className." | ".$functionName." | file: ".$objFile->fileName." | tab: ".$sheetname;
					        $objData->total_time = number_format($stamp_total_micro,2);
					        $objData->last_file_name = $objFile->fileName;
					        $result = JFactory::getDbo()->updateObject('htc_os_schedule_validation_cron_log', $objData, 'id'); 


							$countRow = 1;
							
							$arrTabHolidaysHolidayName = array();
							
							foreach( $sheetData as $arrData )
							{
		
								if ( $countRow > 1 && trim($arrData[0]) != "" )
								{
									$arrTabHolidaysHolidayName[] = $arrData[0];
								}
								
								$countRow += 1;
							}


							$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$sheetname;
							$logMessage .= "\n\r arrTabHolidaysHolidayName: ".print_r($arrTabHolidaysHolidayName, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
						
							break;
						
					}
					
					$spreadsheet->disconnectWorksheets(); 
					$spreadsheet->garbageCollect();
					unset($spreadsheet); 	
					unset($reader); 					
				}


			}


				#test for missing days in the schedule

				$query = "select a.key from htc_os_lu_days a where state = '1'";
				$db->setQuery($query);
				$arrDayCompare = $db->loadColumn();
				
				$resultDiffSchedule = array_diff($arrTabSlotsSlotDays, $arrDayCompare);
				

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r arrDayCompare: ".print_r($arrDayCompare, true);
				$logMessage .= "\n\r arrTabSlotsSlotDays: ".print_r($arrTabSlotsSlotDays, true);
				$logMessage .= "\n\r resultDiffSchedule: ".print_r($resultDiffSchedule, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


							

				$body = "";
				
				
								
				
				if ( $countEmptyPostalCodes > 0 )
				{

					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r WARNING | For Agent: ".$objFile->agent_name.";  there are missing min or max postal codes in geographicalArea tab.";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


					$body = "For Agent: ".$objFile->agent_name."; there are missing min or max postal codes in geographicalArea tab.";
					$body .= "<br><br>";
					
				}
				else
				{
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r Note | For Agent: ".$objFile->agent_name.";  all min or max postal codes in geographicalArea tab.";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }						
				}


				
				
				if ( count( $resultDiffSchedule ) > 0 )
				{

					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r WARNING | For Agent: ".$objFile->agent_name.";  the following slot days [ ".implode( '; ', $resultDiffSchedule). " ] are not listed in the in htc_os_lu_days table.";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


					$body = "For Agent: ".$objFile->agent_name.";  the following slot days [ ".implode( '; ', $resultDiffSchedule). " ] are not listed in the in htc_os_lu_days table.";
					$body .= "<br><br>";
					
				}
				else
				{
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r Note | For Agent: ".$objFile->agent_name.";  all slot days are listed in the in htc_os_lu_days table.";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }						
				}








				#test for missing holidays

				
				#create array from matrix table to compare schedule holidays to matrix key holidays
				
				
				
				
				
				#check for new or missing holiday in holiday tab against the matix

			
				$query = "select distinct data_lookup from htc_os_lu_holiday_matrix where state = '1' order by data_lookup";
				$db->setQuery($query);
				$arrHolidayLUMatrix = $db->loadColumn();


				$resultDiffHolidaysOnHolidaysTab = array_diff($arrTabHolidaysHolidayName, $arrHolidayLUMatrix);


				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r resultDiffHolidaysOnHolidaysTab: ".print_r($resultDiffHolidaysOnHolidaysTab, true);
				$logMessage .= "\n\r arrTabHolidaysHolidayName: ".print_r($arrTabHolidaysHolidayName, true);
				$logMessage .= "\n\r arrHolidayLUMatrix: ".print_r($arrHolidayLUMatrix, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




				if ( count($resultDiffHolidaysOnHolidaysTab) > 0 )
				{

					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r WARNING | For Agent: ".$objFile->agent_name.";  the following Holidays are not listed consistently in the Holiday data.  Ensure that the Holiday data [ ".implode( '; ', $resultDiffHolidaysOnHolidaysTab). " ] in the Holiday tab is in the Holidays matrix table; htc_os_lu_holiday_matrix.";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }								

					$body .= "For Agent: ".$objFile->agent_name.";  the following Holidays are not listed consistently in the Holiday data.  Ensure that the Holiday data [ ".implode( '; ', $resultDiffHolidaysOnHolidaysTab). " ] in the Holiday tab is in the Holidays matrix table; htc_os_lu_holiday_matrix.";					
				}
				else
				{
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r Note | For Agent: ".$objFile->agent_name.";  the following Holidays are listed consistently in the Holiday matrix table.";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }						
				}




				
				#check for new or missing holiday in schedule tab against the matix

/*
				$query = "select distinct key from htc_os_lu_holiday_matrix where state = '1' order by key";
				$db->setQuery($query);
				$arrScheduleMatrix = $db->loadColumn();
*/
				
				
				$resultDiffHolidaysOnScheduleTab = array_diff($arrTabSchedulesHolidays, $arrHolidayLUMatrix);


				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r resultDiffHolidaysOnScheduleTab: ".print_r($resultDiffHolidaysOnScheduleTab, true);
				$logMessage .= "\n\r arrTabSchedulesHolidays: ".print_r($arrTabSchedulesHolidays, true);
				$logMessage .= "\n\r arrHolidayLUMatrix: ".print_r($arrHolidayLUMatrix, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




				if ( count($resultDiffHolidaysOnScheduleTab) > 0 )
				{

					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r WARNING | For Agent: ".$objFile->agent_name.";  the following Holidays are not listed consistently in the Schedule tab data.  Ensure that the Holiday data [ ".implode( '; ', $resultDiffHolidaysOnScheduleTab). " ] in the Schedules tab is in the Holidays matrix table; htc_os_lu_holiday_matrix.";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }								

					$body .= "For Agent: ".$objFile->agent_name.";  the following Holidays are not listed consistently in the Holiday data.  Ensure that the Holiday data [ ".implode( '; ', $resultDiffHolidaysOnScheduleTab). " ] in the Schedules tab  is in the Holidays matrix table; htc_os_lu_holiday_matrix.";					
				}
				else
				{
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r Note | For Agent: ".$objFile->agent_name.";  the following Holidays are listed consistently in the Holiday matrix table.";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }						
				}



				if ( count( $resultDiffSchedule ) > 0 || count($resultDiffHolidaysOnHolidaysTab) > 0 || count($resultDiffHolidaysOnScheduleTab) > 0 ||  $countEmptyPostalCodes > 0  )
				{

					$objSendEmailMessage = new stdClass();
					$objSendEmailMessage->email_to = $paramsComponent->agent_process_send_to;
					$objSendEmailMessage->email_from = $paramsComponent->agent_process_email_send_from;
					$objSendEmailMessage->email_from_name  = $paramsComponent->agent_process_send_from_name;
					$objSendEmailMessage->email_subject = "WARNING | ".$className." | ".$functionName." | Bad Data for ".$objFile->agent_name;
					$objSendEmailMessage->email_body = $body;
					$objSendEmailMessage->email_category = "scheduling support";
			
					$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
					
					
					$objValidateData->flag_complete = "0";
				}				
				else
				{


					$body .= "For Agent: ".$objFile->agent_name.";  the agent file successfully passed validation and is now ready for processing.";
					$body .= "<br><br>Click here to process this file: ".JURI::root()."index.php?option=com_nsd_scheduling&task=cron_schedule_data_processing<br>";

					$body .= "<br><br>Click here to process this file: https://tracking.shipnsd.com/index.php?option=com_nsd_scheduling&task=cron_schedule_data_processing<br>";


					$objSendEmailMessage = new stdClass();
					$objSendEmailMessage->email_to = $paramsComponent->agent_process_send_to;
					$objSendEmailMessage->email_from = $paramsComponent->agent_process_email_send_from;
					$objSendEmailMessage->email_from_name  = $paramsComponent->agent_process_send_from_name;
					$objSendEmailMessage->email_subject = "SUCCESS | Agent File Validation successful for ".$objFile->agent_name;
					$objSendEmailMessage->email_body = $body;
					$objSendEmailMessage->email_category = "scheduling support";
			
					$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
					
					
					$objValidateData->flag_complete = "1";
					
					
				}
			


		}
		else
		{
			$objValidateData->flag_complete = "0";
		}
		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objValidateData;

	}




// !SCHEDULE PROCESSING


	public function cron_schedule_data_processing( )
	{

		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=cron_schedule_data_processing
		

		$className = "Nsd_schedulingController";
		$functionName = "cron_schedule_data_processing";
		
		$db = JFactory::getDBO();

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}		

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->agent_process_send_to			= $params->get('agent_process_send_to');
		$paramsComponent->agent_process_email_send_from	= $params->get('agent_process_email_send_from');
		$paramsComponent->agent_process_send_from_name	= $params->get('agent_process_send_from_name');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "SELECT * from htc_os_schedule_data_cron_log order by id desc limit 1";
		$db->setQuery($query);
		$objLogCron = $db->loadObject();

		if ( $objLogCron->state == "1" )
		{
			$logMessage = "INSIDE | ".$className." | ".$functionName." | cron_schedule_data_processing is already running. The process started at ".$objLogCron->start_datetime;	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		


			$body = "";
			$body .= "<br>The cron_schedule_data_processing process is still running. The process started at ".$objLogCron->start_datetime."<br>";
			$body .= "<br><br>The last note is: ".$objLogCron->notes."<br>";
			$body .= "<br><br>Click here to reset: ".JURI::root()."index.php?option=com_nsd_scheduling&task=cron_schedule_data_processing_reset<br>";
			
			$body .= "<br><br>Click here to reset: https://tracking.shipnsd.com/index.php?option=com_nsd_scheduling&task=cron_schedule_data_processing_reset<br>";




			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = $paramsComponent->agent_process_send_to;
			$objSendEmailMessage->email_from = $paramsComponent->agent_process_email_send_from;
			$objSendEmailMessage->email_from_name  = $paramsComponent->agent_process_send_from_name;
			$objSendEmailMessage->email_subject = "ALERT | ".$className." | ".$functionName." | Cron Still Running";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );



			$logMessage = "INSIDE | ".$className." | ".$functionName." | Alert sent";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
			
					
		}
		else
		{


			$datetime = date("Y-m-d H:i:s");
			$datetime_gmt = gmdate("Y-m-d H:i:s");
			$timestamp = date("YmdHisD").microtime(true);

	
	        $objData = new stdClass();
	        $objData->state = 1;
			$objData->start_datetime_gmt = $datetime_gmt;
			$objData->start_datetime = $datetime;
			$objData->notes = "Start data processing of files.";
			$result = JFactory::getDbo()->insertObject('htc_os_schedule_data_cron_log', $objData);
	        $cronID = $db->insertid();




			$returnScheduleCollector = Nsd_schedulingController::schedule_collector( $cronID );



			$datetime = date("Y-m-d H:i:s");
			$datetime_gmt = gmdate("Y-m-d H:i:s");
			$timestamp = date("YmdHisD").microtime(true);	
	
			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
	        $objData = new stdClass();
	        $objData->id = $cronID;
	        $objData->state = 0;
			$objData->end_datetime_gmt = $datetime_gmt;
			$objData->end_datetime = $datetime;
	        $objData->total_time = number_format($stamp_total_micro,2);
	        $objData->notes = "Finished data processing all files.";
	        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 


		}
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function cron_schedule_data_processing_reset( )
	{

		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=cron_schedule_data_processing_reset
		

		$className = "Nsd_schedulingController";
		$functionName = "cron_schedule_data_processing_reset";
		
		$db = JFactory::getDBO();

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}		

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->agent_process_send_to			= $params->get('agent_process_send_to');
		$paramsComponent->agent_process_email_send_from	= $params->get('agent_process_email_send_from');
		$paramsComponent->agent_process_send_from_name	= $params->get('agent_process_send_from_name');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "SELECT * from htc_os_schedule_data_cron_log order by id desc limit 1";
		$db->setQuery($query);
		$objLogCron = $db->loadObject();

		if ( $objLogCron->state == "1" )
		{
			$logMessage = "INSIDE | ".$className." | ".$functionName." | Cron state is 1.  Begin reset.";	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		



			$objDataUpdate = new stdClass();
			$objDataUpdate->id = $objLogCron->id;
			$objDataUpdate->state = "0";
			$result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objDataUpdate, 'id');          





			$body = "";
			$body .= "<br>The cron_schedule_data_processing process has been reset.";
			$body .= "<br><br>Click here to rerun: ".JURI::root()."index.php?option=com_nsd_scheduling&task=cron_schedule_data_processing<br>";

			$body .= "<br><br>Click here to rerun: https://tracking.shipnsd.com/index.php?option=com_nsd_scheduling&task=cron_schedule_data_processing<br>";
			

			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = $paramsComponent->agent_process_send_to;
			$objSendEmailMessage->email_from = $paramsComponent->agent_process_email_send_from;
			$objSendEmailMessage->email_from_name  = $paramsComponent->agent_process_send_from_name;
			$objSendEmailMessage->email_subject = "ALERT | ".$className." | ".$functionName." | Cron Reset";
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );



			$logMessage = "INSIDE | ".$className." | ".$functionName." | Cron state was reset to  0. Alert sent";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
			
					
		}
		else
		{

			$logMessage = "INSIDE | ".$className." | ".$functionName." | Cron state was 0. Alert sent";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		}
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public static function schedule_collector( $cronID )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=schedule_collector
		

		$className = "Nsd_schedulingController";
		$functionName = "schedule_collector";
		
		$db = JFactory::getDBO();

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();

		$paramsComponent->agent_process_send_to			= $params->get('agent_process_send_to');
		$paramsComponent->agent_process_email_send_from	= $params->get('agent_process_email_send_from');
		$paramsComponent->agent_process_send_from_name	= $params->get('agent_process_send_from_name');
        
		$paramsComponent->directoryInbound		= $params->get('directory_inbound');
		$paramsComponent->directoryProcessed	= $params->get('directory_processed');
		$paramsComponent->directorySkipped		= $params->get('directory_skipped');

		$paramsComponent->pathInbound			=  JPATH_SITE."/".$paramsComponent->directoryInbound."/";
		$paramsComponent->pathProcessed			=  JPATH_SITE."/".$paramsComponent->directoryProcessed."/";
		$paramsComponent->pathSkipped			=  JPATH_SITE."/".$paramsComponent->directorySkipped."/";


		$paramsComponent->default_agent_range_miles						= $params->get('default_agent_range_miles');
		$paramsComponent->default_agent_max_deliveries_per_day			= $params->get('default_agent_max_deliveries_per_day');

		$paramsComponent->default_client_max_deliveries_per_day			= $params->get('default_client_max_deliveries_per_day');
		
		$paramsComponent->default_intervals_delivery_interval_begin		= $params->get('default_intervals_delivery_interval_begin');
		$paramsComponent->default_intervals_delivery_interval_end		= $params->get('default_intervals_delivery_interval_end');
		$paramsComponent->default_intervals_delivery_interval_cutoff	= $params->get('default_intervals_delivery_interval_cutoff');


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$arrFiles = JFolder::files( $paramsComponent->pathInbound );
		
		$numberOfRecords = 4;
		
		$query = "select a.* from htc_os_schedule_data_log a where flag_data_validation = '1' and flag_data_process = '0' order by id desc limit ".$numberOfRecords;
		$db->setQuery($query);
		$objListFiles = $db->loadObjectList();

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objListFiles: ".print_r($objListFiles, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
		


		if ( count($objListFiles) > 0 )
		{

			foreach( $objListFiles as $oFile )
			{

				$datetime = date("Y-m-d H:i:s");
				$datetime_gmt = gmdate("Y-m-d H:i:s");
				$timestamp = date("YmdHisD").microtime(true);

				$objFile = new stdClass();
				$objFile->fileName = $oFile->file_name;
				$objFile->cronID = $cronID;

				#replace space with no space
				$fileNameClean = str_replace( " ", "", $objFile->fileName );


				
				$objFile->fileNameProcessed = $timestamp."-".$fileNameClean;
				
				$objFile->fileFullServerPath = $paramsComponent->pathInbound.$objFile->fileName;
				$objFile->fileProcessedFullServerPath = $paramsComponent->pathProcessed.$objFile->fileNameProcessed;
				$objFile->fileSkippedFullServerPath = $paramsComponent->pathSkipped.$objFile->fileName;



				#does agent exist?  get agent info or creat agent and get agent info


		        $query = "select a.* from htc_os_agents a where a.name = '".$oFile->agent_name."' and a.state = '1' ";
		
		        $db->setQuery($query);
		        
		        $objAgent = $db->loadObject();
		
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		        
		        if ( !$objAgent || count($objAgent) == 0 )
		        {

					$objDataInsert = new stdClass();
					$objDataInsert->name = $oFile->agent_name;
					$objDataInsert->zipcode = "";
					$objDataInsert->latitude = "";
					$objDataInsert->longitude = "";
					$objDataInsert->state = "1";
					$objDataInsert->range_miles = $paramsComponent->default_agent_range_miles;
					$objDataInsert->max_deliveries_per_day = "";
					$result = JFactory::getDbo()->insertObject('htc_os_agents', $objDataInsert);
					$agent_id = $db->insertid();

					$objFile->agent_id = $agent_id;
					$objFile->agent_name = $oFile->agent_name;
				}
				else
				{
					$objFile->agent_id = $objAgent->id;
					$objFile->agent_name = $objAgent->name;
					
				}				



				#does agent interval exist?  get interval information or create agent interval information
				
		        $query = "select a.* from htc_os_delivery_intervals a where a.agent_id = '".$objFile->agent_id."' ";
		
		        $db->setQuery($query);
		        
		        $objIntervals = $db->loadObject();
		
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objIntervals: ".print_r($objIntervals, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			
	
		        if ( !$objIntervals || count($objIntervals) == 0 )
		        {

					$objDataInsert = new stdClass();
					$objDataInsert->client_id = "";
					$objDataInsert->agent_id = $objFile->agent_id;
					$objDataInsert->delivery_interval_begin = $paramsComponent->default_intervals_delivery_interval_begin;
					$objDataInsert->delivery_interval_end = $paramsComponent->default_intervals_delivery_interval_end;
					$objDataInsert->delivery_interval_cutoff = $paramsComponent->default_intervals_delivery_interval_cutoff;
					$result = JFactory::getDbo()->insertObject('htc_os_delivery_intervals', $objDataInsert);
					$intervalID = $db->insertid();	

					$logMessage = "INSIDE | ".$className." | ".$functionName." | INTERVAL ";
					$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			        
		        }				
	
				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
				$datetime = date("Y-m-d H:i:s");
				$datetime_gmt = gmdate("Y-m-d H:i:s");
				$timestamp = date("YmdHisD").microtime(true);

		        $objData = new stdClass();
		        $objData->id = $objFile->cronID;
		       
				$objData->end_datetime_gmt = $datetime_gmt;
				$objData->end_datetime = $datetime;
				$objData->notes = "Inside file: ".$className." | ".$functionName." | ".$objFile->fileName." | beacon 1 | before schedule_parse_save ";
		        $objData->total_time = number_format($stamp_total_micro,2);
		        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 




				$objFile = Nsd_schedulingController::schedule_parse_save( $objFile );



				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
				$datetime = date("Y-m-d H:i:s");
				$datetime_gmt = gmdate("Y-m-d H:i:s");
				$timestamp = date("YmdHisD").microtime(true);

		        $objData = new stdClass();
		        $objData->id = $objFile->cronID;
		       
				$objData->end_datetime_gmt = $datetime_gmt;
				$objData->end_datetime = $datetime;
				$objData->notes = "Inside file: ".$className." | ".$functionName." | ".$objFile->fileName." | beacon 2 | after schedule_parse_save ";
		        $objData->total_time = number_format($stamp_total_micro,2);
		        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 



				if ( $objFile->errorState == "0" && ( $objFile->stage == "File Processed" || $objFile->stage == "File Size Zero" ) )
				{

					if ( JFile::move( $objFile->fileFullServerPath, $objFile->fileProcessedFullServerPath ) )
					{
						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->fileName." moved/renamed ".$objFile->fileProcessedFullServerPath;
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						$additionalBody = "".$objFile->fileName." moved/renamed successfully.";
					}
					else
					{
						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->fileName." NOT moved/renamed ".$objFile->fileProcessedFullServerPath;
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						$additionalBody = "".$objFile->fileName." NOT moved/renamed ".$objFile->fileProcessedFullServerPath;
						$additionalBody .= "<br><br>objFile: ".print_r($objFile, true)."<br>";
					}


					$body = "";
					$body .= "<br>The file, ".$objFile->fileName." for ".$objFile->agent_name." processed successfully.<br>";
					$body .= $additionalBody;
					
					
		
					$objSendEmailMessage = new stdClass();
					$objSendEmailMessage->email_to = $paramsComponent->agent_process_send_to;
					$objSendEmailMessage->email_from = $paramsComponent->agent_process_email_send_from;
					$objSendEmailMessage->email_from_name  = $paramsComponent->agent_process_send_from_name;
					$objSendEmailMessage->email_subject = "SUCCESS | ".$className." | ".$functionName." | The file, ".$objFile->fileName." for ".$objFile->agent_name." processed successfully.";
					$objSendEmailMessage->email_body = $body;
					$objSendEmailMessage->email_category = "scheduling support";
			
					$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );					




				}
				else
				{

					if ( $objFile->stage != "File Processed - Error")
					{

						$logMessage = "INSIDE | ".$className." | ".$functionName." | ".$objFile->fileName." Error ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
						$body = "";
						$body .= "<br>The file, ".$objFile->fileName." for ".$objFile->agent_name." did NOT process successfully.<br>";
						$body .= "<br><br>The last note is: ".$objLogCron->notes."<br>";
						
			
						$objSendEmailMessage = new stdClass();
						$objSendEmailMessage->email_to = $paramsComponent->agent_process_send_to;
						$objSendEmailMessage->email_from = $paramsComponent->agent_process_email_send_from;
						$objSendEmailMessage->email_from_name  = $paramsComponent->agent_process_send_from_name;
						$objSendEmailMessage->email_subject = "ALERT | ".$className." | ".$functionName." | The file, ".$objFile->fileName." for ".$objFile->agent_name." did NOT process successfully.";
						$objSendEmailMessage->email_body = $body;
						$objSendEmailMessage->email_category = "scheduling support";
				
						$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );
					
					}					
	
				}
	
				$last_status = "File Processed";

				$objDataUpdate = new stdClass();
				$objDataUpdate->id = $oFile->id;
				$objDataUpdate->file_name_processed = $objFile->fileNameProcessed;
				$objDataUpdate->flag_data_process = "1";
				$objDataUpdate->last_status = $last_status;
				$objDataUpdate->stamp_datetime = $datetime;
				$objDataUpdate->stamp_datetime_gmt = $datetime_gmt;

				$result = JFactory::getDbo()->updateObject('htc_os_schedule_data_log', $objDataUpdate, 'id'); 

			
			}
		
			$return = 1;
			
		}
		else
		{

			$logMessage = "INSIDE | ".$className." | ".$functionName." | No files to process";
			#$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			$return = 0;
		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | totalFiles: ".count($arrFiles)." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "END | ".$className." | ".$functionName." | totalFiles: ".count($arrFiles)." | seconds: ".number_format($stamp_total_micro,2);			
		#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $return;
	}	


	
	public static function schedule_parse_save( $objFile )
	{
		
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=schedule_parse_save



		$className = "Nsd_schedulingController";
		$functionName = "schedule_parse_save";
		
		$db = JFactory::getDBO();

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		
		if ( file_exists( $objFile->fileFullServerPath ) )
		{
		    # file exists
	
			$countRecords = 0;
		    
		    
		    $filesize = filesize( $objFile->fileFullServerPath  );
		    
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r filesize: ".print_r($filesize, true);	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			    
		    
		    
		    if ( $filesize > 0 )
		    {



				$query = "select data_lookup, data_replace from htc_os_lu_holiday_matrix where state = '1'";
				$db->setQuery($query);
				$arrHolidayMatrix = $db->loadObjectList();	
				
		
				$arrMatrixLookup = array();
				
				foreach( $arrHolidayMatrix as $row )
				{
					$arrMatrixLookup[$row->data_lookup] = $row->data_replace;
				}
		
				#$testMe = $arrMatrixLookup['Christmas'];
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r arrHolidayMatrix: ".print_r($arrHolidayMatrix, true);
				$logMessage .= "\n\r arrMatrixLookup: ".print_r($arrMatrixLookup, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }





				$inputFileType = 'Xlsx';
				$inputFileName = $objFile->fileFullServerPath;
		
		
				$arrSheetNames = array();
				$arrSheetNames[] = "geographicalAreas";
				$arrSheetNames[] = "schedules";
				$arrSheetNames[] = "slots";
				$arrSheetNames[] = "holidays";
		
		
		
				foreach ( $arrSheetNames as $sheetname )
				{

					$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
	
						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
						$datetime = date("Y-m-d H:i:s");
						$datetime_gmt = gmdate("Y-m-d H:i:s");
						$timestamp = date("YmdHisD").microtime(true);
	
				        $objData = new stdClass();
				        $objData->id = $objFile->cronID;
						$objData->end_datetime_gmt = $datetime_gmt;
						$objData->end_datetime = $datetime;
						$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | define reader";
				        $objData->total_time = number_format($stamp_total_micro,2);
				        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id');
				        
	
					
					$reader->setReadDataOnly(true);
			
	
						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
						$datetime = date("Y-m-d H:i:s");
						$datetime_gmt = gmdate("Y-m-d H:i:s");
						$timestamp = date("YmdHisD").microtime(true);
	
				        $objData = new stdClass();
				        $objData->id = $objFile->cronID;
						$objData->end_datetime_gmt = $datetime_gmt;
						$objData->end_datetime = $datetime;
						$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | reader->setReadDataOnly";
				        $objData->total_time = number_format($stamp_total_micro,2);
				        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 
						
	
					$reader->setLoadSheetsOnly($sheetname);


						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
						$datetime = date("Y-m-d H:i:s");
						$datetime_gmt = gmdate("Y-m-d H:i:s");
						$timestamp = date("YmdHisD").microtime(true);
	
				        $objData = new stdClass();
				        $objData->id = $objFile->cronID;
						$objData->end_datetime_gmt = $datetime_gmt;
						$objData->end_datetime = $datetime;
						$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | reader->setLoadSheetsOnly";
				        $objData->total_time = number_format($stamp_total_micro,2);
				        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 					
					
					
					$spreadsheet = $reader->load($inputFileName);

						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
						$datetime = date("Y-m-d H:i:s");
						$datetime_gmt = gmdate("Y-m-d H:i:s");
						$timestamp = date("YmdHisD").microtime(true);
	
				        $objData = new stdClass();
				        $objData->id = $objFile->cronID;
						$objData->end_datetime_gmt = $datetime_gmt;
						$objData->end_datetime = $datetime;
						$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | reader->setReadDataOnly";
				        $objData->total_time = number_format($stamp_total_micro,2);
				        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 					
					
					
					
					
					$sheetData = $spreadsheet->getActiveSheet()->toArray();		
		
						$stamp_end_micro = microtime(true);
						$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
						$datetime = date("Y-m-d H:i:s");
						$datetime_gmt = gmdate("Y-m-d H:i:s");
						$timestamp = date("YmdHisD").microtime(true);
	
				        $objData = new stdClass();
				        $objData->id = $objFile->cronID;
						$objData->end_datetime_gmt = $datetime_gmt;
						$objData->end_datetime = $datetime;
						$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | tab: ".$sheetname." | spreadsheet->getActiveSheet()->toArray()";
				        $objData->total_time = number_format($stamp_total_micro,2);
				        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 		
	
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r sheetData: ".print_r($sheetData, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
					switch( $sheetname )
					{
		
						case "geographicalAreas":


							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$datetime = date("Y-m-d H:i:s");
							$datetime_gmt = gmdate("Y-m-d H:i:s");
							$timestamp = date("YmdHisD").microtime(true);
		
					        $objData = new stdClass();
					        $objData->id = $objFile->cronID;
							$objData->end_datetime_gmt = $datetime_gmt;
							$objData->end_datetime = $datetime;
							$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | tab: ".$sheetname." | processing";
					        $objData->total_time = number_format($stamp_total_micro,2);
					        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 


							$countRow = 1;
							
							foreach( $sheetData as $arrData )
							{
		
								if ( $countRow > 1 && trim($arrData[0]) != "" )
								{
									
									$objDataInput = new stdClass();
									
									$objDataInput ->agent_id = $objFile->agent_id;
									$objDataInput ->geographical_area = $arrData[0];
									$objDataInput ->state = $arrData[1];
									$objDataInput ->city = $arrData[2];
									$objDataInput ->district = $arrData[3];
									$objDataInput ->minpostalcode = $arrData[4];
									$objDataInput ->maxpostalcode = $arrData[5];
									$objDataInput ->geocode = $arrData[6];
									$objDataInput ->subgeographica_areas = $arrData[7];
									$objDataInput ->is_root = $arrData[8];
									$result = JFactory::getDbo()->insertObject('htc_os_raw_geographical_areas', $objDataInput);
									$insertID = $db->insertid();
			
									$logMessage = "INSIDE | ".$className." | ".$functionName;
									$logMessage .= "\n\r objDataInput: ".print_r($objDataInput, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
								}
								
								$countRow += 1;
							}


			
						
							break;
		
		
						case "schedules":

							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$datetime = date("Y-m-d H:i:s");
							$datetime_gmt = gmdate("Y-m-d H:i:s");
							$timestamp = date("YmdHisD").microtime(true);
		
					        $objData = new stdClass();
					        $objData->id = $objFile->cronID;
							$objData->end_datetime_gmt = $datetime_gmt;
							$objData->end_datetime = $datetime;
							$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | tab: ".$sheetname." | processing";
					        $objData->total_time = number_format($stamp_total_micro,2);
					        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 
			
							$countRow = 1;
							
							foreach( $sheetData as $arrData )
							{
		
								if ( $countRow > 1 && trim($arrData[0]) != "" )
								{
									
									
									$objDataInput = new stdClass();
									
									$objDataInput ->agent_id = $objFile->agent_id;
									$objDataInput ->geographical_area = $arrData[0];
									$objDataInput ->schedule_name = $arrData[1];
									$objDataInput ->valid_from = $arrData[2];
									$objDataInput ->valid_until = $arrData[3];
									$objDataInput ->holidays = $arrData[4];
									$result = JFactory::getDbo()->insertObject('htc_os_raw_schedules', $objDataInput);
									$insertID = $db->insertid();
			
									$logMessage = "INSIDE | ".$className." | ".$functionName;
									$logMessage .= "\n\r objDataInput: ".print_r($objDataInput, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
								}
								
								$countRow += 1;
							}
			
						
							break;
		
		
						case "slots":

							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$datetime = date("Y-m-d H:i:s");
							$datetime_gmt = gmdate("Y-m-d H:i:s");
							$timestamp = date("YmdHisD").microtime(true);
		
					        $objData = new stdClass();
					        $objData->id = $objFile->cronID;
							$objData->end_datetime_gmt = $datetime_gmt;
							$objData->end_datetime = $datetime;
							$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | tab: ".$sheetname." | processing";
					        $objData->total_time = number_format($stamp_total_micro,2);
					        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 

			
							$countRow = 1;
							
							foreach( $sheetData as $arrData )
							{
		
								if ( $countRow > 1 && trim($arrData[0]) != "" )
								{


									$arrFind = array( ";", " " );
									$arrReplace = array( ",", "" );
									$slot_days = trim( strtoupper( str_replace( $arrFind, $arrReplace, $arrData[2] ) ) );	

									
									$objDataInput = new stdClass();
									
									$objDataInput ->agent_id = $objFile->agent_id;
									$objDataInput ->schedule_name = $arrData[0];
									$objDataInput ->time_slot = $arrData[1];
									$objDataInput ->slot_days = $slot_days;
									#$objDataInput ->slot_days = strtoupper( $arrData[2] );
									$objDataInput ->capacity = $arrData[3];
									$objDataInput ->services = $arrData[4];
									$objDataInput ->attributes = $arrData[5];
									$objDataInput ->monday = $arrData[6];
									$objDataInput ->tuesday = $arrData[7];
									$objDataInput ->wednesday = $arrData[8];
									$objDataInput ->thursday = $arrData[9];
									$objDataInput ->friday = $arrData[10];
									$objDataInput ->saturday = $arrData[11];
									$result = JFactory::getDbo()->insertObject('htc_os_raw_slots', $objDataInput);
									$insertID = $db->insertid();
			
									$logMessage = "INSIDE | ".$className." | ".$functionName;
									$logMessage .= "\n\r objDataInput: ".print_r($objDataInput, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
								}
								
								$countRow += 1;
							}
			
						
							break;
		
		
						case "holidays":

							$stamp_end_micro = microtime(true);
							$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
							$datetime = date("Y-m-d H:i:s");
							$datetime_gmt = gmdate("Y-m-d H:i:s");
							$timestamp = date("YmdHisD").microtime(true);
		
					        $objData = new stdClass();
					        $objData->id = $objFile->cronID;
							$objData->end_datetime_gmt = $datetime_gmt;
							$objData->end_datetime = $datetime;
							$objData->notes = "Inside ".$className." | ".$functionName." | ".$objFile->fileName." | tab: ".$sheetname." | processing";
					        $objData->total_time = number_format($stamp_total_micro,2);
					        $result = JFactory::getDbo()->updateObject('htc_os_schedule_data_cron_log', $objData, 'id'); 

							$countRow = 1;
							
							foreach( $sheetData as $arrData )
							{
		
								if ( $countRow > 1 && trim($arrData[0]) != "" )
								{
									
									$objDataInput = new stdClass();
			
									$objDataInput->agent_id = $objFile->agent_id;
									$objDataInput->holiday_name = $arrData[0];
									$objDataInput->start = $arrData[1];
									$objDataInput->end = $arrData[2];
									$objDataInput->holiday_name_normalized = $arrMatrixLookup[$arrData[0]];
									$result = JFactory::getDbo()->insertObject('htc_os_raw_holidays', $objDataInput);
									$insertID = $db->insertid();
			
									$logMessage = "INSIDE | ".$className." | ".$functionName;
									$logMessage .= "\n\r objDataInput: ".print_r($objDataInput, true);
									$logMessage .= "\n\r ";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									
								}
								
								$countRow += 1;
							}
						
							break;
						
					}
					
					
					$spreadsheet->disconnectWorksheets(); 
					$spreadsheet->garbageCollect();
					unset($spreadsheet); 	
					unset($reader); 
					
					
					
				}


				
				#geographical_areas
				
				#remove data from table for this agent
		        $query = "delete from htc_os_lu_geographical_areas where agent_id = '".$objFile->agent_id."'";
		        $db->setQuery($query);
		        $db->query() ;				
				

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				


				
				$query = "select a.* from htc_os_raw_geographical_areas a where agent_id = '".$objFile->agent_id."'";
				$db->setQuery($query);
				$objListGeaographicalAreas = $db->loadObjectList();

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objListGeaographicalAreas: ".print_r($objListGeaographicalAreas, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
				
				
				foreach ( $objListGeaographicalAreas as $ga )
				{
				
					$minpostalcode = $ga->minpostalcode;
					$maxpostalcode = $ga->maxpostalcode;				
				
				
					if ( $minpostalcode != $maxpostalcode )
					{

						$postalcode = $minpostalcode;
						
						while( $postalcode <= $maxpostalcode )
						{
							
							$objDataInsert = new stdClass();
							$objDataInsert->agent_id = $objFile->agent_id;
							$objDataInsert->geographical_area = $ga->geographical_area;
							$objDataInsert->postalcode = str_pad( $postalcode, 5, "0", STR_PAD_LEFT );
							$result = JFactory::getDbo()->insertObject('htc_os_lu_geographical_areas', $objDataInsert);
							$gaID = $db->insertid();
							
							$postalcode += 1;
							
						}
						
					}
					else
					{

						$objDataInsert = new stdClass();
						$objDataInsert->agent_id = $objFile->agent_id;
						$objDataInsert->geographical_area = $ga->geographical_area;
						$objDataInsert->postalcode = str_pad( $minpostalcode, 5, "0", STR_PAD_LEFT );
						$result = JFactory::getDbo()->insertObject('htc_os_lu_geographical_areas', $objDataInsert);
						$gaID = $db->insertid();

					}
				
				
				}
				
				
			
				
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				








				#holidays
				
				#get all rows from htc_os_raw_schedules
				
				$query = "select a.* from htc_os_raw_schedules a where agent_id = '".$objFile->agent_id."'";
				$db->setQuery($query);
				$objListSchedules = $db->loadObjectList();

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objListSchedules: ".print_r($objListSchedules, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	






				
				foreach ( $objListSchedules as $schedule )
				{
					$strHolidays = $schedule->holidays;
					
					$arrHolidays = explode( "|", $strHolidays );

					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r strHolidays: ".print_r($strHolidays, true);
					$logMessage .= "\n\r arrHolidays: ".print_r($arrHolidays, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


					
					foreach ( $arrHolidays as $holiday )
					{
						$holidayNameNormalized = $arrMatrixLookup[$holiday];
						
						$query = "select b.start from htc_os_raw_holidays b where b.agent_id = '".$objFile->agent_id."' and b.holiday_name_normalized = '".$holidayNameNormalized."' ";
						$db->setQuery($query);
						$dateRaw = $db->loadResult();
						
						$timestamp = strtotime( $dateRaw );
						
						$holiday_date = date( 'Y-m-d', $timestamp );
						
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r query: ".print_r($query, true);
						$logMessage .= "\n\r dateRaw: ".print_r($dateRaw, true);
						$logMessage .= "\n\r timestamp: ".print_r($timestamp, true);
						$logMessage .= "\n\r holiday_date: ".print_r($holiday_date, true);				
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }						
						
						#insert into interim table
						
						$objDataInsert = new stdClass();
						$objDataInsert->agent_id = $objFile->agent_id;
						$objDataInsert->geographical_area = $schedule->geographical_area;
						$objDataInsert->holiday_name = $holidayNameNormalized;
						$objDataInsert->holiday_date = date( 'Y-m-d', $timestamp );
						$result = JFactory::getDbo()->insertObject('htc_os_interim_holidays', $objDataInsert);
						$activityID = $db->insertid();


						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }						
	
						
					}
					
					
				}					
				
				
				#remove data from table for this agent
		        $query = "delete from htc_os_lu_holidays where agent_id = '".$objFile->agent_id."'";
		        $db->setQuery($query);
		        $db->query() ;				
				

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				

				
				
		        $query = "insert into htc_os_lu_holidays  
		        ( 
					agent_id,
					geographical_area,
					holiday_name,
					holiday_date
		        ) 
		        select 
					agent_id,
					geographical_area,
					holiday_name,
					holiday_date
				from htc_os_interim_holidays z
				where		        
		        z.agent_id = '".$objFile->agent_id."'";
		        $db->setQuery($query);
		        $db->query() ;				
				
				
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				
				
				
				
				
				


				#slots and windows


				$query = "select a.* from htc_os_raw_slots a where agent_id = '".$objFile->agent_id."'";
				$db->setQuery($query);
				$objListSlots = $db->loadObjectList();

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objListSlots: ".print_r($objListSlots, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	



				foreach ( $objListSlots as $slots )
				{
					$strSlotDays = $slots->slot_days;
					
					
					#if strSlotDays as -, get lookup value
					if (strpos($strSlotDays,"-") !== false)
					{
						$query = "select b.value from htc_os_lu_days b where b.key = '".$strSlotDays."' and b.state = '1' ";
						$db->setQuery($query);
						$strSlotDays = $db->loadResult();						
					}
					
					$arrSlotDays = explode(",", $strSlotDays);
					
						$strTimeSlot = $slots->time_slot;
						$arrTimeSlot = explode( "-", $strTimeSlot );
						$start_time = $arrTimeSlot[0];
						$end_time = $arrTimeSlot[1];
	
	
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r strSlotDays: ".print_r($strSlotDays, true);
						$logMessage .= "\n\r arrSlotDays: ".print_r($arrSlotDays, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


						$query = "select b.geographical_area from htc_os_raw_schedules b where b.schedule_name = '".$slots->schedule_name."'";
						$db->setQuery($query);
						$geographical_area = $db->loadResult();	


						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r query: ".print_r($query, true);
						$logMessage .= "\n\r geographical_area: ".print_r($geographical_area, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


						foreach ( $arrSlotDays as $day )
						{

							$arrConvertDays = array(
								0 => "SU",
								1 => "M",
								2 => "T",
								3 => "W",
								4 => "TH",
								5 => "F",
								6 => "SA"
							);
	
							$dayPHP = array_search($day, $arrConvertDays);
	

	
	
	
							$objDataInsert = new stdClass();
							$objDataInsert->agent_id = $objFile->agent_id;
							$objDataInsert->geographical_area = $geographical_area;
							$objDataInsert->schedule_name = $slots->schedule_name;
							$objDataInsert->start_time = $start_time;
							$objDataInsert->end_time = $end_time;
							$objDataInsert->day_of_week = $day;
							$objDataInsert->day_of_week_php = $dayPHP;
							$objDataInsert->slots = $slots->capacity;
							
							$result = JFactory::getDbo()->insertObject('htc_os_interim_slots', $objDataInsert);
							$activityID = $db->insertid();
						}

				}



				#remove data from table for this agent
		        $query = "delete from htc_os_lu_windows where agent_id = '".$objFile->agent_id."'";
		        $db->setQuery($query);
		        $db->query() ;				
				

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		


				$query = "select distinct geographical_area, schedule_name from htc_os_interim_slots a where agent_id = '".$objFile->agent_id."'";
				$db->setQuery($query);
				$objListGeoAreaSchedName = $db->loadObjectList();

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r arrColumnArea: ".print_r($arrColumnArea, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				

				foreach ( $objListGeoAreaSchedName as $objGeoAreaSchedName )
				{

					for ( $x=0; $x < 7; $x++ )
					{

						$query = "select sum(slots) as 'totalSlots' from htc_os_interim_slots a where agent_id = '".$objFile->agent_id."' and geographical_area = '".$objGeoAreaSchedName->geographical_area."' and day_of_week_php = '".$x."' ";
						$db->setQuery($query);
						$sumSlots = $db->loadResult();
	
						$logMessage = "INSIDE | ".$className." | ".$functionName;
						$logMessage .= "\n\r query: ".print_r($query, true);
						$logMessage .= "\n\r sumSlots: ".print_r($sumSlots, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
					
						if ( $sumSlots > 0 )
						{

						$arrConvertDays = array(
							0 => "SU",
							1 => "M",
							2 => "T",
							3 => "W",
							4 => "TH",
							5 => "F",
							6 => "SA"
						);

	
							$objDataInsert = new stdClass();
							$objDataInsert->agent_id = $objFile->agent_id;
							$objDataInsert->geographical_area = $objGeoAreaSchedName->geographical_area;
							$objDataInsert->schedule_name = $objGeoAreaSchedName->schedule_name;
							$objDataInsert->day_of_week = $arrConvertDays[$x];
							$objDataInsert->day_of_week_php = $x;
							$objDataInsert->max_slots = $sumSlots;
							
							$result = JFactory::getDbo()->insertObject('htc_os_lu_windows', $objDataInsert);
							$activityID = $db->insertid();						
							
							
						}

					}

					
				}







				# htc_os_delivery_windows
				
				
				#remove data from table for this agent
		        $query = "delete from htc_os_delivery_windows where agent_id = '".$objFile->agent_id."'";
		        $db->setQuery($query);
		        $db->query() ;				
				
		        $query = "select distinct geographical_area from htc_os_raw_geographical_areas where geographical_area != ''";
		        $db->setQuery($query);
		        $arrGeographicalAreas = $db->loadColumn();
				
				
						
					$arrWindows = array();
					
					$arrWindows[] =  array( "day_of_week"=>"1", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );
					
					$arrWindows[] =  array( "day_of_week"=>"2", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );

					$arrWindows[] =  array( "day_of_week"=>"3", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );

					$arrWindows[] =  array( "day_of_week"=>"4", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );
			
					$arrWindows[] =  array( "day_of_week"=>"5", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );

					$arrWindows[] =  array( "day_of_week"=>"6", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );


					foreach( $arrWindows as $window )
					{


						foreach ($arrGeographicalAreas as $geoArea )
						{

					        $query = "select ordering from htc_os_delivery_windows";
					        $db->setQuery($query);
					        $arrOrdering = $db->loadColumn() ;			
					
							$nextOrderVal = max($arrOrdering) + 1;
	
	
							$objDataInsert = new stdClass();
							$objDataInsert->agent_id = $objFile->agent_id;
							$objDataInsert->client_id = "";
							$objDataInsert->geographical_area = $geoArea;
							$objDataInsert->day_of_week = $window["day_of_week"];
							$objDataInsert->window_name = $window["window_name"];
							$objDataInsert->window_start_time = $window["window_start_time"];
							$objDataInsert->window_end_time = $window["window_end_time"];
							$objDataInsert->max_deliveries = $window["max_deliveries"];
							$objDataInsert->ordering = $nextOrderVal;
							$objDataInsert->state = "1";
							$result = JFactory::getDbo()->insertObject('htc_os_delivery_windows', $objDataInsert);
							$windowID = $db->insertid();	
	
							$logMessage = "INSIDE | ".$className." | ".$functionName." | WINDOW ";
							$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
						}
						
					}				







				
				
				#clean up
				
				$arrTables = array();
				$arrTables[] = "htc_os_raw_geographical_areas";
				$arrTables[] = "htc_os_raw_holidays";
				$arrTables[] = "htc_os_raw_schedules";
				$arrTables[] = "htc_os_raw_slots";
				$arrTables[] = "htc_os_interim_holidays";
				$arrTables[] = "htc_os_interim_slots";
				
				foreach ( $arrTables as $table ) 
				{

					
			        
			        $query = "truncate table $table";
			        $db->setQuery($query);
			        $db->query() ;				
					
	
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }					
				}
				
				


	
				$objFile->stage = "File Processed";
				$objFile->errorState = "0";
				$objFile->countRecords = $countRecords;
				    
		    }
		    else
		    {

				$objFile->stage = "File Size Zero";
				$objFile->errorState = "1";
				$objFile->countRecords = 0;
			    
		    }
		    



		}	
		else 
		{
		   #file does not exist
		    
		    $errMsg = "The upload file does not exist.";
		    
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r errMsg: ".print_r($errMsg, true);	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		    return $errMsg;
		    
		    $objFile->stage = "File Processed - Error";
		    $objFile->errorState = "1";
		    $objFile->errorMessage = $errMsg;

		}


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objFile;
	}	
	
	
	
	
	
	
	
	
	
	
// !depricated
	public static function schedule_check_setup_status( $objFile ) #depricated
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=schedule_check_setup_status


		$className = "Nsd_schedulingController";
		$functionName = "schedule_check_setup_status";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$logMessage = "INSIDE | ".$className." | ".$functionName." | object ";
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




		$objAgentComplete = new stdClass();
		$objAgentComplete->flag_complete = 1;



		#get Agent Name
		
		$objWork = new stdClass();
		
		$objWork->fileName = $objFile->fileName;

		$arrFind = array( " ", "-" );
		$arrReplace = array( "_", "_" );

		$objWork->fileNameClean = str_replace( $arrFind, $arrReplace, $objWork->fileName );
		
		$arrfileName = explode( "_", $objWork->fileNameClean );
		
		$objWork->arrfileName = $arrfileName;
		
		$arrfileName = array_reverse( $arrfileName );
		
		$objWork->arrfileNameRev = $arrfileName;
		
		$objWork->agentName = strstr($arrfileName[0], '.', true); 




		# is active in agent table
		
        $query = "select a.* from htc_os_agents a where a.name = '".$objWork->agentName."' and a.state = '1' ";

        $db->setQuery($query);
        
        $objAgent = $db->loadObject();

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objAgent: ".print_r($objAgent, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

        
        if ( !$objAgent || count($objAgent) == 0 )
        {
			$objAgentComplete->flag_complete = 0;
			$objAgentComplete->error_code = "1";
			$objAgentComplete->error_message = "Agent not complete in htc_os_agents.";
			$objAgentComplete->agent_id = "";
			$objAgentComplete->agent_name = $objWork->agentName;
	        
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

	        
        }
		else
		{

			# has a record in  htc_os_delivery_intervals table
	        $query = "select a.* from htc_os_delivery_intervals a where a.agent_id = '".$objAgent->id."' ";
	
	        $db->setQuery($query);
	        
	        $objIntervals = $db->loadObject();
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objIntervals: ".print_r($objIntervals, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
		

	        if ( !$objIntervals || count($objIntervals) == 0 )
	        {
				$objAgentComplete->flag_complete = 0;
				$objAgentComplete->error_code = "3";
				$objAgentComplete->error_message = "Agent not complete in htc_os_delivery_intervals.";
				$objAgentComplete->agent_id = $objAgent->id;
				$objAgentComplete->agent_name = $objAgent->name;
		        
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		        
	        }
			else
			{


				# has a record in  htc_os_delivery_windows table
		        $query = "select a.* from htc_os_delivery_windows a where a.agent_id = '".$objAgent->id."' and a.state = '1' ";
		
		        $db->setQuery($query);
		        
		        $objWindows = $db->loadObject();
		
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objWindows: ".print_r($objWindows, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
			
		        if ( !$objWindows || count($objWindows) == 0 )
		        {
					$objAgentComplete->flag_complete = 0;
					$objAgentComplete->error_code = "4";
					$objAgentComplete->error_message = "Agent not complete in htc_os_delivery_windows.";
					$objAgentComplete->agent_id = $objAgent->id;
					$objAgentComplete->agent_name = $objAgent->name;
			        
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			        
		        }
				else
				{
					$objAgentComplete->flag_complete = 1;
					$objAgentComplete->error_code = "0";
					$objAgentComplete->error_message = "";
					$objAgentComplete->agent_id = $objAgent->id;
					$objAgentComplete->agent_name = $objAgent->name;


					$logMessage = "INSIDE | ".$className." | ".$functionName." | beacon 77";
					$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				}
			
			}
		
		
		}        
		

		$logMessage = "INSIDE | ".$className." | ".$functionName." | beacon end";;
		$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		return $objAgentComplete;
		
	}	



	public static function schedule_validate_setup( $objFile ) #depricated
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=schedule_validate_setup



		$className = "Nsd_schedulingController";
		$functionName = "schedule_validate_setup";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	


        $app           							= JFactory::getApplication();
        $params         						= $app->getParams();
		$default_agent_range_miles				= $params->get('default_agent_range_miles');
		$default_agent_max_deliveries_per_day	= $params->get('default_agent_max_deliveries_per_day');

		$default_client_max_deliveries_per_day	= $params->get('default_client_max_deliveries_per_day');
		
		$default_intervals_delivery_interval_begin		= $params->get('default_intervals_delivery_interval_begin');
		$default_intervals_delivery_interval_end		= $params->get('default_intervals_delivery_interval_end');
		$default_intervals_delivery_interval_cutoff		= $params->get('default_intervals_delivery_interval_cutoff');

		$default_windows_by_client	= $params->get('default_windows_by_client');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$logMessage = "INSIDE | ".$className." | ".$functionName." | object ";
		$logMessage .= "\n\r objFile: ".print_r($objFile, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


	

		$objValidateSetup = new stdClass();


		#check if agent complete
		$objAgentComplete = Nsd_schedulingController::schedule_check_setup_status( $objFile );

		$logMessage = "INSIDE | ".$className." | ".$functionName." | beacon after Nsd_schedulingController::schedule_check_setup_status call";
		$logMessage .= "\n\r objAgentComplete: ".print_r($objAgentComplete, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
		
		if ( $objAgentComplete->flag_complete == "0" )
		{
			$objValidateSetup->flag_complete = "0";

			switch( $objAgentComplete->error_code )
			{
				
				case "1":
				
					#missing agent
					
					#insert into agent table
					
					#$objGeocodeInput = new stdClass();
					#$objGeocodeInput->zipcode = $objOrder->care_of_pc;
					
					#$objGeocode = Nsd_schedulingController::getLatLng( $objGeocodeInput );
					
					
					$objDataInsert = new stdClass();
					$objDataInsert->name = $objAgentComplete->agent_name;
					$objDataInsert->zipcode = "";
					$objDataInsert->latitude = "";
					$objDataInsert->longitude = "";
					$objDataInsert->state = "1";
					$objDataInsert->range_miles = $default_agent_range_miles;
					$objDataInsert->max_deliveries_per_day = "";
					$result = JFactory::getDbo()->insertObject('htc_os_agents', $objDataInsert);
					

					$logMessage = "INSIDE | ".$className." | ".$functionName." | AGENT ";
					$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
					Nsd_schedulingController::schedule_validate_setup( $objFile );
					
					break;


					
				case "2":
					#missing client

			        $query = "select a.* from htc_os_agents a where a.name = '".$objAgentComplete->agent_name."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objAgentLU = $db->loadObject();



			        $query = "select a.* from htc_os_lu_clients a where a.customer_account_code = '".$objOrder->customer_account_code."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objClientsLU = $db->loadObject();

					if ( is_object($objClientsLU) )
					{

						$objDataInsert = new stdClass();
						$objDataInsert->client_id = $objClientsLU->id;
						$objDataInsert->agent_id = $objAgentLU->id;
						$objDataInsert->max_deliveries_per_day = $default_client_max_deliveries_per_day;
						$objDataInsert->state = "1";
						$result = JFactory::getDbo()->insertObject('htc_os_clients', $objDataInsert);
						$objOrder->client_id = $db->insertid();	
						
						$logMessage = "INSIDE | ".$className." | ".$functionName." | CLIENT ";
						$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
					
						Nsd_schedulingController::schedule_validate_setup( $objFile );
						
					}
				
				
					break;



				case "3":
				
					#missing interval

			        $query = "select a.* from htc_os_agents a where a.name = '".$objAgentComplete->agent_name."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objAgentLU = $db->loadObject();

				
					$objDataInsert = new stdClass();
					$objDataInsert->client_id = "";
					$objDataInsert->agent_id = $objAgentLU->id;
					$objDataInsert->delivery_interval_begin = $default_intervals_delivery_interval_begin;
					$objDataInsert->delivery_interval_end = $default_intervals_delivery_interval_end;
					$objDataInsert->delivery_interval_cutoff = $default_intervals_delivery_interval_cutoff;
					$result = JFactory::getDbo()->insertObject('htc_os_delivery_intervals', $objDataInsert);
					$intervalID = $db->insertid();	

					$logMessage = "INSIDE | ".$className." | ".$functionName." | INTERVAL ";
					$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
					Nsd_schedulingController::schedule_validate_setup( $objFile );


				
					break;



				case "4":
					#missing windows


			        $query = "select a.* from htc_os_agents a where a.name = '".$objAgentComplete->agent_name."' and a.state = '1' ";
			
			        $db->setQuery($query);
			        
			        $objAgentLU = $db->loadObject();


					$query = "select ordering from htc_os_delivery_windows";
					$db->setQuery($query);
					$arrOrdering = $db->loadColumn() ;			
					
					$nextOrderVal = max($arrOrdering) + 1;

						
					$arrHD = array();
					
					$arrHD[] =  array( "day_of_week"=>"1", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );
					
					$arrHD[] =  array( "day_of_week"=>"2", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );

					$arrHD[] =  array( "day_of_week"=>"3", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );

					$arrHD[] =  array( "day_of_week"=>"4", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );
			
					$arrHD[] =  array( "day_of_week"=>"5", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );

					$arrHD[] =  array( "day_of_week"=>"6", "window_name"=>"All Day", "window_start_time"=>"06:00:00", "window_end_time"=>"18:00:00", "max_deliveries"=>"" );


					foreach( $arrHD as $window )
					{

				        $query = "select ordering from htc_os_delivery_windows";
				        $db->setQuery($query);
				        $arrOrdering = $db->loadColumn() ;			
				
						$nextOrderVal = max($arrOrdering) + 1;


						$objDataInsert = new stdClass();
						$objDataInsert->agent_id = $objAgentLU->id;
						$objDataInsert->client_id = "";
						$objDataInsert->day_of_week = $window["day_of_week"];
						$objDataInsert->window_name = $window["window_name"];
						$objDataInsert->window_start_time = $window["window_start_time"];
						$objDataInsert->window_end_time = $window["window_end_time"];
						$objDataInsert->max_deliveries = $window["max_deliveries"];
						$objDataInsert->ordering = $nextOrderVal;
						$objDataInsert->state = "1";
						$result = JFactory::getDbo()->insertObject('htc_os_delivery_windows', $objDataInsert);
						$windowID = $db->insertid();	

						$logMessage = "INSIDE | ".$className." | ".$functionName." | WINDOW ";
						$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

						
					}
						
				
					Nsd_schedulingController::schedule_validate_setup( $objFile );

				
					break;

				default:
				
					break;					
				
			}
			
			
		}
		else
		{

			$objValidateSetup->flag_complete = "1";
			$objValidateSetup->agent_id = $objAgentComplete->agent_id;
			$objValidateSetup->agent_name = $objAgentComplete->agent_name;
		}


		$logMessage = "INSIDE | ".$className." | ".$functionName." | beacon end of schedule_validate_setup";
		$logMessage .= "\n\r objValidateSetup: ".print_r($objValidateSetup, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objValidateSetup;
	}
	
	
	
	
// !TEST FUNCTIONS

	public function test_logger( $file_name=null )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=test_logger



		$className = "Nsd_schedulingController";
		$functionName = "test_logger";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	

	public function test_zipcode(  )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=test_zipcode



		$className = "Nsd_schedulingController";
		$functionName = "test_zipcode";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objInput = new stdClass();

		$objInput->zipcode 	= "01001";

		$objReturn = Nsd_schedulingController::getLatLng( $objInput );

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	



	public function test_zipcode_distance(  )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=test_zipcode_distance



		$className = "Nsd_schedulingController";
		$functionName = "test_zipcode_distance";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objInput1 = new stdClass();
		$objInput1->zipcode 	= "02370";

		$objReturn1 = Nsd_schedulingController::getLatLng( $objInput1 );


		$objInput2 = new stdClass();
		$objInput2->zipcode 	= "02898";

		
		$objReturn2 = Nsd_schedulingController::getLatLng( $objInput2 );


		$distance = MetalakeHelperCore::distance($lat1 = $objReturn1->latitude, $lon1 = $objReturn1->longitude, $lat2 = $objReturn2->latitude, $lon2 = $objReturn2->longitude, $unit="M");


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objInput1: ".print_r($objInput1, true);
		$logMessage .= "\n\r objReturn1: ".print_r($objReturn1, true);		
		$logMessage .= "\n\r objInput2: ".print_r($objInput2, true);
		$logMessage .= "\n\r objReturn2: ".print_r($objReturn2, true);
		$logMessage .= "\n\r distance: ".print_r($distance, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	

	public function test_alerts( )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=test_alerts
		
		$className = "Nsd_schedulingController";
		$functionName = "test_alerts";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}		

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objAlert = new stdClass();
		#$objAlert->recipients = array( 'support@metalake.com' );
		$objAlert->recipients = array( '7039305383@txt.att.net', 'lewsawyer@gmail.com' );
		$objAlert->subject = "Test Subject";
		$objAlert->body = "Test Body <br><br> second line";
		
		$resultAlert = Nsd_schedulingController::sendAlert( $objAlert );

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r resultAlert: ".print_r($resultAlert, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "END | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	



	public function test_validateAgentSetupComplete( $objInput=NULL )
	{
		# http://dev4.metalake.net/index.php?option=com_nsd_scheduling&task=test_validateAgentSetupComplete



		$className = "Nsd_schedulingController";
		$functionName = "test_validateAgentSetupComplete";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objInput = new stdClass();
		$objInput->agent_code = "CLAUDIO";
		
		
		$return = Nsd_schedulingController::validateAgentSetupComplete( $objInput );
		
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r return: ".print_r($return, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	




	public function test_read_excel( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_read_excel



		$className = "Nsd_schedulingController";
		$functionName = "test_read_excel";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$objServer = new stdClass();
		
		$objServer->path_inbound = "nsdscheduling/INBOUND";
		$objServer->path_processed = "nsdscheduling/PROCESSED";
		$objServer->path_skipped = "nsdscheduling/SKIPPED";


		$filename = "NSD Only CARRIER_DELIVERY_scheduled-delivery_template - ASA.xlsx";
		$agent_id = "1";

		


		$filename = "NSD Only CARRIER_DELIVERY_scheduled-delivery_template - BUEHLER.xlsx";
		$agent_id = "2";


		$filename = "NSD Only CARRIER_DELIVERY_scheduled-delivery_template - CBC.xlsx";
		$agent_id = "3";

		

		$filename = "NSD Only CARRIER_DELIVERY_scheduled-delivery_template - VTRAN.xlsx";
		$agent_id = "4";







		$inputFileType = 'Xlsx';
		$inputFileName = JPATH_SITE."/".$objServer->path_inbound."/".$filename;


		$arrSheetNames = array();
		$arrSheetNames[] = "geographicalAreas";
		$arrSheetNames[] = "schedules";
		$arrSheetNames[] = "slots";
		$arrSheetNames[] = "holidays";


		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		
		$reader->setReadDataOnly(true);


		foreach ( $arrSheetNames as $sheetname )
		{
			
			$reader->setLoadSheetsOnly($sheetname);
			$spreadsheet = $reader->load($inputFileName);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();			

			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r sheetData: ".print_r($sheetData, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


			switch( $sheetname )
			{





				case "geographicalAreas":
	
	
					$countRow = 1;
					
					foreach( $sheetData as $arrData )
					{

						if ( $countRow > 1 && trim($arrData[0]) != "" )
						{
							
							$objDataInput = new stdClass();
							
							$objDataInput = new stdClass();
							
							$objDataInput ->agent_id = $agent_id;
							$objDataInput ->geographical_area = $arrData[0];
							$objDataInput ->state = $arrData[1];
							$objDataInput ->city = $arrData[2];
							$objDataInput ->district = $arrData[3];
							$objDataInput ->minpostalcode = $arrData[4];
							$objDataInput ->maxpostalcode = $arrData[5];
							$objDataInput ->geocode = $arrData[6];
							$objDataInput ->subgeographica_areas = $arrData[7];
							$objDataInput ->is_root = $arrData[8];
							$result = JFactory::getDbo()->insertObject('htc_os_lu_geographical_areas', $objDataInput);
							$insertID = $db->insertid();
	
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r objDataInput: ".print_r($objDataInput, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
						}
						
						$countRow += 1;
					}
	
				
					break;






				case "schedules":
	
	
					$countRow = 1;
					
					foreach( $sheetData as $arrData )
					{

						if ( $countRow > 1 && trim($arrData[0]) != "" )
						{
							
							$objDataInput = new stdClass();
							
							$objDataInput ->agent_id = $agent_id;
							$objDataInput ->geographical_area = $arrData[0];
							$objDataInput ->schedule_name = $arrData[1];
							$objDataInput ->valid_from = $arrData[2];
							$objDataInput ->valid_until = $arrData[3];
							$objDataInput ->holidays = $arrData[4];
							$result = JFactory::getDbo()->insertObject('htc_os_lu_schedules', $objDataInput);
							$insertID = $db->insertid();
	
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r objDataInput: ".print_r($objDataInput, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
						}
						
						$countRow += 1;
					}
	
				
					break;





				case "slots":
	
	
					$countRow = 1;
					
					foreach( $sheetData as $arrData )
					{

						if ( $countRow > 1 && trim($arrData[0]) != "" )
						{
							
							$objDataInput = new stdClass();
							
							$objDataInput ->agent_id = $agent_id;
							$objDataInput ->schedule_name = $arrData[0];
							$objDataInput ->time_slot = $arrData[1];
							$objDataInput ->slot_days = $arrData[2];
							$objDataInput ->capacity = $arrData[3];
							$objDataInput ->services = $arrData[4];
							$objDataInput ->attributes = $arrData[5];
							$objDataInput ->monday = $arrData[6];
							$objDataInput ->tuesday = $arrData[7];
							$objDataInput ->wednesday = $arrData[8];
							$objDataInput ->thursday = $arrData[9];
							$objDataInput ->friday = $arrData[10];
							$objDataInput ->saturday = $arrData[11];
							$result = JFactory::getDbo()->insertObject('htc_os_lu_slots', $objDataInput);
							$insertID = $db->insertid();
	
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r objDataInput: ".print_r($objDataInput, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
						}
						
						$countRow += 1;
					}
	
				
					break;





				case "holidays":
	
	
					$countRow = 1;
					
					foreach( $sheetData as $arrData )
					{

						if ( $countRow > 1 && trim($arrData[0]) != "" )
						{
							
							$objDataInput = new stdClass();
	
							$objDataInput ->agent_id = $agent_id;
							$objDataInput ->holiday_name = $arrData[0];
							$objDataInput ->start = $arrData[1];
							$objDataInput ->end = $arrData[2];
							$result = JFactory::getDbo()->insertObject('htc_os_lu_holidays', $objDataInput);
							$insertID = $db->insertid();
	
							$logMessage = "INSIDE | ".$className." | ".$functionName;
							$logMessage .= "\n\r objDataInput: ".print_r($objDataInput, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
						}
						
						$countRow += 1;
					}
	
				
					break;
				
			}
		}




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_listFiles( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_listFiles



		$className = "Nsd_schedulingController";
		$functionName = "test_listFiles";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objServer = new stdClass();
		
		$objServer->path_inbound = "nsdscheduling/INBOUND";
		$objServer->path_processed = "nsdscheduling/PROCESSED";
		$objServer->path_skipped = "nsdscheduling/SKIPPED";

		$pathInbound = JPATH_SITE."/".$objServer->path_inbound;


		$arrFiles = JFolder::files( $pathInbound );




		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r arrFiles: ".print_r($arrFiles, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public function test_parse_file_name( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_parse_file_name



		$className = "Nsd_schedulingController";
		$functionName = "test_parse_file_name";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objWork = new stdClass();


		$objWork->fileName = "NSD Only CARRIER_DELIVERY_scheduled-delivery_template - ASA.xlsx";

		$arrFind = array( " ", "-" );
		$arrReplace = array( "_", "_" );

		$objWork->fileNameClean = str_replace( $arrFind, $arrReplace, $objWork->fileName );
		
		$arrfileName = explode( "_", $objWork->fileNameClean );
		
		$objWork->arrfileName = $arrfileName;
		
		$arrfileName = array_reverse( $arrfileName );
		
		$objWork->arrfileNameRev = $arrfileName;
		
		$objWork->agentName = strstr($arrfileName[0], '.', true); 
		
		
		
		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objWork: ".print_r($objWork, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_token_time( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_token_time



		$className = "Nsd_schedulingController";
		$functionName = "test_token_time";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$window_start_time = "18:00:00";
		
		$timestamp = strtotime($window_start_time);
		
		$newTime = date("H:i", $timestamp);


		$tokenNow = strtotime("+0 minutes");
		
		
		$tokenNow_view = date("Y-m-d H:i", $tokenNow);

		$tokenExpire = strtotime("+30 minutes");

		$k = 1234567;

		$tokenExpire1 = $tokenExpire + $k;

		
		$tokenExpire_view1 = date("Y-m-d H:i", $tokenExpire1);
		
		
		$tokenExpire2 = $tokenExpire1 - $k;
		
		$tokenExpire_view2 = date("Y-m-d H:i", $tokenExpire2);
		
		
		$b64eTokenExpire = base64_encode($tokenExpire);
		$b64dTokenExpire = base64_decode($b64eTokenExpire);
		$b64dtokenExpire_view = date("Y-m-d H:i", $b64dTokenExpire);
		
		

		

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r window_start_time: ".print_r($window_start_time, true);
		$logMessage .= "\n\r timestamp: ".print_r($timestamp, true);
		$logMessage .= "\n\r newTime: ".print_r($newTime, true);
		$logMessage .= "\n\r tokenNow: ".print_r($tokenNow, true);
		$logMessage .= "\n\r tokenNow_view: ".print_r($tokenNow_view, true);
		$logMessage .= "\n\r tokenExpire: ".print_r($tokenExpire, true);
		$logMessage .= "\n\r tokenExpire1: ".print_r($tokenExpire1, true);
		$logMessage .= "\n\r tokenExpire2: ".print_r($tokenExpire2, true);
		$logMessage .= "\n\r tokenExpire_view1: ".print_r($tokenExpire_view1, true);
		$logMessage .= "\n\r tokenExpire_view2: ".print_r($tokenExpire_view2, true);
		
		$logMessage .= "\n\r b64eTokenExpire: ".print_r($b64eTokenExpire, true);
		$logMessage .= "\n\r b64dTokenExpire: ".print_r($b64dTokenExpire, true);
		$logMessage .= "\n\r b64dtokenExpire_view: ".print_r($b64dtokenExpire_view, true);
		

		

		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_create_lookup_array( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_create_lookup_array



		$className = "Nsd_schedulingController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "select data_lookup, data_replace from htc_os_lu_holiday_matrix where state = '1'";
		$db->setQuery($query);
		$arrHolidayMatrix = $db->loadObjectList();	
		

		$arrLookup = array();
		
		
		foreach( $arrHolidayMatrix as $row )
		{
			$arrLookup[$row->data_lookup] = $row->data_replace;
			
		}



		$testMe = $arrLookup['Christmas'];

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r arrHolidayMatrix: ".print_r($arrHolidayMatrix, true);
		$logMessage .= "\n\r arrLookup: ".print_r($arrLookup, true);
		$logMessage .= "\n\r testMe: ".print_r($testMe, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

	public function test_sendSchedulingReport( )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_sendSchedulingReport



		$className = "Nsd_schedulingController";
		$functionName = "test_sendSchedulingReport";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->directoryInbound		= $params->get('directory_inbound');
		$paramsComponent->directoryProcessed	= $params->get('directory_processed');
		$paramsComponent->directorySkipped		= $params->get('directory_skipped');
		$paramsComponent->directoryOutbound		= $params->get('directory_outbound');
		
		$paramsComponent->reportEmailSendTo			= $params->get('report_email_send_to');
		$paramsComponent->reportEmailSendFrom		= $params->get('report_email_send_from');
		$paramsComponent->reportEmailSendFromName	= $params->get('report_email_send_from_name');

		$paramsComponent->pathInbound			=  JPATH_SITE."/".$paramsComponent->directoryInbound."/";
		$paramsComponent->pathProcessed			=  JPATH_SITE."/".$paramsComponent->directoryProcessed."/";
		$paramsComponent->pathSkipped			=  JPATH_SITE."/".$paramsComponent->directorySkipped."/";
		$paramsComponent->pathOutbound			=  JPATH_SITE."/".$paramsComponent->directoryOutbound."/";


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



        $query = "select d.id, d.stamp_datetime, d.system, d.agent_id, 
if (a.name is not null, a.name, '') as 'agent_name',
d.order_id, d.delivery_zipcode, d.record_type, 
if(d.request_delivery_date != '0000-00-00', d.request_delivery_date, '') as request_delivery_date, 
if(d.request_time_window_start != '0000-00-00 00:00:00', d.request_time_window_start, '') as request_time_window_start, 
if(d.request_time_window_end != '0000-00-00 00:00:00', d.request_time_window_end, '') as request_time_window_end,
d.result, d.error, d.error_code, d.system_error_message from htc_os_deliveries d left join htc_os_agents a on a.id=d.agent_id  where stamp_datetime > DATE_SUB(NOW(), INTERVAL 44 hour);";

        $db->setQuery($query);
        
        $assocListReportRecords = $db->loadAssocList();
        


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r assocListReportRecords: ".print_r($assocListReportRecords, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		if ( count($assocListReportRecords) > 0 )
		{

			$return_ctf = 1;

			$filePath = $paramsComponent->pathOutbound;  
	
			$fileName = "shipnsd_schedule_report_".date("Ymdhis").".txt";
			
			$fileInfo = $filePath.$fileName;


			$rptRecord = "";
	
			$fp = fopen( $fileInfo, "a+" );
			fwrite($fp, $rptRecord);
			fclose($fp);
	
			$logMessage = "INSIDE | ".$className." | ".$functionName. " Successful created transfer file: ".$fileName;
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
			$logMessage = "INSIDE | Ml_apiController | ".$functionName;
			$logMessage .= "\n\r filePath: ".print_r($filePath, true);
			$logMessage .= "\n\r fileName: ".print_r($fileName, true);
			$logMessage .= "\n\r fileInfo: ".print_r($fileInfo, true);
			$logMessage .= "\n\r rptRecord: ".print_r($rptRecord, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	

			#header record
			$strExport = array();
			$strExport['stamp_datetime'] = 'stamp_datetime';
			$strExport['system'] = 'system';
			$strExport['agent_id'] = 'agent_id';
			$strExport['agent_name'] = 'agent_name';
			$strExport['order_id'] = 'order_id';
			$strExport['delivery_zipcode'] = 'delivery_zipcode';
			$strExport['record_type'] = 'record_type';
			$strExport['request_delivery_date'] = 'request_delivery_date';
			$strExport['request_time_window_start'] = 'request_time_window_start';
			$strExport['request_time_window_end'] = 'request_time_window_end';
			$strExport['result'] = 'result';
			$strExport['error'] = 'error';
			$strExport['error_code'] = 'error_code';
			$strExport['system_error_message'] = 'system_error_message';
			$fp = fopen( $fileInfo, "a+" );
			fputcsv( $fp, $strExport, "\t" );
			fclose( $fp );
	
	
			$record_count = 0;
	
			foreach ( $assocListReportRecords as $arrRecord )
			{
	
	
				$logMessage = "INSIDE | ".$className." | ".$functionName ." | Added data record id: ".$arrRecord['id']." to file: ".$fileName;
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
				unset($arrRecord['id']);
	
				$fp = fopen( $fileInfo, "a+" );
				fputcsv( $fp, $arrRecord, "\t" );
				fclose( $fp );
	
				$record_count += 1;

			}

		}




			$body = "";
			$body .= "<br>Attached is the ShipNSD Scheduling Report for: ".date("Y-m-d")."<br>";
			$body .= "<br><br>The report file is a tab delimited text file that can be opened in MSExcel.<br>";


			$fgc = file_get_contents($fileInfo);
			$attachment_content = base64_encode( file_get_contents($fileInfo) );


			$objSendEmailMessage = new stdClass();
			$objSendEmailMessage->email_to = $paramsComponent->reportEmailSendTo;
			$objSendEmailMessage->email_from = $paramsComponent->reportEmailSendFrom;
			$objSendEmailMessage->email_from_name = $paramsComponent->reportEmailSendFromName;
			$objSendEmailMessage->email_subject = "ShipNSD Scheduling Report for ".date("Y-m-d");
			$objSendEmailMessage->email_body = $body;
			$objSendEmailMessage->attachment_content = $attachment_content;
			$objSendEmailMessage->attachment_filename = $fileName;
			
			$objSendEmailMessage->email_category = "scheduling support";
	
			$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );

			$logMessage = "INSIDE | Ml_apiController | ".$functionName;
			$logMessage .= "\n\r objSGReturn: ".print_r($objSGReturn, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			if (  $objSGReturn->error == "0"  )
			{
				
				JFile::delete($fileInfo);

				$logMessage = "INSIDE | Ml_apiController | ".$functionName. "| Sending Schedule Report Success";
				$logMessage .= "\n\r objSGReturn: ".print_r($objSGReturn, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				
			}
			else
			{

				$logMessage = "INSIDE | Ml_apiController | ".$functionName. "| Sending Schedule Report Failed";
				$logMessage .= "\n\r objSendEmailMessage: ".print_r($objSendEmailMessage, true);
				$logMessage .= "\n\r objSGReturn: ".print_r($objSGReturn, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


	public function test_build_ludays( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_build_ludays
		# http://nsdstaging.metalake.net/index.php?option=com_nsd_scheduling&task=test_build_ludays



		$className = "Nsd_schedulingController";
		$functionName = "test_build_ludays";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrInsert = array();

		$arrDays = array();
		#$arrDays[] = "SU";
		$arrDays[] = "M";
		$arrDays[] = "T";
		$arrDays[] = "W";
		$arrDays[] = "TH";
		$arrDays[] = "F";
		$arrDays[] = "SA";
		$arrDays[] = "SU";
		
		$result = Nsd_schedulingController::uniqueCombination($arrDays);


		foreach( $result as $r )
		{
			
			$di = implode( ",", $r );
		
			$arrInsert[] = $di;				
			
			
		}


		foreach( $arrInsert as $i  )
		{
			
			$objDataInsert = new stdClass();
			$objDataInsert->key = $i;
			$objDataInsert->value = $i;
			$objDataInsert->state = "1";
			$result = JFactory::getDbo()->insertObject('htc_os_lu_days_copy', $objDataInsert);
			$activityID = $db->insertid();			
		}


		$arrInsert2 = array();
		$arrInsert2[] = array("SU-M","SU,M");
		$arrInsert2[] = array("SU-T","SU,M,T");
		$arrInsert2[] = array("SU-W","SU,M,T,W");
		$arrInsert2[] = array("SU-TH","SU,M,T,W,TH");
		$arrInsert2[] = array("SU-F","SU,M,T,W,TH,F");
		$arrInsert2[] = array("SU-SA","SU,M,T,W,TH,F,SA");
		$arrInsert2[] = array("M-T","M,T");
		$arrInsert2[] = array("M-W","M,T,W");
		$arrInsert2[] = array("M-TH","M,T,W,TH");
		$arrInsert2[] = array("M-F","M,T,W,TH,F");
		$arrInsert2[] = array("M-SA","M,T,W,TH,F,SA");
		$arrInsert2[] = array("M-SU","M,T,W,TH,F,SA,SU");
		$arrInsert2[] = array("T-W","T,W");
		$arrInsert2[] = array("T-TH","T,W,TH");
		$arrInsert2[] = array("T-F","T,W,TH,F,");
		$arrInsert2[] = array("T-SA","T,W,TH,F,SA");
		$arrInsert2[] = array("T-SU","T,W,TH,F,SA,SU");
		$arrInsert2[] = array("W-TH","W,TH");
		$arrInsert2[] = array("W-F","W,TH,F");
		$arrInsert2[] = array("W-SA","W,TH,F,SA");
		$arrInsert2[] = array("W-SU","W,TH,F,SA,SU");
		$arrInsert2[] = array("TH-F","TH,F");
		$arrInsert2[] = array("TH-SA","TH,F,SA");
		$arrInsert2[] = array("TH-SU","TH,F,SA,SU");
		$arrInsert2[] = array("F-SA","F,SA");
		$arrInsert2[] = array("F-SU","F,SA,SU");
		$arrInsert2[] = array("SA-SU","SA.SU");



		foreach( $arrInsert2 as $i2  )
		{
			
			$objDataInsert = new stdClass();
			$objDataInsert->key = $i2[0];
			$objDataInsert->value = $i2[1];
			$objDataInsert->state = "1";
			$result = JFactory::getDbo()->insertObject('htc_os_lu_days_copy', $objDataInsert);
			$activityID = $db->insertid();			
		}




		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r arrDays: ".print_r($arrDays, true);
		$logMessage .= "\n\r result: ".print_r($result, true);		
		$logMessage .= "\n\r arrInsert: ".print_r($arrInsert, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public static function uniqueCombination($in, $minLength = 1, $max = 2000) {
	    $count = count($in);
	    $members = pow(2, $count);
	    $return = array();
	    for($i = 0; $i < $members; $i ++) {
	        $b = sprintf("%0" . $count . "b", $i);
	        $out = array();
	        for($j = 0; $j < $count; $j ++) {
	            $b{$j} == '1' and $out[] = $in[$j];
	        }
	
	        count($out) >= $minLength && count($out) <= $max and $return[] = $out;
	        }
	    return $return;
	}



	public static function test_pruneScheduleLogs()
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_pruneScheduleLogs
		
		
		$className = "Nsd_schedulingController";
		$functionName = "test_pruneScheduleLogs";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	



        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->log_table_prune_size	= $params->get('log_table_prune_size');



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;		
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrTables = array();
		$arrTables[] = "htc_os_schedule_validation_cron_log";


		$log_table_prune_size = $paramsComponent->log_table_prune_size;
		$log_table_prune_size_upper_boundry = ( $log_table_prune_size + 10000 );

				$logMessage = "INSIDE | Ml_apiController | ".$functionName;
				$logMessage .= "\n\r log_table_prune_size: ".print_r($log_table_prune_size, true);
				$logMessage .= "\n\r log_table_prune_size_upper_boundry: ".print_r($log_table_prune_size_upper_boundry, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		#if ( date('H') == '23' )
		
		if ( TRUE )
		{
			foreach( $arrTables as $table )
			{
				$query = "select count(z.id) from ".$table." z";
				$db->setQuery($query);
				$recordCount = $db->loadResult();

				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r recordCount for ".$table.": ".print_r($recordCount, true);
				#$logMessage .= "\n\r dateH: ".print_r(date('H'), true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				if ( $recordCount > $log_table_prune_size_upper_boundry )
				{
		
					$query = "select z.id from ".$table." z order by z.id desc limit ".$log_table_prune_size.", 1";
					$db->setQuery($query);
					$idRecord = $db->loadResult();
			
					if ( $idRecord != "" )
					{
						$queryD = "Delete FROM ".$table." WHERE id <= '".$idRecord."' ";
						$db->setQuery($queryD);
						$db->query();
					}
		
					$logMessage = "INSIDE | ".$className." | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r idRecord: ".print_r($idRecord, true);
					$logMessage .= "\n\r queryD: ".print_r($queryD, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				}


			}
		}

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	

	public function test_determine_allow_schedule( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_determine_allow_schedule



		$className = "Nsd_schedulingController";
		$functionName = "test_determine_allow_schedule";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objOrder = new stdClass();
            $objOrder->customer_account_code = "10343";
            $objOrder->origid = "11717";
            $objOrder->service_level = "WHITEGLOVE";
            $objOrder->op_code = "DD";


		$objReturn = Nsd_schedulingController::determine_can_schedule( $objOrder );

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	
	
// !FUNCTION TEMPLATE

	public function test_function_template( $file_name=null )
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_scheduling&task=test_function_template



		$className = "Nsd_schedulingController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	
}
