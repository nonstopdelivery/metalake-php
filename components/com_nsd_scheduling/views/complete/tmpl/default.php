<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$showObject = false;

$display = "";


if ($this->objDisplay->update->error) {
	$display .= "<h2>Schedule Delivery</h2>";
	$display .= "<br />";
	$display .= $this->objDisplay->update->error_message;
	$display .= "<br /><span class='note'>(error: ".$this->objDisplay->update->error_code.")</span>";
}
else {
	$display .= "<h2>Delivery Scheduled</h2>";
	$display .= "<br />";

	if ($this->objDisplay->order->customer_account_code == "11649") {     //JC Penny
	$display .= "Your delivery has been scheduled for <b>".date('l, F j, Y', strtotime($this->objDisplay->update->requested_time_start))."</b>, An agent will call you the day before your delivery date to provide a 2-hour delivery window.<br />";
	} else {
	$display .= "Your delivery has been scheduled for <b>".date('l, F j, Y', strtotime($this->objDisplay->update->requested_time_start))."</b>, An agent will call you the day before your delivery date to provide a delivery window.<br />";
	#$display .= "Your delivery has been scheduled for <b>".date('l, F j, Y', strtotime($this->objDisplay->update->requested_time_start))."</b>, between <b>".date('g:i a', strtotime($this->objDisplay->update->requested_time_start))."</b> and <b>".date('g:i a', strtotime($this->objDisplay->update->requested_time_end))."</b>.";
	}

	$display .= "<br />";
	$display .= "If you have questions or would like to change your scheduled delivery, please contact customer support at 800-956-7212 or <a class='purechat-button-expand' >chat with us</a> right now!";
	$display .= "<br ><br />";
	
	#$display .= "<a href='https://tracking.shipnsd.com/?t=".$this->objDisplay->order->trace_number."' class='btn'>Track your delivery</a>";

	$display .= "<a href='https://tracking.shipnsd.com/?t=".$this->objDisplay->order->trace_number."&z=".$this->objDisplay->order->destination_zipcode."' class='btn'>Track your delivery</a>";

	$display .= "<br ><br /><br />";
/*
	$display .= "<h2>Get Updates</h2>";
	$display .= "Use the form below to sign up to receive updates about your delivery.<br />";
	$display .= "<form class='form-horizontal notify col-sm-4'>";
	$display .= "	<div class='radio-inline'>";
	$display .= "		<label><input type='radio' name='optionsNotify' id='optionsNotifyEmail' value='Email'> Email</label>";
	$display .= "	</div>";
	$display .= "	<div class='radio-inline'>";
	$display .= "		<label><input type='radio' name='optionsNotify' id='optionsNotifyPhone' value='Phone'> Text Message</label>";
	$display .= "	</div>";
	$display .= "	<div class='form-group hidden' id='notifyEmailGroup'>";
	#$display .= "		<label for='notifyEmail'>Email address</label>";
    $display .= "		<input type='email' class='form-control' id='notifyEmail' placeholder='Enter email address'>";
	$display .= "	</div>";
	$display .= "	<div class='form-group hidden' id='notifyPhoneGroup'>";
	#$display .= "		<label for='notifyPhone'>Phone Number</label>";
    $display .= "		<input type='phone' class='form-control' id='notifyPhone' placeholder='Enter phone number'>";
	$display .= "	</div>";
	$display .= "	<div class='form-group hidden' id='notifySubmit'>";
	$display .= "		<button type='submit' class='btn'>Get Updates</button>";
	$display .= "	</div>";
	$display .= "</form>";
	$display .= "<div class='clearfix'></div>";

	$display .= "<script type=\"text/javascript\">";
	$display .= "	jQuery(document).ready(function() {";
	$display .= "		jQuery('#optionsNotifyEmail').on('click', function () {";
	$display .= "			jQuery('#notifyEmailGroup').removeClass(\"hidden\");";  
	$display .= "			jQuery('#notifyPhoneGroup').addClass(\"hidden\");";  
	$display .= "			jQuery('#notifySubmit').removeClass(\"hidden\");";  
	$display .= "		})";
	$display .= "	});";
	$display .= "	jQuery(document).ready(function() {";
	$display .= "		jQuery('#optionsNotifyPhone').on('click', function () {";
	$display .= "			jQuery('#notifyEmailGroup').addClass(\"hidden\");";  
	$display .= "			jQuery('#notifyPhoneGroup').removeClass(\"hidden\");";  
	$display .= "			jQuery('#notifySubmit').removeClass(\"hidden\");";  
	$display .= "		})";
	$display .= "	});";
	$display .= "</script>";
*/


}




if ($showObject) :
	$display .= "<br /><br /><h3>objDisplay</h3><pre style='text-align:left'>".print_r($this->objDisplay, true)."</pre>";	
endif;

echo $display;
?>
