<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
defined('_JEXEC') or die;

$showObject = false;


$display = "";
$display .= $this->apiReturn;

if ($showObject) :
	$display .= "<br /><br /><h3>objDisplay</h3><pre style='text-align:left'>".print_r($this->objDisplay, true)."</pre>";	
endif;

echo $display;

?>