<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Nsd_schedulingViewApi extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');
		#$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_nsd_scheduling');



		$controllerName = "Nsd_schedulingViewApi";
		$functionName = "view";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;


		#session
		$objForm = new stdClass();
		
		
		$jinput = JFactory::getApplication()->input;

		$trace = trim($jinput->get('billNumber', '', 'STRING'));
		$zip = trim($jinput->get('zipCode', '', 'STRING'));
		
		$objForm->trace = $trace;
		$objForm->zip = $zip;



		$this->assignRef('objForm', $objForm);

		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "scheduling_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "scheduling_prod";
	
		}	


		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		$stamp_start_micro = microtime(true);
		
		
		$objInput = new stdClass();
        $objInput->flag_api = "0";
        $objInput->trace_number = $objForm->trace;
        $objInput->zip = $objForm->zip;
		
		$objDisplay = Nsd_schedulingModelApi::getObjDisplay( $objInput );
		
		$this->assignRef('objDisplay', $objDisplay);
		
		$this->assignRef('apiReturn', $objDisplay); 	
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		


		$objDisplaySess = MetalakeHelperCore::getSessObj( "objDisplaySess" );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		$logMessage .= "\n\r objDisplaySess: ".print_r($objDisplaySess, true);
		$logMessage .= "\n\r ";
		#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		


		if ( $objDisplaySess->order->trace_number !=  $objForm->trace )
		{
			MetalakeHelperCore::clearSessObj( "objDisplaySess" );
			$objDisplaySess = MetalakeHelperCore::getSessObj( "objDisplaySess" );

			MetalakeHelperCore::setSessObj( "objDisplaySess", $objDisplay );
			
			$objDisplaySess = MetalakeHelperCore::getSessObj( "objDisplaySess" );

		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 ";;
		$logMessage .= "\n\r objDisplaySess: ".print_r($objDisplaySess, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$this->assignRef('objDisplaySess', $objDisplaySess);	




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_nsd_scheduling');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_NSD_SCHEDULING_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
