<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_scheduling
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

#$showObject = true;
$showObject = false;

$display = "";

$display .= "<h2>Schedule Your Delivery</h2>";

if ($this->objDisplay->error == "99") {
	$display .= "<br />";
	$display .= $this->objDisplay->error_message;
	$display .= "<br /><span class='note'>(error: ".$this->objDisplay->error_code.")</span>";
}
else {

	//If zipcode in URL is NOT valid/match {
	if ( $this->objDisplay->zipcode_validation->zipcode_identical == "0" ){
	
	$scheduleHiddenClass = "hidden";
		
	$display .= "To begin, enter the delivery destination zip code below:";
	
	$display .= "<form data-toggle='validator' role='form' action='".htmlspecialchars(JFactory::getURI()->toString())."' name='zipForm' id='zipForm' method='get' class='form-inline track'>";
	$display .= "	<div class='row'>";
	$display .= "	<div class='form-group col-sm-4 has-feedback'>";
	$display .= "		<input type='text' class='form-control' maxlength='5' name='zipcode' id='zipcode' data-match='#deliveryZip' data-match-error='Zip Code must match the delivery Zip Code.' data-required-error='Please enter the delivery zip code' value='".$this->objForm->zipcode."' placeholder='Enter Delivery Zip Code' required>";
	$display .= "		<span class='glyphicon form-control-feedback' aria-hidden='true'></span>";
	$display .= "		<div class='help-block with-errors'></div>";
	$display .= "		<div class='help-block'><a class='purechat-button-expand' >Need Help?</a></div>";
	$display .= "	</div>";
	#$display .= "	<input class='btn col-sm-1' type='Submit' id='sendResponse' value='Go' />";
	$display .= "	<input type='hidden' name='trace' value='".$this->objForm->trace."' />";
	$display .= "	<input type='hidden' name='deliveryZip' id='deliveryZip' value='".$this->objDisplay->order->destination_zipcode."' />";
	$display .= "	</div>";
	$display .= "</form>";

	}
	else {
		$scheduleHiddenClass = "noZip";
	} 

	$display .= "<div class='schedule ".$scheduleHiddenClass."'>";
	#$display .= "<div class='schedule hidden'>";
	
	if ($this->objDisplay->error) {
		$display .= $this->objDisplay->error_message;
		$display .= "<br /><span class='note'>(error: ".$this->objDisplay->error_code.")</span>";
	}
	else {
	
	if ($this->objDisplay->order->customer_account_code == "11649") {     //JC Penny
	$display .= "<h3>Choose Your Delivery Date</h3>";
	$display .= "An agent will call you the day before your delivery date to provide a 2-hour delivery window. ";
	} else {
	$display .= "<h3>Choose Your Delivery Date</h3>";
	$display .= "An agent will call you the day before your delivery date to provide a delivery window. ";
	#$display .= "<h3>Choose Your Delivery Date and Time</h3>";
	}
	$display .= "To schedule beyond the dates shown, please contact customer care at the <a class='purechat-button-expand' >chat link below</a>.<br />";
	$display .= "<br />";
	
	$display .= "<form action='".htmlspecialchars(JFactory::getURI()->toString())."' name='windowForm' id='windowForm' method='post'>";
	
	$display .= "	<div class='tab-container'>";
	$display .= "	<ul class='nav nav-tabs'>";
	
	$firstTab = true;
	$buttonDisabled = false;
	foreach ($this->objDisplay->days as $day) {
		$firstTab ? $tabActiveClass = " class='active'" : $tabActiveClass = "";
	
		if ($day->firstWindowID) {
		$display .= "		<li".$tabActiveClass."><a href='#".$day->date."' data-toggle='tab' onclick=\"document.getElementById('submitWindow').removeAttribute('disabled');document.getElementById('window".$day->firstWindowID."').checked='true';\"><span class='hidden-xs'>".$day->dayLabelOfWeek."<br /></span>".date('M j', strtotime($day->date))."</a></li>";
		}
		else {
		$display .= "		<li".$tabActiveClass."><a href='#".$day->date."' data-toggle='tab' onclick=\"document.getElementById('submitWindow').disabled='true';\"><span class='hidden-xs'>".$day->dayLabelOfWeek."<br /></span>".date('M j', strtotime($day->date))."</a></li>";
		if ($firstTab) {$buttonDisabled = true;}
		}


		$firstTab = false;
	}
	
	$display .= "	</ul>";
	$display .= "	<div class='clearfix'></div>";
	$display .= "	<div class='tab-content panel-group' id='dateTabs' role='tablist' aria-multiselectable='true'>";
	
	$firstTabArea = true;
	$firstPanelArea = true;
	$checked = " checked";
	foreach ($this->objDisplay->days as $day) {
		$firstTabArea ? $tabAreaActiveClass = " active" : $tabAreaActiveClass = "";
		$firstPanelArea ? $panelAreaActiveClass = " in" : $panelAreaActiveClass = "";
		$firstPanelArea ? $panelAriaExpanded = "true" : $panelAriaExpanded = "false";
		$display .= "	<div class='tab-pane".$tabAreaActiveClass." panel' id='".$day->date."'>";
		#$display .= "		<div class='panel'>";
/*
		$display .= "			<div class='panel-heading' id='heading-".$day->date."'>";
		$display .= "				<h4 class='panel-title'>";
		$display .= "					<a data-toggle='collapse' data-parent='#dateTabs' href='#collapse-".$day->date."' aria-expanded='".$panelAriaExpanded."' aria-controls='collapse-".$day->date."' onclick=\"document.getElementById('window".$day->firstWindowID."').checked='true'\">".$day->dayLabelOfWeekFull.", ".date('M j', strtotime($day->date))."</a>";
		$display .= "				</h4>";
		$display .= "			</div>";
*/
		$display .= "			<div id='collapse-".$day->date."' class='panel-collapse collapse".$panelAreaActiveClass."' role='tabpanel' aria-labelledby='heading-".$day->date."'>";
		$display .= "				<div class='panel-body'>";
	
			foreach ($day->windows as $window) {
				$isDisabled = $window->enabled ? "" : "disabled";
				
				$display .= "<div class='radio'>";
				$display .= "	<label class='rb-container ".$isDisabled."'>";
				
				if (!$isDisabled) {
				$display .= "		<input type='radio' name='optionsWindows' id='window".$window->uid."' value='".$window->request_date_start."|".$window->request_date_end."|".$window->id."' ".$checked." >";
				$display .= "		<span class='checkmark'></span>";
				$checked = "";
				} 
				else {
				$display .= "		<input type='radio' name='optionsWindows' id='window".$window->uid."' value='".$window->request_date_start."|".$window->request_date_end."|".$window->id."' ".$isDisabled." >";
				$display .= "		<span class='checkmark'></span>";				
				}
				$display .= 		$window->window_name." ".date('g:i a', strtotime($window->window_start_time))." - ".date('g:i a', strtotime($window->window_end_time))."<br />";
				$display .= "	</label>";
				$display .= "</div>";
			}
	
	
		$display .= "				</div>";
		$display .= "			</div>";
		#$display .= "		</div>";
		$display .= "	</div>";
		$firstTabArea = false;
		$firstPanelArea = false;
	}
	
	$display .= "	</div>"; //.tab-content
	if ($buttonDisabled) {
		$display .= "	<input class='btn' type='Submit' id='submitWindow' value='Schedule Delivery' disabled />";
	} else {
		$display .= "	<input class='btn' type='Submit' id='submitWindow' value='Schedule Delivery' />";
	}
	$display .= "	</div>"; //.tab-container
	
	
	$display .= "<input type='hidden' name='option' value='com_nsd_scheduling' />";
	$display .= "<input type='hidden' name='task' value='updateOrderData' />";
	#$display .= "<input type='hidden' name='id' value='".$this->objGroup->id."' />";
	$display .= JHTML::_( 'form.token' );
	
	
	
	$display .= "</form>"; // form
	
	$display .= "</div>"; // .schedule

	} 


	$display .= "<script type=\"text/javascript\">";
	$display .= "	jQuery(document).ready(function() {";
	$display .= "		jQuery('#zipForm').validator().on('invalid.bs.validator', function () {";
	$display .= "			jQuery('.schedule').addClass(\"hidden\");";  
	$display .= "		})";
	$display .= "	});";
	$display .= "	jQuery(document).ready(function() {";
	$display .= "		jQuery('#zipForm').validator().on('valid.bs.validator', function () {";
	$display .= "			jQuery('.schedule').removeClass(\"hidden\");";
	$display .= "			jQuery('#zipcode').prop('disabled', true);"; 
	$display .= "		})";
	$display .= "	});";
	$display .= "	jQuery(document).ready(function() {";
	$display .= "		jQuery('#submitWindow').on('click', function() {";
	$display .= "			jQuery('#submitWindow').attr('disabled', true);";
	$display .= "			jQuery('#windowForm').submit();";
	$display .= "		})";
	$display .= "	});";
	$display .= "</script>";

} // if/else error == 99

if ($showObject) :
	$display .= "<br /><br /><h3>objDisplay</h3><pre style='text-align:left'>".print_r($this->objDisplay, true)."</pre>";	
endif;

echo $display;
?>
