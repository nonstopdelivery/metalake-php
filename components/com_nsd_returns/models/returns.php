<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_returns
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Nsd_returns model.
 *
 * @since  1.6
 */
class Nsd_returnsModelReturns extends JModelItem
{

	public function getApi_return()
	{
	
		# for authentication and return headers
		# https://secure.php.net/manual/en/features.http-auth.php
	
		$controllerName = "Nsd_returnsModelReturns";
		$functionName = "getApi_return";

	
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName."";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$process_id = date("Ymdhis")."-".time();


		
		$collection = JRequest::getVar('collection');
		$command = JRequest::getVar('command');
		$customer_code = JRequest::getVar('customer-code');
		$debug = JRequest::getVar('debug', 0);
		$debug = 0;


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		#$logMessage .= "\n\r objLogger: ".print_r($objLogger, true);
		$logMessage .= "\n\r collection: ".print_r($collection, true);
		$logMessage .= "\n\r command: ".print_r($command, true);
		#$logMessage .= "\n\r tracking_number: ".print_r($tracking_number, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$serverValues = $_SERVER;
	

		$objInput = new stdClass();
		$objInput->username = $serverValues["PHP_AUTH_USER"];
		$objInput->password = $serverValues["PHP_AUTH_PW"];		

		$objValidation = Nsd_returnsController::validateJoomlaUser( $objInput );

		$return_logs = Nsd_returnsController::pruneLogs( );


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 ";
		$logMessage .= "\n\r serverValues: ".print_r($serverValues, true);
		$logMessage .= "\n\r PHP_AUTH_USER: ".print_r($serverValues["PHP_AUTH_USER"], true);
		$logMessage .= "\n\r PHP_AUTH_PW: ".print_r($serverValues["PHP_AUTH_PW"], true);
		$logMessage .= "\n\r REQUEST_METHOD: ".print_r($serverValues["REQUEST_METHOD"], true);
		$logMessage .= "\n\r objValidation: ".print_r($objValidation, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$logTable = "htc_nsd_returns_log";
		$logObj = new stdClass();
		$logObj->process_id = $process_id;
		$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
		$logObj->datetime = date("Y-m-d h:i:s");
		$logObj->milliseconds = round(microtime(true) * 1000);
		$logObj->user = $serverValues["PHP_AUTH_USER"];
		$logObj->customer_code = $customer_code;
		$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
		$logObj->request_method = $serverValues["REQUEST_METHOD"];
		$logObj->collection = $collection;
		$logObj->command = $command;
		$logObj->description = "Initial Call";
		$logObj->server_values = json_encode($serverValues);
		
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3 ";
		$logMessage .= "\n\r logObj: ".print_r($logObj, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		
		
		
		
		MetalakeHelperCore::logOBJ( $logTable, $logObj );


		require_once ('configuration.php' ); // since this file n configuration file both are at the same location

		$var_cls = new JConfig(); // object of the class

	    	// variables that you want to use 
	    	$offline = $var_cls->offline; 
	    	$sitename = $var_cls->sitename; 


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		$logMessage .= "\n\r offline: ".print_r($offline, true);
		$logMessage .= "\n\r sitename: ".print_r($sitename, true);
		$logMessage .= "\n\r var_cls: ".print_r($var_cls, true);

		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		

		if ( $offline == "1" )
		{

			http_response_code(400);
		
			$apiReturn = new stdClass();
			$apiReturn->error_state = "1";
			$apiReturn->error_code = "9999";
			$apiReturn->error_message = "Temporarily Offline for Maintenance. We are performing scheduled maintenance. We should be back online shortly.";
			$return_value = json_encode($apiReturn);		


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		$logMessage .= "\n\r apiReturn: ".print_r($apiReturn, true);
		$logMessage .= "\n\r return_value: ".print_r($return_value, true);

		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			echo $return_value;
			
			exit();

		}

		if ( $objValidation->validUser == 1 || $debug == 1 )
		{

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Valid User";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			if ( $objValidation->validGroup == 1  || $debug == 1 )
			{
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Valid Group";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

				if ( $objValidation->validInterval == 1  || $debug == 1 )
				{

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Valid Interval";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
					switch( $collection )
					{
						
		
		
						case "order":
						
							if ( $serverValues["REQUEST_METHOD"] != "POST" )
							{
									# set header to 405 - Missing parameters and validation errors			
									http_response_code(405);	
									
									$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 405 | Request Method should be POST";
									if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
									if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
									$objData = new stdClass();
									$objData->error_state = "1";
									$objData->error_code = "55";
									$objData->error_message = "Invalid request method. Expecting POST.";
									$return_value = json_encode($objData);
									
									$return_value = "Error [66] - Invalid request method. Expecting POST.";
		
									$logTable = "htc_nsd_returns_log";
									$logObj = new stdClass();
									$logObj->process_id = $process_id;
									$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
									$logObj->datetime = date("Y-m-d h:i:s");
									$logObj->milliseconds = round(microtime(true) * 1000);
									$logObj->user = $serverValues["PHP_AUTH_USER"];
									$logObj->customer_code = $customer_code;
									$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
									$logObj->request_method = $serverValues["REQUEST_METHOD"];
									$logObj->collection = $collection;
									$logObj->command = $command;
									$logObj->return_value = $return_value;
									$logObj->description = "http_response_code set to 400 | Request Method should be POST";
									$logObj->server_values = json_encode($serverValues);
									MetalakeHelperCore::logOBJ( $logTable, $logObj );
		

									JFactory::getDocument()->setMimeEncoding( 'application/text' );
									JResponse::setHeader('Content-Disposition','attachment;filename="results.text"');
									echo $return_value;		
		
							}
							else
							{
								
								switch( $command )
								{
								
									case "create-return-order":
									case "create":			
										$jsonData = file_get_contents("php://input");
										
										$data = json_decode($jsonData);
								
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3 ";
										$logMessage .= "\n\r jsonData: ".print_r($jsonData, true);
										$logMessage .= "\n\r data: ".print_r($data, true);
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
							
										$origin = $data->origin;
										$objDestination = $data->destination;
										
										$arrGoods = $data->goods;
							
							
										$arrDataIds = array();


											$objData = new stdClass();
											$objData->datetime = date("Y-m-d H:i:s");
											$objData->datetime_gmt = gmdate("Y-m-d H:i:s");
											
											$objData->reference1 = $data->reference1;
											$objData->reference2 = $data->reference2;
											$objData->reference3 = $data->reference3;
											$objData->reference4 = $data->reference4;
											$objData->disposal = $data->disposal;
											$objData->return_auth = $data->returnAuth;
											$objData->service_level = $data->localService;
											$objData->service_level_clean = Nsd_returnsController::cleanServiceLevel( $data->localService );
											$objData->pickup_date = $data->pickupTime;
											$objData->requested_date = property_exists('data','requestedDate') ? $data->requestedDate : "";
		
		
											#check additionalemail contains email, if so, put in origin email
											
											$returnEmailTest = MetalakeHelperCore::validateEmail( $origin->additionalInfo1 );		

											if ( $returnEmailTest == "1" )
											{
												
												$origin->email = $origin->additionalInfo1;
												$origin->additionalInfo1 = "";
												
											}


											
											$objEvalReturn = Nsd_returnsController::evalAdditionalInfo2( $origin->additionalInfo2 );



		
											$objData->customer_code = $data->customerCode;
											$objData->origin_name = $origin->name;
											$objData->origin_addressLine1 =$origin->addressLine1;
											$objData->origin_addressLine2 = $origin->addressLine2;
											$objData->origin_additionalInfo1 = $origin->additionalInfo1;
											$objData->origin_additionalInfo2 = $objEvalReturn->additionalInfo2;
											$objData->origin_city = $origin->city;
											$objData->origin_state = $origin->state;
											$objData->origin_zipCode = isset( $origin->zipCode ) ? $origin->zipCode : "" ;
											$objData->origin_country = isset( $origin->country ) ? $origin->country : "" ;
											$objData->origin_phone = $objEvalReturn->phone;
											$objData->origin_alt_phone = isset( $origin->altPhone ) ? $origin->altPhone : "";
											$objData->origin_email = isset( $origin->email ) ? $origin->email : "" ;
											
		
											$destinationCode = Nsd_returnsController::getDestinationCode( $objDestination );
											
											# 12/20: when the disposal flag is set to yes or true the destination code should be defaulted to 10343
											
											$destinationCode = ( strtolower($objData->disposal) == "yes" || strtolower($objData->disposal) == "true" || $objData->disposal == "1"  ) ? "13708" : $destinationCode  ;




											/*
											Home Depot indicate if the order is HAZMAT by putting the word HAZMAT in the consignee name or consignee address (or both).
											
											Whenever we get a HAZMAT order it needs to be processed into our system as disposal. i.e.   "disposal": "true",
											*/
											
											$modifyGoodsDesriptions = "0";
		
											if ( strpos( strtoupper($objDestination->name), "HAZMAT" ) !== false )
											{
												
												$objData->disposal = "true";
												
												$destinationCode = "13708";
												
												$modifyGoodsDesriptions = "1";
											}

		
											#if ( $destinationCode == "10343" ) 
											if ( $destinationCode != "" ) 
											{
												$objDestination->name = "";
												$objDestination->addressLine1 = "";
												$objDestination->addressLine2 = "";
												$objDestination->city = "";
												$objDestination->state = "";
												$objDestination->zipCode = "";
												$objDestination->country = "";
											}
		
		
											switch( $destinationCode )
											{
													
												case "13708":
													$destination_name_clean = "";
													break;	
												
												default:
													$destination_name_clean = $objDestination->name;

													break;
											}	
		
		
											$objData->destination_code = $destinationCode;
											$objData->destination_name = $objDestination->name;
											$objData->destination_name_clean = $destination_name_clean;
											$objData->destination_addressLine1 = $objDestination->addressLine1;
											$objData->destination_addressLine2 = $objDestination->addressLine2;
											$objData->destination_additionalInfo1 = $objDestination->additionalInfo1;
											$objData->destination_additionalInfo2 = $objDestination->additionalInfo2;
											$objData->destination_city = $objDestination->city;
											$objData->destination_state = $objDestination->state;
											$objData->destination_zipCode = isset( $objDestination->zipCode ) ? $objDestination->zipCode : "" ;
											$objData->destination_country = isset( $objDestination->country ) ? $objDestination->country : "" ;
											$objData->destination_phone = isset( $objDestination->phone ) ? $objDestination->phone : "" ;
											$objData->destination_alt_phone = isset( $objDestination->altPhone ) ? $objDestination->altPhone : "" ;
											$objData->destination_email = isset( $objDestination->email ) ? $objDestination->email : "" ;


											$result = JFactory::getDbo()->insertObject('htc_nsd_returns_data', $objData);
											$strDataId = $db->insertid();

										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3.5 ";
										$logMessage .= "\n\r objData: ".print_r($objData, true);
										$logMessage .= "\n\r result: ".print_r($result, true);
										$logMessage .= "\n\r strDataId: ".print_r($strDataId, true);
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


							
										foreach( $arrGoods as $good )
										{
							
											#save data
		
		
											$objDataItem = new stdClass();
											$objDataItem->data_id = $strDataId;
											$objDataItem->datetime = date("Y-m-d H:i:s");
											$objDataItem->datetime_gmt = gmdate("Y-m-d H:i:s");
											
											
											$objDataItem->goods_pieces = $good->pieces;
											$objDataItem->goods_weight = $good->weight;
											$objDataItem->goods_length = $good->length;
											$objDataItem->goods_width = $good->width;
											$objDataItem->goods_height = $good->height;
											
											$objDataItem->goods_description = ( $modifyGoodsDesriptions == "1" ) ? "HAZMAT - ".$good->description :  $good->description ;
											


											$objDataItem->goods_code = $good->code;
											$objDataItem->goods_code_clean = Nsd_returnsController::cleanGoodsCode( $good->code );
											
											$objDataItem->goods_commodity = ( $modifyGoodsDesriptions == "1" ) ? "HAZMAT" : "" ;

											
											$result = JFactory::getDbo()->insertObject('htc_nsd_returns_data_items', $objDataItem);
		
						                }
						                
						                
						                
						                $objImport = new stdClass();
						                $objImport->dataID = $strDataId;
						                
						                #$objImportReturn = Nsd_returnsController::importTMW( $objImport );
						                $objImportReturn = Nsd_truckmateController::insertHDReturnIntoTMW( $objImport );
						                
						                $arrReturn = (array)$objImportReturn;
						                
						                $jsonReturn = json_encode($arrReturn);
						                
										$arrFind = array('"billNumber":"', '","message"');
										$arrReplace = array('"billNumber":', ',"message"');
										$jsonReturn = str_replace( $arrFind, $arrReplace, $jsonReturn );						                


										$logTable = "htc_nsd_returns_log";
										$logObj = new stdClass();
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
										$logObj->datetime = date("Y-m-d h:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->customer_code = $customer_code;
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->description = "Response from TMW";
										$logObj->json_call = $jsonData;
										$logObj->return_value = $jsonReturn;
										$logObj->server_values = json_encode($serverValues);
										MetalakeHelperCore::logOBJ( $logTable, $logObj );


						                echo $jsonReturn;
						                break;



									case "lewtest":
										# set header to 400 - Missing parameters and validation errors			
										http_response_code(400);
			
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | lewtest | zz | zz";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

										$return_value = "testing";
/*
										$return_value = "Error [55] - Invalid command for collection: order";
			
										$logTable = "htc_nsd_returns_log";
										$logObj = new stdClass();
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
										$logObj->datetime = date("Y-m-d h:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->customer_code = $customer_code;
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->return_value = $return_value;
										$logObj->description = "http_response_code set to 400 | Invalid command for collection: order";
										$logObj->server_values = json_encode($serverValues);
										MetalakeHelperCore::logOBJ( $logTable, $logObj );
			
*/	
										JFactory::getDocument()->setMimeEncoding( 'application/text' );
										JResponse::setHeader('Content-Disposition','attachment;filename="results.text"');
										echo $return_value;	



										

										#return $return_value;
									
										break;


									
									default:
										# set header to 400 - Missing parameters and validation errors			
										http_response_code(400);
			
										$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 400 | Invalid command for collection: order";
										if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
										if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


										$return_value = "Error [55] - Invalid command for collection: order";
			
										$logTable = "htc_nsd_returns_log";
										$logObj = new stdClass();
										$logObj->process_id = $process_id;
										$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
										$logObj->datetime = date("Y-m-d h:i:s");
										$logObj->milliseconds = round(microtime(true) * 1000);
										$logObj->user = $serverValues["PHP_AUTH_USER"];
										$logObj->customer_code = $customer_code;
										$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
										$logObj->request_method = $serverValues["REQUEST_METHOD"];
										$logObj->collection = $collection;
										$logObj->command = $command;
										$logObj->return_value = $return_value;
										$logObj->description = "http_response_code set to 400 | Invalid command for collection: order";
										$logObj->server_values = json_encode($serverValues);
										MetalakeHelperCore::logOBJ( $logTable, $logObj );
			
	
										JFactory::getDocument()->setMimeEncoding( 'application/text' );
										JResponse::setHeader('Content-Disposition','attachment;filename="results.text"');
										echo $return_value;	

									
										break;
								}				
							}
							
							break;
		
		
		
		
						default:
		
							# set header to 405 Method Not Allowed			
							http_response_code(405);				
		
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 405 | Invalid collection";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
							$return_value = "Error [66] - Invalid collection";
		
							$logTable = "htc_nsd_returns_log";
							$logObj = new stdClass();
							$logObj->process_id = $process_id;
							$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
							$logObj->datetime = date("Y-m-d h:i:s");
							$logObj->milliseconds = round(microtime(true) * 1000);
							$logObj->user = $serverValues["PHP_AUTH_USER"];
							$logObj->customer_code = $customer_code;
							$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
							$logObj->request_method = $serverValues["REQUEST_METHOD"];
							$logObj->collection = $collection;
							$logObj->command = $command;
							$logObj->description = "ERROR | http_response_code set to 405 | Invalid collection";
							$logObj->return_value = $return_value;
							$logObj->server_values = json_encode($serverValues);
							MetalakeHelperCore::logOBJ( $logTable, $logObj );

							
							JFactory::getDocument()->setMimeEncoding( 'application/text' );
							JResponse::setHeader('Content-Disposition','attachment;filename="results.text"');
							echo $return_value;	
		
							break;
						
					}
				
				
				}
				else
				{
	
					#throw error
		
					# set header to 429 Too Many Requests		
					http_response_code(429);
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 429 | Exceeded the API rate limit";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
					$return_value = "Error [77] - User exceeded the API rate limit";
			
			
					$logTable = "htc_nsd_returns_log";
					$logObj = new stdClass();
					$logObj->process_id = $process_id;
					$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
					$logObj->datetime = date("Y-m-d h:i:s");
					$logObj->milliseconds = round(microtime(true) * 1000);
					$logObj->user = $serverValues["PHP_AUTH_USER"];
					$logObj->customer_code = $customer_code;
					$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
					$logObj->request_method = $serverValues["REQUEST_METHOD"];
					$logObj->collection = $collection;
					$logObj->command = $command;
					$logObj->description = "ERROR | http_response_code set to 429 | Exceeded the API rate limit | apiInterval: ".$objValidation->apiInterval." | diffMilliseconds: ".$objValidation->diffMilliseconds;
					$logObj->return_value = $return_value;
					$logObj->server_values = json_encode($serverValues);
					MetalakeHelperCore::logOBJ( $logTable, $logObj );				
	
				
					JFactory::getDocument()->setMimeEncoding( 'application/text' );
					JResponse::setHeader('Content-Disposition','attachment;filename="results.text"');
					echo $return_value;	
	
	
				}
			
			}
			else
			{

				#throw error
	
				# set header to 403 - 403 Forbidden The user might not have the necessary permissions for a resource, or may need an account of some sort.			
				http_response_code(403);
	
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 403 | User is not in the API group | strGroups: ".$strGroups;
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
				$strGroups = implode(",", $objValidation->arrGroups);

				$return_value = "Error [88] - User not authorized for API";
	
				$logTable = "htc_nsd_returns_log";
				$logObj = new stdClass();
				$logObj->process_id = $process_id;
				$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
				$logObj->datetime = date("Y-m-d h:i:s");
				$logObj->milliseconds = round(microtime(true) * 1000);
				$logObj->user = $serverValues["PHP_AUTH_USER"];
				$logObj->customer_code = $customer_code;
				$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
				$logObj->request_method = $serverValues["REQUEST_METHOD"];
				$logObj->collection = $collection;
				$logObj->command = $command;
				$logObj->description = "ERROR | http_response_code set to 403 | User is not in the API group | strGroups: ".$strGroups;
				$logObj->return_value = $return_value;
				$logObj->server_values = json_encode($serverValues);
				MetalakeHelperCore::logOBJ( $logTable, $logObj );				

				
				JFactory::getDocument()->setMimeEncoding( 'application/text' );
				JResponse::setHeader('Content-Disposition','attachment;filename="results.text"');
				echo $return_value;	


			}
		
		}
		else
		{
			#throw error

			# set header to 401 - 401 Unauthorized		
			http_response_code(401);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ERROR | http_response_code set to 401 | Invalid user or password";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$return_value = "Error [99] - Invalid user or password";

			$logTable = "htc_nsd_returns_log";
			$logObj = new stdClass();
			$logObj->process_id = $process_id;
			$logObj->datetime_gmt = gmdate("Y-m-d h:i:s");
			$logObj->datetime = date("Y-m-d h:i:s");
			$logObj->milliseconds = round(microtime(true) * 1000);
			$logObj->user = $serverValues["PHP_AUTH_USER"];
			$logObj->customer_code = $customer_code;
			$logObj->remote_addr = $serverValues["REMOTE_ADDR"];
			$logObj->request_method = $serverValues["REQUEST_METHOD"];
			$logObj->collection = $collection;
			$logObj->command = $command;
			$logObj->description = "ERROR | http_response_code set to 401 | Invalid user or password";
			$logObj->return_value = $return_value;
			$logObj->server_values = json_encode($serverValues);
			MetalakeHelperCore::logOBJ( $logTable, $logObj );
			
			
			JFactory::getDocument()->setMimeEncoding( 'application/text' );
			JResponse::setHeader('Content-Disposition','attachment;filename="results.text"');
			echo $return_value;			
			
		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		JFactory::getApplication()->close(); // or jexit();


	}

}
