<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_returns
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Returns_copy_1s list controller class.
 *
 * @since  1.6
 */
class Nsd_returnsControllerReturns_copy_1s extends Nsd_returnsController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Returns_copy_1s', $prefix = 'Nsd_returnsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
