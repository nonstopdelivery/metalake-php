<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_returns
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2017 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');

/**
 * Class Nsd_returnsController
 *
 * @since  1.6
 */
class Nsd_returnsController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'returnslogs');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}

// !FUNCTIONS

	public static function validateJoomlaUser( $objInput )
	{	
	

		$controllerName = "Nsd_returnsController";
		$functionName = "validateJoomlaUser";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}
		$stamp_start_micro = microtime(true);	

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		#validate user username/pwd

		$options = array();
		$options['remember'] = false;
		$options['return'] = "";
		
		$credentials = array();
		$credentials['username'] = $objInput->username;
		$credentials['password'] = $objInput->password;;
		
		jimport( 'joomla.user.authentication');
		$authenticate = JAuthentication::getInstance();
		$response	  = $authenticate->authenticate($credentials, $options);		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 | ";	
		$logMessage .= "\n\r objInput: ".print_r( $objInput, true );
		$logMessage .= "\n\r response: ".print_r( $response, true );
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		
		

		
		if ( $response->status == 1 )
		{

			$objInputVU = new stdClass();
			$objInputVU->username = $objInput->username;

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 | ";	
			$logMessage .= "\n\r objInputVU: ".print_r( $objInputVU, true );
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	


			$objUser = Nsd_returnsController::getObjUser( $objInputVU );

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3 | ";	
			$logMessage .= "\n\r objUser: ".print_r( $objUser, true );
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				

			

			
			#if ( $objUser->block == 1 || $validGroup == 1 )
			
			if ( $objUser->block == 1  )
			{
	
				$objReturn = new stdClass();
				$objReturn->validUser = 0;
				
			}
			else
			{

				$validGroup = ( in_array("10", $objUser->groups ) ) ? 1 : 0 ;

				#get time of last api call
				$query = "SELECT * from htc_nsd_returns_log WHERE user = '".$objInputVU->username."' and description='Initial Call' ORDER BY id DESC limit 1 ";
				$db->setQuery($query);
		        $objAPILog = $db->loadObject();
	
				$millisecondsNow = round(microtime(true) * 1000);
				
				$diffMilliseconds = $millisecondsNow - $objAPILog->milliseconds;
	
				$validInterval = ( $diffMilliseconds > $objUser->apiinterval ) ? 1 : 0 ;
	
	
				$objReturn = new stdClass();
				$objReturn->id = $objUser->id;
				$objReturn->username = $objUser->username;
				$objReturn->validUser = 1;
	
				$objReturn->arrGroups = $objUser->groups;
				$objReturn->validGroup = $validGroup;
	
				$objReturn->apiInterval = $objUser->apiinterval;
				$objReturn->diffMilliseconds = $diffMilliseconds;
				$objReturn->validInterval = $validInterval;
				
				
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 4 | ";	
				$logMessage .= "\n\r objAPILog: ".print_r( $objAPILog, true );
				$logMessage .= "\n\r millisecondsNow: ".print_r( $millisecondsNow, true );
				$logMessage .= "\n\r objAPILog->milliseconds: ".print_r( $objAPILog->milliseconds, true );
				$logMessage .= "\n\r diffMilliseconds: ".print_r( $diffMilliseconds, true );
				$logMessage .= "\n\r objUser->api-interval: ".print_r( $objUser->apiinterval, true );
				$logMessage .= "\n\r validInterval: ".print_r( $validInterval, true );			
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
				
			}
			
	
			
		}
		else
		{

			$objReturn = new stdClass();
			$objReturn->validUser = 0;
			
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 5 | ";	
		$logMessage .= "\n\r objReturn: ".print_r( $objReturn, true );
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		$logMessage .= "\n\r\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		
		return $objReturn;
	
	}



	public static function getObjUser( $objInput )
	{	
	
		$controllerName = "Nsd_returnsController";
		$functionName = "getObjUser";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "api_prod";
	
		}
		$stamp_start_micro = microtime(true);	

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	


		$objUser = JFactory::getUser( $objInput->username );

		$groups = $objUser->get('groups');

		$objUser->groups = $groups;



		$query = "SELECT f.name, v.value from htc_fields f, htc_fields_values v WHERE f.id = v.field_id AND v.item_id = '".$objUser->id."' and f.state='1' ";
		$db->setQuery($query);
        $objList = $db->loadObjectList();

		foreach( $objList as $obj )
		{
			$name = (string)$obj->name;
			
			$objUser->$name = $obj->value;
			
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ";	
		$logMessage .= "\n\r objInput: ".print_r( $objInput, true );
		$logMessage .= "\n\r query: ".print_r( $query, true );
		$logMessage .= "\n\r objUser: ".print_r( $objUser, true );
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);	
		$logMessage .= "\n\r\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
		
		return $objUser;
		
	
	
	}



	public static function pruneLogs()
	{
		# http://tracking.nonstopdelivery.com/index.php?option=com_nsd_returns&task=pruneLogs
		
		$controllerName = "Nsd_returnsController";
		$functionName = "pruneLogs";
		
		$db = JFactory::getDBO();
		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "api_prod";
	
		}		

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$query = "select count(z.id) from htc_nsd_returns_log z";
		$db->setQuery($query);
		$recordCount = $db->loadResult();


		if ( $recordCount > 35000 && date('H') == '18' )
		{

			$query = "select z.id from htc_nsd_returns_log z order by z.id desc limit 30000, 1";
			$db->setQuery($query);
			$idRecord = $db->loadResult();
	
			$queryD = "Delete FROM htc_nsd_returns_log WHERE id < '".$idRecord."' ";
			$db->setQuery($queryD);
			$db->query();

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r idRecord: ".print_r($idRecord, true);
			$logMessage .= "\n\r queryD: ".print_r($queryD, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
		}


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r recordCount: ".print_r($recordCount, true);
		$logMessage .= "\n\r dateH: ".print_r(date('H'), true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public static function importTMW( $objImport )  //depricated
	{
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=importTMW

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_returns&task=importTMW

		$controllerName = "Nsd_returnsController";
		$functionName = "importTMW";

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
		$tm4web_usr			= $params->get('tm4web_usr');
		$tm4web_pwd			= $params->get('tm4web_pwd');
		$tm4web_subdomain	= $params->get('tm4web_subdomain');

		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#$subdomain 	= "nonstoptest";
		$loginUrl 	= 'https://'.$tm4web_subdomain.'.tmwcloud.com/import/login/';
		#$tm4web_usr = "TM_API";
		#$tm4web_pwd = "981a8e9169fa53dd4fc9b7ab79c3a860";
		#$tm4web_pwd = "e692993a5869a2142769a5eb1883e45f";		
		

		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$objReturnCredentials = json_decode($store);
		
		$sessionId = $objReturnCredentials->sessionId;
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		$logMessage .= "\n\r objReturnCredentials: ".print_r($objReturnCredentials, true);
		$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$query = "SELECT * from htc_nsd_returns_data WHERE id = '".$objImport->dataID."'";
		$db->setQuery($query);
		$objOrderData = $db->loadObject() ;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objOrderData: ".print_r($objOrderData, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		

		$query = "SELECT * from htc_nsd_returns_data_items WHERE data_id = '".$objImport->dataID."'";
		$db->setQuery($query);
		$objOrderDataItems = $db->loadObjectList() ;




		$arrControls = array( 
		"populateClientInfo"=>"TRUE",  		//hard coded
		"populateCommodityInfo"=>"TRUE",  	//hard coded
		"autoAccept"=>"TRUE",  				//hard coded
		"autoPost"=>"TRUE",  				//hard coded
		"autoUpdate"=>"TRUE"  				//hard coded
		);
		
		
		$arrDetails = array(); 
		foreach( $objOrderDataItems as $item )
		{
			$arrDetails[] = array(
				"description"	=>substr($item->goods_description, 0, 80),
				"pieces"		=>substr($item->goods_pieces, 0, 4),
				"piecesUnits"	=>substr($item->goods_code_clean, 0, 3),
				"weight"		=>substr($item->goods_weight, 0, 8 ),
				"weightUnits"	=>"LB"  		//hard coded				
			);
		}


		

		$arrTraces = array(); 
		$arrTraces[] = array( 
		"traceType"=>"P",							//hard coded
		"traceNumber"=>$objOrderData->reference1,
		"refQualifier"=>"PO"							//empty
		);

		$arrTraces[] = array( 
		"traceType"=>"Q",							//hard coded
		"traceNumber"=>$objOrderData->reference2,
		"refQualifier"=>"OMS"							//empty
		);


		if ( $objOrderData->reference3 != "" )
		{
			$arrTraces[] = array( 
			"traceType"=>"R",							
			"traceNumber"=>$objOrderData->reference3,
			"refQualifier"=>""							
			);
		}


		if ( $objOrderData->return_auth != "" )
		{
			$arrTraces[] = array( 
			"traceType"=>"W",							
			"traceNumber"=>$objOrderData->return_auth,
			"refQualifier"=>""							
			);
		}

		
		
		switch( $objOrderData->destination_code )
		{
			case "11106":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8615",
				"refQualifier"=>"SN"
				);
				break;

			case "11107":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8616",
				"refQualifier"=>"SN"
				);
				break;

			case "11108":
				$arrTraces[] = array( 
				"traceType"=>"3",
				"traceNumber"=>"8617",
				"refQualifier"=>"SN"
				);
				break;
			
		}
		
		
		
		
/*
		if ( strtoupper($objOrderData->disposal == "TRUE") || strtoupper($objOrderData->disposal) == "YES" || $objOrderData->disposal == "1" )
		{
			$arrTraces[] = array( 
			"traceType"=>"3",						
			"traceNumber"=>"SHIPINSTRUCT12", 		
			"refQualifier"=>"SHP" 					
			);		
		}
*/
		



		if ( strtoupper($objOrderData->disposal) == "TRUE" || strtoupper($objOrderData->disposal) == "YES" || $objOrderData->disposal == "1" )
		{

			$arrTraces[] = array( 
			"traceType"=>"3",
			"traceNumber"=>"LM",
			"refQualifier"=>"OPS"
			);

			$arrTraces[] = array( 
			"traceType"=>"3",						
			"traceNumber"=>"SHIPINSTRUCT12", 		
			"refQualifier"=>"SHP" 					
			);		
			
		}
		else
		{
			$arrTraces[] = array( 
			"traceType"=>"3",
			"traceNumber"=>"DD",
			"refQualifier"=>"OPS"
			);			
		}




		$arrNotes = array(); 

		if ( $objOrderData->origin_additionalInfo1 != "" )
		{
			$arrNotes[] = array( 
			"noteType"=>"3",
			"theNote"=>$objOrderData->origin_additionalInfo1
			);			
		}


		
		

		$currentDateTime = date("Y-m-d-H.i.s.00000");



		$arrOrders = array();
		$arrOrders[] = array( 
		"billToCode"	=>	"10343",  //hard coded
		"serviceLevel"	=>$objOrderData->service_level_clean,
		"customer"		=>"10343",  //hard coded
		"destination"	=>$objOrderData->destination_code,
		"destname"		=>$objOrderData->destination_name_clean,

		"pickUpBy"		=>$currentDateTime,
		"pickUpByEnd"	=>$currentDateTime,
		
		"deliverBy"		=>$currentDateTime,
		"deliverByEnd"	=>$currentDateTime,

		"destaddr1"		=>$objOrderData->destination_addressLine1." ".$objOrderData->destination_addressLine2,
		"destcity"		=>$objOrderData->destination_city,
		"destprov"		=>$objOrderData->destination_state,
		"destpc"		=>$objOrderData->destination_zipCode,
		"destphone"		=>"",
		"destemail"		=>"",
		"origin"		=>"",
		"origname"		=>substr($objOrderData->origin_name, 0, 40),
		"origaddr1"		=>substr( ($objOrderData->origin_addressLine1." ".$objOrderData->origin_addressLine2), 0, 40),
		"origcity"		=>substr($objOrderData->origin_city, 0, 30),
		"origprov"		=>substr($objOrderData->origin_state, 0, 4),
		"origpc"		=>substr($objOrderData->origin_zipCode, 0, 10),
		"origphone"		=>substr($objOrderData->origin_phone, 0, 20),
		"siteId"		=>"SITE5",   	
		"startZone"		=>substr($objOrderData->origin_zipCode, 0, 10),
		"billTo"		=>"C",  		
		"endZone"		=>substr($objOrderData->destination_zipCode, 0, 10),
		"details"		=>$arrDetails,
		"traces"		=>$arrTraces,
		"notes"			=>$arrNotes, 
		);



		$arrBody = array( 
		"sessionId"=>$sessionId,
		"controls"=>$arrControls,
		"orders"=>$arrOrders
		);
		
		$jsonBody = json_encode($arrBody);


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$createUrl 	= 'https://'.$tm4web_subdomain.'.tmwcloud.com/import/orders/create';
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $createUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$returnData = curl_exec($ch);
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 2 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		

		$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);

		$returnDataJsonDecode = json_decode($returnData);
		
		$json_last_error = json_last_error();
		
		$detailURL = $returnDataJsonDecode[0]->href;
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r createUrl: ".print_r($createUrl, true);
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }






		//init curl
		#$ch = curl_init();
		
		$detailURL = str_replace("http", "https", $detailURL);
		
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $detailURL);
		curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$detailData = curl_exec($ch);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 3 | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




		$detailDataJsonDecode = json_decode($detailData);
		
		$orderId = $detailDataJsonDecode->orderId;
		$billNumber = $detailDataJsonDecode->billNumber;
		
		
		$objImportReturn = new stdClass();
		$objImportReturn->orderId = $orderId;
		$objImportReturn->billNumber = $billNumber;	
		$objImportReturn->message = $returnDataJsonDecode[0]->Message;

		
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r detailData: ".print_r($detailData, true);
		$logMessage .= "\n\r detailDataJsonDecode: ".print_r($detailDataJsonDecode, true);
		$logMessage .= "\n\r orderId: ".print_r($orderId, true);
		$logMessage .= "\n\r billNumber: ".print_r($billNumber, true);
		$logMessage .= "\n\r objImportReturn: ".print_r($objImportReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }





		curl_close($ch);
		unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
		unset($ch);



		


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		return $objImportReturn;
		
	}


	public static function cleanServiceLevel( $service_level )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=cleanServiceLevel
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=cleanServiceLevel

		$controllerName = "Nsd_returnsController";
		$functionName = "cleanServiceLevel";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $service_level != "" )
		{
			$lower_service_level = strtolower( $service_level ); 
	
	
			
			$query = "SELECT * from htc_nsd_returns_lu_service_levels WHERE state = '1' and LOWER(code) = '".$lower_service_level."' ";
			
			
			$db->setQuery($query);
	        $objReturn = $db->loadObject();
	
			$strReturn = ( $objReturn->service_level != "" ) ? $objReturn->service_level : "not_avail" ; 
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
		}
		else
		{
			$strReturn = "not_avail" ;

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		}




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $strReturn;
	}



	public static function cleanGoodsCode( $good_code )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=cleanGoodsCode
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=cleanGoodsCode

		$controllerName = "Nsd_returnsController";
		$functionName = "cleanGoodsCode";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $good_code != "" )
		{
	
	
			$query = "SELECT * from htc_nsd_returns_lu_piece_code WHERE FIND_IN_SET('".$good_code."',description) and state=1 ";
			$db->setQuery($query);
	        $objReturn = $db->loadObject();
	
			$strReturn = ( $objReturn->name != "" ) ? $objReturn->name : "ERR" ; 
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
		}
		else
		{
			$strReturn = "ERR" ;

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			#$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
			
		}




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $strReturn;
	}




	public static function getDestinationCode( $objDestination )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=getDestinationCode
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=getDestinationCode

		$controllerName = "Nsd_returnsController";
		$functionName = "getDestinationCode";
		
		$db = JFactory::getDBO();

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
		$str_new_vendor_alert_email_to = $params->get('new_vendor_alert_email_to');
		$str_new_vendor_alert_email_from = $params->get('new_vendor_alert_email_from');
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

/*
		$left7addcitystzip = substr($objDestination->addressLine1,0,7).$objDestination->city.$objDestination->state.$objDestination->zipCode;
		$left7addcitystzip = str_replace( " ", "", $left7addcitystzip );

			$query = "SELECT * from htc_nsd_returns_lu_hdvendors WHERE left_addcitystzip = '".$left7addcitystzip."' and  state=1 ";
			$db->setQuery($query);
	        $objReturn = $db->loadObject();
	
			$strReturn = ( $objReturn->truckmate_id != "" ) ? $objReturn->truckmate_id : "" ; 
*/


	
	
	
			$left5addcitystzip = strtoupper(substr($objDestination->addressLine1,0,5).$objDestination->city.$objDestination->state.$objDestination->zipCode);		
			
	
			$left7addcitystzip = strtoupper(substr($objDestination->addressLine1,0,7).$objDestination->city.$objDestination->state.$objDestination->zipCode);
			$left7addcitystzip = str_replace( " ", "", $left7addcitystzip );
	

			$query = "SELECT * from htc_nsd_returns_lu_hdvendors WHERE left_addcitystzip = '".$left7addcitystzip."' and  state=1 ";
			$db->setQuery($query);
	        $objReturn = $db->loadObject();
	
			if ( !is_object($objReturn) )
			{

				$objDataInsert = new stdClass();
				$objDataInsert->name = strtoupper($objDestination->name);
				$objDataInsert->address = strtoupper($objDestination->addressLine1);
				$objDataInsert->postal_city = strtoupper($objDestination->city);
				$objDataInsert->postal_state = strtoupper($objDestination->state);
				$objDataInsert->postal_zip = strtoupper($objDestination->zipCode);
				$objDataInsert->left_citystatezip = $left5addcitystzip;
				$objDataInsert->left_addcitystzip = $left7addcitystzip;
				$objDataInsert->state = 1;
				$result = JFactory::getDbo()->insertObject('htc_nsd_returns_lu_hdvendors', $objDataInsert);
				$vendorID = $db->insertid();			

				

				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
				$logMessage .= "\n\r vendorID: ".print_r($vendorID, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


				$body = "";
				$body .= "The following Home Depot vendor was entered into the system.<br><br>Please login to add its Truckmate ID";
				$body .= "<br><br>";
				$body .= json_encode($objDataInsert);
				$body .= "<br><br>Vendor ID: ".$vendorID;


				$objSendEmailMessage = new stdClass();
				$objSendEmailMessage->email_to = $str_new_vendor_alert_email_to;
				$objSendEmailMessage->email_from = $str_new_vendor_alert_email_from;
				$objSendEmailMessage->email_subject = "ALERT | New Home Depot Vendor | ".$objDataInsert->name." | Vendor ID: ".$vendorID;
				$objSendEmailMessage->email_body = $body;
				$objSendEmailMessage->email_category = "Home Depot Return Support";
		
				$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );	

				$strReturn = "" ; 

				
			}
			else
			{
				$strReturn = ( $objReturn->truckmate_id != "" ) ? $objReturn->truckmate_id : "" ; 

			}

	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r query: ".print_r($query, true);
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r strReturn: ".print_r($strReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $strReturn;

		
	}	
	


	public static function evalAdditionalInfo2( $additionalInfo2 )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_returns&task=evalAdditionalInfo2

		$controllerName = "Nsd_returnsController";
		$functionName = "evalAdditionalInfo2";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$needle = "Sender Phone:";
			$arrString = "";
			#$return = "";
			
			
			$objReturn = new JObject;
			$objReturn->isPhoneNumber = "no";		
			$objReturn->additionalInfo2 = $additionalInfo2;
			$objReturn->phone = "";
			
			if ( stripos( strtolower( $additionalInfo2 ), $needle ) !== false )
			{
				
				$arrString = explode( ":", $additionalInfo2 );
				
				$objReturn->isPhoneNumber = "yes";
				$objReturn->additionalInfo2 = "";
				$objReturn->phone = trim($arrString[1]);
			}



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r additionalInfo2: ".print_r($additionalInfo2, true);
		$logMessage .= "\n\r arrString: ".print_r($arrString, true);
		$logMessage .= "\n\r needle: ".print_r($needle, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}	
	
	
// !TEST


	public function test_poc_tmapi( $file_name=null )
	{
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_poc_tmapi

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_poc_tmapi

		$controllerName = "Nsd_returnsController";
		$functionName = "test_poc_tmapi";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$subdomain 	= "nonstoptest";
		$loginUrl 	= 'https://'.$subdomain.'.tmwcloud.com/import/login/';
		$tm4web_usr = "TM_API";
		$tm4web_pwd = "981a8e9169fa53dd4fc9b7ab79c3a860";
		$tm4web_pwd = "e692993a5869a2142769a5eb1883e45f";		
		

		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$objReturnCredentials = json_decode($store);
		
		$sessionId = $objReturnCredentials->sessionId;
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		$logMessage .= "\n\r objReturnCredentials: ".print_r($objReturnCredentials, true);
		$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrControls = array( 
		"populateClientInfo"=>"TRUE",  		//hard coded
		"populateCommodityInfo"=>"TRUE",  	//hard coded
		"autoAccept"=>"TRUE",  				//hard coded
		"autoPost"=>"TRUE",  				//hard coded
		"autoUpdate"=>"TRUE"  				//hard coded
		);
		
		
		$arrDetails = array(); 
		$arrDetails[] = array( 
		"description"=>"Bed",
		"pieces"=>"2",
		"piecesUnits"=>"CTN",
		"weight"=>"250",
		"weightUnits"=>"LB" 		//hard coded
		);
		$arrDetails[] = array( 
		"description"=>"Bed Frame",
		"pieces"=>"2",
		"piecesUnits"=>"CTN",
		"weight"=>"350",
		"weightUnits"=>"LB"  		//hard coded
		);

		

		$arrTraces = array(); 
		$arrTraces[] = array( 
		"traceType"=>"P",
		"traceNumber"=>"baseball",
		"refQualifier"=>""
		);
		$arrTraces[] = array( 
		"traceType"=>"O",
		"traceNumber"=>"456",
		"refQualifier"=>""
		);
		$arrTraces[] = array( 
		"traceType"=>"W",
		"traceNumber"=>"789",
		"refQualifier"=>""
		);
		$arrTraces[] = array( 
		"traceType"=>"S",
		"traceNumber"=>"SHIPINSTRUCT12", 	//hard coded
		"refQualifier"=>"SHP" 				//hard coded
		);		

		$currentDateTime = date("Y-m-d-H.i.s.00000");

		$arrOrders = array();
		$arrOrders[] = array( 
		"billToCode"=>"10343",  						//hard coded
		"serviceLevel"=>"WHITEGLOVE",
		"customer"=>"10020",  							//hard coded
		"destname"=>"Steve Stevenson",
		"pickUpBy"=>"2018-07-07-13.14.15.00000",
		"pickUpByEnd"=>"2018-07-07-13.14.15.00000",
		"deliverBy"=>$currentDateTime,
		"deliverByEnd"=>$currentDateTime,
		"destaddr1"=>"123 Main Street",
		"destcity"=>"Boston",
		"destprov"=>"MA",
		"destpc"=>"02134",
		"destphone"=>"",
		"destemail"=>"",
		"origin"=>"",
		"origname"=>"Rob Robertson",
		"origaddr1"=>"999 Ninth Street",
		"origaddr2"=>"",
		"origcity"=>"Wyoming",
		"origprov"=>"RI",
		"origpc"=>"02898", //58101
		"origphone"=>"8005551212",
		"siteId"=>"SITE6",   							//hard coded
		"startZone"=>"02898",
		"billTo"=>"C",  								//hard coded
		"endZone"=>"02134",
		"details"=>$arrDetails,
		"traces"=>$arrTraces 
		);



		$arrBody = array( 
		"sessionId"=>$sessionId,
		"controls"=>$arrControls,
		"orders"=>$arrOrders
		);
		
		$jsonBody = json_encode($arrBody);




		$createUrl 	= 'https://'.$subdomain.'.tmwcloud.com/import/orders/create';
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $createUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$returnData = curl_exec($ch);

		$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);

		$returnDataJsonDecode = json_decode($returnData);
		
		$json_last_error = json_last_error();
		
		$detailURL = $returnDataJsonDecode[0]->href;
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r createUrl: ".print_r($createUrl, true);
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }






		//init curl
		#$ch = curl_init();
		
		$detailURL = str_replace("http", "https", $detailURL);
		
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $detailURL);
		
		// ENABLE HTTP POST
		//curl_setopt($ch, CURLOPT_POST, 0);
		
		//Set the post parameters
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		
		//Handle cookies for the login
		//curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		//curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		//curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		
		curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$detailData = curl_exec($ch);

		$detailDataJsonDecode = json_decode($detailData);
		
		$orderId = $detailDataJsonDecode->orderId;
		$billNumber = $detailDataJsonDecode->billNumber;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r detailData: ".print_r($detailData, true);
		$logMessage .= "\n\r detailDataJsonDecode: ".print_r($detailDataJsonDecode, true);
		$logMessage .= "\n\r orderId: ".print_r($orderId, true);
		$logMessage .= "\n\r billNumber: ".print_r($billNumber, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }





		curl_close($ch);
		unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
		unset($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public function test_poc_tmapi_update( $file_name=null )
	{
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_poc_tmapi_update

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_poc_tmapi_update

		$controllerName = "Nsd_returnsController";
		$functionName = "test_poc_tmapi_update";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$subdomain 	= "nonstoptest";
		$loginUrl 	= 'https://'.$subdomain.'.tmwcloud.com/import/login/';
		$tm4web_usr = "TM_API";
		$tm4web_pwd = "981a8e9169fa53dd4fc9b7ab79c3a860";
		$tm4web_pwd = "e692993a5869a2142769a5eb1883e45f";		
		

		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$objReturnCredentials = json_decode($store);
		
		$sessionId = $objReturnCredentials->sessionId;
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		$logMessage .= "\n\r objReturnCredentials: ".print_r($objReturnCredentials, true);
		$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrControls = array( 
		"populateClientInfo"=>"FALSE",  		//hard coded
		"populateCommodityInfo"=>"FALSE",  	//hard coded
		"autoAccept"=>"TRUE",  				//hard coded
		"autoPost"=>"TRUE",  				//hard coded
		"autoUpdate"=>"TRUE"  				//hard coded
		);

		$currentDateTime = date("Y-m-d-H.i.s.00000");

		$arrOrders = array();
		$arrOrders[] = array( 
		"billToCode"=>"10343",
		//"billNumber"=>"31147809",
		"customer"=>"10020", 		
		"orderId"=>"137392",  						//hard coded
		"origcity"=>"Wyomingg",
		"deliverBy"=>$currentDateTime,
		"deliverByEnd"=>$currentDateTime,
		);



		$arrBody = array( 
		"sessionId"=>$sessionId,
		"controls"=>$arrControls,
		"orders"=>$arrOrders
		);
		
		$jsonBody = json_encode($arrBody);




		$createUrl 	= 'https://'.$subdomain.'.tmwcloud.com/import/orders/update';
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $createUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$returnData = curl_exec($ch);

		$returnData = str_replace (array("\r\n", "\n", "\r"), ' ', $returnData);

		$returnDataJsonDecode = json_decode($returnData);
		
		$json_last_error = json_last_error();
		
		$detailURL = $returnDataJsonDecode[0]->href;
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r createUrl: ".print_r($createUrl, true);
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r returnDataJsonDecode: ".print_r($returnDataJsonDecode, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r json_last_error: ".print_r($json_last_error, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }






		//init curl
		#$ch = curl_init();
		
		$detailURL = str_replace("http", "https", $detailURL);
		
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $detailURL);
		
		// ENABLE HTTP POST
		//curl_setopt($ch, CURLOPT_POST, 0);
		
		//Set the post parameters
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
		
		//Handle cookies for the login
		//curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		//curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		//curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		
		curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$detailData = curl_exec($ch);

		$detailDataJsonDecode = json_decode($detailData);
		
		$orderId = $detailDataJsonDecode->orderId;
		$billNumber = $detailDataJsonDecode->billNumber;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r detailData: ".print_r($detailData, true);
		$logMessage .= "\n\r detailDataJsonDecode: ".print_r($detailDataJsonDecode, true);
		$logMessage .= "\n\r orderId: ".print_r($orderId, true);
		$logMessage .= "\n\r billNumber: ".print_r($billNumber, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }





		curl_close($ch);
		unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
		unset($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function createReturnOrderJson()
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=createReturnOrderJson

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_returns&task=createReturnOrderJson

		$controllerName = "Nsd_returnsController";
		
		
		$functionName = "createReturnOrderJson";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
		$arrControls = array( 
		"populateClientInfo"=>"TRUE",  		//hard coded
		"populateCommodityInfo"=>"TRUE",  	//hard coded
		"autoAccept"=>"TRUE",  				//hard coded
		"autoPost"=>"TRUE",  				//hard coded
		"autoUpdate"=>"TRUE"  				//hard coded
		);
		$jsonControls = json_encode($arrControls);
		


		
		
		$arrDetails = array(); 
		$arrDetails[] = array( 
		"description"=>"Bed",
		"pieces"=>"2",
		"piecesUnits"=>"CTN",
		"weight"=>"250",
		"weightUnits"=>"LB" 		//hard coded
		);
		$arrDetails[] = array( 
		"description"=>"Bed Frame",
		"pieces"=>"2",
		"piecesUnits"=>"CTN",
		"weight"=>"350",
		"weightUnits"=>"LB"  		//hard coded
		);
		$jsonDetails = json_encode($arrDetails);
		

		$arrTraces = array(); 
		$arrTraces[] = array( 
		"traceType"=>"P",
		"traceNumber"=>"123",
		"refQualifier"=>""
		);
		$arrTraces[] = array( 
		"traceType"=>"O",
		"traceNumber"=>"456",
		"refQualifier"=>""
		);
		$arrTraces[] = array( 
		"traceType"=>"W",
		"traceNumber"=>"789",
		"refQualifier"=>""
		);
		$arrTraces[] = array( 
		"traceType"=>"S",
		"traceNumber"=>"SHIPINSTRUCT12", 	//hard coded
		"refQualifier"=>"SHP" 				//hard coded
		);		
		$jsonTracess = json_encode($arrTraces);		



		$currentDateTime = date("Y-m-d-H.i.s.00000");

		$arrOrders = array();
		$arrOrders[] = array( 
		"billToCode"=>"10343",  						//hard coded
		"serviceLevel"=>"WHITEGLOVE",
		"customer"=>"10343",  							//hard coded
		"destname"=>"The Home Depot Direct-YOW",
		"pickUpBy"=>"2018-07-07-13.14.15.00000",
		"pickUpByEnd"=>"2018-07-07-13.14.15.00000",
		"deliverBy"=>$currentDateTime,
		"deliverByEnd"=>$currentDateTime,
		"destaddr1"=>"2929 LONGHORN BOULEVARD",
		"destcity"=>"AUSTIN",
		"destprov"=>"TX",
		"destpc"=>"78758",
		"destphone"=>"",
		"destemail"=>"",
		"origin"=>"",
		"origname"=>"Rob Robertson",
		"origaddr1"=>"999 Ninth Street",
		"origaddr2"=>"",
		"origcity"=>"Fargo",
		"origprov"=>"ND",
		"origpc"=>"54321",
		"origphone"=>"8005551212",
		"siteId"=>"SITE6",   							//hard coded
		"startZone"=>"54321",
		"billTo"=>"C",  								//hard coded
		"endZone"=>"78758",
		"details"=>$arrDetails,
		"traces"=>$arrTraces 
		);
		
		$jsonOrders = json_encode($arrOrders);



		$arrBody = array( 
		"sessionid"=>"2101",
		"controls"=>$arrControls,
		"orders"=>$arrOrders
		);
		
		$jsonBody = json_encode($arrBody);
		
		$jsonDecodeBody = json_decode($jsonBody);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r";
		$logMessage .= "\n\r jsonDecodeBody: ".print_r($jsonDecodeBody, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$display = "";		
	
		$display .= "<pre>";	
		$display .= print_r($jsonBody, true);		
		$display .= "</pre>";		


		$display .= "<pre>";	
		$display .= print_r($jsonDecodeBody, true);		
		$display .= "</pre>";		


		echo $display;	
		


		$logMessage = "END | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function createReturnOrderJsonPostman()
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=createReturnOrderJsonPostman
		
		$functionName = "createReturnOrderJsonPostman";
		
		$db =& JFactory::getDBO();
		
		$logger = 0;
		$logFile = "metalake";	
		$loggerProd = 0;
		$logFileProd = "production";	

		$logMessage = "START | Ml_apiController | ".$functionName;	
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }	
		
		
		$arrOrigin = array( 
		"name"=>"John Doe",
		"addressLine1"=>"2455 W. Paces Ferry Road",
		"addressLine2"=>"Unit 10",
		"additionalInfo1"=>"Use front entrance",
		"additionalInfo2"=>"",
		"city"=>"ATLANTA",
		"zipCode"=>"30339",
		"state"=>"GA",
		"country"=>""
		);
		$jsonOrigin = json_encode($arrOrigin);
		
		$arrDestination = array( 
		"name"=>"ZAMMA CORPORATION",
		"addressLine1"=>"14468 LITCHFIELD DRIVE",
		"addressLine2"=>"",
		"additionalInfo1"=>"",
		"additionalInfo2"=>"",
		"city"=>"ORANGE",
		"zipCode"=>"22960",
		"state"=>"VA",
		"country"=>""
		);
		$jsonDestination = json_encode($arrDestination);
		
		$arrGoods = array(); 
		$arrGoods[] = array( 
		"pieces"=>"2",
		"weight"=>"15",
		"length"=>"10",
		"width"=>"10",
		"height"=>"10",
		"description"=>"Carton of Gizmos",
		"code"=>"CTN"
		);
		$arrGoods[] = array( 
		"pieces"=>"1",
		"weight"=>"20",
		"length"=>"5",
		"width"=>"6",
		"height"=>"7",
		"description"=>"Box of things",
		"code"=>"BOX"
		);
		$arrGoods[] = array( 
		"pieces"=>"1",
		"weight"=>"10",
		"length"=>"15",
		"width"=>"60",
		"height"=>"7",
		"description"=>"Pallet of Widgets",
		"code"=>"PLT"
		);		
		$jsonGoods = json_encode($arrGoods);
		
		
/*
		$arrBody = array( 
		"customerCode"=>"2101",
		"callerName"=>"Fred",
		"phone"=>"212-212-2121",
		"reference1"=>"015106341",
		"reference2"=>"W1510142001",
		"reference3"=>"reference3",
		"reference4"=>"reference4",
		"disposal"=>"true",
		"returnAuth"=>"500160211",
		"localService"=>"T1R",
		"pickupTime"=>"2013-12-13 18:30",
		"origin"=>$jsonOrigin,
		"destination"=>$jsonDestination,
		"goods"=>$jsonGoods,
		);
*/


		$arrBody = array( 
		"customerCode"=>"2101",
		"callerName"=>"Sally",
		"phone"=>"800-212-2121",
		"reference1"=>"315106341",
		"reference2"=>"W1510142001",
		"reference3"=>"reference3",
		"reference4"=>"reference4",
		"disposal"=>"true",
		"returnAuth"=>"500160211",
		"localService"=>"T1R",
		"pickupTime"=>"2013-12-13 18:30",
		"origin"=>$arrOrigin,
		"destination"=>$arrDestination,
		"goods"=>$arrGoods,
		);
		
		$jsonBody = json_encode($arrBody);
		
		$jsonDecodeBody = json_decode($jsonBody);

		$logMessage = "INSIDE | Ml_apiController | ".$functionName;
		$logMessage .= "\n\r jsonBody: ".print_r($jsonBody, true);
		$logMessage .= "\n\r";
		$logMessage .= "\n\r jsonDecodeBody: ".print_r($jsonDecodeBody, true);
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

		$display = "";		
	
		$display .= "<pre>";	
		$display .= print_r($jsonBody, true);		
		$display .= "</pre>";		


		$display .= "<pre>";	
		$display .= print_r($jsonDecodeBody, true);		
		$display .= "</pre>";		


		echo $display;	
		


		$logMessage = "END | Ml_apiController | ".$functionName;	
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }	

		
	}	



	public function test_curl( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_curl
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=test_curl

		$controllerName = "Nsd_returnsController";
		$functionName = "test_curl";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		//init curl
		$ch = curl_init();
		
		$detailURL = "https://nonstoptest.tmwcloud.com/import/orders/135237";
		#$detailURL = "https://www.metalake.com/";
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $detailURL);
		
		curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$detailData = curl_exec($ch);

		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
		}
		$curlError = curl_error($ch);


		$detailDataJsonDecode = json_decode($detailData);
		
		$orderId = $detailDataJsonDecode->orderId;
		$billNumber = $detailDataJsonDecode->billNumber;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
		$logMessage .= "\n\r detailData: ".print_r($detailData, true);
		$logMessage .= "\n\r detailDataJsonDecode: ".print_r($detailDataJsonDecode, true);
		$logMessage .= "\n\r orderId: ".print_r($orderId, true);
		$logMessage .= "\n\r billNumber: ".print_r($billNumber, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }





		curl_close($ch);
		unset($ch);

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


	public function test_poc_204( $file_name )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_poc_204
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=test_poc_204

		$controllerName = "Nsd_returnsController";
		$functionName = "test_poc_204";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		/*
		When calling the TM4Web Dispatch 204 Web Service 2 parameters are required:
		
		Access String: Generated in Profiles > Tools Menu > Web Services > 204 Activation Key. This parameter is required for security purposes when calling the web service, so that not just anyone can create orders. 
		
		FreightBillArray: This complex data structure must contain all the data for creating the Order. The complex type TFreightBill is defined in the WSDL document.


		The return will be in the form of an array containing a boolean result, the unique record id (detail_line_id), if no error occurred, and an error description, should an error occur.

		*/


		$wsdl = "https://nonstoptest.tmwcloud.com/webservices/dispatch_workloads_204/index.msw?wsdl";

		$arrSoapClient = array('connection_timeout' => 0, 'trace' => 1, 'compression' => 'SOAP_COMPRESSION_ACCEPT');


		$soap_client = new SoapClient( $wsdl, $arrSoapClient );


		$accessStr = "7c36df236f3ef86382cf36858979bb7b";

		$params_204 = Nsd_returnsController::poc_204_set_parameters();


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r arrSoapClient: ".print_r($arrSoapClient, true);
			$logMessage .= "\n\r soap_client: ".print_r($soap_client, true);
			$logMessage .= "\n\r accessStr: ".print_r($accessStr, true);
			$logMessage .= "\n\r params_204: ".print_r($params_204, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		try 
		{
			$LoadOffer = $soap_client->SetLoadOffer($accessStr, $params_204);
		} 
		catch (SoapFault $exception) 
		{
			#echo '<br><br>EXCEPTION = <br>'.$exception;
			#echo "<br><br>EX RESPONSE: " . $soap_client->__getLastResponse() ."<br/>";
			#echo "<br/><br/>exception->getMessage: ".$exception->getMessage() ."<br/>";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r exception: ".print_r($exception, true);
			$logMessage .= "\n\r exception response: ".print_r($soap_client->__getLastResponse(), true);
			$logMessage .= "\n\r exception->getMessage: ".print_r($exception->getMessage(), true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		}
		
		
		if(isset($LoadOffer))
		{
		
			#echo "<br/><br/> New LoadOffer: ";
			#print_r($LoadOffer);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r New LoadOffer: ".print_r($LoadOffer, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		}
		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


# 

	public function poc_204_set_parameters()
	{
			$dtl[]= array(
				'order_id' => NULL,
				'commodity' => 'FAK'
				);

			$caller = array(
				'internal_id' => '12'
				);


			$arrDetails = array(); 
				$arrDetails[] = array(
				'order_id' => NULL, 
				"description"=>"Bed",
				"pieces"=>"2",
				"pieces_units"=>"CTN",
				"weight"=>"250",
				"weight_units"=>"LB" 		//hard coded
				);
				$arrDetails[] = array( 
				'order_id' => NULL,	
				"description"=>"Bed Frame",
				"pieces"=>"2",
				"pieces_units"=>"CTN",
				"weight"=>"350",
				"weight_units"=>"LB"  		//hard coded
				);
		
		
/*
			$origin = array(
				'name' => 'Homer D. Poe',
				'address_1' => '2455 W. Paces Ferry Road',
				'city' => 'ATLANTA',
				'province' => 'GA',
				'postal_code' => '30339'
				);
*/


			$origin = array(
				'internal_id' => '10343'
				);



				
/*
			$destination = array(
				'name' => 'The Home Depot Direct-YOW',
				'address_1' => '16500 Hunters Green Pkwy',
				'city' => 'HAGERSTOWN',
				'province' => 'MD',
				'postal_code' => '21740'
				);	
*/					

			$destination = array(
				'internal_id' => '10008'
				);



			$arrTrace = array(); 
				$arrTrace[] = array(
					'PurchaseOrder' => '888777666',
					'UserTrace3' => '333222111'
					);

		
		
			$currentDateTime = date("Y-m-d-H.i.s.00000");
		
			$fb = array(
				'order_type' 	    => 'T',
				//'mode_code'  		=> 'NEW',
				'bill_to_code' 		=> '10343',  // hard coded
				'CustomsNumber' 	=> '10020',  // hard coded
				'pick_up_by' 		=> '2018-07-07-13.14.15.00000',
				'pick_up_by_end' 	=> '2018-07-07-13.14.15.00000',
				'deliver_by'	 	=> $currentDateTime,
				'deliver_by_end' 	=> $currentDateTime,
				'site_id' 			=> 'SITE5',
				'bill_to'	 		=> 'C',  // hard coded
				'start_zone' 		=> '30339',
				'end_zone' 			=> '21740',
				'service_level'		=> 'WHITEGLOVE',
				'caller'     		=> $origin,
				//'caller'     		=>  NULL,
				'shipper'    		=> $origin,
				//'shipper'    		=> NULL,				
				'consignee'  		=> $destination,
				//'consignee'  		=> NULL,
				'master_order'		=> NULL,
				'extra_stops'		=> 'False',
				'FBDetails'			=> $arrDetails,
				'FBTraceNumbers'	=> $arrTrace
				);
		
		       
			$a_fb[] = $fb;
			  return $a_fb;
		}





	public function test_poc_trace( $file_name )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_poc_trace&trace=R1147757
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=test_poc_trace

		$controllerName = "Nsd_returnsController";
		$functionName = "test_poc_trace";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$jinput = JFactory::getApplication()->input;
		$trace_number = $jinput->get('trace', '', RAW);


		$wsdl = "https://nonstoptest.tmwcloud.com/webservices/ws_trace/index.msw?wsdl";

		$arrSoapClient = array('connection_timeout' => 0, 'trace' => 1, 'compression' => 'SOAP_COMPRESSION_ACCEPT');


		$soap_client = new SoapClient( $wsdl, $arrSoapClient );



		$params_trace = Nsd_returnsController::poc_trace_set_parameters( $trace_number );


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r arrSoapClient: ".print_r($arrSoapClient, true);
			$logMessage .= "\n\r soap_client: ".print_r($soap_client, true);
			$logMessage .= "\n\r params_trace: ".print_r($params_trace, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		try 
		{
			$Trace_Result = $soap_client->Trace( $params_trace );
		} 
		catch (SoapFault $exception) 
		{
			#echo '<br><br>EXCEPTION = <br>'.$exception;
			#echo "<br><br>EX RESPONSE: " . $soap_client->__getLastResponse() ."<br/>";
			#echo "<br/><br/>exception->getMessage: ".$exception->getMessage() ."<br/>";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r exception: ".print_r($exception, true);
			$logMessage .= "\n\r exception response: ".print_r($soap_client->__getLastResponse(), true);
			$logMessage .= "\n\r exception->getMessage: ".print_r($exception->getMessage(), true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		}
		
		
		if( isset( $Trace_Result ) )
		{
		
			#echo "<br/><br/> New LoadOffer: ";
			#print_r($LoadOffer);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r Trace_Result: ".print_r($Trace_Result, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		}
		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}






	public function poc_trace_set_parameters( $trace_number )
	{

		//$trace_attributes = "T00113,~|L00031,~";
		#$trace_attributes = "R1147751,~";
		$trace_attributes = $trace_number.",~";
		
		
		//$output_attributes = "TLORDER=UNIQUE_ID, USER3,DETAIL_LINE_ID,TOTAL_CHARGES";
		
		//$output_attributes = "TLORDER=DETAIL_LINE_ID, USER3, CREATED_TIME,CREATED_BY, CALLER, PICK_UP_BY, DELIVER_BY, SERVICE_LEVEL, CURRENT_ZONE,START_ZONE, END_ZONE, BILL_NUMBER, BILL_TO_CODE, BILL_TO, DECLARED_VALUE,TOTAL_CHARGES, CURRENCY_CODE"; //|ACHARGE_TLORDER=ACODE_ID";
		
		//$output_attributes = "TLORDER=BILL_NUMBER, TOTAL_CHARGES,CURRENCY_CODE|TLDTL=RATE";
		
		
		
		$arrOuput = array();
		
		#$arrOuput[] = "order_type";  /don't use
		$arrOuput[] = "BILL_NUMBER";
		#$arrOuput[] = "bill_to_code";
		#$arrOuput[] = "CustomsNumber";
		$arrOuput[] = "pick_up_by";
		$arrOuput[] = "pick_up_by_end";
		$arrOuput[] = "deliver_by";
		$arrOuput[] = "deliver_by_end";
		$arrOuput[] = "site_id";
		$arrOuput[] = "bill_to";
		$arrOuput[] = "start_zone";
		$arrOuput[] = "end_zone";
		$arrOuput[] = "service_level";
		$arrOuput[] = "caller";
		$arrOuput[] = "shipper";
		$arrOuput[] = "consignee";
		#$arrOuput[] = "master_order";
		#$arrOuput[] = "extra_stops";
		#$arrOuput[] = "FBDetails";
		$arrOuput[] = "FBTraceNumbers";


		sort($arrOuput);		
		
		$strOutput = implode( ', ', $arrOuput );
		
		$strOutput = strtoupper($strOutput);
		
		$output_attributes = "TLORDER=".$strOutput;
		
		
		
		#$output_attributes = "TLORDER=BILL_NUMBER, USER3, DELIVER_BY, site_id, caller";
		
		
		$parameters['trace_attributes'] = $trace_attributes;
		
		$parameters['output_attributes'] = $output_attributes;
		
		return $parameters;

	}


	public function test_substr( $file_name=null )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_returns&task=test_substr

		$controllerName = "Nsd_returnsController";
		$functionName = "test_substr";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		for ( $x = 0; $x < 10; $x++ )
		{
			$string = "abcdefghij";
			#$string = "";
			$substring = substr( $string, 0, $x );

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r string: ".print_r($string, true);
			$logMessage .= "\n\r x: ".print_r($x, true);
			$logMessage .= "\n\r substring: ".print_r($substring, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
			
		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public static function test_jsonreturn( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_jsonreturn
		# http://dev6.metalake.net/index.php?option=com_nsd_returns&task=test_jsonreturn

		$controllerName = "Nsd_returnsController";
		$functionName = "test_jsonreturn";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$orderId = 1233456;
		$billNumber = "R123456";

		$objImportReturn = new stdClass();
		$objImportReturn->orderId = $orderId;
		$objImportReturn->billNumber = $billNumber;	
		$objImportReturn->message = "this is a message";


		#$arrReturn = (array)$objImportReturn;
		#$jsonReturn = json_encode($arrReturn);		

		$jsonReturn = json_encode($objImportReturn);	
		#$jsonReturn = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $jsonReturn);  // remove double quotes from keys

		$arrFind = array('"billNumber":"', '","message"');
		$arrReplace = array('"billNumber":', ',"message"');


		$jsonReturn = str_replace( $arrFind, $arrReplace, $jsonReturn );


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objImportReturn: ".print_r($objImportReturn, true);
		$logMessage .= "\n\r arrReturn: ".print_r($arrReturn, true);
		$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public static function test_create_vendor( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_create_vendor
		# http://dev6.metalake.net/index.php?option=com_nsd_returns&task=test_create_vendor

		$controllerName = "Nsd_returnsController";
		$functionName = "test_create_vendor";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$destinationCode = Nsd_returnsController::getDestinationCode( $objDestination );

		$objDestination = new stdClass();
/*
		$objDestination->name = "SPACELY SPROCKETS";
		$objDestination->addressLine1 = "888 MEADOWBROOK ROAD";
		$objDestination->city = "ARLINGTON";
		$objDestination->state = "VA";
		$objDestination->zipCode = "22207";
*/
		
		$objDestination->name = "NOVA Baseball Inc";
		$objDestination->addressLine1 = "888 Pinehaven Drive";
		$objDestination->city = "Arlington";
		$objDestination->state = "VA";
		$objDestination->zipCode = "22207";		
		
		
		$left5addcitystzip = strtoupper(substr($objDestination->addressLine1,0,5).$objDestination->city.$objDestination->state.$objDestination->zipCode);		
		

		$left7addcitystzip = strtoupper(substr($objDestination->addressLine1,0,7).$objDestination->city.$objDestination->state.$objDestination->zipCode);
		$left7addcitystzip = str_replace( " ", "", $left7addcitystzip );



		$objDataInsert = new stdClass();
		$objDataInsert->name = strtoupper($objDestination->name);
		$objDataInsert->address = strtoupper($objDestination->addressLine1);
		$objDataInsert->postal_city = strtoupper($objDestination->city);
		$objDataInsert->postal_state = strtoupper($objDestination->state);
		$objDataInsert->postal_zip = strtoupper($objDestination->zipCode);
		$objDataInsert->left_citystatezip = $left5addcitystzip;
		$objDataInsert->left_addcitystzip = $left7addcitystzip;
		$objDataInsert->state = 1;
		#$result = JFactory::getDbo()->insertObject('htc_nsd_returns_lu_hdvendors', $objDataInsert);
		#$activityID = $db->insertid();		



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objDestination: ".print_r($objDestination, true);
		$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




	public function test_properties( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_properties
		# http://dev6.metalake.net/index.php?option=com_nsd_returns&task=test_properties

		$controllerName = "Nsd_returnsController";
		$functionName = "test_properties";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$jsonString = '{"customerCode":"2101","callerName":"Sally","phone":"800-212-2121","reference1":"315106341","reference2":"W1510142001","reference3":"reference3","reference4":"reference4","disposal":"true","returnAuth":"500160211","localService":"LTL","pickupTime":"2018-07-13 18:30","origin":{"name":"Homer D. Poe","addressLine1":"2455 W. Paces Ferry Road","addressLine2":"Unit 10","additionalInfo1":"Use front entrance","additionalInfo2":"","city":"ATLANTA","zipCode":"30339","state":"GA","country":""},"destination":{"name":"NOVA Baseball Inc","addressLine1":"888 Meadowbrook Road","addressLine2":"","additionalInfo1":"","additionalInfo2":"","city":"Arlington","zipCode":"22207","state":"VA","country":""},"goods":[{"pieces":"2","weight":"15","length":"10","width":"10","height":"10","description":"Carton of stuff","code":"Carton"},{"pieces":"1","weight":"20","length":"5","width":"6","height":"7","description":"Box of stuff","code":""},{"pieces":"1","weight":"10","length":"15","width":"60","height":"7","description":"Bof of things","code":"PLT"}]}';
		
		
		$jsondecodeJsonString = json_decode($jsonString);

		$origin = $jsondecodeJsonString->origin;		
		#$origin_zipCode = property_exists( 'jsondecodeJsonString->origin','zipCode' ) ? $origin->zipCode : "z";
		$origin_zipCode = isset( $origin->zipCode ) ? $origin->zipCode : "z";
		$origin_alt_phone = isset( $origin->altPhone ) ? "y" : "z" ;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r jsonString: ".print_r($jsonString, true);
		$logMessage .= "\n\r jsondecodeJsonString: ".print_r($jsondecodeJsonString, true);
		$logMessage .= "\n\r origin: ".print_r($origin, true);
		$logMessage .= "\n\r origin_zipCode: ".print_r($origin_zipCode, true);
		$logMessage .= "\n\r origin_alt_phone: ".print_r($origin_alt_phone, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public static function test_evalAdditionalInfo2( )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_returns&task=test_evalAdditionalInfo2

		$controllerName = "Nsd_returnsController";
		$functionName = "test_evalAdditionalInfo2";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$additionalInfo2 = "Sender Phone: 8325096399";

		$objReturn = Nsd_returnsController::evalAdditionalInfo2( $additionalInfo2 );


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r additionalInfo2: ".print_r($additionalInfo2, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


	public static function test_evalHazmat( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_function_template
		# http://dev6.metalake.net/index.php?option=com_nsd_returns&task=test_evalHazmat

		$controllerName = "Nsd_returnsController";
		$functionName = "test_evalHazmat";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objInput = new JObject;
		$objInput->haystack = "HAzMAT - 123 MAIN STREET";
		$objInput->needle = "HAZMAT";

		$objReturn = new JObject;


			if ( strpos( strtoupper($objInput->haystack), strtoupper($objInput->needle) ) !== false )
			{
				
				$objReturn->result = "found";
			}
			else
			{
				$objReturn->result = "not found";
			}


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



// !FUNCTION TEMPLATE

	public static function test_function_template( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_returns&task=test_function_template
		# http://deve.metalake.net/index.php?option=com_nsd_returns&task=test_function_template

		$controllerName = "Nsd_returnsController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "returns_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "returns_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "returns_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r aaa: ".print_r($aaa, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


}
