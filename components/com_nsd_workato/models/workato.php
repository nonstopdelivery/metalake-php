<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_workato
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Nsd_workato model.
 *
 * @since  1.6
 */
class Nsd_workatoModelWorkato extends JModelItem
{

}
