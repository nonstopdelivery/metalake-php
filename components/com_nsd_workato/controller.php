<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_workato
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');

/**
 * Class Nsd_workatoController
 *
 * @since  1.6
 */
class Nsd_workatoController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'logs');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}


// !WORKATO


	public static function updateTruckmateOrderScheduling( $objData )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=updateTruckmateOrderShipper


		$controllerName = "Nsd_workatoController";
		$functionName = "updateTruckmateOrderScheduling";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();

		$paramsComponent->workato_api_token = $params->get('workato_api_token');

		$paramsComponent->workato_api_endpoint_truckmate_update_order_scheduling = $params->get('workato_api_endpoint_truckmate_update_order_scheduling');

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		$urlToPost = $paramsComponent->workato_api_endpoint_truckmate_update_order_scheduling;

		$postPayload = $objData;
	
		$jsonPostPayload = json_encode($postPayload);
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r paramsComponent->workato_api_token: ".print_r($paramsComponent->workato_api_token, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		$logMessage .= "\n\r strRow: ".print_r($strRow, true);		
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".print_r($strRow, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
		$stamp_start_micro_beacon1 = microtime(true);
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Execute ";
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			

		$returnData = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro_beacon1 = ($stamp_end_micro - $stamp_start_micro_beacon1);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Close | seconds: ".number_format($stamp_total_micro_beacon1,2);;
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->system_message = "CURL ERROR";
			$objReturn->note = "Curl Error: ".print_r($error, true);
			$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnFromWorkato->errors[0]->field. " | ".$objReturnFromWorkato->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
								
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";			
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a the server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
				
					break;

														
			}

			
			$jsonReturn = json_encode($objReturn);
			
			$jsonReturnFromWorkato = json_encode($objReturnFromWorkato);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Result: ".$arrCurlHeader["http_code"]. " | ".$objReturn->system_message." | ".$jsonReturnFromWorkato;
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "updateTruckmateOrderScheduling";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_payload = $jsonPostPayload;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturnFromWorkato: ".print_r($jsonReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		




		return $objReturn;
	}



// !WORKATO - MMO

	public static function insertMMOStaging( $objData )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=insertMMOStaging


		$controllerName = "Nsd_workatoController";
		$functionName = "insertMMOStaging";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->workato_api_token_mmo	= $params->get('workato_api_token_mmo');
		#key 3fc311b9ae3fc6a98a257506dfee25a3b6f046a9c039423c92d1e0b5d70d6674

		$paramsComponent->workato_mmo_delay	= $params->get('workato_mmo_delay');

		$paramsComponent->workato_mmo_endpoint_upload	= $params->get('workato_mmo_endpoint_upload');

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		#$urlToPost = "https://apim.workato.com/development/mmo-v1/post-shipment-line";
		$urlToPost = $paramsComponent->workato_mmo_endpoint_upload;


		$arrData = (array)$objData;
		$strRow = implode(",", $arrData);

		$postPayload = new stdClass();
		$postPayload->row = $strRow;
	
		$jsonPostPayload = json_encode($postPayload);
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r paramsComponent->workato_api_token_mmo: ".print_r($paramsComponent->workato_api_token_mmo, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		$logMessage .= "\n\r strRow: ".print_r($strRow, true);		
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".print_r($strRow, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->logFileProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_mmo;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
		$stamp_start_micro_beacon1 = microtime(true);
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Execute ";
		$logMessage .= "\n\r ";
		if ( $objLogger->logFileProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			

		$returnData = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro_beacon1 = ($stamp_end_micro - $stamp_start_micro_beacon1);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Close | seconds: ".number_format($stamp_total_micro_beacon1,2);;
		$logMessage .= "\n\r ";
		if ( $objLogger->logFileProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->system_message = "CURL ERROR";
			$objReturn->note = "Curl Error: ".print_r($error, true);
			$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnFromWorkato->errors[0]->field. " | ".$objReturnFromWorkato->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
								
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";			
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a the server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
				
					break;

														
			}

			
			$jsonReturn = json_encode($objReturn);
			
			$jsonReturnFromWorkato = json_encode($objReturnFromWorkato);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Result: ".$arrCurlHeader["http_code"]. " | ".$objReturn->system_message." | ".$jsonReturnFromWorkato;
			$logMessage .= "\n\r ";
			if ( $objLogger->logFileProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "insertMMOStaging";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_payload = $jsonPostPayload;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturnFromWorkato: ".print_r($jsonReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		



		#Delay the next call by seconds.  The param returns a number of seconds, like 1 or 2.   Can be a fraction of seconds, like 1.5 seconds.
		
		usleep($paramsComponent->workato_mmo_delay * 1000000);



		return $objReturn;
	}


	public static function executeMMOValidation( )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=executeMMOValidation


		$controllerName = "Nsd_workatoController";
		$functionName = "executeMMOValidation";
		
		$db = JFactory::getDBO();



        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->workato_api_token_mmo	= $params->get('workato_api_token_mmo');
		#key 3fc311b9ae3fc6a98a257506dfee25a3b6f046a9c039423c92d1e0b5d70d6674


		$paramsComponent->workato_mmo_endpoint_execute	= $params->get('workato_mmo_endpoint_execute');

		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		#$urlToPost = "https://apim.workato.com/development/mmo-v1/post-all-updates";
		$urlToPost = $paramsComponent->workato_mmo_endpoint_execute;


		$postPayload = new stdClass();
		
		$postPayload->go = "1";

		$jsonPostPayload = json_encode($postPayload);
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r paramsComponent->workato_api_token_mmo: ".print_r($paramsComponent->workato_api_token_mmo, true);
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_mmo;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			


		$returnData = curl_exec ($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->note = "Curl Error: ".print_r($error, true);

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnData->errors[0]->field. " | ".$objReturnData->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
					$logMessage .= "\n\r ";
					#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					
					if( $objReturnFromWorkato->reply_type == "error" )
					{
						$arrErrors = array();
						
						
						foreach ( $objReturnFromWorkato->message as $message )
						{
						
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
							$logMessage .= "\n\r message: ".print_r($message, true);
							$logMessage .= "\n\r ";
							#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


							$arrSplit = explode( "-", $message->message );
						

							$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
							$logMessage .= "\n\r arrSplit: ".print_r($arrSplit, true);
							$logMessage .= "\n\r ";
							#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

						
													
							$objError = new stdClass();
							$objError->freight_bill = trim($arrSplit[0]);
							$objError->error = trim($arrSplit[1]);


							$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
							$logMessage .= "\n\r objError: ".print_r($objError, true);
							$logMessage .= "\n\r ";
							#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


							$arrErrors[] = $objError;
						}

						$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
						$logMessage .= "\n\r arrErrors: ".print_r($arrErrors, true);
						$logMessage .= "\n\r ";
						#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


						
						$objReturnFromWorkato->arrErrors = $arrErrors;
					}
					
					
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;

				
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a Workato server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
				
					break;

														
			}
			
			
			$jsonReturn = json_encode($objReturn);

			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "executeMMOValidation";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnData: ".print_r($objReturnData, true);
			$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			#if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }






		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}





// !TRACE PAGE FUNCTIONS



	public static function getTruckmateListOrderData( $objForm )
	{

		# http://nsddev.metalake.net/index.php?option=com_nsd_workato&task=getTruckmateOrderData


		$controllerName = "Nsd_workatoController";
		$functionName = "getTruckmateListOrderData";

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->workato_api_token_truckmate = $params->get('workato_api_token_truckmate');
		$paramsComponent->workato_api_endpoint_truckmate_list_order_search = $params->get('workato_api_endpoint_truckmate_list_order_search');
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		#https://apim.workato.com/dev/order/searchv2?trackingNumber=10014802&showHistory=0&showTrace=0&showNotes=1&showInterliner=0&showLineItems=1

		$endpoint = $paramsComponent->workato_api_endpoint_truckmate_list_order_search;


		$urlWorkato = $endpoint."?trace=".urlencode($objForm->trace_number);


		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_truckmate;


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r urlWorkato: ".print_r($urlWorkato, true);
		$logMessage .= "\n\r arrHeader: ".print_r($arrHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt ($ch, CURLOPT_URL, $urlWorkato);
		
		// ENABLE HTTP POST
		curl_setopt ($ch, CURLOPT_POST, 0);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	

		//execute the request (the login)
		$curlReturn = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);
		
		$json_last_error = json_last_error();

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);				
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		

		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;		    
			$logMessage .= "\n\r Error: ".print_r($curlError, true);
			$logMessage .= "\n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		    
   	    
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r curlReturn: ".print_r($curlReturn, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


		$objTMW = json_decode($curlReturn);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objTMW;
	}
	

	public static function getTruckmateOrderData( $objForm )
	{

		# http://nsddev.metalake.net/index.php?option=com_nsd_workato&task=getTruckmateOrderData


		$controllerName = "Nsd_workatoController";
		$functionName = "getTruckmateOrderData";

        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->workato_api_token_truckmate 			= $params->get('workato_api_token_truckmate');
		$paramsComponent->workato_api_endpoint_truckmate 			= $params->get('workato_api_endpoint_truckmate');
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		#https://apim.workato.com/dev/order/searchv2?trackingNumber=10014802&showHistory=0&showTrace=0&showNotes=1&showInterliner=0&showLineItems=1

		$endpoint = $paramsComponent->workato_api_endpoint_truckmate;


		$urlParams = array();
		$urlParams[] = "showHistory=1";
		$urlParams[] = "showTrace=1";
		$urlParams[] = "showNotes=1";
		$urlParams[] = "showInterliner=1";
		$urlParams[] = "showLineItems=1";
		$urlParams[] = "showShippingInstructions=1";

		$strParams = implode("&",$urlParams);


		$urlWorkato = $endpoint."?trace=".$objForm->trace_number."&".$strParams;

		#$urlWorkato = "https://apim.workato.com/dev/order/searchv2?trace=10014802&showHistory=0&showTrace=0&showNotes=1&showInterliner=0&showLineItems=1";



		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_truckmate;


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r urlWorkato: ".print_r($urlWorkato, true);
		$logMessage .= "\n\r arrHeader: ".print_r($arrHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt ($ch, CURLOPT_URL, $urlWorkato);
		
		// ENABLE HTTP POST
		curl_setopt ($ch, CURLOPT_POST, 0);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	

		//execute the request (the login)
		$curlReturn = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);
		
		$json_last_error = json_last_error();

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "TIMER | ".$controllerName." | ".$functionName." | 1 | seconds: ".number_format($stamp_total_micro,2);				
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		

		$curlError = "";
		
		if(curl_errno($ch)){
		    $curlError = curl_error($ch);
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;		    
			$logMessage .= "\n\r Error: ".print_r($curlError, true);
			$logMessage .= "\n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }		    
   	    
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r curlReturn: ".print_r($curlReturn, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


		$objTMW = json_decode($curlReturn);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objTMW;
	}


	public static function insertTruckmateOrderHistory( $objData )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=insertMMOStaging


		$controllerName = "Nsd_workatoController";
		$functionName = "insertTruckmateOrderHistory";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();

		$paramsComponent->workato_api_token_truckmate = $params->get('workato_api_token_truckmate');

		$paramsComponent->workato_api_endpoint_truckmate_insert_order_history = $params->get('workato_api_endpoint_truckmate_insert_order_history');

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		$urlToPost = $paramsComponent->workato_api_endpoint_truckmate_insert_order_history;

		$postPayload = $objData;
		unset($postPayload->task);
		unset($postPayload->option);
		unset($postPayload->histUpdatedByEmail);
	
		$jsonPostPayload = json_encode($postPayload);
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r paramsComponent->workato_api_token_truckmate: ".print_r($paramsComponent->workato_api_token_truckmate, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		$logMessage .= "\n\r strRow: ".print_r($strRow, true);		
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".print_r($strRow, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_truckmate;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
		$stamp_start_micro_beacon1 = microtime(true);
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Execute ";
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			

		$returnData = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro_beacon1 = ($stamp_end_micro - $stamp_start_micro_beacon1);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Close | seconds: ".number_format($stamp_total_micro_beacon1,2);;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->system_message = "CURL ERROR";
			$objReturn->note = "Curl Error: ".print_r($error, true);
			$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnFromWorkato->errors[0]->field. " | ".$objReturnFromWorkato->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
								
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";			
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a the server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
				
					break;

														
			}

			
			$jsonReturn = json_encode($objReturn);
			
			$jsonReturnFromWorkato = json_encode($objReturnFromWorkato);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Result: ".$arrCurlHeader["http_code"]. " | ".$objReturn->system_message." | ".$jsonReturnFromWorkato;
			$logMessage .= "\n\r ";
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "insertMMOStaging";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_payload = $jsonPostPayload;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturnFromWorkato: ".print_r($jsonReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		




		return $objReturn;
	}


	public static function updateTruckmateOrderConsignee( $objData )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=insertMMOStaging


		$controllerName = "Nsd_workatoController";
		$functionName = "updateTruckmateOrderConsignee";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();

		$paramsComponent->workato_api_token_truckmate = $params->get('workato_api_token_truckmate');

		$paramsComponent->workato_api_endpoint_truckmate_update_consignee = $params->get('workato_api_endpoint_truckmate_update_consignee');

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		$urlToPost = $paramsComponent->workato_api_endpoint_truckmate_update_consignee;

		$postPayload = $objData;
		unset($postPayload->task);
		unset($postPayload->option);
		unset($postPayload->trace);
		unset($postPayload->destId);
		unset($postPayload->destName);
		unset($postPayload->histUpdatedByEmail);

/*
		unset($postPayload->destAddr1);
		unset($postPayload->destAddr2);
		unset($postPayload->destCity);				
		unset($postPayload->destState);
		unset($postPayload->destZip);
		unset($postPayload->destPhone);
		unset($postPayload->destCell);
*/
		
	
		$jsonPostPayload = json_encode($postPayload);
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r paramsComponent->workato_api_token_truckmate: ".print_r($paramsComponent->workato_api_token_truckmate, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		$logMessage .= "\n\r postPayload: ".print_r($postPayload, true);		
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".print_r($strRow, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_truckmate;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
		$stamp_start_micro_beacon1 = microtime(true);
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Execute ";
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			

		$returnData = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro_beacon1 = ($stamp_end_micro - $stamp_start_micro_beacon1);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Close | seconds: ".number_format($stamp_total_micro_beacon1,2);;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->system_message = "CURL ERROR";
			$objReturn->note = "Curl Error: ".print_r($error, true);
			$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnFromWorkato->errors[0]->field. " | ".$objReturnFromWorkato->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
								
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";			
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a the server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
				
					break;

														
			}

			
			$jsonReturn = json_encode($objReturn);
			
			$jsonReturnFromWorkato = json_encode($objReturnFromWorkato);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Result: ".$arrCurlHeader["http_code"]. " | ".$objReturn->system_message." | ".$jsonReturnFromWorkato;
			$logMessage .= "\n\r ";
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "insertMMOStaging";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_payload = $jsonPostPayload;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturnFromWorkato: ".print_r($jsonReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		




		return $objReturn;
	}


	public static function updateTruckmateOrderShipper( $objData )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=updateTruckmateOrderShipper


		$controllerName = "Nsd_workatoController";
		$functionName = "updateTruckmateOrderShipper";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();

		$paramsComponent->workato_api_token_truckmate = $params->get('workato_api_token_truckmate');

		$paramsComponent->workato_api_endpoint_truckmate_update_shipper = $params->get('workato_api_endpoint_truckmate_update_shipper');

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		$urlToPost = $paramsComponent->workato_api_endpoint_truckmate_update_shipper;

		$postPayload = $objData;
		unset($postPayload->task);
		unset($postPayload->option);
		unset($postPayload->trace);
		unset($postPayload->origId);
		unset($postPayload->origName);
		unset($postPayload->histUpdatedByEmail);		
	
		$jsonPostPayload = json_encode($postPayload);
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r paramsComponent->workato_api_token_truckmate: ".print_r($paramsComponent->workato_api_token_truckmate, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		$logMessage .= "\n\r strRow: ".print_r($strRow, true);		
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".print_r($strRow, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->logFileProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_truckmate;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
		$stamp_start_micro_beacon1 = microtime(true);
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Execute ";
		$logMessage .= "\n\r ";
		if ( $objLogger->logFileProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			

		$returnData = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro_beacon1 = ($stamp_end_micro - $stamp_start_micro_beacon1);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Close | seconds: ".number_format($stamp_total_micro_beacon1,2);;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->system_message = "CURL ERROR";
			$objReturn->note = "Curl Error: ".print_r($error, true);
			$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnFromWorkato->errors[0]->field. " | ".$objReturnFromWorkato->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
								
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";			
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a the server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
				
					break;

														
			}

			
			$jsonReturn = json_encode($objReturn);
			
			$jsonReturnFromWorkato = json_encode($objReturnFromWorkato);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Result: ".$arrCurlHeader["http_code"]. " | ".$objReturn->system_message." | ".$jsonReturnFromWorkato;
			$logMessage .= "\n\r ";
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "insertMMOStaging";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_payload = $jsonPostPayload;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturnFromWorkato: ".print_r($jsonReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		




		return $objReturn;
	}



	public static function getTruckmateObjUser( $objData )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=updateTruckmateOrderShipper


		$controllerName = "Nsd_workatoController";
		$functionName = "getTruckmateObjUser";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();

		$paramsComponent->workato_api_token_truckmate = $params->get('workato_api_token_truckmate');

		$paramsComponent->workato_api_endpoint_truckmate_get_user = $params->get('workato_api_endpoint_truckmate_get_user');

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		
		$urlWorkato = $paramsComponent->workato_api_endpoint_truckmate_get_user."?user_id=".$objData->user_id;
		
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r urlWorkato: ".print_r($urlWorkato, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".print_r($strRow, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_truckmate;
	

		$ch = curl_init ($urlToPost);
		curl_setopt ($ch, CURLOPT_URL, $urlWorkato);
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, 0);
		#curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
		
		
			
		$stamp_start_micro_beacon1 = microtime(true);
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Execute ";
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			

		$returnData = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro_beacon1 = ($stamp_end_micro - $stamp_start_micro_beacon1);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Close | seconds: ".number_format($stamp_total_micro_beacon1,2);;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->system_message = "CURL ERROR";
			$objReturn->note = "Curl Error: ".print_r($error, true);
			$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnFromWorkato->errors[0]->field. " | ".$objReturnFromWorkato->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
								
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";			
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a the server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
				
					break;

														
			}

			
			$jsonReturn = json_encode($objReturn);
			
			$jsonReturnFromWorkato = json_encode($objReturnFromWorkato);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Result: ".$arrCurlHeader["http_code"]. " | ".$objReturn->system_message." | ".$jsonReturnFromWorkato;
			$logMessage .= "\n\r ";
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "insertMMOStaging";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_payload = $jsonPostPayload;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturnFromWorkato: ".print_r($jsonReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		




		return $objReturn;
	}




// !CONTACTLESS POD


	public static function insertOrderStatus( $objData )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=insertOrderStatus


		$controllerName = "Nsd_workatoController";
		$functionName = "insertOrderStatus";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();

		$paramsComponent->workato_api_token_truckmate = $params->get('workato_api_token_truckmate');

		$paramsComponent->workato_api_endpoint_truckmate_insert_order_status = $params->get('workato_api_endpoint_truckmate_insert_order_status');

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		$urlToPost = $paramsComponent->workato_api_endpoint_truckmate_insert_order_status;

		$postPayload = $objData;
	
		$jsonPostPayload = json_encode($postPayload);
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r paramsComponent->workato_api_token_truckmate: ".print_r($paramsComponent->workato_api_token_truckmate, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		$logMessage .= "\n\r strRow: ".print_r($strRow, true);		
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".print_r($strRow, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_truckmate;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
		$stamp_start_micro_beacon1 = microtime(true);
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Execute ";
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			

		$returnData = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro_beacon1 = ($stamp_end_micro - $stamp_start_micro_beacon1);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Close | seconds: ".number_format($stamp_total_micro_beacon1,2);;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->system_message = "CURL ERROR";
			$objReturn->note = "Curl Error: ".print_r($error, true);
			$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnFromWorkato->errors[0]->field. " | ".$objReturnFromWorkato->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
								
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";			
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a the server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
				
					break;

														
			}

			
			$jsonReturn = json_encode($objReturn);
			
			$jsonReturnFromWorkato = json_encode($objReturnFromWorkato);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Result: ".$arrCurlHeader["http_code"]. " | ".$objReturn->system_message." | ".$jsonReturnFromWorkato;
			$logMessage .= "\n\r ";
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

/*


			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "insertMMOStaging";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_payload = $jsonPostPayload;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();
*/



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturnFromWorkato: ".print_r($jsonReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		




		return $objReturn;
	}

	public static function sendEmailMessagePOD( $objData )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=insertOrderStatus


		$controllerName = "Nsd_workatoController";
		$functionName = "sendEmailMessagePOD";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();

		$paramsComponent->workato_api_token_truckmate = $params->get('workato_api_token_truckmate');

		$paramsComponent->workato_api_endpoint_email_status_pod = $params->get('workato_api_endpoint_email_status_pod');

		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objReturn = new stdClass();


		$urlToPost = $paramsComponent->workato_api_endpoint_email_status_pod;

		$postPayload = $objData;
	
		$jsonPostPayload = json_encode($postPayload);
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r paramsComponent->workato_api_token_truckmate: ".print_r($paramsComponent->workato_api_token_truckmate, true);
		$logMessage .= "\n\r arrData: ".print_r($arrData, true);
		$logMessage .= "\n\r strRow: ".print_r($strRow, true);		
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".print_r($strRow, true);		
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token_truckmate;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
		$stamp_start_micro_beacon1 = microtime(true);
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Execute ";
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			

		$returnData = curl_exec($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro_beacon1 = ($stamp_end_micro - $stamp_start_micro_beacon1);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." Curl Close | seconds: ".number_format($stamp_total_micro_beacon1,2);;
		$logMessage .= "\n\r ";
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->system_message = "CURL ERROR";
			$objReturn->note = "Curl Error: ".print_r($error, true);
			$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnFromWorkato = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnFromWorkato->errors[0]->field. " | ".$objReturnFromWorkato->errors[0]->message;
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
								
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;	
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";			
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;				
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a the server.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "9999";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
					$objReturn->objReturnFromWorkato = $objReturnFromWorkato;
					$objReturn->user_messsage = "Connection failed, please contact IT support team to assist with this issue by submitting a ticket at: https://help.shipnsd.com";
				
					break;

														
			}

			
			$jsonReturn = json_encode($objReturn);
			
			$jsonReturnFromWorkato = json_encode($objReturnFromWorkato);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Result: ".$arrCurlHeader["http_code"]. " | ".$objReturn->system_message." | ".$jsonReturnFromWorkato;
			$logMessage .= "\n\r ";
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

/*


			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "insertMMOStaging";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_payload = $jsonPostPayload;
			$objDataInsert->json_return = $jsonReturn;
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = false;
			$objDataInsert->scheduled = false;
			$objDataInsert->docked = false;
			$objDataInsert->outForDelivery = false;
			$objDataInsert->delivered = false;
			$objDataInsert->available = false;
			$objDataInsert->exception = false;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();
*/



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnFromWorkato: ".print_r($objReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturnFromWorkato: ".print_r($jsonReturnFromWorkato, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		




		return $objReturn;
	}


// !COMMON FUNCTIONS


// !TEST FUNCTIONS



	public function test_updateWorkato( )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=test_updateWorkato


		$controllerName = "Nsd_workatoController";
		$functionName = "test_updateWorkato";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objInput = new stdClass();
		
		$objInput->detailLineID = "144263";
		$objInput->voiceOptIn = "8885554444";
		$objInput->textOptIn = "8005553333";
		$objInput->emailOptIn = "person@email.com";

		$objReturn = Nsd_workatoController::updateWorkato( $objInput );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objInput: ".print_r($objInput, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_workato_connect( )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=test_workato_connect


		$controllerName = "Nsd_workatoController";
		$functionName = "test_workato_connect";
		
		$db = JFactory::getDBO();


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->workato_api_token	= $params->get('workato_api_token');


		
		$flag_production = 1;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
            
			$urlToPost = "https://www.workato.com/service/nsd/prod/order/optin";
	
			$postPayload = new stdClass();
			
			$postPayload->detailLineID = "272119";
			$postPayload->voiceOptIn = "8885551212";
			$postPayload->textOptIn = "";
			$postPayload->emailOptIn = "";
			$postPayload->entry = true;
			$postPayload->scheduled = true;
			$postPayload->docked = true;
			$postPayload->outForDelivery = true;
			$postPayload->delivered = true;
			$postPayload->available = true;
			$postPayload->exception = true;            
            

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
            
            
			$urlToPost = "https://www.workato.com/service/nsd/dev/order/optin";
	
			$postPayload = new stdClass();
			
			$postPayload->detailLineID = "144263";
			$postPayload->voiceOptIn = "8885551212";
			$postPayload->textOptIn = "";
			$postPayload->emailOptIn = "";
			$postPayload->entry = true;
			$postPayload->scheduled = true;
			$postPayload->docked = true;
			$postPayload->outForDelivery = true;
			$postPayload->delivered = true;
			$postPayload->available = true;
			$postPayload->exception = true;            
            
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


			$objReturn = new stdClass();

		
			$jsonPostPayload = json_encode($postPayload);
		
		
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
			$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


			
			
			$arrHeader = array();
			$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token;
		

			$ch = curl_init ($urlToPost);
			
			curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
			curl_setopt ($ch, CURLOPT_POST, true);
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
			curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			
			
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$returnData = curl_exec ($ch);
		$arrCurlHeader = curl_getinfo($ch);


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->note = "Curl Error: ".print_r($error, true);

		
			
		}
		else
		{
			
			
			$objReturnData = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "The request is valid and successful.";
				
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnData->errors[0]->field. " | ".$objReturnData->errors[0]->message;
				
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
				
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
				
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
				
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
				
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
				
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
				
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a SendGrid server.";
				
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
				
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "61";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
				
					break;

														
			}


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnData: ".print_r($objReturnData, true);
			$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }






		}

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_workato_mmo_insertMMOStaging( )
	{

		# http://nsddev.metalake.net/index.php?option=com_nsd_workato&task=test_workato_mmo_insertMMOStaging


		$controllerName = "Nsd_workatoController";
		$functionName = "test_workato_mmo_insertMMOStaging";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objData = new stdClass();
		$objData->FREIGHT_BILL_NUMBER = "31449951";
		$objData->CARRIER_NAME = "VETERANS";
		$objData->CONSOLIDATION_PRO = "A";
		$objData->ETA = "6/1/2019";
		$objData->COST = "";
		$objData->DETAIL_LINE_ID = "451084";


		$return = Nsd_workatoController::insertMMOStaging( $objData );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r return: ".print_r($return, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_workato_mmo_executeMMOValidation( )
	{

		# http://nsddev.metalake.net/index.php?option=com_nsd_workato&task=test_workato_mmo_executeMMOValidation


		$controllerName = "Nsd_workatoController";
		$functionName = "test_workato_mmo_executeMMOValidation";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$return = Nsd_workatoController::executeMMOValidation();

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r return: ".print_r($return, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



// !FUNCTION TEMPLATE

	public function test_function_template( )
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=test_function_template


		$controllerName = "Nsd_workatoController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




// !DEPRICATED

	public static function updateWorkato(  ) // depricated - used to be used on scheduling age to add user to alerts
	{

		# http://dev6.metalake.net/index.php?option=com_nsd_workato&task=test_workato_connect


		$controllerName = "Nsd_workatoController";
		$functionName = "updateWorkato";
		
		$db = JFactory::getDBO();


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_workato');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->workato_api_token	= $params->get('workato_api_token');

		ob_start();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
            
            $urlToPost = "https://www.workato.com/service/nsd/prod/order/optin";


		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "workato_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "workato_prod";
            
            $urlToPost = "https://www.workato.com/service/nsd/dev/order/optin";
            
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objInput = new stdClass();
		
		$jinput = JFactory::getApplication()->input;
		$objInput->detailLineID = trim($jinput->get('detailLineID', '', 'STRING'));
		$objInput->voiceOptIn = trim($jinput->get('notifyPhone', '', 'STRING'));
		$objInput->textOptIn = trim($jinput->get('notifyPhone', '', 'STRING'));
		$objInput->emailOptIn = trim($jinput->get('notifyEmail', '', 'STRING'));

		$objReturn = new stdClass();


		$postPayload = new stdClass();
		
		$postPayload->detailLineID = $objInput->detailLineID;
		
		#never send
		#$postPayload->voiceOptIn = $objInput->voiceOptIn;
		
		if ( trim($objInput->textOptIn) != "" )
		{
			$postPayload->textOptIn = $objInput->textOptIn;
		}
		
		
		if ( trim($objInput->emailOptIn) != "" )
		{		
			$postPayload->emailOptIn = $objInput->emailOptIn;
		}		
		
		
		$postPayload->entry = true;
		$postPayload->scheduled = true;
		$postPayload->docked = true;
		$postPayload->outForDelivery = true;
		$postPayload->delivered = true;
		$postPayload->available = true;
		$postPayload->exception = true;
		
	
		$jsonPostPayload = json_encode($postPayload);
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r jsonPostPayload: ".print_r($jsonPostPayload, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




		
		
		$arrHeader = array();
		$arrHeader[] = "API-TOKEN: ".$paramsComponent->workato_api_token;
	

		$ch = curl_init ($urlToPost);
		
		curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonPostPayload);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $arrHeader);

		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			
			



		$returnData = curl_exec ($ch);
		$arrCurlHeader = curl_getinfo($ch);


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urlToPost: ".print_r($urlToPost, true);
		$logMessage .= "\n\r returnData: ".print_r($returnData, true);
		$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		curl_close($ch);



		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r error: ".print_r($error, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$objReturn->error = "1";
			$objReturn->error_code = "999";
			$objReturn->error_message = "Curl Error: ".print_r($error, true);
			$objReturn->note = "Curl Error: ".print_r($error, true);

			die(header("HTTP/1.0 404 Not Found")); //Throw an error on failure
			
		}
		else
		{
			
			
			$objReturnData = json_decode($returnData);


			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "SUCCESS";
					$objReturn->note = "The request is valid and successful.";
				
					break;
				
				case "202":

					$objReturn->error = "0";
					$objReturn->error_code = "";
					$objReturn->error_message = "";
					$objReturn->system_message = "";
					$objReturn->note = "Your message is both valid, and queued to be delivered.";
				
					break;				

				case "400":

					$objReturn->error = "1";
					$objReturn->error_code = "400";
					$objReturn->error_message = "BAD REQUEST";
					$objReturn->system_message = "BAD REQUEST";
					$objReturn->note = $objReturnData->errors[0]->field. " | ".$objReturnData->errors[0]->message;
				
					break;				
	

				case "401":

					$objReturn->error = "1";
					$objReturn->error_code = "401";
					$objReturn->error_message = "UNAUTHORIZED";
					$objReturn->system_message = "UNAUTHORIZED";
					$objReturn->note = "You do not have authorization to make the request.";
				
					break;	
				
				case "403":

					$objReturn->error = "1";
					$objReturn->error_code = "403";
					$objReturn->error_message = "FORBIDDEN";
					$objReturn->system_message = "FORBIDDEN";
					$objReturn->note = "You do not have authorization to make the request.";
				
					break;	

				case "404":

					$objReturn->error = "1";
					$objReturn->error_code = "404";
					$objReturn->error_message = "NOT FOUND";
					$objReturn->system_message = "NOT FOUND";
					
					$objReturn->note = "The resource you tried to locate could not be found or does not exist.";
				
					break;	
					

				case "405":

					$objReturn->error = "1";
					$objReturn->error_code = "405";
					$objReturn->error_message = "METHOD NOT ALLOWED";
					$objReturn->system_message = "METHOD NOT ALLOWED";
					$objReturn->note = "API method not allowed";
				
					break;	

				case "413":

					$objReturn->error = "1";
					$objReturn->error_code = "413";
					$objReturn->error_message = "PAYLOAD TOO LARGE";
					$objReturn->system_message = "PAYLOAD TOO LARGE";
					$objReturn->note = "The JSON payload you have included in your request is too large";
				
					break;	

				case "422":

					$objReturn->error = "1";
					$objReturn->error_code = "422";
					$objReturn->error_message = "Processing Error";
					$objReturn->system_message = "PROCESSING ERROR";
					$objReturn->note = "There was a processing error.";
				
					break;	

				case "429":

					$objReturn->error = "1";
					$objReturn->error_code = "429";
					$objReturn->error_message = "TOO MANY REQUESTS";
					$objReturn->system_message = "TOO MANY REQUESTS";
					$objReturn->note = "The number of requests you have made exceeds Workato's rate limitations";
				
					break;	

				case "500":

					$objReturn->error = "1";
					$objReturn->error_code = "500";
					$objReturn->error_message = "SERVER UNAVAILABLE";
					$objReturn->system_message = "SERVER UNAVAILABLE";
					$objReturn->note = "An error occurred on a SendGrid server.";
				
					break;	

				case "503":

					$objReturn->error = "1";
					$objReturn->error_code = "503";
					$objReturn->error_message = "SERVICE NOT AVAILABLE";
					$objReturn->system_message = "SERVICE NOT AVAILABLE";
					$objReturn->note = "The SendGrid v3 Web API is not available.";
				
					break;	

				default:

					$objReturn->error = "1";
					$objReturn->error_code = "61";
					$objReturn->error_message = "Unknown error from Workato";
					$objReturn->system_message = "Unknown error from Workato";
					$objReturn->note = "Unknown error from Workato";
				
					break;

														
			}
			
			
			$jsonReturn = json_encode($objReturn);

			#log

			$objDataInsert = new stdClass();
						
			$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
			$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
			$objDataInsert->action = "insert";
			$objDataInsert->error = $objReturn->error;
			$objDataInsert->error_code = $objReturn->error_code;
			$objDataInsert->error_message = $objReturn->error_message;
			$objDataInsert->system_message = $objReturn->system_message;
			$objDataInsert->note = $objReturn->note;
			$objDataInsert->json_return = json_encode($objReturnData);
			$objDataInsert->detailLineID = $objInput->detailLineID;
			$objDataInsert->voiceOptIn = $objInput->voiceOptIn;
			$objDataInsert->textOptIn = $objInput->textOptIn;
			$objDataInsert->emailOptIn = $objInput->emailOptIn;
			$objDataInsert->entry = true;
			$objDataInsert->scheduled = true;
			$objDataInsert->docked = true;
			$objDataInsert->outForDelivery = true;
			$objDataInsert->delivered = true;
			$objDataInsert->available = true;
			$objDataInsert->exception = true;

			$result = JFactory::getDbo()->insertObject('htc_nsd_workato_log', $objDataInsert);
			$logID = $db->insertid();



			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnData: ".print_r($objReturnData, true);
			$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r jsonReturn: ".print_r($jsonReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }






		}

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		

		ob_end_clean();

		echo $jsonReturn;
		
		JFactory::getApplication()->close();
		
		
	}

}
