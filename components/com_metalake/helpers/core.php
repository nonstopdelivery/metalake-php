<?php

/**
 * @version     1.0.0
 * @package     com_metalake
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Lew Sawyer <lsawyer@metalake.com> - http://www.metalake.com
 */
defined('_JEXEC') or die;

# install these libraries for Salesforce	 
#require_once ('libraries/salesforce/SforcePartnerClient.php');
#require_once ('libraries/salesforce/SforceHeaderOptions.php');
require_once ('components/com_metalake/helpers/mlgeocode.php');


class MetalakeHelperCore
{
	



	#                     
	#        ####   ####  
	#       #    # #    # 
	#       #    # #      
	#       #    # #  ### 
	#       #    # #    # 
	#######  ####   ####  

	// !LOGGING
	                      

	public static function logger( $logFile, $logMessage )
	{
		
		
		$logPath = JPATH_ROOT."/logs/".$logFile.".log";
		
		if ( file_exists( $logPath ) )
		{
			$log_str = "[".date("Y-m-d H:i:s")."] ".$logMessage."\n\r";
			$fp = fopen( $logPath, "a+" );
			fwrite($fp, $log_str);
			fclose($fp);
		}
		else
		{
			$newLogFile = fopen($logPath, "w");	

			MetalakeHelperCore::logger($logFile, $logMessage);
			
		}		
	}



	public static function logOBJ( $table, $logObj )
	{
		global $mainframe, $option;
		 
		JFactory::getDbo()->insertObject($table, $logObj);

		$logger = 0;
		$logFile = "metalake";	
		$logMessage = "logOBJ";	
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

	}






	 #####                                      
	#     # ##### #####  # #    #  ####   ####  
	#         #   #    # # ##   # #    # #      
	 #####    #   #    # # # #  # #       ####  
	      #   #   #####  # #  # # #  ###      # 
	#     #   #   #   #  # #   ## #    # #    # 
	 #####    #   #    # # #    #  ####   ####  
	                                            
	// !STRINGS

	public function getStringBetween($string, $start, $end)
	{
		$string = trim($string);
		$ini = strpos($string,$start);
		if ($ini === FALSE) return "";
		$ini += strlen($start);
		$len = strpos($string,$end,$ini) - $ini;

		return substr($string,$ini,$len);
	}


	public function createRandomString( $strLength )
	{
		$returnString = '';
			
		$pstring = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789";
		$plength = strlen($pstring)-1;
	
		for ($i = 1; $i <= $strLength; $i++)
		{
			$start = rand(0,$plength);
			$returnString .= substr($pstring, $start, 1);
		}
	
		return $returnString;
	}


	public function createRandomNumber( $numberOfDigits )
	{
		$rnum = substr(number_format(time() * mt_rand(),0,'',''),0,$numberOfDigits);

		return $rnum;

	}


	public function wordCount( $inputText, $wordCount )
	{
	
		$arrParagraphLong = explode(' ', $inputText);
		$arrParagraph = array_slice ($paragraphLong, 0, $wordCount);
		$paragraphShort = implode(' ', $arrParagraph);
		$return = ( count($arrParagraphLong) <= $wordCount ) ? $inputText : $paragraphShort . "..." ;
		
		return $return;
	
	}


	function scrubString( $string, $start, $end, $replace )
	{

		$string = trim($string);

		$begin_pos = strpos($string,$start);

		if ($begin_pos === FALSE) return "";

		$end_length = strlen($end);

		$end_pos = strpos($string,$end) + $end_length;

		$totlength = $end_pos - $begin_pos;

			/*
			echo "<br>---------------";
			echo "<br>start: ".$start;
			echo "<br>end: ".$end;
			echo "<br>replace: ".$replace;

			echo "<br>begin_pos: ".$begin_pos;
			echo "<br>end_length: ".$end_length;
			echo "<br>end_pos: ".$end_pos;
			echo "<br>totlength: ".$totlength;


			echo "<br>---------------";
			*/


		return $return = substr_replace($string, $replace, $begin_pos, $totlength);

	}


	public function createUniqueRandomNumber( $table, $column, $numberOfDigits )
	{
		$db	=& JFactory::getDBO();

		$count = 1;

		while ( $count > 0 )
		{
		
			$randomNumber = MetalakeHelperCore::createRandomNumber( $numberOfDigits );
		
			$query = "select count($column) as 'count' from $table WHERE $column = '$randomNumber' ";
			$db->setQuery($query);
			$count = $db->loadResult();
			
		}
		
		return $randomNumber;
	
	}


	// !ENCODE AND ENCRYPT
	
	                       #######                                    
	# #    # ######  ####  #       #    #  ####   ####  #####  ###### 
	# ##   # #      #    # #       ##   # #    # #    # #    # #      
	# # #  # #####  #    # #####   # #  # #      #    # #    # #####  
	# #  # # #      #    # #       #  # # #      #    # #    # #      
	# #   ## #      #    # #       #   ## #    # #    # #    # #      
	# #    # #       ####  ####### #    #  ####   ####  #####  ###### 


	public function infoEncode( $direction, $info )
	{
		return $EncSer = ( strtolower( $direction ) == "encode" ) ? base64_encode(serialize( $info )) : unserialize(base64_decode( $info )) ; 
	}





	                       #######                                         
	# #    # ######  ####  #       #    #  ####  #####  #   # #####  ##### 
	# ##   # #      #    # #       ##   # #    # #    #  # #  #    #   #   
	# # #  # #####  #    # #####   # #  # #      #    #   #   #    #   #   
	# #  # # #      #    # #       #  # # #      #####    #   #####    #   
	# #   ## #      #    # #       #   ## #    # #   #    #   #        #   
	# #    # #       ####  ####### #    #  ####  #    #   #   #        #   



	public function infoEncrypt( $direction, $info )
	{
		#https://stackoverflow.com/questions/1289061/best-way-to-use-php-to-encrypt-and-decrypt-passwords

		$controllerName = "MetalakeHelperCore";
		$functionName = "infoEncrypt";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "api_prod";
	
		}


	
		if( strtolower( $direction ) == "encrypt" )
		{

			$info = time() ."|". $info ;
			
				
			#$encrypt = trim(base64_encode(serialize(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $info, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))))) ;

			$key = SALT;

			$iv = mcrypt_create_iv(
			    mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
			    MCRYPT_DEV_URANDOM
			);
			
			$encrypted = base64_encode(
			    $iv .
			    mcrypt_encrypt(
			        MCRYPT_RIJNDAEL_128,
			        hash('sha256', $key, true),
			        $info,
			        MCRYPT_MODE_CBC,
			        $iv
			    )
			);




			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r direction: ".print_r($direction, true);
			$logMessage .= "\n\r info: ".print_r($info, true);
			$logMessage .= "\n\r SALT: ".print_r(SALT, true);
			$logMessage .= "\n\r encrypted: ".print_r($encrypted, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


			return $encrypted;
	
		}
		else
		{
			
			
	
			#$decrypt = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, unserialize(base64_decode($info)), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))) ;


			$key = SALT;

			$data = base64_decode($info);
			$iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
			
			$decrypted = rtrim(
			    mcrypt_decrypt(
			        MCRYPT_RIJNDAEL_128,
			        hash('sha256', $key, true),
			        substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
			        MCRYPT_MODE_CBC,
			        $iv
			    ),
			    ""
			);

	
			$arr = explode("|",$decrypted);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r direction: ".print_r($direction, true);
			$logMessage .= "\n\r info: ".print_r($info, true);
			$logMessage .= "\n\r decrypted: ".print_r($decrypted, true);
			$logMessage .= "\n\r arr: ".print_r($arr, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


	
			return $decrypt =	$arr[1] ;

		}

	}



	public function OLD_infoEncrypt( $direction, $info )
	{
	
		if( strtolower( $direction ) == "encrypt" )
		{

			$info = time() ."|". $info ;
			
				
			return $encrypt = trim(base64_encode(serialize(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $info, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))))) ;
	
		}
		else
		{
	
			
	
			$decrypt = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, unserialize(base64_decode($info)), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))) ;
	
			$arr = explode("|",$decrypt);
	
			return $decrypt =	$arr[1] ;

		}

	}





	#####  #  ####  #####   ##   #    #  ####  ###### 
	#    # # #        #    #  #  ##   # #    # #      
	#    # #  ####    #   #    # # #  # #      #####  
	#    # #      #   #   ###### #  # # #      #      
	#    # # #    #   #   #    # #   ## #    # #      
	#####  #  ####    #   #    # #    #  ####  ###### 

	// !DISTANCE

		 
	public static function distance($lat1, $lon1, $lat2, $lon2, $unit) 
	{ 
	
		#unit is either K for kilometers, N for nautical miles, default is miles.
		#http://www.zipcodeworld.com/samples/distance.php.html
	
		$lat1 = floatval($lat1);
		$lon1 = floatval($lon1);
		$lat2 = floatval($lat2);
		$lon2 = floatval($lon2);
	
	  $theta = floatval($lon1 - $lon2); 
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
	  $dist = acos($dist); 
	  $dist = rad2deg($dist); 
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);
	
	  if ($unit == "K") {
	    return ($miles * 1.609344); 
	  } else if ($unit == "N") {
	      return ($miles * 0.8684);
	    } else {
	        return $miles;
	      }
	}
	
	
	
	
		
	 #####                 #####                       
	#     # ######  ####  #     #  ####  #####  ###### 
	#       #      #    # #       #    # #    # #      
	#  #### #####  #    # #       #    # #    # #####  
	#     # #      #    # #       #    # #    # #      
	#     # #      #    # #     # #    # #    # #      
	 #####  ######  ####   #####   ####  #####  ###### 
	

	// !GEOCODE


	public function getObjGeocode( $objData )
	{

		// Callthis function using this object
		// $objData = new stdClass();
		// $objData->address = "4130 27th St N Arlington VA 22207";
		// $objData->latlng = "38.9062383,-77.10757009";
		// $objReturn = MetalakeHelperCore::getObjGeocode( $objData );



		$controllerName = "MetalakeHelperCore";
		$functionName = "getObjGeocode";
	
		$db = JFactory::getDBO();

		$app            		= JFactory::getApplication();
	    $params_ml     			= $app->getParams('com_metalake');

		$objParams = new stdClass();
		$objParams->google_api_key	= $params_ml->get('google_api_key');
	
		$flag_production = 0;
	
		if ( $flag_production )
		{

	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "metalake_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "metalake_prod";

		}
		else
		{

	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "metalake_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "metalake_prod";

		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
		$tmp_fname = tempnam("/tmp", "COOKIE");
	
	
		$urlParams = array();
		$urlParams['key'] = $objParams->google_api_key;
	
		if ( $objData->address != "" )
		{
			$urlParams['address'] = $objData->address;			
		}
		else
		{
			$urlParams['latlng'] = $objData->latlng;		
		}
	
		$theUrl = "https://maps.googleapis.com/maps/api/geocode/json?".http_build_query($urlParams);
	
	
		//init curl
		$ch = curl_init();
	
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $theUrl);
	
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
	
		//Set the post parameters
		//curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
	
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	
		//execute the request (the login)
		$store = curl_exec($ch);
	
		if(curl_errno($ch)){
			$curlError =  curl_error($ch);
		}
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r theUrl: ".print_r($theUrl, true);
		#$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
	
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$jsonStore = json_decode($store);

		$objReturn  = new stdClass();

		$objReturn->address = $jsonStore->results[0]->formatted_address;
		$objReturn->lat = $jsonStore->results[0]->geometry->location->lat;
		$objReturn->lng = $jsonStore->results[0]->geometry->location->lng;		
		$objReturn->latlng = $jsonStore->results[0]->geometry->location->lat.",".$jsonStore->results[0]->geometry->location->lng;		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	
	}

	
	 function getGeocode( $address )  #DEPRICATED
	 {

		$logger = 0;
		$logFile = "metalake";	

		 global $mainframe, $option;
		 
		$logMessage = "getGeocode\n\r";
		$logMessage .= "address: ".$address."\n\r";
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }
		 
		$coder = new Geocoder();


		$coder->setSecure(true);
		$coder->address = $address;;
		$xml = $coder->get();
	
		$lat = $xml->result->geometry->location->lat;
		$lng = $xml->result->geometry->location->lng;
	
		$latitude = "".$lat;
		$longitude = "".$lng;			
	
		$arr_info = array();
		$arr_info['status'] 	= ( $lat != "" && $lng != ""  ) ? 1 : 0 ;
		$arr_info['address'] 	= $address;
		$arr_info['latitude'] 	= $latitude;
		$arr_info['longitude'] 	= $longitude;
		$arr_info['latlng'] 	= $lat.", ".$lng;
		 
		$logMessage = "getGeocode\n\r";
		$logMessage .= "lat: ".$lat."\n\r";
		$logMessage .= "lng: ".$lng."\n\r";
		$logMessage .= "xml: ".print_r($xml, true)."\n\r";
		$logMessage .= "arr_info: ".print_r($arr_info, true)."\n\r";															
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }
		 
		return $arr_info;
		 
	 }


	 function getGeocodeAddress( $latlng )  #DEPRICATED
	 {

		$logger = 0;
		$logFile = "metalake";	

		global $mainframe, $option;
		 
		$logMessage = "getGeocodeAddress\n\r";
		$logMessage .= "latlng: ".$latlng."\n\r";
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }
		 
		$coder = new Geocoder();


		$coder->setSecure(true);
		$coder->latlng = $latlng;
		$xml = $coder->get();
	
		$address = (string) $xml->result{0}->formatted_address;
		$lat = $xml->result{0}->geometry->location->lat;
		$lng = $xml->result{0}->geometry->location->lng;
	
		$latitude = "".$lat;
		$longitude = "".$lng;			
		
		$arr_info = array();
				
		$arr_info['status'] 	= ( $address != "" ) ? 1 : 0 ;
		$arr_info['address'] 	= $address;
		$arr_info['latitude'] 	= $latitude;
		$arr_info['longitude'] 	= $longitude;
		$arr_info['latlng'] 	= $lat.", ".$lng;
		 
		$logMessage = "getGeocodeAddress\n\r";
		$logMessage .= "address: ".print_r($address, true)."\n\r";
		$logMessage .= "lat: ".$lat."\n\r";
		$logMessage .= "lng: ".$lng."\n\r";
		$logMessage .= "xml: ".print_r($xml, true)."\n\r";
	
		$logmessage .= "arr_info: ".print_r($arr_info, true)."\n\r";															
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }
	 
		return $arr_info;
		 
	 }
	
	
	
	

	
	#     #                                            
	#     #   ##   #      # #####    ##   ##### ###### 
	#     #  #  #  #      # #    #  #  #    #   #      
	#     # #    # #      # #    # #    #   #   #####  
	 #   #  ###### #      # #    # ######   #   #      
	  # #   #    # #      # #    # #    #   #   #      
	   #    #    # ###### # #####  #    #   #   ###### 

	// !VALIDATE


	public static function validateEmail( $email )
	{
	   $isValid = true;
	   $atIndex = strrpos($email, "@");
	   if (is_bool($atIndex) && !$atIndex)
	   {
	      $isValid = false;
	   }
	   else
	   {
	      $domain = substr($email, $atIndex+1);
	      $local = substr($email, 0, $atIndex);
	      $localLen = strlen($local);
	      $domainLen = strlen($domain);
	      if ($localLen < 1 || $localLen > 64)
	      {
	         // local part length exceeded
	         $isValid = false;
	      }
	      else if ($domainLen < 1 || $domainLen > 255)
	      {
	         // domain part length exceeded
	         $isValid = false;
	      }
	      else if ($local[0] == '.' || $local[$localLen-1] == '.')
	      {
	         // local part starts or ends with '.'
	         $isValid = false;
	      }
	      else if (preg_match('/\\.\\./', $local))
	      {
	         // local part has two consecutive dots
	         $isValid = false;
	      }
	      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
	      {
	         // character not valid in domain part
	         $isValid = false;
	      }
	      else if (preg_match('/\\.\\./', $domain))
	      {
	         // domain part has two consecutive dots
	         $isValid = false;
	      }
	      else if
	(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)))
	      {
	         // character not valid in local part unless 
	         // local part is quoted
	         if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local)))
	         {
	            $isValid = false;
	         }
	      }
	      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
	      {
	         // domain not found in DNS
	         $isValid = false;
	      }
	   }
	   return $isValid;
	}





	
	      #                                    
	      #  ####   ####  #    # #        ##   
	      # #    # #    # ##  ## #       #  #  
	      # #    # #    # # ## # #      #    # 
	#     # #    # #    # #    # #      ###### 
	#     # #    # #    # #    # #      #    # 
	 #####   ####   ####  #    # ###### #    # 

	// !JOOMLA



public function registerJoomlaUser( $objInput )
{
	# http://dev1.metalake.net/index.php?option=com_fling&task=test_function_template


	$controllerName = "MetalakeHelperCore";
	$functionName = "registerJoomlaUser";
	
	jimport('joomla.user.helper');
	
	$db = JFactory::getDBO();
	
	$flag_production = 0;
	
	if ( $flag_production )
	{

        $objLogger = new stdClass();
        $objLogger->logger = 0;
        $objLogger->logFile = "api_verbose";
        $objLogger->loggerProd = 0;
        $objLogger->logFileProd = "api_prod";

	}
	else
	{

        $objLogger = new stdClass();
        $objLogger->logger = 1;
        $objLogger->logFile = "api_verbose";
        $objLogger->loggerProd = 0;
        $objLogger->logFileProd = "api_prod";

	}	

	$stamp_start_micro = microtime(true);

	$logMessage = "START | ".$controllerName." | ".$functionName;	
	if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


	$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
	$logMessage .= "\n\r objInput: ".print_r($objInput, true);
	$logMessage .= "\n\r ";
	if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


	$udata = array(
	    "name"=>$objInput->userName,
	    "username"=>$objInput->userEmail,
	    "password"=>$objInput->userPassword,
	    "password2"=>$objInput->userPassword,
	    "email"=>$objInput->userEmail,
	    "block"=>0,
	    "groups"=>array("2")
	);


	$user = new JUser;
	           
	    //Write to database
	if(!$user->bind($udata)) {

	    #throw new Exception("Could not bind data. Error: " . $user->getError());

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r Could not bind data. Error: " . $user->getError();
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


	}
	if (!$user->save()) {


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r Could not save user. Error: " . $user->getError();
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

	     #throw new Exception("Could not save user. Error: " . $user->getError());
	     
	     
	     
	     
	}
	
	$objJoomlaUser = new stdClass();


    if ($user->id > 0)
    {
		$objJoomlaUser->status = "success";
		$objJoomlaUser->joomlaID = $user->id;
		$objJoomlaUser->name = $user->name;
		$objJoomlaUser->username = $user->username;
		$objJoomlaUser->email = $user->email;
		$objJoomlaUser->password = $user->password;
		$objJoomlaUser->password_clear = $objInput->password;
		$objJoomlaUser->message = "SUCCESS - User created in Joomla";
    }	
    else
    {

		$objJoomlaUser->status = "fail";
		$objJoomlaUser->message = "Sorry - User not created in Joomla";		    
	    
    }	



	$stamp_end_micro = microtime(true);
	$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


	$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
	$logMessage .= "\n\r \n\r";
	if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

	return $objJoomlaUser;
    
}




	public function OLD_registerJoomlaUser( $name, $username, $email, $password )
    {
        
        $debug = 0;
        
        
        $mainframe =& JFactory::getApplication('site');
        $mainframe->initialise();
        $user = clone(JFactory::getUser());
        $pathway = & $mainframe->getPathway();
        $config = & JFactory::getConfig();
        $authorize = & JFactory::getACL();
        $document = & JFactory::getDocument();
         
        $response = array();
        $usersConfig = &JComponentHelper::getParams( 'com_users' );
 
        #if($usersConfig->get('allowUserRegistration') == '1')
        if( true )
        {

			$objJoomlaUser = new JObject;


            // Initialize new usertype setting
            jimport('joomla.user.user');
            jimport('joomla.application.component.helper');
 
            #$useractivation = $usersConfig->get('useractivation');
            $useractivation = 0;
 
            $db = JFactory::getDBO();
            // Default group, 2=registered
            $defaultUserGroup = 2;
 
            $acl = JFactory::getACL();
            
            $password = ( $password == "" ) ? MetalakeHelperCore::createRandomString( '20' ) : $password ;
             
            jimport('joomla.user.helper');
            $salt     = JUserHelper::genRandomPassword(32);
            $password_clear = $password;
 
			#$email = "lewsawyer+".$salt."@gmail.com";
 
            $crypted  = JUserHelper::getCryptedPassword($password_clear, $salt);
            $password = $crypted.':'.$salt;
            $instance = JUser::getInstance();
            $instance->set('id'         , 0);
            $instance->set('name'           , $name);
            $instance->set('username'       , $username);
            $instance->set('password' , $password);
            $instance->set('password_clear' , $password_clear);
            $instance->set('email'          , $email);
            $instance->set('usertype'       , 'deprecated');
            $instance->set('groups'     , array($defaultUserGroup));
            // Here is possible set user profile details
            // $instance->set('profile'    , array('gender' =>  $gender));
 
            // Email with activation link
            if($useractivation == 1)
            {
                $instance->set('block'    , 1);
                $instance->set('activation'    , JApplication::getHash(JUserHelper::genRandomPassword()));
            }
 
            if (!$instance->save())
            {                    
                // Email already used!!!
                // Your code here...

				$objJoomlaUser->status = "error";
				$objJoomlaUser->joomlaID = $newUserID;
				$objJoomlaUser->name = $name;
				$objJoomlaUser->username = $username;
				$objJoomlaUser->email = $email;
				$objJoomlaUser->password = $password;
				$objJoomlaUser->password_clear = $password_clear;
				$objJoomlaUser->message = "ERROR - Email in use";

           }
            else
            {   
                $db->setQuery("update #__users set email='$email' where username='$username'");
                $db->query();
 
                $db->setQuery("SELECT id FROM #__users WHERE email='$email'");
                $db->query();
                $newUserID = $db->loadResult();
 
                $user = JFactory::getUser($newUserID);
 
                // Everything OK!               
                if ($user->id != 0)
                {                   
                    // Auto registration
                    if($useractivation == 0)
                    {
                         
                        $emailSubject = 'Email Subject for registration successfully';
                        $emailBody = 'Email body for registration successfully';                       
                        #$return = JFactory::getMailer()->sendMail('sender email', 'sender name', $user->email, $emailSubject, $emailBody);
 
                        // Your code here...
                    }
                    else if($useractivation == 1)
                    {
                        $emailSubject = 'Email Subject for activate the account';
                        $emailBody = 'Email body for for activate the account';     
                        $user_activation_url = JURI::base().JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $user->activation, false);  // Append this URL in your email body
                        #$return = JFactory::getMailer()->sendMail('sender email', 'sender name', $user->email, $emailSubject, $emailBody);                             
 
                        // Your code here...
                    }


					$objJoomlaUser->status = "success";
					$objJoomlaUser->joomlaID = $newUserID;
					$objJoomlaUser->name = $name;
					$objJoomlaUser->username = $username;
					$objJoomlaUser->email = $email;
					$objJoomlaUser->password = $password;
					$objJoomlaUser->password_clear = $password_clear;
					$objJoomlaUser->message = "SUCCESS - User created in Joomla";

                }
             }
 
        } else {
            // Registration CLOSED!
            // Your code here...             
        }

				$logmessage = "";
				$logmessage .= "--- registerJoomlaUser\n\r";
				$logmessage .= "------ objJoomlaUser: ".print_r($objJoomlaUser, true)."\n\r";
				if ( $debug ) { Ml_churchillHelperCore::logger( $logmessage ); }

        
        return $objJoomlaUser;
        
        
    }



	public function registerJoomlaUserBulk( $name, $username, $email, $password )
    {
        
        $debug = 1;
        
        
        $mainframe =& JFactory::getApplication('site');
        $mainframe->initialise();
        $user = clone(JFactory::getUser());
        $pathway = & $mainframe->getPathway();
        $config = & JFactory::getConfig();
        $authorize = & JFactory::getACL();
        $document = & JFactory::getDocument();
         
        $response = array();
        $usersConfig = &JComponentHelper::getParams( 'com_users' );
 
        #if($usersConfig->get('allowUserRegistration') == '1')
        if( true )
        {

			$objJoomlaUser = new JObject;


            // Initialize new usertype setting
            jimport('joomla.user.user');
            jimport('joomla.application.component.helper');
 
            #$useractivation = $usersConfig->get('useractivation');
            $useractivation = 0;
 
            $db = JFactory::getDBO();
            // Default group, 2=registered
            $defaultUserGroup = 2;
 
            $acl = JFactory::getACL();
            
            $password = ( $password == "" ) ? MetalakeHelperCore::createRandomString( '20' ) : $password ;
             
            jimport('joomla.user.helper');
            $salt     = JUserHelper::genRandomPassword(32);
            $password_clear = $password;
 
			#$email = "lewsawyer+".$salt."@gmail.com";
 
            $crypted  = JUserHelper::getCryptedPassword($password_clear, $salt);
            $password = $crypted.':'.$salt;
            $instance = JUser::getInstance();
            $instance->set('id'         , 0);
            $instance->set('name'           , $name);
            $instance->set('username'       , $username);
            $instance->set('password' , $password_clear);
            $instance->set('password_clear' , $password_clear);
            $instance->set('email'          , $email);
            $instance->set('usertype'       , 'deprecated');
            $instance->set('groups'     , array($defaultUserGroup));
            // Here is possible set user profile details
            // $instance->set('profile'    , array('gender' =>  $gender));
 
            // Email with activation link
            if($useractivation == 1)
            {
                $instance->set('block'    , 1);
                $instance->set('activation'    , JApplication::getHash(JUserHelper::genRandomPassword()));
            }
 
            if (!$instance->save())
            {                    
                // Email already used!!!
                // Your code here...

				$objJoomlaUser->status = "error";
				$objJoomlaUser->joomlaID = $newUserID;
				$objJoomlaUser->name = $name;
				$objJoomlaUser->username = $username;
				$objJoomlaUser->email = $email;
				$objJoomlaUser->password = $password;
				$objJoomlaUser->password_clear = $password_clear;
				$objJoomlaUser->message = "ERROR - Email in use";

           }
            else
            {   
                $db->setQuery("update #__users set email='$email' where username='$username'");
                $db->query();
 
                $db->setQuery("SELECT id FROM #__users WHERE email='$email'");
                $db->query();
                $newUserID = $db->loadResult();
 
                $user = JFactory::getUser($newUserID);
 
                // Everything OK!               
                if ($user->id != 0)
                {                   
                    // Auto registration
                    if($useractivation == 0)
                    {
                         
                        $emailSubject = 'Email Subject for registration successfully';
                        $emailBody = 'Email body for registration successfully';                       
                        #$return = JFactory::getMailer()->sendMail('sender email', 'sender name', $user->email, $emailSubject, $emailBody);
 
                        // Your code here...
                    }
                    else if($useractivation == 1)
                    {
                        $emailSubject = 'Email Subject for activate the account';
                        $emailBody = 'Email body for for activate the account';     
                        $user_activation_url = JURI::base().JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $user->activation, false);  // Append this URL in your email body
                        #$return = JFactory::getMailer()->sendMail('sender email', 'sender name', $user->email, $emailSubject, $emailBody);                             
 
                        // Your code here...
                    }


					$objJoomlaUser->status = "success";
					$objJoomlaUser->joomlaID = $newUserID;
					$objJoomlaUser->name = $name;
					$objJoomlaUser->username = $username;
					$objJoomlaUser->email = $email;
					$objJoomlaUser->password = $password;
					$objJoomlaUser->password_clear = $password_clear;
					$objJoomlaUser->message = "SUCCESS - User created in Joomla";

                }
             }
 
        } else {
            // Registration CLOSED!
            // Your code here...             
        }

				$logmessage = "";
				$logmessage .= "--- registerJoomlaUserBulk\n\r";
				$logmessage .= "------ objJoomlaUser: ".print_r($objJoomlaUser, true)."\n\r";
				if ( $debug ) { Ml_churchillHelperCore::logger( $logmessage ); }

        
        return $objJoomlaUser;
        
        
    }


	public function createJoomlaAlias( $table, $column, $string )
	{
		$db	=& JFactory::getDBO();
		
		$find = array('/');
		$replace = " ";
		
		$entryAlias = strtolower(str_replace($find,$replace,trim($event_alias)));
		
		$find = array("Í", "_", ".", "'", ",", "-", '"', "?", ":", "$", "+", "`", ";", "<", ">", "|", "#", "!", "*", "&", "(", ")", "%", "@", "/" );
		$replace = "";
					
		$aliasClean = strtolower(str_replace($find,$replace,trim($string)));
		
		$aliasClean = str_replace(" ", "-", $aliasClean);
		$countDash = strpos( $aliasClean , "--" );
		while( $countDash > 0 )
		{
			$aliasClean = str_replace("--", "-", $aliasClean);			
			$countDash = strpos( $aliasClean , "--" );
		}
		
		$query = "select count($column) as 'count' from $table WHERE $column = '$aliasClean'";
		$db->setQuery($query);
		$count = $db->loadResult();

		$aliasCleanTmp = $aliasClean;
		
		while ( $count > 0 )
		{
			$nextnum += 1;
		
			$aliasClean = $nextnum."-".$aliasCleanTmp;
		
			$query = "select count($column) as 'count' from $table where $column = '$aliasClean'";
			$db->setQuery($query);
			$count = $db->loadResult();
			
		}
		
		return $aliasClean;
	
	}





	                            #######               
	 ####  ######  ####   ####  #     # #####       # 
	#      #      #      #      #     # #    #      # 
	 ####  #####   ####   ####  #     # #####       # 
	     # #           #      # #     # #    #      # 
	#    # #      #    # #    # #     # #    # #    # 
	 ####  ######  ####   ####  ####### #####   ####  
	                                                  

	// !SESSION

  	public static function clearSessObj( $objName )
	{

		$controllerName = "MetalakeHelperCore";
		$functionName = "clearSessObj";

		$flag_production = 0;
		
		if ( $flag_production )
		{
	
	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "core_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "core_prod";
	
		}
		else
		{
	
	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "core_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "core_prod";
	
		}	

		$stamp_start_micro = microtime(true);
	
		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objName: ".print_r($objName, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	

		$session =& JFactory::getSession();
		$session->clear( $objName );
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
	
		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


	}



	public static function createSessObj( $objName )
	{
	
		global $mainframe, $option;


		$controllerName = "MetalakeHelperCore";
		$functionName = "clearSessObj";

		$flag_production = 0;
		
		if ( $flag_production )
		{
	
	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "core_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "core_prod";
	
		}
		else
		{
	
	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "core_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "core_prod";
	
		}	

		$stamp_start_micro = microtime(true);
	
		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



	
		
		$objData = new stdClass();
		
		$session =& JFactory::getSession();
		$session->set( $objName , $objData) ;
		
		
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objName: ".print_r($objName, true);
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		
		
		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
	
		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objData ;
	}



	public static function getSessObj( $objName )
	{

		$controllerName = "MetalakeHelperCore";
		$functionName = "getSessObj";

		$flag_production = 0;
		
		if ( $flag_production )
		{
	
	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "core_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "core_prod";
	
		}
		else
		{
	
	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "core_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "core_prod";
	
		}	

		$stamp_start_micro = microtime(true);
	
		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
		
		$session = JFactory::getSession();

		$objData = $session->get( $objName ) ;
		
		$objData = ( !is_object($objData) ) ? MetalakeHelperCore::createSessObj( $objName ) : $objData  ;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objName: ".print_r($objName, true);
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		
		
		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
	
		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		return $objData ;
		
	}



	public static function setSessObj( $objName, $objData )
	{

		$controllerName = "MetalakeHelperCore";
		$functionName = "setSessObj";

		$flag_production = 0;
		
		if ( $flag_production )
		{
	
	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "core_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "core_prod";
	
		}
		else
		{
	
	        $objLogger = new stdClass();
	        $objLogger->logger = 0;
	        $objLogger->logFile = "core_verbose";
	        $objLogger->loggerProd = 0;
	        $objLogger->logFileProd = "core_prod";
	
		}	

		$stamp_start_micro = microtime(true);
	
		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$session =& JFactory::getSession();
		$session->set( $objName , $objData) ;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objName: ".print_r($objName, true);
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		
		
		
		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
	
	
		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
	}




	
	 #####                                       
	#     #  ####   ####  #    # # ######  ####  
	#       #    # #    # #   #  # #      #      
	#       #    # #    # ####   # #####   ####  
	#       #    # #    # #  #   # #           # 
	#     # #    # #    # #   #  # #      #    # 
	 #####   ####   ####  #    # # ######  ####  

	// !COOKIES
	
	public function setCookie( $strCookieName, $objCookie )
	{
		
		$logger = 0;
		$logFile = "metalake";	

		// Get input cookie object
		$inputCookie  = JFactory::getApplication()->input->cookie;
		
		$jsonCookie = json_encode($objCookie);
			
		$b64eCookie = base64_encode($jsonCookie);

		$expire = time()+60*60*24*30*365;  // 1 year

		// Set cookie data
		$inputCookie->set( $name = $strCookieName, $value = $b64eCookie, $expire );

		$logMessage = "";
		$logMessage .= "--- setCookie\n\r";
		$logMessage .= "------ cookie name: ".print_r($strCookieName, true)."\n\r";
		$logMessage .= "------ objCookie: ".print_r($objCookie, true)."\n\r";
		$logMessage .= "------ jsonCookie: ".print_r($jsonCookie, true)."\n\r";
		$logMessage .= "------ b64eCookie: ".print_r($b64eCookie, true)."\n\r";
		$logMessage .= "------ expire: ".print_r($expire, true)."\n\r";
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

		
	}



	public function getCookie( $strCookieName )
	{
		
		$logger = 0;
		$logFile = "metalake";	

		// Get input cookie object
		$inputCookie  = JFactory::getApplication()->input->cookie;
		
		$b64dCookie        = $inputCookie->get(  $name = $strCookieName, $defaultValue = null);

		$cookieExists = ($b64dCookie === null) ? "0" : "1" ;

		if ( $cookieExists )
		{

			$jsonCookie = base64_decode($b64dCookie);
			
			$objCookie = json_decode($jsonCookie);
			$objCookie->cookieExists = $cookieExists;
			
		}
		else
		{
			$objCookie = new stdClass();
			$objCookie->cookieExists = $cookieExists;
			
		}		


		$logMessage = "";
		$logMessage .= "--- getCookie\n\r";
		$logMessage .= "------ cookie name: ".print_r($strCookieName, true)."\n\r";
		$logMessage .= "------ cookieExists: ".print_r($cookieExists, true)."\n\r";
		$logMessage .= "------ b64dCookie: ".print_r($b64dCookie, true)."\n\r";
		$logMessage .= "------ jsonCookie: ".print_r($jsonCookie, true)."\n\r";
		$logMessage .= "------ objCookie: ".print_r($objCookie, true)."\n\r";
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }		

		return $objCookie;
		
	}


	public function clearCookie( $strCookieName )
	{
		
		$logger = 0;
		$logFile = "metalake";	

		$inputCookie  = JFactory::getApplication()->input->cookie;
		
		$inputCookie->set($strCookieName, null, time() - 1);

		$logmessage = "";
		$logmessage .= "--- clearCookie\n\r";
		$logmessage .= "------ cookie name: ".print_r($strCookieName, true)."\n\r";
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

	}


	
	 #####                                                                 
	#     #   ##   #      ######  ####  ######  ####  #####   ####  ###### 
	#        #  #  #      #      #      #      #    # #    # #    # #      
	 #####  #    # #      #####   ####  #####  #    # #    # #      #####  
	      # ###### #      #           # #      #    # #####  #      #      
	#     # #    # #      #      #    # #      #    # #   #  #    # #      
	 #####  #    # ###### ######  ####  #       ####  #    #  ####  ###### 

	// !SALESFORCE	 

	public function salesforceConnect()
	{

		$isProduction = 1;


		$app            		= JFactory::getApplication();
	    $params_ml     			= $app->getParams('com_metalake');
	    $salesforce_username	= $params_ml->get('salesforce_username');
	    $salesforce_password	= $params_ml->get('salesforce_password');
	    $salesforce_token		= $params_ml->get('salesforce_token');

		$mySforceConnection 	= new SforcePartnerClient();
		
		if ( $isProduction )
		{
		
			#production
			$mySoapClient = $mySforceConnection->createConnection("libraries/salesforce/partner.wsdl.xml");
		
		}
		else
		{
		
			#development
			$mySoapClient = $mySforceConnection->createConnection("libraries/salesforce/partnerdev.wsdl.xml");

		}
		
		$mylogin = $mySforceConnection->login( $salesforce_username , $salesforce_password.$salesforce_token );		
	
		return $mySforceConnection;
	}


	public function salesforceSafeConnectLog()
	{
		global $mainframe, $option;
		
		$logger = 0;
		$logFile = "metalake";	
		
		try 
		{
			$salesforceConnect = MetalakeHelperCore::salesforceConnect();
			
		
			//throw exception if can't move the file
			if ( !$salesforceConnect ) 
			{
				throw new Exception('Could not login to Salesforce');
			}
			
			$logMessage = "salesforceSafeConnectLog | login complete";
			if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }	
			
			return $salesforceConnect;
		} 
		catch (Exception $e) 
		{
			$logMessage = 'salesforceSafeConnectLog | error: ' . $e->getMessage();
			if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }			
			
			return $fail = "0";
		}
	}


	public function salesforceSafeConnect()
	{

        $app = JFactory::getApplication();
        $params = $app->getParams();
		$errorpage_itemid = $params->get("errorpage_itemid");

	
		$salesforceConnect =  MetalakeHelperCore::salesforceSafeConnectLog();


			if( !$salesforceConnect )
			{
				
				$returnLink = JRoute::_( 'index.php?Itemid='.$errorpage_itemid );
				
				$app->redirect($returnLink);
				
			}

		return $salesforceConnect;
		
	}

	// !COMMON FUNCTIONS

	public static function getObjTableData( $table, $field, $value )
	{
		

		$controllerName = "MetalakeHelperCore";
		$functionName = "getObjTableData";

		$db = JFactory::getDBO();

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		

		$query = "SELECT * from $table where ".$field." = \"".$value."\"";
		$db->setQuery($query);
		$objData = $db->loadObject();


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		return $objData;
		
		
		
	}

	public function getRemoteDBConnection( )
	{
		

		$controllerName = "MetalakeHelperCore";
		$functionName = "getRemoteDBConnection";

		$db = JFactory::getDBO();

		$flag_production = 0;


		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


			#database
			$option = array(); //prevent problems
			$option['driver']   = 'mysql';            // Database driver name
			$option['host']     = 'host.com';    // Database host name
			$option['user']     = 'db_prod';       // User for database authentication
			$option['password'] = 'pwd';   // Password for database authentication
			$option['database'] = 'db_prod';      // Database name
			$option['prefix']   = '';             // Database prefix (may be empty)
			$dbRemote = JDatabaseDriver::getInstance( $option );



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r db: ".print_r($db, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $dbRemote;
	}






}