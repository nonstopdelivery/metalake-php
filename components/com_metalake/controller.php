<?php

/**
 * @version     1.0.0
 * @package     com_metalake
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Lew Sawyer <lsawyer@metalake.com> - http://www.metalake.com
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

JLoader::register('MetalakeHelperCore', JPATH_COMPONENT.'/helpers/core.php');
//JLoader::register('Geocoder', JPATH_COMPONENT.'/helpers/mlgeocode.php');





class MetalakeController extends JControllerLegacy {

    /**
     * Method to display a view.
     *
     * @param	boolean			$cachable	If true, the view output will be cached
     * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return	JController		This object to support chaining.
     * @since	1.5
     */
    public function display($cachable = false, $urlparams = false) {
        require_once JPATH_COMPONENT . '/helpers/metalake.php';

        $view = JFactory::getApplication()->input->getCmd('view', 'metalakes');
        JFactory::getApplication()->input->set('view', $view);

        parent::display($cachable, $urlparams);

        return $this;
    }


		public function logRotate()
		{

			# http://devc.metalake.net/index.php?option=com_metalake&task=logRotate

			$logger = 1;
			$logFile = "logrotate";			

	    	$app            		= JFactory::getApplication();
	        $params_ml     			= $app->getParams('com_metalake');

			$objParams = new stdClass();
			$objParams->logrotate_files	= $params_ml->get('logrotate_files');
			$objParams->logrotate_days_to_archive	= $params_ml->get('logrotate_days_to_archive');


			$logMessage = "START | logRotate";
			if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

			$logPath = JPATH_SITE."/logs/";

			$dayNumber = date("d");

			$strOfLogFiles = str_replace(" ", "", $objParams->logrotate_files);

			$arrLogFiles = explode( ",", $strOfLogFiles );


			$logMessage = "INSIDE | logRotate";
			$logMessage .= "\n\r objParams: ".print_r($objParams, true);
			$logMessage .= "\n\r logPath: ".print_r($logPath, true);
			$logMessage .= "\n\r dayNumber: ".print_r($dayNumber, true);
			$logMessage .= "\n\r arrLogFiles: ".print_r($arrLogFiles, true);
			if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }


			foreach( $arrLogFiles as $file )
			{

				if ( file_exists( $logPath.$file ) )
				{

					$command = "cp ".$logPath.$file." ".$logPath.$dayNumber."_".$file." 2>&1" ;

					$logMessage = "INSIDE | logRotate";
					$logMessage .= "\n\r command: ".print_r($command, true);
					if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

					exec( $command, $output, $return_var );		


					$command = "gzip ".$logPath.$dayNumber."_".$file." 2>&1" ;

					$logMessage = "INSIDE | logRotate";
					$logMessage .= "\n\r command: ".print_r($command, true);
					if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

					exec( $command, $output, $return_var );		


					$command = "> ".$logPath.$file." 2>&1" ;

					$logMessage = "INSIDE | logRotate";
					$logMessage .= "\n\r command: ".print_r($command, true);
					if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

					exec( $command, $output, $return_var );	


				}
				else
				{
					$logMessage = "INSIDE | logRotate";
					$logMessage .= "\n\r file does not exist: ".print_r($logPath.$file, true);
					if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }
				}


			}


			$command = "find ".$logPath."*.gz -mtime +".$objParams->logrotate_days_to_archive." -type f -exec rm -f {} + 2>&1" ;

			$logMessage = "INSIDE | logRotate";
			$logMessage .= "\n\r command: ".print_r($command, true);
			if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

			exec( $command, $output, $return_var );		





			$logMessage = "END | logRotate";
			if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }				


		}





	// !TEST

	function test_config()
	{
		
		# http://dev5.metalake.net/index.php?option=com_metalake&task=test_config


		$logger = 1;
		$logFile = "metalake";
		
    
    	$app            		= JFactory::getApplication();
        $params_ml     			= $app->getParams('com_metalake');

		$objParams = new stdClass();
		$objParams->salesforce_username	= $params_ml->get('salesforce_username');
		$objParams->salesforce_password	= $params_ml->get('salesforce_password');
		$objParams->salesforce_token	= $params_ml->get('salesforce_token');

		$logMessage = print_r( $objParams, true );
    
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }
    
	}




	public function test_logger( )
	{

		# http://dev4.metalake.net/index.php?option=com_metalake&task=test_logger
		
		$controllerName = "MetalakeController";
		$functionName = "test_logger";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "metalake_prod";

		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$hello_world = "Hello World";


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r hello_world: ".print_r($hello_world, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


	public function test_remote_sql( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_api&task=test_remote_sql
		# http://deve.metalake.net/index.php?option=com_nsd_api&task=test_remote_sql
		
		$controllerName = "MetalakeController";
		$functionName = "test_remote_sql";
		
		#$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "api_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "api_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "api_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$dbRemote = MetalakeHelperCore::getRemoteDBConnection();
		
		$tracking_no = "31022005";
		
		#$query = "SELECT * from htc_nsd_data where tracking_no = '".$tracking_no."' ";
		$dbRemote->setQuery($query);
		$objListRecords = $dbRemote->loadObjectList();

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objListRecords: ".print_r($objListRecords, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	




	function test_sf()
	{
		# http://dev5.metalake.net/index.php?option=com_metalake&task=test_logger

		$logger = 1;
		$logFile = "metalake";	
	
		$sfConnect = MetalakeHelperCore::salesforceSafeConnect();
	
		$logMessage = print_r( $sfConnect, true );
		
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }
	}



	 function test_getGeocode()
	 {
		 
	 	# http://dev5.metalake.net/index.php?option=com_metalake&task=test_getGeocode
		 
		# to get geodata, make sure allow_url_fopen is 'on' in the PHP server settings for the domain

	 	$address = "14 Meadowbrook Road, Wyoming, RI 02898, USA";

	 	MetalakeHelperCore::getGeocode( $address );

	 	$latlng = "38.906420935617476,-77.10777497913976";
	 
	 	MetalakeHelperCore::getGeocodeAddress( $latlng );
		 
	 }


	public function test_setCookie()
	{
		
		# http://dev5.metalake.net/index.php?option=com_metalake&task=test_setCookie
			
		$strCookieName = "mlLocation";
		
		$objLocation = new stdClass();
		$objLocation->latitude = "38.9065706";
		$objLocation->longitude = "-77.1078579";
		$objLocation->address = "4130 27th St N Arlington VA 22207";
	
		MetalakeHelperCore::setCookie( $strCookieName, $objLocation );

		$logger = 1;
		$logFile = "metalake";	
		$logMessage = "test_setCookie | strCookieName ".$strCookieName." | objLocation: ".print_r( $objLocation, true );
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }

		
	}



	public function test_getCookie()
	{
		
		# http://dev5.metalake.net/index.php?option=com_metalake&task=test_getCookie

		$logger = 1;
		$logFile = "metalake";	
		
		$strCookieName = "mlLocation";
		
		$objCookie = MetalakeHelperCore::getCookie( $strCookieName );
			
		$logMessage = "test_getCookie | objCookie: ".print_r( $objCookie, true );
	
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }
	
	}




	public function test_clearCookie()
	{
		
		# http://dev5.metalake.net/index.php?option=com_metalake&task=test_clearCookie
		
		$strCookieName = "mlLocation";
		
		MetalakeHelperCore::clearCookie( $strCookieName );

		$logger = 1;
		$logFile = "metalake";	
		$logMessage = "test_clearCookie | strCookieName ".$strCookieName;
		if ( $logger ) { MetalakeHelperCore::logger( $logFile, $logMessage ); }


		
	}

	public function test_email_validation( )
	{
		# http://dev6.metalake.net/index.php?option=com_metalake&task=test_email_validation

		$controllerName = "MetalakeController";
		$functionName = "test_email_validation";

		$db =& JFactory::getDBO();

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrEmail = array();
		$arrEmail[] = "lsawyer@metalake.com";
		$arrEmail[] = "lsawyer@metalake";
		$arrEmail[] = "lsawyermetalake.com";
		$arrEmail[] = "lsawyer@metalake.cm";
		

		foreach ( $arrEmail as $email )
		{
			
			$return = MetalakeHelperCore::validateEmail( $email );

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r email: ".print_r($email, true);
			$logMessage .= "\n\r return: ".print_r($return, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


	}	



	public function test_substring( )
	{
		# http://dev6.metalake.net/index.php?option=com_metalake&task=test_substring

		$controllerName = "MetalakeController";
		$functionName = "test_substring";

		$db =& JFactory::getDBO();

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 1;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "metalake_prod";

		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrInfo = array();
		$arrInfo[] = "Sender Phone: 8885551212";
		$arrInfo[] = "qwerty";
		$arrInfo[] = "Sender Phone lsawyermetalake.com";
		$arrInfo[] = "lsawyer@metalake.cm";
		

		foreach ( $arrInfo as $info )
		{
			$arrString = "";
			$return = "";
			
			if ( stripos(strtolower($info), 'Sender Phone:') !== false )
			{
				
				$arrString = explode(":", $info);
				
				$return = trim($arrString[1]);
				
			}
			else
			{
				$return = $info;
			}
			


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r info: ".print_r($info, true);
			$logMessage .= "\n\r arrString: ".print_r($arrString, true);
			$logMessage .= "\n\r return: ".print_r($return, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


	}



	// !FUNCTION TEMPLATE

		public function test_function_template( $file_name )
		{
			# http://dev0.metalake.net/index.php?option=com_metalake&task=test_function_template

			$controllerName = "MetalakeController";
			$functionName = "test_function_template";

			$db =& JFactory::getDBO();

			$flag_production = 0;

			if ( $flag_production )
			{

	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "metalake_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "metalake_prod";

			}
			else
			{

	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "metalake_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "metalake_prod";

			}

			$stamp_start_micro = microtime(true);

			$logMessage = "START | ".$controllerName." | ".$functionName;	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$aaa = "hello";
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r aaa: ".print_r($aaa, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


			$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
			$logMessage .= "\n\r \n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		}	


	
}
