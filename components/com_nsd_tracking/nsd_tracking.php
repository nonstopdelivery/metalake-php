<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Nsd_tracking', JPATH_COMPONENT);
JLoader::register('Nsd_trackingController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Nsd_tracking');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
