<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Nsd_trackingViewTrace extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');
		#$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_nsd_tracking');



		$controllerName = "Nsd_trackingViewTrace";
		$functionName = "view";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		$flag_run = JRequest::getVar('r', "0");
		$trace_number_old = JRequest::getVar('trace_number', '');
		$trace_number = JRequest::getVar('t', '');
		$trace_zipcode = JRequest::getVar('z', '');
		

/*
		if (!$format=JRequest::getVar('format', 'hmtl'))
		{
			$this->setRedirect(JRoute::_('index.php?option=com_nsd_tracking&amp;view=trace&amp;format='.$format));
			return false;
		}
*/

		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	


		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		$stamp_start_micro = microtime(true);



		#session
		#$objForm = MetalakeHelperCore::getSessObj( "objSessTracking" );
		
		$objForm = new stdClass();
		
		$jinput = JFactory::getApplication()->input;
		
		$trace_number_old = trim($jinput->get('trace_number', '', 'STRING'));
		$trace_number = trim($jinput->get('t', '', 'STRING'));
		$trace_zipcode = trim($jinput->get('z', '', 'STRING'));
		$trace_solo = trim($jinput->get('s', '', 'STRING'));
		$histUpdatedByEmail = trim($jinput->get('a', '', 'STRING'));

		$objForm->trace_number = ( $trace_number != "" ) ? $trace_number : $trace_number_old ;
		$objForm->trace_zipcode = $trace_zipcode;
		$objForm->flag_api = "0";
		$objForm->flag_trace_page = "1";
		$objForm->trace_solo = $trace_solo;
		$objForm->histUpdatedByEmail = $histUpdatedByEmail;
		
		$parts = explode("@", $histUpdatedByEmail);
		$objForm->histUpdatedBy = strtoupper($parts[0]);
		
		$objDataWorkato = new stdClass();
		$objDataWorkato->user_id = $objForm->histUpdatedBy;
		
		$objReturnWorkato = Nsd_workatoController::getTruckmateObjUser( $objDataWorkato );
		
		
		if ( $objReturnWorkato->error == "0" )
		{
			
			$objForm->notesTitleOfRep = $objReturnWorkato->objReturnFromWorkato->job_description;			
			$objForm->allowAddEntry = 1;
		}
		else
		{
			$objForm->notesTitleOfRep = "";			
			$objForm->allowAddEntry = 0;
			$objForm->allowAddEntryErrorMessage = "Sorry, you are not able to add an entry at this time. The user does not exist in TruckMate.  Please contact your supervisor for assistance.";

		}		
		
		$objForm->notesTitleOfRep = $objReturnWorkato->objReturnFromWorkato->job_description;
		
		$this->assignRef('objForm', $objForm); 
		
		


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r objReturnWorkato: ".print_r($objReturnWorkato, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objDisplay = new stdClass();
		$objDisplay->flag_run = $flag_run;

		if ( $flag_run == "1" )
		{


			if ( trim($objForm->trace_number) == "" )
			{

				$objDisplay->error = 1;
				$objDisplay->error_code = 33;
				$objDisplay->error_message = "<h2>No Trace Value</h2>No trace value was entered. Please enter a trace value.";
				$this->assignRef('objDisplay', $objDisplay);
				
			}
			else
			{
				
				#truckmate
				$objDisplay->dataOrigin = "truckmate";

				$objDisplay->error = 0;
				$objDisplay->error_code = "";
				$objDisplay->error_message = "";
		
		
				if ( $objForm->trace_solo == "1" )
				{


					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | beacon 1";
					$logMessage .= "\n\r objForm: ".print_r($objForm, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


					
					$objListTMW = new stdClass();
					$objListTMW->display = 0;
					$objListTMW->error = 1;
					$objListTMW->error_message = "";
					$this->assignRef('objListTMW', $objListTMW);
					

					$objTMW = Nsd_truckmateController::getTruckmateOrderFromWorkato( $objForm );
					
					
					if ( $objTMW->error == "1" )
					{
						$objDisplay->error = 1;
						$objDisplay->error_code = 44;
						$objDisplay->error_message = "<h2>Error</h2>No order was found. Please try again.";
						$this->assignRef('objDisplay', $objDisplay);
					}
					else
					{
							$objTMW->display = 1;
							$this->assignRef('objTMW', $objTMW);				
					}
										
				}
				else
				{
					
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | beacon 2";
					$logMessage .= "\n\r objForm: ".print_r($objForm, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



					#$objTMW = Nsd_truckmateController::getTruckmateData( $objForm );
					$objListTMW = Nsd_truckmateController::getTruckmateListOrderFromWorkato( $objForm );				
			
					if ( $objListTMW->error == "1" )
					{
						$objDisplay->error = 1;
						$objDisplay->error_code = 33;
						$objDisplay->error_message = "<h2>Error</h2>There is an error with the TMS connection . Please contact the Administrator.";
						$this->assignRef('objDisplay', $objDisplay);
					}
					else
					{
			
						switch( $objListTMW->rowresults  )
						{
							case "0":
	
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | beacon 3";
								$logMessage .= "\n\r objForm: ".print_r($objForm, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
	
	
								$objTMW = new stdClass();
								$objTMW->display = 0;
								$objTMW->error = 0;
								$objTMW->error_message = "";
								$this->assignRef('objTMW', $objTMW);
	
								$objListTMW->display = 1;
								$objListTMW->error = 1;
								$objListTMW->error_message = "<h2>No Results</h2>There are no orders matching your trace value.";
								$this->assignRef('objListTMW', $objListTMW);
							
								break;
	
							case "1":
	
	
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | beacon 4";
								$logMessage .= "\n\r objForm: ".print_r($objForm, true);
								$logMessage .= "\n\r objListTMW: ".print_r($objListTMW, true);
								$logMessage .= "\n\r objListTMW->orders[0]: ".print_r($objListTMW->orders[0], true);
								$logMessage .= "\n\r objListTMW->orders[0]->BILLNUMBER: ".print_r($objListTMW->orders[0]->BILLNUMBER, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
								$BILLNUMBER = $objListTMW->orders[0]->BILLNUMBER;
	
								$objListTMW = new stdClass();
								$objListTMW->display = 0;
								$objListTMW->error = 0;
								$objListTMW->error_message = "";
								$this->assignRef('objListTMW', $objListTMW);
	
								
	
								$objForm->trace_number = $BILLNUMBER;
								$objForm->trace_solo = "1";

								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | beacon 4.5";
								$logMessage .= "\n\r temp: ".print_r($temp, true);
								$logMessage .= "\n\r objForm: ".print_r($objForm, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
	
								#$objTMW = Nsd_truckmateController::getTruckmateData( $objForm );
								$objTMW = Nsd_truckmateController::getTruckmateOrderFromWorkato( $objForm );
								$objTMW->display = 1;
								$objTMW->error = 0;
								$this->assignRef('objTMW', $objTMW);	
							
								break;
	
	
	
							default:
	
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | beacon 5";
								$logMessage .= "\n\r objForm: ".print_r($objForm, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
	
								$objTMW = new stdClass();
								$objTMW->display = 0;
								$objTMW->error = 1;
								$objTMW->error_message = "";
								$this->assignRef('objTMW', $objTMW);
							
							
								$objListTMW->display = 1;
								$this->assignRef('objListTMW', $objListTMW);
								break;
							
							
						}
			
			
						
					}
			
					
				}
		
		
		
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r objListTMW: ".print_r($objListTMW, true);
				$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
				$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
				#$this->assignRef('objTMW', $objTMW);



				#$objTMWNew = Nsd_truckmateController::getTruckmateOrderFromWorkato( $objForm );
				#$objTMWNew = Nsd_workatoController::getTruckmateOrderData( $objForm );
				


	
				#log the activity
	
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				    $remote_address = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				    $remote_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
				    $remote_address = $_SERVER['REMOTE_ADDR'];
				}
	
				$objActivityLog = new stdClass();
				$objActivityLog->remote_address =  $remote_address;
				$objActivityLog->trace = $trace_number;
				$objActivityLog->trace_type = "Bill Number";
				$objActivityLog->delivery_type = ""; //$objDisplay->delivery_type_description;
				$objActivityLog->order_type = ""; //$objDisplay->order_type;
				$objActivityLog->json_return = ""; //json_encode($objDisplay);
				$objActivityLog->tm4web_usr = ""; //$tm4web_usr;
				$objActivityLog->flag_api = $objForm->flag_api;
				$objActivityLog->return_format = "web";
				
				Nsd_trackingController::logActivity( $objActivityLog );
				
			}	
		
		}
		else
		{

			$objDisplay->error = 1;
			$objDisplay->error_code = 00;
			$objDisplay->error_message = "";
			$this->assignRef('objDisplay', $objDisplay);
			
		}

		$this->assignRef('objDisplay', $objDisplay);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_nsd_tracking');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_NSD_TRACKING_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
