<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


$showobject = false;
$allowAddEntry = true;
$alloweditDeliveryPickup = false;


$arrStatus = array();
$arrStatus[] = "APPRVD";
$arrStatus[] = "BILLED";
$arrStatus[] = "CANCELLED";
$arrStatus[] = "PRINTED";
$arrStatus[] = "ST PRINTED";

# do not allow editing of Consignee or Shipper if the order has a status in array above.
if ( !(in_array( trim($this->objTMW->order->CURRENT_STATUS), $arrStatus ) ) )
{
	$allowedit = true;
}
else
{
	$allowedit = false;	
}






#sorting using datatables
#https://datatables.net/examples/index
#https://datatables.net/examples/basic_init/table_sorting
#https://datatables.net/forums/discussion/45692/how-to-date-sort-as-date-instead-of-string
#https://datatables.net/examples/styling/bootstrap.html

echo <<<METALAKE
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

$(document).ready(function(e) {
    $('#statushistorytable').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    $('#listordertable').DataTable( {
        "order": [[ 8, "desc" ]]
    } );

	e.preventDefault();

} );
</script> 
METALAKE;
#this must be left justified



$display = "";

$display .= "<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='get' name='adminForm' id='adminForm' class='form-inline track'>";
$display .= "	<div class='row'>";
$display .= "		<div class='form-group col-sm-3'>";
$display .= "			<label for='trace_number'>Trace</label>";
$display .= "			<input type='text' class='form-control' name='t' id='t' value=\"".$this->objForm->trace_number."\" placeholder='Enter Trace Value' />";
$display .= "		</div>";
/*
$display .= "		<div class='form-group col-sm-3'>";
$display .= "			<label for='trace_zipcode'>Zip Code</label>";
$display .= "			<input type='text' class='form-control' name='z' id='z' value='".$this->objForm->trace_zipcode."' placeholder='Enter Zip Code' />";
$display .= "		</div>";
*/
$display .= "		<div class='form-group col-sm-2'>";
$display .= "			<label for='sendResponse'>&nbsp;</label>";
$display .= "			<input class='form-control btn' type='Submit' id='sendResponse' value='Trace' onclick=\"document.adminForm.submit();\" />";
#$display .= "			<input type='hidden' name='trace_number' />";
$display .= "		</div>";
$display .= "	</div>";
$display .= "<input type='hidden' name='r' value='1' />";
$display .= "<input type='hidden' name='a' value='".$this->objForm->histUpdatedByEmail."' />";

$display .= "</form>";

if ( $this->objListTMW->display == "1" ) 
{

	if ( $this->objListTMW->error == "0" )
	{

		$display .= "				<div class='table-responsive'>";
		$display .= "				<table class='table history' id='listordertable'>";
		$display .= "				<thead>";
		$display .= "				<tr>";
		$display .= "					<th>Freight Bill</div>";
		$display .= "					<th>Client</div>";
		$display .= "					<th>Consignee Name</div>";
		$display .= "					<th>Consignee City</div>";
		$display .= "					<th>Consignee State</div>";
		$display .= "					<th>Consignee Phone</div>";
		$display .= "					<th>Order Type</div>";
		$display .= "					<th>Order Status</div>";
		$display .= "					<th>Date of Last Update</div>";
		$display .= "				</tr>";
		$display .= "				</thead>";
		$display .= "				<tbody>";
		foreach ($this->objListTMW->orders as $order) :
			$display .= "				<tr>";
			$display .= "					<td><a href='".$order->LINK."'>".$order->BILLNUMBER."</a></td>";
			$display .= "					<td>".$order->CALLNAME."</td>";
			$display .= "					<td>".$order->SHIPPERNAME."</td>";
			$display .= "					<td>".$order->SHIPPERCITY."</td>";
			$display .= "					<td>".$order->SHIPPERPROV."</td>";
			$display .= "					<td>".$order->SHIPPERPHONE."</td>";
			$display .= "					<td>".$order->ORDERTYPE."</td>";
			$display .= "					<td>".$order->ORDERSTATUS."</td>";
			$display .= "					<td data-sort='".strtotime($order->ROWTIMESTAMP)."'>".date('m/d/Y g:i A', strtotime($order->ROWTIMESTAMP) )."</td>";
			$display .= "				</tr>";
		endforeach;
		$display .= "				</tbody>";
		$display .= "				</table>";
		$display .= "				</div>"; // table-responsive
		
	}
	else
	{
		$display .= $this->objListTMW->error_message;
	}
}





if ( $this->objDisplay->error == "0" )
{ 

	if ( $this->objTMW->display == "1" )
	{
	
		if ( $this->objTMW->error == "0" )
		{
	
	
			$display .= "<div class='details section row'>";
			$display .= "	<div class='col-sm-3'><h2>Freight Bill: ".$this->objTMW->order->BILL_NUMBER."</h2><br /></div>";
			if ($this->objTMW->order->SITE_ID == "SITE6") {
			$display .= "	<div class='col-sm-3'>";
			$display .= "		<div class='item'>";
			$display .= "			<span>Delivery</span>";
			$display .=				$alloweditDeliveryPickup ? "<a class='modalToggle' data-toggle='modal' data-target='#editDelivery'><span class='glyphicon glyphicon-edit'></span></a>" : "";
			$display .= "		</div>";
			$display .= "		<div class='item'>".date('m/d/Y g:i A', strtotime($this->objTMW->order->DELIVER_BY) )." - ".date('m/d/Y g:i A', strtotime($this->objTMW->order->DELIVER_BY_END) )."</div>";
			
			$dam = ($this->objTMW->order->DELIVERY_APPT_MADE == "False") ? 'Appt Made: NO' : 'Appt Made: YES';
			$display .= "		<div class='item'><span>".$dam."</span></div>";
			
			$impby = ( trim($this->objTMW->order->USER5) == "" ) ? 'Impacted By:' : 'Impacted By: '.trim($this->objTMW->order->USER5);
			$display .= "		<div class='item'><span>".$impby."</span></div>";			
			
			
			$display .= "	</div>";
			}
			elseif ($this->objTMW->order->SITE_ID == "SITE5") {
			$display .= "	<div class='col-sm-3'>";
			$display .= "		<div class='item'>";
			$display .= "			<span>Pick Up</span>";
			$display .=				$alloweditDeliveryPickup ? "<a class='modalToggle' data-toggle='modal' data-target='#editPickup'><span class='glyphicon glyphicon-edit'></span></a>" : "";
			$display .= "		</div>";
			$display .= "		<div class='item'>".date('m/d/Y g:i A', strtotime($this->objTMW->order->PICK_UP_BY) )." - ".date('m/d/Y g:i A', strtotime($this->objTMW->order->PICK_UP_BY_END) )."</div>";
			
			$puam = ($this->objTMW->order->PICK_UP_APPT_MADE == "False") ? 'Appt Made: NO' : 'Appt Made: YES';
			
			$display .= "		<div class='item'><span>".$puam."</span></div>";
			
			
			$impby = ( trim($this->objTMW->order->USER5) == "" ) ? 'Impacted By:' : 'Impacted By: '.trim($this->objTMW->order->USER5);
			$display .= "		<div class='item'><span>".$impby."</span></div>";				
			
			
			$display .= "	</div>";
			}
			$display .= "	<div class='col-sm-3'>";
			$display .= "		<div class='item'><span>Status: </span>".$this->objTMW->order->CURRENT_STATUS."</div>";
			$display .= "		<div class='item'><span>Mode: </span>".$this->objTMW->order->OP_CODE."</div>";
			$display .= "		<div class='item'><span>Service: </span>".$this->objTMW->order->SERVICE_LEVEL."</div>";
			$display .= "		<div class='item'><span>LMC: </span>".$this->objTMW->order->USER8."</div>";
			$display .= "		<div class='item'><span>AM: </span>".$this->objTMW->order->USER9."</div>";
			$display .= "	</div>";

			$display .= "	<div class='col-sm-3'>";
			$display .= "		<div class='item'><span>Shipping Instructions</span></div>";
			$display .= 			"<ul>";
			foreach ($this->objTMW->shipping_instructions as $shipping_instruction) :
			$display .= 			"<li>".$shipping_instruction."</li>";
			endforeach;
			$display .= 			"</ul>";
			$display .= "	</div>";
			
			$display .= "</div>"; // /details
			
			$display .= "<div class='details section row'>";
			$display .= "	<div class='col-sm-3'>";
			$display .= "		<div class='item'><span>Caller</span></div>";
			$display .= "		<div class='item'>".$this->objTMW->order->CUSTOMER." - ".$this->objTMW->order->CALLNAME."</div>";
			$display .= "	</div>";
			$display .= "	<div class='col-sm-3'>";
			$display .= "		<div class='item'><span>Shipper</span>";
			$display .=				(($this->objTMW->order->SITE_ID == "SITE5") && $allowedit) ? "<a class='modalToggle' data-toggle='modal' data-target='#editShipper'><span class='glyphicon glyphicon-edit'></span></a>" : "";
			$display .= "		</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->ORIGID."</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->ORIGNAME."</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->ORIGADDR1."</div>";
			$display .= 		$this->objTMW->order->ORIGADDR2 ? "		<div class='item'>".$this->objTMW->order->ORIGADDR2."</div>" : "";
			$display .= "		<div class='item'>".$this->objTMW->order->ORIGCITY.", ".$this->objTMW->order->ORIGPROV." ".$this->objTMW->order->ORIGPC."</div>";
			
			$oext = ($this->objTMW->order->ORIGPHONEEXT != '') ? "&nbsp;&nbsp;<span>ext:</span>".$this->objTMW->order->ORIGPHONEEXT : "";
			$display .= ($this->objTMW->order->ORIGPHONE != '') ? "<div class='item'><span>Phone: </span>".$this->objTMW->order->ORIGPHONE.$oext."</div>" : "";
			#$display .= ($this->objTMW->order->ORIGPHONEEXT != '') ? "<div class='item'><span>Phone Ext: </span>".$this->objTMW->order->ORIGPHONEEXT."</div>" : "";
			$display .= ($this->objTMW->order->ORIGCELL != '') ? "<div class='item'><span>Cell: </span>".$this->objTMW->order->ORIGCELL."</div>" : "";
			$display .= ($this->objTMW->order->ORIGEMAIL != '') ? "<div class='item'><span>Email: </span>".$this->objTMW->order->ORIGEMAIL."</div>" : "";
			$display .= "	</div>";
			$display .= "	<div class='col-sm-3'>";
			$display .= "		<div class='item'><span>NSD Terminal (Agent)</span></div>";
			$display .= "		<div class='item'>".$this->objTMW->order->CARE_OF."</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->CARE_OF_NAME."</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->CARE_OF_ADDR1."</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->CARE_OF_CITY.", ".$this->objTMW->order->CARE_OF_PROV." ".$this->objTMW->order->CARE_OF_PC."</div>";
			
			$aext = ($this->objTMW->order->CARE_OF_PHONEEXT != '') ? "&nbsp;&nbsp;<span>ext:</span>".$this->objTMW->order->CARE_OF_PHONEEXT : "";
			$display .= ($this->objTMW->order->CARE_OF_PHONE != '') ? "<div class='item'><span>Phone: </span>".$this->objTMW->order->CARE_OF_PHONE.$aext."</div>" : "";
			$display .= ($this->objTMW->order->CARE_OF_EMAIL != '') ? "<div class='item'><span>Email: </span>".$this->objTMW->order->CARE_OF_EMAIL."</div>" : "";
			$display .= "	</div>";
			$display .= "	<div class='col-sm-3'>";
			$display .= "		<div class='item'>";
			$display .= "			<span>Consignee</span>";
			$display .=				(($this->objTMW->order->SITE_ID == "SITE6") && $allowedit) ? "<a class='modalToggle' data-toggle='modal' data-target='#editConsignee'><span class='glyphicon glyphicon-edit'></span></a>" : "";
			$display .= "		</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->DESTID."</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->DESTNAME."</div>";
			$display .= "		<div class='item'>".$this->objTMW->order->DESTADDR1."</div>";
			$display .= 		$this->objTMW->order->DESTADDR2 ? "		<div class='item'>".$this->objTMW->order->DESTADDR2."</div>" : "";
			
			$cext = ($this->objTMW->order->DESTPHONEEXT != '') ? "&nbsp;&nbsp;<span>ext:</span>".$this->objTMW->order->DESTPHONEEXT : "";
			$display .= "		<div class='item'>".$this->objTMW->order->DESTCITY.", ".$this->objTMW->order->DESTPROV." ".$this->objTMW->order->DESTPC."</div>";
			$display .= ($this->objTMW->order->DESTPHONE != '') ? "<div class='item'><span>Phone: </span>".$this->objTMW->order->DESTPHONE.$cext."</div>" : "";
			#$display .= ($this->objTMW->order->DESTPHONEEXT != '') ? "<div class='item'><span>Phone Ext: </span>".$this->objTMW->order->DESTPHONEEXT."</div>" : "";
			$display .= ($this->objTMW->order->DESTCELL != '') ? "<div class='item'><span>Cell: </span>".$this->objTMW->order->DESTCELL."</div>" : "";
			$display .= ($this->objTMW->order->DESTEMAIL != '') ? "<div class='item'><span>Email: </span>".$this->objTMW->order->DESTEMAIL."</div>" : "";
			$display .= "	</div>";
			$display .= "</div>"; 
			
			$display .= "<div class='history section row'>";
			
			if ( $this->objForm->allowAddEntry == "1" )
			{
				$display .=	"<a class='modalToggle btn' data-toggle='modal' data-target='#addHistory'>Add Entry</a><br /><br />";			
			}
			else
			{
				$display .=	"<a class='modalToggle btn' disabled >Add Entry</a><div class='error'>".$this->objForm->allowAddEntryErrorMessage."</div><br /><br />";				
			}
	
			

			
			
			$display .= "	<div class='col-sm-12'>";
			
			$display .= "		<ul class='nav nav-tabs' role='tablist'>";
			$display .= "			<li role='presentation' class='active'><a href='#statushistory' aria-controls='statushistory' role='tab' data-toggle='tab'>Status History</a></li>";
			$display .= "			<li role='presentation'><a href='#trace' aria-controls='trace' role='tab' data-toggle='tab'>Trace</a></li>";
			$display .= "			<li role='presentation'><a href='#details' aria-controls='details' role='tab' data-toggle='tab'>Details</a></li>";
			$display .= "			<li role='presentation'><a href='#notes' aria-controls='notes' role='tab' data-toggle='tab'>Notes</a></li>";
			$display .= "		</ul>";
			
			$display .= " 		<div class='tab-content'>";
			
			$display .= "			<div role='tabpanel' class='tab-pane active' id='statushistory'>"; 
			$display .= "				<div class='table-responsive'>";
			$display .= "				<table class='table history' id='statushistorytable'>";
			$display .= "				<thead>";
			$display .= "				<tr>";
			$display .= "					<th>Date Inserted</div>";
			$display .= "					<th>Date Changed</div>";
			$display .= "					<th>Status Code</div>";
			$display .= "					<th>Reason Code</div>";
			$display .= "					<th class='wrap'>Comment</div>";
			$display .= "					<th>Updated By</div>";
			$display .= "				</tr>";
			$display .= "				</thead>";
			$display .= "				<tbody>";
			foreach ($this->objTMW->order_history as $history) :
				$display .= "				<tr>";
				$display .= "					<td data-sort='".strtotime($history->OS_INS_DATE)."'>".date('m/d/Y g:i A', strtotime($history->OS_INS_DATE) )."</td>";
				$display .= "					<td data-sort='".strtotime($history->OS_CHANGED)."'>".date('m/d/Y g:i A', strtotime($history->OS_CHANGED) )."</td>";
				$display .= "					<td>".$history->OS_STATUS_CODE."</td>";
				$display .= "					<td>".$history->OS_SF_REASON_CODE."</td>";
				$display .= "					<td class='wrap'>".$history->OS_STAT_COMMENT."</td>";
				$display .= "					<td>".$history->OS_UPDATED_BY."</td>";
				$display .= "				</tr>";
			endforeach;
			$display .= "				</tbody>";
			$display .= "				</table>";
			$display .= "				</div>"; // table-responsive
			#$display .=					$allowedit ? "<a class='modalToggle btn' data-toggle='modal' data-target='#addHistory'>Add Entry</a><br /><br />" : "";
			$display .= "			</div>"; // tab-content status history
			
			
			
			
			$display .= "			<div role='tabpanel' class='tab-pane' id='trace'>";
			$display .= "				<div class='table-responsive'>";
			$display .= "				<table class='table history'>";
			$display .= "				<thead>";
			$display .= "				<tr>";
			$display .= "					<th>Description</div>";
			$display .= "					<th>Trace Number</div>";
			$display .= "				</tr>";
			$display .= "				</thead>";
			$display .= "				<tbody>";
			foreach ($this->objTMW->trace_types as $trace) :
				$display .= "			<tr>";
				$display .= "				<td>".$trace->TRACE_TYPE."</td>";
				$display .= "				<td>".$trace->TRACE_NUMBER."</td>";
				$display .= "			</tr>";
			endforeach;
			$display .= "				</tbody>";
			$display .= "				</table>";
			$display .= "				</div>"; // table-responsive
			$display .= "			</div>"; // /trace
			
			
			
			
			$display .= "			<div role='tabpanel' class='tab-pane' id='details'>";
			$display .= "				<div class='table-responsive'>";
			$display .= "				<table class='table history'>";
			$display .= "				<thead>";
			$display .= "				<tr>";
			$display .= "					<th>Description</div>";
			$display .= "					<th>Pieces</div>";
			$display .= "					<th>PCS Units</div>";
			$display .= "					<th>Weight</div>";
			$display .= "					<th>Wgt Units</div>";
			$display .= "				</tr>";
			$display .= "				</thead>";
			$display .= "				<tbody>";
			foreach ($this->objTMW->order_details as $detail) :
				$display .= "			<tr>";
				$display .= "				<td>".$detail->DESCRIPTION."</td>";
				$display .= "				<td>".$detail->PIECES."</td>";
				$display .= "				<td>".$detail->PIECES_UNITS."</td>";
				$display .= "				<td>".$detail->WEIGHT."</td>";
				$display .= "				<td>".$detail->WEIGHT_UNITS."</td>";
				$display .= "			</tr>";
			endforeach;
			$display .= "				</tbody>";
			$display .= "				</table>";
			$display .= "				</div>"; // table-responsive
			$display .= "			</div>"; // /details
			
			
			
			$display .= "			<div role='tabpanel' class='tab-pane' id='notes'>";
			$display .= "				<div class='table-responsive'>";
			$display .= "				<table class='table notes'>";
			$display .= "				<thead>";
			$display .= "				<tr>";
			$display .= "					<th>Notes</div>";
			$display .= "				</tr>";
			$display .= "				</thead>";
			$display .= "				<tbody>";
			foreach ($this->objTMW->notes as $note) :
				$display .= "			<tr>";
				$display .= "				<td>".$note->noteExpanded."</td>";
				$display .= "			</tr>";
			endforeach;
			$display .= "				</tbody>";
			$display .= "				</table>";
			$display .= "				</div>"; // table-responsive
			$display .= "			</div>"; // /details
			
			
			
			
			$display .= "		</div>"; //Tab Content;
			$display .= "	</div>";
			$display .= "</div>"; 
			
			
			// Modal popups for editing
			
			if ($alloweditDeliveryPickup) :
				$display .= "<div class='modal fade' id='editDelivery' tabindex='-1' role='dialog' aria-labelledby='editDeliveryLabel'>";
				$display .= "	<div class='modal-dialog' role='document'>";
				$display .= "		<div class='modal-content'>";
				$display .= "			<div class='modal-header'>";
				$display .= "				<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
				$display .= "				<h3 class='modal-title' id='editDeliveryLabel'>Change Delivery</h3>";
				$display .= "			</div>";
				$display .= "			<form>";
				$display .= "			<div class='modal-body'>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='apptFrom'>From</label>";
				$display .= "					<input type='text' class='form-control' id='apptFrom' placeholder='From' value='".date('m/d/Y g:i A', strtotime($this->objTMW->order->DELIVER_BY) )."'>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='apptTo'>To</label>";
				$display .= "					<input type='text' class='form-control' id='apptTo' placeholder='To' value='".date('m/d/Y g:i A', strtotime($this->objTMW->order->DELIVER_BY_END) )."'>";
				$display .= "				</div>";
			
				$checked = ($this->objTMW->order->DELIVERY_APPT_MADE == "False") ? "" : "checked";
			
				$display .= "				<div class='form-group'>";
				$display .= "					<div class='checkbox cb-container'>";
				$display .= "						<label>";
				$display .= "				    	<input type='checkbox' value='' ".$checked.">";
				$display .= "						<span class='checkmark'></span>";				
				$display .= "				    	Appointment Made";
				$display .= "						</label>";
				$display .= "					</div>";
				$display .= "				</div>";
				$display .= "			</div>";
				$display .= "			<div class='modal-footer'>";
				$display .= "				<button type='button' class='btn' data-dismiss='modal'>Cancel</button>";
				$display .= "				<button type='submit' class='btn'>Save Changes</button>";
				$display .= "			</div>";
				$display .= "			</form>";
				$display .= "		</div>";
				$display .= "	</div>";
				$display .= "</div>";
			
				$display .= "<div class='modal fade' id='editPickup' tabindex='-1' role='dialog' aria-labelledby='editPickupLabel'>";
				$display .= "	<div class='modal-dialog' role='document'>";
				$display .= "		<div class='modal-content'>";
				$display .= "			<div class='modal-header'>";
				$display .= "				<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
				$display .= "				<h3 class='modal-title' id='editDeliveryLabel'>Change Pickup</h3>";
				$display .= "			</div>";
				$display .= "			<form>";
				$display .= "			<div class='modal-body'>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='apptFrom'>From</label>";
				$display .= "					<input type='text' class='form-control' id='apptFrom' placeholder='From' value='".date('m/d/Y g:i A', strtotime($this->objTMW->order->PICK_UP_BY) )."'>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='apptTo'>To</label>";
				$display .= "					<input type='text' class='form-control' id='apptTo' placeholder='To' value='".date('m/d/Y g:i A', strtotime($this->objTMW->order->PICK_UP_BY_END) )."'>";
				$display .= "				</div>";
			
				$checked = ($this->objTMW->order->PICK_UP_APPT_MADE == "False") ? "" : "checked";
			
				$display .= "				<div class='form-group'>";
				$display .= "					<div class='checkbox cb-container'>";
				$display .= "						<label>";
				$display .= "				    	<input type='checkbox' value='' ".$checked.">";
				$display .= "						<span class='checkmark'></span>";				
				$display .= "				    	Appointment Made";
				$display .= "						</label>";
				$display .= "					</div>";
				$display .= "				</div>";
				$display .= "			</div>";
				$display .= "			<div class='modal-footer'>";
				$display .= "				<button type='button' class='btn' data-dismiss='modal'>Cancel</button>";
				$display .= "				<button type='submit' class='btn'>Save Changes</button>";
				$display .= "			</div>";
				$display .= "			</form>";
				$display .= "		</div>";
				$display .= "	</div>";
				$display .= "</div>";
			
			endif;
			
			
			
			
			if ($allowedit) :
				$display .= "<div class='modal fade' id='editConsignee' tabindex='-1' role='dialog' aria-labelledby='editConsigneeLabel'>";
				$display .= "	<div class='modal-dialog' role='document'>";
				$display .= "		<div class='modal-content'>";
				$display .= "			<div class='modal-header'>";
				$display .= "				<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
				$display .= "				<h3 class='modal-title' id='editConsigneeLabel'>Edit Consignee</h3>";
				$display .= "			</div>";
				$display .= "			<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='post' name='consigneeForm' id='consigneeForm'>";
				$display .= "			<div class='modal-body'>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destID'>ID</label>";
				$display .= "					<input type='text' class='form-control' name='destId' id='destId' placeholder='ID' value='".$this->objTMW->order->DESTID."' readonly>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destNAME'>Name</label>";
				$display .= "					<input type='text' class='form-control' name='destName' id='destName' placeholder='Name' value='".$this->objTMW->order->DESTNAME."' readonly>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destADDR1'>Address Line 1</label>";
				$display .= "					<input type='text' class='form-control' name='destAddr1' id='destAddr1' placeholder='Address Line 1' value='".$this->objTMW->order->DESTADDR1."' >";
				$display .= "					<div class='error' id='destAddr1ErrorMessage'></div>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destADDR2'>Address Line 2</label>";
				$display .= "					<input type='text' class='form-control' name='destAddr2' id='destAddr2' placeholder='Address Line 2' value='".$this->objTMW->order->DESTADDR2."' >";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destCITY'>City</label>";
				$display .= "					<input type='text' class='form-control' name='destCity' id='destCity' placeholder='City' value='".$this->objTMW->order->DESTCITY."' >";
				$display .= "					<div class='error' id='destCityErrorMessage'></div>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destPROV'>State</label>";
				$display .= "					<input type='text' class='form-control' name='destState' id='destState' placeholder='State' value='".$this->objTMW->order->DESTPROV."' >";
				$display .= "					<div class='error' id='destProvErrorMessage'></div>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destPC'>Zip Code</label>";
				$display .= "					<input type='text' class='form-control' name='destZip' id='destZip' placeholder='Zip Code' value='".$this->objTMW->order->DESTPC."' >";
				$display .= "					<div class='error' id='destPcErrorMessage'></div>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destPHONE'>Phone</label>";
				$display .= "					<input type='text' class='form-control' name='destPhone' id='destPhone' placeholder='Phone' maxlength='20' value='".$this->objTMW->order->DESTPHONE."' >";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destPHONE'>Phone Ext</label>";
				$display .= "					<input type='text' class='form-control' name='destPhoneExt' id='destPhoneExt' placeholder='Extention' maxlength='5' value='".$this->objTMW->order->DESTPHONEEXT."' >";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destCELL'>Cell</label>";
				$display .= "					<input type='text' class='form-control' name='destCell' id='destCell' placeholder='Cell' maxlength='20' value='".$this->objTMW->order->DESTCELL."' >";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destEMAIL'>Email</label>";
				$display .= "					<input type='text' class='form-control' name='destEmail' id='destEmail' placeholder='Email' value='".$this->objTMW->order->DESTEMAIL."' >";
				$display .= "				</div>";
				$display .= "			</div>";
				$display .= "			<div class='modal-footer'>";
				$display .= "				<button type='button' class='btn' data-dismiss='modal'>Cancel</button>";
				$display .= "				<button type='button' class='btn' id='consigneeSubmitButton' onClick=\"this.disabled = true; validateConsigneeForm();\">Save Changes</button>";
				$display .= "			</div>";
				$display .= "			<input type='hidden' name='option' value='com_nsd_tracking' />";
				$display .= "			<input type='hidden' name='task' value='updateConsigneeFromTracePage' />";
				$display .= "			<input type='hidden' name='trace' value='".$this->objForm->trace_number."' />";
				$display .= "			<input type='hidden' name='detailLineId' value='".$this->objTMW->order->DETAIL_LINE_ID."' />";		
				$display .= "           <input type='hidden' name='histUpdatedByEmail' value='".$this->objForm->histUpdatedByEmail."' />";						$display .= "			</form>";
				$display .= "		</div>";
				$display .= "	</div>";
				$display .= "</div>";
			
			
			
			
			
			
			
			
				$display .= "<div class='modal fade' id='editShipper' tabindex='-1' role='dialog' aria-labelledby='editShipperLabel'>";
				$display .= "	<div class='modal-dialog' role='document'>";
				$display .= "		<div class='modal-content'>";
				$display .= "			<div class='modal-header'>";
				$display .= "				<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
				$display .= "				<h3 class='modal-title' id='editConsigneeLabel'>Edit Shipper</h3>";
				$display .= "			</div>";
				$display .= "			<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='post' name='shipperForm' id='shipperForm'>";
				$display .= "			<div class='modal-body'>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destID'>ID</label>";
				$display .= "					<input type='text' class='form-control' name='origId' id='origId' placeholder='ID' value='".$this->objTMW->order->ORIGID."' readonly>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destNAME'>Name</label>";
				$display .= "					<input type='text' class='form-control' name='origName' id='origName' placeholder='Name' value='".$this->objTMW->order->ORIGNAME."' readonly>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destADDR1'>Address Line 1</label>";
				$display .= "					<input type='text' class='form-control' name='origAddr1' id='origAddr1' placeholder='Address Line 1' value='".$this->objTMW->order->ORIGADDR1."' >";
				$display .= "					<div class='error' id='origAddr1ErrorMessage'></div>";
				$display .= "				</div>";
				
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destADDR2'>Address Line 2</label>";
				$display .= "					<input type='text' class='form-control' name='origAddr2' id='origAddr2' placeholder='Address Line 2' value='".$this->objTMW->order->ORIGADDR2."' >";
				$display .= "				</div>";				
				
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destCITY'>City</label>";
				$display .= "					<input type='text' class='form-control' name='origCity' id='origCity' placeholder='City' value='".$this->objTMW->order->ORIGCITY."' >";
				$display .= "					<div class='error' id='origCityErrorMessage'></div>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destPROV'>State</label>";
				$display .= "					<input type='text' class='form-control' name='origState' id='origState' placeholder='State' value='".$this->objTMW->order->ORIGPROV."' >";
				$display .= "					<div class='error' id='origProvErrorMessage'></div>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destPC'>Zip Code</label>";
				$display .= "					<input type='text' class='form-control' name='origZip' id='origZip' placeholder='Zip Code' value='".$this->objTMW->order->ORIGPC."' >";
				$display .= "					<div class='error' id='origPcErrorMessage'></div>";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destPHONE'>Phone</label>";
				$display .= "					<input type='text' class='form-control' name='origPhone' id='origPhone' maxlength='20' placeholder='Phone' value='".$this->objTMW->order->ORIGPHONE."' >";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='originPHONEExt'>Phone Ext</label>";
				$display .= "					<input type='text' class='form-control' name='origPhoneExt' id='origPhoneExt' maxlength='5' placeholder='Extention' value='".$this->objTMW->order->ORIGPHONEEXT."' >";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destCELL'>Cell</label>";
				$display .= "					<input type='text' class='form-control' name='origCell' id='origCell' maxlength='20' placeholder='Cell' value='".$this->objTMW->order->ORIGCELL."' >";
				$display .= "				</div>";
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='destEMAIL'>Email</label>";
				$display .= "					<input type='text' class='form-control' name='origEmail' id='origEmail' placeholder='Email' value='".$this->objTMW->order->ORIGEMAIL."' >";
				$display .= "				</div>";
				$display .= "			</div>";
				$display .= "			<div class='modal-footer'>";
				$display .= "				<button type='button' class='btn' data-dismiss='modal'>Cancel</button>";
				$display .= "				<button type='button' class='btn' id='shipperSubmitButton' onClick=\"this.disabled = true; validateShipperForm();\">Save Changes</button>";
				$display .= "			</div>";
				$display .= "			<input type='hidden' name='option' value='com_nsd_tracking' />";
				$display .= "			<input type='hidden' name='task' value='updateShipperFromTracePage' />";
				$display .= "			<input type='hidden' name='trace' value='".$this->objForm->trace_number."' />";
				$display .= "			<input type='hidden' name='detailLineId' value='".$this->objTMW->order->DETAIL_LINE_ID."' />";
				$display .= "			<input type='hidden' name='histUpdatedByEmail' value='".$this->objForm->histUpdatedByEmail."' />";						$display .= "			</form>";
				$display .= "		</div>";
				$display .= "	</div>";
				$display .= "</div>";
			
			endif;
			
			
			if ($allowAddEntry) :

				$arrImpactedBy = array();
				$arrImpactedBy[] = "";
				$arrImpactedBy[] = " COV19";
	
				$arrReasonCodes = array();
				$arrReasonCodes[] = "";
				$arrReasonCodes[] = "ADDRSCHNG";
				$arrReasonCodes[] = "CANCELLED";
				$arrReasonCodes[] = "DOCKSWEEP";
				$arrReasonCodes[] = "OS&D";
				$arrReasonCodes[] = "STORAGE";
				$arrReasonCodes[] = "UPGRADE";
				
				$arrCallerIdentifier = array();
				$arrCallerIdentifier[] = "Client";
				$arrCallerIdentifier[] = "Customer";
				$arrCallerIdentifier[] = "Terminal";
			
				$arrCallType = array();
				$arrCallType[] = "Claims";
				$arrCallType[] = "Confirm Status";
				$arrCallType[] = "Delivery Details";
				$arrCallType[] = "DNC List";
				$arrCallType[] = "ETA";
				$arrCallType[] = "Loss";
				$arrCallType[] = "NCNS";
				$arrCallType[] = "Scheduling";
				$arrCallType[] = "Service Failure";
				$arrCallType[] = "Update Info";
				
				$arrTitleOfRep = array();
				$arrTitleOfRep[] = "Customer Service Representative";
				$arrTitleOfRep[] = "Customer Service Lead";
				$arrTitleOfRep[] = "Customer Service Supervisor";
			
			
				$display .= "<div class='modal fade' id='addHistory' tabindex='-1' role='dialog' aria-labelledby='addHistoryLabel'>";
				$display .= "	<div class='modal-dialog' role='document'>";
				$display .= "		<div class='modal-content'>";
				$display .= "			<div class='modal-header'>";
				$display .= "				<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
				$display .= "				<h3 class='modal-title' id='addHistoryLabel'>Add History Entry</h3>";
				$display .= "			</div>";
				$display .= "			<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='post' name='histForm' id='histForm'>";
				$display .= "			<div class='modal-body'>";
			
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='notesCallerIdentifier'>Caller Identifier</label>";
				$display .= "					<select class='form-control' name='notesCallerIdentifier' id='notesCallerIdentifier'>";
				foreach( $arrCallerIdentifier as $identifier )
				{
					$display .= "					<option value='".$identifier."'>".$identifier."</option>";
				}
				$display .= "					</select>";
				$display .= "				</div>";
			
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='notesPersonCalling'>Caller Name</label>";
				$display .= "					<input type='text' class='form-control' name='notesPersonCalling' id='notesPersonCalling' placeholder='Person Calling'>";
				$display .= "					<div class='error' id='CalllerNameErrorMessage'></div>";
				$display .= "				</div>";
			
				if ( trim($this->objTMW->order->USER5) == "" )
				{

					$display .= "				<div class='form-group'>";
					$display .= "					<label for='orderImpactedBy'>Impacted By</label>";
					$display .= "					<select class='form-control' name='orderImpactedBy' id='orderImpactedBy'>";
					foreach( $arrImpactedBy as $impby )
					{
						$selected = ( $impby == $this->objTMW->order->USER5 ) ? "selected" : "" ; 
						
						$display .= "					<option value='".$impby."' ".$selected.">".$impby."</option>";
					}
					$display .= "					</select>";
					$display .= "				</div>";
					
				}
				else
				{
					$display .= "<input type='hidden' name='orderImpactedBy' value='".$this->objTMW->order->USER5."' />";
				}
						
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='histReasonCode'>Reason Code</label>";
				$display .= "					<select class='form-control' name='histReasonCode' id='histReasonCode'>";
				foreach( $arrReasonCodes as $code )
				{
					$display .= "					<option value='".$code."'>".$code."</option>";
				}
				$display .= "					</select>";
				$display .= "				</div>";
				
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='notesPersonConfirmed'>Person Confirmed Change at Terminal</label>";
				$display .= "					<input type='text' class='form-control' name='notesPersonConfirmed' id='notesPersonConfirmed' placeholder='Person Name'>";
				$display .= "				</div>";

				$display .= "				<div class='form-group'>";
				$display .= "					<label for='notesCallType'>Call Type</label>";
				$display .= "					<table width='100%'>";
				$display .= "					<tr>";
				$display .= "					<td>";
				
				$countCT = 1;
				foreach( $arrCallType as $type )
				{
			
					$display .= "				<div class='form-group'>";
					$display .= "					<input type='checkbox' name='notesCallType[]' value='".$type."' class='nct'> ".$type."";
					$display .= "				</div'>";
			
					$display .= ( $countCT == 5 ) ? "</td><td>" : "" ;
					$countCT += 1; 
				}



				$display .= "					</td>";
				$display .= "					</tr>";
				$display .= "					</table>";

				$display .= "					<div class='error' id='CallTypeErrorMessage'></div>";
				$display .= "					<div class='error' id='CallTypeErrorMessage2'></div>";
				$display .= "				</div>";
				
				
				$display .= "				<div class='form-group'>";
				$display .= "					<label for='notesNote'>Notes</label>";
				$display .= "					<input type='text' class='form-control' name='notesNote' id='notesNote' placeholder='Note'>";
				$display .= "					<div class='error' id='NoteErrorMessage'></div>";
				$display .= "				</div>";	

				
				
				$display .= "			</div>";
				$display .= "			<div class='modal-footer'>";
				$display .= "				<button type='button' class='btn' data-dismiss='modal'>Cancel</button>";
				$display .= "				<button type='button' id='histSubmitButton' class='btn' onClick=\"this.disabled = true; validateHistForm();\">Save Changes</button>";

				$display .= "			</div>";
				
				$display .= "<input type='hidden' name='option' value='com_nsd_tracking' />";
				$display .= "<input type='hidden' name='task' value='insertOrderDetailsFromTracePage' />";
				$display .= "<input type='hidden' name='trace' value='".$this->objForm->trace_number."' />";
				$display .= "<input type='hidden' name='detailLineId' value='".$this->objTMW->order->DETAIL_LINE_ID."' />";
				$display .= "<input type='hidden' name='histUpdatedByEmail' value='".$this->objForm->histUpdatedByEmail."' />";
				$display .= "<input type='hidden' name='histUpdatedBy' value='".$this->objForm->histUpdatedBy."' />";
				$display .= "<input type='hidden' name='notesTitleOfRep' value='".$this->objForm->notesTitleOfRep."' />";
				
				
				$display .= "			</form>";
				$display .= "		</div>";
				$display .= "	</div>";
				$display .= "</div>";
				
				
			endif;
			
			
			}
		else
		{
			$display .= $this->objTMW->error_message;
		}

	}

}
else
{
	$display .= $this->objDisplay->error_message;
}


$display .= $showobject ? "<h3>objDisplay</h3><pre>".print_r($this->objDisplay, true)."</pre>" : "";	

			
$display .= $showobject ? "<h3>objTMW</h3><pre>".print_r($this->objTMW, true)."</pre>" : "";	
$display .= $showobject ? "<h3>objListTMW</h3><pre>".print_r($this->objListTMW, true)."</pre>" : "";	



	$display .= "<script type=\"text/javascript\">";


	$display .=  " function validateConsigneeForm() { ";
		$display .=  " var errCount = 0; ";

		$display .=  " var varAdd1 = document.forms['consigneeForm']['destAddr1'].value; ";

		$display .=  " var destAddr1ErrorMessage = document.getElementById('destAddr1ErrorMessage'); ";
		$display .=  " var varCity = document.forms['consigneeForm']['destCity'].value; ";
		$display .=  " var destCityErrorMessage = document.getElementById('destCityErrorMessage'); ";
		$display .=  " var varProv = document.forms['consigneeForm']['destState'].value; ";
		$display .=  " var destProvErrorMessage = document.getElementById('destProvErrorMessage'); ";
		$display .=  " var varPC = document.forms['consigneeForm']['destZip'].value; ";
		$display .=  " var destPcErrorMessage = document.getElementById('destPcErrorMessage'); ";

	
		$display .=  " 	if ( varAdd1 == '' ) { ";
		$display .=  " 		errCount += 1; ";	
		$display .=  "		destAddr1ErrorMessage.innerHTML = 'Address must be provided'; ";
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		destAddr1ErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";

	
	
		$display .=  " 	if ( varCity == '') { ";
		$display .=  "		destCityErrorMessage.innerHTML = 'City must be provided'; ";
		$display .=  " 		errCount += 1; ";	
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		destCityErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";
	
		$display .=  " 	if ( varProv == '') { ";
		$display .=  "		destProvErrorMessage.innerHTML = 'State must be provided'; ";
		$display .=  " 		errCount += 1; ";	
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		destProvErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";


		$display .=  " 	if ( varPC == '') { ";
		$display .=  "		destPcErrorMessage.innerHTML = 'Zip Code must be provided'; ";
		$display .=  " 		errCount += 1; ";	
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		destPcErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";

		$display .=  "if ( errCount == 0 ) ";
		$display .=  "{ ";
		$display .=  "	 document.consigneeForm.submit() ";
		$display .=  "} ";
		$display .=  "else ";
		$display .=  "{ ";
		$display .=  "   document.getElementById(\"consigneeSubmitButton\").disabled = false; ";
		$display .=  "   exit(); ";
		$display .=  " ";		
		$display .=  "} ";
	$display .=  " } ";


	$display .=  " function validateShipperForm() { ";
		$display .=  " var errCount = 0; ";

		$display .=  " var varAdd1 = document.forms['shipperForm']['origAddr1'].value; ";
		$display .=  " var origAddr1ErrorMessage = document.getElementById('origAddr1ErrorMessage'); ";
		$display .=  " var varCity = document.forms['shipperForm']['origCity'].value; ";
		$display .=  " var origCityErrorMessage = document.getElementById('origCityErrorMessage'); ";
		$display .=  " var varProv = document.forms['shipperForm']['origState'].value; ";
		$display .=  " var origProvErrorMessage = document.getElementById('origProvErrorMessage'); ";
		$display .=  " var varPC = document.forms['shipperForm']['origZip'].value; ";
		$display .=  " var origPcErrorMessage = document.getElementById('origPcErrorMessage'); ";

	
		$display .=  " 	if ( varAdd1 == '' ) { ";
		$display .=  " 		errCount += 1; ";	
		$display .=  "		origAddr1ErrorMessage.innerHTML = 'Address must be provided'; ";
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		origAddr1ErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";

	
	
		$display .=  " 	if ( varCity == '') { ";
		$display .=  "		origCityErrorMessage.innerHTML = 'City must be provided'; ";
		$display .=  " 		errCount += 1; ";	
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		origCityErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";
	
		$display .=  " 	if ( varProv == '') { ";
		$display .=  "		origProvErrorMessage.innerHTML = 'State must be provided'; ";
		$display .=  " 		errCount += 1; ";	
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		origProvErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";


		$display .=  " 	if ( varPC == '') { ";
		$display .=  "		origPcErrorMessage.innerHTML = 'Zip Code must be provided'; ";
		$display .=  " 		errCount += 1; ";	
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		origPcErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";

		$display .=  "if ( errCount == 0 ) ";
		$display .=  "{ ";
		$display .=  "	 document.shipperForm.submit() ";
		$display .=  "} ";
		$display .=  "else ";
		$display .=  "{ ";
		$display .=  "   document.getElementById(\"shipperSubmitButton\").disabled = false; ";
		$display .=  "   exit(); ";
		$display .=  " ";		
		$display .=  "} ";
	$display .=  " } ";



	$display .=  " function validateHistForm() { ";
		$display .=  " var errCount = 0; ";
		$display .=  " var varNotesPersonCalling = document.forms['histForm']['notesPersonCalling'].value; ";
		$display .=  " var varNotesNote = document.forms['histForm']['notesNote'].value; ";
		$display .=  " var divCalllerNameErrorMessage = document.getElementById('CalllerNameErrorMessage'); ";
		$display .=  " var divNoteErrorMessage = document.getElementById('NoteErrorMessage'); ";
		$display .=  " var divCallTypeErrorMessage = document.getElementById('CallTypeErrorMessage'); ";
		$display .=  " var divCallTypeErrorMessage2 = document.getElementById('CallTypeErrorMessage2'); ";
	
		$display .=  " 	if ( varNotesPersonCalling == '' ) { ";
		$display .=  " 		errCount += 1; ";	
		$display .=  "		divCalllerNameErrorMessage.innerHTML = 'Caller Name must be provided'; ";
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		divCalllerNameErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";
	
	
		$display .=  " 	if ( varNotesNote == '') { ";
		$display .=  "      var divNoteErrorMessage = document.getElementById('NoteErrorMessage'); ";
		$display .=  "		divNoteErrorMessage.innerHTML = 'Note must be provided'; ";
		$display .=  " 		errCount += 1; ";	
		$display .=  " 	} ";
		$display .=  " 	else ";
		$display .=  " 	{ ";
		$display .=  "		divNoteErrorMessage.innerHTML = ''; ";	
		$display .=  " 	} ";
	
		
		$display .=  " var checked = false; ";
		$display .=  " var countChecked = 0; ";
		
		$display .=  " var elements = document.getElementsByName(\"notesCallType[]\"); ";
	
		$display .=  " for(var i=0; i < elements.length; i++){ ";
		$display .=  "	if(elements[i].checked) { ";
		$display .=  "		checked = true; ";
		$display .=  "		countChecked += 1; ";
		$display .=  "	} ";
		$display .=  " } ";
		
		$display .=  " if (!checked) { ";
		$display .=  "	  divCallTypeErrorMessage.innerHTML = 'Select at least one Call Type'; ";
		$display .=  " 	  errCount += 1; ";
		$display .=  " } ";
		$display .=  " else ";
		$display .=  " { ";
		$display .=  "    divCallTypeErrorMessage.innerHTML = ''; ";	
		$display .=  " } ";


		$display .=  " if (countChecked > 3) { ";
		$display .=  "	  divCallTypeErrorMessage2.innerHTML = 'You may select up to 3 Call Types'; ";
		$display .=  " 	  errCount += 1; ";
		$display .=  " } ";
		$display .=  " else ";
		$display .=  " { ";
		$display .=  "    divCallTypeErrorMessage2.innerHTML = ''; ";	
		$display .=  " } ";
	
	
		$display .=  "if ( errCount == 0 ) ";
		$display .=  "{ ";
		$display .=  "	 document.histForm.submit() ";
		$display .=  "} ";
		$display .=  "else ";
		$display .=  "{ ";
		$display .=  "   document.getElementById(\"histSubmitButton\").disabled = false; ";
		$display .=  "   exit(); ";
		$display .=  " ";		
		$display .=  "} ";
	$display .=  " } ";
	
	$display .= "</script>";


echo  $display;


?>



