<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Nsd_trackingViewTrack extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');
		#$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_nsd_tracking');



		$controllerName = "Nsd_trackingViewTrack";
		$functionName = "view";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		$flag_run = JRequest::getVar('flag_run', "1");
		$trace_number_old = JRequest::getVar('trace_number', '');
		$trace_number = JRequest::getVar('t', '');
		$trace_zipcode = JRequest::getVar('z', '');
		

		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	


		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		$stamp_start_micro = microtime(true);



		#session
		$objForm = MetalakeHelperCore::getSessObj( "objSessTracking" );
		
		$jinput = JFactory::getApplication()->input;
		
		$trace_number_old = trim($jinput->get('trace_number', '', 'STRING'));
		$trace_number = trim($jinput->get('t', '', 'STRING'));
		$trace_zipcode = trim($jinput->get('z', '', 'STRING'));

		$objForm->trace_number = ( $trace_number != "" ) ? $trace_number : $trace_number_old ;
		$objForm->trace_zipcode = $trace_zipcode;
		$objForm->flag_api = "0";
		$objForm->flag_trace_page = "0";
		$objForm->error_code_test = "";
		
		$this->assignRef('objForm', $objForm); 


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objDisplay = new stdClass();
		$objDisplay->flag_run = $flag_run;

		if ( $flag_run == "1" )
		{


			if ( trim($objForm->trace_number) == "" )
			{

				$objDisplay->error = 1;
				$objDisplay->error_code = 33;
				$objDisplay->error_message = "<h2>No Tracking Number</h2>No tracking number was entered. Please enter your tracking number.";

				
			}
			elseif ( trim($objForm->trace_zipcode) == "" )
			{

				$objDisplay->error = 1;
				$objDisplay->error_code = 33;
				$objDisplay->error_message = "<h2>No Zip Code</h2>No zip code was entered. Please enter the zip code.";

				
			}
			else
			{

					
					#assign objRockhopper type property to empty
					$objRockhopper =  new stdClass();
					$objRockhopper->type = "";
					$this->assignRef('objRockhopper', $objRockhopper); 
					
					
					#truckmate
					$objDisplay->dataOrigin = "truckmate";
	

					# get order information from Truckmate					
					$objTMW = Nsd_truckmateController::getTruckmateData( $objForm );
					$this->assignRef('objTMW', $objTMW);
					
			
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			

					# get tracking matrix information based on form and objTMW
					$objTrackingMatrix = Nsd_trackingController::getTrackingMatrix( $objForm, $objTMW );
			
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r objTrackingMatrix: ".print_r($objTrackingMatrix, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			



					#if there is an error from truckmate getting the order or the tracking matrix, display an error to the user	
					if ( $objTMW->error == "0" && !is_object($objTrackingMatrix) )
					{

						$objDisplay->error = "1";
						$objDisplay->error_code = 999;
						$objDisplay->error_message = "<h2>Sorry</h2>We are unable to display tracking information at this time.";			
						
					}
					else
					{
						$objDisplay->error = $objTMW->error;
						$objDisplay->error_message = $objTMW->error_message;			
					}



	
					# test the zip code from the order to the zip code from t
					$challenge_zipcode = ( $objTMW->order->SITE_ID == "SITE5"  ) ?  $objTMW->order->ORIGPC : $objTMW->order->DESTPC;


					if ( !$objTMW->error && (trim($objForm->trace_zipcode) != $challenge_zipcode) )
					{
						$objDisplay->error = 1;
						$objDisplay->error_code = 33;
						$objDisplay->error_message = "<h2>Incorrect Zip Code</h2>Please enter the correct zip code.";
					}

				
					$objDisplay->detail_line_id = $objTMW->order->DETAIL_LINE_ID;
				
					$objDisplay->delivery_type = $objTrackingMatrix->delivery_type;
			
					$objDisplay->delivery_type_description = $objTrackingMatrix->delivery_type_description ;
			
					$objDisplay->order_type = $objTrackingMatrix->order_type;
			
					$objDisplay->tracking_number = $objTMW->order->BILL_NUMBER;
			
					$objDisplay->current_state = $objTrackingMatrix->current_state;
					
					$objDisplay->current_state_exception = "0";
					
					$objDisplay->bar_state = $objTrackingMatrix->bar_state;
					
					$objDisplay->bar_color = $objTrackingMatrix->bar_color;
			
						$objInput = new stdClass();
						$objInput->delivery_type = $objTrackingMatrix->delivery_type;
						$objInput->order_type = $objTrackingMatrix->order_type;
			
					$objListBarMatrix = Nsd_trackingController::getBarMatrixInfo( $objInput );
			
					$objDisplay->bar_matrix = $objListBarMatrix;
			
					// !flag_po_number | po_number		
					
					if ( $objTrackingMatrix->flag_po_number )
					{
						$objDisplay->po_number = trim($objTMW->order->TRACE_TYPE_P);
					}
					else
					{
						$objDisplay->po_number = "";
					}
			
			
			
					// !flag_ref_1_number | ref_1_number			
					
					if ( $objTrackingMatrix->flag_ref_1_number )
					{
						$objDisplay->ref_1_number = trim($objTMW->order->TRACE_TYPE_Q);
					}
					else
					{
						$objDisplay->ref_1_number = "";
					}		
			
			
			
					// !ref_2_number	
			
					$objDisplay->ref_2_number = ( $objTrackingMatrix->flag_ref_2_number ) ? trim($objTMW->order->TRACE_TYPE_W) : "" ;
			
			
			
			
					// !flag_service_level | service_level	
			
					if ( $objTrackingMatrix->flag_service_level == "TRUE"  )
					{
						
						$objServiceLevel = Nsd_trackingController::getServiceLevelInformation( $objTMW );
						
						$objDisplay->service_level = $objServiceLevel->description;
						
						$objDisplay->service_level_description_long = $objServiceLevel->arrDescriptionLong;
						
					}
					else
					{
						$objDisplay->service_level = $objDisplay->service_level_description_long = "";
						
					}
			
			
					// !flag_origin | origin	
			
					if ( $objTrackingMatrix->flag_origin )
					{
						switch( $objTrackingMatrix->origin_location )
						{
							case "ORIGCITY ORIGPROV":
							$objDisplay->origin = trim($objTMW->order->ORIGCITY).", ".trim($objTMW->order->ORIGPROV);
								break;
			
			
							case "ORIGCITY ORIGPROV ORIGPC":
							$objDisplay->origin = trim($objTMW->order->ORIGCITY).", ".trim($objTMW->order->ORIGPROV)." ".trim($objTMW->order->ORIGPC);
								break;
							
							default:
							$objDisplay->origin = "";
						}
					}
			
			
			
					// !flag_destination | destination	
			
					if ( $objTrackingMatrix->flag_destination )
					{
						switch( $objTrackingMatrix->destination_location )
						{
							case "DESTCITY DESTROV":
							$objDisplay->destination = trim($objTMW->order->DESTCITY).", ".trim($objTMW->order->DESTPROV);
								break;
			
			
							case "DESTCITY DESTROV DESTPC":
							$objDisplay->destination = trim($objTMW->order->DESTCITY).", ".trim($objTMW->order->DESTPROV)." ".trim($objTMW->order->DESTPC);
								break;
							
							default:
							$objDisplay->destination = "";
						}
					}
			
			
			
					// !flag_estimated_delivery_date | estimated_delivery_date	
			
					if ( $objTrackingMatrix->flag_estimated_delivery_date == "TRUE"  )
					{
			
						switch( $objTrackingMatrix->estimated_delivery )
						{
							case "DELIVER_BY":
							
								$objInputDate = new stdClass();
								$objInputDate->date = date('Y-m-d', strtotime($objTMW->order->DELIVER_BY) );
								$arrDate = Nsd_trackingController::getTwoDaysAway($objInputDate);
								
								$delivered_by = ( $objTMW->order->DELIVER_BY != "" ) ? date('F j, Y', strtotime($objTMW->order->DELIVER_BY) )." - ".date('F j, Y', strtotime(end($arrDate)) ) : "";
								
								
								$objDisplay->estimated_delivery_date = $delivered_by;
								break;
			
			
							case "Not Yet Known":
							$objDisplay->estimated_delivery_date = "Not Yet Known";
								break;
							
							default:
							$objDisplay->estimated_delivery_date = "";
						}
						
					}
					else
					{
						$objDisplay->estimated_delivery_date = "";
					}	
			
			
			
					// !flag_scheduled_delivery_date | scheduled_delivery_date	
			
					if ( $objTrackingMatrix->flag_scheduled_delivery_date == "TRUE" )
					{
			
						#If DELIVER_APPT_MADE = 'True' then value = "Not Yet Scheduled"
						#If DELIVER_APPT_MADE = 'False' then vlaue = "DELIVER_BY and DELIVER_BY_END)
			
						if( $objTMW->order->DELIVERY_APPT_MADE == "False" )
						{
							$objDisplay->scheduled_delivery_date = "Not Yet Scheduled";
						}
						else
						{
							$delivered_by = ( $objTMW->order->DELIVER_BY != "" ) ? date('F j, Y g:i A', strtotime($objTMW->order->DELIVER_BY) ) : "";
							$delivered_by_end = ( $objTMW->order->DELIVER_BY_END != "" ) ? date('g:i A', strtotime($objTMW->order->DELIVER_BY_END) ) : "";				
							
							
							$objDisplay->scheduled_delivery_date = $delivered_by." - ".$delivered_by_end;
							
							$objDisplay->estimated_delivery_date = "";
						}
						
					}
					else
					{
						$objDisplay->scheduled_delivery_date = "";
					}
			
			
					// !flag_scheduled_pickup_date | scheduled_pickup_date	
			
					if ( $objTrackingMatrix->flag_scheduled_pickup_date == "TRUE"  )
					{
			
						#If DELIVER_APPT_MADE = 'True' then value = "Not Yet Scheduled"  
						#If DELIVER_APPT_MADE = 'False' then vlaue = "DELIVER_BY and DELIVER_BY_END)
			
			
						if( $objTMW->order->PICK_UP_APPT_MADE == "False" )
						{
							$objDisplay->scheduled_pickup_date = "Not Yet Scheduled";
						}
						else
						{
							$delivered_by = ( $objTMW->order->DELIVER_BY != "" ) ? date('F j, Y g:i A', strtotime($objTMW->order->PICK_UP_BY) ) : "";
							$delivered_by_end = ( $objTMW->order->DELIVER_BY_END != "" ) ? date('g:i A', strtotime($objTMW->order->PICK_UP_BY_END) ) : "";
							
							$objDisplay->scheduled_pickup_date = $delivered_by." - ".$delivered_by_end;
						}
						
					}
					else
					{
						$objDisplay->scheduled_pickup_date = "";
					}
			
			
					// !flag_actual_pickup_date | actual_pickup_date	
			
					if ( $objTrackingMatrix->flag_actual_pickup_date == "TRUE"  )
					{
			
						
						$objOrderHistory = Nsd_trackingController::getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status = "PICKEDUP" );
						
						$arrCStatuses = array();
						$arrCStatuses[] = "APPRVD";
						$arrCStatuses[] = "ARRDLVTERM";
						$arrCStatuses[] = "ARRSHIP";
						$arrCStatuses[] = "ARRTERM";
						$arrCStatuses[] = "BILLED";
						$arrCStatuses[] = "COMPLETED";
						$arrCStatuses[] = "DELIVERED";
						$arrCStatuses[] = "DEPDLVTERM";
						$arrCStatuses[] = "DEPSHIP";
						$arrCStatuses[] = "DEPTERM";
						$arrCStatuses[] = "DISPOSED";
						$arrCStatuses[] = "INTRANSIT";
						$arrCStatuses[] = "LHAPPTCONS";
						$arrCStatuses[] = "LHAPPTSHIP";
						$arrCStatuses[] = "LHETA";
						$arrCStatuses[] = "LTLAVAIL";
						$arrCStatuses[] = "PICKEDUP";
						$arrCStatuses[] = "PRINTED";
						$arrCStatuses[] = "SHPTACK";
						$arrCStatuses[] = "ST PRINTED";
						$arrCStatuses[] = "TENDER";
						$arrCStatuses[] = "UNAPPRVD";
		
						
						$objDisplay->actual_pickup_date = ( $objOrderHistory->OS_CHANGED != "" && ( in_array( trim($objTMW->order->CURRENT_STATUS), $arrCStatuses ) ) ) ? date('F j, Y g:i A', strtotime($objOrderHistory->OS_CHANGED)) : "";
		
						
						
						
						$objDisplay->scheduled_pickup_date = "";				
						
					}
					else
					{
						$objDisplay->actual_pickup_date = "";
					}
			
			
					// !flag_actual_delivery_date | actual_delivery_date	
			
					if ( $objTrackingMatrix->flag_actual_delivery_date == "TRUE"  )
					{
						
						$objOrderHistory = Nsd_trackingController::getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status = "DELIVERED" );
			
			
						$objDisplay->actual_delivery_date = ( $objOrderHistory->OS_CHANGED != "" && 
						( trim($objTMW->order->CURRENT_STATUS) == "DELIVERED" || 
						trim($objTMW->order->CURRENT_STATUS) == "COMPLETED" || 
						trim($objTMW->order->CURRENT_STATUS) == "APPRVD" || 
						trim($objTMW->order->CURRENT_STATUS) == "UNAPPRVD" || 
						trim($objTMW->order->CURRENT_STATUS) == "BILLED" || 
						trim($objTMW->order->CURRENT_STATUS) == "PRINTED" || 
						trim($objTMW->order->CURRENT_STATUS) == "ST PRINTED") ) ? date('F j, Y g:i A', strtotime($objOrderHistory->OS_CHANGED)) : "";
					}
					else
					{
						$objDisplay->actual_delivery_date = "";
					}
			
			
			
					// !flag_pod_information | pod_information
			
					if ( $objTrackingMatrix->flag_pod_information == "TRUE"  )
					{
			
			
						if ( $objTMW->order->SITE_ID == "SITE6" )
						{
							
							$objOrderHistory = Nsd_trackingController::getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status = "DELIVERED" );
							
							$objDisplay->pod_information = ( $objOrderHistory->OS_STAT_COMMENT != "" && 
							( trim($objTMW->order->CURRENT_STATUS) == "DELIVERED" || 
							trim($objTMW->order->CURRENT_STATUS) == "COMPLETED" || 
							trim($objTMW->order->CURRENT_STATUS) == "APPRVD" || 
							trim($objTMW->order->CURRENT_STATUS) == "UNAPPRVD" || 
							trim($objTMW->order->CURRENT_STATUS) == "BILLED" || 
							trim($objTMW->order->CURRENT_STATUS) == "PRINTED" || 
							trim($objTMW->order->CURRENT_STATUS) == "ST PRINTED" ) ) ? "Signature on file" : "";
						}
						else
						{
			
							$objOrderHistory = Nsd_trackingController::getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status = "PICKEDUP" );
		
							$arrCStatuses = array();
							$arrCStatuses[] = "APPRVD";
							$arrCStatuses[] = "ARRDLVTERM";
							$arrCStatuses[] = "ARRSHIP";
							$arrCStatuses[] = "ARRTERM";
							$arrCStatuses[] = "BILLED";
							$arrCStatuses[] = "COMPLETED";
							$arrCStatuses[] = "DELIVERED";
							$arrCStatuses[] = "DEPDLVTERM";
							$arrCStatuses[] = "DEPSHIP";
							$arrCStatuses[] = "DEPTERM";
							$arrCStatuses[] = "DISPOSED";
							$arrCStatuses[] = "INTRANSIT";
							$arrCStatuses[] = "LHAPPTCONS";
							$arrCStatuses[] = "LHAPPTSHIP";
							$arrCStatuses[] = "LHETA";
							$arrCStatuses[] = "LTLAVAIL";
							$arrCStatuses[] = "PICKEDUP";
							$arrCStatuses[] = "PRINTED";
							$arrCStatuses[] = "SHPTACK";
							$arrCStatuses[] = "ST PRINTED";
							$arrCStatuses[] = "TENDER";
							$arrCStatuses[] = "UNAPPRVD";
		
						
							$objDisplay->pod_information = ( $objOrderHistory->OS_STAT_COMMENT != "" && ( in_array( trim($objTMW->order->CURRENT_STATUS), $arrCStatuses ) ) ) ? "Signature on file" : "";
		
							
					#$objOrderHistory->OS_STAT_COMMENT		
							
						}
			
						
					}
					else
					{
						$objDisplay->pod_information = "";
					}
			
			
					// !order history
					
					$arrOrderHistory = Nsd_trackingController::getArrOrderHistory( $objForm, $objTMW, $objTrackingMatrix );
			
					$objDisplay->order_history = $arrOrderHistory;
		
		
					#update the color in the bar_matrix
		
		
					$isFirstInArray = "1";
					
					$flag_milestone_status = "0";
					
					
					$flagPrintedExpiredTest = "0";
					$flagPrintedExpired = "0";
					
					foreach( $arrOrderHistory as $history )
					{
		
						if ( $isFirstInArray == "1" && $history->suppressed == "0")
						{
							$barColor = $history->bar_color;
							
							$objDisplay->current_state = $history->status_description;
							
							if ( $history->status_exception == "1" )
							{
								
								$objDisplay->current_state_exception = "1";
								
							}
							
							$isFirstInArray = "0";
							
						}	
						
						
						#is this status a milestone?
						#Y get barstate and stop
						#N continue until you get milestone status

						if ( $flag_milestone_status == "0" )
						{

							$query = "SELECT * from htc_nsd_tracking_matrix where state='1' and delivery_type ='".$objDisplay->delivery_type."' and  order_type = '".$objDisplay->order_type."' and order_status = '".$history->OS_STATUS_CODE."' ";
					        $db->setQuery($query);
					        $objMatrixMilestone = $db->loadObject();
					        
					        if ( $objMatrixMilestone->flag_milestone == "1" )
					        {
						        
						        $barState = $history->bar_state;	
						        
						        $objDisplay->current_state = $history->status_description;					        
						        
						        $flag_milestone_status = "1";
					        }								

							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | BEACON88";
							$logMessage .= "\n\r query: ".print_r($query, true);
							$logMessage .= "\n\r objMatrixMilestone: ".print_r($objMatrixMilestone, true);		
							$logMessage .= "\n\r barState: ".print_r($barState, true);
							$logMessage .= "\n\r flag_milestone_status: ".print_r($flag_milestone_status, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
							
						}
						
						
						if ( $flag_milestone_status == "0" )
						{
							$history->suppressed = "1";
						}

						if ( $flagPrintedExpiredTest == "0" &&  $objTMW->order->CURRENT_STATUS == "PRINTED" )
						{
							if ( strtotime($history->OS_INS_DATE) < strtotime('-30 days') )
							{
								$flagPrintedExpired = "1";
								$flagPrintedExpiredTest = "1";
								
							}
						}
					}


					if ( $flagPrintedExpired == "1" )
					{
						$objDisplay->error = "1";
						$objDisplay->error_code = 998;
						$objDisplay->error_message = "<h2>Sorry</h2>Tracking information for this order is no longer available. Should you need further assistance, please contact our Customer Service Team.";	
					}


					
		
					$objDisplay->bar_matrix = array_reverse($objDisplay->bar_matrix, true);
		
					$foundBarState = "0";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r barColor before: ".print_r($barColor, true);
					$logMessage .= "\n\r barState before: ".print_r($barState, true);		
					$logMessage .= "\n\r objDisplay->bar_matrix before: ".print_r($objDisplay->bar_matrix, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					foreach( $objDisplay->bar_matrix as $objBar  )
					{
		
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
						$logMessage .= "\n\r objBar before: ".print_r($objBar, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
		
						$objBar->color = ( $foundBarState == "1" ) ? "green" : "";
						$objBar->flag_current = 0;
						
						if ( $objBar->bar_state == $barState && $foundBarState == "0")
						{
							$objBar->color = $barColor;
							
							$foundBarState = "1";
							$objBar->flag_current = 1;
						}
		
						if ( $objDisplay->bar_state == "FALSE" )
						{
							$objBar->color = "";
							$foundBarState = "0";
							$objBar->flag_current = 0;
						}
		
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
						$logMessage .= "\n\r objBar after: ".print_r($objBar, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					}
		
		
					$objDisplay->bar_matrix = array_reverse($objDisplay->bar_matrix, true);
		
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r objDisplay->bar_matrix after: ".print_r($objDisplay->bar_matrix, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
		
					#log the activity
		
					if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					    $remote_address = $_SERVER['HTTP_CLIENT_IP'];
					} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					    $remote_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
					} else {
					    $remote_address = $_SERVER['REMOTE_ADDR'];
					}
		
					$objActivityLog = new stdClass();
					$objActivityLog->remote_address =  $remote_address;
					$objActivityLog->trace = $trace_number;
					$objActivityLog->trace_type = "Bill Number";
					$objActivityLog->delivery_type = $objDisplay->delivery_type_description;
					$objActivityLog->order_type = $objDisplay->order_type;
					$objActivityLog->json_return = json_encode($objDisplay);
					$objActivityLog->tm4web_usr = "";
					$objActivityLog->flag_api = $objForm->flag_api;
					$objActivityLog->return_format = "web";
					
					Nsd_trackingController::logActivity( $objActivityLog );
		
				
			}	
		
		}

		$this->assignRef('objDisplay', $objDisplay);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_nsd_tracking');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_NSD_TRACKING_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
