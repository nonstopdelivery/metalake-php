<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Nsd_trackingViewTrack extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');
		#$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_nsd_tracking');



		$controllerName = "Nsd_trackingViewTrack";
		$functionName = "view";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		$flag_run = JRequest::getVar('flag_run', 1);
		$trace_number = JRequest::getVar('trace', '');

		$tm4web_usr = JRequest::getVar('user', '');
		$tm4web_pwd = JRequest::getVar('pswd', '');

		
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	


		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		$stamp_start_micro = microtime(true);



		#session
		$objForm = MetalakeHelperCore::getSessObj( "objSessTracking" );
		
		$jinput = JFactory::getApplication()->input;
		$trace_number = $jinput->get('trace_number', '', 'STRING');
		
		$objForm->trace_number = trim($trace_number);
		$objForm->tmw4web_user = $tm4web_usr;
		$objForm->tmw4web_password = $tm4web_pwd;
		$objForm->flag_api = 1;		
		$objForm->flag_trace_page = "0";
		
		$this->assignRef('objForm', $objForm); 


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objDisplay = new stdClass();

		if ( $flag_run == "1" )
		{

				#TMW

				$objTMW = Nsd_truckmateController::getTruckmateData( $objForm );
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		
				$objTrackingMatrix = Nsd_trackingController::getTrackingMatrix( $objForm, $objTMW );
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r objTrackingMatrix: ".print_r($objTrackingMatrix, true);
				$logMessage .= "\n\r is object objTrackingMatrix: ".print_r(is_object($objTrackingMatrix), true);
				
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
				if (  $objTMW->error  == "1" )
				{
					$objDisplay->error = $objTMW->error;
					
					$objDisplay->error_code = $objTMW->error_code;
					
					$objDisplay->error_message = $objTMW->error_message;			
				}
				else
				{
					
					$objDisplay = Nsd_trackingController::getAPIDisplayDataForOutput( $objDisplay, $objForm, $objTMW, $objTrackingMatrix );
		
				} 
	
				#log the activity
	
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				    $remote_address = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				    $remote_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
				    $remote_address = $_SERVER['REMOTE_ADDR'];
				}
	
				$objActivityLog = new stdClass();
				$objActivityLog->remote_address =  $remote_address;
				$objActivityLog->trace = $trace_number;
				$objActivityLog->trace_type = "Bill Number";
				$objActivityLog->delivery_type = $objDisplay->delivery_type_description;
				$objActivityLog->order_type = $objDisplay->order_type;
				$objActivityLog->json_return = json_encode($objDisplay);
				$objActivityLog->tm4web_usr = $tm4web_usr;
				$objActivityLog->flag_api = $objForm->flag_api;
				$objActivityLog->return_format = "xml";
				
				Nsd_trackingController::logActivity( $objActivityLog );
	
				$this->assignRef('objDisplay', $objDisplay);
				
		


				# RETURN OVERRIDE IF SITE IS OFFLINE
				require_once ('configuration.php' ); // since this file n configuration file both are at the same location

				$var_cls = new JConfig(); // object of the class

				// variables that you want to use
				$offline = $var_cls->offline;
				$sitename = $var_cls->sitename;


                $logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
                $logMessage .= "\n\r offline: ".print_r($offline, true);
                $logMessage .= "\n\r sitename: ".print_r($sitename, true);
                $logMessage .= "\n\r var_cls: ".print_r($var_cls, true);

                if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

                if ( $offline == "1" )
                {

                        http_response_code(400);

                        $objDisplay = new stdClass();
                        $objDisplay->error_state = "1";
                        $objDisplay->error_code = "9999";
                        $objDisplay->error_message = "Temporarily Offline for Maintenance. We are performing scheduled maintenance. We should be back online shortly.";

                }


				#OUTPUT FOR XML
				$objDisplay = json_decode(json_encode($objDisplay));
		
				#https://stackoverflow.com/questions/137021/php-object-as-xml-document
				$replacements = array();		
				
				$apiReturn = XMLSerializer::generateValidXmlFromMixiedObj($objDisplay, "", $replacements, $add_header="false");				
				
				


				
		}
		
		$this->assignRef('apiReturn', $apiReturn);		
		




		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_nsd_tracking');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}


		$this->_prepareDocument();

		parent::display($tpl="xml");





	}


	public function array2xml($data, $root = null){
	    $xml = new SimpleXMLElement($root ? '<' . $root . '/>' : '<root/>');
	    array_walk_recursive($data, function($value, $key)use($xml){
	        $xml->addChild($key, $value);
	    });
	    return $xml->asXML();
	}



	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_NSD_TRACKING_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
