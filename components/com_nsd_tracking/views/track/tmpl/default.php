<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$showobject = false;
#$showobject = true;

$doortodoor = false;
$lastmile = false;
$delivery = false;
$return = false;
$trackingbarclass = "";

switch ($this->objDisplay->delivery_type) {
	case "DD":
		$doortodoor = true;
		break;
	case "LM":
		$lastmile = true;
		break;
}

switch ($this->objDisplay->order_type) {
	case "Delivery":
		$delivery = true;
		break;
	case "Return":
		$return = true;
		break;
}
if ($this->objRockhopper->type == 'return') {
	$return = true;
	$trackingbarclass = " tracking-bar-return";
}


?>
<script type="text/javascript">

	jQuery(function() { 
	   
		 jQuery("#formNotify").submit(function(e) {    
		    

			e.preventDefault();

			var detailLineID = document.getElementById('detailLineID').value;
			var notifyEmail = document.getElementById('notifyEmail').value;
			//var notifyPhone = document.getElementById('notifyPhone').value;


			var msgError = ""

			//if ( notifyEmail.trim() === "" &&  notifyPhone.trim() === "" )
			if ( notifyEmail.trim() === "" )
			{
				
				//var msgError = "Please enter an email address or a phone number."
				var msgError = "Please enter an email address."
				
				
				jQuery(".error-return").html(msgError);
			}
			else
			{


				var url = 'index.php?option=com_nsd_workato&task=updateWorkato';
	
				jQuery.ajax({ 
					type: "POST", 
					url: url,
					data: {
						detailLineID	: detailLineID,
						notifyEmail 	: notifyEmail,
						//notifyPhone		: notifyPhone
						},
						success: function(dataReturn) {
							//alert("SUCCESS:");
							//alert(dataReturn);
							
							var dataResponse = JSON.parse(dataReturn);
	
							if ( dataResponse.error == "0" )
							{
								
								var msgSuccess = "<div class='col-sm-12'>You will now receive updates regarding your delivery. To change this setting, please <a class='purechat-butto-expand'>click here to chat</a> or call us at 800-956-7212 if you need immediate assistance.</div>"
								
								jQuery(".the-return").html(msgSuccess);
								jQuery(".error-return").html(msgError);
								
							}	
							else
							{
								// here, we are showing the note field from the 'error state'.
								
								jQuery(".the-return").html(dataResponse.note);
								jQuery(".error-return").html(msgError);
							}
							
						},
						error:function(dataReturn) {
							//alert("server error occured"); 
							
							var msgServerError = "<div class='col-sm-12'>We're sorry, but we were unable to process your request. Please <a class='purechat-button-expand' >click here to chat</a> or call us at 800-956-7212 if you need immediate assistance.</div>"
								
							jQuery(".the-return").html(msgServerError);
							
	    				}
					});
					return false;
	            
	        }    
	            
	            
	        });
	        

	 }); 

</script>
<?php


$display = "";



$display .= "<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='get' name='adminForm' id='adminForm' class='form-inline track'>";
$display .= "	<div class='row'>";
$display .= "	<div class='form-group col-sm-3'>";
$display .= "		<label for='trace_number'>Tracking Number</label>";
$display .= "		<input type='text' class='form-control' name='t' id='t' value='".$this->objForm->trace_number."' placeholder='Enter Tracking Number' />";
$display .= "	</div>";
$display .= "	<div class='form-group col-sm-3'>";
$display .= "		<label for='trace_zipcode'>Zip Code</label>";
$display .= "		<input type='text' class='form-control' name='z' id='z' value='".$this->objForm->trace_zipcode."' placeholder='Enter Zip Code' />";
$display .= "	</div>";
$display .= "	<div class='form-group col-sm-2'>";
$display .= "		<label for='sendResponse'>&nbsp;</label>";
$display .= "		<input class='form-control btn' type='Submit' id='sendResponse' value='Track' onclick=\"document.adminForm.submit();\" />";
#$display .= "		<input type='hidden' name='trace_number' />";
$display .= "	</div>";
$display .= "	</div>";
$display .= "</form>";

if ( $this->objDisplay->flag_run == "1" ) :

	if ( $this->objDisplay->error == "0" ) :


		if( $this->objDisplay->dataOrigin == "rockhopper" )
		{
			#$display .= "rockhopper";


			$display .= "<div class='details section row'>";
			$display .= "	<div class='col-sm-12'><h2>Details</h2></div>";
			$display .= "	<div class='col-sm-6'>";
			$display .= "		<div class='item'><span>Tracking Number:</span>".$this->objRockhopper->tracking_number."</div>";

			foreach( $this->objRockhopper->details as $key => $value ) {
				$display .= "		<div class='item'><span>".$key.":</span>".$value."</div>";
			}

			$display .= "	</div>";
			$display .= "	<div class='col-sm-6'>";
			$display .= "		<div class='item'><span>Service:</span>".$this->objRockhopper->service."</div>";

			if ( ! empty($this->objRockhopper->service_descriptions) ) {
				$display .= "		<div class='item'><span>Service Description:</span><ul>";
				foreach ( $this->objRockhopper->service_descriptions as $bullet ) {
					$display .= "			<li>" . $bullet . "</li>";
				}
				$display .= "		</ul></div>";
			}

			$display .= "	</div>";
			$display .= "</div>"; // /details


/*
			$display .= '<div class="clearfix tracking-info">';
		
		      $display .= '<h2 class="key-main">Details</h2>';
		      $display .= '<div class="tracking-details">';
		          $display .= '<div class="data-row">';
		              $display .= '<div class="section"><strong>Tracking Number</strong>: <span itemprop="trackingNumber">' . $this->objRockhopper->tracking_number . '</span></div>';
		              foreach( $this->objRockhopper->details as $key => $value ) {
		                  $display .= '<div class="section"><strong>' . $key .'</strong>: '. $value . '</div>';
		              }
		          $display .= '</div>';
		      $display .= '</div>';
		      $display .= '<div class="tracking-service">';
		          $display .= '<div class="data-row">';
		              $display .= '<div class="section"><strong>Service</strong>: <span itemprop="hasDeliveryMethod">' . $this->objRockhopper->service . '</span></div>';
		              if ( ! empty($this->objRockhopper->service_descriptions) ) {
		                  $display .= '<div class="section"><strong>Service Description</strong>: <ul>';
		                      foreach ( $this->objRockhopper->service_descriptions as $bullet ) {
		                          $display .= '<li>' . $bullet . '</li>';
		                      }
		                  $display .= '</ul></div>';
		              }
		          $display .= '</div>';
		      $display .= '</div>';
		
		  $display .= '</div>';  
*/


			$display .= "<div class='status section row'>";
			$display .= "	<div class='col-sm-6'>";

			if ( $this->objRockhopper->type !== 'return' ) {
			$display .= "		<div class='item'><span>Origin:</span>".$this->objRockhopper->stops_1_city_clean."</div>";
			}

			foreach( $this->objRockhopper->shipping as $key => $value ) {
			$display .= "		<div class='item'><span>".$key.":</span>".$value."</div>";
			}

			$display .= "	</div>";
			$display .= "	<div class='col-sm-6'>";

			if ( $this->objRockhopper->estimated ) {
			$display .= "		<meta itemprop='expectedArrivalFrom' content='" . date(DATE_ISO8601, strtotime($this->objRockhopper->estimated)) . "' />";		
			}
			
			if ( $this->objRockhopper->type !== 'return' ) {
			$display .= "		<div class='item'><span>Destination:</span>".$this->objRockhopper->stops_3_city_clean."</div>";
			}
			foreach( $this->objRockhopper->delivery as $key => $value ) {
			$display .= "		<div class='item'><span>".$key.":</span>".$value."</div>";
			}
			$display .= "	</div>";


/*
		  $display .= '<div class="clearfix tracking-info">';
		
		      $display .= '<div class="tracking-shipping">';
		          $display .= '<div class="data-row">';

		          if ( $this->objRockhopper->type !== 'return' ) {
		              $display .= '<div class="section"><strong>Origin</strong>: '. $this->objRockhopper->stops_1_city_clean . '</div>';
		          }
		          foreach( $this->objRockhopper->shipping as $key => $value ) {
		              $display .= '<div class="section"><strong>' . $key .'</strong>: '. $value . '</div>';
		          }
		          $display .= '</div>';
		      $display .= '</div>';


		      $display .= '<div class="tracking-delivery">';

		          if ( $this->objRockhopper->estimated ) {
		              $display .= '<meta itemprop="expectedArrivalFrom" content="' . date(DATE_ISO8601, strtotime($this->objRockhopper->estimated)) . '" />';

		          }
		          $display .= '<div class="data-row">';

		          if ( $this->objRockhopper->type !== 'return' ) {
		              $display .= '<div class="section"><strong>Destination</strong>: '. $this->objRockhopper->stops_3_city_clean . '</div>';
		          }
		          foreach( $this->objRockhopper->delivery as $key => $value ) {
		              $display .= '<div class="section"><strong>' . $key .'</strong>: '. $value . '</div>';
		          }
		          $display .= '</div>';
		      $display .= '</div>';
		
		  $display .= '</div>';
*/



			$display .= "	<div class='col-sm-12 bar-container'>";
			$display .= "		<div class='tracking-bar".$trackingbarclass."'>";
			foreach ($this->objRockhopper->bar as $index => $chevron) :
				$display .= "			<div class='tracking-bar-position ".$chevron['class']."'>";
				$display .= "				<div class='tracking-bar-container'>";
				$display .= "					<div class='tracking-bar-alert'></div>";
				$display .= "					<div class='tracking-bar-chevron'></div>";
				$display .= "				</div>";
				$display .= "				<div class='tracking-bar-label'>".$chevron['label']."</div>";
				$display .= "				<div class='tracking-bar-line left'></div>";
				$display .= "				<div class='tracking-bar-line right'></div>";
				$display .= "			</div>";
			endforeach;
			$display .= "		</div>";
			$display .= "	</div>";


/*
		  $display .= '<div class="tracking-bar ' . 'tracking-bar-' . $this->objRockhopper->type . '">';
		      foreach ( $this->objRockhopper->bar as $index => $chevron ) {
		          $display .= '<div class="tracking-bar-position ' . $chevron['class'] . '">';
		              $display .= '<div class="tracking-bar-container">';
		                  $display .= '<div class="tracking-bar-alert"></div>';
		                  $display .= '<div class="tracking-bar-chevron"></div>';
		              $display .= '</div>';
		              $display .= '<div class="tracking-bar-label">' . $chevron['label'] . '</div>';
		              
		              $display .= $index > 0 ? '<div class="tracking-bar-line tracking-bar-line-left"></div>' : '';
		              $display .= $index < count($this->objRockhopper->bar) - 1 ? '<div class="tracking-bar-line tracking-bar-line-right"></div>' : '';
		          $display .= '</div>';
		      }
		  $display .= '</div>';
*/


			$display .= "	<div class='col-sm-12 text-center current-status ".$this->objRockhopper->status['class']."'><h3>Current Status: ".$this->objRockhopper->status['activity']."</h3></div>";


/*
		  $display .= '<div class="tracking-status ' . $this->objRockhopper->status['class'] . '" itemprop="deliveryStatus">';
		      $display .= 'Current Status: ' . $this->objRockhopper->status['activity'];
		  $display .= '</div>';
*/


		  $display .= "</div>"; // /status


			$display .= "<div class='history section row'>";
			$display .= "	<div class='col-sm-12'>";
			$display .= "		<h2>Transit History</h2>";
			$display .= "		<a class='toggleHistory' data-toggle='collapse' href='#collapseHistory' aria-expanded='false' aria-controls='collapseHistory'> </a>";
			$display .= "	</div>";
			$display .= "	<div class='clearfix'></div>";
	
			$display .= "	<div class='collapse' id='collapseHistory'>";
	
			$display .= "	<div class='col-sm-3 head'>Date/Time</div>";
			$display .= "	<div class='col-sm-7 head'>Activity</div>";
			$display .= "	<div class='col-sm-2 head'>Location</div>";
			
			foreach ($this->objRockhopper->history as $event) :
				if (!$history->suppressed) :
					$display .= "	<div class='history-row'>";
					$display .= "	<div class='col-sm-3'>".$event['date']." ".$event['time']."</div>";
					$statusExceptionClass = $history->status_exception ? " exception" : "";
					$display .= "	<div class='col-sm-7 ".$event['activityClass']."'>".$event['activity']."</div>";  //Need bar color as class to change message color
					$display .= "	<div class='col-sm-2'>".(strlen(trim($event['location'])) ? trim($event['location']) : '-')."</div>";
					$display .= "	<div class='clearfix'></div>";
					$display .= "	</div>";
				endif;
			endforeach;
			
			$display .= "	</div>"; // collapse
			$display .= "</div>"; // /history


		
/*
		  $display .= '<div class="tracking-info tracking-history">';
		      $display .= '<h2 class="key-main">Transit History</h2>';
		      $display .= '<div class="tracking-table">';
		      $display .= '<ul><li>Date</li><li>Time</li><li>Activity</li><li>Location</li></ul>';
		      foreach ( $this->objRockhopper->history as $event ) {
		          $display .= '<ul>';
		          $display .= '<li data-label="Date:">' . $event['date'] . '</li>';
		          $display .= '<li data-label="Time:">' . $event['time'] . '</li>';
		          
		          # Ross - leave this so you can work your stuff.  
		          $display .= '<li data-label="Activity:" class="' . Nsd_trackingModelTrack::nsd_tracking_class($event) . '">Lorum Ipsum</li>';
		          
		          # I am having trouble getting the activity description to appear.
		          #$display .= '<li data-label="Activity:" class="' . Nsd_trackingModelTrack::nsd_tracking_class($event) . '">' . $event['activity'] . '</li>';
		          #$display .= '<li data-label="Activity:" class="' . $event['activityClass'] . '">' . $event['activity'] . '</li>';
		          
		          
		          $display .= '<li data-label="Location:">' . (strlen(trim($event['location'])) ? trim($event['location']) : '-') . '</li>';
		          $display .= '</ul>';
		      }
		      $display .= '</div>';
		  $display .= '</div>';
*/




		}
		else
		{
	
			$display .= "<div class='details section row'>";
			$display .= "	<div class='col-sm-12'><h2>Details</h2></div>";
			$display .= "	<div class='col-sm-6'>";
			$display .= "		<div class='item'><span>Tracking Number:</span>".$this->objDisplay->tracking_number."</div>";
			$display .= "		<div class='item'><span>PO #:</span>".$this->objDisplay->po_number."</div>";
			$display .= "		<div class='item'><span>REF #1:</span>".$this->objDisplay->ref_1_number."</div>";
			$display .= "	</div>";
			$display .= "	<div class='col-sm-6'>";
			$display .= "		<div class='item'><span>Service:</span>".$this->objDisplay->service_level."</div>";
			$display .= "		<div class='item'><span>Service Description:</span>".$this->objDisplay->service_level_description_long."</div>";
			$display .= "	</div>";
			$display .= "</div>"; // /details
	
			$display .= "<div class='status section row'>";
			$display .= "	<div class='col-sm-6'>";
			$display .= "		<div class='item'><span>Origin:</span>".$this->objDisplay->origin."</div>";
			$display .= "	</div>";
			$display .= "	<div class='col-sm-6'>";
			$display .= "		<div class='item'><span>Destination:</span>".$this->objDisplay->destination."</div>";
			
				if ($delivery) :
	/*
	# This is commented out because scheduled delivery always has a value, so estimated delivery was never showing.
	
					if ($this->objDisplay->scheduled_delivery_date) :
						$display .= "<div class='item'><span>Scheduled Delivery:</span>".$this->objDisplay->scheduled_delivery_date."</div>";
					elseif ($this->objDisplay->estimated_delivery_date) :
						$display .= "<div class='item'><span>Estimated Delivery:</span>".$this->objDisplay->estimated_delivery_date."</div>";
					endif;
	*/
				$display .= $this->objDisplay->estimated_delivery_date ? "		<div class='item'><span>Estimated Delivery:</span>".$this->objDisplay->estimated_delivery_date."</div>" : "";
				$display .= $this->objDisplay->scheduled_delivery_date ? "		<div class='item'><span>Scheduled Delivery:</span>".$this->objDisplay->scheduled_delivery_date."</div>" : "";
				$display .= $this->objDisplay->actual_delivery_date ? "		<div class='item'><span>Actual Delivery:</span>".$this->objDisplay->actual_delivery_date."</div>" : "";
				$display .= $this->objDisplay->pod_information ? "		<div class='item'><span>POD Info:</span>".$this->objDisplay->pod_information."</div>" : "";
				endif;
		
				if ($return) :
				$display .= $this->objDisplay->scheduled_pickup_date ? "		<div class='item'><span>Scheduled Pickup:</span>".$this->objDisplay->scheduled_pickup_date."</div>" : "";
				$display .= $this->objDisplay->actual_pickup_date ? "		<div class='item'><span>Actual Pickup:</span>".$this->objDisplay->actual_pickup_date."</div>" : "";
				$display .= $this->objDisplay->pod_information ? "		<div class='item'><span>POP Info:</span>".$this->objDisplay->pod_information."</div>" : "";
				endif;
	
			$display .= "	</div>";

			
	
			$display .= "	<div class='col-sm-12 bar-container'>";
			$display .= "		<div class='tracking-bar'>";
			foreach ($this->objDisplay->bar_matrix as $bar) :
				$currentClass = $bar->flag_current ? " current" : "";
	
				$display .= "			<div class='tracking-bar-position ".$bar->class." ".$bar->color.$currentClass."'>";
				#$display .= "			<div class='tracking-bar-position ".$bar->color.$currentClass."'>";
				$display .= "				<div class='tracking-bar-container'>";
				$display .= "					<div class='tracking-bar-alert'></div>";
				$display .= "					<div class='tracking-bar-chevron'></div>";
				$display .= "				</div>";
				$display .= "				<div class='tracking-bar-label'>".$bar->bar_state."</div>";
				$display .= "				<div class='tracking-bar-line left'></div>";
				$display .= "				<div class='tracking-bar-line right'></div>";
				$display .= "			</div>";
			endforeach;
			$display .= "		</div>";
			$display .= "	</div>";
	
			$stateExceptionClass = $this->objDisplay->current_state_exception ? " exception" : "";
			$display .= "	<div class='col-sm-12 text-center current-status".$stateExceptionClass."'><h3>Current Status: ".$this->objDisplay->current_state."</h3></div>";
	
			$display .= "</div>"; // /status

			

		
/*
			$display .= "<div class='notify section row'>";
			
			$display .= "<div class='col-sm-12'>";
			$display .= "	<h2>Get Updates</h2>";
			$display .= "</div>";
			
			$display .= "<div class='the-return'>";
			#$display .= "<div class='col-sm-12 intro'>Use the form below to sign up to receive updates on your order.  Enter your email if you would like updates via email, enter your cell phone number if you would like updates via text, or enter both if you would like both.</div>";
			$display .= "<div class='col-sm-12 intro'>Enter your email address in the form below to sign up to receive updates on your order.</div>";
			$display .= "<form data-toggle='validator' role='form' class='notify' id='formNotify'>";
			$display .= "	<div class='form-group'>";
			$display .= " 		<div class='form-inline'>";

			$display .= "			<div class='form-group has-feedback col-sm-3' id='notifyEmailGroup'>";
			#$display .= "				<label for='notifyEmail'>Email</label>";
		    $display .= "				<input type='email' class='form-control' id='notifyEmail' data-delay='2000' placeholder='Enter email address'>";
			$display .= "				<span class='glyphicon form-control-feedback' aria-hidden='true'></span>";
			$display .= "				<div class='help-block with-errors'></div>";
			$display .= "				<div class='help-block'><a class='purechat-button-expand' >Need Help?</a></div>";
			$display .= "			</div>";

			#$display .= "			<div class='form-group has-feedback col-sm-3' id='notifyPhoneGroup'>";
			##$display .= "				<label for='notifyPhone'>Text </label>";
		    #$display .= "				<input type='phone' class='form-control' id='notifyPhone' pattern='^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$' data-error='Please enter a valid 10-digit phone number' placeholder='Enter phone number'>";
			#$display .= "				<span class='glyphicon form-control-feedback' aria-hidden='true'></span>";
			#$display .= "				<div class='help-block with-errors'></div>";
			#$display .= "			</div>";

			$display .= "			<div class='form-group col-sm-3' id='notifySubmit'>";
			$display .= "				<button type='submit' id='btnGetUpdates' class='btn' disabled>Get Updates</button>";
			$display .= "				<div class='help-block with-errors'></div>";
			$display .= "			</div>";
			$display .= "			<input type='hidden' name='detailLineID' id='detailLineID' value='".$this->objDisplay->detail_line_id."' />";
			$display .= "			<div class='error-return col-sm-12 help-block with-errors text-danger'></div>";
			$display .= "		</div>";
			$display .= "	</div>";
			$display .= "</form>";
			$display .= "</div>"; // the-return
*/



			$display .= "</div>"; // /notify

			$display .= "<script type=\"text/javascript\">";
			$display .= "	jQuery(document).ready(function() {";
			$display .= "		jQuery('#formNotify').validator().on('invalid.bs.validator', function () {";
			$display .= "			jQuery('#btnGetUpdates').attr('disabled', true);";
			$display .= "		})";
			$display .= "	});";
			$display .= "	jQuery(document).ready(function() {";
			$display .= "		jQuery('#formNotify').validator().on('valid.bs.validator', function () {";
			$display .= "			jQuery('#btnGetUpdates').attr('disabled', false);";
			$display .= "		})";
			$display .= "	});";
			$display .= "</script>";			



	
			$display .= "<div class='history section row'>";
			$display .= "	<div class='col-sm-12'>";
			$display .= "		<h2>Transit History</h2>";
			$display .= "		<a class='toggleHistory' data-toggle='collapse' href='#collapseHistory' aria-expanded='false' aria-controls='collapseHistory'> </a>";
			$display .= "	</div>";
			$display .= "	<div class='clearfix'></div>";
	
			$display .= "	<div class='collapse' id='collapseHistory'>";
	
			$display .= "	<div class='col-sm-3 head'>Date/Time</div>";
			$display .= "	<div class='col-sm-7 head'>Activity</div>";
			$display .= "	<div class='col-sm-2 head'>Location</div>";
			
			foreach ($this->objDisplay->order_history as $history) :
				if (!$history->suppressed) :
					$display .= "	<div class='history-row'>";
					$display .= "	<div class='col-sm-3'>".$history->status_date."</div>";
					$statusExceptionClass = $history->status_exception ? " exception" : "";

					$history_status_description = explode("; ", $history->status_description);
					$display .= "	<div class='col-sm-7".$statusExceptionClass."'>".$history_status_description[0]."</div>";  //Need bar color as class to change message color
					$display .= "	<div class='col-sm-2'>".$history->status_location."</div>";
					$display .= "	<div class='clearfix'></div>";
					$display .= "	</div>";
				endif;
			endforeach;
			
			$display .= "	</div>"; // collapse
			$display .= "</div>"; // /history
		
		}	
		
		
	else :
		$display .= $this->objDisplay->error_message;
	endif;
endif;

$display .= $showobject ? "<h3>objDisplay</h3><pre>".print_r($this->objDisplay, true)."</pre>" : "";	
			
$display .= $showobject ? "<h3>objTMW</h3><pre>".print_r($this->objTMW, true)."</pre>" : "";	

$display .= $showobject ? "<h3>objRockhopper</h3><pre>".print_r($this->objRockhopper, true)."</pre>" : "";	

echo  $display;

?>

