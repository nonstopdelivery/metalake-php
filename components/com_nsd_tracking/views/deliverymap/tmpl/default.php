<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$showobject = true;
$showlinks = true;

$doortodoor = false;
$lastmile = false;
$delivery = false;
$return = false;

switch ($this->objDisplay->delivery_type) {
	case "DD":
		$doortodoor = true;
		break;
	case "LM":
		$lastmile = true;
		break;
}

switch ($this->objDisplay->order_type) {
	case "Delivery":
		$delivery = true;
		break;
	case "Return":
		$return = true;
		break;
}

$display = "";

$display .= "<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='post' name='adminForm' id='adminForm' class='form-inline track'>";
$display .= "	<div class='row'>";
$display .= "	<div class='form-group col-sm-4'>";
$display .= "		<input type='text' class='form-control' name='trace_number' id='trace_number' value='".$this->objForm->trace_number."' placeholder='Enter Tracking Number' />";
$display .= "	</div>";
$display .= "	<input class='btn col-sm-1' type='Submit' id='sendResponse' value='Track' onclick=\"document.adminForm.submit();\" />";
$display .= "	<input type='hidden' name='flag_run' value='1' />";
$display .= "	</div>";
$display .= "</form>";



if ( $this->objDisplay->flag_run == "1" ) :
	if ( $this->objDisplay->error == "0" ) :

		$display .= "<div class='details section row'>";
		$display .= "	<div class='col-sm-12'><h2>Details</h2></div>";
		$display .= "	<div class='col-sm-6'>";
		$display .= "		<div class='item'><span>Tracking Number:</span>".$this->objDisplay->tracking_number."</div>";
		$display .= "		<div class='item'><span>PO #:</span>".$this->objDisplay->po_number."</div>";
		$display .= "		<div class='item'><span>REF #1:</span>".$this->objDisplay->ref_1_number."</div>";
		$display .= "	</div>";
		$display .= "	<div class='col-sm-6'>";
		$display .= "		<div class='item'><span>Service:</span>".$this->objDisplay->service_level."</div>";
		$display .= "		<div class='item'><span>Service Description:</span>".$this->objDisplay->service_level_description_long."</div>";
		$display .= "	</div>";
		$display .= "</div>"; // /details

		$display .= "<div class='status section row'>";
		$display .= "	<div class='col-sm-6'>";
		$display .= "		<div class='item'><span>Origin:</span>".$this->objDisplay->origin."</div>";
		$display .= "	</div>";
		$display .= "	<div class='col-sm-6'>";
		$display .= "		<div class='item'><span>Destination:</span>".$this->objDisplay->destination."</div>";
		
			if ($delivery) :
			$display .= $this->objDisplay->estimated_delivery_date ? "		<div class='item'><span>Estimated Delivery Date:</span>".$this->objDisplay->estimated_delivery_date."</div>" : "";
			$display .= $this->objDisplay->scheduled_delivery_date ? "		<div class='item'><span>Scheduled Delivery Date:</span>".$this->objDisplay->scheduled_delivery_date."</div>" : "";
			$display .= $this->objDisplay->actual_delivery_date ? "		<div class='item'><span>Actual Delivery Date:</span>".$this->objDisplay->actual_delivery_date."</div>" : "";
			$display .= $this->objDisplay->pod_information ? "		<div class='item'><span>POD Info:</span>".$this->objDisplay->pod_information."</div>" : "";
			endif;
	
			if ($return) :
			$display .= $this->objDisplay->scheduled_pickup_date ? "		<div class='item'><span>Scheduled Pickup Date:</span>".$this->objDisplay->scheduled_pickup_date."</div>" : "";
			$display .= $this->objDisplay->actual_pickup_date ? "		<div class='item'><span>Actual Pickup Date:</span>".$this->objDisplay->actual_pickup_date."</div>" : "";
			$display .= $this->objDisplay->pod_information ? "		<div class='item'><span>POD Info:</span>".$this->objDisplay->pod_information."</div>" : "";
			endif;

		$display .= "	</div>";

		$display .= "	<div class='col-sm-12 bar-container'>";
		$display .= "		<div class='tracking-bar'>";
		foreach ($this->objDisplay->bar_matrix as $bar) :
			$currentClass = $bar->flag_current ? " current" : "";

			$display .= "			<div class='tracking-bar-position ".$bar->class." ".$bar->color.$currentClass."'>";
			#$display .= "			<div class='tracking-bar-position ".$bar->color.$currentClass."'>";
			$display .= "				<div class='tracking-bar-container'>";
			$display .= "					<div class='tracking-bar-alert'></div>";
			$display .= "					<div class='tracking-bar-chevron'></div>";
			$display .= "				</div>";
			$display .= "				<div class='tracking-bar-label'>".$bar->bar_state."</div>";
			$display .= "				<div class='tracking-bar-line left'></div>";
			$display .= "				<div class='tracking-bar-line right'></div>";
			$display .= "			</div>";
		endforeach;
		$display .= "		</div>";
		$display .= "	</div>";

		$stateExceptionClass = $this->objDisplay->current_state_exception ? " exception" : "";
		$display .= "	<div class='col-sm-12 text-center current-status".$stateExceptionClass."'><h3>Current Status: ".$this->objDisplay->current_state."</h3></div>";

		$display .= "</div>"; // /status


		$display .= "<div class='history section row'>";
		$display .= "	<div class='col-sm-12'><h2>Transit History</h2></div>";

		$display .= "	<div class='col-sm-3 head'>Date/Time</div>";
		$display .= "	<div class='col-sm-7 head'>Activity</div>";
		$display .= "	<div class='col-sm-2 head'>Location</div>";
		
		foreach ($this->objDisplay->order_history as $history) :
			if (!$history->suppressed) :
				$display .= "	<div class='history-row'>";
				$display .= "	<div class='col-sm-3'>".$history->status_date."</div>";
				$statusExceptionClass = $history->status_exception ? " exception" : "";
				$display .= "	<div class='col-sm-7".$statusExceptionClass."'>".$history->status_description."</div>";  //Need bar color as class to change message color
				$display .= "	<div class='col-sm-2'>".$history->status_location."</div>";
				$display .= "	<div class='clearfix'></div>";
				$display .= "	</div>";
			endif;
		endforeach;
		
		$display .= "</div>"; // /history


		$display .= $showobject ? "<pre>".print_r($this->objDisplay, true)."</pre>" : "";		
	else :
		$display .= $this->objDisplay->error_message;
	endif;
endif;





if ($showlinks) :
$display .= "<div class='clearfix'><Br /><br ></div>";
$display .= "<pre>";

$display .= "<h3>Testing</h3>";
$display .= "<h4>Door-to-Door</h4>";

$arrOID = array("20100047", "20200000", "20200261", "20100004", "20204163", "20204053");

foreach( $arrOID as $oid )
{
	$display .= "delivery: <a href='#' onClick='document.adminForm.trace_number.value=\"".$oid."\"; document.adminForm.submit();'>".$oid."</a><br />";	
}


$arrOID = array("31012204-R", "31012666-R", "R0500035", "R1058860");
foreach( $arrOID as $oid )
{
	$display .= "return: <a href='#' onClick='document.adminForm.trace_number.value=\"".$oid."\"; document.adminForm.submit();'>".$oid."</a><br />";	
}
$display .= "<br />";




$display .= "<h4>Last Mile</h4>";
$arrOID = array("31010990", "31011050", "1571971", "9101078", "31012795", "31029644", "31021162");

foreach( $arrOID as $oid )
{
	$display .= "delivery: <a href='#' onClick='document.adminForm.trace_number.value=\"".$oid."\"; document.adminForm.submit();'>".$oid."</a><br />";	
}


$arrOID = array("31020501-R", "31026278-R", "31026749-R", "31020542-R");
foreach( $arrOID as $oid )
{
	$display .= "return: <a href='#' onClick='document.adminForm.trace_number.value=\"".$oid."\"; document.adminForm.submit();'>".$oid."</a><br />";	
}
$display .= "<br />";

$display .= "more: <a href='https://nonstoptest.tmwcloud.com/' target='new'>https://nonstoptest.tmwcloud.com/</a><br />";
$display .= "</pre>";
endif;

echo  $display;

?>

