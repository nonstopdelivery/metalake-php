<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Nsd_trackingViewTrack extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');
		#$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_nsd_tracking');



		$controllerName = "Nsd_trackingViewTrack";
		$functionName = "view";
		
		$db =& JFactory::getDBO();
		
		$flag_production = 0;
		
		$flag_run = JRequest::getVar('flag_run', 1);
		$trace_number = JRequest::getVar('trace', '');

		$tm4web_usr = JRequest::getVar('user', '');
		$tm4web_pwd = JRequest::getVar('pswd', '');

		
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	


		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		$stamp_start_micro = microtime(true);



		#session
		$objForm = MetalakeHelperCore::getSessObj( "objSessTracking" );
		
		$jinput = JFactory::getApplication()->input;
		$trace_number = $jinput->get('trace_number', '', 'STRING');
		
		$objForm->trace_number = $trace_number;
		$objForm->tmw4web_user = $tm4web_usr;
		$objForm->tmw4web_password = $tm4web_pwd;
		$objForm->flag_api = 1;		
		
		$this->assignRef('objForm', $objForm); 


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objDisplay = new stdClass();
		$objDisplay->flag_run = $flag_run;

		if ( $flag_run == "1" )
		{

			$objTMW = Nsd_trackingModelTrack::getTMW( $objForm );
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
			$objTrackingMatrix = Nsd_trackingModelTrack::getTrackingMatrix( $objForm, $objTMW );
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objTrackingMatrix: ".print_r($objTrackingMatrix, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
			$objDisplay->error = $objTMW->error;
			
			$objDisplay->error_message = $objTMW->error_message;			
			
			$objDisplay->delivery_type = $objTrackingMatrix->delivery_type;
			
			$objDisplay->delivery_type_description = ( $objTrackingMatrix->delivery_type == "DD" ) ? "Door-to-Door" : "Last Mile" ;
	
			$objDisplay->order_type = $objTrackingMatrix->order_type;
	
			$objDisplay->tracking_number = $objTMW->order->BILL_NUMBER;
	
			$objDisplay->current_state = $objTrackingMatrix->current_state;
			
			$objDisplay->current_state_exception = "0";
			
			$objDisplay->bar_state = $objTrackingMatrix->bar_state;
			
			$objDisplay->bar_color = $objTrackingMatrix->bar_color;
	
				$objInput = new stdClass();
				$objInput->delivery_type = $objTrackingMatrix->delivery_type;
				$objInput->order_type = $objTrackingMatrix->order_type;
	
			$objListBarMatrix = Nsd_trackingModelTrack::getBarMatrixInfo( $objInput );
	
			$objDisplay->bar_matrix = $objListBarMatrix;
	
			// !flag_po_number | po_number		
			
			if ( $objTrackingMatrix->flag_po_number )
			{
				if ( $objTMW->order->CUSTOMER == "10343" ) 
				{
					$objDisplay->po_number = trim($objTMW->order->TRACE_TYPE_O);
				}
				else
				{
					$objDisplay->po_number = trim($objTMW->order->TRACE_TYPE_P);
				}
				
			}
			else
			{
				$objDisplay->po_number = "";
			}
	
	
	
			// !flag_ref_1_number | ref_1_number			
			
			if ( $objTrackingMatrix->flag_ref_1_number )
			{
				if ( $objTMW->order->CUSTOMER == "10343" ) 
				{
					$objDisplay->ref_1_number = trim($objTMW->order->TRACE_TYPE_M);
				}
				else
				{
					$objDisplay->ref_1_number = trim($objTMW->order->TRACE_TYPE_Q);
				}
				
			}
			else
			{
				$objDisplay->ref_1_number = "";
			}		
	
	
	
	
	
	
			// !ref_2_number	
	
			$objDisplay->ref_2_number = ( $objTrackingMatrix->flag_ref_2_number ) ? trim($objTMW->order->TRACE_TYPE_W) : "" ;
	
	
	
	
			// !flag_service_level | service_level	
	
			if ( $objTrackingMatrix->flag_service_level == "TRUE"  )
			{
				
				$objServiceLevel = Nsd_trackingModelTrack::getServiceLevelInformation( trim($objTMW->order->SERVICE_LEVEL) );
				
				$objDisplay->service_level = $objServiceLevel->description;
				
				$objDisplay->service_level_description_long = $objServiceLevel->arrDescriptionLong;
				
			}
			else
			{
				$objDisplay->service_level = $objDisplay->service_level_description_long = "";
				
			}
	
	
	
	
	
			// !flag_origin | origin	
	
			if ( $objTrackingMatrix->flag_origin )
			{
				switch( $objTrackingMatrix->origin_location )
				{
					case "ORIGCITY, ORIGPROV":
					$objDisplay->origin = trim($objTMW->order->ORIGCITY).", ".trim($objTMW->order->ORIGPROV);
						break;
	
	
					case "ORIGCITY, ORIGPROV, ORIGPC":
					$objDisplay->origin = trim($objTMW->order->ORIGCITY).", ".trim($objTMW->order->ORIGPROV)." ".trim($objTMW->order->ORIGPC);
						break;
					
					default:
					$objDisplay->origin = "";
				}
			}
	
	
	
	
	
			// !flag_destination | destination	
	
			if ( $objTrackingMatrix->flag_destination )
			{
				switch( $objTrackingMatrix->destination_location )
				{
					case "DESTCITY, DESTROV":
					$objDisplay->destination = trim($objTMW->order->DESTCITY).", ".trim($objTMW->order->DESTPROV);
						break;
	
	
					case "DESTCITY, DESTROV, DESTPC":
					$objDisplay->destination = trim($objTMW->order->DESTCITY).", ".trim($objTMW->order->DESTPROV)." ".trim($objTMW->order->DESTPC);
						break;
					
					default:
					$objDisplay->destination = "";
				}
			}
	
	
	
	
	
	
	
			// !flag_estimated_delivery_date | estimated_delivery_date	
	
			if ( $objTrackingMatrix->flag_estimated_delivery_date == "TRUE"  )
			{
	
				switch( $objTrackingMatrix->estimated_delivery )
				{
					case "DELIVER_BY":
					
					$delivered_by = ( $objTMW->order->DELIVER_BY != "" ) ? date('m/d/Y', strtotime($objTMW->order->DELIVER_BY) ) : "";
					$objDisplay->estimated_delivery_date = $delivered_by;
						break;
	
	
					case "Not Yet Known":
					$objDisplay->estimated_delivery_date = "Not Yet Known";
						break;
					
					default:
					$objDisplay->estimated_delivery_date = "";
				}
				
			}
			else
			{
				$objDisplay->estimated_delivery_date = "";
			}	
	
	
	
	
	
	
	
			// !flag_scheduled_delivery_date | scheduled_delivery_date	
	
			if ( $objTrackingMatrix->flag_scheduled_delivery_date == "TRUE" )
			{
	
				#If DELIVER_APPT_MADE = 'True' then value = "Not Yet Scheduled"
				#If DELIVER_APPT_MADE = 'False' then vlaue = "DELIVER_BY and DELIVER_BY_END)
	
				if( $objTMW->order->DELIVERY_APPT_MADE )
				{
					$objDisplay->scheduled_delivery_date = "Not Yet Scheduled";
				}
				else
				{
					$delivered_by = ( $objTMW->order->DELIVER_BY != "" ) ? date('m/d/Y', strtotime($objTMW->order->DELIVER_BY) ) : "";
					$delivered_by_end = ( $objTMW->order->DELIVER_BY_END != "" ) ? date('m/d/Y', strtotime($objTMW->order->DELIVER_BY_END) ) : "";				
					
					
					$objDisplay->scheduled_delivery_date = $delivered_by." / ".$delivered_by_end;
				}
				
			}
			else
			{
				$objDisplay->scheduled_delivery_date = "";
			}
	
	
	
	
	
	
			// !flag_scheduled_pickup_date | scheduled_pickup_date	
	
			if ( $objTrackingMatrix->flag_scheduled_pickup_date == "TRUE"  )
			{
	
				#If DELIVER_APPT_MADE = 'True' then value = "Not Yet Scheduled"  
				#If DELIVER_APPT_MADE = 'False' then vlaue = "DELIVER_BY and DELIVER_BY_END)
	
	
				if( $objTMW->order->DELIVERY_APPT_MADE == "TRUE"  )
				{
					$objDisplay->scheduled_pickup_date = "Not Yet Scheduled";
				}
				else
				{
					$delivered_by = ( $objTMW->order->DELIVER_BY != "" ) ? date('m/d/Y h:i:sa', strtotime($objTMW->order->DELIVER_BY) ) : "";
					$delivered_by_end = ( $objTMW->order->DELIVER_BY_END != "" ) ? date('m/d/Y h:i:sa', strtotime($objTMW->order->DELIVER_BY_END) ) : "";
					
					$objDisplay->scheduled_pickup_date = $delivered_by." / ".$delivered_by_end;
				}
				
			}
			else
			{
				$objDisplay->scheduled_pickup_date = "";
			}
	
	
	
	
	
	
	
			// !flag_actual_pickup_date | actual_pickup_date	
	
			if ( $objTrackingMatrix->flag_actual_pickup_date == "TRUE"  )
			{
	
				#get ODRSTAT.CHANGED where ODR.STATUS_CODE = 'PICKEDUP' and ODRSTAT.ORDER_ID = DETAIL_LINE_ID
				
				$objOrderHistory = Nsd_trackingModelTrack::getOrderHistoryDate( $objForm, $objTMW, $objTrackingMatrix, $order_status = "PICKEDUP" );
				
	
				$objDisplay->actual_pickup_date = ( $objOrderHistory->OS_CHANGED != "" ) ? date('m/d/Y h:i:sa', strtotime($objOrderHistory->OS_CHANGED)) : "";
			}
			else
			{
				$objDisplay->actual_pickup_date = "";
			}
	
	
	
	
	
	
	
	
			// !flag_actual_delivery_date | actual_delivery_date	
	
			if ( $objTrackingMatrix->flag_actual_delivery_date == "TRUE"  )
			{
	
				# ODRSTAT.CHANGED where ODR.STATUS_CODE = 'DELIVERED' and ODRSTAT.ORDER_ID = TLORDER.DETAIL_LINE_ID
				
				$objOrderHistory = Nsd_trackingModelTrack::getOrderHistoryDate( $objForm, $objTMW, $objTrackingMatrix, $order_status = "DELIVERED" );
	
	
				$objDisplay->actual_delivery_date = ( $objOrderHistory->OS_CHANGED != "" ) ? date('m/d/Y h:i:sa', strtotime($objOrderHistory->OS_CHANGED)) : "";
			}
			else
			{
				$objDisplay->actual_delivery_date = "";
			}
	
	
	
	
	
	
	
	
			// !flag_pod_information | pod_information
	
			if ( $objTrackingMatrix->flag_pod_information == "TRUE"  )
			{
	
	
				if ( $objTMW->order->SITE_ID == "SITE6" )
				{
					#for SITE6 (delivery) ODRSTAT.STATE_COMMENT where ODR.STATUS_CODE = 'DELIVERED' and ODRSTAT.ORDER_ID = TLORDER.DETAIL_LINE_ID
					
					$objOrderHistory = Nsd_trackingModelTrack::getOrderHistoryDate( $objForm, $objTMW, $objTrackingMatrix, $order_status = "DELIVERED" );
					
					$objDisplay->pod_information = ( $objOrderHistory->OS_CHANGED != "" ) ? date('m/d/Y h:i:sa', strtotime($objOrderHistory->OS_CHANGED)) : "";
				}
				else
				{
	
					# for site5 (returns) ODRSTAT.STATE_COMMENT where ODR.STATUS_CODE = 'PICKEDUP' and ODRSTAT.ORDER_ID = TLORDER.DETAIL_LINE_ID
	
					$objOrderHistory = Nsd_trackingModelTrack::getOrderHistoryDate( $objForm, $objTMW, $objTrackingMatrix, $order_status = "PICKEDUP" );
					
					$objDisplay->pod_information = ( $objOrderHistory->OS_CHANGED != "" ) ? date('m/d/Y h:i:sa', strtotime($objOrderHistory->OS_CHANGED)) : "";
				}
	
				
			}
			else
			{
				$objDisplay->pod_information = "";
			}
	
	
			// !order history
			
			$arrOrderHistory = Nsd_trackingModelTrack::getArrOrderHistory( $objForm, $objTMW, $objTrackingMatrix );
	
			$objDisplay->order_history = $arrOrderHistory;


			#update the color in the bar_matrix


			$isFirstInArray = "1";
			foreach( $arrOrderHistory as $history )
			{

				

				if ( $isFirstInArray == "1" && $history->suppressed == "0")
				{
					$barColor = $history->bar_color;
					$barState = $history->bar_state;
					
					if ( $history->status_exception == "1" )
					{
						$objDisplay->current_state = $history->status_description;
						$objDisplay->current_state_exception = "1";
						
					}
					
					
					
					$isFirstInArray = "0";
					
					
					
					
					
					
				}			
				
			}


			$objDisplay->bar_matrix = array_reverse($objDisplay->bar_matrix, true);

			$foundBarState = "0";
			
			foreach( $objDisplay->bar_matrix as $objBar  )
			{
				$objBar->color = ( $foundBarState == "1" ) ? "green" : "";
				$objBar->flag_current = 0;
				
				if ( $objBar->bar_state == $barState && $foundBarState == "0")
				{
					$objBar->color = $barColor;
					$foundBarState = "1";
					$objBar->flag_current = 1;
				}
			
			}


			$objDisplay->bar_matrix = array_reverse($objDisplay->bar_matrix, true);


			#log the activity

			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			    $remote_address = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			    $remote_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
			    $remote_address = $_SERVER['REMOTE_ADDR'];
			}

			$objActivityLog = new stdClass();
			$objActivityLog->remote_address =  $remote_address;
			$objActivityLog->trace = $trace_number;
			$objActivityLog->trace_type = "Bill Number";
			$objActivityLog->delivery_type = $objDisplay->delivery_type_description;
			$objActivityLog->order_type = $objDisplay->order_type;
			$objActivityLog->json_return = json_encode($objDisplay);

			
			Nsd_trackingController::logActivity( $objActivityLog );

			
		}


		$this->assignRef('objDisplay', $objDisplay);
		

		#https://stackoverflow.com/questions/137021/php-object-as-xml-document
		$replacements = array();

		$objDisplay = json_decode(json_encode($objDisplay));
		
		$apiReturn = XMLSerializer::generateValidXmlFromMixiedObj($objDisplay, "", $replacements, $add_header="false");
		

		
		$this->assignRef('apiReturn', $apiReturn);		
		




		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_nsd_tracking');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}


		$this->_prepareDocument();

		parent::display($tpl="xml");





	}


	public function array2xml($data, $root = null){
	    $xml = new SimpleXMLElement($root ? '<' . $root . '/>' : '<root/>');
	    array_walk_recursive($data, function($value, $key)use($xml){
	        $xml->addChild($key, $value);
	    });
	    return $xml->asXML();
	}



	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_NSD_TRACKING_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
