<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Nsd_trackingViewContactlesspod extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');
		#$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_nsd_tracking');



		$controllerName = "Nsd_trackingViewContactlesspod";
		$functionName = "view";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	


		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		$stamp_start_micro = microtime(true);



		#session
		#$objForm = MetalakeHelperCore::getSessObj( "objSessTracking" );
		
		$objForm = new stdClass();
		
		$jinput = JFactory::getApplication()->input;
		
		$trace_number = trim($jinput->get('t', '', 'STRING'));
		$trace_zipcode = JRequest::getVar('z', '', 'STRING');
		$flag_run = trim($jinput->get('r', '0', 'STRING'));

		$objForm->trace_number = $trace_number;
		$objForm->trace_zipcode = $trace_zipcode;
		$objForm->flag_api = "0";
		$objForm->flag_trace_page = "1";
		
		$this->assignRef('objForm', $objForm); 
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objDisplay = new stdClass();

		if ( $flag_run == "1" )
		{


			if ( trim($objForm->trace_number) == "" )
			{

				$objDisplay->error = 1;
				$objDisplay->error_code = 33;
				$objDisplay->error_message = "<h2>No Order Value</h2>No order number was entered. Please enter an order number.";
				$this->assignRef('objDisplay', $objDisplay);
				
			}
			elseif ( trim($objForm->trace_zipcode) == "" )
			{

				$objDisplay->error = 1;
				$objDisplay->error_code = 33;
				$objDisplay->error_message = "<h2>No Zip Code</h2>No zip code was entered. Please enter the zip code.";

				
			}
			else
			{
				
				#truckmate
				$objDisplay->dataOrigin = "truckmate";

				$objDisplay->error = 0;
				$objDisplay->error_code = "";
				$objDisplay->error_message = "";
		
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | beacon 1";
				$logMessage .= "\n\r objForm: ".print_r($objForm, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				$objTMW = Nsd_truckmateController::getTruckmateOrderFromWorkato( $objForm );
				
				$arrStatus = array();
				$arrStatus[] = "OFD";
				$arrStatus[] = "OFP";
				$objReturnStatusExists = Nsd_trackingController::determineOrderStatusValid( $objTMW, $arrStatus );
				

				$arrStatus = array();
				$arrStatus[] = "DELIVERED";
				$arrStatus[] = "COMPLETED";
				$arrStatus[] = "DISPOSED";
				$arrStatus[] = "PICKEDUP";
				
				$objReturnEarliestDateOfOrderStatus = Nsd_trackingController::determineEarliestDateOfOrderStatus( $objTMW, $arrStatus );
				
								
				
				
				
				if ( $objTMW->error == "1" )
				{
					$objDisplay->error = 1;
					$objDisplay->error_code = 44;
					$objDisplay->error_message = "<h2>Error</h2>No order was found. Please try again.";
					$this->assignRef('objDisplay', $objDisplay);
				}
				else
				{
					$objTMW->display = 1;
					$this->assignRef('objTMW', $objTMW);	

					
					MetalakeHelperCore::clearSessObj( "objSessPODObjTMW" );
					
					MetalakeHelperCore::setSessObj( "objSessPODObjTMW", $objTMW );			


					# test the zip code from the order to the zip code from t
					$challenge_zipcode = ( $objTMW->order->SITE_ID == "SITE5"  ) ?  $objTMW->order->ORIGPC : $objTMW->order->DESTPC;
	
	
					if ( !$objTMW->error && (trim($objForm->trace_zipcode) != $challenge_zipcode) )
					{
						$objDisplay->error = 1;
						$objDisplay->error_code = 33;
						$objDisplay->error_message = "<h2>Incorrect Zip Code</h2>Please enter the correct zip code.";
					}		


					if ( !$objTMW->error && $objDisplay->error == "0" && $objReturnStatusExists->order_status_vaild == "0" )
					{
						$objDisplay->error = 1;
						$objDisplay->error_code = 43;
						$objDisplay->error_message = "<h2>Delivery Confirmation form is not available at this time.";
					}
					

					if ( !$objTMW->error && $objDisplay->error == "0" && $objReturnEarliestDateOfOrderStatus->order_status_exists == "1" && $objReturnEarliestDateOfOrderStatus->confirmation_eligible == "0" )
					{
						$objDisplay->error = 1;
						$objDisplay->error_code = 53;
						$objDisplay->error_message = "<h2>Your delivery/pickup was completed more than 48 hours ago, the feedback form is no longer available.";
					}	


				}
										

		
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r objListTMW: ".print_r($objListTMW, true);
				$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
				$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
				$logMessage .= "\n\r objHistory: ".print_r($objHistory, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
				#$this->assignRef('objTMW', $objTMW);



				#$objTMWNew = Nsd_truckmateController::getTruckmateOrderFromWorkato( $objForm );
				#$objTMWNew = Nsd_workatoController::getTruckmateOrderData( $objForm );
				


	
				#log the activity
	
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				    $remote_address = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				    $remote_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
				    $remote_address = $_SERVER['REMOTE_ADDR'];
				}
	
				$objActivityLog = new stdClass();
				$objActivityLog->remote_address =  $remote_address;
				$objActivityLog->trace = $trace_number;
				$objActivityLog->trace_type = "Bill Number";
				$objActivityLog->delivery_type = ""; //$objDisplay->delivery_type_description;
				$objActivityLog->order_type = ""; //$objDisplay->order_type;
				$objActivityLog->json_return = ""; //json_encode($objDisplay);
				$objActivityLog->tm4web_usr = ""; //$tm4web_usr;
				$objActivityLog->flag_api = $objForm->flag_api;
				$objActivityLog->return_format = "web";
				
				Nsd_trackingController::logActivity( $objActivityLog );
				
			}	
		
		}
		else
		{

			$objDisplay->error = 1;
			$objDisplay->error_code = 00;
			$objDisplay->error_message = "";
			$this->assignRef('objDisplay', $objDisplay);
			
		}

		$this->assignRef('objDisplay', $objDisplay);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objDisplay: ".print_r($objDisplay, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_nsd_tracking');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_NSD_TRACKING_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
