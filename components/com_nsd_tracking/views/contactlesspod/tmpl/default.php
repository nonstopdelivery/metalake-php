<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


$showobject = false;
#<script src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
?>


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script>
<!--
(function() {
	'use strict';
	window.addEventListener('load', function() {
    	// Fetch all the forms we want to apply custom Bootstrap validation styles to
		var forms = document.getElementsByClassName('needs-validation');
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {

		        if (form.checkValidity() === false) {
		        	event.preventDefault();
					event.stopPropagation();
		
					var errorElements = document.querySelectorAll("input.form-control:invalid");
					//console.log('a: ' + errorElements[0]);
				   
					for (let index = 0; index < errorElements.length; index++) {
					    const element = errorElements[index];
					    //console.log('b: ' +element);
					    //console.log('c: ' +element.id);
					    //console.log('d: ' +element.name);
					      
					    document.getElementById(element.id).parentElement.classList.add("has-error");
					    document.getElementById(element.id).nextSibling.innerHTML = "Required";
					    document.getElementsByName('error_'+element.name)[0].innerHTML = "Required";
					    document.getElementById(element.id).focus();
					     
					    jQuery('html, body').animate({
					    	scrollTop: jQuery(errorElements[0]).offset().top - 150   
					    }, 100);   
		
					    return false;
					}
		
			    }

	    	}, false);
	    });
	}, false);
})();


jQuery(document).ready(function(){
	
    jQuery('input[type="radio"]').click(function(){
        var inputValue = jQuery(this).attr("name");
		document.getElementsByName(inputValue)[0].parentElement.classList.remove("has-error");
        document.getElementsByName('error_'+inputValue)[0].innerHTML = "";
    });
});


function clearError(thing) 
{
	thing.parentElement.classList.remove("has-error");
	thing.nextSibling.innerHTML = "";
}


//-->
</script>
<?php


echo <<<METALAKE
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript">

$(document).ready(function(e) {

	$(".print_name").on('keyup',function(){
	    $(".signature").val($(this).val());
	});
} );



</script> 
METALAKE;
#this must be left justified



$display = "";

$display .= "<h2>Delivery Confirmation</h2>";
$display .= "<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='get' name='adminForm' id='adminForm' class='form-inline track'>";
$display .= "	<div class='row'>";
$display .= "		<div class='form-group col-sm-3'>";
$display .= "			<br /><input type='text' class='form-control' name='t' id='t' value=\"".$this->objForm->trace_number."\" placeholder='Order Number' />";
$display .= "		</div>";
$display .= "	<div class='form-group col-sm-3'>";
$display .= "			<br /><input type='text' class='form-control' name='z' id='z' value='".$this->objForm->trace_zipcode."' placeholder='Zip Code' />";
$display .= "	</div>";
$display .= "		<div class='form-group col-sm-2'>";
$display .= "			<br /><input class='form-control btn' type='Submit' id='sendResponse' value='Search' onclick=\"document.adminForm.submit();\" />";
$display .= "		</div>";
$display .= "		<div class='form-group col-sm-4 text-right'>";
$display .= "			<b>Questions?</b><br />Contact Customer Care:<br /><a href='tel:18337447673'>1-833-SHIP-NSD</a>";
$display .= "		</div>";
$display .= "	</div>";
$display .= "<input type='hidden' name='r' value='1' />";
$display .= "</form>";


if ( $this->objDisplay->error == "0" )
{ 

	if ( $this->objTMW->display == "1" )
	{
	
		if ( $this->objTMW->error == "0" )
		{
	
			$display .= "<form action='".htmlspecialchars(JFactory::getURI()->toString())."' method='post' name='podForm' id='podForm' class='needs-validation' data-disable='false' data-toggle='validator' novalidate>";	
	
	
			$display .= "<h2>".$this->objTMW->order->BILL_NUMBER."</h2>";
			$display .= "<br />";
	
			$display .= "<div class='row'>";

			$display .= "<div class='col-sm-4'>";
				$display .= "<h4>Origin (Shipper)</h4>";
				
				if( $this->objTMW->order->SITE_ID == "SITE5" )
				{
					#return
					$display .= $this->objTMW->order->ORIGNAME."<br />";
					$display .= $this->objTMW->order->ORIGADDR1."<br />";
					$display .= $this->objTMW->order->ORIGADDR2 ? $this->objTMW->order->ORIGADDR2."<br />" : "";
					$display .= $this->objTMW->order->ORIGCITY.", ".$this->objTMW->order->ORIGPROV." ".$this->objTMW->order->ORIGPC;	
																				
				}
				else
				{
					#delivery
					$display .= $this->objTMW->order->CALLNAME."<br />";					
				}
				
			$display .= "</div>";
			$display .= "<div class='col-sm-4'>";
				$display .= "<h4>Destination (Consignee)</h4>";
				
				if( $this->objTMW->order->SITE_ID == "SITE5" )
				{				

					#return
					$display .= "C/O NONSTOP DELIVERY<br />";
					$display .= $this->objTMW->order->CARE_OF_ADDR1."<br />";
					$display .= $this->objTMW->order->CARE_OF_ADDR2 ? $this->objTMW->order->CARE_OF_ADDR2."<br />" : "";
					$display .= $this->objTMW->order->CARE_OF_CITY.", ".$this->objTMW->order->CARE_OF_PROV." ".$this->objTMW->order->CARE_OF_PC;	

				}
				else
				{

					#delivery
					$display .= $this->objTMW->order->DESTNAME."<br />";
					$display .= $this->objTMW->order->DESTADDR1."<br />";
					$display .= $this->objTMW->order->DESTADDR2 ? $this->objTMW->order->DESTADDR2."<br />" : "";
					$display .= $this->objTMW->order->DESTCITY.", ".$this->objTMW->order->DESTPROV." ".$this->objTMW->order->DESTPC;	
					
				}

			$display .= "</div>";
			$display .= "<div class='col-sm-4'>";
				$display .= "<h4>Service Level</h4>";
				$display .= $this->objTMW->order->SERVICE_LEVEL;
				$display .= "<br /><br />";
				$display .= "<h4>Customer Reference</h4>";
				$display .= $this->objTMW->trace_types[array_search("Reference #1", array_column($this->objTMW->trace_types, 'TRACE_TYPE'))]->TRACE_NUMBER;
			$display .= "</div>";

			$display .= "</div>"; //row

			$display .= "<br />";
			$display .= "<h2>Order Information</h2>";
			$display .= "<br />";

			$display .= "<div class='row pod info header'>";
			$display .= "	<div class='col-sm-4'>Description</div>";
			$display .= "	<div class='col-sm-1 text-center'>QTY</div>";
			$display .= "	<div class='col-sm-1 text-center'>Weight</div>";
			$display .= "	<div class='col-sm-2'>PRODUCT ACCEPTED</div>";
			$display .= "	<div class='col-sm-2'>PRODUCT DAMAGED</div>";
			$display .= "	<div class='col-sm-2'>PACKAGING DAMAGED</div>";
			$display .= "</div>";
			
			foreach ($this->objTMW->order_details as $detail) :

				$display .= "<div class='row pod info'>";
				$display .= "	<div class='col-sm-4'>".$detail->DESCRIPTION."</div>";
				$display .= "	<div class='col-sm-1 text-center'>".$detail->PIECES."</div>";
				$display .= "	<div class='col-sm-1 text-center'>".$detail->WEIGHT."</div>";
				$display .= "	<div class='col-sm-2'>";
				
				$display .= "		<div  class='form-group'>";
				$display .= "		<div class='switch-field'>";
				$display .= "			<input class='form-control' type='radio' name='productReceived_".$detail->SEQUENCE."' id='productReceived_1_".$detail->SEQUENCE."' value='YES' required>";
				$display .= "			<label class='yes' for='productReceived_1_".$detail->SEQUENCE."'>Yes</label>";
				$display .= "			<input type='radio' name='productReceived_".$detail->SEQUENCE."' id='productReceived_2_".$detail->SEQUENCE."' value='NO' required>";
				$display .= "			<label class='no' for='productReceived_2_".$detail->SEQUENCE."'>No</label>";
				$display .= "		</div>";
				$display .= "		<div name='error_productReceived_".$detail->SEQUENCE."' class='with-errors'></div>";	
				$display .= "		</div>";





				$display .= "	</div>";		
				
				$display .= "	<div class='col-sm-2'>";
				$display .= "		<div  class='form-group'>";
				$display .= "		<div class='switch-field'>";
				$display .= "			<input class='form-control'  type='radio' name='productDamaged_".$detail->SEQUENCE."' id='productDamaged_1_".$detail->SEQUENCE."' value='YES'>";
				$display .= "			<label class='yes' for='productDamaged_1_".$detail->SEQUENCE."'>Yes</label>";
				$display .= "			<input type='radio' name='productDamaged_".$detail->SEQUENCE."' id='productDamaged_2_".$detail->SEQUENCE."' value='NO' required>";
				$display .= "			<label class='no' for='productDamaged_2_".$detail->SEQUENCE."'>No</label>";
				$display .= "		</div>";		
				$display .= "		<div name='error_productDamaged_".$detail->SEQUENCE."' class='with-errors'></div>";	
				$display .= "		</div>";
				$display .= "	</div>";		


				$display .= "	<div class='col-sm-2'>";
				$display .= "		<div  class='form-group'>";
				$display .= "		<div class='switch-field'>";
				$display .= "			<input class='form-control' type='radio' name='packagingDamaged_".$detail->SEQUENCE."' id='packagingDamaged_1_".$detail->SEQUENCE."' value='YES'>";
				$display .= "			<label class='yes' for='packagingDamaged_1_".$detail->SEQUENCE."'>Yes</label>";
				$display .= "			<input type='radio' name='packagingDamaged_".$detail->SEQUENCE."' id='packagingDamaged_2_".$detail->SEQUENCE."' value='NO' required>";
				$display .= "			<label class='no' for='packagingDamaged_2_".$detail->SEQUENCE."'>No</label>";
				$display .= "		</div>";		
				$display .= "		<div name='error_packagingDamaged_".$detail->SEQUENCE."' class='with-errors'></div>";	
				$display .= "		</div>";
				$display .= "	</div>";		

				$display .= "	<div class='col-sm-11'>";
				$display .= "    <textarea class='form-control' name='comment_".$detail->SEQUENCE."' id='comment_".$detail->SEQUENCE."' rows='3' placeholder='Product Condition or Comment'></textarea>";
				$display .= "	</div>";				



				$display .= "</div>"; //row


				$display .= "<script>";
				$display .= "        jQuery('#productReceived_1_".$detail->SEQUENCE."').click(function() {";
				$display .= "			var productReceivedVal1 = jQuery('#productReceived_1_".$detail->SEQUENCE."').val();";
				$display .= " 			if ( productReceivedVal1 == 'YES' ) ";
				$display .= " 			{ ";
				$display .= "           	jQuery('#productDamaged_1_".$detail->SEQUENCE."').prop('required', true);";
				$display .= "           	jQuery('#productDamaged_2_".$detail->SEQUENCE."').prop('required', true);";
				$display .= "           	jQuery('#packagingDamaged_1_".$detail->SEQUENCE."').prop('required', true);";
				$display .= "           	jQuery('#packagingDamaged_2_".$detail->SEQUENCE."').prop('required', true);";				
				$display .= " 			}";
				$display .= "        });";	

				$display .= "        jQuery('#productReceived_2_".$detail->SEQUENCE."').click(function() {";
				$display .= "			var productReceivedVal2 = jQuery('#productReceived_2_".$detail->SEQUENCE."').val();";
				$display .= " 			if ( productReceivedVal2 == 'NO' ) ";
				$display .= " 			{ ";
				$display .= "            	jQuery('#productDamaged_1_".$detail->SEQUENCE."').prop('required', false);";
				$display .= "            	jQuery('#productDamaged_2_".$detail->SEQUENCE."').prop('required', false);";				
				$display .= "            	jQuery('#packagingDamaged_1_".$detail->SEQUENCE."').prop('required', false);";
				$display .= "            	jQuery('#packagingDamaged_2_".$detail->SEQUENCE."').prop('required', false);";
				$display .= " 			}";
				$display .= "        });";	
				$display .= "</script>";



			endforeach;
			

			$display .= "<br />";
			$display .= "<h2>Delivery Feedback</h2>";
			$display .= "<br />";

			$rowcount = "1";
			foreach ( $this->objTMW->shipping_instructions_pod as $instructions ) :

				$display .= "<div class='row pod feedback'>";
				$display .= "	<div class='col-sm-7'>".strtoupper($instructions->shortDescriptionView)."</div>";
				$display .= "	<div class='col-sm-5'>";
				$display .= "		<div name='error_shipInstructions_".$instructions->instructionID."' class='with-errors pull-right'></div>";	
				$display .= "		<div class='switch-field'>";
				
				
				$display .= "			<input class='form-control' type='radio' name='shipInstructions_".$instructions->instructionID."' id='shipInstructions_1_".$instructions->instructionID."' value='YES' required>";
				$display .= "			<label class='yes' for='shipInstructions_1_".$instructions->instructionID."'>Yes</label>";
				
				
				$display .= "			<input type='radio' name='shipInstructions_".$instructions->instructionID."' id='shipInstructions_2_".$instructions->instructionID."' value='NO' required>";
				$display .= "			<label class='no' for='shipInstructions_2_".$instructions->instructionID."'>No</label>";
				
				$display .= "			<input class='form-control' type='radio' name='shipInstructions_".$instructions->instructionID."' id='shipInstructions_3_".$instructions->instructionID."' value='WAIVED' required>";				
				
				$display .= "			<label class='partial' for='shipInstructions_3_".$instructions->instructionID."'>Waived</label>";
				$display .= "		</div>";		
				$display .= "	</div>";
				$display .= "</div>";	

			endforeach;
			$display .= "<div><b>Note:</b> Property damage claims that are not noted at time of delivery will be denied without further review.</div>";



			$display .= "<br />";
			$display .= "<div class='form-group'>";
			$display .= "    <label for='comment'>Please place any comments here, including any notation of product condition, damage, or refusal.</label>";
			$display .= "    <textarea class='form-control' name='deliveryFeedbackComment' id='deliveryFeedbackComment' rows='3'></textarea>";
			$display .= "</div>";


			$display .= "<br />";
			$display .= "<h2>Customer Delivery Acceptance</h2>";
			$display .= "<br />";
			
			
			$display .= "<div class='form-group'>";
			$display .= "	<div class='switch-field'>";
			$display .= "		<b>Delivery Accepted</b>&nbsp;&nbsp;";
			$display .= "		<input class='form-control' type='radio' name='deliveryAccepted' id='deliveryAccepted1' value='YES' required>";
			$display .= "		<label class='yes' for='deliveryAccepted1'>Yes</label>";
			$display .= "		<input type='radio' name='deliveryAccepted' id='deliveryAccepted2' value='NO' required>";
			$display .= "		<label class='no' for='deliveryAccepted2'>No</label>";
			$display .= "		<input type='radio' name='deliveryAccepted' id='deliveryAccepted3' value='PARTIAL' required>";
			$display .= "		<label class='partial' for='deliveryAccepted3'>Partial</label>";
			$display .= "	</div>";		
			$display .= "	<div name='error_deliveryAccepted' class='with-errors'></div>";				
			$display .= "</div>";		

			$display .= "By typing your name below you are electronically signing this document.<br />";
			$display .= "<div class='form-group'>";
			$display .= "	<input type='text' name='print_name' id='print_name' class='print_name form-control' size='40' placeholder='Type Name' oninput='clearError(this);' required><div class='with-errors display-inline'></div>";
			$display .= "	<br /><input type='text' name='signature' id='signature' class='signature' size='40' placeholder='' disabled><br />";
			$display .= "</div>";		
			
			$display .= "<br />";
			$display .= "<input class='btn' type='Submit' name='submitPOD' id='submitPOD' value='Submit'/>";
			
			$display .= "<br />";
			$display .= "<br />";
			$display .= "<b>Disclaimer</b><Br />If you have any concerns with your product that is not related to damage or missing items, please contact your place of purchase. This document or service is not meant to replace a claim being filed. Please contact place of purchase to file a formal claim.";



			$display .= JHTML::_( 'form.token' );
			$display .= "<input type='hidden' name='option' value='com_nsd_tracking' />";
			$display .= "<input type='hidden' name='task' value='submitPOD' />";
			$display .= "<input type='hidden' name='trace_number' value='".$this->objForm->trace_number."' />";
			$display .= "<input type='hidden' name='trace_zipcode' value='".$this->objForm->trace_zipcode."' />";	
			$display .= "<input type='hidden' name='detailLineId' value='".$this->objTMW->order->DETAIL_LINE_ID."' />";		
			$display .= "</form>";

			
			
		}
		else
		{
			$display .= $this->objTMW->error_message;
		}

	}

}
else
{
	$display .= $this->objDisplay->error_message;
}


$display .= $showobject ? "<h3>objDisplay</h3><pre>".print_r($this->objDisplay, true)."</pre>" : "";	

			
$display .= $showobject ? "<h3>objTMW</h3><pre>".print_r($this->objTMW, true)."</pre>" : "";	
$display .= $showobject ? "<h3>objListTMW</h3><pre>".print_r($this->objListTMW, true)."</pre>" : "";	


echo  $display;


?>



