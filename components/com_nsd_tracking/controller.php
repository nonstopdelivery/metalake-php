<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');
JLoader::register('XMLSerializer', JPATH_SITE.'/components/com_nsd_tracking/helpers/xmlserializer.php');
jimport('twilio.Services.Twilio');
jimport('joomla.filesystem.file' );
require_once( JPATH_SITE.'/libraries/mpdf/vendor/autoload.php' );


/**
 * Class Nsd_trackingController
 *
 * @since  1.6
 */
class Nsd_trackingController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'trackingmatrixs');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}


// !TMW4WEB



	// !TRACKING PAGE FUNCTIONS


	// !getTrackingMatrix
	public static function getTrackingMatrix( $objForm, $objTMW )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getTrackingMatrix";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$order_type = ( strtoupper($objTMW->order->SITE_ID) == "SITE6" ) ? "Delivery" : "Return" ;

		$delivery_type = $objTMW->order->OP_CODE;


		$arrWhere = array();
		$arrWhere[] = "m.state = 1";
		#$arrWhere[] = "delivery_type = '".$objTMW->order->OP_CODE."'";
		$arrWhere[] = "delivery_type = '".$delivery_type."'";
		$arrWhere[] = "order_status = '".$objTMW->order->CURRENT_STATUS."'";
		$arrWhere[] = "order_type = '".$order_type."'";		

		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
		
		
		$query = "SELECT m.*, dt.description as 'delivery_type_description' FROM htc_nsd_tracking_matrix m left join htc_nsd_tracking_lu_delivery_type dt ON m.delivery_type=dt.name ". $where ;


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$db->setQuery($query);
		$objReturn = $db->loadObject();


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;		
	}


	// !getServiceLevelInformation
	public static function getServiceLevelInformation( $objTMW )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getServiceLevelInformation";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$strServiceLevel = trim($objTMW->order->SERVICE_LEVEL);
		$flag_return = ( trim($objTMW->order->SITE_ID) == "SITE5" ) ? "1" : "0";



		$service_level = strtoupper( $strServiceLevel );


		$arrWhere = array();
		$arrWhere[] = "state = 1";
		$arrWhere[] = "name = '".$service_level."'";
		$arrWhere[] = "flag_return = '".$flag_return."'";

		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
		
		
		$query = "SELECT * FROM htc_nsd_tracking_lu_service_descriptions ". $where ;
		$db->setQuery($query);
		$objReturn = $db->loadObject();
		
		$arrSL = json_decode($objReturn->description_long);
		
		$retUL = "<ul>";
		
		foreach( $arrSL as $sl )
		{
			
			$retUL .= "<li>".$sl."</li>";
		}
		
		$retUL .= "</ul>";
		
		$objReturn->arrDescriptionLong = $retUL;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;		
	}




	// !getOrderHistoryData
	public static function getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getOrderHistoryData";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		

		foreach( $objTMW->order_history as $history )
		{
			
			if ( $history->OS_STATUS_CODE == $order_status )
			{
				return $history;
			}
			
		}
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objReturn = new stdClass();
		$objReturn->OS_CHANGED = "";
		return $objReturn;			
		
	}




	// !getArrOrderHistory

	public static function getArrOrderHistory( $objForm, $objTMW, $objTrackingMatrix )
	{


		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getArrOrderHistory";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
		

		$logMessage = "INSIDE | 1 | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r objTMW->order_history: ".print_r($objTMW->order_history, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		foreach( $objTMW->order_history as $history )
		{

			$objInput = new stdClass();
			$objInput->delivery_type = $objTrackingMatrix->delivery_type;
			$objInput->order_type = $objTrackingMatrix->order_type;
			$objInput->order_status = $history->OS_STATUS_CODE;

			$objMatrix = Nsd_trackingController::getMatrixInfo( $objInput );

			$logMessage = "INSIDE | 1.1 | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objInput: ".print_r($objInput, true);
			$logMessage .= "\n\r objMatrix: ".print_r($objMatrix, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
			switch( $objMatrix->status_location )
			{

				case "CARE_OF_CITY CARE_OF_PROV":
				
				$cityName = ( trim($objTMW->order->CARE_OF_CITY) != "" ) ? trim($objTMW->order->CARE_OF_CITY).", " : "" ;
				
				$history->status_location = $cityName.trim($objTMW->order->CARE_OF_PROV);
					break;

				case "DESTCITY DESTPROV":
				$cityName = ( trim($objTMW->order->DESTCITY) != "" ) ? trim($objTMW->order->DESTCITY).", " : "" ;
				
				$history->status_location = $cityName.trim($objTMW->order->DESTPROV);
					break;

				case "ORIGCITY ORIGPROV":
				
					$cityName = ( trim($objTMW->order->ORIGCITY) != "" ) ? trim($objTMW->order->ORIGCITY).", " : "" ;
				
				$history->status_location = $cityName.trim($objTMW->order->ORIGPROV);
					break;

				case "Status comment":
				
					if ( substr($history->OS_STAT_COMMENT, 0, 3) == "EDI" )
					{
						$arrOne = explode( "-", $history->OS_STAT_COMMENT );
						$arrTwo = explode( ",", $arrOne[1] );
						$arrThree = explode( " ", $arrTwo[1] );
						$strOUT = $arrTwo[0].", ".$arrThree[1];
						$history->status_location = trim($strOUT);
					}
					else
					{
						$history->status_location = "";						
					}
				
				
					break;
				
				default:
				$history->status_location = "";
			}				
			
			
			$history->status_date = date('F j, Y g:i A', strtotime($history->OS_CHANGED));

			if ( $objForm->flag_api == "0" )
			{
				$history->status_description = ( $objMatrix ) ? $objMatrix->current_state : "";
				$history->bar_state  = ( $objMatrix ) ? $objMatrix->bar_state : "";
				$history->bar_color = strtolower($objMatrix->bar_color); #"green";
			}

			$history->status_exception = "0";








		
			if ( $history->OS_SF_REASON_CODE != "" )
			{

				if ( $objForm->flag_api == "1" )
				{
					
					
					
				}
				else
				{				
					
					$arrWhereReason = array();
					$arrWhereReason[] = "state = 1";
					$arrWhereReason[] = "code = '".$history->OS_SF_REASON_CODE."'";
			
					$whereReason = count( $arrWhereReason ) ? " WHERE ". implode( ' AND ', $arrWhereReason ) : '' ;
									
					$query = "SELECT * FROM htc_nsd_tracking_lu_reason_codes ". $whereReason ;
					$db->setQuery($query);
					$objReasonCodes = $db->loadObject();	
					
					if ( $objReasonCodes )
					{
						

					$history->status_description = (  $objReasonCodes->current_state != "No change" ) ?  $objReasonCodes->current_state : $history->status_description  ;				
					
					$history->status_exception = (  $objReasonCodes->current_state != "No change" || $history->OS_STATUS_CODE == "DISPOSED" ) ?  "1" : $history->status_exception  ;
				
					$history->bar_color = (  $objReasonCodes->current_state != "No change" ) ?  $objReasonCodes->bar_color : "green"  ;

					$history->bar_color = ( $history->OS_STATUS_CODE == "DISPOSED" ) ? "red" : $history->bar_color ;

					$logMessage = "INSIDE | 1.2 | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r objReasonCodes: ".print_r($objReasonCodes, true);
					$logMessage .= "\n\r history: ".print_r($history, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					}



				}
			}
		
			$history->status_engagement = $objMatrix->status_engagement;
		
			switch( $objMatrix->status_engagement )
			{

				case "1":
					$history->suppressed = 0;
					break;
					
				default:
					$history->suppressed = 1;
			}

			$history->suppressed = ( $history->OS_SF_REASON_CODE == "CONTATMPT" ) ? 1 : $history->suppressed ;
			
			
			
			if ( $objForm->flag_api == "1" )
			{

				$arrWhereReasonAPI = array();
				$arrWhereReasonAPI[] = "state = 1";
				$arrWhereReasonAPI[] = "tracking_code = '".$history->OS_STATUS_CODE."'";
				$arrWhereReasonAPI[] = "reason_code = '".$history->OS_SF_REASON_CODE."'";
				$arrWhereReasonAPI[] = "id_level = '".$objTMW->id_level."'";
		
				$whereReasonAPI = count( $arrWhereReasonAPI ) ? " WHERE ". implode( ' AND ', $arrWhereReasonAPI ) : '' ;


				$query = "SELECT * FROM htc_nsd_tracking_api_matrix ". $whereReasonAPI ;
				$db->setQuery($query);
				$objResult = $db->loadObject();
				
				
				if ( $objResult )
				{ 
					$history->api_suppressed = 0;
					$history->api_description = $objResult->description;
					
				}
				else
				{
					$history->api_suppressed = 1;
				}
				

				
			}
			
			
			
			
			
		}


		$logMessage = "INSIDE | 2 | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objTMW->order_history: ".print_r($objTMW->order_history, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $objForm->flag_api == "1" )
		{

			foreach($objTMW->order_history as $elementKey => $element) {
			    foreach($element as $valueKey => $value) {

			        if($valueKey == 'api_suppressed' && $value == '1'){
			            //delete this particular object from the $array
			            unset($objTMW->order_history[$elementKey]);
			        } 
			    }
			}

			$objTMW->order_history = array_values($objTMW->order_history);


			$logMessage = "INSIDE | 3 | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objTMW->order_history: ".print_r($objTMW->order_history, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$arrAPI = array();
			
			foreach( $objTMW->order_history as $objRecord )
			{
				$objNew = new stdClass();
				$objNew->status_code = $objRecord->OS_STATUS_CODE;
				$objNew->reason_code = $objRecord->OS_SF_REASON_CODE;
				$objNew->status_location = $objRecord->status_location;
				$objNew->status_date = $objRecord->status_date;
				$objNew->status_description = $objRecord->api_description;

				
				$arrAPI[] = $objNew;
				
			}

			
			$objTMW->order_history = $arrAPI;


		}

		$logMessage = "INSIDE | 4 | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objTMW->order_history: ".print_r($objTMW->order_history, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$arrReturn = $objTMW->order_history;


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $arrReturn;					
		
		
	}



	// !getMatrixInfo
	public static function getMatrixInfo( $objInput )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getMatrixInfo";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		

		$arrWhere = array();
		$arrWhere[] = "state = 1";
		$arrWhere[] = "delivery_type = '".$objInput->delivery_type."'";
		$arrWhere[] = "order_type = '".$objInput->order_type."'";
		$arrWhere[] = "order_status = '".$objInput->order_status."'";

		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
						
		$query = "SELECT * FROM htc_nsd_tracking_matrix ". $where ;
		$db->setQuery($query);
		$objReturn = $db->loadObject();
		

				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;			
		
	}



	// !getBarMatrixInfo
	public static function getBarMatrixInfo( $objInput )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getBarMatrixInfo";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		

		$arrWhere = array();
		$arrWhere[] = "state = 1";
		$arrWhere[] = "delivery_type = '".$objInput->delivery_type."'";
		$arrWhere[] = "order_type = '".$objInput->order_type."'";
		$arrWhere[] = "visible = 1";

		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
						
		$query = "SELECT bar_state, class, '' as color FROM htc_nsd_tracking_lu_bar_states_by_type ". $where ." order by ordering" ;
		$db->setQuery($query);
		$objListReturn = $db->loadObjectList();
		

				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objListReturn: ".print_r($objListReturn, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objListReturn;			
		
	}


	// !getAPIDisplayDataForOutput
	public static function getAPIDisplayDataForOutput( $objDisplay, $objForm, $objTMW, $objTrackingMatrix )
	{

		$controllerName = "Nsd_trackingController";
		$functionName = "getAPIDataForOutput";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		


				$objDisplay->tracking_number = trim($objTMW->order->BILL_NUMBER);
		
				$objDisplay->order_type = $objTrackingMatrix->order_type;
				
				$objDisplay->current_status = $objTMW->order->CURRENT_STATUS;
		
				#EARTH-633 - make current state duplicate current_status
				$objDisplay->current_state = $objTMW->order->CURRENT_STATUS;
				#$objDisplay->current_state = $objTrackingMatrix->current_state;
				
				
				$objDisplay->delivery_type_description = $objTrackingMatrix->delivery_type_description;
				
		
				// !flag_po_number | po_number		
				
				if ( $objTrackingMatrix->flag_po_number )
				{
	
							$objDisplay->po_number = trim($objTMW->order->TRACE_TYPE_P);
					
				}
				else
				{
					$objDisplay->po_number = "";
				}
	
	
	
				// !flag_ref_1_number | ref_1_number			
				
				if ( $objTrackingMatrix->flag_ref_1_number )
				{
						
							$objDisplay->ref_1_number = trim($objTMW->order->TRACE_TYPE_Q);
					
				}
				else
				{
					$objDisplay->ref_1_number = "";
				}		
	
	
				// !ref_2_number	
		
				$objDisplay->ref_2_number = ( $objTrackingMatrix->flag_ref_2_number ) ? trim($objTMW->order->TRACE_TYPE_W) : "" ;
	
	
	
	
				// !flag_service_level | service_level	
		
				if ( $objTrackingMatrix->flag_service_level == "TRUE"  )
				{
					
					$objServiceLevel = Nsd_trackingController::getServiceLevelInformation( $objTMW );
					
					$objDisplay->service_level = $objServiceLevel->name;
					
					
				}

	
	
	
				// !flag_origin | origin	
		
				if ( $objTrackingMatrix->flag_origin )
				{
					switch( $objTrackingMatrix->origin_location )
					{
						case "ORIGCITY ORIGPROV":
						$objDisplay->origin = trim($objTMW->order->ORIGCITY).", ".trim($objTMW->order->ORIGPROV);
							break;
		
		
						case "ORIGCITY ORIGPROV ORIGPC":
						$objDisplay->origin = trim($objTMW->order->ORIGCITY).", ".trim($objTMW->order->ORIGPROV)." ".trim($objTMW->order->ORIGPC);
							break;
						
						default:
						$objDisplay->origin = "";
					}
				}
		
	
	
	
	
				// !flag_destination | destination	
		
				if ( $objTrackingMatrix->flag_destination )
				{
					switch( $objTrackingMatrix->destination_location )
					{
						case "DESTCITY DESTROV":
						$objDisplay->destination = trim($objTMW->order->DESTCITY).", ".trim($objTMW->order->DESTPROV);
							break;
		
		
						case "DESTCITY DESTROV DESTPC":
						$objDisplay->destination = trim($objTMW->order->DESTCITY).", ".trim($objTMW->order->DESTPROV)." ".trim($objTMW->order->DESTPC);
							break;
						
						default:
						$objDisplay->destination = "";
					}
				}
	
	
	
	
				// !flag_scheduled_delivery_date | scheduled_delivery_date	

				if (  $objTrackingMatrix->order_type  == "Delivery" )
				{
			
					if ( $objTrackingMatrix->flag_scheduled_delivery_date == "TRUE" )
					{
			
						if( $objTMW->order->DELIVERY_APPT_MADE == "False" )
						{
							$objDisplay->scheduled_delivery_date_start = "Not Yet Scheduled";
							$objDisplay->scheduled_delivery_date_end = "";
						}
						else
						{
							
							$delivered_by = ( $objTMW->order->DELIVER_BY != "" ) ? date('m-j-Y-H:i', strtotime($objTMW->order->DELIVER_BY) ) : "";
							$delivered_by_end = ( $objTMW->order->DELIVER_BY_END != "" ) ? date('m-j-Y-H:i', strtotime($objTMW->order->DELIVER_BY_END) ) : "";
							
							$objDisplay->scheduled_delivery_date_start = $delivered_by;
							$objDisplay->scheduled_delivery_date_end = $delivered_by_end;
						}
						
					}
					else
					{
						$objDisplay->scheduled_delivery_date_start = "";
						$objDisplay->scheduled_delivery_date_end = "";
					}
			
				}



				# DELIVERY ETA	
				if (  $objTrackingMatrix->order_type  == "Delivery" )
				{
			
	
						if( $objTMW->order->DELIVERY_APPT_MADE == "True" )
						{
							$objDisplay->delivery_eta = "";

						}
						else
						{
						
							$test_customer = $objTMW->order->CUSTOMER;  //must be 10343
							
							$test_op_code = $objTMW->order->OP_CODE;  //must be DD
							
							$test_site_id = $objTMW->order->SITE_ID;  //must be SITE6

							$objOrderHistory = Nsd_trackingController::getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status = "DEPSHIP" );

							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | delivery ETA";
							$logMessage .= "\n\r test_customer: ".print_r($test_customer, true);
							$logMessage .= "\n\r test_op_code: ".print_r($test_op_code, true);
							$logMessage .= "\n\r test_site_id: ".print_r($test_site_id, true);
							$logMessage .= "\n\r objOrderHistory: ".print_r($objOrderHistory, true);
							
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


							
							$delivered_by = ( $objTMW->order->DELIVER_BY != "" ) ? date('m-j-Y', strtotime($objTMW->order->DELIVER_BY) ) : "";
							$objDisplay->delivery_eta = "";							
							
							
							if( $test_customer == "10343")
							{
								if( $test_site_id == "SITE6" )
								{
									$objDisplay->delivery_eta = $delivered_by;
								}								
							}
							else
							{
						
								if( $test_site_id == "SITE6" && $test_op_code == "DD" && $objOrderHistory->OS_CHANGED != "" )
								{
									$objDisplay->delivery_eta = $delivered_by;
								}
							
							}
						}

			
				}
	
	
	
	
				// !flag_scheduled_pickup_date | scheduled_pickup_date	

				if (  $objTrackingMatrix->order_type  == "Return" )
				{
			
					if ( $objTrackingMatrix->flag_scheduled_pickup_date == "TRUE"  )
					{
		
			
						if( $objTMW->order->PICK_UP_APPT_MADE == "False" )
						{
							$objDisplay->scheduled_pickup_date_start = "Not Yet Scheduled";
							$objDisplay->scheduled_pickup_date_end = "";
							
						}
						else
						{
		
							$pickup_by = ( $objTMW->order->PICK_UP_BY != "" ) ? date('m-j-Y-H:i', strtotime($objTMW->order->PICK_UP_BY) ) : "";
							$pickup_by_end = ( $objTMW->order->PICK_UP_BY_END != "" ) ? date('m-j-Y-H:i', strtotime($objTMW->order->PICK_UP_BY_END) ) : "";
							
							$objDisplay->scheduled_pickup_date_start = $pickup_by;
							$objDisplay->scheduled_pickup_date_end = $pickup_by_end;
						}
						
					}
					else
					{
						$objDisplay->scheduled_pickup_date_start = "";
						$objDisplay->scheduled_pickup_date_end = "";
					}
			
				}
	
	
	
				// !flag_pod_information | pod_information
	
				if ( $objTrackingMatrix->flag_pod_information == "TRUE"  )
				{
		
		
					if ( $objTMW->order->SITE_ID == "SITE6" )
					{

						
						$objOrderHistory = Nsd_trackingController::getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status = "DELIVERED" );
						
						$objDisplay->pod_information = ( $objOrderHistory->OS_STAT_COMMENT != "" && ( trim($objTMW->order->CURRENT_STATUS) == "DELIVERED" || trim($objTMW->order->CURRENT_STATUS) == "COMPLETED" || trim($objTMW->order->CURRENT_STATUS) == "APPRVD" || trim($objTMW->order->CURRENT_STATUS) == "UNAPPRVD" || trim($objTMW->order->CURRENT_STATUS) == "BILLED" || trim($objTMW->order->CURRENT_STATUS) == "PRINTED" || trim($objTMW->order->CURRENT_STATUS) == "ST PRINTED" ) ) ? "Signature on file" : "";
					}
					else
					{
		
						$objOrderHistory = Nsd_trackingModelTrack::getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status = "PICKEDUP" );
	
	
						$arrCStatuses = array();
						$arrCStatuses[] = "APPRVD";
						$arrCStatuses[] = "ARRDLVTERM";
						$arrCStatuses[] = "ARRSHIP";
						$arrCStatuses[] = "ARRTERM";
						$arrCStatuses[] = "BILLED";
						$arrCStatuses[] = "COMPLETED";
						$arrCStatuses[] = "DELIVERED";
						$arrCStatuses[] = "DEPDLVTERM";
						$arrCStatuses[] = "DEPSHIP";
						$arrCStatuses[] = "DEPTERM";
						$arrCStatuses[] = "DISPOSED";
						$arrCStatuses[] = "INTRANSIT";
						$arrCStatuses[] = "LHAPPTCONS";
						$arrCStatuses[] = "LHAPPTSHIP";
						$arrCStatuses[] = "LHETA";
						$arrCStatuses[] = "LTLAVAIL";
						$arrCStatuses[] = "PICKEDUP";
						$arrCStatuses[] = "PRINTED";
						$arrCStatuses[] = "SHPTACK";
						$arrCStatuses[] = "ST PRINTED";
						$arrCStatuses[] = "TENDER";
						$arrCStatuses[] = "UNAPPRVD";
	
					
						$objDisplay->pod_information = ( $objOrderHistory->OS_STAT_COMMENT != "" && ( in_array( trim($objTMW->order->CURRENT_STATUS), $arrCStatuses ) ) ) ? "Signature on file" : "";
	
						
						
						
					}
		
					
				}
				else
				{
					$objDisplay->pod_information = "";
				}

	
				$objOrderAllowSchedule = new stdClass();
	            $objOrderAllowSchedule->customer_account_code = $objTMW->order->CUSTOMER;
	            $objOrderAllowSchedule->origid = $objTMW->order->ORIGIN;
	            $objOrderAllowSchedule->service_level = $objTMW->order->SERVICE_LEVEL;
	            $objOrderAllowSchedule->op_code = $objTMW->order->OP_CODE;


				$objReturnAllow = Nsd_schedulingController::determine_allow_schedule( $objOrderAllowSchedule );

				if ( $objReturnAllow->allowSchedule == "1" )
				{
		
		
					$objInput = new stdClass();
					$objInput->trace_number = trim($objTMW->order->BILL_NUMBER);
					$objInput->zip_code = $objTMW->order->DESTPC;
					$objCanSchedule = Shipnsd_apiController::determineCanSchedule( $objInput );
					
					#$objCanSchedule = Nsd_schedulingController::determine_can_schedule( $objInput );
		
					if( $objCanSchedule->canSchedule == "1" )
					{
						
						$objDisplay->canSchedule = 1;
						
					}
					else
					{
						
						$objDisplay->canSchedule = 0;
					}
				}
				else
				{
				        $objDisplay->canSchedule = 0;
				}


				// !order history
			
				$arrOrderHistory = Nsd_trackingController::getArrOrderHistory( $objForm, $objTMW, $objTrackingMatrix );
		
				$objDisplay->order_history = $arrOrderHistory;


				$isFirstInArray = "1";
				foreach( $arrOrderHistory as $history )
				{
	
					if ( $isFirstInArray == "1" && $history->suppressed == "0")
					{
						
						if ( $history->status_exception == "1" )
						{
							$objDisplay->current_state = $history->status_description;
							$objDisplay->current_state_exception = "1";
							
						}
						
						$isFirstInArray = "0";
						
					}			
				}
			

				# if current status is an accounting status, replace status with first status in order history
				# ticket: Earth-579; 02-19-2020
				
				$arrAccountingStatus = array();
				$arrAccountingStatus[] = "APPRVD";
				$arrAccountingStatus[] = "BILLED";
				$arrAccountingStatus[] = "PRINTED";
				$arrAccountingStatus[] = "ST PRINTED";
				$arrAccountingStatus[] = "UNAPPRVD";


				if ( in_array( $objTMW->order->CURRENT_STATUS, $arrAccountingStatus ) )
				{
					
					$objDisplay->current_status = $arrOrderHistory[0]->status_code;
					
					#EARTH-633 - make current state duplicate current_status
					$objDisplay->current_state = $objDisplay->current_status;
				}






		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objDisplay;

		
	}







	// !TRACE PAGE FUNCTIONS

	public static function insertOrderDetailsFromTracePage( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_function_template

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_function_template

		$controllerName = "Nsd_trackingController";
		$functionName = "insertOrderDetailsFromTracePage";
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$postData = JRequest::get('post');

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;	
		$logMessage .= "\n\r postData: ".print_r($postData, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objForm = new stdClass();
		
		foreach($postData as $key => $value)
		{		
			
			if ( is_string($value) )
			{
				$objForm->$key = trim($value);
			}
		}
		
		$arrNotesCallType = $_POST['notesCallType'];

		$objForm->notesCallType = implode(', ', $arrNotesCallType);
		
		switch( $objForm->notesCallerIdentifier )
		{
			
			case "Client":
			case "Terminal":
				$objForm->histStatusCode = "CLIENTCALL";
				break;

			case "Customer":
				$objForm->histStatusCode = "CSTMRCALL";
				break;
				
			default:
				$objForm->histStatusCode = "CSTMRCALL";
				break;	
			
		}		
		
		
		$objForm->histComment = $objForm->notesCallType;
		
		$objForm->notesNote = $objForm->notesCallType . " | " .$objForm->notesNote;
		
		$histUpdatedByEmail = $objForm->histUpdatedByEmail; 

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | BEACON ALPHA";
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

		$objReturn = Nsd_workatoController::insertTruckmateOrderHistory( $objForm );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$msg = "";

		if ( $objReturn->error == "1" )
		{
			$msg = ( $objReturn->error_code == "422"  ) ? "There was an error saving your information.  Please try again." : $objReturn->user_messsage ; 
		}


		#$link = JRoute::_('index.php?option=com_nsd_scheduling&view=complete&Itemid='.$itemid_complete);		
		#$link = JRoute::_('index.php?option=com_nsd_tracking&view=trace&Itemid=642&t='.$objForm->trace);	
		#$link = rtrim(JURI::root(),'/').'trace/?t='.$objForm->trace;		
		#$link = JURI::root().'trace?t='.$objForm->trace.'&s=1';		
		$link = '/trace?a='.$histUpdatedByEmail.'&t='.$objForm->trace.'&r=1&s=1';		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$app->redirect($link, $msg);
		
	}


	public static function updateConsigneeFromTracePage( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=updateConsigneeFromTracePage

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=updateConsigneeFromTracePage

		$controllerName = "Nsd_trackingController";
		$functionName = "updateConsigneeFromTracePage";
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$postData = JRequest::get('post');

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;	
		$logMessage .= "\n\r postData: ".print_r($postData, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objForm = new stdClass();
		
		foreach($postData as $key => $value)
		{		
			
			if ( is_string($value) )
			{
				$objForm->$key = trim($value);
			}
		}
		
		$trace = $objForm->trace;
		
		$histUpdatedByEmail = $objForm->histUpdatedByEmail;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

		$objReturn = Nsd_workatoController::updateTruckmateOrderConsignee( $objForm );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$msg = "";

		if ( $objReturn->error == "1" )
		{
			$msg = ( $objReturn->error_code == "422"  ) ? "There was an error saving your information.  Please try again." : $objReturn->user_messsage ; 
		}

		
		#$link = JRoute::_('index.php?option=com_nsd_scheduling&view=complete&Itemid='.$itemid_complete);		
		#$link = JRoute::_('index.php?option=com_nsd_tracking&view=trace&Itemid=642&t='.$objForm->trace);	
		#$link = rtrim(JURI::root(),'/').'trace/?t='.$objForm->trace;		
		#$link = JURI::root().'trace?t='.$objForm->trace.'&s=1';		
		$link = '/trace?a='.$histUpdatedByEmail.'&t='.$trace.'&r=1&s=1';		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r link: ".print_r($link, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }




		$app->redirect($link, $msg);
		
	}

	public static function updateShipperFromTracePage( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=updateShipperFromTracePage

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=updateShipperFromTracePage

		$controllerName = "Nsd_trackingController";
		$functionName = "updateShipperFromTracePage";
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "metalake_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$postData = JRequest::get('post');

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;	
		$logMessage .= "\n\r postData: ".print_r($postData, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objForm = new stdClass();
		
		foreach($postData as $key => $value)
		{		
			
			if ( is_string($value) )
			{
				$objForm->$key = trim($value);
			}
		}
		
		$trace = $objForm->trace;
		
		$histUpdatedByEmail = $objForm->histUpdatedByEmail;
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		

		$objReturn = Nsd_workatoController::updateTruckmateOrderShipper( $objForm );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$msg = "";

		if ( $objReturn->error == "1" )
		{
			$msg = ( $objReturn->error_code == "422"  ) ? "There was an error saving your information.  Please try again." : $objReturn->user_messsage ; 
		}

		
		#$link = JRoute::_('index.php?option=com_nsd_scheduling&view=complete&Itemid='.$itemid_complete);		
		#$link = JRoute::_('index.php?option=com_nsd_tracking&view=trace&Itemid=642&t='.$objForm->trace);	
		#$link = rtrim(JURI::root(),'/').'trace/?t='.$objForm->trace;		
		#$link = JURI::root().'trace?t='.$objForm->trace.'&s=1';		
		$link = '/trace?a='.$histUpdatedByEmail.'&t='.$trace.'&r=1&s=1';		


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$app->redirect($link, $msg);
		
	}



	// !CONTACTLESS POD PAGE FUNCTIONS

	public static function submitPOD ( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_function_template

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_function_template

		$className = "Nsd_trackingController";
		$functionName = "submitPOD";
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



			$jinput = JFactory::getApplication()->input;

			$postData = $jinput->post->getArray(array());
	
			$logMessage = "INSIDE 1 | ".$controllerName." | ".$functionName;	
			$logMessage .= "\n\r postData: ".print_r($postData, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
	
			$objForm = new stdClass();
			
			foreach($postData as $key => $value)
			{		
				
				if ( is_string($value) )
				{
					$objForm->$key = trim($value);
				}
			}

			
			$logMessage = "INSIDE 2 | ".$className." | ".$functionName;
			$logMessage .= "\n\r objForm: ".print_r($objForm, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


			#session
			$objTMW = MetalakeHelperCore::getSessObj( "objSessPODObjTMW" );

			$logMessage = "INSIDE 3 | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objTMW: ".print_r($objTMW, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	



			$objReceipt = Nsd_trackingController::prepareReceipt( $objTMW, $objForm );
			$objReturnReceipt = Nsd_trackingController::generateReceipt( $objReceipt );

			$logMessage = "INSIDE 4 | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objReturnReceipt: ".print_r($objReturnReceipt, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


			
			
			$objEmail = new stdClass();
			$objEmail->orderNumber = $objTMW->order->BILL_NUMBER;
			$objEmail->detailLineId = $objTMW->order->DETAIL_LINE_ID;
			$objEmail->consigneeInfo = ( $objTMW->order->SITE_ID == "SITE5" ) ? $objTMW->order->ORIGNAME : $objTMW->order->DESTNAME;
			$objEmail->serviceLevel = $objTMW->order->SERVICE_LEVEL;
			
			
			$objEmail->propertyDamage = $objForm->shipInstructions_24;
			$objEmail->deliveryFeedbackComment = $objForm->deliveryFeedbackComment;
			$objEmail->deliveryAccepted = $objForm->deliveryAccepted;
			
			$objEmail->orderDetailsAll = $objTMW->order_details;			

			$arrOrderDetailsAll = array();
			$arrOrderDetailExceptions = array();
			
			
			foreach( $objTMW->order_details as $detail )
			{
				
				$tmp1 = "productReceived_".$detail->SEQUENCE;
				$productReceived = $objForm->$tmp1;	

				$tmp2 = "productDamaged_".$detail->SEQUENCE;
				$productDamaged = $objForm->$tmp2;	

				$tmp3 = "packagingDamaged_".$detail->SEQUENCE;
				$packagingDamaged = $objForm->$tmp3;					

				$tmp4 = "comment_".$detail->SEQUENCE;				
				$packageConditionComment = $objForm->$tmp4;					
	
				$detail->productReceived = $productReceived;
				$detail->productDamaged = $productDamaged;
				$detail->packagingDamaged = $packagingDamaged;			
				$detail->packageConditionComment = $packageConditionComment;			

				$arrOrderDetailsAll[] = $detail;
				
				if ( $productReceived == "NO" || $productDamaged == "YES" || $packagingDamaged == "YES" )
				{
					$arrOrderDetailsExceptions[] = $detail;
				}
			}
			
			$objEmail->orderDetailsAll = $arrOrderDetailsAll;
			$objEmail->orderDetailsExceptions = $arrOrderDetailsExceptions;
			

						
			#via workato: send PDF to synergize@nonstopdelivery.com

			$objEmail->messageType = "SYNERGIZE";
			$objEmail->subjectLine = trim($objTMW->order->BILL_NUMBER)."_WEBPOD";
			$objEmail->pdfName = $objReturnReceipt->receipt_file_name;
			
			$receipt_file_full_path = $objReturnReceipt->receipt_file_full_path;
			$objEmail->pdfFile = JURI::root()."media/com_nsd_tracking/contactlesspod/".$objReturnReceipt->receipt_file_name;
			
			

			$logMessage = "INSIDE 5 | ".$className." | ".$functionName;
			$logMessage .= "\n\r objEmail: ".print_r($objEmail, true);
			$logMessage .= "\n\r json objEmail: ".print_r(json_encode($objEmail), true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


			$objReturn = Nsd_workatoController::sendEmailMessagePOD( $objEmail );

			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			/*
			After consignee submits a web form for delivery, 
			if there is an exception - email needs to be sent to OSD and/or Property Damage department for review.

			If below conditions are selected on the web form - email should be sent to: OSD@nonstopdelivery.com
			==If any one of the highlighted responses or all, are checked - email should go to OSD:
			*/
			
			
			if ( count( $arrOrderDetailsExceptions ) > 0 || $objForm->deliveryAccepted == "NO" || $objForm->deliveryAccepted == "PARTIAL" )
			{
				$objEmail->messageType = "OSD";
				$objEmail->subjectLine = "Web POD Submitted with Exceptions for Order: ".$objTMW->order->BILL_NUMBER;				
				$objEmail->pdfName = "";
				$objEmail->pdfFile = "";

				$logMessage = "INSIDE 6 | ".$className." | ".$functionName;
				$logMessage .= "\n\r objEmail: ".print_r($objEmail, true);
				$logMessage .= "\n\r json objEmail: ".print_r(json_encode($objEmail), true);				
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				$objReturn = Nsd_workatoController::sendEmailMessagePOD( $objEmail );
	
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			}

			
			
			
			
			/*
			==If yes is selected for question in Delivery Feedback: Was there any damage caused to your home or property while the NSD driver team was onsite? - email should be sent to: propertydamage@nonstopdelivery.com
			Email will be going out per order #, not a list of orders

			*/

			if ( $objForm->shipInstructions_24 == "YES" )
			{
				$objEmail->messageType = "DAMAGED";
				$objEmail->subjectLine = "Web POD Submitted with Property Damage for Order: ".$objTMW->order->BILL_NUMBER;	
				$objEmail->pdfName = "";
				$objEmail->pdfFile = "";
				
				$logMessage = "INSIDE 7 | ".$className." | ".$functionName;
				$logMessage .= "\n\r objEmail: ".print_r($objEmail, true);
				$logMessage .= "\n\r json objEmail: ".print_r(json_encode($objEmail), true);				
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				


				$objReturn = Nsd_workatoController::sendEmailMessagePOD( $objEmail );
	
				$logMessage = "INSIDE | ".$className." | ".$functionName;
				$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
			}
			


			
		
			/*
			SUBMIT TO WORKATO
			https://www.workato.com/recipes/943989-new-status-odrstat-insert#jobs
			Insert below into TM:
			Updated By
			Status AUDIT
			Status Comment: Web POD form Submitted
			Reason Code: WEBPOD
			*Detail Line ID is required field
			*Status is required field
			*/


			$objAudit = new stdClass();
			$objAudit->statusCode = "AUDIT";
			$objAudit->comment = "Web POD form Submitted";
			$objAudit->reasonCode = "WEBPOD";
			$objAudit->detailLineId = $objTMW->order->DETAIL_LINE_ID;
			$objAudit->updatedBy = "WEBPOD";


			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objAudit: ".print_r($objAudit, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	


			$objReturn = Nsd_trackingController::insertOrderStatus( $objAudit );



			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objAudit: ".print_r($objAudit, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$link = '/contactlesspod?t='.$objForm->trace_number.'&z='.$objForm->trace_zipcode.'&r=1';
		$link = "/contactlesspod-confirm";
		$link = "/deliveryconfirmation-confirm";
		
		$returnMessage = "";
		$app->redirect($link, $returnMessage);

		
	}



	public static function prepareReceipt( $objTMW, $objForm )
	{

		#manual
		#https://mpdf.github.io/
		
		
		$className = "Nsd_trackingController";
		$functionName = "prepareReceipt";

		$db 	= JFactory::getDBO();

		$app = JFactory::getApplication();

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	
	
		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		


		$objReceipt = new stdClass();

		$objReceipt->receipt_file_name = "WEBPOD_".$objForm->trace_number."_".date('YmdHis').".pdf";
				
					
		$objReceipt->receipt_body = "";
		#$objReceipt->receipt_body .= "<img src='/templates/nonstopdelivery/images/logo.png' width='20%'>";
		$objReceipt->receipt_body .= "<img src='templates/nonstopdelivery/images/logo.png' width='20%'>";
		$objReceipt->receipt_body .= "<div style='font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
		$objReceipt->receipt_body .= "<div style='text-align: center; font-family: sans-serif; font-size: 14pt; color: #000; font-weight: bold;'>Delivery Confirmation</div>";

		
		$objReceipt->receipt_body .= "<h2>Order Number: ".$objForm->trace_number."</h2>";

		$objReceipt->receipt_body .= "<table style='width:100%; font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
		$objReceipt->receipt_body .= "<thead>";
		$objReceipt->receipt_body .= "<tr>";
		$objReceipt->receipt_body .= "<th style='width:34%;text-align:left;'></th>";
		$objReceipt->receipt_body .= "<th style='width:33%;text-align:left;'></th>";
		$objReceipt->receipt_body .= "<th style='width:33%;text-align:left;'></th>";
		$objReceipt->receipt_body .= "</tr>";
		$objReceipt->receipt_body .= "</thead>";
		$objReceipt->receipt_body .= "<tbody>";
		$objReceipt->receipt_body .= "<tr>";

		$objReceipt->receipt_body .= "<td>";
		$objReceipt->receipt_body .= "<b>Origin (Shipper)</b><br />";	

		if ( $objTMW->order->SITE_ID == "SITE5" )
		{
			#return
			$objReceipt->receipt_body .= $objTMW->order->ORIGNAME."<br />";
			$objReceipt->receipt_body .= $objTMW->order->ORIGADDR1."<br />";
			$objReceipt->receipt_body .= $objTMW->order->ORIGADDR2 ? $objTMW->order->ORIGADDR2."<br />" : "";
			$objReceipt->receipt_body .= $objTMW->order->ORIGCITY.", ".$objTMW->order->ORIGPROV." ".$objTMW->order->ORIGPC;	
		}
		else
		{
			#delivery
			$objReceipt->receipt_body .= $objTMW->order->CALLNAME;			
		}

		$objReceipt->receipt_body .= "</td>";


		$objReceipt->receipt_body .= "<td>";
		$objReceipt->receipt_body .= "<b>Destination (Consignee)</b><br />";
		
		if ( $objTMW->order->SITE_ID == "SITE5" )
		{
			#return
			$objReceipt->receipt_body .= "C/O NONSTOP DELIVERY<br />";
			$objReceipt->receipt_body .= $objTMW->order->CARE_OF_ADDR1."<br />";
			$objReceipt->receipt_body .= $objTMW->order->CARE_OF_ADDR2 ? $objTMW->order->CARE_OF_ADDR2."<br />" : "";
			$objReceipt->receipt_body .= $objTMW->order->CARE_OF_CITY.", ".$objTMW->order->CARE_OF_PROV." ".$objTMW->order->CARE_OF_PC;
			
		}
		else
		{
			#delivery	
			$objReceipt->receipt_body .= $objTMW->order->DESTNAME."<br />";
			$objReceipt->receipt_body .= $objTMW->order->DESTADDR1."<br />";
			$objReceipt->receipt_body .= $objTMW->order->DESTADDR2 ? $objTMW->order->DESTADDR2."<br />" : "";
			$objReceipt->receipt_body .= $objTMW->order->DESTCITY.", ".$objTMW->order->DESTPROV." ".$objTMW->order->DESTPC;
			
		}
		
		$objReceipt->receipt_body .= "</td>";
	
		$objReceipt->receipt_body .= "<td>";
		$objReceipt->receipt_body .= "<b>Service Level</b><br />";
		$objReceipt->receipt_body .= $objTMW->order->SERVICE_LEVEL."<br />";
		$objReceipt->receipt_body .= "<br />";
		$objReceipt->receipt_body .= "<b>Customer Reference</b><br />";
		$objReceipt->receipt_body .= $objTMW->trace_types[array_search("Reference #1", array_column($objTMW->trace_types, 'TRACE_TYPE'))]->TRACE_NUMBER;
		$objReceipt->receipt_body .= "</td>";
	
		
		$objReceipt->receipt_body .= "</tr>";
		$objReceipt->receipt_body .= "</tbody>";
		$objReceipt->receipt_body .= "</table>";

		$objReceipt->receipt_body .= "<h2>Order Information</h2>";

		$objReceipt->receipt_body .= "<table style='width:100%; font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
		$objReceipt->receipt_body .= "<thead>";
		$objReceipt->receipt_body .= "<tr>";
		$objReceipt->receipt_body .= "<th style='width:50%;text-align:left;'>DESCRIPTION</th>";
		$objReceipt->receipt_body .= "<th style='width:4%;text-align:left;'>QTY</th>";
		$objReceipt->receipt_body .= "<th style='width:10%;text-align:left;'>WEIGHT</th>";
		$objReceipt->receipt_body .= "<th style='width:12%;text-align:left;'>PRODUCT RECEIVED</th>";
		$objReceipt->receipt_body .= "<th style='width:12%;text-align:left;'>PRODUCT DAMAGED</th>";
		$objReceipt->receipt_body .= "<th style='width:12%;text-align:left;'>PACKAGING DAMAGED</th>";
		$objReceipt->receipt_body .= "</tr>";
		$objReceipt->receipt_body .= "</thead>";
		$objReceipt->receipt_body .= "<tbody>";

		foreach ($objTMW->order_details as $detail) :

			$objReceipt->receipt_body .= "<tr>";
	
			$objReceipt->receipt_body .= "<td>";
			$objReceipt->receipt_body .= $detail->DESCRIPTION;	
			$objReceipt->receipt_body .= "</td>";
	
			$objReceipt->receipt_body .= "<td>";
			$objReceipt->receipt_body .= $detail->PIECES;	
			$objReceipt->receipt_body .= "</td>";
	
			$objReceipt->receipt_body .= "<td>";
			$objReceipt->receipt_body .= $detail->WEIGHT;	
			$objReceipt->receipt_body .= "</td>";
	
			$objReceipt->receipt_body .= "<td>";
				$tmp1 = "productReceived_".$detail->SEQUENCE;
			$objReceipt->receipt_body .= $objForm->$tmp1;	
			$objReceipt->receipt_body .= "</td>";
	
			$objReceipt->receipt_body .= "<td>";
				$tmp2 = "productDamaged_".$detail->SEQUENCE;
			$objReceipt->receipt_body .= $objForm->$tmp2;	
			$objReceipt->receipt_body .= "</td>";
	
			$objReceipt->receipt_body .= "<td>";
				$tmp3 = "packagingDamaged_".$detail->SEQUENCE;
			$objReceipt->receipt_body .= $objForm->$tmp3;	
			$objReceipt->receipt_body .= "</td>";
			$objReceipt->receipt_body .= "</tr>";

			$tmp4 = "comment_".$detail->SEQUENCE;	
			if ( $objForm->$tmp4 != "" )
			{
				$objReceipt->receipt_body .= "<tr>";
				$objReceipt->receipt_body .= "<td  colspan='6'>";
				$objReceipt->receipt_body .= "Product Condition or Comment: ".$objForm->$tmp4;	
				$objReceipt->receipt_body .= "</td>";
				$objReceipt->receipt_body .= "</tr>";
			}

		endforeach;
			


		$objReceipt->receipt_body .= "</tbody>";
		$objReceipt->receipt_body .= "</table>";


		$objReceipt->receipt_body .= "<h2>Delivery Feedback</h2>";

		$objReceipt->receipt_body .= "<table style='width:100%; font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
		$objReceipt->receipt_body .= "<thead>";
		$objReceipt->receipt_body .= "<tr>";
		$objReceipt->receipt_body .= "<th style='width:70%;text-align:left;'></th>";
		$objReceipt->receipt_body .= "<th style='width:30%;text-align:left;'></th>";
		$objReceipt->receipt_body .= "</tr>";
		$objReceipt->receipt_body .= "</thead>";
		$objReceipt->receipt_body .= "<tbody>";

		foreach ( $objTMW->shipping_instructions_pod as $instructions ) :

			$tmp5 = "shipInstructions_".$instructions->instructionID;
			$objReceipt->receipt_body .= "<tr>";
			$objReceipt->receipt_body .= "<td>".strtoupper($instructions->shortDescriptionView)."</td>";
			$objReceipt->receipt_body .= "<td>".$objForm->$tmp5."</td>";
			$objReceipt->receipt_body .= "</tr>";

		endforeach;

		$objReceipt->receipt_body .= "</tbody>";
		$objReceipt->receipt_body .= "</table>";


		$objReceipt->receipt_body .= "<p><b>Please place any comments here, including any notation of product condition, damage, or refusal.</b>";
		$objReceipt->receipt_body .= "<br />".$objForm->deliveryFeedbackComment;
		$objReceipt->receipt_body .= "</p>";


		$objReceipt->receipt_body .= "<h2>Customer Delivery Acceptance</h2>";
		$objReceipt->receipt_body .= "<p>Delivery Accepted: ".$objForm->deliveryAccepted."</p>";
		
		
		$objReceipt->receipt_body .= "<p>By typing your name below you are electronically signing this document.";
		$objReceipt->receipt_body .= "<br>Signed: ".$objForm->print_name;
		$objReceipt->receipt_body .= "<br>Date: ".date('F j, Y')."</p>";

		
		$objReceipt->receipt_body .= "</div>";



		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objReceipt: ".print_r($objReceipt, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReceipt;
		
	}




	public static function generateReceipt( $objReceipt )
	{

		#manual
		#https://mpdf.github.io/
		
		
		$className = "Nsd_trackingController";
		$functionName = "generateReceipt";

		$db 	= JFactory::getDBO();

		$app = JFactory::getApplication();

		$objParams = new stdClass();

		$params         						= $app->getParams();
		$objParams->path_to_pod_directory	= $params->get('path_to_pod_directory');


		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	
	
		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objParams: ".print_r($objParams, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$objReturn = new stdClass();


		$mpdf = new \Mpdf\Mpdf(
			[
			    'margin_left' => 5,
			    'margin_right' => 5,
			    'margin_top' => 5,
			    'margin_bottom' => 5,
			    'margin_header' => 5,
			    'margin_footer' => 5,
			]					
		);
		
		#$mpdf->SetHTMLHeader('');				
		
		$mpdf->WriteHTML($objReceipt->receipt_body);
		
		$objReturn->receipt_file_name = $objReceipt->receipt_file_name;
		$objReturn->receipt_file_full_path = $objParams->path_to_pod_directory."/".$objReceipt->receipt_file_name;

		$mpdf->Output( $objReturn->receipt_file_full_path, \Mpdf\Output\Destination::FILE );			

		$objReturn->error = "0";


		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r fullpath: ".print_r($fullpath, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




/*
		$logPath = $objParams->path_to_pod_directory."/";
		
		$numMinutes = 24 * 60 * 2;  // hours * minutes * days
		
		$command = "find ".$logPath."*.pdf -mmin +".$numMinutes." -type f -exec rm -f {} + 2>&1" ;  //60 minutes = 1 hours

		$logMessage = "INSIDE C | ".$className." | ".$functionName;	
		$logMessage .= "\n\r command: ".print_r($command, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
		#exec( $command, $output, $return_var );	

		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r command: ".print_r($command, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
*/





		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
		
	}




	public static function insertOrderStatus( $objData )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_function_template

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_function_template

		$className = "Nsd_trackingController";
		$functionName = "insertOrderStatus";
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$logMessage = "INSIDE | ".$className." | ".$functionName;	
		$logMessage .= "\n\r objData: ".print_r($objData, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objReturn = Nsd_workatoController::insertOrderStatus( $objData );

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}
	
	
	public static function determineOrderStatusValid( $objTMW, $arrStatus )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=determineOrderStatusValid

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=determineOrderStatusValid

		$className = "Nsd_trackingController";
		$functionName = "determineOrderStatusValid";
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objTMW: ".print_r( $objTMW, true );
		$logMessage .= "\n\r arrStatus: ".print_r( $arrStatus, true );
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
		$countExists = 0;
		
		foreach( $arrStatus as $order_status )
		{

			$objHistory = Nsd_truckmateController::getOrderHistoryData( $objorderHistory=$objTMW->order_history, $order_status );
	
			
			$countExists = ( $objHistory->history_exists == "1" ) ?  $countExists + 1 : $countExists ;


			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objHistory: ".print_r($objHistory, true);
			$logMessage .= "\n\r countExists: ".print_r($countExists, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		}


		$flagOFD = ( $objTMW->order->CURRENT_STATUS == "OFD" )  ? 1 : 0 ;
		$flagOFP = ( $objTMW->order->CURRENT_STATUS == "OFP" )  ? 1 : 0 ;
		$flagExists = ( $countExists > 0 ) ? 1 : 0 ;

		$sumFlags = $flagOFD + $flagOFP + $flagExists;

		$objReturn = new stdClass();
		$objReturn->order_status_vaild = ( $sumFlags > 0 ) ? 1 : 0 ;


		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objHistory: ".print_r($objHistory, true);
		$logMessage .= "\n\r CURRENT_STATUS: ".print_r($objTMW->order->CURRENT_STATUS, true);
		$logMessage .= "\n\r flagOFD: ".print_r($flagOFD, true);
		$logMessage .= "\n\r flagOFP: ".print_r($flagOFP, true);
		$logMessage .= "\n\r countExists: ".print_r($countExists, true);
		$logMessage .= "\n\r flagExists: ".print_r($flagExists, true);
		$logMessage .= "\n\r sumFlags: ".print_r($sumFlags, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}


	public static function determineEarliestDateOfOrderStatus( $objTMW, $arrStatus )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=determineEarliestDateOfOrderStatus

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=determineEarliestDateOfOrderStatus

		$className = "Nsd_trackingController";
		$functionName = "determineEarliestDateOfOrderStatus";
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$className." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r objTMW: ".print_r( $objTMW, true );
		$logMessage .= "\n\r arrStatus: ".print_r( $arrStatus, true );
		$logMessage .= "\n\r ";
		#if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		
		$countExists = 0;
		$arrDates = array();
		
		foreach( $arrStatus as $order_status )
		{

			$objHistory = Nsd_truckmateController::getOrderHistoryData( $objorderHistory=$objTMW->order_history, $order_status );
	

			$countExists = ( $objHistory->history_exists == "1" ) ?  $countExists + 1 : $countExists ;

			if ( $objHistory->history_exists == "1"  ) 
			{
				$arrDates[] = $objHistory->OS_INS_DATE;
			}



			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objHistory: ".print_r($objHistory, true);
			$logMessage .= "\n\r countExists: ".print_r($countExists, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		}



		$maxUnix = max(array_map('strtotime', $arrDates));
		$maxDate = date("Y-m-d H:i:s", $maxUnix );

		$minUnix = min(array_map('strtotime', $arrDates));
		$minDate = date("Y-m-d H:i:s", $minUnix );


		#User can fill out the form up to 48 hours after the inserted date of first DELIVERED, COMPLETED, DISPOSED, PICKEDUP status on the order, otherwise - the message will display: Your delivery/pickup was completed more than 48 hours ago, the feedback form is no longer available.

		$now = time();
		
		$allowableTime = 60 * 60 * 48;  // seconds * miniutes * hours = 172800 seconds
		
		$timeExpires = $minUnix + $allowableTime;
		
		$confirmationEligible = ( $now < $timeExpires ) ? 1 : 0 ;   


		$objReturn = new stdClass();
		$objReturn->order_status_exists = ( $countExists > 0 ) ? 1 : 0 ;
		$objReturn->earliest_datetime_unix = ( $minUnix > 0 ) ? $minUnix : "" ;
		$objReturn->earliest_datetime_date = ( $minDate != "" ) ? $minDate : "" ;	
		$objReturn->confirmation_eligible = $confirmationEligible ;				



		$logMessage = "INSIDE | ".$className." | ".$functionName;
		$logMessage .= "\n\r arrDates: ".print_r($arrDates, true);
		$logMessage .= "\n\r maxDate: ".print_r($maxDate, true);
		$logMessage .= "\n\r minDate: ".print_r($minDate, true);
		
		$logMessage .= "\n\r now: ".print_r($now, true);
		$logMessage .= "\n\r now view: ".print_r(date("Y-m-d H:i:s", $now), true);
		$logMessage .= "\n\r allowableTime: ".print_r($allowableTime, true);
		$logMessage .= "\n\r timeExpires: ".print_r($timeExpires, true);
		$logMessage .= "\n\r timeExpires view: ".print_r(date("Y-m-d H:i:s", $timeExpires), true);
		$logMessage .= "\n\r confirmationEligible: ".print_r($confirmationEligible, true);
		
		
		
		
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}


	// !===== ROCKHOPPER
	
	// !getRockhopper
	public static function getRockhopper( $objForm )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=getRockhopper

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=getRockhopper

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getRockhopper";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objRockhopper = new stdClass();


		$trace_number = $objForm->trace_number;

	    $url = 'http://nstracking.nonstopdelivery.com/api/rest/nsdws/tracking/ws-track-shipment/' . $trace_number;
	    
	    
	    
	
	    $response = file_get_contents($url);

		$body = simplexml_load_string($response);

		$tracking_number = (string)$body->BasicInformation->TrackingNumber;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r url: ".print_r($url, true);
		#$logMessage .= "\n\r response: ".print_r($response, true);
		#$logMessage .= "\n\r body: ".print_r($body, true);
		$logMessage .= "\n\r TrackingNumber: ".print_r($tracking_number, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $trace_number == $tracking_number )
		{

			$objRockhopper->tracking_number = $tracking_number;


			// Type
			$type = Nsd_trackingModelTrack::nsd_tracking_type($body->BasicInformation->IsReturnService, $body->BasicInformation->IsLonghaul); // String
			
			$objRockhopper->type = $type;


			// State
			$state = (string) $body->BasicInformation->TabState;


			// Phase (Open/Scheduled/Closed)
			$phase = Nsd_trackingModelTrack::nsd_tracking_phase($type, $state); // String

			$objRockhopper->phase = $phase;


		    // Details
		    $tracking_number = (string) $body->BasicInformation->TrackingNumber;
		    $details = array();
		    
		    if ( ! empty($body->BasicInformation->Ref1) ) {
		        $details[(string) $body->BasicInformation->Ref1Description] = (string) $body->BasicInformation->Ref1;
		    }
		    if ( ! empty($body->BasicInformation->Ref2) ) {
		        $details[(string) $body->BasicInformation->Ref2Description] = (string) $body->BasicInformation->Ref2;
		    }
		    $objRockhopper->details = $details;
		    
		    
		    $service = Nsd_trackingModelTrack::nsd_tracking_service($body->BasicInformation->Service, 'Unknown'); // String
		    $service_descriptions = Nsd_trackingModelTrack::nsd_tracking_service_descriptions($service); // Array

			$objRockhopper->service = $service;
			$objRockhopper->service_descriptions = $service_descriptions;


		    // History
		    $history = array();
		    if ( isset($body->Events->event) ) {
		        foreach ( $body->Events->event as $event ) {
		            $history[] = Nsd_trackingModelTrack::nsd_tracking_event($event); // Array
		        }
		        $history = Nsd_trackingModelTrack::nsd_tracking_history($history); // Sorted/Filtered Array
		    }

			foreach ( $history as $key => $eventHistory  )
			{
				
				$history[$key]['activityClass'] = Nsd_trackingModelTrack::nsd_tracking_class($eventHistory);
	
			}
			
			$objRockhopper->history = $history;			


		    // Stops
		    $stops = array();
		    if ( isset($body->Stops->stop) ) {
		        foreach ( $body->Stops->stop as $stop ) {
		            $stop = Nsd_trackingModelTrack::nsd_tracking_stop($stop); // Array
		            $stops[$stop['sequence']] = $stop; // Indexed by sequence (1,2,3)
		        }
		    }
		    
			$objRockhopper->stops = $stops;

			$objRockhopper->stops_1_city_clean = Nsd_trackingModelTrack::nsd_tracking_city($objRockhopper->stops[1]['city']);
			$objRockhopper->stops_3_city_clean = Nsd_trackingModelTrack::nsd_tracking_city($objRockhopper->stops[3]['city']);
			$objRockhopper->stops_1_zip = substr($objRockhopper->stops[1]['city'],-5);
			$objRockhopper->stops_3_zip = substr($objRockhopper->stops[3]['city'],-5);


		    // Shipping Date
		    $shipping = Nsd_trackingModelTrack::nsd_tracking_shipping($type, $history);
		    
			$objRockhopper->shipping = $shipping;



		    // Delivery Date(s)
		    $estimated = ! empty($body->BasicInformation->EstimatedDelivery) ? (string) $body->BasicInformation->EstimatedDelivery : 'N/A'; // Failsafe in case it does not exist (returns)
		    $delivery = Nsd_trackingModelTrack::nsd_tracking_delivery($type, $phase, $estimated, $history); // Array

			$objRockhopper->estimated = $estimated;
			$objRockhopper->delivery = $delivery;


		    // Position
		    $position = Nsd_trackingModelTrack::nsd_tracking_position($type, $state, $history); // Int
		    $objRockhopper->position = $position;
		    
		    
		    // Tracking bar
		    $bar = Nsd_trackingModelTrack::nsd_tracking_bar($type, $position, $history, $stops); // Array
		
			$objRockhopper->bar = $bar;
		    
		    
		    // Status (Overall)
		    $status = Nsd_trackingModelTrack::nsd_tracking_status($type, $phase, $history); // Array

			$objRockhopper->status = $status;

			
		}
		else
		{
			
			$objRockhopper->error = "1";
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r failed to get data from getRockhopper";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
			
		}


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objRockhopper: ".print_r($objRockhopper, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objRockhopper;
	}


	public static function nsd_tracking_type ( $is_return_service, $is_longhaul ) 
	{
	    $is_return_service = (string) $is_return_service;
	    $is_longhaul = (string) $is_longhaul;
	    
	    // Determine type
	    if ( $is_return_service === 'true' ) {
	        return 'return';
	    } elseif ( $is_longhaul === 'true' ) {
	        return 'door-to-door';
	    } elseif ( $is_longhaul === 'false' ) {
	        return 'last-mile';
	    } else {
	        return null;
	    }
	}



	public static function nsd_tracking_phase ( $type, $state ) 
	{
	    // Phase based on client spreadsheet (and email clarifications)
	    if ( $type === 'return' ) {
	        if ( in_array($state, array('PEND', 'DOCK', 'RTNS', 'STOR')) ) {
	            return 'open';
	        } else if ( in_array($state, array('SCHD', 'ACTV')) ) {
	            return 'scheduled';
	        } else if ( in_array($state, array('FIN', 'WPU')) ) {
	            return 'closed';
	        }
	    } else {
	        if ( $state === 'PEND' ) {
	            return 'pending';
	        } else if ( in_array($state, array('DOCK', 'RTNS', 'WPU', 'STOR')) ) {
	            return 'open';
	        } else if ( in_array($state, array('SCHD', 'ACTV')) ) {
	            return 'scheduled';
	        } else if ( $state === 'FIN' ) {
	            return 'closed';
	        }
	    }
	
	    return 'unknown';
	}


	
	
	public static function nsd_tracking_service ( $service, $default = '-' ) 
	{
	    $service = (string) $service;
	
	    $services = array(
	        'Asset Recov 1man' => 'Asset Recovery',
	        'Asset Recov 2man' => 'Asset Recovery',
	        'Basic' => 'Basic Home Delivery',
	        'Basic1' => 'Basic Home Delivery',
	        'Basic1 Attempt' => 'Basic Attempt',
	        'Basic2' => 'Basic Home Delivery',
	        'Basic2 Attempt' => 'Basic Attempt',
	        'Basic Attempt' => 'Basic Attempt',
	        'Deluxing Service' => 'Deluxing Service',
	        'LTL' => 'Misc',
	        'HD-No Preference' => 'No Preference',
	        'Linehaul Return Only' => 'Linehaul Return',
	        'Store Pickup' => 'Store Pickup',
	        'Sweep' => 'Sweep',
	        'Threshold 1' => 'Threshold Home Delivery',
	        'Threshold 1 Attempt' => 'Threshold Attempt',
	        'Threshold 1 Return' => 'Threshold Return',
	        'Thresh 1 Redelivery' => 'Threshold Redelivery',
	        'Threshold 2' => 'Threshold Home Delivery',
	        'Threshold 2 Attempt' => 'Threshold Attempt',
	        'Threshold 2 Return' => 'Threshold Return',
	        'Thresh 2 Redelivery' => 'Threshold Redelivery',
	        'WhiteGlove 1 Redeliv' => 'White Glove Redelivery',
	        'WhiteGlove 2 Redeliv' => 'White Glove Redelivery',
	        'WhiteGlove' => 'White Glove Home Delivery',
	        'Whiteglove 1' => 'White Glove Home Delivery',
	        'WhiteGlove 1 Attempt' => 'White Glove Attempt',
	        'Whiteglove 1 Return' => 'White Glove Return',
	        'Whiteglove 2' => 'White Glove Home Delivery',
	        'WhiteGlove 2 Attempt' => 'White Glove Attempt',
	        'Whiteglove 2 Return' => 'White Glove Return',
	        'WhiteGlove Attempt' => 'White Glove Attempt',
	        'WhiteGlove' => 'White Glove Home Delivery',
	        'WhiteGlove Return' => 'White Glove Return',
	        'WhiteGlove Redelive' => 'White Glove Redelivery',
	        'Plus' => 'Basic Plus Home Delivery', // NEW!
	        'BASIC' => 'Basic Home Delivery',
	        'BASICPLUS' => 'Basic Plus Home Delivery',
	        'THRESHOLD' => 'Threshold Home Delivery', 
	        'WHITEGLOVE' => 'White Glove Home Delivery',
	        'THRESHOLD RETURN' => 'Threshold Return',
	        'Threshold Return' => 'Threshold Return',
	        'WHITEGLOVERETURN' => 'White Glove Return'        
	    );
	
	    if ( array_key_exists($service, $services) ) {
	        // Return the service
	        return $services[$service];
	    } else {
	        // Unknown
	        //var_dump($service);
	        return $default;
	    }
	}
	
	public static function nsd_tracking_service_descriptions ( $service ) 
	{
	
	    switch ( $service ) {
	    case 'Basic Home Delivery':
	        return array(
	            'Deliver to first available "dry" area (e.g. patio, garage, etc.)',
	            '30-minute courtesy pre-call while en route',
	            'No signature required'
	        );
	    case 'Basic Plus Home Delivery':
	        return array(
	            'Deliver to first available "dry" area (e.g. patio, garage, etc.)',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture'
	        );
	    case 'Threshold Home Delivery':
	    case 'Threshold Redelivery':
	        return array(
	            'Delivery inside main entrance',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture'
	        );
	    case 'White Glove Home Delivery':
	    case 'White Glove Redelivery':
	        return array(
	            'Delivery to room-of-choice',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture',
	            'Product unpack & debris removal'
	        );
	    case 'Threshold Return':
	        return array(
	            'Pickup inside main entrance',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture'
	        );
	    case 'White Glove Return':
	    case 'Asset Recovery':
	        return array(
	            'Pickup from room-of-choice',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture'
	        );
	    default:
	        return array();
	    }
	}
	



	
	
	public static function nsd_tracking_event ( $data ) 
	{
	    $attributes = $data->attributes();
	
		if ( (string) $attributes['code'] == 'DELIVERED' )
		{
		    $location = implode('<br>', array_filter((array) strtok($data->stop3_city, ',')));	
	
			$city = (string)$data->stop3_city;
	
	        $city = str_replace(",", " ", $city);
	        $city = preg_replace('/\s+[0-9]{5}$/', '', trim($city)); // Strip off zip code
	        $city = preg_replace('/\s+([A-Z]{2})$/', ', $1', trim($city)); // Add comma between city and state
	
		    
		    $location = $city;
	
			#$location = implode('<br>', array_filter((array) $data->stop3_city));	
						
		}
		else
		{
		    $location = implode('<br>', array_filter((array) $data->location));		
		}
	
	
	    return array(
	        'timestamp' => ( (string) $attributes['code'] == 'DELIVERED' ) ? Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'U', 0)+1800 : Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'U', 0),
	        'timestamp_real' => Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'U', 0),
			'date' => Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'F j, Y'),
	        'time' => Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'g:i A'),
	        'code' => (string) $attributes['code'],
	        'description' => (string) $data->description,
	        'activity' => Nsd_trackingModelTrack::nsd_tracking_activity($attributes['code'], $data->description, $data->tracking_no),
	        'location' => $location,
	        'eta' => Nsd_trackingModelTrack::nsd_tracking_date($data->eta, 'U', 0)
	    );
	}
	
	
	public static function nsd_tracking_event_is_exception ( $event ) 
	{
	    // Handles some special logic for exceptions provided by client
	    if ( $event['code'] === 'POD' ) {
	        // "If POD description is “Delivery Exception Occurred”, change to red.  Otherwise, green"
	        // Account for typo in API which contains trailing apostrophe ("Delivery Exception occurred'")
	        if ( stripos($event['description'], 'Delivery Exception occurred') !== false ) {
	            return true;
	        } else if ( stripos($event['description'], 'Pickup Exception occurred') !== false ) { // Requested by client
	            return true;
	        }
	    }
	    return false;
	}
	
	
	public static function nsd_tracking_history ( $history ) 
	{
	    // Sort oldest to newest
	    usort($history, function ( $a, $b ) {
	        if ( $a['timestamp'] === $b['timestamp'] ) {
	            return 0;
	        }
	        return $a['timestamp'] > $b['timestamp'] ? 1 : -1;
	    });
	
	    // Fill in missing locations
	    $last_location = '';
	    foreach ( $history as $index => $event ) {
	        if ( empty($event['location']) ) {
	            $history[$index]['location'] = $last_location;
	        } else {
	            $last_location = $event['location'];
	        }
	    }
	
	    // Flip it back for display
	    $history = array_reverse($history);
	
	    return $history;
	}
	
	
	public static function nsd_tracking_activity ( $code, $description, $tracking_no, $default = '-' ) 
	{
	    $code = (string) $code;
	    $description = (string) $description;
	    $tracking_no = (string) $tracking_no;
	
		$code = ( $code == "SCHEDULED" && $tracking_no[0] == "R" ) ? "SCHEDULEDPICKUP" : $code ;
	
		#echo $tracking_no;
		#var_dump($tracking_no);
		
	
	
	    $activities = array(
	        'BCCI' => 'Unable to reach customer; Please email us at customercare@nonstopdelivery.com',
	        'CCCS' => 'Customer Called to Check Stat',
	        'CCXL' => 'Customer Cancelled', // note: spelling
	        'CDAM' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'CDPL' => 'Departed Pick-up Location',
	        'COMP' => 'Completed',
	        'CRCU' => 'Unable to reach customer; Please email us at customercare@nonstopdelivery.com',
	        'CRDL' => 'Customer Refused Delivery',
	        'CRST' => 'Customer Requesting Storage',
	        'CWOR' => 'Customer Waiting on Replacement', // note: updated
	        'DELA' => 'Delivery Attempted',
	        'DELR' => 'Delivery Rescheduled',
	        'DELS' => 'Delivery Scheduled',
	        'DOCK' => 'Arrived at local NSD terminal',
	        'LTLD' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'LTLR' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'LTLS' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'LVMC' => 'Unable to reach customer; Please email us at customercare@nonstopdelivery.com',
	        'MECH' => 'Mechanical',
	        'NEW' => 'Order registered',
	        'NSDE' => 'Dispatch Error',
	        'OCIT' => 'Departed Pick-up Location',
	        'ODEL' => 'Out for Delivery',
	        'PNA' => 'Product Not Arrived',
	        'POD' => 'Proof of Delivery',
	        'PSFO' => 'Pending Shipment from Origin',
	        'UPINF' => 'Updated Order Information',
	        'WPU' => 'Awaiting LTL pickup',
	        'PICKEDUP' => 'Pickup Complete',
	        'WTHR' => 'Weather',
	        'CANCEL' => '', //Is this case we don’t want to show anything for tracking, it is like order didn’t exist (hard cancelled).
	        'TENDER' => 'Order Registered',
	        'SHPTACK' => 'Order Registered',
	        'ARRSHIP' => 'Order Registered',
	        'DEPSHIP' => 'Order Shipped',
	        'ARRTERM' => 'In Transit',
	        'DEPTERM' => 'In Transit',
	        'ARRDLVTERM' => 'In Transit',
	        'DEPDLVTERM' => 'In Transit',
	        'OFP' => 'Out for Pick Up',
	        'AVAIL' => 'Order Registered',
	        'DOCKED' => 'Arrived at local NSD terminal',
	        'SCHEDULED' => 'Delivery Scheduled',
	        'DELIVERED' => 'Delivered',
	        'OFD' => 'Out for Delivery',
	        'COMPLETED' => 'Completed',
	        'AVAIL-R' => 'Order Registered',
	        'CANCEL' => 'Order Canceled',
	        'CANCELLED' => 'Order Canceled',
	        'INTRANSIT' => 'In Transit',
	        'LTLU' => 'In Transit', 
	        'STORAGE' => 'Customer Requesting Storage',
	        'SCHEDULEDPICKUP' => 'Pickup Scheduled'
	    );

	    // Code of "UNSTAT" or "RWSTAT"
	    $stat_activities = array(
	        'Arrived at Terminal Location. B01 Refused by Customer;' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'Arrived at Terminal Location. Held for Consignee;' => 'Arrived at linehaul Terminal',
	        'Arrived at Terminal Location. Held for Full Carrier Load;' => 'Arrived at linehaul Terminal',
	        'Arrived at Terminal Location. Held Pending Appointment;' => 'Arrived at linehaul Terminal',
	        'Arrived at Terminal Location. Prearranged Appointment;' => 'Arrived at linehaul Terminal',
	        'Arrived at Terminal Location. Weather or Natural Disaster Related;' => 'Weather',
	        'Arrived at Terminal Location;' => 'Arrived at linehaul Terminal', // Formerly "Arrived at terminal"
	        'Carrier Departed Pick-up Location with Shipment;' => 'Departed Pick-up Location', // note: added semicolon
	    );
	

	    if ( Nsd_trackingModelTrack::nsd_tracking_event_is_exception(array('code' => $code, 'description' => $description)) ) {
	        return 'Exception Occurred';
	    } else if ( array_key_exists($code, $activities) ) {
	        // Return the activity
	        return $activities[$code];
	    } else if ( ($code === 'UNSTAT' || $code === 'RWSTAT') && array_key_exists($description, $stat_activities) ) {
	        // Return the activity
	        return $stat_activities[$description];
	    } else {
	        // Unknown or do not show
	        //var_dump($code, $description);
	        return $default;
	    }
	
	}
	
	
	
	public static function nsd_tracking_date ( $datetime, $format, $default = '-' ) 
	{
	    // Cast as string (gets rid of XML element)
	    $datetime = (string) $datetime;
	    $matches = array();
	
	    // Check the date format
	    if ( preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}$/', $datetime) && $time = strtotime($datetime) ) {
	        // "Normal" date format that we know will parse
	        return date($format, $time);
	    } else if ( preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2} [C|E]T$/', $datetime) && $time = strtotime(substr($datetime, 0, -3) . ':00') ) { // Assume local time
	        // @note - Applies to UNSTAT and RWSTAT events - "2015-06-30T21:19 ET", "2015-06-30T00:01 CT". 1455535
	        // "Linehaul" date format that we know will parse
	        return date($format, $time);
	    } else if ( preg_match('#^[a-z]{3} ([0-9]{1,2}/[0-9]{1,2}/[0-9]{4})$#i', $datetime, $matches) && $time = strtotime($matches[1]) ) {
	        // Estimated Delivery Date format (secondary)
	        return date($format, $time);
	    } else { 
	        // Ignore rather than display 1970
	        return $default;
	    }
	}
	


	public static function nsd_tracking_stop ( $data ) 
	{
	    $attributes = $data->attributes();
	    return array(
	        'sequence' => (int) $attributes['sequence'],
	        'city' => (string) $data->City
	    );
	}


	public static function nsd_tracking_shipping ( $type, $history ) 
	{
	
	    /**
	     * If Door-to-Door order
	     *     If UNSTAT status is available
	     *         Use earliest UNSTAT date
	     *     Else 
	     *         Item has not shipped, no date is available
	     * Else 
	     *     Order will not have a Ship Date, and that line should be hidden from view
	     * 
	     * Only Door-to-Door should have a Ship Date, Last Mile and Returns will not and should not have that line shown
	     *
	     * The ship date should be under the “Carrier Departed Pick-up Location with Shipment” line (example: 1466604).  
	     * However, in this case (1455557) it doesn’t look like this was sent by the linehaul carrier.  
	     * In cases where this is missing, can we use the earliest date from the “Arrived at Terminal Location” entries?
	     * Yes, the ship date should never be tied to the DOCK date.
	     */
	    if ( $type === 'door-to-door' ) { // If door-to-door
	        $reverse_history = array_reverse($history); // Read oldest to newest
	        $shipping = array_reduce($reverse_history, function ( $shipping, $event ) { // In case there are multiple ship date events, use the latest
	            if ( empty($shipping) && $event['code'] === 'UNSTAT' ) {
	                $shipping = array(
	                    'Ship Date' => date('F j, Y', $event['timestamp']) // No time component
	                );
	            }
	            return $shipping;
	        }, array());
	    } else {
	        $shipping = array(); // No ship date
	    }
	
	    return $shipping;
	}


	public static function nsd_tracking_delivery ( $type, $phase, $estimated, $history ) 
	{
	
	    if ( $type === 'return' ) {
	        $language = 'Pickup';
	    } else {
	        $language = 'Delivery';
	    }
	
	    /**
	     * If Order is a Delivery (D2D or Last Mile):
	     *     If Order is CLOSED, show
	     *         Actual Delivery Date:   (POD Date)
	     *         POD Info:       (POD Name)
	     *     If NOT CLOSED but IS SCHEDULED
	     *         Scheduled Delivery Date:    (Scheduled Date 
	     *                     and Window)
	     *     OTHERWISE, show
	     *         Estimated Delivery Date:    (ETA Date)
	     *         Scheduled Delivery Date:    Not Scheduled
	     * If Order is a Return:
	     *     If orders is CLOSED, show
	     *         Actual Pickup Date: (POD Date)
	     *         POD Info: (POD Name)
	     *     If NOT CLOSED but IS SCHEDULED
	     *         Scheduled Pickup Date: (Scheduled date
	     *                     and window)
	     *     OTHERWISE, show
	     *         Scheduled Pickup Date: Not Scheduled
	     *
	     * New Rule, 9/8/2015:
	     * In the case where the POD Info is “Delivery Exception occurred”
	     *      Could the Actual Delivery Date line either be blanked out
	     */
	    if ( $phase === 'closed' ) { // Closed
	        $delivery = array_reduce($history, function ( $proof_of_delivery, $event ) use ( $language ) { // In case there are multiple POD events, use the latest
	            if ( empty($proof_of_delivery) && $event['code'] === 'POD' || empty($proof_of_delivery) && $event['code'] === 'DELIVERED') {
	                if ( Nsd_trackingModelTrack::nsd_tracking_event_is_exception($event) ) {
	                    $proof_of_delivery = array(
	                        'POD Info' => rtrim($event['description'], " ';") // Trim off extra characters the API includes
	                    );
	                } else {
		                
	/*
		                if ( $event['eta'] != "" )
		                {
			                $proof_of_delivery = array(
							#'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['timestamp']),
	                        'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['eta']),
	                        'POD Info' => rtrim($event['description'], " ';") // Trim off extra characters the API includes
							);
		                }
		                else
		                {
			                $proof_of_delivery = array(
							'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['timestamp']),
	                        #'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['eta']),
	                        'POD Info' => rtrim($event['description'], " ';") // Trim off extra characters the API includes
							);
		                }
	*/
		                
	                    $proof_of_delivery = array(
	
							
	                        'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['timestamp_real']),
	                        #'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['eta']),
	
	
	                        'POD Info' => rtrim($event['description'], " ';") // Trim off extra characters the API includes
	                    );
	                }
	            }
	            return $proof_of_delivery;
	        }, array());
	    } else if ( $phase === 'scheduled' ) { // Scheduled
	
	        $delivery = array_reduce($history, function ( $scheduled, $event ) use ( $language, $estimated ) { // In case there are multiple DELS events, use the latest
	
	
	            if ( (empty($scheduled) && $event['code'] === 'DELS') ) {
	                // "Delivery scheduled: 2015-07-09, 12:00 pm -  4:00 pm"
	                
	                $matches = array();
	                if ( preg_match('/: (\d{4}-\d{2}-\d{2}),( .*)/', $event['description'], $matches) ) {
	                    $scheduled = array(
	                        'Scheduled ' . $language . ' Date' => date('F j, Y,', strtotime($matches[1])) . $matches[2]
	                    );
	                }
	            }
	            
	
	            
	            if ( empty($scheduled) && $event['code'] === 'SCHEDULED' ) {
	                // "Delivery scheduled: 2015-07-09, 12:00 pm -  4:00 pm"
	                
				    if (  strpos($estimated, '-') !== FALSE )
				    {                
						#2016-10-25T00:00:00
						
						#orig
		                #$scheduled = array( 'Scheduled ' . $language . ' Date' => _nsd_tracking_estimated_date($estimated, 'F j, Y') );
	
		                $year = (int) substr($estimated, 0, 4);  // year
		                $month = (int) substr($estimated, 5, 2);  // month
		                $day = (int) substr($estimated, 8, 2);  // day
		                $hour = (int) substr($estimated, 11, 2);  // hour
		                $minute = (int) substr($estimated, 14, 2);  // minute   
	
		                $theDate = date('F j, Y, g:i a', mktime( $hour, $minute, 0, $month, $day, $year) ). ' - '.date('g:i a', mktime( ($hour+4), $minute, 0, $month, $day, $year) ) ;                
		
		                $scheduled = array( 'Scheduled ' . $language . ' Date' =>  $theDate ); 
	
					}
					else
					{
	
		                $year = (int) substr($estimated, 0, 4);  // year
		                $month = (int) substr($estimated, 4, 2);  // month
		                $day = (int) substr($estimated, 6, 2);  // day
		                $hour = (int) substr($estimated, 8, 2);  // hour
		                $min = (int) substr($estimated, 10, 2);  // minute   
		                             
		                $theDate = date('F j, Y, g:i a', mktime( $hour, $min, 0, $month, $day, $year) ). ' - '.date('g:i a', mktime( ($hour+4), $min, 0, $month, $day, $year) ) ;                
		
		                $scheduled = array( 'Scheduled ' . $language . ' Date' =>  $theDate ); 
					}                
	            }
	
	            return $scheduled;
	        }, array());
	    } else if ( $type === 'return' ) {
	        $delivery = array(
	            'Scheduled ' . $language . ' Date' => 'Not Scheduled'
	        );
	    } else {
	
		        $delivery = array(	        
		            'Estimated ' . $language . ' Date' => Nsd_trackingModelTrack::nsd_tracking_estimated_date($estimated, 'F j, Y'),
		            'Scheduled ' . $language . ' Date' => 'Not Scheduled'
		        );
	    }
	
	    return $delivery;
	}	
		
	
	public static function nsd_tracking_estimated_date ( $datetime, $format ) 
	{
	    // Cast as string (gets rid of XML element)
	    $datetime = (string) $datetime;
	    $default = 'N/A'; // If $datetime is not parseable or empty
	
	    $date = Nsd_trackingModelTrack::nsd_tracking_date($datetime, $format, $default);
	
	    if ( strlen($datetime) > 1 && $date === $default ) {
	        return $datetime;
	    }
	
	    return $date;
	}	
		
		

	
	public static function nsd_tracking_status ( $type, $phase, $history ) 
	{
	    // First event
	    $event = $history[0];
	
	    if ( $type === 'return' ) {
	        $language = 'Pickup';
	    } else {
	        $language = 'Delivery';
	    }
	
	    /**
	     * If Order is a Delivery (D2D or Last Mile):
	     *     If Order is CLOSED and POD is "Delivery Exception occurred", show 
	     *         Delivery Exception Occurred
	     *     If Order is CLOSED and no exception, show
	     *         Completed
	     *     If NOT CLOSED, show
	     *         (Last Update Note)
	     * If Order is a Return:
	     *     If Order is CLOSED and POD is Delivery Exception occurred", show
	     *         Pickup Exception Occurred
	     *     If Order is CLOSED and no exception, show
	     *         Completed
	     *     If NOT CLOSED, show
	     *         (Last Update Note)
	     *
	     * New Rule, 9/8/2015:
	     * In the case where the POD Info is “Delivery Exception occurred”
	     *      Could the Current Status be amended to read “Delivery Exception Occurred; Please email us at customercare@nonstopdelivery.com”
	     */
	    if ( $phase === 'closed' ) { // Closed
	        // Find POD event
	        $proof_of_delivery = array_reduce($history, function ( $proof_of_delivery, $event ) use ( $language ) { // In case there are multiple POD events, use the latest
	            if ( empty($proof_of_delivery) && $event['code'] === 'POD' ) {
	                $proof_of_delivery = $event;
	            }
	            return $proof_of_delivery;
	        }, null);
	
	        if ( Nsd_trackingModelTrack::nsd_tracking_event_is_exception($proof_of_delivery) ) {
	            return array(
	                'class' => 'tracking-bad',
	                'activity' => $language . ' Exception Occurred; Please email us at customercare@nonstopdelivery.com'
	            );
	        } else if ( $proof_of_delivery ) { // As long as POD exists
	            return array(
	                'class' => 'tracking-good',
	                'activity' => 'Completed'
	            );
	        }
	    }
	
	    // Everything else displays the last event status
	    return array(
	        'class' => Nsd_trackingModelTrack::nsd_tracking_class($event),
	        'activity' => $event['activity']
	    );
	}

	public static function nsd_tracking_city ( $city, $default = null ) 
	{
	    // Cast as string (gets rid of XML element)
	    $city = (string) $city;
	
	    if ( strlen(trim($city)) > 0 ) {
	        // PHOENIX           AZ 85004
	        $city = preg_replace('/\s+[0-9]{5}$/', '', trim($city)); // Strip off zip code
	        $city = preg_replace('/\s+([A-Z]{2})$/', ', $1', trim($city)); // Add comma between city and state
	        return $city;
	    } else { 
	        // Ignore
	        return $default;
	    }
	}


	public static function lew_nsd_tracking_city ( $city, $default = null ) 
	{
	    // Cast as string (gets rid of XML element)
	    $city = (string) $city;
	
	    if ( strlen(trim($city)) > 0 ) {
	        // PHOENIX           AZ 85004
	        $city = preg_replace('/\s+[0-9]{5}$/', '', trim($city)); // Strip off zip code
	        $city = preg_replace('/\s+([A-Z]{2})$/', ', $1', trim($city)); // Add comma between city and state
	        return $city;
	    } else { 
	        // Ignore
	        return $default;
	    }
	}


	public static function nsd_tracking_class ( $event ) 
	{
	
	    $code_classes = array(
	        'BCCI' => 'tracking-bad',
	        //'CCCS' => 'No change',
	        'CCXL' => 'tracking-bad',
	        'CDAM' => 'tracking-bad',
	        'CDPL' => 'tracking-good',
	        'COMP' => 'tracking-good',
	        'CRCU' => 'tracking-bad',
	        'CRDL' => 'tracking-bad',
	        'CRST' => 'tracking-good',
	        //'CWOR' => 'No change',
	        'DELA' => 'tracking-bad',
	        'DELR' => 'tracking-bad',
	        'DELS' => 'tracking-good',
	        'DOCK' => 'tracking-good',
	        //'INVEN' => 'No change',
	        //'LTLBOL' => 'No change',
	        'LTLD' => 'tracking-bad',
	        'LTLR' => 'tracking-bad',
	        'LTLS' => 'tracking-bad',
	        'LTLU' => 'tracking-good',
	        'LVMC' => 'tracking-bad',
	        'MECH' => 'tracking-bad',
	        'NEW' => 'tracking-good',
	        'NSDE' => 'tracking-bad',
	        'OCIT' => 'tracking-good',
	        'ODEL' => 'tracking-good',
	        //'OTHR' => 'No change',
	        'PNA' => 'tracking-bad',
	        'POD' => 'tracking-good',
	        //'PSFO' => 'No change',
	        //'UPINF' => 'No change',
	        //'WPU' => 'No change',
	        'WTHR' => 'tracking-bad',
	        'CANCEL' => 'tracking-bad',
	        'TENDER' => 'tracking-good',
	        'SHPTACK' => 'tracking-good',
	        'ARRSHIP' => 'tracking-good',
	        'DEPSHIP' => 'tracking-good',
	        'ARRTERM' => 'tracking-good',
	        'DEPTERM' => 'tracking-good',
	        'ARRDLVTERM' => 'tracking-good',
	        'DEPDLVTERM' => 'tracking-good',
	        'OFP' => 'tracking-good',
	        'SCHEDULED' => 'tracking-good',
	        'DELIVERED' => 'tracking-good',
	        'OFD' => 'tracking-good',
	        'DOCKED' => 'tracking-good',
	        'AVAIL-R' => 'tracking-good',
	        'INTRANSIT' => 'tracking-good',
	        'CANCELLED' => 'tracking-bad',
	        'STORAGE' => 'tracking-good'
	    );
	
	    // Code of "UNSTAT" or 'RWSTAT'
	    $stat_classes = array(
	        'Arrived at Terminal Location. B01 Refused by Customer;' => 'tracking-bad',
	        'Arrived at Terminal Location. Held for Consignee;' => 'tracking-good',
	        'Arrived at Terminal Location. Held for Full Carrier Load;' => 'tracking-good',
	        'Arrived at Terminal Location. Held Pending Appointment;' => 'tracking-good',
	        'Arrived at Terminal Location. Prearranged Appointment;' => 'tracking-good',
	        'Arrived at Terminal Location. Weather or Natural Disaster Related;' => 'tracking-bad',
	        'Arrived at Terminal Location;' => 'tracking-good',
	        'Carrier Departed Pick-up Location with Shipment;' => 'tracking-good', // note: added semicolon
	    );
	
	    if ( Nsd_trackingModelTrack::nsd_tracking_event_is_exception($event) ) {
	        return 'tracking-bad';
	    } else if ( array_key_exists($event['code'], $code_classes) ) {
	        return $code_classes[$event['code']];
	    } else if ( ($event['code'] === 'UNSTAT' || $event['code'] === 'RWSTAT') && array_key_exists($event['description'], $stat_classes) ) {
	        return $stat_classes[$event['description']];
	    } else {
	        // No class
	        return '';
	    }
	}


	public static function nsd_tracking_position ( $type, $state, $history ) 
	{
	    
	    // Append Arrived or Departed for certain D2D statuses
	    if ( $type === 'door-to-door' && $state === 'PEND' ) {
	        // CHECK FOR UNSTAT/RWSTAT
	        // @link http://nstracking.nonstopdelivery.com/api/rest/nsdws/tracking/ws-track-shipment/9495546
	        // A "Departed" or "Arrived" event means shipped
	        $state .= array_reduce($history, function ( $shipped, $event ) {
	            if ( empty($shipped) && ($event['code'] === 'UNSTAT' || $event['code'] === 'RWSTAT') ) {
	                if ( stripos($event['description'], 'Carrier Departed') === 0 || stripos($event['description'], 'Arrived') === 0 ) {
	                    // <description>Carrier Departed Pick-up Location with Shipment;</description>
	                    // <description>Arrived at Terminal Location;</description>
	                    $shipped = 'Shipped';
	                }
	            }
	            return $shipped;
	        }, '');
	    }
	
	    // Returns the position the state corresponds to
	    $positions = array(
	        'door-to-door' => array(
	            'PEND' => 1,
	            'PENDShipped' => 2,
	            'DOCK' => 3,
	            'SCHD' => 3,
	            'ACTV' => 4,
	            'FIN' => 5,
	            'RTNS' => 3,
	            'WPU' => 3,
	            'STOR' => 3,
	        ),
	        'last-mile' => array(
	            'PEND' => 1,
	            'DOCK' => 2,
	            'SCHD' => 2,
	            'ACTV' => 3,
	            'FIN' => 4,
	            'RTNS' => 2,
	            'WPU' => 2,
	            'STOR' => 2,
	        ),
	        'return' => array(
	            'PEND' => 1,
	            'DOCK' => 1,
	            'SCHD' => 2,
	            'ACTV' => 2,
	            'FIN' => 3,
	            'RTNS' => 1,
	            'WPU' => 3,
	            'STOR' => 1,
	        ),
	    );
	
	    if ( array_key_exists($type, $positions) && array_key_exists($state, $positions[$type]) ) {
	        return $positions[$type][$state];
	    } else {
	        return false;
	    }
	}



	
	public static function nsd_tracking_bar ( $type, $position, $history, $stops ) 
	{
	    switch ( $type ) {
	    case 'door-to-door':
	        return array(
	            array(
	                'class' => 'tracking-bar-soon' . Nsd_trackingModelTrack::nsd_tracking_chevron(1, $position, $history),
	                'label' => 'Shipping Soon'
	            ),
	            array(
	                'class' => 'tracking-bar-shipped' . Nsd_trackingModelTrack::nsd_tracking_chevron(1.5, $position, $history),
	                'label' => 'Shipped'
	            ),
	            array(
	                'class' => 'tracking-bar-transit' . Nsd_trackingModelTrack::nsd_tracking_chevron(2, $position, $history),
	                'label' => 'In Transit'
	            ),
	            array(
	                'class' => 'tracking-bar-arrived' . Nsd_trackingModelTrack::nsd_tracking_chevron(3, $position, $history),
	                'label' => 'Arrived at Last Mile Terminal'
	            ),
	            array(
	                'class' => 'tracking-bar-out' . Nsd_trackingModelTrack::nsd_tracking_chevron(4, $position, $history),
	                'label' => 'Out for Delivery'
	            ),
	            array(
	                'class' => 'tracking-bar-delivered' . Nsd_trackingModelTrack::nsd_tracking_chevron(5, $position, $history),
	                'label' => 'Delivered'
	            ),
	        );
	        break;
	    case 'last-mile': 
	        return array(
	            array(
	                'class' => 'tracking-bar-enroute' . Nsd_trackingModelTrack::nsd_tracking_chevron(1, $position, $history),
	                'label' => 'Enroute to Last Mile Terminal'
	            ),
	            array(
	                'class' => 'tracking-bar-arrived' . Nsd_trackingModelTrack::nsd_tracking_chevron(2, $position, $history),
	                'label' => 'Arrived at Last Mile Terminal'
	            ),
	            array(
	                'class' => 'tracking-bar-out' . Nsd_trackingModelTrack::nsd_tracking_chevron(3, $position, $history),
	                'label' => 'Out for Delivery'
	            ),
	            array(
	                'class' => 'tracking-bar-delivered' . Nsd_trackingModelTrack::nsd_tracking_chevron(4, $position, $history),
	                'label' => 'Delivered'
	            ),
	        );
	        break;
	    case 'return':
	        return array(
	            array(
	                'class' => 'tracking-bar-notified' . Nsd_trackingModelTrack::nsd_tracking_chevron(1, $position, $history),
	                'label' => 'Notified of Pick-up Order'
	            ),
	            array(
	                'class' => 'tracking-bar-scheduled' . Nsd_trackingModelTrack::nsd_tracking_chevron(2, $position, $history),
	                'label' => 'Scheduled for Pick-up'
	            ),
	            array(
	                'class' => 'tracking-bar-picked' . Nsd_trackingModelTrack::nsd_tracking_chevron(3, $position, $history),
	                'label' => 'Picked up'
	            ),
	        );
	        break;
	    default:
	        return array();
	    }
	}
	

	public static function nsd_tracking_chevron ( $index, $position, $history ) 
	{
	    $class = '';
	    if ( $position >= $index ) {
	        $class .= ' active';
	    } else {
	        $class .= ' inactive';
	    }
	
	    if ( $position === $index ) {
	        $class .= ' current';        
	
	        // Allow multiple codes at the exact same time
	        $stop = false;
	        foreach ( $history as $event ) {
	            // Stop looking if we are beyond the stop time
	            if ( $stop && $event['timestamp'] < $stop ) {
	                break;
	            }
	
	            // Check code for color
	            if ( $event_class = Nsd_trackingModelTrack::nsd_tracking_class($event) ) {
	                $class .= ' ' . $event_class;
	                // Set stop time to allow for other codes at the same time
	                $stop = $event['timestamp'];
	            }
	        }
	    }
	
	    return $class;
	}



	public static function d_createCurlCookie( $objCookie )  //DEPRICATED
	{
		
		$controllerName = "Nsd_trackingController";
		$functionName = "createCurlCookie";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;

		if ( $flag_production )
		{
            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
		#$objCookie->tm4web_usr
		#$objCookie->tm4web_pwd
		#$objCookie->domain
		$objCookie->curl_error = "";
		$objCookie->result = "0";
		$objCookie->attempt = 1;
		$objCookie->cookieName = $objCookie->domain.".".$objCookie->tm4web_usr;
		$objCookie->cookieFilePath = JPATH_SITE."/tmp/".$objCookie->cookieName;
		$objCookie->loginUrl = "https://".$objCookie->domain."/login/do_login.msw";


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 0 ";
		$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		while ( $objCookie->attempt <= 9 )
		{
			
			
			
			//init curl
			$ch = curl_init();
			

			//Set the URL to work with
			curl_setopt($ch, CURLOPT_URL, $objCookie->loginUrl);


			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 1);
			
			//Set the post parameters
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$objCookie->tm4web_usr.'&tm4web_pwd='.$objCookie->tm4web_pwd);
						
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $objCookie->cookieFilePath);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
			
	
			ob_start();      // prevent any output

			$store = curl_exec ($ch); // execute the curl command
			
			if ( curl_errno($ch) )
			{
			    $objCookie->curl_error = curl_error($ch);
			}			
	
			ob_end_clean();  // stop preventing output

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
			#$logMessage .= "\n\r store: ".print_r($store, true);
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	

			
			curl_close ($ch);
			unset($ch);					
			
			if ( $store != "" )
			{

				$objCookie->result = "1";

				$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 ";
				$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				$stamp_end_micro = microtime(true);
				$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
				$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
				$logMessage .= "\n\r \n\r";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


				$objDataInsert = new stdClass();
				$objDataInsert->tmw_user = $objCookie->tm4web_usr;
				$objDataInsert->cookie_name = $objCookie->cookieName;
				$objDataInsert->datetime = date("Y-m-d H:i:s");;
				$objDataInsert->datetime_gmt = gmdate("Y-m-d H:i:s");
				$objDataInsert->attempts = $objCookie->attempt;
				$objDataInsert->result = $objCookie->result;
				$objDataInsert->result_error = "";
				$objDataInsert->duration = number_format($stamp_total_micro,2);
				$objDataInsert->objCookie = json_encode($objCookie);
				$result = JFactory::getDbo()->insertObject('htc_nsd_truckmate_cookie_log', $objDataInsert);
				$IID = $db->insertid();



				return $objCookie;				
			}
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1.1 ";
			$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }				
			
			
			$objCookie->attempt += 1;
			
			#remove cookie file
			unlink( $objCookie->cookieFilePath );
		}






		# there has been 5 attempts to create a curl cookie and it failed.
		# log
		# alert admin
		# return fail

		$objCookie->result = "0";
		$objCookie->result_error = "Reached maximum attempts to create cURL cookie for user";		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$objDataInsert = new stdClass();
		$objDataInsert->tmw_user = $objCookie->tm4web_usr;
		$objDataInsert->cookie_name = $objCookie->cookieName;
		$objDataInsert->datetime = date("Y-m-d H:i:s");;
		$objDataInsert->datetime_gmt = gmdate("Y-m-d H:i:s");
		$objDataInsert->attempts = ($objCookie->attempt - 1) ;
		$objDataInsert->result = $objCookie->result;
		$objDataInsert->result_error = $objCookie->result_error;
		$objDataInsert->duration = number_format($stamp_total_micro,2);
		$objDataInsert->objCookie = json_encode($objCookie);
		$result = JFactory::getDbo()->insertObject('htc_nsd_truckmate_cookie_log', $objDataInsert);
		$IID = $db->insertid();


		$body = "";
		$body .= "<br>Unabele to create a cURL cookie for ".$objCookie->tm4web_usr."<br>";
		$body .= "<br><br>The objCookie is: ".json_encode($objCookie)."<br>";
		

		$objSendEmailMessage = new stdClass();
		$objSendEmailMessage->email_to = "lsawyer@metalake.com";
		$objSendEmailMessage->email_from = "nsdalert@metalake.com";
		$objSendEmailMessage->email_subject = "ALERT | com_nsd_tracking | createCurlCookie";
		$objSendEmailMessage->email_body = $body;
		$objSendEmailMessage->email_category = "tracking support";

		#$objSGReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 3 ";
		$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }




		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
		
		return $objCookie;



		
	}


// !COMMON FUNCTIONS


	public static function logActivity( $objActivityLog )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=logActivity

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=logActivity

		$controllerName = "Nsd_trackingController";
		$functionName = "logActivity";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$objData = new stdClass();
		$objData->remote_address = $objActivityLog->remote_address;
		$objData->trace = $objActivityLog->trace;
		$objData->trace_type = $objActivityLog->trace_type;
		$objData->delivery_type = $objActivityLog->delivery_type;
		$objData->order_type = $objActivityLog->order_type;
		$objData->json_return = $objActivityLog->json_return;
		$objData->trace_datetime = date("Y-m-d H:i:s");
		$objData->trace_datetime_gmt = gmdate("Y-m-d H:i:s");
		$objData->trace_milliseconds = round(microtime(true) * 1000);
		$objData->tm4web_usr = $objActivityLog->tm4web_usr;
		$objData->flag_api = $objActivityLog->flag_api;
		$objData->return_format = $objActivityLog->return_format;
		
		$result = JFactory::getDbo()->insertObject('htc_nsd_tracking_activity_log', $objData);
		$logID = $db->insertid();


		Nsd_trackingController::pruneLogs();


/*
		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
*/


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public static function existRockhopper( $objForm )
	{

		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=existRockhopper
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=existRockhopper

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=existRockhopper
		

		$controllerName = "Nsd_trackingController";
		$functionName = "existRockhopper";
		
		$db = JFactory::getDBO();
		
		$app = JFactory::getApplication();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$trace_number = $objForm->trace_number;


	    $url = 'http://nstracking.nonstopdelivery.com/api/rest/nsdws/tracking/ws-track-shipment/' . $trace_number;
	
	    $response = file_get_contents($url);

		$body = simplexml_load_string($response);

		$tracking_number = (string)$body->BasicInformation->TrackingNumber;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r url: ".print_r($url, true);
		$logMessage .= "\n\r response: ".print_r($response, true);
		$logMessage .= "\n\r body: ".print_r($body, true);
		$logMessage .= "\n\r TrackingNumber: ".print_r($tracking_number, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $trace_number == $tracking_number )
		{

			$link = 'http://www.nonstopdelivery.com/tracking/?tracking='.$tracking_number;
			#$this->setRedirect($link, $msg);
			
			$app->redirect($link);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r link: ".print_r($link, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			
		}
		else
		{
			$objReturn = new stdClass();
			$objReturn->error = "1";
			$objReturn->error_message = "The trace number does not exist in Rockhopper.";
		}

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}


	public static function testForRockhopper( $objForm )
	{

		$controllerName = "Nsd_trackingController";
		$functionName = "testForRockhopper";
		
		$db = JFactory::getDBO();
		
		$app = JFactory::getApplication();
		
		$objReturn = new stdClass();
		$objReturn->isRockhopper = 0;
		$objReturn->error = "0";
		$objReturn->error_message = "";


		return $objReturn;
	}


	public static function sendAlert( $objAlert )
	{
        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
		$receive_alerts	= 1;

		if ( $receive_alerts == "1" )
		{

			$mailer = JFactory::getMailer();
			$config = JFactory::getConfig();
	
			$sender = array( 
			    $config->get( 'mailfrom' ),
			    $config->get( 'fromname' ) 
			);
			$mailer->setSender($sender);
	
			$mailer->addRecipient( $objAlert->recipients );
	
			$mailer->setSubject( $objAlert->subject );
			$mailer->setBody( $objAlert->body );
			$mailer->isHTML(true);
	
			if ($mailer->Send() !== true)
			{
				$return = 0;			
			}			
			else
			{
				$return = 1;			
			}

		}	
		else
		{
			$return = 0;
		}	

		return $return;
	}

	public static function pruneLogs()
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_ml_api&task=pruneLogs
		
		$functionName = "pruneLogs";
		
		$db = JFactory::getDBO();


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->log_table_prune_size	= $params->get('log_table_prune_size');

		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "dispatchtrack_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "dispatchtrack_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | Ml_apiController | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrTables = array();
		$arrTables[] = "htc_nsd_tracking_activity_log";


		$log_table_prune_size = $paramsComponent->log_table_prune_size;
		$log_table_prune_size_upper_boundry = ( $log_table_prune_size + 100000 );

				$logMessage = "INSIDE | Ml_apiController | ".$functionName;
				$logMessage .= "\n\r log_table_prune_size: ".print_r($log_table_prune_size, true);
				$logMessage .= "\n\r log_table_prune_size_upper_boundry: ".print_r($log_table_prune_size_upper_boundry, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		#if ( date('H') == '23' )
		
		if ( TRUE )
		{
			foreach( $arrTables as $table )
			{
				$query = "select count(z.id) from ".$table." z";
				$db->setQuery($query);
				$recordCount = $db->loadResult();

				$logMessage = "INSIDE | Ml_apiController | ".$functionName;
				$logMessage .= "\n\r recordCount for ".$table.": ".print_r($recordCount, true);
				#$logMessage .= "\n\r dateH: ".print_r(date('H'), true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


				if ( $recordCount > $log_table_prune_size_upper_boundry )
				{
		
					$query = "select z.id from ".$table." z order by z.id desc limit ".$log_table_prune_size.", 1";
					$db->setQuery($query);
					$idRecord = $db->loadResult();
			
					if ( $idRecord != "" )
					{
						$queryD = "Delete FROM ".$table." WHERE id <= '".$idRecord."' ";
						$db->setQuery($queryD);
						$db->query();
					}
		
					$logMessage = "INSIDE | Ml_apiController | ".$functionName;
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r idRecord: ".print_r($idRecord, true);
					$logMessage .= "\n\r queryD: ".print_r($queryD, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

				}


			}
		}







		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | Ml_apiController | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);
		$logMessage .= "\n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


	public static function getTwoDaysAway( $objInput )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_function_template

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=getTwoDaysAway

		$controllerName = "Nsd_trackingController";
		$functionName = "getTwoDaysAway";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrHoliday = array();
		
		$arrYears = array();
		$arrYears[] = date('Y');
		$arrYears[] = date('Y', strtotime('+1 year'));

		foreach( $arrYears as $currentYear )
		{

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$i;
			$logMessage .= "\n\r currentYear: ".print_r($currentYear, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			// New Years Day
	        //$arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("first day of january $currentYear")));
	        $arrHoliday[] = date('Y-m-d', strtotime("first day of january $currentYear"));
	        
	        // Martin Luther King, Jr. Day
	        //$arrHoliday[] = date('Y-m-d', strtotime("january $currentYear third monday"));
	
	        // President's Day
			// $arrHoliday[] = date('Y-m-d', strtotime("february $currentYear third monday"));
	
	        // Memorial Day
	        //$arrHoliday[] = (new DateTime("Last monday of May"))->format("Y-m-d");
	
	        // Independence Day
	        //$arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("july 4 $currentYear")));
	        //$arrHoliday[] = date('Y-m-d', strtotime("july 4 $currentYear"));
	
	
	        // Labor Day
	        //$arrHoliday[] = date('Y-m-d', strtotime("september $currentYear first monday"));
	
	        // Columbus Day
	        // $arrHoliday[] = date('Y-m-d', strtotime("october $currentYear second monday"));
	
	        // Veteran's Day
			// $arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("november 11 $currentYear")));
	
	        // Thanksgiving Day
			$arrHoliday[] = date('Y-m-d', strtotime("november $currentYear fourth thursday"));


	        // Christmas Eve
	        //$arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("december 25 $currentYear")));
			$arrHoliday[] = date('Y-m-d', strtotime("december 24 $currentYear"));


	
	        // Christmas Day
	        //$arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("december 25 $currentYear")));
			$arrHoliday[] = date('Y-m-d', strtotime("december 25 $currentYear"));

			
	        // New Years Eve
	        //$arrHoliday[] = Nsd_schedulingController::observed_date(date('Ymd', strtotime("december 25 $currentYear")));
			$arrHoliday[] = date('Y-m-d', strtotime("december 31 $currentYear"));
			
			
			
			//test holiday
			//$arrHolidays[] = "2018-09-26";

		}

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$i;
			$logMessage .= "\n\r arrYears: ".print_r($arrYears, true);
			$logMessage .= "\n\r arrHoliday: ".print_r($arrHoliday, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$weekend = array('Sun','Sat');
		$date = new DateTime($objInput->date);
		$nextDay = clone $date;
		
		
		$i = 0; // We have 0 future dates to start with

		$nextDates = array(); // Empty array to hold the next 3 dates

		while ($i < 2)
		{
		   $nextDay->add(new DateInterval('P1D')); // Add 1 day
   
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$i;
			$logMessage .= "\n\r nextDay: ".print_r($nextDay, true);
			$logMessage .= "\n\r nextDay->format('D'): ".print_r($nextDay->format('D'), true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
    
		    if (in_array($nextDay->format('Y-m-d'), $arrHoliday)) continue; // Don't include year to ensure the check is year independent
		    
		    
		    // Note that you may need to do more complicated things for special holidays that don't use specific dates like "the last Friday of this month"
		    if (in_array($nextDay->format('D'), $weekend)) continue;

		    // These next lines will only execute if continue isn't called for this iteration
		    $nextDates[] = $nextDay->format('Y-m-d');
		    $i++;
		}



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r nextDates: ".print_r($nextDates, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $nextDates;
	}



// !TEST FUNCTIONS


	public static function test_sendEmailStatus()
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_sendEmailStatus

		#manual
		#https://mpdf.github.io/
		
		
			$className = "Nsd_trackingController";
			$functionName = "test_sendEmailStatus";

			$db 	= JFactory::getDBO();

			$app = JFactory::getApplication();
	
			$objParams = new stdClass();
	
			$params         						= $app->getParams();
			$objParams->path_to_pod_directory	= $params->get('path_to_pod_directory');
			
		
			$flag_production = 0;
	
			if ( $flag_production )
			{
	
	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "tracking_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "tracking_prod";
	
			}
			else
			{
	
	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "tracking_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "tracking_prod";
		
			}	
		
			$stamp_start_micro = microtime(true);

			$logMessage = "START | ".$className." | ".$functionName;	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
	
			$objEmail = new stdClass();
			$objEmail->orderNumber = "123456789";
			$objEmail->detailLineId = "987654321";
			#$objEmail->consigneeInfo = $objTMW->order->CALLNAME;
			#$objEmail->serviceLevel = $objTMW->order->SERVICE_LEVEL;
			
			
			#$objEmail->propertyDamage = $objForm->shipInstructions_24;
			#$objEmail->deliveryFeedbackComment = $objForm->deliveryFeedbackComment;
			#$objEmail->deliveryAccepted = $objForm->deliveryAccepted;
			
			#$objEmail->orderDetailsAll = $objTMW->order_details;
			#$objEmail->orderDetailsExceptions = $objTMW->order_details;

			$objEmail->messageType = "SYNERGIZE";
			$objEmail->subjectLine = "Subject Line ".date("YmdHis");
			#$objEmail->pdf_name = $objReturnReceipt->receipt_file_name;
			#$receipt_file_full_path = $objReturnReceipt->receipt_file_full_path;
			#$objEmail->pdf_file = "zzz";

	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objEmail: ".print_r($objEmail, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			$objReturn = Nsd_workatoController::sendEmailMessagePOD( $objEmail );

			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

	
			$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
			$logMessage .= "\n\r \n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public static function test_insertOrderStatus()
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_insertOrderStatus

		#manual
		#https://mpdf.github.io/
		
		
			$className = "Nsd_trackingController";
			$functionName = "test_insertOrderStatus";

			$db 	= JFactory::getDBO();

			$app = JFactory::getApplication();
	
			$objParams = new stdClass();
	
			$params         						= $app->getParams();
			$objParams->path_to_pod_directory	= $params->get('path_to_pod_directory');
			
		
			$flag_production = 0;
	
			if ( $flag_production )
			{
	
	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "tracking_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "tracking_prod";
	
			}
			else
			{
	
	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "tracking_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "tracking_prod";
		
			}	
		
			$stamp_start_micro = microtime(true);

			$logMessage = "START | ".$className." | ".$functionName;	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			/*
			SUBMIT TO WORKATO
			https://www.workato.com/recipes/943989-new-status-odrstat-insert#jobs
			Insert below into TM:
			Updated By
			Status AUDIT
			Status Comment: Web POD form Submitted
			Reason Code: WEBPOD
			*Detail Line ID is required field
			*Status is required field
			*/
	
	
			$objAudit = new stdClass();
			$objAudit->statusCode = "AUDIT";
			$objAudit->comment = "Web POD form Submitted";
			$objAudit->reasonCode = "WEBPOD";
			$objAudit->detailLineId = "979774"; #$objTMW->order->DETAIL_LINE_ID;
			$objAudit->updatedBy = "WEBPOD";
	
	
			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objAudit: ".print_r($objAudit, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			$objReturn = Nsd_trackingController::insertOrderStatus( $objAudit );

			$logMessage = "INSIDE | ".$className." | ".$functionName;
			$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

	
			$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
			$logMessage .= "\n\r \n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_createCurlCookie( )
	{
		# https://tracking.shipnsd.com/index.php?option=com_nsd_tracking&task=test_createCurlCookie

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_createCurlCookie
		
		

		$controllerName = "Nsd_trackingController";
		$functionName = "test_createCurlCookie";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$objForm = new stdClass();
		$objForm->flag_api = "1";
		$objForm->tmw4web_user = "NSD_API_DEMO";
		$objForm->tmw4web_password = "83dpKjMn";
		$objForm->flag_api = "0";


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('tmw4web_user');
		$paramsComponent->tmw4web_password			= $params->get('tmw4web_password');
		$paramsComponent->tmw4web_subdomain			= $params->get('tmw4web_subdomain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_subdomain_test	= $params->get('tmw4web_subdomain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_subdomain;   
			
		}
		else
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_subdomain_test;   		
		}
		



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." ";
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objCookie = new stdClass();
		$objCookie->tm4web_usr = $tm4web_usr;
		$objCookie->tm4web_pwd = $tm4web_pwd;
		$objCookie->domain = $domain;


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objCookie = Nsd_trackingController::createCurlCookie( $objCookie );


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 ";
		$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_createCurlCookie2( )
	{
		# https://tracking.shipnsd.com/index.php?option=com_nsd_tracking&task=test_createCurlCookie2

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_createCurlCookie2
		
		

		$controllerName = "Nsd_trackingController";
		$functionName = "test_createCurlCookie2";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$objForm = new stdClass();
		$objForm->flag_api = "1";
		$objForm->tmw4web_user = "NSD_API_DEMO";
		$objForm->tmw4web_password = "83dpKjMn";
		$objForm->flag_api = "0";


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('tmw4web_user');
		$paramsComponent->tmw4web_password			= $params->get('tmw4web_password');
		$paramsComponent->tmw4web_subdomain			= $params->get('tmw4web_subdomain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_subdomain_test	= $params->get('tmw4web_subdomain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_subdomain;   
			
		}
		else
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_subdomain_test;   		
		}
		



		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." ";
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$objCookie = new stdClass();
		$objCookie->tm4web_usr = $tm4web_usr;
		$objCookie->tm4web_pwd = $tm4web_pwd;
		$objCookie->domain = $domain;
		$objCookie->cookieName = $objCookie->domain.".".$objCookie->tm4web_usr;
		$objCookie->cookieFilePath = JPATH_SITE."/tmp/".$objCookie->cookieName;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
		$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		if ( JFile::exists( $objCookie->cookieFilePath ) )
		{
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1.2 | cookie exists";
			
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

		}
		else
		{

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1.4 | could not find cookie ";
			$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }			
		}


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 2 ";
		$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);
		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}
	
	

	public function test_tmw4web( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web&trace=1063701216
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web&trace=31065532&prod=1
		
		
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_tmw4web&trace=1063701216


		$controllerName = "Nsd_trackingController";
		$functionName = "test_tmw4web";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		$flag_production = JRequest::getVar('prod', 0);
		$trace_number = JRequest::getVar('trace', '');



		
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
            
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$subdomain = 'nonstopdelivery';
            

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
            
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = 'rVELXtjAjh';
			#$subdomain = 'nonstoptest';  
			$subdomain = 'nonstopdelivery';          
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		if ( $trace_number != "" )
		{
	
			$tmp_fname = tempnam("/tmp", "COOKIE");
	
	
			#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD
	
	
	
	/*
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$loginUrl = 'https://nonstopdelivery.tmwcloud.com/login/do_login.msw';
	*/
	
	/*
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = '1234';
			$loginUrl = 'https://nonstoptest.tmwcloud.com/login/do_login.msw';
	*/
			
			$loginUrl = 'https://'.$subdomain.'.tmwcloud.com/login/do_login.msw';
			
			//init curl
			$ch = curl_init();
			
			//Set the URL to work with
			curl_setopt($ch, CURLOPT_URL, $loginUrl);
			
			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 1);
			
			//Set the post parameters
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
			
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);			
			
			//execute the request (the login)
			$store = curl_exec($ch);
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
	
				if ( $store == "" )
				{
					
					$objReturn->error = 1;
					$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number in system";
					$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
					
					
				}	
	
	
	
	
			#bill number
			$queryURL = "https://".$subdomain.".tmwcloud.com/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=equals&trace_number=".$trace_number;
			
			#po #
			#$queryURL = "https://".$subdomain.".tmwcloud.com/trace/trace_class.msw?include_all_clients=false&trace_type=PPTRACE&search_style=equals&trace_number=".$trace_number;
			
			#reference1
			#$queryURL = "https://".$subdomain.".tmwcloud.com/trace/trace_class.msw?include_all_clients=false&trace_type=QPTRACE&search_style=equals&trace_number=".$trace_number;
			
			
			
			curl_setopt ($ch, CURLOPT_HTTPGET, 1);
			curl_setopt ($ch, CURLOPT_URL, $queryURL);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_HEADER, 0);
			
			//execute the request
			$contentQuery = curl_exec($ch);
			
			#echo "contentQuery: <PRE>".htmlentities($contentQuery)."</PRE>";
	
			if(curl_errno($ch)){
			    echo 'Curl error: ' . curl_error($ch);
			}
	
			#print_r(curl_getinfo($ch));
	
	
			$objDataOrder = json_decode($contentQuery);
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
			$logMessage .= "\n\r contentQuery: ".print_r($objDataOrder, true);
			$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
			#$logMessage .= "\n\r location: ".print_r($location, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
			$detailLineID = $objDataOrder->dataResults[0]->DETAIL_LINE_ID;
	
	
	
	
	
	
	
	
	
	
	
	
	
			$detailURL = "https://".$subdomain.".tmwcloud.com/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;
			
	
			curl_setopt ($ch, CURLOPT_HEADER, 0);
			curl_setopt ($ch, CURLOPT_HTTPGET, 1);
			curl_setopt ($ch, CURLOPT_URL, $detailURL);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt ($ch, CURLOPT_ENCODING, "");
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_VERBOSE, 0);
			
			//execute the request
			$detailQuery = curl_exec($ch);
	
			#$headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT ); // request headers
			#echo "headerSent: <PRE>".htmlentities($headerSent)."</PRE>";
			#echo "detailQuery: <PRE>".htmlentities($detailQuery)."</PRE>";
			#print_r(curl_getinfo($ch));
	
	
	
			if (curl_error($ch)) {
			    $curl_error = curl_error($ch);
			}
	
			$objDataOrderDetail = json_decode($detailQuery);
			
			$arrOrderSteps = $objDataOrderDetail->ODRSTAT->data;
	
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r curl_error: ".print_r(json_decode($curl_error), true);
			$logMessage .= "\n\r detailLineID: ".print_r($detailLineID, true);
			$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
			$logMessage .= "\n\r detailQuery: ".print_r($detailQuery, true);
			$logMessage .= "\n\r objDataOrderDetail: ".print_r($objDataOrderDetail, true);
			$logMessage .= "\n\r arrOrderSteps: ".print_r($arrOrderSteps, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
	
	
			curl_close($ch);
			unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
			unset($ch);
		
		}
		else
		{

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
		}

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	



	public function test_logger( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_logger
		
		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_logger
		
		$controllerName = "Nsd_trackingController";
		$functionName = "test_logger";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}		

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	


	public function test_build_sldescriptions( $file_name=null )
	{
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_build_sldescriptions

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_build_sldescriptions

		$controllerName = "Nsd_trackingController";
		$functionName = "test_build_sldescriptions";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrDesc = array(
'Pickup from room-of-choice',
'Pre-scheduled appointment window',
'30-minute courtesy pre-call while en route',
'Signature capture',
           	);

		$jsonDesc = json_encode($arrDesc);

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r arrDesc: ".print_r($arrDesc, true);
		$logMessage .= "\n\r jsonDesc: ".print_r($jsonDesc, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




	public function test_show_sldescriptions( $file_name=null )
	{
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_show_sldescriptions

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_show_sldescriptions

		$controllerName = "Nsd_trackingController";
		$functionName = "test_show_sldescriptions";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrWhere = array();
		$arrWhere[] = "state = 1";
		
		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
		
		
		$query = "SELECT * FROM htc_nsd_tracking_lu_service_descriptions ". $where ;
		$db->setQuery($query);
		$objListReturn = $db->loadObjectList();
		
		$display = "";
		
		foreach( $objListReturn as $sl )
		{
			

			$arrSL = json_decode($sl->description_long);
			
			$retUL = "";
			$retUL .= "<br>";
			$retUL .= "<br>";
			$retUL .= "<br>".$sl->name;
			$retUL .= "<br>".$sl->description;
			$retUL .= "<ul>";
			
			
			
			foreach( $arrSL as $sl )
			{
				
				$retUL .= "<li>".$sl."</li>";
			}
			
			$retUL .= "</ul>";
			
			$display .= $retUL;			
		}
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r arrDesc: ".print_r($arrDesc, true);
		$logMessage .= "\n\r jsonDesc: ".print_r($jsonDesc, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		echo $display;
	}




	public function test_rockhopper( )
	{

		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_rockhopper

		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_rockhopper

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_rockhopper

		$controllerName = "Nsd_trackingController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$tracking_number = "13717586";
		$tracking_number = "13730052";
		

	    $url = 'http://nstracking.nonstopdelivery.com/api/rest/nsdws/tracking/ws-track-shipment/' . $tracking_number;
	
	    $response = file_get_contents($url);


		$body = simplexml_load_string($response);
		

		$objRockhopper = new stdClass();

		$objOrder = new stdClass();
		$objOrder->DETAIL_LINE_ID = "";
		$objOrder->CUSTOMER = "";
		$objOrder->BILL_NUMBER = "";
		$objOrder->OP_CODE = ""; //DD or LM
		$objOrder->SITE_ID = ""; //SITE5 (return) SITE6 (delivery)
		$objOrder->SERVICE_LEVEL = ""; //eg THRESHOLD
		$objOrder->CURRENT_STATUS = "";
		$objOrder->ORIGCITY = "";
		$objOrder->ORIGPROV = "";
		$objOrder->ORIGPC = "";
		$objOrder->DESTCITY = "";
		$objOrder->DESTPROV = "";
		$objOrder->DESTPC = "";
		$objOrder->PIECES = "";
		$objOrder->WEIGHT = "";
		$objOrder->CARE_OF_ADDR1 = "";
		$objOrder->CARE_OF_CITY = "";
		$objOrder->CARE_OF_PROV = "";
		$objOrder->CARE_OF_PC = "";
		$objOrder->DELIVER_BY = "";
		$objOrder->DELIVER_BY_END = "";
		$objOrder->DELIVERY_APPT_MADE = "";
		$objOrder->PICK_UP_BY = "";
		$objOrder->PICK_UP_BY_END = "";
		$objOrder->PICK_UP_APPT_MADE = "";
		$objOrder->TRACE_TYPE_P = "";
		$objOrder->TRACE_TYPE_Q = "";
		$objOrder->TRACE_TYPE_R = "";
		$objOrder->TRACE_TYPE_O = "";
		$objOrder->TRACE_TYPE_M = "";
		$objOrder->TRACE_TYPE_W = "";
		$objOrder->CREATED_TIME = "";
		$objOrder->IM_LOADED = "";
		$objOrder->INTERMODAL = "";
		#$objOrder->zzzz = "";


		$objOrder->BILL_NUMBER = $tracking_number;


	    $is_return_service = (string)$body->BasicInformation->IsReturnService;
	    $objOrder->SITE_ID = ( $is_return_service ) ? "SITE5" : "SITE6" ;  //SITE5 (return) SITE6 (delivery)

	    
	    $is_longhaul = (string)$body->BasicInformation->IsLonghaul;
	    $objOrder->OP_CODE = ( $is_longhaul ) ? "DD" : "LM" ;
	    
	    
	    $objOrder->SERVICE_LEVEL = strtoupper( (string)$body->BasicInformation->Service );


		if ( substr( (string)$body->BasicInformation->Ref1Description, 0, 2 ) == "PO" )
		{
			
			$objOrder->TRACE_TYPE_P = (string)$body->BasicInformation->Ref1;
		}


		
		foreach( $body->Stops->stop as $stop  )
		{
			$attributes = $stop->attributes();
			$sequence = (int) $attributes['sequence'];
			
			$objCSZ = Nsd_trackingController::getObjCityStateZip( (string)$stop->City );
			
			switch(  $sequence  )
			{
				case "1":
					#origin
					$objOrder->ORIGCITY = $objCSZ->city;
					$objOrder->ORIGPROV = $objCSZ->state;
					$objOrder->ORIGPC = $objCSZ->zip;
					break;


				case "2":
					#care of
					$objOrder->CARE_OF_CITY = $objCSZ->city;
					$objOrder->CARE_OF_PROV = $objCSZ->state;
					$objOrder->CARE_OF_PC = $objCSZ->zip;
					break;


				case "3":
					#destination
					$objOrder->DESTCITY = $objCSZ->city;
					$objOrder->DESTPROV = $objCSZ->state;
					$objOrder->DESTPC = $objCSZ->zip;				
					break;
				
			}
			
		}


		$objRockhopper->order = $objOrder;
		
		
		$arrOrderHistory = array();

		foreach( $body->Events->event as $event  )
		{

			$attributes = $event->attributes();
			$status_code = (string) $attributes['code'];
			
			$eventDateTime = (string)$event->datetime;
			
			$year = (int) substr($eventDateTime, 0, 4);  // year
			$month = (int) substr($eventDateTime, 5, 2);  // month
			$day = (int) substr($eventDateTime, 8, 2);  // day
			$hour = (int) substr($eventDateTime, 11, 2);  // hour
			$min = (int) substr($eventDateTime, 14, 2);  // minute 
			  
			$eventDateTimeFormat = date('Y-m-d H:i:s', mktime( $hour, $min, 0, $month, $day, $year) );
			

			$objEvent = new stdClass();
			$objEvent->ORDER_ID = ""; 
			$objEvent->OS_LEG_ID = ""; 
			$objEvent->OS_STATUS_CODE = $status_code; 
			$objEvent->S_SHORT_DESCRIPTION = (string)$event->description; 
			$objEvent->OS_SF_REASON_CODE = ""; 
			$objEvent->OS_INS_DATE = $eventDateTimeFormat; 
			$objEvent->OS_CHANGED = $eventDateTimeFormat; 
			
			$arrOrderHistory[] = $objEvent;
			

/*
                    [ORDER_ID] => 5144
                    [OS_LEG_ID] =>
                    [OS_STATUS_CODE] => DOCKED
                    [S_SHORT_DESCRIPTION] => DOCKED - RECEIVED ONDOCK
                    [OS_SF_REASON_CODE] => CONTATMPT
                    [OS_INS_DATE] => 2017-09-19 10:54:56.132000
                    [OS_CHANGED] => 2017-09-19 10:54:30.895000
*/
			
		}

		$objRockhopper->order_history = $arrOrderHistory;
		
		

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r url: ".print_r($url, true);
		$logMessage .= "\n\r response: ".print_r($response, true);
		$logMessage .= "\n\r body: ".print_r($body, true);
		$logMessage .= "\n\r objRockhopper: ".print_r($objRockhopper, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $response;
		
	}



	public function getObjCityStateZip( $strInput=null )
	{
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=getObjCityStateZip
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=getObjCityStateZip

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=getObjCityStateZip

		$controllerName = "Nsd_trackingController";
		$functionName = "getObjCityStateZip";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#$strInput = "New RICHMOND VA 23224";
		
		$arrCity = explode(" ", $strInput);

		$objReturn = new stdClass();
		
		$objReturn->zip = trim(array_pop($arrCity));
		
		$objReturn->state = trim(array_pop($arrCity));
		
		$objReturn->city = trim(implode(" ", $arrCity));

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r strInput: ".print_r($strInput, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objReturn;
	}


	public function test_getXML( $file_name=null )
	{
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_getXML
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_getXML

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_getXML

		$controllerName = "Nsd_trackingController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$url = "http://dev7.metalake.net/?trace_number=20200000&flag_run=1&format=xml";
		$xml = simplexml_load_file($url);
		
		$order_type = (string)$xml->order_type;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r xml: ".print_r($xml, true);
		$logMessage .= "\n\r order_type: ".print_r($order_type, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




	public function test_exist_rockhopper( $file_name=null )
	{

		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_exist_rockhopper
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_exist_rockhopper

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_exist_rockhopper

		$controllerName = "Nsd_trackingController";
		$functionName = "test_exist_rockhopper";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$tracking_number = "13717586";
		#$tracking_number = "137300529";
		$tracking_number = "1";

	    $url = 'http://nstracking.nonstopdelivery.com/api/rest/nsdws/tracking/ws-track-shipment/' . $tracking_number;
	
	    $response = file_get_contents($url);


		$body = simplexml_load_string($response);

		$tracking_number = (string)$body->BasicInformation->TrackingNumber;

		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r tracking_number: ".print_r($tracking_number, true);
		$logMessage .= "\n\r url: ".print_r($url, true);
		$logMessage .= "\n\r response: ".print_r($response, true);
		$logMessage .= "\n\r body: ".print_r($body, true);
		$logMessage .= "\n\r TrackingNumber: ".print_r($tracking_number, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$link = JRoute::_('http://www.nonstopdelivery.com/tracking/?tracking='.$tracking_number);
		$this->setRedirect($link, $msg);



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}




	public function test_tmw4web_deliverymap( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web_deliverymap
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web_deliverymap&trace=1063701216
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web_deliverymap&trace=31065532&prod=1
		
		
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_tmw4web_deliverymap&order_status=OFP


		$controllerName = "Nsd_trackingController";
		$functionName = "test_tmw4web_deliverymap";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		$flag_production = JRequest::getVar('prod', 0);
		$order_status = JRequest::getVar('order_status', 'OFD');



		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
            
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$subdomain = 'nonstopdelivery';
            

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
            
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = '1234';
			$subdomain = 'nonstoptest';            
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		if ( true )
		{
	
			$tmp_fname = tempnam("/tmp", "COOKIE");
	
	
			#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD
	
	
	
	/*
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$loginUrl = 'https://nonstopdelivery.tmwcloud.com/login/do_login.msw';
	*/
	
	/*
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = '1234';
			$loginUrl = 'https://nonstoptest.tmwcloud.com/login/do_login.msw';
	*/
			
			$loginUrl = 'https://'.$subdomain.'.tmwcloud.com/login/do_login.msw';
			
			//init curl
			$ch = curl_init();
			
			//Set the URL to work with
			curl_setopt($ch, CURLOPT_URL, $loginUrl);
			
			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 1);
			
			//Set the post parameters
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
			
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			//execute the request (the login)
			$store = curl_exec($ch);
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
	
	
			#bill number
			$queryURL = "https://".$subdomain.".tmwcloud.com/trace/trace_class.msw?include_all_clients=false&take=10000&skip=0&page=10000&pageSize=10000&TLORDER__CURRENT_STATUS=".$order_status;
			
			
			
			curl_setopt ($ch, CURLOPT_HTTPGET, 1);
			curl_setopt ($ch, CURLOPT_URL, $queryURL);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_HEADER, 0);
			
			//execute the request
			$contentQuery = curl_exec($ch);
			
			#echo "contentQuery: <PRE>".htmlentities($contentQuery)."</PRE>";
	
			if(curl_errno($ch)){
			    echo 'Curl error: ' . curl_error($ch);
			}
	
			#print_r(curl_getinfo($ch));
	
	
			$objDataOrder = json_decode($contentQuery);
	

			$stop_type = ( $order_status == "OFD" ) ? "Delivery" : "Return" ;

			$output = array();

/*
			foreach( $objDataOrder->dataResults as $order ) 
			{
			    $filename = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$order->DESTPC.'&sensor=false';
			    $json = file_get_contents($filename);
			    $location = json_decode($json);
			
			    $output[] =  array(
			        'zip'  => $order->DESTPC,
			        'type' => $stop_type,
			        'lat'  => $location->results[0]->geometry->location->lat,
			        'lng'  => $location->results[0]->geometry->location->lng
			        );
			    #usleep(300000);
			    sleep(1);
			}
*/
			$arrZipcodes = Nsd_trackingController::test_createZipLookup();

			foreach( $objDataOrder->dataResults as $order ) 
			{

				$output[] = $order->DESTPC;
			}
			
			sort($output);
			
			$output = array_unique($output, SORT_REGULAR);
			
			$output = array_values($output);
	
			
			$arrZipcodes = Nsd_trackingController::test_createZipLookup();

			$output2 = array();

			foreach( $output as $order ) 
			{

				$zip = $order;
				$lat = Nsd_trackingController::findMyValue( $arrZipcodes, 0, $zip, 1 );
				$lng = Nsd_trackingController::findMyValue( $arrZipcodes, 0, $zip, 2 );


			    $output2[] =  array(
			        'zip'  => $zip,
			        'type' => $stop_type,
			        'lat'  => $lat,
			        'lng'  => $lng
			        );

			    #usleep(300000);
			    #sleep(1);
			}		
			
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
			$logMessage .= "\n\r contentQuery: ".print_r($objDataOrder, true);
			$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
			$logMessage .= "\n\r output: ".print_r($output, true);
			$logMessage .= "\n\r output2: ".print_r($output2, true);

			
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

	/*

			$detailLineID = $objDataOrder->dataResults[0]->DETAIL_LINE_ID;
	
	
	
	
	
	
	
	
	
	
	
	
	
			$detailURL = "https://".$subdomain.".tmwcloud.com/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;
			
	
			curl_setopt ($ch, CURLOPT_HEADER, 0);
			curl_setopt ($ch, CURLOPT_HTTPGET, 1);
			curl_setopt ($ch, CURLOPT_URL, $detailURL);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt ($ch, CURLOPT_ENCODING, "");
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_VERBOSE, 0);
			
			//execute the request
			$detailQuery = curl_exec($ch);
	
			#$headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT ); // request headers
			#echo "headerSent: <PRE>".htmlentities($headerSent)."</PRE>";
			#echo "detailQuery: <PRE>".htmlentities($detailQuery)."</PRE>";
			#print_r(curl_getinfo($ch));
	
	
	
			if (curl_error($ch)) {
			    $curl_error = curl_error($ch);
			}
	
			$objDataOrderDetail = json_decode($detailQuery);
			
			$arrOrderSteps = $objDataOrderDetail->ODRSTAT->data;
	
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r curl_error: ".print_r(json_decode($curl_error), true);
			$logMessage .= "\n\r detailLineID: ".print_r($detailLineID, true);
			$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
			$logMessage .= "\n\r detailQuery: ".print_r($detailQuery, true);
			$logMessage .= "\n\r objDataOrderDetail: ".print_r($objDataOrderDetail, true);
			$logMessage .= "\n\r arrOrderSteps: ".print_r($arrOrderSteps, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
*/
	
	
			curl_close($ch);
			unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
			unset($ch);
		
		}
		else
		{

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
		}

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	



	public function test_createZipLookup( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_createZipLookup

		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_createZipLookup

		$controllerName = "Nsd_trackingController";
		$functionName = "test_createZipLookup";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$arrZipcodes = array();

		$file = fopen('zipcodes.txt', 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
		   //$line[0] = '1004000018' in first iteration
		   #print_r($line);
		   $arrZipcodes[] = $line;

/*
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r line: ".print_r($line, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
*/

		}
		
		
		
		
		fclose($file);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r arrZipcodes: ".print_r($arrZipcodes, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		#$myfile = fopen("/var/www/vhosts/metalake.net/subdomains/dev7/httpdocs/ziplookup.txt", "a");
		
		#fwrite($myfile, $arrZipcodes);
		
		#file_put_contents("/var/www/vhosts/metalake.net/subdomains/dev7/httpdocs/ziplookup.txt", print_r($arrZipcodes, true), FILE_APPEND );
		
		#fclose($myfile);

$fp = fopen('/var/www/vhosts/metalake.net/subdomains/dev7/httpdocs/ziplookup.txt', 'w');
fwrite($fp, print_r($arrZipcodes, TRUE));
fclose($fp);

/*
		$zip = "02898";
		$lat = Nsd_trackingController::findMyValue( $arrZipcodes, 0, $zip, 1 );
		$lng = Nsd_trackingController::findMyValue( $arrZipcodes, 0, $zip, 2 );
		
		



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		#$logMessage .= "\n\r arrZipcodes: ".print_r($arrZipcodes, true);
		$logMessage .= "\n\r zip: ".print_r($zip, true);
		$logMessage .= "\n\r lat: ".print_r($lat, true);
		$logMessage .= "\n\r lng: ".print_r($lng, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
*/


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $arrZipcodes;
	}


	public function findMyValue( $array, $search_key, $search_str, $key	) 
	{
	    foreach ($array as $v) {
	        if ($v[$search_key] == $search_str) {
	            return $v[$key];
	        }
	    }
	    return 'NOT_FOUND';
	}



	public function test_getTMWZipcodes( $objInput )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web_deliverymap
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web_deliverymap&trace=1063701216
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web_deliverymap&trace=31065532&prod=1
		
		
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_getTMWZipcodes&order_status=OFP


		$controllerName = "Nsd_trackingController";
		$functionName = "test_getTMWZipcodes";
		
		$db = JFactory::getDBO();
		
		#$flag_production = 0;
		
		#$flag_production = JRequest::getVar('prod', 0);
		#$order_status = JRequest::getVar('order_status', 'OFD');

		$flag_production = $objInput->flag_production;

		$order_status = $objInput->order_status;


		#$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
            
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$subdomain = 'nonstopdelivery';
            

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
            
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = '1234';
			$subdomain = 'nonstoptest';            
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		if ( true )
		{
	
			$tmp_fname = tempnam("/tmp", "COOKIE");
	
	
			#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD
	
	
	
	/*
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$loginUrl = 'https://nonstopdelivery.tmwcloud.com/login/do_login.msw';
	*/
	
	/*
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = '1234';
			$loginUrl = 'https://nonstoptest.tmwcloud.com/login/do_login.msw';
	*/
			
			$loginUrl = 'https://'.$subdomain.'.tmwcloud.com/login/do_login.msw';
			
			//init curl
			$ch = curl_init();
			
			//Set the URL to work with
			curl_setopt($ch, CURLOPT_URL, $loginUrl);
			
			// ENABLE HTTP POST
			curl_setopt($ch, CURLOPT_POST, 1);
			
			//Set the post parameters
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
			
			//Handle cookies for the login
			curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			//execute the request (the login)
			$store = curl_exec($ch);
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
	
	
			#bill number
			$queryURL = "https://".$subdomain.".tmwcloud.com/trace/trace_class.msw?include_all_clients=false&take=10000&skip=0&page=10000&pageSize=10000&TLORDER__CURRENT_STATUS=".$order_status;
			
			
			
			curl_setopt ($ch, CURLOPT_HTTPGET, 1);
			curl_setopt ($ch, CURLOPT_URL, $queryURL);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_HEADER, 0);
			
			//execute the request
			$contentQuery = curl_exec($ch);
			
			#echo "contentQuery: <PRE>".htmlentities($contentQuery)."</PRE>";
	
			if(curl_errno($ch)){
			    echo 'Curl error: ' . curl_error($ch);
			}
	
			#print_r(curl_getinfo($ch));
	
	
			$objDataOrder = json_decode($contentQuery);
	

			$stop_type = ( $order_status == "OFD" ) ? "Delivery" : "Return" ;

			$output = array();


			$arrZipcodes = Nsd_trackingController::test_createZipLookup();

			foreach( $objDataOrder->dataResults as $order ) 
			{

				$output[] = $order->DESTPC;
			}
			
			sort($output);
			
			$output = array_unique($output, SORT_REGULAR);
			
			$output = array_values($output);
	
			
			$arrZipcodes = Nsd_trackingController::test_createZipLookup();

			$output2 = array();

			foreach( $output as $order ) 
			{

				$zip = $order;
				$lat = Nsd_trackingController::findMyValue( $arrZipcodes, 0, $zip, 1 );
				$lng = Nsd_trackingController::findMyValue( $arrZipcodes, 0, $zip, 2 );


			    $output2[] =  array(
			        'zip'  => $zip,
			        'type' => $stop_type,
			        'lat'  => $lat,
			        'lng'  => $lng
			        );

			    #usleep(300000);
			    #sleep(1);
			}		
			
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
			$logMessage .= "\n\r contentQuery: ".print_r($objDataOrder, true);
			$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
			$logMessage .= "\n\r output: ".print_r($output, true);
			$logMessage .= "\n\r output2: ".print_r($output2, true);

			
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

	/*

			$detailLineID = $objDataOrder->dataResults[0]->DETAIL_LINE_ID;
	
	
	
	
	
	
	
	
	
	
	
	
	
			$detailURL = "https://".$subdomain.".tmwcloud.com/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;
			
	
			curl_setopt ($ch, CURLOPT_HEADER, 0);
			curl_setopt ($ch, CURLOPT_HTTPGET, 1);
			curl_setopt ($ch, CURLOPT_URL, $detailURL);
			curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt ($ch, CURLOPT_ENCODING, "");
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($ch, CURLOPT_VERBOSE, 0);
			
			//execute the request
			$detailQuery = curl_exec($ch);
	
			#$headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT ); // request headers
			#echo "headerSent: <PRE>".htmlentities($headerSent)."</PRE>";
			#echo "detailQuery: <PRE>".htmlentities($detailQuery)."</PRE>";
			#print_r(curl_getinfo($ch));
	
	
	
			if (curl_error($ch)) {
			    $curl_error = curl_error($ch);
			}
	
			$objDataOrderDetail = json_decode($detailQuery);
			
			$arrOrderSteps = $objDataOrderDetail->ODRSTAT->data;
	
	
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r curl_error: ".print_r(json_decode($curl_error), true);
			$logMessage .= "\n\r detailLineID: ".print_r($detailLineID, true);
			$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
			$logMessage .= "\n\r detailQuery: ".print_r($detailQuery, true);
			$logMessage .= "\n\r objDataOrderDetail: ".print_r($objDataOrderDetail, true);
			$logMessage .= "\n\r arrOrderSteps: ".print_r($arrOrderSteps, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
*/
	
	
			curl_close($ch);
			unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
			unset($ch);
		
		}
		else
		{

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
		}

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	




	public function test_arrayMerge( $file_name=null )
	{
		
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_arrayMerge
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_arrayMerge

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_arrayMerge

		$controllerName = "Nsd_trackingController";
		$functionName = "test_arrayMerge";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$jsonONE = '[{"zip":"01266","type":"Return","lat":42.3165041,"lng":-73.4062342},{"zip":"01364","type":"Return","lat":42.5903491,"lng":-72.3099167},{"zip":"01431","type":"Return","lat":42.6792111,"lng":-71.8215545},{"zip":"01523","type":"Return","lat":42.4850811,"lng":-71.6792008},{"zip":"01564","type":"Return","lat":null,"lng":null},{"zip":"01564","type":"Return","lat":42.4551075,"lng":-71.785972}]';
		
		$jsonTWO = '[{"zip":"11023","type":"Delivery","lat":40.8007661,"lng":-73.7330753},{"zip":"11023","type":"Delivery","lat":40.8007661,"lng":-73.7330753},{"zip":"11030","type":"Delivery","lat":40.7960005,"lng":-73.6830521},{"zip":"11101","type":"Delivery","lat":40.7443091,"lng":-73.9418603},{"zip":"11106","type":"Delivery","lat":40.7595044,"lng":-73.9271644},{"zip":"11109","type":"Delivery","lat":40.7449081,"lng":-73.9569225},{"zip":"11205","type":"Delivery","lat":40.6945036,"lng":-73.9565551}]';		




		$jsonONE = '[{"zip":"01266","type":"Return"},{"zip":"01364","type":"Return"},{"zip":"11023","type":"Return"}]';
		
		$jsonTWO = '[{"zip":"11205","type":"Delivery"},{"zip":"11023","type":"Delivery"},{"zip":"11205","type":"Delivery"}]';		



		$arrOne = json_decode($jsonONE);
		
		$arrTWO = json_decode($jsonTWO);
		
		
		$arrMerge = array_merge_recursive($arrOne, $arrTWO);


			sort($arrMerge);
			
			$arrMerge = array_unique($arrMerge, SORT_REGULAR);
			
			$arrMerge = array_values($arrMerge);

		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r jsonONE: ".print_r($jsonONE, true);
		$logMessage .= "\n\r arrOne: ".print_r($arrOne, true);
		$logMessage .= "\n\r jsonTWO: ".print_r($jsonTWO, true);
		$logMessage .= "\n\r arrTWO: ".print_r($arrTWO, true);
		$logMessage .= "\n\r arrMerge: ".print_r($arrMerge, true);
		
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_is_curl_installed() {
	    if  (in_array  ('curl', get_loaded_extensions())) {
	        return true;
	    }
	    else {
	        return false;
	    }
	}

	public function test_curl( )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_curl

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_curl

		$controllerName = "Nsd_trackingController";
		$functionName = "test_curl";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$tmp_fname = tempnam("/var/www/html/nsdtracking.com/public_html/tmp", "COOKIE");
		$tmp_fname = tempnam("/tmp", "COOKIE");
		#$tmp_fname = tempnam("/var/www/html/nsdtracking.com/public_html/tmp", "COOKIE");


		#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD

		
		$theUrl = 'http://dev7.metalake.net/?user=TRACKING&pswd=1234&trace_number=20200000&format=json';
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $theUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		//curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		if(curl_errno($ch)){
			$curlError =  curl_error($ch);
		}
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r curlError: ".print_r($curlError, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r result: ".print_r($result, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

	public function test_parse_comment( $file_name=null )
	{
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_parse_comment

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_parse_comment

		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_parse_comment

		$controllerName = "Nsd_trackingController";
		$functionName = "test_parse_comment";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$strIN = "EDI Code: 'X3' 'NS'-PERRIS, CA 5/24/2018 1:45:00 PM CT";
		$strIN = "EDI Code: 'X6' 'NS'-LA GRANGE, IL 5/17/2018 8:46:00 PM";

		$arrOne = explode( "-", $strIN );
		$arrTwo = explode( ",", $arrOne[1] );
		$arrThree = explode( " ", $arrTwo[1] );
		$strOUT = $arrTwo[0].", ".$arrThree[1];
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r strIN: ".print_r($strIN, true);
		$logMessage .= "\n\r arrOne: ".print_r($arrOne, true);
		$logMessage .= "\n\r arrTwo: ".print_r($arrTwo, true);
		$logMessage .= "\n\r arrThree: ".print_r($arrThree, true);
		$logMessage .= "\n\r strOUT: ".print_r($strOUT, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


	public function test_poc_tmapi( $file_name=null )
	{
		
		# http://dev7.metalake.net/index.php?option=com_nsd_tracking&task=test_poc_tmapi
		
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_poc_tmapi

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_poc_tmapi

		$controllerName = "Nsd_trackingController";
		$functionName = "test_poc_tmapi";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tmapi_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tmapi_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tmapi_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tmapi_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$subdomain 	= "nonstoptest";
		$loginUrl 	= 'https://'.$subdomain.'.tmwcloud.com/import/login/';
		$tm4web_usr = "TM_API";
		$tm4web_pwd = "981a8e9169fa53dd4fc9b7ab79c3a860";

		$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
		
		//init curl
		$ch = curl_init();
		
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		
		// ENABLE HTTP POST
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//Set the post parameters
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$tm4web_usr.'&password='.$tm4web_pwd);
		
		//Handle cookies for the login
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
		curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);			
		
		//execute the request (the login)
		$store = curl_exec($ch);
		
		$objReturnCredentials = json_decode($store);
		
		$sessionId = $objReturnCredentials->sessionId;
		
		
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r store: ".print_r($store, true);
		$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
		$logMessage .= "\n\r objReturnCredentials: ".print_r($objReturnCredentials, true);
		$logMessage .= "\n\r sessionId: ".print_r($sessionId, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$arrOrders = array();
		$arrOrder = array( 
		"name"=>"The Home Depot Direct-YOW",
		"billToCode"=>"",
		"startZone"=>"",  // need zones
		"endZone"=>"",
		"pickUpBy"=>"",  // timestamp
		"pickUpByEnd"=>"", // timestamp
		"deliverBy"=>"",  // timestamp
		"deliverByEnd"=>"", // timestamp
		"customer"=>"",
		"destination"=>"", // destination customer code
		"origin"=>"", // origin customer code
		);
		$arrOrders[] = $arrOrder;



		$arrBody = array( 
		"sessionId"=>$sessionId,
		"orders"=>$arrOrder
		);






		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

	public function test_alerts( )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_tracking&task=test_alerts
		# http://tracking.shipnsd.com/index.php?option=com_nsd_tracking&task=test_alerts
		
		$controllerName = "Nsd_trackingController";
		$functionName = "test_alerts";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}			

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objAlert = new stdClass();
		#$objAlert->recipients = array( 'support@metalake.com' );
		$objAlert->recipients = array( '7039305383@txt.att.net', 'lewsawyer@gmail.com' );
		$objAlert->subject = "Test Alert Subject";
		$objAlert->body = "Test Alert Body <br><br> second line";
		
		$resultAlert = Nsd_trackingController::sendAlert( $objAlert );

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r resultAlert: ".print_r($resultAlert, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "END | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	



	


	public function test_findspace( $file_name=null )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_findspace

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_findspace

		$controllerName = "Nsd_trackingController";
		$functionName = "test_findspace";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$subject = "97920434/WA40454273";
		$pattern = " ";
		
		if ( !strpos($subject, $pattern) !== false )
		{
		   $helloWorld = "Pattern found in subject";
		}
		else
		{
		   $helloWorld =  "No pattern found in subject";
		}

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}






	public static function test_createPDF()
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_createPDF

		#manual
		#https://mpdf.github.io/
		
		
			$className = "Nsd_trackingController";
			$functionName = "test_createPDF";

			$db 	= JFactory::getDBO();

			$app = JFactory::getApplication();
	
			$objParams = new stdClass();
	
			$params         						= $app->getParams();
			#$objParams->path_to_pod_directory	= $params->get('path_to_pod_directory');
			#$objParams->path_to_receipts_directory	= "/var/www/vhosts/metalake.net/subdomains/nsddev/httpdocs/media/com_nsd_tracking/contactlesspod"
			
			#$pdfpath = "/var/www/vhosts/metalake.net/subdomains/nsddev/httpdocs/media/com_nsd_tracking/contactlesspod";
			$pdfpath = "/var/www/html/media/com_nsd_tracking/contactlesspod";
			
			$flag_production = 0;

			if ( $flag_production )
			{
	
	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "tracking_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "tracking_prod";
	
			}
			else
			{
	
	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "tracking_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "tracking_prod";
		
			}	
		
			$stamp_start_micro = microtime(true);

			$logMessage = "START | ".$controllerName." | ".$functionName;	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r objParams: ".print_r($objParams, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


	$objReceipt = new stdClass();

	$objReceipt->receipt_file_name = "BSF_Receipt_".$objReceiptInput->user_account_number."_".date('YmdHis').".pdf";
	
				
	$objReceipt->receipt_body = "";
	$objReceipt->receipt_body .= "<img src='/templates/nonstopdelivery/images/logo.png' width='20%'>";
	$objReceipt->receipt_body .= "<div style='font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
	
	$objReceipt->receipt_body .= "<h2>Order Number: 31913953</h2>";
	#$objReceipt->receipt_body .= "<b>".date( "M d, Y g:i A", strtotime($objReceiptInput->update_datetime) )."</b>";
	#$objReceipt->receipt_body .= "<p>We have completed your transaction and have provided this statement for your records.</p>";


	$objReceipt->receipt_body .= "<table style='width:100%; font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
	$objReceipt->receipt_body .= "<thead>";
	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<th style='width:34%;text-align:left;'></th>";
	$objReceipt->receipt_body .= "<th style='width:33%;text-align:left;'></th>";
	$objReceipt->receipt_body .= "<th style='width:33%;text-align:left;'></th>";
	$objReceipt->receipt_body .= "</tr>";
	$objReceipt->receipt_body .= "</thead>";
	$objReceipt->receipt_body .= "<tbody>";
	$objReceipt->receipt_body .= "<tr>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "<b>Origin (Shipper)</b><br />";	
	$objReceipt->receipt_body .= "J.C. PENNEY CORPORATION, INC.";
	$objReceipt->receipt_body .= "</td>";


	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "<b>Destination (Consignee)</b><br />";
	$objReceipt->receipt_body .= "MAXINE STEELE SMITH<br />";
	$objReceipt->receipt_body .= "5478 N 56TH ST<br />";
	$objReceipt->receipt_body .= "MILWAUKEE, WI 53218<br />";
	$objReceipt->receipt_body .= "</td>";


	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "<b>Service Level</b><br />";
	$objReceipt->receipt_body .= "WHITEGLOVE<br />";
	$objReceipt->receipt_body .= "<br />";
	$objReceipt->receipt_body .= "<b>Customer Reference</b><br />";
	$objReceipt->receipt_body .= "325687999_799<br />";
	$objReceipt->receipt_body .= "</td>";

	
	$objReceipt->receipt_body .= "</tr>";
	$objReceipt->receipt_body .= "</tbody>";
	$objReceipt->receipt_body .= "</table>";

	$objReceipt->receipt_body .= "<h2>Order Information</h2>";

	$objReceipt->receipt_body .= "<table style='width:100%; font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
	$objReceipt->receipt_body .= "<thead>";
	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<th style='width:40%;text-align:left;'>DESCRIPTION</th>";
	$objReceipt->receipt_body .= "<th style='width:5;text-align:left;'>QTY</th>";
	$objReceipt->receipt_body .= "<th style='width:10;text-align:left;'>WEIGHT</th>";
	$objReceipt->receipt_body .= "<th style='width:15%;text-align:left;'>PRODUCT RECEIVED</th>";
	$objReceipt->receipt_body .= "<th style='width:15%;text-align:left;'>PRODUCT DAMAGED</th>";
	$objReceipt->receipt_body .= "<th style='width:15%;text-align:left;'>PACKAGING DAMAGED</th>";
	$objReceipt->receipt_body .= "</tr>";
	$objReceipt->receipt_body .= "</thead>";
	$objReceipt->receipt_body .= "<tbody>";



	$objReceipt->receipt_body .= "<tr>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "ASSEMBLY-799-9454 SY DAV PLPT CK (SLY)";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "1";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "83.0";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "YES";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "NO";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "NO";	
	$objReceipt->receipt_body .= "</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td  colspan='6'>";
	$objReceipt->receipt_body .= "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris";	
	$objReceipt->receipt_body .= "</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "ASSEMBLY-799-9454 SY DAV PLPT CK (SLY)";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "1";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "83.0";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "YES";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "NO";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "NO";	
	$objReceipt->receipt_body .= "</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td  colspan='6'>";
	$objReceipt->receipt_body .= "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris";	
	$objReceipt->receipt_body .= "</td>";
	$objReceipt->receipt_body .= "</tr>";
	
	$objReceipt->receipt_body .= "<tr>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "ASSEMBLY-799-9454 SY DAV PLPT CK (SLY)";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "1";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "83.0";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "YES";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "NO";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "NO";	
	$objReceipt->receipt_body .= "</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td  colspan='6'>";
	$objReceipt->receipt_body .= "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris";	
	$objReceipt->receipt_body .= "</td>";
	$objReceipt->receipt_body .= "</tr>";
	
	$objReceipt->receipt_body .= "<tr>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "ASSEMBLY-799-9454 SY DAV PLPT CK (SLY)";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "1";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "83.0";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "YES";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "NO";	
	$objReceipt->receipt_body .= "</td>";

	$objReceipt->receipt_body .= "<td>";
	$objReceipt->receipt_body .= "NO";	
	$objReceipt->receipt_body .= "</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td  colspan='6'>";
	$objReceipt->receipt_body .= "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris";	
	$objReceipt->receipt_body .= "</td>";
	$objReceipt->receipt_body .= "</tr>";				


	$objReceipt->receipt_body .= "</tbody>";
	$objReceipt->receipt_body .= "</table>";


	$objReceipt->receipt_body .= "<h2>Delivery Feedback</h2>";

	$objReceipt->receipt_body .= "<table style='width:100%; font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
	$objReceipt->receipt_body .= "<thead>";
	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<th style='width:40%;text-align:left;'></th>";
	$objReceipt->receipt_body .= "<th style='width:60%;text-align:left;'></th>";
	$objReceipt->receipt_body .= "</tr>";
	$objReceipt->receipt_body .= "</thead>";
	$objReceipt->receipt_body .= "<tbody>";


	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td>30 MINUTE COURTESY PRECALL</td>";
	$objReceipt->receipt_body .= "<td>YES</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td>ASSEMBLY REQUIRED FOR THE FREIGHT</td>";
	$objReceipt->receipt_body .= "<td>WAIVED</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td>DELIVER TO ROOM OF CHOICE</td>";
	$objReceipt->receipt_body .= "<td>YES</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td>OLD MATTRESS REMOVAL</td>";
	$objReceipt->receipt_body .= "<td>YES</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "<tr>";
	$objReceipt->receipt_body .= "<td>PRODUCT UNPACK & DEBRIS REMOVAL</td>";
	$objReceipt->receipt_body .= "<td>YES</td>";
	$objReceipt->receipt_body .= "</tr>";

	$objReceipt->receipt_body .= "</tbody>";
	$objReceipt->receipt_body .= "</table>";


	$objReceipt->receipt_body .= "<p><b>Please place any comments here, including any notation of product condition, damage, or refusal.</b>";
	$objReceipt->receipt_body .= "<br />Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris";
	$objReceipt->receipt_body .= "</p>";


	$objReceipt->receipt_body .= "<h2>Customer Delivery Acceptance</h2>";
	$objReceipt->receipt_body .= "<p>Delivery Accepted: YES</p>";
	
	
	$objReceipt->receipt_body .= "<p>By typing your name below you are electronically signing this document.";
	$objReceipt->receipt_body .= "<br>Signed: BOB ROBERTSON";
	$objReceipt->receipt_body .= "<br>Date: 06/10/2020</p>";

	
	$objReceipt->receipt_body .= "</div>";

	$receipt_body = $objReceipt->receipt_body;

		
/*
			$receipt_body = "";
			
			$receipt_body .= "<img src='templates/cfnova/images/logo.png' width='25%'>";
			$receipt_body .= "<div style='font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
			
			$receipt_body .= "<h2>Receipt of Contribution</h2>";
			$receipt_body .= "<p>We have completed your transaction and have provided this statement for your records. Thank you for your support!</p>";

			$receipt_body .= "<table style='width:100%; font-family: sans-serif; font-size: 11pt; color: #000; font-weight: normal;vertical-align:top;'>";
			$receipt_body .= "<thead>";
			$receipt_body .= "<tr>";
			$receipt_body .= "<th style='width:50%;text-align:left;'>Your Donation</th>";
			$receipt_body .= "<th style='width:50%;text-align:left;'></th>";
			$receipt_body .= "</tr>";
			$receipt_body .= "</thead>";

			$receipt_body .= "<tbody>";
			$receipt_body .= "<tr>";

			$receipt_body .= "<td>";
			$receipt_body .= "February 25, 2020<br />";
			$receipt_body .= "$100.00 monthly<br />";
			$receipt_body .= "Test Fund of Northern Virginia<br />";
			$receipt_body .= "A Donor Advised Fund<br />";
			
			//if Designation
			$receipt_body .= "<br />";
			$receipt_body .= "Designated for:<br />";
			$receipt_body .= "The thing that I chose to designate<br />";

			//if Tribute
			$receipt_body .= "<br />";
			$receipt_body .= "In Celebration of:<br />";
			$receipt_body .= "the name of the tributee<br />";
			$receipt_body .= "the address of the tributee<br />";
			$receipt_body .= "</td>";


			$receipt_body .= "<td>";
			$receipt_body .= "Firstname Lastname<br />";
			$receipt_body .= "List as: Preferred Name/Anonymous<br />";
			$receipt_body .= "7037390990<br />";
			$receipt_body .= "ross@myemail.com<br />";
			$receipt_body .= "Company Name<br />";
			//if company match
			$receipt_body .= "My company will match my gift.";
			$receipt_body .= "</td>";
			$receipt_body .= "/tr>";
			$receipt_body .= "</tbody>";
			$receipt_body .= "</table>";

			
			$receipt_body .= "<h3>Receipt Total: $100.00</h3>";
			
			$receipt_body .= "<p>The Community Foundation for Northern Virginia is a 501(c)3 charitable organization (Tax ID 51-0232459) and all contributions are tax deductible to the fullest extent of the law. No goods or services were given in exchange for your donation.  Please save this receipt for your tax records.</p>";
			
			$receipt_body .= "<p style='font-size:10pt;'><em>No distribution from any donor advised fund of the Community Foundation may be used to fulfill pledges of the donor, donor advisors, and related parties, or used to secure non-incidental benefits from the distribution recipient for the donor, donor advisors, and related parties.  In addition, no donors, donor advisors, or related parties may receive any form of grant, loan, compensation, or similar payment (including expense reimbursements) from donor advised funds.  The Community Foundation has exclusive legal control over the contributed assets.</em></p>";

			$receipt_body .= "<p>Please direct any questions about your contribution to Josiah Day, Director of Donor Relations, at 703-879-7636 or josiah.day@cfnova.org.</p>";

			$receipt_body .= "<p>Community Foundation for Northern Virginia<br />2940 Hunter Mill Road, Suite 201<br />Oakton, VA 22124<br />www.cfnova.org</p>";
			
			$receipt_body .= "</div>";
*/

			$mpdf = new \Mpdf\Mpdf(
				[
				    'margin_left' => 5,
				    'margin_right' => 5,
				    'margin_top' => 5,
				    'margin_bottom' => 5,
				    'margin_header' => 5,
				    'margin_footer' => 5,
				]					
			);
			
			#$mpdf->SetHTMLHeader('<img src="templates/cfnova/images/logo.png" width="25%">');				
			
			$mpdf->WriteHTML($receipt_body);
			
			$filename = "test_".date('YmdHis').".pdf";
			#$fullpath = $objParams->path_to_pod_directory."/".$filename;
			$fullpath = $pdfpath."/".$filename;


			$mpdf->Output( $fullpath, \Mpdf\Output\Destination::FILE );			



			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r filename: ".print_r($filename, true);
			$logMessage .= "\n\r fullpath: ".print_r($fullpath, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

	
			$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
			$logMessage .= "\n\r \n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}



	public static function test_pruneDirectoryPDF()
	{
		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_pruneDirectoryPDF

		#manual
		#https://mpdf.github.io/
		
		
			$className = "Nsd_trackingController";
			$functionName = "test_pruneDirectoryPDF";

			$db 	= JFactory::getDBO();

			$app = JFactory::getApplication();
	
			$objParams = new stdClass();
	
			$params         						= $app->getParams();
			$objParams->path_to_pod_directory	= $params->get('path_to_pod_directory');
			
			#$pdfpath = "/var/www/vhosts/metalake.net/subdomains/nsddev/httpdocs/media/com_nsd_tracking/contactlesspod";
			#$pdfpath = "/var/www/html/media/com_nsd_tracking/contactlesspod";
			
			$flag_production = 0;

			if ( $flag_production )
			{
	
	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "tracking_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "tracking_prod";
	
			}
			else
			{
	
	            $objLogger = new stdClass();
	            $objLogger->logger = 0;
	            $objLogger->logFile = "tracking_verbose";
	            $objLogger->loggerProd = 0;
	            $objLogger->logFileProd = "tracking_prod";
		
			}	
		
			$stamp_start_micro = microtime(true);

			$logMessage = "START | ".$controllerName." | ".$functionName;	
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r objParams: ".print_r($objParams, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

			$logPath = $objParams->path_to_pod_directory."/";
			
			$numMinutes = 24 * 60 * 2;  // hours * minutes * days
			
			$command = "find ".$logPath."*.pdf -mmin +".$numMinutes." -type f -exec rm -f {} + 2>&1" ;  //60 minutes = 1 hours
	
			$logMessage = "INSIDE C | ".$className." | ".$functionName;	
			$logMessage .= "\n\r command: ".print_r($command, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
			exec( $command, $output, $return_var );	



			$logMessage = "INSIDE | ".$className." | ".$functionName;	
			$logMessage .= "\n\r filename: ".print_r($filename, true);
			$logMessage .= "\n\r fullpath: ".print_r($fullpath, true);
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


			$stamp_end_micro = microtime(true);
			$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

	
			$logMessage = "END | ".$className." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
			$logMessage .= "\n\r \n\r";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}





// !FUNCTION TEMPLATE

	public function test_function_template( $file_name=null )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_function_template

		# http://nsddev.metalake.net/index.php?option=com_nsd_tracking&task=test_function_template

		$controllerName = "Nsd_trackingController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}

}
