<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Nsd_tracking model.
 *
 * @since  1.6
 */
class Nsd_trackingModelTrack extends JModelItem
{

	// !===== TRUCKMATE

	// !getTMW





	public function getTMW ( $objForm )  // 3/6/19 - Prod - included calls to curl session creation and use.
	{


		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getTMW";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('tmw4web_user');
		$paramsComponent->tmw4web_password			= $params->get('tmw4web_password');
		$paramsComponent->tmw4web_subdomain			= $params->get('tmw4web_subdomain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_subdomain_test	= $params->get('tmw4web_subdomain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_subdomain;   
			
		}
		else
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_subdomain_test;   		
		}
		

		$trace_number = $objForm->trace_number;

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
            
       

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerRate = 0;
            $objLogger->logFileRate = "tracking_rate";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
            
       
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r subdomain: ".print_r($subdomain, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$objReturn = new stdClass();

		if ( $tm4web_usr == "" || $tm4web_pwd == "" )
		{

			$objReturn->error = 1;
			$objReturn->error_code = 99;
			$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
		}
		else
		{
	
			$validInterval = 1;
				
			if ( $objForm->flag_api == "1" )
			{

				#$query = "select * from htc_nsd_tracking_activity_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$query = "select trace_milliseconds from htc_nsd_tracking_activity_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$db->setQuery($query);
				$objAPILog = $db->loadObject();


				$millisecondsNow = round(microtime(true) * 1000);
				
				#10000 = 10 seconds or 360 requests an hour
				#5000 = 5 seconds or 720 requests an hour
				#2000 = 2 seconds or 1200 requests an hour
				#2000 = 2 seconds or 1800 requests an hour
				#$intervalInMilliseconds = 1;
				$intervalInMilliseconds = $paramsComponent->api_rate_limit_milliseconds;
				
				
				$diffMilliseconds = $millisecondsNow - $objAPILog->trace_milliseconds;
	
				$validInterval = ( $diffMilliseconds > $intervalInMilliseconds ) ? 1 : 0 ;

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | validInterval";
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objAPILog: ".print_r($objAPILog, true);
					$logMessage .= "\n\r millisecondsNow: ".print_r($millisecondsNow, true);
					$logMessage .= "\n\r diffMilliseconds: ".print_r($diffMilliseconds, true);
					$logMessage .= "\n\r validInterval: ".print_r($validInterval, true);
					$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerRate ) { MetalakeHelperCore::logger( $objLogger->logFileRate, $logMessage ); }
				
			}	
			
			if( $validInterval == 1 )
			{

				if ( trim($trace_number) != "" && !strpos(trim($trace_number), " ") !== false )
				{

					
					
					$objCookie = new stdClass();
					$objCookie->tm4web_usr = $tm4web_usr;
					$objCookie->tm4web_pwd = $tm4web_pwd;
					$objCookie->domain = $domain;					
					$objCookie->cookieName = $objCookie->domain.".".$objCookie->tm4web_usr;
					$objCookie->cookieFilePath = JPATH_SITE."/tmp/".$objCookie->cookieName;
			
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$msg;
					$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }	
					
			
					if ( !JFile::exists( $objCookie->cookieFilePath ) )
					{
						#call func createCurlCookie
						$objCookie = Nsd_trackingController::createCurlCookie( $objCookie );
						
						if ( $objCookie->result == "1" )
						{
							$msg = "cURL cookie";
			
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | ".$msg;
							$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }					
							
						}
						else
						{
							#return error
							
							$objReturn->error = 1;
							$objReturn->error_code = 99;
							$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password 1";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
							
						}
						
					}
					else
					{

						$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Curl Session Cookie Exists";
						$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }							
						
					}
					
			
					if ( JFile::exists( $objCookie->cookieFilePath ) )
					{
						#cookie file exists
				
						$ch = curl_init();
				
						
						#bill number
						$queryURL = "https://".$objCookie->domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$objForm->trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
						curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);						
						
						//execute the request
						//$contentQuery = curl_exec($ch);


						ob_start();      // prevent any output
			
						$contentQuery = curl_exec ($ch); // execute the curl command
						
						if ( curl_errno($ch) )
						{
						    $objCookie->curl_error = curl_error($ch);
						}			
				
						ob_end_clean();  // stop preventing output


						
						$objDataOrder = json_decode($contentQuery);
				
				
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Bill Number ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r objCookie: ".print_r($objCookie, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($objForm->trace_number, true);
						$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
			
			
			
						if ( $objDataOrder == "" )
						{
							# no data due to authentication
			
							# create cookie for user
							$objCookie = Nsd_trackingController::createCurlCookie( $objCookie );
							
							if ( $objCookie->result == "1" )
							{
			
			
								# Try again
								$queryURL = "https://".$objCookie->domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$objForm->trace_number;
								
								curl_setopt ($ch, CURLOPT_HTTPGET, 1);
								curl_setopt ($ch, CURLOPT_URL, $queryURL);
								curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
								curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
								curl_setopt ($ch, CURLOPT_HEADER, 0);
								curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
								curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
								
								//execute the request
								$contentQuery = curl_exec($ch);
								
								$objDataOrder = json_decode($contentQuery);
						
						
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Bill Number ";
								$logMessage .= "\n\r ch: ".print_r($ch, true);
								$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
								$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
								$logMessage .= "\n\r trace_number: ".print_r($objForm->trace_number, true);
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
			
							}				
							else
							{
								$objReturn->error = 1;
								$objReturn->error_code = 99;
								$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
					
								$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password 2";
								$logMessage .= "\n\r ";
								if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
								if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
			
							}				
							
						}
			
			
						if ( $contentQuery != "" && $objForm->flag_api == "1" )
						{
							#get api display level and set it.
			
							$query = "select * from htc_nsd_tracking_api_clients where name = '".$objForm->tmw4web_user."' and state = '1' ";
							$db->setQuery($query);
							$objApiAgent = $db->loadObject();
			
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | htc_nsd_tracking_api_clients ";
							$logMessage .= "\n\r query: ".print_r($query, true);
							$logMessage .= "\n\r objApiAgent: ".print_r($objApiAgent, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
			
							if ( is_object($objApiAgent) )
							{
								$objReturn->id_level = $objApiAgent->id_level;						
							}
							else
							{
								$objReturn->error = 1;
								$objReturn->error_code = 88;
								$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "The API account is not active"  : "<h2>API Username or Password</h2>The API account is not active";					
								return $objReturn;	
							}
			
						}
			
			
			
				
						if ( $objDataOrder->totalCount == "0" )
						{
				
							#BOL
							$queryURL = "https://".$objCookie->domain."/trace/trace_class.msw?include_all_clients=false&trace_type=BPTRACE&search_style=exact&trace_number=".$objForm->trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);	
							$objDataOrder = json_decode($contentQuery);				
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | BOL ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				
							
						}
				
				
						if ( $objDataOrder->totalCount == "0" )
						{
				
							#po #
							$queryURL = "https://".$objCookie->domain."/trace/trace_class.msw?include_all_clients=false&trace_type=PPTRACE&search_style=exact&trace_number=".$objForm->trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);	
							$objDataOrder = json_decode($contentQuery);				
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | PO ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
				
							
						}
				
				
						if ( $objDataOrder->totalCount == "0" )
						{
				
							#reference1
							$queryURL = "https://".$objCookie->domain."/trace/trace_class.msw?include_all_clients=false&trace_type=QPTRACE&search_style=exact&trace_number=".$objForm->trace_number;
							
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $queryURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							
							//execute the request
							$contentQuery = curl_exec($ch);
							$objDataOrder = json_decode($contentQuery);					
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | reference1 ";
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
							$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
							$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							
				
							
						}
				
						$objDataOrder = json_decode($contentQuery);
				
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | jsonDecode ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
						$logMessage .= "\n\r trace_number: ".print_r($objForm->trace_number, true);
						$logMessage .= "\n\r objDataOrder->dataResults[0]->BILL_NUMBER: ".print_r($objDataOrder->dataResults[0]->BILL_NUMBER, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
				
						if ( $contentQuery == "" )
						{
							
							$objReturn->error = 1;
							$objReturn->error_code = 99;
							$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password 3";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
							
							
						}
						elseif ( trim($objDataOrder->dataResults[0]->BILL_NUMBER) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_B) == trim($trace_number)  || trim($objDataOrder->dataResults[0]->TRACE_TYPE_P) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_Q) == trim($trace_number)  )
						{
				
							$detailLineID = $objDataOrder->dataResults[0]->DETAIL_LINE_ID;
					
				
							$objReturn->order = $objDataOrder->dataResults[0];	
					
					
							$detailURL = "https://".$objCookie->domain."/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;
							
					
							curl_setopt ($ch, CURLOPT_HEADER, 0);
							curl_setopt ($ch, CURLOPT_HTTPGET, 1);
							curl_setopt ($ch, CURLOPT_URL, $detailURL);
							curl_setopt ($ch, CURLOPT_COOKIEFILE, $objCookie->cookieFilePath);
							curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
							curl_setopt ($ch, CURLOPT_ENCODING, "");
							curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt ($ch, CURLOPT_VERBOSE, 0);
							
							//execute the request
							$detailQuery = curl_exec($ch);
					
							#$headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT ); // request headers
							#echo "headerSent: <PRE>".htmlentities($headerSent)."</PRE>";
							#echo "detailQuery: <PRE>".htmlentities($detailQuery)."</PRE>";
							#print_r(curl_getinfo($ch));
					
					
							$curl_error = "";
							if (curl_error($ch)) {
							    $curl_error = curl_error($ch);
							}
					
							$objDataOrderDetail = json_decode($detailQuery);
							
							$arrOrderSteps = $objDataOrderDetail->ODRSTAT->data;
					
					
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
							$logMessage .= "\n\r ch: ".print_r($ch, true);
							$logMessage .= "\n\r curl_error: ".print_r(json_decode($curl_error), true);
							$logMessage .= "\n\r detailLineID: ".print_r($detailLineID, true);
							$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
							$logMessage .= "\n\r detailQuery: ".print_r($detailQuery, true);
							$logMessage .= "\n\r objDataOrderDetail: ".print_r($objDataOrderDetail, true);
							$logMessage .= "\n\r arrOrderSteps: ".print_r($arrOrderSteps, true);
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
					
					
					
					
							curl_close($ch);
							#unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
							unset($ch);
							
							$objReturn->order_history = $objDataOrderDetail->ODRSTAT->data;
							$objReturn->error = 0;
							$objReturn->error_message = "";
						
						}
						elseif( $objDataOrder == "" )
						{ 
							
							$objReturn->error = 1;
							$objReturn->error_code = 22;
							$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Order tracking is currently unavailable"  :  "<h2>Sorry for the inconvenience</h2>Order tracking is currently unavailable. We are aware of the issue and are working to restore tracking.";
							
						}
						else
						{
				
							$objReturn->error = 1;
							$objReturn->error_code = 11;
							$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "The trace number you entered cannot be found"  :  "<h2>Not Found</h2>The trace number you entered cannot be found. Please re-enter your trace number.";
				
				
							#Try Rockhopper, if Rockhopper exists, transfer them to Rockhopper tracking page. If not, keep error.
							
							#Nsd_trackingController::existRockhopper( $objForm );
				
				
							$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number in system";
							$logMessage .= "\n\r ";
							if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
							if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
							
						}
						
			
					}
					else
					{
						$objReturn->error = 1;
						$objReturn->error_code = 99;
						$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
			
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
						
					}




					
					

				
				}
				else
				{
		
					$objReturn->error = 1;
					$objReturn->error_code = 33;
					$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "No trace number was entered"  :   "<h2>No Tracking Number</h2>No tracking number was entered. Please enter your tracking number.";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
			
				}
				
			}
			else
			{

					$objReturn->error = 1;
					$objReturn->error_code = 77;
					$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Account exceeded the API rate limit"  :   "<h2>Rate Limit Exceeded</h2>Account exceeded the API rate limit";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Rate Limit Exceeded";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
				
			}


		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}



	public function getTMW_version1 ( $objForm ) // 3/6/19 - first version of update to function to reuse cURL session.
	{


		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getTMW";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('tmw4web_user');
		$paramsComponent->tmw4web_password			= $params->get('tmw4web_password');
		$paramsComponent->tmw4web_subdomain			= $params->get('tmw4web_subdomain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_subdomain_test	= $params->get('tmw4web_subdomain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_subdomain;   
			
		}
		else
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_subdomain_test;   		
		}
		

		$trace_number = $objForm->trace_number;

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
            
/*
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$subdomain = 'nonstopdelivery';
*/
            

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerRate = 0;
            $objLogger->logFileRate = "tracking_rate";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
            
/*
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = '1234';
			$subdomain = 'nonstoptest';   
*/         
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r subdomain: ".print_r($subdomain, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$objReturn = new stdClass();

		if ( $tm4web_usr == "" || $tm4web_pwd == "" )
		{

			$objReturn->error = 1;
			$objReturn->error_code = 99;
			$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
		}
		else
		{
	
			$validInterval = 1;
				
			if ( $objForm->flag_api == "1" )
			{

				#$query = "select * from htc_nsd_tracking_activity_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$query = "select trace_milliseconds from htc_nsd_tracking_activity_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$db->setQuery($query);
				$objAPILog = $db->loadObject();


				$millisecondsNow = round(microtime(true) * 1000);
				
				#10000 = 10 seconds or 360 requests an hour
				#5000 = 5 seconds or 720 requests an hour
				#2000 = 2 seconds or 1200 requests an hour
				#2000 = 2 seconds or 1800 requests an hour
				#$intervalInMilliseconds = 1;
				$intervalInMilliseconds = $paramsComponent->api_rate_limit_milliseconds;
				
				
				$diffMilliseconds = $millisecondsNow - $objAPILog->trace_milliseconds;
	
				$validInterval = ( $diffMilliseconds > $intervalInMilliseconds ) ? 1 : 0 ;

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | validInterval";
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objAPILog: ".print_r($objAPILog, true);
					$logMessage .= "\n\r millisecondsNow: ".print_r($millisecondsNow, true);
					$logMessage .= "\n\r diffMilliseconds: ".print_r($diffMilliseconds, true);
					$logMessage .= "\n\r validInterval: ".print_r($validInterval, true);
					$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerRate ) { MetalakeHelperCore::logger( $objLogger->logFileRate, $logMessage ); }
				
			}	
			
			if( $validInterval == 1 )
			{

				if ( $trace_number != "" )
				{

					
					
					$objCookie = new stdClass();
					$objCookie->tm4web_usr = $tm4web_usr;
					$objCookie->tm4web_pwd = $tm4web_pwd;
					$objCookie->domain = $domain;					

					$objForm->attempts = 0;
					$objForm->attempts_auth = 0;

					
					$objReturn = Nsd_trackingController::getTmwDataWithCookie( $objCookie, $objForm );



/*
					if ( $store != "" && $objForm->flag_api == "1" )
					{
						#get api display level and set it.
	
						$query = "select * from htc_nsd_tracking_api_clients where name = '".$objForm->tmw4web_user."' and state = '1' ";
						$db->setQuery($query);
						$objApiAgent = $db->loadObject();
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | htc_nsd_tracking_api_clients ";
						$logMessage .= "\n\r query: ".print_r($query, true);
						$logMessage .= "\n\r objApiAgent: ".print_r($objApiAgent, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
						if ( is_object($objApiAgent) )
						{
							$objReturn->id_level = $objApiAgent->id_level;						
						}
						else
						{
							$objReturn->error = 1;
							$objReturn->error_code = 88;
							$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "The API account is not active"  : "<h2>API Username or Password</h2>The API account is not active";					
							return $objReturn;	
						}
	
					}
*/
					
					

				
				}
				else
				{
		
					$objReturn->error = 1;
					$objReturn->error_code = 33;
					$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "No trace number was entered"  :   "<h2>No Tracking Number</h2>No tracking number was entered. Please enter your tracking number.";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
			
				}
				
			}
			else
			{

					$objReturn->error = 1;
					$objReturn->error_code = 77;
					$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Account exceeded the API rate limit"  :   "<h2>Rate Limit Exceeded</h2>Account exceeded the API rate limit";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Rate Limit Exceeded";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
				
			}


		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}


	
	public function getTMW_orig ( $objForm ) // 3/6/19 - original getTMW call
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web&trace=1063701216
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web&trace=31065532&prod=1


		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getTMW";
		
		$db = JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('tmw4web_user');
		$paramsComponent->tmw4web_password			= $params->get('tmw4web_password');
		$paramsComponent->tmw4web_subdomain			= $params->get('tmw4web_subdomain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_subdomain_test	= $params->get('tmw4web_subdomain_test');
		$paramsComponent->api_rate_limit_milliseconds = $params->get('api_rate_limit_milliseconds');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_subdomain;   
			
		}
		else
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_subdomain_test;   		
		}
		

		$trace_number = $objForm->trace_number;

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
            
/*
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$subdomain = 'nonstopdelivery';
*/
            

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerRate = 0;
            $objLogger->logFileRate = "tracking_rate";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
            
/*
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = '1234';
			$subdomain = 'nonstoptest';   
*/         
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r subdomain: ".print_r($subdomain, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$objReturn = new stdClass();

		if ( $tm4web_usr == "" || $tm4web_pwd == "" )
		{

			$objReturn->error = 1;
			$objReturn->error_code = 99;
			$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";

			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
		}
		else
		{
	
			$validInterval = 1;
				
			if ( $objForm->flag_api == "1" )
			{

				#$query = "select * from htc_nsd_tracking_activity_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$query = "select trace_milliseconds from htc_nsd_tracking_activity_log where tm4web_usr = '".$tm4web_usr."' ORDER BY id DESC limit 1 ";
				$db->setQuery($query);
				$objAPILog = $db->loadObject();


				$millisecondsNow = round(microtime(true) * 1000);
				
				#10000 = 10 seconds or 360 requests an hour
				#5000 = 5 seconds or 720 requests an hour
				#2000 = 2 seconds or 1200 requests an hour
				#2000 = 2 seconds or 1800 requests an hour
				#$intervalInMilliseconds = 1;
				$intervalInMilliseconds = $paramsComponent->api_rate_limit_milliseconds;
				
				
				$diffMilliseconds = $millisecondsNow - $objAPILog->trace_milliseconds;
	
				$validInterval = ( $diffMilliseconds > $intervalInMilliseconds ) ? 1 : 0 ;

					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | validInterval";
					$logMessage .= "\n\r query: ".print_r($query, true);
					$logMessage .= "\n\r objAPILog: ".print_r($objAPILog, true);
					$logMessage .= "\n\r millisecondsNow: ".print_r($millisecondsNow, true);
					$logMessage .= "\n\r diffMilliseconds: ".print_r($diffMilliseconds, true);
					$logMessage .= "\n\r validInterval: ".print_r($validInterval, true);
					$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerRate ) { MetalakeHelperCore::logger( $objLogger->logFileRate, $logMessage ); }
				
			}	
			
			if( $validInterval == 1 )
			{

				if ( $trace_number != "" )
				{
			
					$tmp_fname = tempnam("/tmp", "COOKIE");
			
			
					#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD
		
					
					$loginUrl = "https://".$domain."/login/do_login.msw";
					
					//init curl
					$ch = curl_init();
					
					//Set the URL to work with
					curl_setopt($ch, CURLOPT_URL, $loginUrl);
					
					// ENABLE HTTP POST
					curl_setopt($ch, CURLOPT_POST, 1);
					
					//Set the post parameters
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
					
					//Handle cookies for the login
					curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
					curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
					curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					
					//execute the request (the login)
					$store = curl_exec($ch);
					
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | 1 ";
					#$logMessage .= "\n\r store: ".print_r($store, true);
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
					
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
					
					if ( $store != "" && $objForm->flag_api == "1" )
					{
						#get api display level and set it.
	
						$query = "select * from htc_nsd_tracking_api_clients where name = '".$objForm->tmw4web_user."' and state = '1' ";
						$db->setQuery($query);
						$objApiAgent = $db->loadObject();
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | htc_nsd_tracking_api_clients ";
						$logMessage .= "\n\r query: ".print_r($query, true);
						$logMessage .= "\n\r objApiAgent: ".print_r($objApiAgent, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
						if ( is_object($objApiAgent) )
						{
							$objReturn->id_level = $objApiAgent->id_level;						
						}
						else
						{
							$objReturn->error = 1;
							$objReturn->error_code = 88;
							$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "The API account is not active"  : "<h2>API Username or Password</h2>The API account is not active";					
							return $objReturn;	
						}
	
					}
					
					
					
					
					#bill number
					$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=~PTLORDER&search_style=exact&trace_number=".$trace_number;
					
					curl_setopt ($ch, CURLOPT_HTTPGET, 1);
					curl_setopt ($ch, CURLOPT_URL, $queryURL);
					curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
					curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt ($ch, CURLOPT_HEADER, 0);
					
					//execute the request
					$contentQuery = curl_exec($ch);
					$objDataOrder = json_decode($contentQuery);
	
					
					#echo "contentQuery: <PRE>".htmlentities($contentQuery)."</PRE>";
			
	/*
					if(curl_errno($ch)){
					    echo 'Curl error: ' . curl_error($ch);
					}
	*/
			
					#print_r(curl_getinfo($ch));
	
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | Bill Number ";
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
					$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
					$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
	
	
					if ( $objDataOrder->totalCount == "0" )
					{
	
						#BOL
						$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=BPTRACE&search_style=exact&trace_number=".$trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						
						//execute the request
						$contentQuery = curl_exec($ch);	
						$objDataOrder = json_decode($contentQuery);				
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | BOL ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
	
						
					}
	
	
					if ( $objDataOrder->totalCount == "0" )
					{
	
						#po #
						$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=PPTRACE&search_style=exact&trace_number=".$trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						
						//execute the request
						$contentQuery = curl_exec($ch);	
						$objDataOrder = json_decode($contentQuery);				
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | PO ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
	
						
					}
	
	
					if ( $objDataOrder->totalCount == "0" )
					{
	
						#reference1
						$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&trace_type=QPTRACE&search_style=exact&trace_number=".$trace_number;
						
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $queryURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						
						//execute the request
						$contentQuery = curl_exec($ch);
						$objDataOrder = json_decode($contentQuery);					
	
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | reference1 ";
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
						$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
						$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						
	
						
					}
	
	
	
			
			
					$objDataOrder = json_decode($contentQuery);
			
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName." | jsonDecode ";
					$logMessage .= "\n\r ch: ".print_r($ch, true);
					$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
					$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
					$logMessage .= "\n\r objDataOrder->dataResults[0]->BILL_NUMBER: ".print_r($objDataOrder->dataResults[0]->BILL_NUMBER, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					
			
					if ( $store == "" )
					{
						
						$objReturn->error = 1;
						$objReturn->error_code = 99;
						$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";
			
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Invalid API Username or Password";
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
						
						
					}
					elseif ( trim($objDataOrder->dataResults[0]->BILL_NUMBER) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_B) == trim($trace_number)  || trim($objDataOrder->dataResults[0]->TRACE_TYPE_P) == trim($trace_number) || trim($objDataOrder->dataResults[0]->TRACE_TYPE_Q) == trim($trace_number)  )
					{
		
						$detailLineID = $objDataOrder->dataResults[0]->DETAIL_LINE_ID;
				
			
						$objReturn->order = $objDataOrder->dataResults[0];	
				
				
						$detailURL = "https://".$domain."/trace/bill_details_ajax.msw?func=TraceBillDetails&dld=".$detailLineID;
						
				
						curl_setopt ($ch, CURLOPT_HEADER, 0);
						curl_setopt ($ch, CURLOPT_HTTPGET, 1);
						curl_setopt ($ch, CURLOPT_URL, $detailURL);
						curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
						curl_setopt ($ch, CURLOPT_ENCODING, "");
						curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
						curl_setopt ($ch, CURLOPT_VERBOSE, 0);
						
						//execute the request
						$detailQuery = curl_exec($ch);
				
						#$headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT ); // request headers
						#echo "headerSent: <PRE>".htmlentities($headerSent)."</PRE>";
						#echo "detailQuery: <PRE>".htmlentities($detailQuery)."</PRE>";
						#print_r(curl_getinfo($ch));
				
				
						$curl_error = "";
						if (curl_error($ch)) {
						    $curl_error = curl_error($ch);
						}
				
						$objDataOrderDetail = json_decode($detailQuery);
						
						$arrOrderSteps = $objDataOrderDetail->ODRSTAT->data;
				
				
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
						$logMessage .= "\n\r ch: ".print_r($ch, true);
						$logMessage .= "\n\r curl_error: ".print_r(json_decode($curl_error), true);
						$logMessage .= "\n\r detailLineID: ".print_r($detailLineID, true);
						$logMessage .= "\n\r detailURL: ".print_r($detailURL, true);
						$logMessage .= "\n\r detailQuery: ".print_r($detailQuery, true);
						$logMessage .= "\n\r objDataOrderDetail: ".print_r($objDataOrderDetail, true);
						$logMessage .= "\n\r arrOrderSteps: ".print_r($arrOrderSteps, true);
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
				
				
				
				
						curl_close($ch);
						unlink($tmp_fname) or die("Can't unlink ".$tmp_fname);
						unset($ch);
						
						$objReturn->order_history =$objDataOrderDetail->ODRSTAT->data;
						$objReturn->error = 0;
						$objReturn->error_message = "";
					
					}
					elseif( $objDataOrder == "" )
					{ 
						
						$objReturn->error = 1;
						$objReturn->error_code = 22;
						$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Order tracking is currently unavailable"  :  "<h2>Sorry for the inconvenience</h2>Order tracking is currently unavailable. We are aware of the issue and are working to restore tracking.";
						
					}
					else
					{
		
						$objReturn->error = 1;
						$objReturn->error_code = 11;
						$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "The trace number you entered cannot be found"  :  "<h2>Not Found</h2>The trace number you entered cannot be found. Please re-enter your trace number.";
			
			
						#Try Rockhopper, if Rockhopper exists, transfer them to Rockhopper tracking page. If not, keep error.
						
						#Nsd_trackingController::existRockhopper( $objForm );
			
			
						$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number in system";
						$logMessage .= "\n\r ";
						if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
						if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
						
					}
					
				
				}
				else
				{
		
					$objReturn->error = 1;
					$objReturn->error_code = 33;
					$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "No trace number was entered"  :   "<h2>No Tracking Number</h2>No tracking number was entered. Please enter your tracking number.";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
			
				}
				
			}
			else
			{

					$objReturn->error = 1;
					$objReturn->error_code = 77;
					$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Account exceeded the API rate limit"  :   "<h2>Rate Limit Exceeded</h2>Account exceeded the API rate limit";
		
					$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | Rate Limit Exceeded";
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
					if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
				
			}


		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}	














	// !getTrackingMatrix
	public static function getTrackingMatrix( $objForm, $objTMW )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getTrackingMatrix";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$order_type = ( strtoupper($objTMW->order->SITE_ID) == "SITE6" ) ? "Delivery" : "Return" ;

		$delivery_type = $objTMW->order->OP_CODE;

		#$delivery_type = ( $objTMW->order->OP_CODE == "LHR" ) ? "DD" : $objTMW->order->OP_CODE ;
		
/*
		switch( $objTMW->order->OP_CODE  )
		{
			case "DD":
			case "LHR":
			case "STH":
				$delivery_type = "DD";
				break;

			case "LM":
			case "RV":
			case "RESCUE":
				$delivery_type = "LM";
				break;

			default:
				$delivery_type = "DD";
				
				$trace_number = $objForm->trace_number;
				
				if ( $objTMW->order->OP_CODE != "" )
				{

					#send email with details
					$objAlert = new stdClass();
					$objAlert->recipients = array( 'lsawyer@metalake.com' );
					$objAlert->subject = "ALERT | Dispatchtrack Tracking Page | getTrackingMatrix | Unknown OP Code ".$objTMW->order->OP_CODE;
					$body = "";
					$body .= "<br>ALERT | Dispatchtrack Tracking Page | getTrackingMatrix | Unknown OP Code ".$objTMW->order->OP_CODE."<br>";
					$body .= "<br>Defaulted to DD (door-to-door) so page would render.<br>";
					$body .= "<br>Trace Number: ".$trace_number."<br>";
					$body .= "<br>OP Code: ".$objTMW->order->OP_CODE."<br>";
					$objAlert->body = $body;
					$resultAlert = Nsd_trackingController::sendAlert( $objAlert );				

					
				}
				
				
				break;
		}
*/
		
		
		
		

		$arrWhere = array();
		$arrWhere[] = "m.state = 1";
		#$arrWhere[] = "delivery_type = '".$objTMW->order->OP_CODE."'";
		$arrWhere[] = "delivery_type = '".$delivery_type."'";
		$arrWhere[] = "order_status = '".$objTMW->order->CURRENT_STATUS."'";
		$arrWhere[] = "order_type = '".$order_type."'";		

		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
		
		
		$query = "SELECT m.*, dt.description as 'delivery_type_description' FROM htc_nsd_tracking_matrix m left join htc_nsd_tracking_lu_delivery_type dt ON m.delivery_type=dt.name ". $where ;


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$db->setQuery($query);
		$objReturn = $db->loadObject();


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;		
	}


	// !getServiceLevelInformation
	public static function getServiceLevelInformation( $objTMW )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getServiceLevelInformation";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$strServiceLevel = trim($objTMW->order->SERVICE_LEVEL);
		$flag_return = ( trim($objTMW->order->SITE_ID) == "SITE5" ) ? "1" : "0";



		$service_level = strtoupper( $strServiceLevel );


		$arrWhere = array();
		$arrWhere[] = "state = 1";
		$arrWhere[] = "name = '".$service_level."'";
		$arrWhere[] = "flag_return = '".$flag_return."'";

		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
		
		
		$query = "SELECT * FROM htc_nsd_tracking_lu_service_descriptions ". $where ;
		$db->setQuery($query);
		$objReturn = $db->loadObject();
		
		$arrSL = json_decode($objReturn->description_long);
		
		$retUL = "<ul>";
		
		foreach( $arrSL as $sl )
		{
			
			$retUL .= "<li>".$sl."</li>";
		}
		
		$retUL .= "</ul>";
		
		$objReturn->arrDescriptionLong = $retUL;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r query: ".print_r($query, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;		
	}




	// !getOrderHistoryData
	public static function getOrderHistoryData( $objForm, $objTMW, $objTrackingMatrix, $order_status )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getOrderHistoryData";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		

		foreach( $objTMW->order_history as $history )
		{
			
			if ( $history->OS_STATUS_CODE == $order_status )
			{
				return $history;
			}
			
		}
		

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objReturn = new stdClass();
		$objReturn->OS_CHANGED = "";
		return $objReturn;			
		
	}




	// !getArrOrderHistory

	public static function getArrOrderHistory( $objForm, $objTMW, $objTrackingMatrix )
	{


		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getArrOrderHistory";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }				
		


		$prior_history_status_description = "";



		$logMessage = "INSIDE | 1 | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r objTMW->order_history: ".print_r($objTMW->order_history, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		foreach( $objTMW->order_history as $history )
		{

			$objInput = new stdClass();
			$objInput->delivery_type = $objTrackingMatrix->delivery_type;
			$objInput->order_type = $objTrackingMatrix->order_type;
			$objInput->order_status = $history->OS_STATUS_CODE;

			$objMatrix = Nsd_trackingModelTrack::getMatrixInfo( $objInput );

			$logMessage = "INSIDE | 1.1 | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objInput: ".print_r($objInput, true);
			$logMessage .= "\n\r objMatrix: ".print_r($objMatrix, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
			switch( $objMatrix->status_location )
			{

				case "CARE_OF_CITY CARE_OF_PROV":
				$history->status_location = trim($objTMW->order->CARE_OF_CITY).", ".trim($objTMW->order->CARE_OF_PROV);
					break;

				case "DESTCITY DESTPROV":
				$history->status_location = trim($objTMW->order->DESTCITY).", ".trim($objTMW->order->DESTPROV);
					break;

				case "ORIGCITY ORIGPROV":
				$history->status_location = trim($objTMW->order->ORIGCITY).", ".trim($objTMW->order->ORIGPROV);
					break;

				case "Status comment":
				
					$arrOne = explode( "-", $history->OS_STAT_COMMENT );
					$arrTwo = explode( ",", $arrOne[1] );
					$arrThree = explode( " ", $arrTwo[1] );
					$strOUT = $arrTwo[0].", ".$arrThree[1];
				
				
				$history->status_location = trim($strOUT);
					break;
				
				default:
				$history->status_location = "";
			}				
			
			
			$history->status_date = date('F j, Y g:i A', strtotime($history->OS_CHANGED));

			if ( $objForm->flag_api == "0" )
			{
				$history->status_description = ( $objMatrix ) ? $objMatrix->current_state : "";
				$history->bar_state  = ( $objMatrix ) ? $objMatrix->bar_state : "";
				$history->bar_color = strtolower($objMatrix->bar_color); #"green";
			}

			$history->status_exception = "0";








		
			if ( $history->OS_SF_REASON_CODE != "" )
			{

				if ( $objForm->flag_api == "1" )
				{
					
					
					
				}
				else
				{				
					
					$arrWhereReason = array();
					$arrWhereReason[] = "state = 1";
					$arrWhereReason[] = "code = '".$history->OS_SF_REASON_CODE."'";
			
					$whereReason = count( $arrWhereReason ) ? " WHERE ". implode( ' AND ', $arrWhereReason ) : '' ;
									
					$query = "SELECT * FROM htc_nsd_tracking_lu_reason_codes ". $whereReason ;
					$db->setQuery($query);
					$objReasonCodes = $db->loadObject();	
					
					if ( $objReasonCodes )
					{	
				
					$history->status_description = (  $objReasonCodes->current_state != "No change" ) ?  $objReasonCodes->current_state : $history->status_description  ;				
					
					$history->status_exception = (  $objReasonCodes->current_state != "No change" || $history->OS_STATUS_CODE == "DISPOSED" ) ?  "1" : $history->status_exception  ;
				
					$history->bar_color = (  $objReasonCodes->current_state != "No change" ) ?  $objReasonCodes->bar_color : "green"  ;

					$history->bar_color = ( $history->OS_STATUS_CODE == "DISPOSED" ) ? "red" : $history->bar_color ;

					$logMessage = "INSIDE | 1.2 | ".$controllerName." | ".$functionName;
					$logMessage .= "\n\r objReasonCodes: ".print_r($objReasonCodes, true);
					$logMessage .= "\n\r history: ".print_r($history, true);
					$logMessage .= "\n\r ";
					if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }

					}



				}
			}
		
			$history->status_engagement = $objMatrix->status_engagement;
		
			switch( $objMatrix->status_engagement )
			{

				case "1":
					$history->suppressed = 0;
					break;
					
				default:
					$history->suppressed = 1;
			}

			$history->suppressed = ( $history->OS_SF_REASON_CODE == "CONTATMPT" ) ? 1 : $history->suppressed ;	
			
			$prior_history_status_description = $history->status_description;
			
			
			if ( $objForm->flag_api == "1" )
			{

				$arrWhereReasonAPI = array();
				$arrWhereReasonAPI[] = "state = 1";
				$arrWhereReasonAPI[] = "tracking_code = '".$history->OS_STATUS_CODE."'";
				$arrWhereReasonAPI[] = "reason_code = '".$history->OS_SF_REASON_CODE."'";
				$arrWhereReasonAPI[] = "id_level = '".$objTMW->id_level."'";
		
				$whereReasonAPI = count( $arrWhereReasonAPI ) ? " WHERE ". implode( ' AND ', $arrWhereReasonAPI ) : '' ;


				$query = "SELECT * FROM htc_nsd_tracking_api_matrix ". $whereReasonAPI ;
				$db->setQuery($query);
				$objResult = $db->loadObject();
				
				
				if ( $objResult )
				{ 
					$history->api_suppressed = 0;
					$history->api_description = $objResult->description;
					
				}
				else
				{
					$history->api_suppressed = 1;
				}
				

				
			}
			
			
			
			
			
		}


		$logMessage = "INSIDE | 2 | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objTMW->order_history: ".print_r($objTMW->order_history, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $objForm->flag_api == "1" )
		{

/*
			#How to delete object from array inside foreach loop?
			#https://stackoverflow.com/questions/2304570/how-to-delete-object-from-array-inside-foreach-loop
	
			foreach($array as $elementKey => $element) {
			    foreach($element as $valueKey => $value) {
			        if($valueKey == 'id' && $value == 'searched_value'){
			            //delete this particular object from the $array
			            unset($array[$elementKey]);
			        } 
			    }
			}
*/


			foreach($objTMW->order_history as $elementKey => $element) {
			    foreach($element as $valueKey => $value) {

			        if($valueKey == 'api_suppressed' && $value == '1'){
			            //delete this particular object from the $array
			            unset($objTMW->order_history[$elementKey]);
			        } 
			    }
			}

			$objTMW->order_history = array_values($objTMW->order_history);


			$logMessage = "INSIDE | 3 | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r objTMW->order_history: ".print_r($objTMW->order_history, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

			$arrAPI = array();
			
			foreach( $objTMW->order_history as $objRecord )
			{
				$objNew = new stdClass();
				$objNew->status_code = $objRecord->OS_STATUS_CODE;
				$objNew->reason_code = $objRecord->OS_SF_REASON_CODE;
				$objNew->status_location = $objRecord->status_location;
				$objNew->status_date = $objRecord->status_date;
				$objNew->status_description = $objRecord->api_description;
				#$objNew->insert_date = $objRecord->OS_INS_DATE;
				#$objNew->change_date = $objRecord->OS_CHANGED;
				
				$arrAPI[] = $objNew;
				
			}

			
			$objTMW->order_history = $arrAPI;


		}

		$logMessage = "INSIDE | 4 | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objTMW->order_history: ".print_r($objTMW->order_history, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$arrReturn = $objTMW->order_history;


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);

		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $arrReturn;					
		
		
	}



	// !getMatrixInfo
	public static function getMatrixInfo( $objInput )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getMatrixInfo";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		

		$arrWhere = array();
		$arrWhere[] = "state = 1";
		$arrWhere[] = "delivery_type = '".$objInput->delivery_type."'";
		$arrWhere[] = "order_type = '".$objInput->order_type."'";
		$arrWhere[] = "order_status = '".$objInput->order_status."'";

		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
						
		$query = "SELECT * FROM htc_nsd_tracking_matrix ". $where ;
		$db->setQuery($query);
		$objReturn = $db->loadObject();
		

				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;			
		
	}



	// !getBarMatrixInfo
	public static function getBarMatrixInfo( $objInput )
	{

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getBarMatrixInfo";
		
		$db = JFactory::getDBO();


		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
		}


		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }		

		$arrWhere = array();
		$arrWhere[] = "state = 1";
		$arrWhere[] = "delivery_type = '".$objInput->delivery_type."'";
		$arrWhere[] = "order_type = '".$objInput->order_type."'";
		$arrWhere[] = "visible = 1";

		$where = count( $arrWhere ) ? " WHERE ". implode( ' AND ', $arrWhere ) : '' ;
						
		$query = "SELECT bar_state, class, '' as color FROM htc_nsd_tracking_lu_bar_states_by_type ". $where ." order by ordering" ;
		$db->setQuery($query);
		$objListReturn = $db->loadObjectList();
		

				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r query: ".print_r($query, true);
				$logMessage .= "\n\r objListReturn: ".print_r($objListReturn, true);
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objListReturn;			
		
	}


	// !===== ROCKHOPPER
	
	// !getRockhopper
	public static function getRockhopper( $objForm )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=getRockhopper

		# http://process.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=getRockhopper

		$controllerName = "Nsd_trackingModelTrack";
		$functionName = "getRockhopper";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objRockhopper = new stdClass();


		$trace_number = $objForm->trace_number;

	    $url = 'http://nstracking.nonstopdelivery.com/api/rest/nsdws/tracking/ws-track-shipment/' . $trace_number;
	    
	    
	    
	
	    $response = file_get_contents($url);

		$body = simplexml_load_string($response);

		$tracking_number = (string)$body->BasicInformation->TrackingNumber;

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r url: ".print_r($url, true);
		#$logMessage .= "\n\r response: ".print_r($response, true);
		#$logMessage .= "\n\r body: ".print_r($body, true);
		$logMessage .= "\n\r TrackingNumber: ".print_r($tracking_number, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		if ( $trace_number == $tracking_number )
		{

			$objRockhopper->tracking_number = $tracking_number;


			// Type
			$type = Nsd_trackingModelTrack::nsd_tracking_type($body->BasicInformation->IsReturnService, $body->BasicInformation->IsLonghaul); // String
			
			$objRockhopper->type = $type;


			// State
			$state = (string) $body->BasicInformation->TabState;


			// Phase (Open/Scheduled/Closed)
			$phase = Nsd_trackingModelTrack::nsd_tracking_phase($type, $state); // String

			$objRockhopper->phase = $phase;


		    // Details
		    $tracking_number = (string) $body->BasicInformation->TrackingNumber;
		    $details = array();
		    
		    if ( ! empty($body->BasicInformation->Ref1) ) {
		        $details[(string) $body->BasicInformation->Ref1Description] = (string) $body->BasicInformation->Ref1;
		    }
		    if ( ! empty($body->BasicInformation->Ref2) ) {
		        $details[(string) $body->BasicInformation->Ref2Description] = (string) $body->BasicInformation->Ref2;
		    }
		    $objRockhopper->details = $details;
		    
		    
		    $service = Nsd_trackingModelTrack::nsd_tracking_service($body->BasicInformation->Service, 'Unknown'); // String
		    $service_descriptions = Nsd_trackingModelTrack::nsd_tracking_service_descriptions($service); // Array

			$objRockhopper->service = $service;
			$objRockhopper->service_descriptions = $service_descriptions;


		    // History
		    $history = array();
		    if ( isset($body->Events->event) ) {
		        foreach ( $body->Events->event as $event ) {
		            $history[] = Nsd_trackingModelTrack::nsd_tracking_event($event); // Array
		        }
		        $history = Nsd_trackingModelTrack::nsd_tracking_history($history); // Sorted/Filtered Array
		    }

			foreach ( $history as $key => $eventHistory  )
			{
				
				$history[$key]['activityClass'] = Nsd_trackingModelTrack::nsd_tracking_class($eventHistory);
	
			}
			
			$objRockhopper->history = $history;			


		    // Stops
		    $stops = array();
		    if ( isset($body->Stops->stop) ) {
		        foreach ( $body->Stops->stop as $stop ) {
		            $stop = Nsd_trackingModelTrack::nsd_tracking_stop($stop); // Array
		            $stops[$stop['sequence']] = $stop; // Indexed by sequence (1,2,3)
		        }
		    }
		    
			$objRockhopper->stops = $stops;

			$objRockhopper->stops_1_city_clean = Nsd_trackingModelTrack::nsd_tracking_city($objRockhopper->stops[1]['city']);
			$objRockhopper->stops_3_city_clean = Nsd_trackingModelTrack::nsd_tracking_city($objRockhopper->stops[3]['city']);
			$objRockhopper->stops_1_zip = substr($objRockhopper->stops[1]['city'],-5);
			$objRockhopper->stops_3_zip = substr($objRockhopper->stops[3]['city'],-5);


		    // Shipping Date
		    $shipping = Nsd_trackingModelTrack::nsd_tracking_shipping($type, $history);
		    
			$objRockhopper->shipping = $shipping;



		    // Delivery Date(s)
		    $estimated = ! empty($body->BasicInformation->EstimatedDelivery) ? (string) $body->BasicInformation->EstimatedDelivery : 'N/A'; // Failsafe in case it does not exist (returns)
		    $delivery = Nsd_trackingModelTrack::nsd_tracking_delivery($type, $phase, $estimated, $history); // Array

			$objRockhopper->estimated = $estimated;
			$objRockhopper->delivery = $delivery;


		    // Position
		    $position = Nsd_trackingModelTrack::nsd_tracking_position($type, $state, $history); // Int
		    $objRockhopper->position = $position;
		    
		    
		    // Tracking bar
		    $bar = Nsd_trackingModelTrack::nsd_tracking_bar($type, $position, $history, $stops); // Array
		
			$objRockhopper->bar = $bar;
		    
		    
		    // Status (Overall)
		    $status = Nsd_trackingModelTrack::nsd_tracking_status($type, $phase, $history); // Array

			$objRockhopper->status = $status;

			
		}
		else
		{
			
			$objRockhopper->error = "1";
			
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r failed to get data from getRockhopper";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }			
			
		}


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objRockhopper: ".print_r($objRockhopper, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		return $objRockhopper;
	}


	public function nsd_tracking_type ( $is_return_service, $is_longhaul ) 
	{
	    $is_return_service = (string) $is_return_service;
	    $is_longhaul = (string) $is_longhaul;
	    
	    // Determine type
	    if ( $is_return_service === 'true' ) {
	        return 'return';
	    } elseif ( $is_longhaul === 'true' ) {
	        return 'door-to-door';
	    } elseif ( $is_longhaul === 'false' ) {
	        return 'last-mile';
	    } else {
	        return null;
	    }
	}



	public function nsd_tracking_phase ( $type, $state ) 
	{
	    // Phase based on client spreadsheet (and email clarifications)
	    if ( $type === 'return' ) {
	        if ( in_array($state, array('PEND', 'DOCK', 'RTNS', 'STOR')) ) {
	            return 'open';
	        } else if ( in_array($state, array('SCHD', 'ACTV')) ) {
	            return 'scheduled';
	        } else if ( in_array($state, array('FIN', 'WPU')) ) {
	            return 'closed';
	        }
	    } else {
	        if ( $state === 'PEND' ) {
	            return 'pending';
	        } else if ( in_array($state, array('DOCK', 'RTNS', 'WPU', 'STOR')) ) {
	            return 'open';
	        } else if ( in_array($state, array('SCHD', 'ACTV')) ) {
	            return 'scheduled';
	        } else if ( $state === 'FIN' ) {
	            return 'closed';
	        }
	    }
	
	    return 'unknown';
	}


	
	
	public function nsd_tracking_service ( $service, $default = '-' ) 
	{
	    $service = (string) $service;
	
	    $services = array(
	        'Asset Recov 1man' => 'Asset Recovery',
	        'Asset Recov 2man' => 'Asset Recovery',
	        'Basic' => 'Basic Home Delivery',
	        'Basic1' => 'Basic Home Delivery',
	        'Basic1 Attempt' => 'Basic Attempt',
	        'Basic2' => 'Basic Home Delivery',
	        'Basic2 Attempt' => 'Basic Attempt',
	        'Basic Attempt' => 'Basic Attempt',
	        'Deluxing Service' => 'Deluxing Service',
	        'LTL' => 'Misc',
	        'HD-No Preference' => 'No Preference',
	        'Linehaul Return Only' => 'Linehaul Return',
	        'Store Pickup' => 'Store Pickup',
	        'Sweep' => 'Sweep',
	        'Threshold 1' => 'Threshold Home Delivery',
	        'Threshold 1 Attempt' => 'Threshold Attempt',
	        'Threshold 1 Return' => 'Threshold Return',
	        'Thresh 1 Redelivery' => 'Threshold Redelivery',
	        'Threshold 2' => 'Threshold Home Delivery',
	        'Threshold 2 Attempt' => 'Threshold Attempt',
	        'Threshold 2 Return' => 'Threshold Return',
	        'Thresh 2 Redelivery' => 'Threshold Redelivery',
	        'WhiteGlove 1 Redeliv' => 'White Glove Redelivery',
	        'WhiteGlove 2 Redeliv' => 'White Glove Redelivery',
	        'WhiteGlove' => 'White Glove Home Delivery',
	        'Whiteglove 1' => 'White Glove Home Delivery',
	        'WhiteGlove 1 Attempt' => 'White Glove Attempt',
	        'Whiteglove 1 Return' => 'White Glove Return',
	        'Whiteglove 2' => 'White Glove Home Delivery',
	        'WhiteGlove 2 Attempt' => 'White Glove Attempt',
	        'Whiteglove 2 Return' => 'White Glove Return',
	        'WhiteGlove Attempt' => 'White Glove Attempt',
	        'WhiteGlove' => 'White Glove Home Delivery',
	        'WhiteGlove Return' => 'White Glove Return',
	        'WhiteGlove Redelive' => 'White Glove Redelivery',
	        'Plus' => 'Basic Plus Home Delivery', // NEW!
	        'BASIC' => 'Basic Home Delivery',
	        'BASICPLUS' => 'Basic Plus Home Delivery',
	        'THRESHOLD' => 'Threshold Home Delivery', 
	        'WHITEGLOVE' => 'White Glove Home Delivery',
	        'THRESHOLD RETURN' => 'Threshold Return',
	        'Threshold Return' => 'Threshold Return',
	        'WHITEGLOVERETURN' => 'White Glove Return'        
	    );
	
	    if ( array_key_exists($service, $services) ) {
	        // Return the service
	        return $services[$service];
	    } else {
	        // Unknown
	        //var_dump($service);
	        return $default;
	    }
	}
	
	public function nsd_tracking_service_descriptions ( $service ) 
	{
	
	    switch ( $service ) {
	    case 'Basic Home Delivery':
	        return array(
	            'Deliver to first available "dry" area (e.g. patio, garage, etc.)',
	            '30-minute courtesy pre-call while en route',
	            'No signature required'
	        );
	    case 'Basic Plus Home Delivery':
	        return array(
	            'Deliver to first available "dry" area (e.g. patio, garage, etc.)',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture'
	        );
	    case 'Threshold Home Delivery':
	    case 'Threshold Redelivery':
	        return array(
	            'Delivery inside main entrance',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture'
	        );
	    case 'White Glove Home Delivery':
	    case 'White Glove Redelivery':
	        return array(
	            'Delivery to room-of-choice',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture',
	            'Product unpack & debris removal'
	        );
	    case 'Threshold Return':
	        return array(
	            'Pickup inside main entrance',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture'
	        );
	    case 'White Glove Return':
	    case 'Asset Recovery':
	        return array(
	            'Pickup from room-of-choice',
	            'Pre-scheduled Appointment window',
	            '30-minute courtesy pre-call while en route',
	            'Signature capture'
	        );
	    default:
	        return array();
	    }
	}
	



	
	
	public function nsd_tracking_event ( $data ) 
	{
	    $attributes = $data->attributes();
	
		if ( (string) $attributes['code'] == 'DELIVERED' )
		{
		    $location = implode('<br>', array_filter((array) strtok($data->stop3_city, ',')));	
	
			$city = (string)$data->stop3_city;
	
	        $city = str_replace(",", " ", $city);
	        $city = preg_replace('/\s+[0-9]{5}$/', '', trim($city)); // Strip off zip code
	        $city = preg_replace('/\s+([A-Z]{2})$/', ', $1', trim($city)); // Add comma between city and state
	
		    
		    $location = $city;
	
			#$location = implode('<br>', array_filter((array) $data->stop3_city));	
						
		}
		else
		{
		    $location = implode('<br>', array_filter((array) $data->location));		
		}
	
	
	    return array(
	        'timestamp' => ( (string) $attributes['code'] == 'DELIVERED' ) ? Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'U', 0)+1800 : Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'U', 0),
	        'timestamp_real' => Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'U', 0),
			'date' => Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'F j, Y'),
	        'time' => Nsd_trackingModelTrack::nsd_tracking_date($data->datetime, 'g:i A'),
	        'code' => (string) $attributes['code'],
	        'description' => (string) $data->description,
	        'activity' => Nsd_trackingModelTrack::nsd_tracking_activity($attributes['code'], $data->description, $data->tracking_no),
	        'location' => $location,
	        'eta' => Nsd_trackingModelTrack::nsd_tracking_date($data->eta, 'U', 0)
	    );
	}
	
	
	public function nsd_tracking_event_is_exception ( $event ) 
	{
	    // Handles some special logic for exceptions provided by client
	    if ( $event['code'] === 'POD' ) {
	        // "If POD description is “Delivery Exception Occurred”, change to red.  Otherwise, green"
	        // Account for typo in API which contains trailing apostrophe ("Delivery Exception occurred'")
	        if ( stripos($event['description'], 'Delivery Exception occurred') !== false ) {
	            return true;
	        } else if ( stripos($event['description'], 'Pickup Exception occurred') !== false ) { // Requested by client
	            return true;
	        }
	    }
	    return false;
	}
	
	
	public function nsd_tracking_history ( $history ) 
	{
	    // Sort oldest to newest
	    usort($history, function ( $a, $b ) {
	        if ( $a['timestamp'] === $b['timestamp'] ) {
	            return 0;
	        }
	        return $a['timestamp'] > $b['timestamp'] ? 1 : -1;
	    });
	
	    // Fill in missing locations
	    $last_location = '';
	    foreach ( $history as $index => $event ) {
	        if ( empty($event['location']) ) {
	            $history[$index]['location'] = $last_location;
	        } else {
	            $last_location = $event['location'];
	        }
	    }
	
	    // Flip it back for display
	    $history = array_reverse($history);
	
	    return $history;
	}
	
	
	public function nsd_tracking_activity ( $code, $description, $tracking_no, $default = '-' ) 
	{
	    $code = (string) $code;
	    $description = (string) $description;
	    $tracking_no = (string) $tracking_no;
	
		$code = ( $code == "SCHEDULED" && $tracking_no[0] == "R" ) ? "SCHEDULEDPICKUP" : $code ;
	
		#echo $tracking_no;
		#var_dump($tracking_no);
		
	
	
	    $activities = array(
	        'BCCI' => 'Unable to reach customer; Please email us at customercare@nonstopdelivery.com',
	        'CCCS' => 'Customer Called to Check Stat',
	        'CCXL' => 'Customer Cancelled', // note: spelling
	        'CDAM' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'CDPL' => 'Departed Pick-up Location',
	        'COMP' => 'Completed',
	        'CRCU' => 'Unable to reach customer; Please email us at customercare@nonstopdelivery.com',
	        'CRDL' => 'Customer Refused Delivery',
	        'CRST' => 'Customer Requesting Storage',
	        'CWOR' => 'Customer Waiting on Replacement', // note: updated
	        'DELA' => 'Delivery Attempted',
	        'DELR' => 'Delivery Rescheduled',
	        'DELS' => 'Delivery Scheduled',
	        'DOCK' => 'Arrived at local NSD terminal',
	        'LTLD' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'LTLR' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'LTLS' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'LVMC' => 'Unable to reach customer; Please email us at customercare@nonstopdelivery.com',
	        'MECH' => 'Mechanical',
	        'NEW' => 'Order registered',
	        'NSDE' => 'Dispatch Error',
	        'OCIT' => 'Departed Pick-up Location',
	        'ODEL' => 'Out for Delivery',
	        'PNA' => 'Product Not Arrived',
	        'POD' => 'Proof of Delivery',
	        'PSFO' => 'Pending Shipment from Origin',
	        'UPINF' => 'Updated Order Information',
	        'WPU' => 'Awaiting LTL pickup',
	        'PICKEDUP' => 'Pickup Complete',
	        'WTHR' => 'Weather',
	        'CANCEL' => '', //Is this case we don’t want to show anything for tracking, it is like order didn’t exist (hard cancelled).
	        'TENDER' => 'Order Registered',
	        'SHPTACK' => 'Order Registered',
	        'ARRSHIP' => 'Order Registered',
	        'DEPSHIP' => 'Order Shipped',
	        'ARRTERM' => 'In Transit',
	        'DEPTERM' => 'In Transit',
	        'ARRDLVTERM' => 'In Transit',
	        'DEPDLVTERM' => 'In Transit',
	        'OFP' => 'Out for Pick Up',
	        'AVAIL' => 'Order Registered',
	        'DOCKED' => 'Arrived at local NSD terminal',
	        'SCHEDULED' => 'Delivery Scheduled',
	        'DELIVERED' => 'Delivered',
	        'OFD' => 'Out for Delivery',
	        'COMPLETED' => 'Completed',
	        'AVAIL-R' => 'Order Registered',
	        'CANCEL' => 'Order Canceled',
	        'CANCELLED' => 'Order Canceled',
	        'INTRANSIT' => 'In Transit',
	        'LTLU' => 'In Transit', 
	        'STORAGE' => 'Customer Requesting Storage',
	        'SCHEDULEDPICKUP' => 'Pickup Scheduled'
	    );

	    // Code of "UNSTAT" or "RWSTAT"
	    $stat_activities = array(
	        'Arrived at Terminal Location. B01 Refused by Customer;' => 'Delivery exception occurred; Please email us at customercare@nonstopdelivery.com',
	        'Arrived at Terminal Location. Held for Consignee;' => 'Arrived at linehaul Terminal',
	        'Arrived at Terminal Location. Held for Full Carrier Load;' => 'Arrived at linehaul Terminal',
	        'Arrived at Terminal Location. Held Pending Appointment;' => 'Arrived at linehaul Terminal',
	        'Arrived at Terminal Location. Prearranged Appointment;' => 'Arrived at linehaul Terminal',
	        'Arrived at Terminal Location. Weather or Natural Disaster Related;' => 'Weather',
	        'Arrived at Terminal Location;' => 'Arrived at linehaul Terminal', // Formerly "Arrived at terminal"
	        'Carrier Departed Pick-up Location with Shipment;' => 'Departed Pick-up Location', // note: added semicolon
	    );
	

	    if ( Nsd_trackingModelTrack::nsd_tracking_event_is_exception(array('code' => $code, 'description' => $description)) ) {
	        return 'Exception Occurred';
	    } else if ( array_key_exists($code, $activities) ) {
	        // Return the activity
	        return $activities[$code];
	    } else if ( ($code === 'UNSTAT' || $code === 'RWSTAT') && array_key_exists($description, $stat_activities) ) {
	        // Return the activity
	        return $stat_activities[$description];
	    } else {
	        // Unknown or do not show
	        //var_dump($code, $description);
	        return $default;
	    }
	
	}
	
	
	
	public function nsd_tracking_date ( $datetime, $format, $default = '-' ) 
	{
	    // Cast as string (gets rid of XML element)
	    $datetime = (string) $datetime;
	    $matches = array();
	
	    // Check the date format
	    if ( preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}$/', $datetime) && $time = strtotime($datetime) ) {
	        // "Normal" date format that we know will parse
	        return date($format, $time);
	    } else if ( preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2} [C|E]T$/', $datetime) && $time = strtotime(substr($datetime, 0, -3) . ':00') ) { // Assume local time
	        // @note - Applies to UNSTAT and RWSTAT events - "2015-06-30T21:19 ET", "2015-06-30T00:01 CT". 1455535
	        // "Linehaul" date format that we know will parse
	        return date($format, $time);
	    } else if ( preg_match('#^[a-z]{3} ([0-9]{1,2}/[0-9]{1,2}/[0-9]{4})$#i', $datetime, $matches) && $time = strtotime($matches[1]) ) {
	        // Estimated Delivery Date format (secondary)
	        return date($format, $time);
	    } else { 
	        // Ignore rather than display 1970
	        return $default;
	    }
	}
	


	public function nsd_tracking_stop ( $data ) 
	{
	    $attributes = $data->attributes();
	    return array(
	        'sequence' => (int) $attributes['sequence'],
	        'city' => (string) $data->City
	    );
	}


	function nsd_tracking_shipping ( $type, $history ) 
	{
	
	    /**
	     * If Door-to-Door order
	     *     If UNSTAT status is available
	     *         Use earliest UNSTAT date
	     *     Else 
	     *         Item has not shipped, no date is available
	     * Else 
	     *     Order will not have a Ship Date, and that line should be hidden from view
	     * 
	     * Only Door-to-Door should have a Ship Date, Last Mile and Returns will not and should not have that line shown
	     *
	     * The ship date should be under the “Carrier Departed Pick-up Location with Shipment” line (example: 1466604).  
	     * However, in this case (1455557) it doesn’t look like this was sent by the linehaul carrier.  
	     * In cases where this is missing, can we use the earliest date from the “Arrived at Terminal Location” entries?
	     * Yes, the ship date should never be tied to the DOCK date.
	     */
	    if ( $type === 'door-to-door' ) { // If door-to-door
	        $reverse_history = array_reverse($history); // Read oldest to newest
	        $shipping = array_reduce($reverse_history, function ( $shipping, $event ) { // In case there are multiple ship date events, use the latest
	            if ( empty($shipping) && $event['code'] === 'UNSTAT' ) {
	                $shipping = array(
	                    'Ship Date' => date('F j, Y', $event['timestamp']) // No time component
	                );
	            }
	            return $shipping;
	        }, array());
	    } else {
	        $shipping = array(); // No ship date
	    }
	
	    return $shipping;
	}


	public function nsd_tracking_delivery ( $type, $phase, $estimated, $history ) 
	{
	
	    if ( $type === 'return' ) {
	        $language = 'Pickup';
	    } else {
	        $language = 'Delivery';
	    }
	
	    /**
	     * If Order is a Delivery (D2D or Last Mile):
	     *     If Order is CLOSED, show
	     *         Actual Delivery Date:   (POD Date)
	     *         POD Info:       (POD Name)
	     *     If NOT CLOSED but IS SCHEDULED
	     *         Scheduled Delivery Date:    (Scheduled Date 
	     *                     and Window)
	     *     OTHERWISE, show
	     *         Estimated Delivery Date:    (ETA Date)
	     *         Scheduled Delivery Date:    Not Scheduled
	     * If Order is a Return:
	     *     If orders is CLOSED, show
	     *         Actual Pickup Date: (POD Date)
	     *         POD Info: (POD Name)
	     *     If NOT CLOSED but IS SCHEDULED
	     *         Scheduled Pickup Date: (Scheduled date
	     *                     and window)
	     *     OTHERWISE, show
	     *         Scheduled Pickup Date: Not Scheduled
	     *
	     * New Rule, 9/8/2015:
	     * In the case where the POD Info is “Delivery Exception occurred”
	     *      Could the Actual Delivery Date line either be blanked out
	     */
	    if ( $phase === 'closed' ) { // Closed
	        $delivery = array_reduce($history, function ( $proof_of_delivery, $event ) use ( $language ) { // In case there are multiple POD events, use the latest
	            if ( empty($proof_of_delivery) && $event['code'] === 'POD' || empty($proof_of_delivery) && $event['code'] === 'DELIVERED') {
	                if ( Nsd_trackingModelTrack::nsd_tracking_event_is_exception($event) ) {
	                    $proof_of_delivery = array(
	                        'POD Info' => rtrim($event['description'], " ';") // Trim off extra characters the API includes
	                    );
	                } else {
		                
	/*
		                if ( $event['eta'] != "" )
		                {
			                $proof_of_delivery = array(
							#'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['timestamp']),
	                        'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['eta']),
	                        'POD Info' => rtrim($event['description'], " ';") // Trim off extra characters the API includes
							);
		                }
		                else
		                {
			                $proof_of_delivery = array(
							'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['timestamp']),
	                        #'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['eta']),
	                        'POD Info' => rtrim($event['description'], " ';") // Trim off extra characters the API includes
							);
		                }
	*/
		                
	                    $proof_of_delivery = array(
	
							
	                        'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['timestamp_real']),
	                        #'Actual ' . $language . ' Date' => date('F j, Y, g:ia', $event['eta']),
	
	
	                        'POD Info' => rtrim($event['description'], " ';") // Trim off extra characters the API includes
	                    );
	                }
	            }
	            return $proof_of_delivery;
	        }, array());
	    } else if ( $phase === 'scheduled' ) { // Scheduled
	
	        $delivery = array_reduce($history, function ( $scheduled, $event ) use ( $language, $estimated ) { // In case there are multiple DELS events, use the latest
	
	
	            if ( (empty($scheduled) && $event['code'] === 'DELS') ) {
	                // "Delivery scheduled: 2015-07-09, 12:00 pm -  4:00 pm"
	                
	                $matches = array();
	                if ( preg_match('/: (\d{4}-\d{2}-\d{2}),( .*)/', $event['description'], $matches) ) {
	                    $scheduled = array(
	                        'Scheduled ' . $language . ' Date' => date('F j, Y,', strtotime($matches[1])) . $matches[2]
	                    );
	                }
	            }
	            
	
	            
	            if ( empty($scheduled) && $event['code'] === 'SCHEDULED' ) {
	                // "Delivery scheduled: 2015-07-09, 12:00 pm -  4:00 pm"
	                
				    if (  strpos($estimated, '-') !== FALSE )
				    {                
						#2016-10-25T00:00:00
						
						#orig
		                #$scheduled = array( 'Scheduled ' . $language . ' Date' => _nsd_tracking_estimated_date($estimated, 'F j, Y') );
	
		                $year = (int) substr($estimated, 0, 4);  // year
		                $month = (int) substr($estimated, 5, 2);  // month
		                $day = (int) substr($estimated, 8, 2);  // day
		                $hour = (int) substr($estimated, 11, 2);  // hour
		                $minute = (int) substr($estimated, 14, 2);  // minute   
	
		                $theDate = date('F j, Y, g:i a', mktime( $hour, $minute, 0, $month, $day, $year) ). ' - '.date('g:i a', mktime( ($hour+4), $minute, 0, $month, $day, $year) ) ;                
		
		                $scheduled = array( 'Scheduled ' . $language . ' Date' =>  $theDate ); 
	
					}
					else
					{
	
		                $year = (int) substr($estimated, 0, 4);  // year
		                $month = (int) substr($estimated, 4, 2);  // month
		                $day = (int) substr($estimated, 6, 2);  // day
		                $hour = (int) substr($estimated, 8, 2);  // hour
		                $min = (int) substr($estimated, 10, 2);  // minute   
		                             
		                $theDate = date('F j, Y, g:i a', mktime( $hour, $min, 0, $month, $day, $year) ). ' - '.date('g:i a', mktime( ($hour+4), $min, 0, $month, $day, $year) ) ;                
		
		                $scheduled = array( 'Scheduled ' . $language . ' Date' =>  $theDate ); 
					}                
	            }
	
	            return $scheduled;
	        }, array());
	    } else if ( $type === 'return' ) {
	        $delivery = array(
	            'Scheduled ' . $language . ' Date' => 'Not Scheduled'
	        );
	    } else {
	
		        $delivery = array(	        
		            'Estimated ' . $language . ' Date' => Nsd_trackingModelTrack::nsd_tracking_estimated_date($estimated, 'F j, Y'),
		            'Scheduled ' . $language . ' Date' => 'Not Scheduled'
		        );
	    }
	
	    return $delivery;
	}	
		
	
	public function nsd_tracking_estimated_date ( $datetime, $format ) 
	{
	    // Cast as string (gets rid of XML element)
	    $datetime = (string) $datetime;
	    $default = 'N/A'; // If $datetime is not parseable or empty
	
	    $date = Nsd_trackingModelTrack::nsd_tracking_date($datetime, $format, $default);
	
	    if ( strlen($datetime) > 1 && $date === $default ) {
	        return $datetime;
	    }
	
	    return $date;
	}	
		
		

	
	public function nsd_tracking_status ( $type, $phase, $history ) 
	{
	    // First event
	    $event = $history[0];
	
	    if ( $type === 'return' ) {
	        $language = 'Pickup';
	    } else {
	        $language = 'Delivery';
	    }
	
	    /**
	     * If Order is a Delivery (D2D or Last Mile):
	     *     If Order is CLOSED and POD is "Delivery Exception occurred", show 
	     *         Delivery Exception Occurred
	     *     If Order is CLOSED and no exception, show
	     *         Completed
	     *     If NOT CLOSED, show
	     *         (Last Update Note)
	     * If Order is a Return:
	     *     If Order is CLOSED and POD is Delivery Exception occurred", show
	     *         Pickup Exception Occurred
	     *     If Order is CLOSED and no exception, show
	     *         Completed
	     *     If NOT CLOSED, show
	     *         (Last Update Note)
	     *
	     * New Rule, 9/8/2015:
	     * In the case where the POD Info is “Delivery Exception occurred”
	     *      Could the Current Status be amended to read “Delivery Exception Occurred; Please email us at customercare@nonstopdelivery.com”
	     */
	    if ( $phase === 'closed' ) { // Closed
	        // Find POD event
	        $proof_of_delivery = array_reduce($history, function ( $proof_of_delivery, $event ) use ( $language ) { // In case there are multiple POD events, use the latest
	            if ( empty($proof_of_delivery) && $event['code'] === 'POD' ) {
	                $proof_of_delivery = $event;
	            }
	            return $proof_of_delivery;
	        }, null);
	
	        if ( Nsd_trackingModelTrack::nsd_tracking_event_is_exception($proof_of_delivery) ) {
	            return array(
	                'class' => 'tracking-bad',
	                'activity' => $language . ' Exception Occurred; Please email us at customercare@nonstopdelivery.com'
	            );
	        } else if ( $proof_of_delivery ) { // As long as POD exists
	            return array(
	                'class' => 'tracking-good',
	                'activity' => 'Completed'
	            );
	        }
	    }
	
	    // Everything else displays the last event status
	    return array(
	        'class' => Nsd_trackingModelTrack::nsd_tracking_class($event),
	        'activity' => $event['activity']
	    );
	}

	public function nsd_tracking_city ( $city, $default = null ) 
	{
	    // Cast as string (gets rid of XML element)
	    $city = (string) $city;
	
	    if ( strlen(trim($city)) > 0 ) {
	        // PHOENIX           AZ 85004
	        $city = preg_replace('/\s+[0-9]{5}$/', '', trim($city)); // Strip off zip code
	        $city = preg_replace('/\s+([A-Z]{2})$/', ', $1', trim($city)); // Add comma between city and state
	        return $city;
	    } else { 
	        // Ignore
	        return $default;
	    }
	}


	public function lew_nsd_tracking_city ( $city, $default = null ) 
	{
	    // Cast as string (gets rid of XML element)
	    $city = (string) $city;
	
	    if ( strlen(trim($city)) > 0 ) {
	        // PHOENIX           AZ 85004
	        $city = preg_replace('/\s+[0-9]{5}$/', '', trim($city)); // Strip off zip code
	        $city = preg_replace('/\s+([A-Z]{2})$/', ', $1', trim($city)); // Add comma between city and state
	        return $city;
	    } else { 
	        // Ignore
	        return $default;
	    }
	}


	public function nsd_tracking_class ( $event ) 
	{
	
	    $code_classes = array(
	        'BCCI' => 'tracking-bad',
	        //'CCCS' => 'No change',
	        'CCXL' => 'tracking-bad',
	        'CDAM' => 'tracking-bad',
	        'CDPL' => 'tracking-good',
	        'COMP' => 'tracking-good',
	        'CRCU' => 'tracking-bad',
	        'CRDL' => 'tracking-bad',
	        'CRST' => 'tracking-good',
	        //'CWOR' => 'No change',
	        'DELA' => 'tracking-bad',
	        'DELR' => 'tracking-bad',
	        'DELS' => 'tracking-good',
	        'DOCK' => 'tracking-good',
	        //'INVEN' => 'No change',
	        //'LTLBOL' => 'No change',
	        'LTLD' => 'tracking-bad',
	        'LTLR' => 'tracking-bad',
	        'LTLS' => 'tracking-bad',
	        'LTLU' => 'tracking-good',
	        'LVMC' => 'tracking-bad',
	        'MECH' => 'tracking-bad',
	        'NEW' => 'tracking-good',
	        'NSDE' => 'tracking-bad',
	        'OCIT' => 'tracking-good',
	        'ODEL' => 'tracking-good',
	        //'OTHR' => 'No change',
	        'PNA' => 'tracking-bad',
	        'POD' => 'tracking-good',
	        //'PSFO' => 'No change',
	        //'UPINF' => 'No change',
	        //'WPU' => 'No change',
	        'WTHR' => 'tracking-bad',
	        'CANCEL' => 'tracking-bad',
	        'TENDER' => 'tracking-good',
	        'SHPTACK' => 'tracking-good',
	        'ARRSHIP' => 'tracking-good',
	        'DEPSHIP' => 'tracking-good',
	        'ARRTERM' => 'tracking-good',
	        'DEPTERM' => 'tracking-good',
	        'ARRDLVTERM' => 'tracking-good',
	        'DEPDLVTERM' => 'tracking-good',
	        'OFP' => 'tracking-good',
	        'SCHEDULED' => 'tracking-good',
	        'DELIVERED' => 'tracking-good',
	        'OFD' => 'tracking-good',
	        'DOCKED' => 'tracking-good',
	        'AVAIL-R' => 'tracking-good',
	        'INTRANSIT' => 'tracking-good',
	        'CANCELLED' => 'tracking-bad',
	        'STORAGE' => 'tracking-good'
	    );
	
	    // Code of "UNSTAT" or 'RWSTAT'
	    $stat_classes = array(
	        'Arrived at Terminal Location. B01 Refused by Customer;' => 'tracking-bad',
	        'Arrived at Terminal Location. Held for Consignee;' => 'tracking-good',
	        'Arrived at Terminal Location. Held for Full Carrier Load;' => 'tracking-good',
	        'Arrived at Terminal Location. Held Pending Appointment;' => 'tracking-good',
	        'Arrived at Terminal Location. Prearranged Appointment;' => 'tracking-good',
	        'Arrived at Terminal Location. Weather or Natural Disaster Related;' => 'tracking-bad',
	        'Arrived at Terminal Location;' => 'tracking-good',
	        'Carrier Departed Pick-up Location with Shipment;' => 'tracking-good', // note: added semicolon
	    );
	
	    if ( Nsd_trackingModelTrack::nsd_tracking_event_is_exception($event) ) {
	        return 'tracking-bad';
	    } else if ( array_key_exists($event['code'], $code_classes) ) {
	        return $code_classes[$event['code']];
	    } else if ( ($event['code'] === 'UNSTAT' || $event['code'] === 'RWSTAT') && array_key_exists($event['description'], $stat_classes) ) {
	        return $stat_classes[$event['description']];
	    } else {
	        // No class
	        return '';
	    }
	}


	public function nsd_tracking_position ( $type, $state, $history ) 
	{
	    
	    // Append Arrived or Departed for certain D2D statuses
	    if ( $type === 'door-to-door' && $state === 'PEND' ) {
	        // CHECK FOR UNSTAT/RWSTAT
	        // @link http://nstracking.nonstopdelivery.com/api/rest/nsdws/tracking/ws-track-shipment/9495546
	        // A "Departed" or "Arrived" event means shipped
	        $state .= array_reduce($history, function ( $shipped, $event ) {
	            if ( empty($shipped) && ($event['code'] === 'UNSTAT' || $event['code'] === 'RWSTAT') ) {
	                if ( stripos($event['description'], 'Carrier Departed') === 0 || stripos($event['description'], 'Arrived') === 0 ) {
	                    // <description>Carrier Departed Pick-up Location with Shipment;</description>
	                    // <description>Arrived at Terminal Location;</description>
	                    $shipped = 'Shipped';
	                }
	            }
	            return $shipped;
	        }, '');
	    }
	
	    // Returns the position the state corresponds to
	    $positions = array(
	        'door-to-door' => array(
	            'PEND' => 1,
	            'PENDShipped' => 2,
	            'DOCK' => 3,
	            'SCHD' => 3,
	            'ACTV' => 4,
	            'FIN' => 5,
	            'RTNS' => 3,
	            'WPU' => 3,
	            'STOR' => 3,
	        ),
	        'last-mile' => array(
	            'PEND' => 1,
	            'DOCK' => 2,
	            'SCHD' => 2,
	            'ACTV' => 3,
	            'FIN' => 4,
	            'RTNS' => 2,
	            'WPU' => 2,
	            'STOR' => 2,
	        ),
	        'return' => array(
	            'PEND' => 1,
	            'DOCK' => 1,
	            'SCHD' => 2,
	            'ACTV' => 2,
	            'FIN' => 3,
	            'RTNS' => 1,
	            'WPU' => 3,
	            'STOR' => 1,
	        ),
	    );
	
	    if ( array_key_exists($type, $positions) && array_key_exists($state, $positions[$type]) ) {
	        return $positions[$type][$state];
	    } else {
	        return false;
	    }
	}



	
	public function nsd_tracking_bar ( $type, $position, $history, $stops ) 
	{
	    switch ( $type ) {
	    case 'door-to-door':
	        return array(
	            array(
	                'class' => 'tracking-bar-soon' . Nsd_trackingModelTrack::nsd_tracking_chevron(1, $position, $history),
	                'label' => 'Shipping Soon'
	            ),
	            array(
	                'class' => 'tracking-bar-shipped' . Nsd_trackingModelTrack::nsd_tracking_chevron(1.5, $position, $history),
	                'label' => 'Shipped'
	            ),
	            array(
	                'class' => 'tracking-bar-transit' . Nsd_trackingModelTrack::nsd_tracking_chevron(2, $position, $history),
	                'label' => 'In Transit'
	            ),
	            array(
	                'class' => 'tracking-bar-arrived' . Nsd_trackingModelTrack::nsd_tracking_chevron(3, $position, $history),
	                'label' => 'Arrived at Last Mile Terminal'
	            ),
	            array(
	                'class' => 'tracking-bar-out' . Nsd_trackingModelTrack::nsd_tracking_chevron(4, $position, $history),
	                'label' => 'Out for Delivery'
	            ),
	            array(
	                'class' => 'tracking-bar-delivered' . Nsd_trackingModelTrack::nsd_tracking_chevron(5, $position, $history),
	                'label' => 'Delivered'
	            ),
	        );
	        break;
	    case 'last-mile': 
	        return array(
	            array(
	                'class' => 'tracking-bar-enroute' . Nsd_trackingModelTrack::nsd_tracking_chevron(1, $position, $history),
	                'label' => 'Enroute to Last Mile Terminal'
	            ),
	            array(
	                'class' => 'tracking-bar-arrived' . Nsd_trackingModelTrack::nsd_tracking_chevron(2, $position, $history),
	                'label' => 'Arrived at Last Mile Terminal'
	            ),
	            array(
	                'class' => 'tracking-bar-out' . Nsd_trackingModelTrack::nsd_tracking_chevron(3, $position, $history),
	                'label' => 'Out for Delivery'
	            ),
	            array(
	                'class' => 'tracking-bar-delivered' . Nsd_trackingModelTrack::nsd_tracking_chevron(4, $position, $history),
	                'label' => 'Delivered'
	            ),
	        );
	        break;
	    case 'return':
	        return array(
	            array(
	                'class' => 'tracking-bar-notified' . Nsd_trackingModelTrack::nsd_tracking_chevron(1, $position, $history),
	                'label' => 'Notified of Pick-up Order'
	            ),
	            array(
	                'class' => 'tracking-bar-scheduled' . Nsd_trackingModelTrack::nsd_tracking_chevron(2, $position, $history),
	                'label' => 'Scheduled for Pick-up'
	            ),
	            array(
	                'class' => 'tracking-bar-picked' . Nsd_trackingModelTrack::nsd_tracking_chevron(3, $position, $history),
	                'label' => 'Picked up'
	            ),
	        );
	        break;
	    default:
	        return array();
	    }
	}
	

	public function nsd_tracking_chevron ( $index, $position, $history ) 
	{
	    $class = '';
	    if ( $position >= $index ) {
	        $class .= ' active';
	    } else {
	        $class .= ' inactive';
	    }
	
	    if ( $position === $index ) {
	        $class .= ' current';        
	
	        // Allow multiple codes at the exact same time
	        $stop = false;
	        foreach ( $history as $event ) {
	            // Stop looking if we are beyond the stop time
	            if ( $stop && $event['timestamp'] < $stop ) {
	                break;
	            }
	
	            // Check code for color
	            if ( $event_class = Nsd_trackingModelTrack::nsd_tracking_class($event) ) {
	                $class .= ' ' . $event_class;
	                // Set stop time to allow for other codes at the same time
	                $stop = $event['timestamp'];
	            }
	        }
	    }
	
	    return $class;
	}





#close
}


