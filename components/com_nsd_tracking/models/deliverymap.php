<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Nsd_tracking model.
 *
 * @since  1.6
 */
class Nsd_trackingModelDeliverymap extends JModelItem
{



	// !getTMW
	
	public function getTMW( $objForm )
	{
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web&trace=1063701216
		# http://devprocess.nonstopdelivery.com/index.php?option=com_nsd_tracking&task=test_tmw4web&trace=31065532&prod=1


		$controllerName = "Nsd_trackingModelDeliverymap";
		$functionName = "getTMW";
		
		$db =& JFactory::getDBO();
		


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams();
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->tmw4web_user				= $params->get('tmw4web_user');
		$paramsComponent->tmw4web_password			= $params->get('tmw4web_password');
		$paramsComponent->tmw4web_subdomain			= $params->get('tmw4web_subdomain');
		$paramsComponent->flag_test_mode			= $params->get('flag_test_mode');
		$paramsComponent->tmw4web_user_test			= $params->get('tmw4web_user_test');
		$paramsComponent->tmw4web_password_test		= $params->get('tmw4web_password_test');
		$paramsComponent->tmw4web_subdomain_test	= $params->get('tmw4web_subdomain_test');
		
		if ( $paramsComponent->flag_test_mode == "0"  )
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password;
			$domain = $paramsComponent->tmw4web_subdomain;   
			
		}
		else
		{
			$tm4web_usr = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_user : $paramsComponent->tmw4web_user_test	;
			$tm4web_pwd = ( $objForm->flag_api == "1" ) ? $objForm->tmw4web_password : $paramsComponent->tmw4web_password_test;
			$domain = $paramsComponent->tmw4web_subdomain_test;   		
		}
		

		$order_status = $objForm->order_status;

		$flag_production = 0;

		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
            
/*
			$tm4web_usr = 'NSD11447';
			$tm4web_pwd = '11447NSD';
			$subdomain = 'nonstopdelivery';
*/
            

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "tracking_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "tracking_prod";
            
/*
			$tm4web_usr = 'TRACKING';
			$tm4web_pwd = '1234';
			$subdomain = 'nonstoptest';   
*/         
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r paramsComponent: ".print_r($paramsComponent, true);
		$logMessage .= "\n\r flag_production: ".print_r($flag_production, true);
		$logMessage .= "\n\r trace_number: ".print_r($trace_number, true);
		$logMessage .= "\n\r objForm: ".print_r($objForm, true);
		$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
		$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
		$logMessage .= "\n\r subdomain: ".print_r($subdomain, true);
		
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$objReturn = new stdClass();

		if ( $tm4web_usr == "" || $tm4web_pwd == "" )
		{

			$objReturn->error = 1;
			$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "Invalid API Username or Password"  : "<h2>Invalid API Username or Password</h2>An invalid username or password was used.";


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No Credentials";
			$logMessage .= "\n\r tm4web_usr: ".print_r($tm4web_usr, true);
			$logMessage .= "\n\r tm4web_pwd: ".print_r($tm4web_pwd, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
	
		}
		else
		{
	

			if ( $order_status != "" )
			{
		
				$tmp_fname = tempnam("/tmp", "COOKIE");
		
		
				#https://nonstopdelivery.tmwcloud.com/login/do_login.msw?tm4web_usr=NSD11447&tm4web_pwd=11447NSD
	
				
				$loginUrl = "https://".$domain."/login/do_login.msw";
				
				//init curl
				$ch = curl_init();
				
				//Set the URL to work with
				curl_setopt($ch, CURLOPT_URL, $loginUrl);
				
				// ENABLE HTTP POST
				curl_setopt($ch, CURLOPT_POST, 1);
				
				//Set the post parameters
				curl_setopt($ch, CURLOPT_POSTFIELDS, 'tm4web_usr='.$tm4web_usr.'&tm4web_pwd='.$tm4web_pwd);
				
				//Handle cookies for the login
				curl_setopt ($ch, CURLOPT_COOKIEJAR, $tmp_fname);
				curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
				curl_setopt ($ch, CURLOPT_COOKIESESSION, 1);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
				//execute the request (the login)
				$store = curl_exec($ch);
				
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				#$logMessage .= "\n\r store: ".print_r($store, true);
				$logMessage .= "\n\r ch: ".print_r($ch, true);
				$logMessage .= "\n\r tmp_fname: ".print_r($tmp_fname, true);
				
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }
				
		
				$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&take=30&skip=0&page=16&pageSize=30&TLORDER__CURRENT_STATUS=".$order_status;
				
				
				
				curl_setopt ($ch, CURLOPT_HTTPGET, 1);
				curl_setopt ($ch, CURLOPT_URL, $queryURL);
				curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt ($ch, CURLOPT_HEADER, 0);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
				//execute the request
				$contentQuery = curl_exec($ch);
				
				#echo "contentQuery: <PRE>".htmlentities($contentQuery)."</PRE>";
		
				if(curl_errno($ch)){
				    echo 'Curl error: ' . curl_error($ch);
				}
		
				#print_r(curl_getinfo($ch));
		
				$stop_type = ( $order_status == "OFD" ) ? "Delivery" : "Return" ;
		
				$objDataOrder = json_decode($contentQuery);
				
				$arrOutput = array();
				
				foreach( $objDataOrder->dataResults as $order ) 
				{
	
					if ( $order->DESTPC != "")
					{
						$arrOutput[] = array( 'zip'  => $order->DESTPC, 'type' => $stop_type );						
					}
				
				}
				
				sort($arrOutput);
				
				$countArray = count( $arrOutput );
				
				if ( $countArray == 480 )
				{

					$queryURL = "https://".$domain."/trace/trace_class.msw?include_all_clients=false&take=30&skip=480&page=50&pageSize=30&TLORDER__CURRENT_STATUS=".$order_status;
					
					curl_setopt ($ch, CURLOPT_HTTPGET, 1);
					curl_setopt ($ch, CURLOPT_URL, $queryURL);
					curl_setopt ($ch, CURLOPT_COOKIEFILE, $tmp_fname);
					curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt ($ch, CURLOPT_HEADER, 0);
					curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
					
					//execute the request
					$contentQuery2 = curl_exec($ch);
					
					$objDataOrder2 = json_decode($contentQuery2);
					
					foreach( $objDataOrder2->dataResults as $order2 ) 
					{
		
						if ( $order->DESTPC != "")
						{
							$arrOutput[] = array( 'zip'  => $order->DESTPC, 'type' => $stop_type );						
						}
						
					}
					
					sort($arrOutput);
				}
				
				
				
				$arrOutput = array_unique($arrOutput, SORT_REGULAR);
				
				$arrOutput = array_values($arrOutput);				
				
		
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
				$logMessage .= "\n\r ch: ".print_r($ch, true);
				$logMessage .= "\n\r queryURL: ".print_r($queryURL, true);
				#$logMessage .= "\n\r contentQuery: ".print_r($contentQuery, true);
				#$logMessage .= "\n\r objDataOrder: ".print_r($objDataOrder, true);
				#$logMessage .= "\n\r order_status: ".print_r($order_status, true);
				#$logMessage .= "\n\r arrOutput: ".print_r($arrOutput, true);
				$logMessage .= "\n\r countArray: ".print_r($countArray, true);
				
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

				$objReturn->arrOutput = $arrOutput;
				$objReturn->countArray = $countArray;
				$objReturn->error = 0;
				$objReturn->error_message = "";


			
			}
			else
			{
	
				$objReturn->error = 1;
				$objReturn->error_message = ( $objForm->flag_api == "1" ) ? "No Order Status was entered."  :   "<h2>No Order Status</h2>No tracking number was entered. Please enter your tracking number.";
	
				$logMessage = "INSIDE | ".$controllerName." | ".$functionName ." | No trace number";
				$logMessage .= "\n\r ";
				if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
				if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }	
		
			}
		}



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		return $objReturn;
		
	}	

}
