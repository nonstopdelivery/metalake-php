<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Nsd_tracking records.
 *
 * @since  1.6
 */
class Nsd_trackingModelTracks extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		

		// List state information.
		parent::populateState($ordering, $direction);

        $app = Factory::getApplication();

        $ordering  = $app->getUserStateFromRequest($this->context . '.ordercol', 'filter_order', $ordering);
        $direction = $app->getUserStateFromRequest($this->context . '.orderdirn', 'filter_order_Dir', $ordering);

        $this->setState('list.ordering', $ordering);
        $this->setState('list.direction', $direction);

        $start = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'int');
        $limit = $app->getUserStateFromRequest($this->context . '.limit', 'limit', 0, 'int');

        if ($limit == 0)
        {
            $limit = $app->get('list_limit', 0);
        }

        $this->setState('list.limit', $limit);
        $this->setState('list.start', $start);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		$db	= $this->getDbo();
		$query	= $db->getQuery(true);

		return $query;
	}

	/**
	 * Method to get an array of data items
	 *
	 * @return  mixed An array of data on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
		
		foreach ($items as $item)
		{

			$item->delivery_type = JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_DELIVERY_TYPE_OPTION_' . strtoupper($item->delivery_type));

			$item->order_type = JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_ORDER_TYPE_OPTION_' . strtoupper($item->order_type));

			if (isset($item->order_status))
			{
				$values    = explode(',', $item->order_status);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db    = Factory::getDbo();
						$query = "select '' as 'key', '' as 'value' from htc_nsd_tracking_lu_order_status a union select b.name as 'key', b.name as 'value' from htc_nsd_tracking_lu_order_status b where b.state = 1";

						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$item->order_status = !empty($textValue) ? implode(', ', $textValue) : $item->order_status;
			}

			if (isset($item->bar_state))
			{
				$values    = explode(',', $item->bar_state);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db    = Factory::getDbo();
						$query = "select '' as 'key', '' as 'value' from htc_nsd_tracking_lu_bar_state a union select b.name as 'key', b.name as 'value' from htc_nsd_tracking_lu_bar_state b where b.state = 1";

						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$item->bar_state = !empty($textValue) ? implode(', ', $textValue) : $item->bar_state;
			}

			$item->bar_color = JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_BAR_COLOR_OPTION_' . strtoupper($item->bar_color));

			if (isset($item->current_state))
			{
				$values    = explode(',', $item->current_state);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db    = Factory::getDbo();
						$query = "select '' as 'key', '' as 'value' from htc_nsd_tracking_lu_current_state a union select b.name as 'key', b.name as 'value' from htc_nsd_tracking_lu_current_state b where b.state = 1 HAVING key LIKE '" . $value . "'";

						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$item->current_state = !empty($textValue) ? implode(', ', $textValue) : $item->current_state;
			}

			if (isset($item->status_location))
			{
				$values    = explode(',', $item->status_location);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db    = Factory::getDbo();
						$query = "select '' as 'key', '' as 'value' from htc_nsd_tracking_lu_locations a union select b.name as 'key', b.name as 'value' from htc_nsd_tracking_lu_locations b where b.state = 1 HAVING key LIKE '" . $value . "'";

						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$item->status_location = !empty($textValue) ? implode(', ', $textValue) : $item->status_location;
			}
				$item->flag_tracking_number = empty($item->flag_tracking_number) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_TRACKING_NUMBER_OPTION_' . strtoupper($item->flag_tracking_number));
				$item->flag_po_number = empty($item->flag_po_number) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_PO_NUMBER_OPTION_' . strtoupper($item->flag_po_number));
				$item->flag_ref_1_number = empty($item->flag_ref_1_number) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_REF_1_NUMBER_OPTION_' . strtoupper($item->flag_ref_1_number));
				$item->flag_ref_2_number = empty($item->flag_ref_2_number) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_REF_2_NUMBER_OPTION_' . strtoupper($item->flag_ref_2_number));
				$item->flag_service_level = empty($item->flag_service_level) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_SERVICE_LEVEL_OPTION_' . strtoupper($item->flag_service_level));
				$item->flag_origin = empty($item->flag_origin) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_ORIGIN_OPTION_' . strtoupper($item->flag_origin));

			if (isset($item->origin_location))
			{
				$values    = explode(',', $item->origin_location);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db    = Factory::getDbo();
						$query = "select '' as 'key', '' as 'value' from htc_nsd_tracking_lu_locations a union select b.name as 'key', b.name as 'value' from htc_nsd_tracking_lu_locations b where b.state = 1 HAVING key LIKE '" . $value . "'";

						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$item->origin_location = !empty($textValue) ? implode(', ', $textValue) : $item->origin_location;
			}
				$item->flag_destination = empty($item->flag_destination) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_DESTINATION_OPTION_' . strtoupper($item->flag_destination));

			if (isset($item->destination_location))
			{
				$values    = explode(',', $item->destination_location);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db    = Factory::getDbo();
						$query = "select '' as 'key', '' as 'value' from htc_nsd_tracking_lu_locations a union select b.name as 'key', b.name as 'value' from htc_nsd_tracking_lu_locations b where b.state = 1 HAVING key LIKE '" . $value . "'";

						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$item->destination_location = !empty($textValue) ? implode(', ', $textValue) : $item->destination_location;
			}
				$item->flag_estimated_delivery_date = empty($item->flag_estimated_delivery_date) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_ESTIMATED_DELIVERY_DATE_OPTION_' . strtoupper($item->flag_estimated_delivery_date));

			if (isset($item->estimated_delivery))
			{
				$values    = explode(',', $item->estimated_delivery);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db    = Factory::getDbo();
						$query = "select '' as 'key', '' as 'value' from htc_nsd_tracking_lu_estimated_delivery a union select b.name as 'key', b.name as 'value' from htc_nsd_tracking_lu_estimated_delivery b where b.state = 1 HAVING key LIKE '" . $value . "'";

						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$item->estimated_delivery = !empty($textValue) ? implode(', ', $textValue) : $item->estimated_delivery;
			}
				$item->flag_scheduled_delivery_date = empty($item->flag_scheduled_delivery_date) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_SCHEDULED_DELIVERY_DATE_OPTION_' . strtoupper($item->flag_scheduled_delivery_date));
				$item->flag_scheduled_pickup_date = empty($item->flag_scheduled_pickup_date) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_SCHEDULED_PICKUP_DATE_OPTION_' . strtoupper($item->flag_scheduled_pickup_date));
				$item->flag_actual_pickup_date = empty($item->flag_actual_pickup_date) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_ACTUAL_PICKUP_DATE_OPTION_' . strtoupper($item->flag_actual_pickup_date));
				$item->flag_actual_delivery_date = empty($item->flag_actual_delivery_date) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_ACTUAL_DELIVERY_DATE_OPTION_' . strtoupper($item->flag_actual_delivery_date));
				$item->flag_pod_information = empty($item->flag_pod_information) ? '' : JText::_('COM_NSD_TRACKING_TRACKINGMATRIXS_FLAG_POD_INFORMATION_OPTION_' . strtoupper($item->flag_pod_information));
		}

		return $items;
	}

	/**
	 * Overrides the default function to check Date fields format, identified by
	 * "_dateformat" suffix, and erases the field if it's not correct.
	 *
	 * @return void
	 */
	protected function loadFormData()
	{
		$app              = Factory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;

		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && $this->isValidDate($value) == null)
			{
				$filters[$key]    = '';
				$error_dateformat = true;
			}
		}

		if ($error_dateformat)
		{
			$app->enqueueMessage(JText::_("COM_NSD_TRACKING_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	/**
	 * Checks if a given date is valid and in a specified format (YYYY-MM-DD)
	 *
	 * @param   string  $date  Date to be checked
	 *
	 * @return bool
	 */
	private function isValidDate($date)
	{
		$date = str_replace('/', '-', $date);
		return (date_create($date)) ? Factory::getDate($date)->format("Y-m-d") : null;
	}
}
