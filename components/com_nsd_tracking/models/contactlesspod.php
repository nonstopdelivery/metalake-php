<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_tracking
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Nsd_tracking model.
 *
 * @since  1.6
 */
class Nsd_trackingModelContactlesspod extends JModelItem
{
}


