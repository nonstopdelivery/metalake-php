<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_sendgrid
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Nsd_sendgrid', JPATH_COMPONENT);
JLoader::register('Nsd_sendgridController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Nsd_sendgrid');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
