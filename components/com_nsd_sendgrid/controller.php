<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Nsd_sendgrid
 * @author     Lew Sawyer <lsawyer@metalake.com>
 * @copyright  2018 Lew Sawyer
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JLoader::register('MetalakeHelperCore', JPATH_SITE.'/components/com_metalake/helpers/core.php');

/**
 * Class Nsd_sendgridController
 *
 * @since  1.6
 */
class Nsd_sendgridController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'logs');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}




	public static function sendSendgrid( $objSendEmailMessage )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_sendgrid&task=sendSendgrid


		# SendGrid Documentation
		# https://sendgrid.com/docs/api-reference/
		# https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/index.html


		$controllerName = "Nsd_sendgridController";
		$functionName = "sendSendgrid";
		
		$db = JFactory::getDBO();


        $app            		= JFactory::getApplication();
        $params         		= $app->getParams('com_nsd_sendgrid');
        
        $paramsComponent = new stdClass();
        
		$paramsComponent->sendgrid_key				= $params->get('sendgrid_key');


		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "sendgrid_verbose";
            $objLogger->loggerProd = 1;
            $objLogger->logFileProd = "sendgrid_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "sendgrid_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "sendgrid_prod";
	
		}	




		

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objSendEmailMessage: ".print_r($objSendEmailMessage, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			$urltopost = "https://api.sendgrid.com/v3/mail/send";
	
			$requestBody = new stdClass();
			
			
			
				$arrPersonalizations = array();
				
					$arrTo = array();
				
						$arrEmailsTo = explode(",", $objSendEmailMessage->email_to);
				
				
						foreach($arrEmailsTo as $singleEmail )
						{

							$objTo = new stdClass();
							$objTo->email = $singleEmail;
							$arrTo[] = $objTo;
							
						}
				
					
					$objPersonalizations = new stdClass();
						$objPersonalizations->to = $arrTo;
						$objPersonalizations->subject = $objSendEmailMessage->email_subject;
				
				$arrPersonalizations[] = $objPersonalizations;

			$requestBody->personalizations = $arrPersonalizations;
			
			
			
			
			
			
			
				$objFrom = new stdClass();
				$objFrom->email = $objSendEmailMessage->email_from;
				$objFrom->name = ( $objSendEmailMessage->email_from_name != "" ) ? $objSendEmailMessage->email_from_name : $objSendEmailMessage->email_from ;
			
			$requestBody->from = $objFrom;
			
			
			
			
			
			
				$arrContent = array();
					$objContent = new stdClass();
					$objContent->type = "text/html";
					$objContent->value = $objSendEmailMessage->email_body;
				$arrContent[] = $objContent;
	
			$requestBody->content = $arrContent;




			$arrCategories = array();
			$arrCategories[] = $objSendEmailMessage->email_category;
			
			$requestBody->categories = $arrCategories;


			if ( $objSendEmailMessage->attachment_content != "" && $objSendEmailMessage->attachment_filename != "" )
			{

				$arrAttachments = array();
					$objAttachment = new stdClass();
					$objAttachment->content = $objSendEmailMessage->attachment_content;
					$objAttachment->filename = $objSendEmailMessage->attachment_filename;
				$arrAttachments[] = $objAttachment;
					
					
				$requestBody->attachments = $arrAttachments;

				
			}




		
			$jsonRequestBody = json_encode($requestBody);
		
		
			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r urltopost: ".print_r($urltopost, true);
			$logMessage .= "\n\r jsonRequestBody: ".print_r($jsonRequestBody, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }



			$authorization = "Authorization: Bearer ".$paramsComponent->sendgrid_key;
			
		

			$ch = curl_init ($urltopost);
			
			curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
			curl_setopt ($ch, CURLOPT_POST, true);
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonRequestBody);
			curl_setopt ($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
		
			
			
		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r ch: ".print_r($ch, true);
		$logMessage .= "\n\r urltopost: ".print_r($urltopost, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }


		$returnData = curl_exec ($ch);


		if( $returnData === false )
		{
			#curl error
			
			$error = curl_error($ch);

			$objSendEmailMessage->error = "1";
			$objSendEmailMessage->error_code = "60";
			$objSendEmailMessage->error_message = "Curl error: ".curl_error($ch);
			$objSendEmailMessage->note = "There was an error accessing SendGrid via curl.";
		
			
		}
		else
		{
			
			
			$objReturnData = json_decode($returnData);
			
			$objSendEmailMessage->objReturn = $returnData;

			$arrCurlHeader = curl_getinfo($ch);


			$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
			$logMessage .= "\n\r ch: ".print_r($ch, true);
			$logMessage .= "\n\r urltopost: ".print_r($urltopost, true);
			$logMessage .= "\n\r returnData: ".print_r($returnData, true);
			$logMessage .= "\n\r objReturnData: ".print_r($objReturnData, true);
			$logMessage .= "\n\r arrCurlHeader: ".print_r($arrCurlHeader, true);
			$logMessage .= "\n\r ";
			if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
			
			
			switch( $arrCurlHeader["http_code"] )
			{
				case "200":

					$objSendEmailMessage->error = "0";
					$objSendEmailMessage->error_code = "";
					$objSendEmailMessage->error_message = "";
					$objSendEmailMessage->note = "The request that you made is valid and successful.";
				
					break;
				
				case "202":

					$objSendEmailMessage->error = "0";
					$objSendEmailMessage->error_code = "";
					$objSendEmailMessage->error_message = "";
					$objSendEmailMessage->note = "Your message is both valid and queued to be delivered.";
				
					break;				

				case "400":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "400";
					$objSendEmailMessage->error_message = "BAD REQUEST";
					$objSendEmailMessage->note = $objReturnData->errors[0]->field. " | ".$objReturnData->errors[0]->message;
				
					break;				
	

				case "401":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "401";
					$objSendEmailMessage->error_message = "UNAUTHORIZED";
					$objSendEmailMessage->note = "You do not have authorization to make the request.";
				
					break;	
				
				case "403":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "403";
					$objSendEmailMessage->error_message = "FORBIDDEN";
					$objSendEmailMessage->note = "You do not have authorization to make the request.";
				
					break;	

				case "404":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "404";
					$objSendEmailMessage->error_message = "NOT FOUND";
					$objSendEmailMessage->note = "The resource you tried to locate could not be found or does not exist.";
				
					break;	
					

				case "405":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "405";
					$objSendEmailMessage->error_message = "METHOD NOT ALLOWED";
					$objSendEmailMessage->note = "API method not allowed";
				
					break;	

				case "413":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "413";
					$objSendEmailMessage->error_message = "PAYLOAD TOO LARGE";
					$objSendEmailMessage->note = "The JSON payload you have included in your request is too large";
				
					break;	

				case "429":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "429";
					$objSendEmailMessage->error_message = "TOO MANY REQUESTS";
					$objSendEmailMessage->note = "The number of requests you have made exceeds SendGrid’s rate limitations";
				
					break;	

				case "500":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "500";
					$objSendEmailMessage->error_message = "SERVER UNAVAILABLE";
					$objSendEmailMessage->note = "An error occurred on a SendGrid server.";
				
					break;	

				case "503":

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "503";
					$objSendEmailMessage->error_message = "SERVICE NOT AVAILABLE";
					$objSendEmailMessage->note = "The SendGrid v3 Web API is not available.";
				
					break;	

				default:

					$objSendEmailMessage->error = "1";
					$objSendEmailMessage->error_code = "61";
					$objSendEmailMessage->error_message = "Unknown error from SendGrid";
					$objSendEmailMessage->note = "Unknown error from SendGrid";
				
					break;

														
			}
			
			
			
			
			
			
		}



		



		#log
		$objDataInsert = new stdClass();
		$objDataInsert->email_to = $objSendEmailMessage->email_to;
		$objDataInsert->email_from = $objSendEmailMessage->email_from;
		$objDataInsert->email_subject = $objSendEmailMessage->email_subject;
		$objDataInsert->email_body = $objSendEmailMessage->email_body;
		$objDataInsert->objReturn = $objSendEmailMessage->objReturn;
		$objDataInsert->error = $objSendEmailMessage->error;
		$objDataInsert->error_code = $objSendEmailMessage->error_code;
		$objDataInsert->error_message = $objSendEmailMessage->error_message;
		$objDataInsert->stamp_datetime = date("Y-m-d H:i:s");
		$objDataInsert->stamp_datetime_gmt = gmdate("Y-m-d H:i:s");
		$objDataInsert->note = $objSendEmailMessage->note;
		
		$result = JFactory::getDbo()->insertObject('htc_sendgrid_log', $objDataInsert);
		#$activityID = $db->insertid();


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objSendEmailMessage: ".print_r($objSendEmailMessage, true);
		$logMessage .= "\n\r objDataInsert: ".print_r($objDataInsert, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }



		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		
		
		return $objSendEmailMessage;
		
		
	}



// !COMMON FUNCTIONS

// !TEST FUNCTIONS

	public function test_sendSendgrid( )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_sendgrid&task=test_sendSendgrid



		$controllerName = "Nsd_sendgridController";
		$functionName = "test_sendSendgrid";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "sendgrid_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "sendgrid_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "sendgrid_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "sendgrid_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		$objSendEmailMessage = new stdClass();
		$objSendEmailMessage->email_to = "lsawyer@metalake.com,lewsawyer@gmail.com,7039305383@txt.att.net";
		$objSendEmailMessage->email_from = "rfeldman@metalake.com";
		$objSendEmailMessage->email_subject = "test double subject";
		$objSendEmailMessage->email_body = "test double body<br>test body";
		$objSendEmailMessage->email_category = "scheduling support";
		
		$objReturn = Nsd_sendgridController::sendSendgrid( $objSendEmailMessage );


		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r objSendEmailMessage: ".print_r($objSendEmailMessage, true);
		$logMessage .= "\n\r objReturn: ".print_r($objReturn, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}


// !FUNCTION TEMPLATE

	public function test_function_template( $file_name=null )
	{
		# http://dev6.metalake.net/index.php?option=com_nsd_sendgrid&task=test_function_template



		$controllerName = "Nsd_sendgridController";
		$functionName = "test_function_template";
		
		$db = JFactory::getDBO();
		
		$flag_production = 0;
		
		if ( $flag_production )
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "sendgrid_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "sendgrid_prod";

		}
		else
		{

            $objLogger = new stdClass();
            $objLogger->logger = 0;
            $objLogger->logFile = "sendgrid_verbose";
            $objLogger->loggerProd = 0;
            $objLogger->logFileProd = "sendgrid_prod";
	
		}	

		$stamp_start_micro = microtime(true);

		$logMessage = "START | ".$controllerName." | ".$functionName;	
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$helloWorld = "Hello World";

		$logMessage = "INSIDE | ".$controllerName." | ".$functionName;
		$logMessage .= "\n\r helloWorld: ".print_r($helloWorld, true);
		$logMessage .= "\n\r ";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }


		$stamp_end_micro = microtime(true);
		$stamp_total_micro = ($stamp_end_micro - $stamp_start_micro);


		$logMessage = "END | ".$controllerName." | ".$functionName." | seconds: ".number_format($stamp_total_micro,2);			
		$logMessage .= "\n\r \n\r";
		if ( $objLogger->logger ) { MetalakeHelperCore::logger( $objLogger->logFile, $logMessage ); }
		if ( $objLogger->loggerProd ) { MetalakeHelperCore::logger( $objLogger->logFileProd, $logMessage ); }

		
	}	

}
