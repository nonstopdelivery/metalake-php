<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentError $this */

if (!isset($this->error))
{
	$this->error = JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
	$this->debug = false;
}

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="apple-touch-icon" sizes="180x180" href="/templates/nonstopdelivery/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/templates/nonstopdelivery/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/templates/nonstopdelivery/favicons/favicon-16x16.png">
	<link rel="manifest" href="/templates/nonstopdelivery/favicons/site.webmanifest">
	<link rel="mask-icon" href="/templates/nonstopdelivery/favicons/safari-pinned-tab.svg" color="#2b5797">
	<link rel="shortcut icon" href="/templates/nonstopdelivery/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#2b5797">
	<meta name="msapplication-config" content="/templates/nonstopdelivery/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<jdoc:include type="head" />
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->

	<link rel="stylesheet" href="/templates/nonstopdelivery/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="/templates/nonstopdelivery/css/bootstrap-theme.min.css" type="text/css" />
	<link rel="stylesheet" href="/templates/nonstopdelivery/css/template.css" type="text/css" />

	<script src="/templates/nonstopdelivery/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet">


</head>
<body class='site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($bannerclass ? ' banner-' . $bannerclass : '');
?>'>

	<a name='top'></a>

	<!-- Navigation -->
	<div class="navigation">
		<div class="container">
			<div class="row">
				<div class="navigation-logo col-sm-3">
					<a class="navbar-brand" href="/">
						<img src="/templates/nonstopdelivery/images/logo.jpg" alt="<?php echo $sitename ?>" />
					</a>
				</div>
				<div class="navigation-content col-sm-9">
					<nav class="navbar navbar-default" role="navigation">
						<div class="container-fluid">

					    	<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
					
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="navbar-collapse-1">
								<?php
								foreach (JModuleHelper::getModules('nav') as $module)
								print(JModuleHelper::renderModule($module, array('style'=>'xhtml')));
								?>
							</div><!-- /.navbar-collapse -->

						</div><!-- /.container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</div>

	<!-- Main Content -->
	<div class="mainbody">
		<div class="container">
			<div class="row">
				
				<!-- Content -->
				<div id="content" class="col-sm-12">
					<div class="padding">
					<?php if ($this->error->getCode() == '404') : ?>
					<h1>Page Not Found</h1>
					<?php else : ?>
					<h1>Something Went Wrong</h1>
					<?php endif; ?>
					<h2>Error code: <?php echo $this->error->getCode(); ?></h2>
					<?php if ($this->error->getCode() == '404') : ?>
					<strong><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></strong><br /><Br />
						<ol>
							<li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
							<li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
							<li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
							<li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
							<li><?php echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND'); ?></li>
							<li><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></li>
						</ol>
					<?php endif; ?>
					<br />
					<br />
					If difficulties persist, please <a href='/contact-us'>contact us</a>.<br /><br /><br /><br /><br /><br />
					<?php if ($this->debug) : ?>
						<div>
							<?php echo $this->renderBacktrace(); ?>
							<?php // Check if there are more Exceptions and render their data as well ?>
							<?php if ($this->error->getPrevious()) : ?>
								<?php $loop = true; ?>
								<?php // Reference $this->_error here and in the loop as setError() assigns errors to this property and we need this for the backtrace to work correctly ?>
								<?php // Make the first assignment to setError() outside the loop so the loop does not skip Exceptions ?>
								<?php $this->setError($this->_error->getPrevious()); ?>
								<?php while ($loop === true) : ?>
									<p><strong><?php echo JText::_('JERROR_LAYOUT_PREVIOUS_ERROR'); ?></strong></p>
									<p>
										<?php echo htmlspecialchars($this->_error->getMessage(), ENT_QUOTES, 'UTF-8'); ?>
										<br/><?php echo htmlspecialchars($this->_error->getFile(), ENT_QUOTES, 'UTF-8');?>:<?php echo $this->_error->getLine(); ?>
									</p>
									<?php echo $this->renderBacktrace(); ?>
									<?php $loop = $this->setError($this->_error->getPrevious()); ?>
								<?php endwhile; ?>
								<?php // Reset the main error object to the base error ?>
								<?php $this->setError($this->error); ?>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					</div>
				</div>
				
			</div>
		</div>
	</div>

	<!-- End Interior Page -->

	<!-- Footer -->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="footer-content col-sm-12">
				<?php
				foreach (JModuleHelper::getModules('footer') as $module)
				print(JModuleHelper::renderModule($module, array('style'=>'xhtml')));
				?>
				</div>
			</div>
		</div>
	</div>

	<?php
	foreach (JModuleHelper::getModules('debug') as $module)
	print(JModuleHelper::renderModule($module, array('style'=>'none')));
	?>
	</body>
</html>
