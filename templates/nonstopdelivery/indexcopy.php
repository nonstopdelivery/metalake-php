<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.master
 *
 * @copyright   Copyright (C) 2015 MetaLake, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');
$active = JFactory::getApplication()->getMenu()->getActive();

$this->setGenerator(null);
$doc->setMetaData( 'author', '' );

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet('templates/'.$this->template.'/css/bootstrap.min.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/bootstrap-theme.min.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css?v=2.07');
$doc->addStyleSheet('templates/'.$this->template.'/css/main-menu.css?v=2.03');

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);


// Stop Joomla from loading bootstrap - we do it as part of template
unset($doc->_scripts[$this->baseurl.'/media/jui/js/bootstrap.min.js']);
unset($doc->_scripts[$this->baseurl.'/media/jui/js/bootstrap.js']);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125038545-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-125038545-1');
	</script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="apple-touch-icon" sizes="180x180" href="/templates/nonstopdelivery/favicons/apple-touch-icon.png?v=8j8EErbmYR">
	<link rel="icon" type="image/png" sizes="32x32" href="/templates/nonstopdelivery/favicons/favicon-32x32.png?v=8j8EErbmYR">
	<link rel="icon" type="image/png" sizes="16x16" href="/templates/nonstopdelivery/favicons/favicon-16x16.png?v=8j8EErbmYR">
	<link rel="manifest" href="/templates/nonstopdelivery/favicons/site.webmanifest?v=8j8EErbmYR">
	<link rel="mask-icon" href="/templates/nonstopdelivery/favicons/safari-pinned-tab.svg?v=8j8EErbmYR" color="#2b5797">
	<link rel="shortcut icon" href="/templates/nonstopdelivery/favicons/favicon.ico?v=8j8EErbmYR">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-config" content="/templates/nonstopdelivery/favicons/browserconfig.xml?v=8j8EErbmYR">
	<meta name="theme-color" content="#ffffff">
	<jdoc:include type="head" />
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->

<!-- 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->

	<script src="<?php echo $this->baseurl ?>templates/<?php echo $this->template ?>/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet">
	<script src="<?php echo $this->baseurl ?>templates/<?php echo $this->template ?>/js/validator.js"></script>
	<script src="<?php echo $this->baseurl ?>templates/<?php echo $this->template ?>/js/responsive-tabs.js"></script>

	<!--[if lte IE 10]>
		<link href="/templates/conservationfund/css/ie10_fix.css" rel="stylesheet" type="text/css">
	<![endif]-->


</head>
<body class='site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($bannerclass ? ' banner-' . $bannerclass : '');
?>'>

	<a name='top'></a>

	<?php if (!$this->countModules('admin')) : ?>
	<!-- Navigation -->
	<div class="navigation" data-spy="affix" data-offset-top="1">
		<div class="container-fluid">
			<div class="row">
				<div class="navigation-logo">
					<a class="navbar-brand" href="https://www.shipnsd.com">
						<img src="<?php echo $this->baseurl ?>templates/<?php echo $this->template ?>/images/logo.png" alt="<?php echo $sitename ?>" />
					</a>
					<nav class="navbar navbar-default" role="navigation">
						<div class="container-fluid">

					    	<!-- Brand and toggle get grouped for better mobile display -->
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
					
							<div class="collapse navbar-collapse overlay overlay-scale" id="navbar-collapse-1">
								<button type="button" class="overlay-close">Close</button>
								<jdoc:include type="modules" name="mainnav" style="xhtml" />
							</div><!-- /.navbar-collapse -->

							<jdoc:include type="modules" name="nav" style="xhtml" />


						</div><!-- /.container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<!-- Main Content -->
	<div class="mainbody">
		<?php if ($this->countModules('admin')) : ?>
		<div class="container-fluid">
		<?php else : ?>
		<div class="container">
		<?php endif; ?>
			<div class="row">
				
				<?php if ($this->countModules('left')) : ?>

				<!-- Content -->
				<div id="content" class="col-sm-9 col-sm-push-3">
					<div class="padding">
						<jdoc:include type="message" />
						<jdoc:include type="component" />
					</div>
				</div>
		
				<!-- Left Column -->
				<div id="left" class="col-sm-3 col-sm-pull-9">
					<div class="padding">
						<jdoc:include type="modules" name="left" style="xhtml" />
					</div>
				</div>
				
				<?php else : ?>

				<!-- Content -->
				<div id="content" class="col-sm-12">
					<div class="padding">
						<jdoc:include type="message" />
						<jdoc:include type="component" />
					</div>
				</div>
				
				<?php endif; ?>

			</div>
		</div>
	</div>

	<!-- End Interior Page -->

	<?php if (!$this->countModules('admin')) : ?>
	<!-- Bottom -->
	<div class="bottom">
		<div class="container">
			<div class="row">
				<div class="bottom-content col-sm-12">
					<jdoc:include type="modules" name="bottom" style="xhtml" />
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>


	<?php if (!$this->countModules('admin')) : ?>
	<!-- Footer -->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="footer-content col-sm-12">
					<?php if (!$this->countModules('admin')) : ?>
					<jdoc:include type="modules" name="footer" style="xhtml" />
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

<?php if (!$this->countModules('admin')) { ?>
	<script type="text/javascript">
		var el = document.querySelector('.overlay-scale');
		el.onclick = function() {
			el.classList.toggle('in');
			el.classList.toggle('out');
		}
	</script>
<?php } ?>

<?php if (!$this->countModules('admin')) : ?>
<!-- <script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'a5234876-9fec-4d80-8a7f-00d6bc46c9f6', f: true }); done = true; } }; })();</script> -->
<?php endif; ?>

	<jdoc:include type="modules" name="debug" style="none" />
	</body>


	<jdoc:include type="modules" name="chat" style="none" />

</html>
