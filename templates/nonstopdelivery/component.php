<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.acus
 *
 * @copyright   Copyright (C) 2013 MetaLake, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later
 */

defined('_JEXEC') or die;

$app   = JFactory::getApplication();
$doc   = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');

// Load optional rtl Bootstrap css and Bootstrap bugfixes
JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<jdoc:include type="head" />
<!--[if lt IE 9]>
	<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
<![endif]-->
</head>
<body class="contentpane modal">
	<img class='logo' src="<?php echo $this->baseurl ?>templates/<?php echo $this->template ?>/images/logo.png" alt="<?php echo $sitename ?>" />
	<br /><br />	
	<hr />
	<br />
	<table>
		<tr style='vertical-align:top'>

			<!-- Content -->
			<td id="content" width="72.5%";>
				<div class="padding" style='padding:0 30px 0 0;'>
					<jdoc:include type="message" />
					<jdoc:include type="component" />
				</div>
			</td>
	
			<!-- Right Column -->
			<td id="right" width="27.5%">
				<div class="padding">
					<?php
					foreach (JModuleHelper::getModules('t_right') as $module)
					print(JModuleHelper::renderModule($module, array('style'=>'xhtml')));
					?>
				</div>
			</td>

		</tr>
	</table>




	<br />
	<hr />
	<br />
	Copyright &copy; <?php echo date('Y'); ?> The Conservation Fund<br />
	<br />
</body>
</html>
