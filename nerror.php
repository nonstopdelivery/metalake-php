<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="apple-touch-icon" sizes="180x180" href="/templates/nonstopdelivery/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/templates/nonstopdelivery/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/templates/nonstopdelivery/favicons/favicon-16x16.png">
	<link rel="mask-icon" href="/templates/nonstopdelivery/favicons/safari-pinned-tab.svg" color="#2b5797">
	<link rel="shortcut icon" href="/templates/nonstopdelivery/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#2b5797">
	<meta name="theme-color" content="#ffffff">
</head>
<body class="site com_nsd_tracking view-trackingmatrixs no-layout no-task itemid-380">
	<!--[if lt IE 9]>
		<script src="/media/jui/js/html5.js"></script>
	<![endif]-->

	<link rel="stylesheet" href="/templates/nonstopdelivery/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="/templates/nonstopdelivery/css/bootstrap-theme.min.css" type="text/css">
	<link rel="stylesheet" href="/templates/nonstopdelivery/css/template.css" type="text/css">

	<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script src="/templates/nonstopdelivery/js/bootstrap.min.js"></script>





	<a name="top"></a>

	<!-- Navigation -->
	<div class="navigation">
		<div class="container">
			<div class="row">
				<div class="navigation-logo col-sm-3">
					<a class="navbar-brand" href="https://tracking.shipnsd.com/">
						<img src="/templates/nonstopdelivery/images/logo.jpg" alt="NSD">
					</a>
				</div>
				<div class="navigation-content col-sm-9">
					<nav class="navbar navbar-default" role="navigation">
						<div class="container-fluid">

					    	<!-- Brand and toggle get grouped for better mobile display -->
<!--
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
					
-->
							<!-- Collect the nav links, forms, and other content for toggling -->
<!-- 							<div class="collapse navbar-collapse" id="navbar-collapse-1"> -->
										<div class="moduletable loginmenu">
											<ul class="nav menu mod-list">
												<li class="item-492 current active"><a href="https://tracking.shipnsd.com/">Track</a></li>
												<li class="item-488 deeper parent"><span class="nav-header ">Login</span>
													<ul class="nav-child unstyled small">
														<li class="item-489"><a href="https://nstracking.nonstopdelivery.com/selfserve/login.do?start=true">Account Login</a></li>
														<li class="item-490"><a href="https://nstracking.nonstopdelivery.com/agentconsole/">Carrier Login</a></li>
														<li class="item-491"><a href="https://portal.shipnsd.com/login.msw">TM Login</a></li>
													</ul>
												</li>
											</ul>
										</div>
<!-- 							</div> --><!-- /.navbar-collapse -->

						</div><!-- /.container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</div>

	<!-- Main Content -->
	<div class="mainbody">
		<div class="container">
			<div class="row">
				
				<!-- Content -->
				<div id="content" class="col-sm-12">
					<div class="padding">
						<h1>Processing Error</h1>
						<strong>We're currently unable to process your request.</strong><br /><br />
						Notification has been sent to the support team to review and resolve the issue.	If difficulties persist, please <a href="https://www.shipnsd.com/contact-us">contact us</a>.
						<br /><br /><br /><br /><br /><br />
					</div>
				</div>
				
			</div>
		</div>
	</div>

	<!-- End Interior Page -->

	<!-- Footer -->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="footer-content col-sm-12">
						<div class="moduletable pull-right connect">
						

<div class="custom pull-right connect">
	<a href="https://www.facebook.com/NonstopDeliveryUSA" target="_blank" rel="noopener noreferrer"><img src="/images/facebook.png" alt="facebook"></a><a href="https://www.twitter.com/nsdeliveryusa" target="_blank" rel="noopener noreferrer"><img src="/images/twitter.png" alt="twitter"></a><a href="https://www.linkedin.com/company/nonstopdelivery-inc-" target="_blank" rel="noopener noreferrer"><img src="/images/linkedin.png" alt="linkedin"></a></div>
		</div>
			<div class="moduletable">
						

<div class="custom">
	© Copyright NSD. All Rights Reserved <span class="sep">|</span> <a href="http://www.shipnsd.com/terms-and-conditions">Terms of Use</a> • <a href="http://www.shipnsd.com/privacy-policy">Privacy Policy</a></div>
		</div>
					</div>
			</div>
		</div>
	</div>

</body></html>
<?php

	$objSendEmailMessage = new stdClass();
	$objSendEmailMessage->email_to = "itsupport@nonstopdelivery.com,support@metalake.com";
	#$objSendEmailMessage->email_to = "lsawyer@metalake.com";
	$objSendEmailMessage->email_from = "alert@shipnsd.com";
	$objSendEmailMessage->email_subject = "ALERT | tracking.shipnsd.com  ";
	$objSendEmailMessage->email_body = date("Y-m-d H:i:s")." | The site at tracking.shipnsd.com is failing to connect to database.";
	$objSendEmailMessage->email_body .= "<br><br>The number of database connections has been exceeded on the server hosting tracking.shipnsd.com.";
	$objSendEmailMessage->email_body .= "<br><br>To acknowledge these alerts and mute them, click: https://tracking.shipnsd.com/nerror.php?setstatus=acknowledge";
	$objSendEmailMessage->email_body .= "<br><br>Once the issue has been resolved, reset the alert monitoring by clicking: https://tracking.shipnsd.com/nerror.php?setstatus=reset";	
	$objSendEmailMessage->email_category = "NSD APP support";

	#sendMessage($objSendEmailMessage);
	$action = isset($_GET["setstatus"]) ? $_GET["setstatus"] : "noaction";
	
	switch( $action )
	{
		
		case "acknowledge":
			$myfile = fopen("nerrorstatus.txt", "w") or die("Unable to open file!");
			$txt = "MUTE";
			fwrite($myfile, $txt);
			fclose($myfile);	
	
			break;
		
		case "reset":
			$myfile = fopen("nerrorstatus.txt", "w") or die("Unable to open file!");
			$txt = "ACTIVE";
			fwrite($myfile, $txt);
			fclose($myfile);		
			break;

		case "init":
			$myfile = fopen("nerrorstatus.txt", "w") or die("Unable to open file!");
			$txt = "MUTE";
			fwrite($myfile, $txt);
			fclose($myfile);		
			break;
		
		default:
			break;
		
	}


	$sendStatus = file_get_contents('nerrorstatus.txt');
	#echo "<br />The action is: ".$action;
	#echo "<br />The sendStatus is: ".$sendStatus;

	if ( $sendStatus == "ACTIVE" && $action != "reset" )
	{
		sendMessage($objSendEmailMessage);
	}

	function sendMessage( $objSendEmailMessage )
	{
			
			$objConfig = new stdClass();
			$objConfig->urlToPost = "https://api.sendgrid.com/v3/mail/send";
			$objConfig->sendGridKey = "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";
			
			
			$requestBody = new stdClass();
			
				$arrPersonalizations = array();
				
					$arrTo = array();
				
						$arrEmailsTo = explode(",", $objSendEmailMessage->email_to);
				
				
						foreach($arrEmailsTo as $singleEmail )
						{

							$objTo = new stdClass();
							$objTo->email = $singleEmail;
							$arrTo[] = $objTo;
							
						}
				
					
					$objPersonalizations = new stdClass();
						$objPersonalizations->to = $arrTo;
						$objPersonalizations->subject = $objSendEmailMessage->email_subject;
				
				$arrPersonalizations[] = $objPersonalizations;

			$requestBody->personalizations = $arrPersonalizations;
			
			
				$objFrom = new stdClass();
				$objFrom->email = $objSendEmailMessage->email_from;
				$objFrom->name = ( $objSendEmailMessage->email_from_name != "" ) ? $objSendEmailMessage->email_from_name : $objSendEmailMessage->email_from ;
			
			$requestBody->from = $objFrom;
			
			
				$arrContent = array();
					$objContent = new stdClass();
					$objContent->type = "text/html";
					$objContent->value = $objSendEmailMessage->email_body;
				$arrContent[] = $objContent;
	
			$requestBody->content = $arrContent;

			
			if ( $objSendEmailMessage->email_attachment_filename != "" &&  $objSendEmailMessage->email_attachment_base64 !="" )
			{
				$arrAttachments = array();
					$objAttachment = new stdClass();
					$objAttachment->filename = $objSendEmailMessage->email_attachment_filename;
					$objAttachment->content = $objSendEmailMessage->email_attachment_base64;
				$arrAttachments[] = $objAttachment;
	
				$requestBody->attachments = $arrAttachments;
			}


			$arrCategories = array();
			$arrCategories[] = $objSendEmailMessage->email_category;
			
			$requestBody->categories = $arrCategories;


			if ( $objSendEmailMessage->attachment_content != "" && $objSendEmailMessage->attachment_filename != "" )
			{
				$arrAttachments = array();
					$objAttachment = new stdClass();
					$objAttachment->content = $objSendEmailMessage->attachment_content;
					$objAttachment->filename = $objSendEmailMessage->attachment_filename;
				$arrAttachments[] = $objAttachment;
					
					
				$requestBody->attachments = $arrAttachments;
			}
		
			$jsonRequestBody = json_encode($requestBody);


			$authorization = "Authorization: Bearer ".$objConfig->sendGridKey;


			$ch = curl_init ( $objConfig->urlToPost );
			
			curl_setopt ($ch, CURL_HTTP_VERSION_1_1, true);
			curl_setopt ($ch, CURLOPT_POST, true);
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $jsonRequestBody);
			curl_setopt ($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$returnData = curl_exec ($ch);

			$objReturnData = json_decode($returnData);

			$arrCurlHeader = curl_getinfo($ch);
		
	}

?>
